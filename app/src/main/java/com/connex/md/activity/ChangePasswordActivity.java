package com.connex.md.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.connex.md.R;
import com.connex.md.interfaces.AsyncTaskListner;
import com.connex.md.others.App;
import com.connex.md.patient.activity.PTDashboardActivity;
import com.connex.md.ws.CallRequest;
import com.connex.md.ws.Constant;
import com.connex.md.ws.MyConstants;
import com.connex.md.ws.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ChangePasswordActivity extends AppCompatActivity implements AsyncTaskListner {

    EditText etOldPassword, etNewPassword, etConfirmPassword;
    Button btnChangePassword, btnCancel;
    SharedPreferences sharedpreferences;
    SharedPreferences.Editor editor;
    String newPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);

        sharedpreferences = getSharedPreferences(MyConstants.PREF, MODE_PRIVATE);

        etOldPassword = findViewById(R.id.etOldPassword);
        etNewPassword = findViewById(R.id.etNewPassword);
        etConfirmPassword = findViewById(R.id.etConfirmPassword);
        btnChangePassword = findViewById(R.id.btnChangePassword);
        btnCancel = findViewById(R.id.btnCancel);

        btnChangePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callChangePasswordAPI();
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

    }

    private void callChangePasswordAPI() {

        String currentPassword = etOldPassword.getText().toString().trim();
        newPassword = etNewPassword.getText().toString().trim();
        String confirmPassword = etConfirmPassword.getText().toString().trim();

        if (TextUtils.isEmpty(currentPassword)){
            etOldPassword.requestFocus();
            etOldPassword.setError("Enter old password");
            return;
        } else if (TextUtils.isEmpty(newPassword)){
            etNewPassword.requestFocus();
            etNewPassword.setError("Enter new password");
            return;
        } else if (TextUtils.isEmpty(confirmPassword)){
            etConfirmPassword.requestFocus();
            etConfirmPassword.setError("Enter confirm password");
            return;
        }

        Map<String, String> map = new HashMap<String, String>();

        if (App.user.getUser_Type().equalsIgnoreCase(MyConstants.USER_PT)) {
            map.put("url", MyConstants.BASE_URL + "changePassword");
            map.put("UserId", App.user.getUserID());
        } else {
            map.put("url", MyConstants.DOCTOR_BASE_URL + "changePassword");
            map.put("DoctorId", App.user.getUserID());
        }
        map.put("ApiToken", MyConstants.API_TOKEN);
        map.put("Version", MyConstants.WS_VERSION);
        map.put("CurrentPassword", currentPassword);
        map.put("password", newPassword);
        map.put("ConfirmPassword", confirmPassword);

        new CallRequest(ChangePasswordActivity.this).changePassword(map);
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        Utils.removeSimpleSpinProgressDialog();
        try {
            if (result != null && !result.isEmpty()) {
                Log.i("TAG", "TAG Result : " + result);
                //      {"document":{"response":{"status":1,"message":"Success."}}}
                switch (request) {
                    case changePassword:
                        try {
                            JSONObject mainObj = new JSONObject(result);
                            if (mainObj.getString("error_code").equalsIgnoreCase("0")) {
                                Utils.showToast(mainObj.getString("error_string"), ChangePasswordActivity.this);
                                etOldPassword.setText("");
                                etNewPassword.setText("");
                                etConfirmPassword.setText("");
                                //Utils.showToast("Password changed successfully", ChangePasswordActivity.this);

                                if (sharedpreferences.getString(MyConstants.USER_TYPE, "").equalsIgnoreCase(MyConstants.USER_PT)) {
                                    if (!TextUtils.isEmpty(sharedpreferences.getString(MyConstants.PT_PASSWORD, ""))) {
                                        editor = sharedpreferences.edit();
                                        editor.putString(MyConstants.PT_PASSWORD,newPassword);
                                        editor.commit();
                                    }
                                } else if (sharedpreferences.getString(MyConstants.USER_TYPE, "").equalsIgnoreCase(MyConstants.USER_DR)){
                                    if (!TextUtils.isEmpty(sharedpreferences.getString(MyConstants.DR_PASSWORD, ""))) {
                                        editor = sharedpreferences.edit();
                                        editor.putString(MyConstants.DR_PASSWORD,newPassword);
                                        editor.commit();
                                    }
                                }

                                Intent intent = new Intent(ChangePasswordActivity.this, PTDashboardActivity.class);
                                startActivity(intent);
                                ActivityCompat.finishAffinity(ChangePasswordActivity.this);

                            } else {
                                Utils.showToast(mainObj.getString("error_string"), ChangePasswordActivity.this);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onProgressUpdate(String uniqueMessageId, int progres) {

    }

    @Override
    public void onProgressComplete(String uniqueMessageId, String result, Constant.REQUESTS request) {

    }
}
