package com.connex.md.activity;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.LinearLayout;

import com.connex.md.custom_views.CenteredToolbar;
import com.crashlytics.android.Crashlytics;
import com.connex.md.R;
import com.connex.md.custom_views.RobottoTextView;
import com.connex.md.ws.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.fabric.sdk.android.Fabric;

public class FaqActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    CenteredToolbar mToolbar;

    public WebView wbeView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_faq);

        ButterKnife.bind(this);

        setupToolbar();

        Utils.logUser();

        wbeView = findViewById(R.id.wbeView);

        WebSettings settings = wbeView.getSettings();
        //settings.setMinimumFontSize(20);
        settings.setDefaultFontSize(30);
        settings.setAllowFileAccess(true);
        settings.setAllowContentAccess(true);
        settings.setAllowFileAccessFromFileURLs(true);
        settings.setLoadWithOverviewMode(true);
        settings.setUseWideViewPort(true);
        wbeView.setInitialScale(1);
        wbeView.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
        wbeView.setScrollbarFadingEnabled(false);
        wbeView.loadUrl("file:///android_asset/faq.html");

    }


    private void setupToolbar() {
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle("FAQs");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
