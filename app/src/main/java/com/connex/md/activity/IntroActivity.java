package com.connex.md.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import com.connex.md.R;
import com.connex.md.fragments.Tour1Fragment;
import com.connex.md.interfaces.FragmentChangeListener;
import com.connex.md.patient.activity.PTLoginActivity;
import com.connex.md.ws.MyConstants;

public class IntroActivity extends AppCompatActivity implements FragmentChangeListener {

    SharedPreferences sharedpreferences;
    SharedPreferences.Editor editor;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro);


        sharedpreferences = getSharedPreferences(MyConstants.PREF, MODE_PRIVATE);
        editor = sharedpreferences.edit();

        if (sharedpreferences.getBoolean(MyConstants.IS_FIRST, false)) {
            /*editor.putBoolean(MyConstants.IS_FIRST, true);
            editor.commit();*/
            Intent intent = new Intent(IntroActivity.this, PTLoginActivity.class);
            startActivity(intent);
            ActivityCompat.finishAffinity(IntroActivity.this);
        }

        Fragment fragment = new Tour1Fragment();
        replaceFragment(fragment);

    }


    @Override
    public void replaceFragment(Fragment fragment) {
        try {
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.container, fragment, fragment.toString());
            /*fragmentTransaction.addToBackStack(fragment.toString());*/
            fragmentTransaction.commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
