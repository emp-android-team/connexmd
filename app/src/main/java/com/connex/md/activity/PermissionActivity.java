package com.connex.md.activity;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import com.anthonycr.grant.PermissionsManager;
import com.anthonycr.grant.PermissionsResultAction;
import com.connex.md.R;
import com.connex.md.custom_views.SmoothCheckBox;
import com.connex.md.patient.activity.PTLoginActivity;
import com.connex.md.service.GPSTracker;
import com.connex.md.ws.MyConstants;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class PermissionActivity extends AppCompatActivity implements
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {

    private static final int REQUEST_CODE_PERMISSION = 2;
    SmoothCheckBox cbMic, cbCamera, cbLocation;
    Button btnGetStarted;
    SharedPreferences sharedpreferences;
    SharedPreferences.Editor editor;
    String[] location = {Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.ACCESS_FINE_LOCATION};
    String[] camera = {Manifest.permission.CAMERA,
            Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE};
    String[] mic = {Manifest.permission.RECORD_AUDIO, Manifest.permission.READ_PHONE_STATE};
    GoogleApiClient mGoogleApiClient;
    Location mLastLocation;
    LocationRequest mLocationRequest;
    LinearLayout llMic, llCamera, llLocation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_permission);

        sharedpreferences = getSharedPreferences(MyConstants.PREF, MODE_PRIVATE);
        editor = sharedpreferences.edit();


        cbMic = findViewById(R.id.cbMic);
        cbCamera = findViewById(R.id.cbCamera);
        cbLocation = findViewById(R.id.cbLocation);
        btnGetStarted = findViewById(R.id.btnGetStarted);
        llMic = findViewById(R.id.llMic);
        llCamera = findViewById(R.id.llCamera);
        llLocation = findViewById(R.id.llLocation);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            cbMic.setEnabled(true);
            cbCamera.setEnabled(true);
        } else {
            cbMic.setChecked(true);
            cbCamera.setChecked(true);
            cbMic.setEnabled(false);
            cbCamera.setEnabled(false);
        }


        cbMic.setOnCheckedChangeListener(new SmoothCheckBox.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(SmoothCheckBox compoundButton, boolean b) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (b) {
                        PermissionsManager.getInstance().requestPermissionsIfNecessaryForResult(PermissionActivity.this, mic, new PermissionsResultAction() {
                            @Override
                            public void onGranted() {
                                System.out.println("Granted");
                            }

                            @Override
                            public void onDenied(String permission) {
                                System.out.println("Denied");
                            }
                        });
                    }
                }
            }
        });

        cbCamera.setOnCheckedChangeListener(new SmoothCheckBox.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(SmoothCheckBox compoundButton, boolean b) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (b) {
                        PermissionsManager.getInstance().requestPermissionsIfNecessaryForResult(PermissionActivity.this, camera, new PermissionsResultAction() {
                            @Override
                            public void onGranted() {
                                System.out.println("Granted");
                            }

                            @Override
                            public void onDenied(String permission) {
                                System.out.println("Denied");
                            }
                        });
                    }
                }
            }
        });

        cbLocation.setOnCheckedChangeListener(new SmoothCheckBox.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(SmoothCheckBox compoundButton, boolean b) {

                if (b) {

                    mGoogleApiClient = new GoogleApiClient.Builder(PermissionActivity.this)
                            .addConnectionCallbacks(PermissionActivity.this)
                            .addOnConnectionFailedListener(PermissionActivity.this)
                            .addApi(LocationServices.API)
                            .build();

                    if (mGoogleApiClient != null) {
                        mGoogleApiClient.connect();
                    } else {
                        //Toast.makeText(this, "Not Connected!", Toast.LENGTH_SHORT).show();
                        System.out.println("mGoogleApiClient Not Connected!");
                    }

                    GPSTracker gpsTracker = new GPSTracker(PermissionActivity.this);
                    gpsTracker.getLocation();

                }

            }
        });

        btnGetStarted.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                editor.putBoolean(MyConstants.IS_FIRST, true);
                editor.commit();

                Intent intent = new Intent(PermissionActivity.this, PTLoginActivity.class);
                startActivity(intent);
                ActivityCompat.finishAffinity(PermissionActivity.this);
            }
        });
    }

    private void startLocation() {
        /*Getting the location after acquiring location service*/

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            ActivityCompat.requestPermissions(PermissionActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_CODE_PERMISSION);
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                mGoogleApiClient);

        if (mLastLocation != null) {
            System.out.println("Latitude::::" + String.valueOf(mLastLocation.getLatitude()));
            System.out.println("Longitude::::" + String.valueOf(mLastLocation.getLongitude()));
            getAddress(mLastLocation.getLatitude(), mLastLocation.getLongitude());
        } else {
            /*if there is no last known location. Which means the device has no data for the location currently.
             * So we will get the current location.
             * For this we'll implement Location Listener and override onLocationChanged*/
            Log.i("Current Location", "No data for location found");

            if (!mGoogleApiClient.isConnected())
                mGoogleApiClient.connect();

            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, PermissionActivity.this);

        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        settingRequest();
    }

    @Override
    public void onConnectionSuspended(int i) {
        System.out.println("connection suspended");
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        if (connectionResult.hasResolution()) {
            try {
                // Start an Activity that tries to resolve the error
                connectionResult.startResolutionForResult(this, 90000);
            } catch (IntentSender.SendIntentException e) {
                e.printStackTrace();
            }
        } else {
            Log.i("Current Location", "Location services connection failed with code " + connectionResult.getErrorCode());
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        mLastLocation = location;
        System.out.println("Latitude::::" + String.valueOf(mLastLocation.getLatitude()));
        System.out.println("Longitude::::" + String.valueOf(mLastLocation.getLongitude()));
        getAddress(mLastLocation.getLatitude(), mLastLocation.getLongitude());
    }

    private void getAddress(double latitude, double longitude) {
        Geocoder geocoder;
        List<Address> addresses;
        geocoder = new Geocoder(this, Locale.getDefault());

        try {
            addresses = geocoder.getFromLocation(latitude, longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
            String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
            String city = addresses.get(0).getLocality();
            String state = addresses.get(0).getAdminArea();
            String country = addresses.get(0).getCountryName();
            String postalCode = addresses.get(0).getPostalCode();
            String knownName = addresses.get(0).getFeatureName(); // Only if available else return NULL

            System.out.println("address:::" + address);
            System.out.println("city::" + city);
            System.out.println("postal code:::" + postalCode);
        } catch (IOException e) {
             e.printStackTrace();
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == REQUEST_CODE_PERMISSION && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            startLocation();
        }
    }

    @Override
    protected void onStop() {
        if (mGoogleApiClient != null)
            mGoogleApiClient.disconnect();
        super.onStop();
    }

    /*Method to get the enable location settings dialog*/
    public void settingRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(10000);    // 10 seconds, in milliseconds
        mLocationRequest.setFastestInterval(1000);   // 1 second, in milliseconds
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);

        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient,
                        builder.build());

        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {

            @Override
            public void onResult(@NonNull LocationSettingsResult result) {
                final Status status = result.getStatus();
                final LocationSettingsStates state = result.getLocationSettingsStates();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        // All location settings are satisfied. The client can
                        // initialize location requests here.
                        startLocation();
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        // Location settings are not satisfied, but this can be fixed
                        // by showing the user a dialog.
                        try {
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            status.startResolutionForResult(PermissionActivity.this, 1000);
                        } catch (IntentSender.SendIntentException e) {
                            // Ignore the error.
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        // Location settings are not satisfied. However, we have no way
                        // to fix the settings so we won't show the dialog.
                        break;
                }
            }

        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        try {
            final LocationSettingsStates states = LocationSettingsStates.fromIntent(data);
            switch (requestCode) {
                case 1000:
                    switch (resultCode) {
                        case Activity.RESULT_OK:
                            // All required changes were successfully made
                            startLocation();
                            break;
                        case Activity.RESULT_CANCELED:
                            // The user was asked to change settings, but chose not to
                            System.out.println("Location Service not Enabled");
                            //Toast.makeText(this, "Location Service not Enabled", Toast.LENGTH_SHORT).show();
                            break;
                        default:
                            break;
                    }
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
