package com.connex.md.activity;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.connex.md.R;
import com.connex.md.interfaces.AsyncTaskListner;
import com.connex.md.ws.CallRequest;
import com.connex.md.ws.Constant;
import com.connex.md.ws.MyConstants;
import com.connex.md.ws.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ResetPasswordActivity extends AppCompatActivity implements AsyncTaskListner {

    Button btnResetPassword, btnCancel;
    EditText etEmail, etPhone;
    String user_type;
    public static Activity mActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password);

        mActivity = this;

        btnResetPassword = findViewById(R.id.btnResetPassword);
        btnCancel = findViewById(R.id.btnCancel);
        etEmail = findViewById(R.id.etEmail);
        etPhone = findViewById(R.id.etPhone);
        etEmail = findViewById(R.id.etEmail);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            user_type = bundle.getString("user_type");
        }

        btnResetPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callResetPasswordAPI();
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    private void callResetPasswordAPI() {
        String email = etEmail.getText().toString().trim();
        String phone = etPhone.getText().toString().trim();

        if (TextUtils.isEmpty(email)) {
            Utils.showToast("Enter email", ResetPasswordActivity.this);
            return;
        }

        System.out.println("phone::" + phone);

        Map<String, String> map = new HashMap<String, String>();
        if (user_type.equalsIgnoreCase("patient")) {
            map.put("url", MyConstants.BASE_URL + "sendResetPassword");
        } else {
            map.put("url", MyConstants.DOCTOR_BASE_URL + "sendResetPassword");
        }
        map.put("Version", MyConstants.WS_VERSION);
        map.put("email", email);
        //map.put("Phone", phone);

        new CallRequest(ResetPasswordActivity.this).sendResetPassword(map);

    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        Utils.removeSimpleSpinProgressDialog();
        try {
            if (result != null && !result.isEmpty()) {
                Log.i("TAG", "TAG Result : " + result);
                //      {"document":{"response":{"status":1,"message":"Success."}}}
                switch (request) {
                    case sendResetPassword:
                        try {
                            JSONObject mainObj = new JSONObject(result);
                            if (mainObj.getString("error_code").equalsIgnoreCase("0")) {

                                JSONObject resultObj = mainObj.getJSONArray("result").getJSONObject(0);

                                //String EmailToken = resultObj.getString("EmailToken");
                                String id = resultObj.getString("id");
                                String email = resultObj.getString("email");

                                String phone = "";
                                if (resultObj.has("phone")){
                                    phone = resultObj.getString("phone");
                                }

                                etEmail.setText("");
                                etPhone.setText("");

                                Intent intent = new Intent(ResetPasswordActivity.this, ResetPasswordOTPActivity.class);
                                intent.putExtra("user_id", id);
                                intent.putExtra("user_type", user_type);
                                intent.putExtra("email", email);
                                intent.putExtra("phone", phone);
                                startActivity(intent);
                                finish();

                                //Utils.showToast(mainObj.getString("error_string"), ResetPasswordActivity.this);

                            } else {
                                Utils.showToast(mainObj.getString("error_string"), ResetPasswordActivity.this);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onProgressUpdate(String uniqueMessageId, int progres) {

    }

    @Override
    public void onProgressComplete(String uniqueMessageId, String result, Constant.REQUESTS request) {

    }
}
