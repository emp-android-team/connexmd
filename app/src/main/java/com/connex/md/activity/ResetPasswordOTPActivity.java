package com.connex.md.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.alimuzaffar.lib.pin.PinEntryEditText;
import com.connex.md.R;
import com.connex.md.doctor.activity.DRLoginActivity;
import com.connex.md.interfaces.AsyncTaskListner;
import com.connex.md.patient.activity.PTLoginActivity;
import com.connex.md.ws.CallRequest;
import com.connex.md.ws.Constant;
import com.connex.md.ws.MyConstants;
import com.connex.md.ws.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ResetPasswordOTPActivity extends AppCompatActivity implements AsyncTaskListner {

    PinEntryEditText pinView;
    Button btnSendAgain, btnResetPassword;
    String OTP, user_id, user_type, email, phone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password_otp);

        pinView = findViewById(R.id.pinview);
        btnSendAgain = findViewById(R.id.btnSendAgain);
        btnResetPassword = findViewById(R.id.btnResetPassword);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            user_id = bundle.getString("user_id");
            user_type = bundle.getString("user_type");
            email = bundle.getString("email");
            phone = bundle.getString("phone");
        }

        btnResetPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (pinView.getText().toString().length() != 6){
                    pinView.setError("Enter 6 digit password");
                    pinView.requestFocus();
                    return;
                }

                callResetPasswordOTPApi();
            }
        });

        btnSendAgain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                pinView.setText("");

                callResetPasswordAPI();
            }
        });
    }

    private void callResetPasswordAPI() {

        System.out.println("phone::" + phone);

        Map<String, String> map = new HashMap<String, String>();
        if (user_type.equalsIgnoreCase("patient")) {
            map.put("url", MyConstants.BASE_URL + "sendResetPassword");
        } else {
            map.put("url", MyConstants.DOCTOR_BASE_URL + "sendResetPassword");
        }
        map.put("Version", MyConstants.WS_VERSION);
        map.put("email", email);
        //map.put("Phone", phone);

        new CallRequest(ResetPasswordOTPActivity.this).sendResetPassword(map);

    }

    private void callResetPasswordOTPApi() {

        OTP = pinView.getText().toString();
        System.out.println("otp::" + OTP);

        if (TextUtils.isEmpty(OTP)) {
            Utils.showToast("Please enter valid OTP", ResetPasswordOTPActivity.this);
        } else {

            Map<String, String> map = new HashMap<String, String>();
            if (user_type.equalsIgnoreCase("patient")) {
                map.put("url", MyConstants.BASE_URL + "resetPassword");
                map.put("UserId", user_id);
            } else {
                map.put("url", MyConstants.DOCTOR_BASE_URL + "resetPassword");
                map.put("DoctorId", user_id);
            }
            map.put("Version", MyConstants.WS_VERSION);
            map.put("OtpToken", OTP);

            new CallRequest(ResetPasswordOTPActivity.this).resetPassword(map);
        }
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        Utils.removeSimpleSpinProgressDialog();
        try {
            if (result != null && !result.isEmpty()) {
                Log.i("TAG", "TAG Result : " + result);
                //      {"document":{"response":{"status":1,"message":"Success."}}}
                switch (request) {
                    case resetPassword:
                        try {
                            JSONObject mainObj = new JSONObject(result);
                            if (mainObj.getString("error_code").equalsIgnoreCase("0")) {

                                new AlertDialog.Builder(ResetPasswordOTPActivity.this)
                                        .setMessage(mainObj.getString("error_string"))
                                        .setCancelable(false)
                                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                if (user_type.equalsIgnoreCase("patient")) {
                                                    ResetPasswordActivity.mActivity.finish();
                                                    Intent intent = new Intent(ResetPasswordOTPActivity.this, PTLoginActivity.class);
                                                    startActivity(intent);
                                                    finish();
                                                } else {
                                                    DRLoginActivity.mActivity.finish();
                                                    ResetPasswordActivity.mActivity.finish();
                                                    Intent intent = new Intent(ResetPasswordOTPActivity.this, DRLoginActivity.class);
                                                    startActivity(intent);
                                                    finish();
                                                }
                                            }
                                        })
                                        .show();

                            } else {
                                Utils.showToast(mainObj.getString("error_string"), ResetPasswordOTPActivity.this);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;

                    case sendResetPassword:
                        try {
                            JSONObject mainObj = new JSONObject(result);
                            if (mainObj.getString("error_code").equalsIgnoreCase("0")) {

                                JSONObject resultObj = mainObj.getJSONArray("result").getJSONObject(0);

                                //String EmailToken = resultObj.getString("EmailToken");
                                user_id = resultObj.getString("id");
                                email = resultObj.getString("email");

                                phone = "";
                                if (resultObj.has("phone")){
                                    phone = resultObj.getString("phone");
                                }

                                Utils.showToast(mainObj.getString("error_string"), ResetPasswordOTPActivity.this);

                            } else {
                                Utils.showToast(mainObj.getString("error_string"), ResetPasswordOTPActivity.this);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onProgressUpdate(String uniqueMessageId, int progres) {

    }

    @Override
    public void onProgressComplete(String uniqueMessageId, String result, Constant.REQUESTS request) {

    }
}
