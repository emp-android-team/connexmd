package com.connex.md.activity;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;

import com.connex.md.R;
import com.connex.md.custom_views.CenteredToolbar;
import com.connex.md.interfaces.AsyncTaskListner;
import com.connex.md.others.App;
import com.connex.md.others.Internet;
import com.connex.md.ws.CallRequest;
import com.connex.md.ws.Constant;
import com.connex.md.ws.MyConstants;
import com.connex.md.ws.Utils;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SettingsActivity extends AppCompatActivity implements AsyncTaskListner, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {

    private static final int REQUEST_CODE_PERMISSION = 2;

    @BindView(R.id.toolbar)
    CenteredToolbar mToolbar;

    LinearLayout llChangePassword, llVerify;
    Switch switchLocation;
    Button btnSendEmail;
    TextView tvVerified;
    GoogleApiClient mGoogleApiClient;
    Location mLastLocation;
    LocationRequest mLocationRequest;
    View line1;
    private SharedPreferences sharedpreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        ButterKnife.bind(this);
        setupToolbar();

        llChangePassword = findViewById(R.id.ll_change_password);
        llVerify = findViewById(R.id.llVerify);
        switchLocation = findViewById(R.id.switchLocation);
        line1 = findViewById(R.id.line1);
        btnSendEmail = findViewById(R.id.btnSendEmail);
        tvVerified = findViewById(R.id.tvVerified);

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();

        btnSendEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendEmail();
            }
        });

        sharedpreferences = getSharedPreferences(MyConstants.PREF, Context.MODE_PRIVATE);

        checkValidation();

        if (App.user.getUser_Type().equalsIgnoreCase(MyConstants.USER_PT) && MyConstants.isGuest.equalsIgnoreCase("0")
                && App.user.getLoginType().equalsIgnoreCase("0")) {
            llChangePassword.setVisibility(View.VISIBLE);
            line1.setVisibility(View.VISIBLE);
        } else if (App.user.getUser_Type().equalsIgnoreCase(MyConstants.USER_DR)) {
            llChangePassword.setVisibility(View.VISIBLE);
            line1.setVisibility(View.VISIBLE);
        } else {
            llChangePassword.setVisibility(View.GONE);
            line1.setVisibility(View.GONE);
        }

        if (App.user.getUser_Type().equalsIgnoreCase(MyConstants.USER_PT) && MyConstants.isGuest.equalsIgnoreCase("0")) {
            callVerifyAccountUsingLink();
            llVerify.setVisibility(View.VISIBLE);
        } else {
            llVerify.setVisibility(View.GONE);
        }

        final LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        if (manager != null && !manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            switchLocation.setChecked(false);
        } else {
            switchLocation.setChecked(true);
        }

        llChangePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(SettingsActivity.this, ChangePasswordActivity.class);
                startActivity(intent);
            }
        });

        switchLocation.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    mGoogleApiClient = new GoogleApiClient.Builder(SettingsActivity.this)
                            .addConnectionCallbacks(SettingsActivity.this)
                            .addOnConnectionFailedListener(SettingsActivity.this)
                            .addApi(LocationServices.API)
                            .build();
                    if (mGoogleApiClient != null) {
                        mGoogleApiClient.connect();
                    } else {
                        //Toast.makeText(this, "Not Connected!", Toast.LENGTH_SHORT).show();
                        System.out.println("mGoogleApiClient Not Connected!");
                    }
                } else {
                    final LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

                    if (manager != null && manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                        startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }

                    //turnGPSOff();
                }
            }
        });

    }


    private void setupToolbar() {
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle("Settings");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void checkValidation() {
        if (App.user.getUser_Type() == null) {
            if (sharedpreferences.getBoolean(MyConstants.IS_LOGGED_IN, false)) {
                App.user.setUserID(sharedpreferences.getString(MyConstants.USER_ID, ""));
                App.user.setName(sharedpreferences.getString(MyConstants.PT_NAME, ""));
                App.user.setFirstName(sharedpreferences.getString(MyConstants.PT_FIRST_NAME, ""));
                App.user.setLastName(sharedpreferences.getString(MyConstants.PT_LAST_NAME, ""));
                App.user.setUserEmail(sharedpreferences.getString(MyConstants.USER_EMAIL, ""));
                App.user.setUser_Type(sharedpreferences.getString(MyConstants.USER_TYPE, ""));
                App.user.setSinch_id(sharedpreferences.getString(MyConstants.SINCH_ID, ""));
                App.user.setLoginType(sharedpreferences.getString(MyConstants.LOGIN_TYPE, "0"));
                App.user.setProfileUrl(sharedpreferences.getString(MyConstants.PROFILE_PIC, ""));
                MyConstants.API_TOKEN = sharedpreferences.getString(MyConstants.TOKEN, "");

                if (sharedpreferences.getBoolean(MyConstants.IS_GUEST, false)) {
                    MyConstants.isGuest = "1";
                } else {
                    MyConstants.isGuest = "0";
                }
            }
        }
    }

    private void sendEmail() {
        if (!Internet.isAvailable(this)) {
            Internet.showAlertDialog(this, "Error!", "No Internet Connection", false);

            return;
        }

        Map<String, String> map = new HashMap<String, String>();
        map.put("url", MyConstants.BASE_URL + "sendEmailLink");
        map.put("UserId", App.user.getUserID());
        map.put("Version", MyConstants.WS_VERSION);
        map.put("ApiToken", MyConstants.API_TOKEN);

        new CallRequest(SettingsActivity.this).sendEmail(map);
    }

    private void callVerifyAccountUsingLink() {
        if (!Internet.isAvailable(this)) {
            Internet.showAlertDialog(this, "Error!", "No Internet Connection", false);

            return;
        }

        Map<String, String> map = new HashMap<String, String>();
        map.put("url", MyConstants.BASE_URL + "checkUserVerify");
        map.put("UserId", App.user.getUserID());
        map.put("Version", MyConstants.WS_VERSION);

        new CallRequest(SettingsActivity.this).isUserVerified(map);
    }

    @Override
    protected void onResume() {
        final LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        if (manager != null && !manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            switchLocation.setChecked(false);
        } else {
            switchLocation.setChecked(true);
        }
        super.onResume();
    }

    private void turnGPSOff() {
        String provider = Settings.Secure.getString(getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);

        if (provider.contains("gps")) { //if gps is enabled
            final Intent poke = new Intent();
            poke.setClassName("com.android.settings", "com.android.settings.widget.SettingsAppWidgetProvider");
            poke.addCategory(Intent.CATEGORY_ALTERNATIVE);
            poke.setData(Uri.parse("3"));
            sendBroadcast(poke);
        }
    }

    private void startLocation() {
        /*Getting the location after acquiring location service*/

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            ActivityCompat.requestPermissions(SettingsActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_CODE_PERMISSION);
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                mGoogleApiClient);

        if (mLastLocation != null) {
            System.out.println("Latitude::::" + String.valueOf(mLastLocation.getLatitude()));
            System.out.println("Longitude::::" + String.valueOf(mLastLocation.getLongitude()));
            getAddress(mLastLocation.getLatitude(), mLastLocation.getLongitude());
        } else {
            /*if there is no last known location. Which means the device has no data for the location currently.
             * So we will get the current location.
             * For this we'll implement Location Listener and override onLocationChanged*/
            Log.i("Current Location", "No data for location found");

            if (!mGoogleApiClient.isConnected())
                mGoogleApiClient.connect();

            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, SettingsActivity.this);

        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == REQUEST_CODE_PERMISSION && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            startLocation();
        }
    }

    @Override
    protected void onStop() {
        mGoogleApiClient.disconnect();
        super.onStop();
    }

    /*Method to get the enable location settings dialog*/
    public void settingRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(10000);    // 10 seconds, in milliseconds
        mLocationRequest.setFastestInterval(1000);   // 1 second, in milliseconds
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);

        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient,
                        builder.build());

        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {

            @Override
            public void onResult(@NonNull LocationSettingsResult result) {
                final Status status = result.getStatus();
                final LocationSettingsStates state = result.getLocationSettingsStates();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        // All location settings are satisfied. The client can
                        // initialize location requests here.
                        startLocation();
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        // Location settings are not satisfied, but this can be fixed
                        // by showing the user a dialog.
                        try {
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            status.startResolutionForResult(SettingsActivity.this, 1000);
                        } catch (IntentSender.SendIntentException e) {
                            // Ignore the error.
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        // Location settings are not satisfied. However, we have no way
                        // to fix the settings so we won't show the dialog.
                        break;
                }
            }

        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        final LocationSettingsStates states = LocationSettingsStates.fromIntent(data);
        switch (requestCode) {
            case 1000:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        // All required changes were successfully made
                        startLocation();
                        break;
                    case Activity.RESULT_CANCELED:
                        // The user was asked to change settings, but chose not to
                        switchLocation.setChecked(false);
                        System.out.println("Location Service not Enabled");
                        //Toast.makeText(this, "Location Service not Enabled", Toast.LENGTH_SHORT).show();
                        break;
                    default:
                        break;
                }
                break;
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        settingRequest();
    }

    @Override
    public void onConnectionSuspended(int i) {
        System.out.println("connection suspended");
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        if (connectionResult.hasResolution()) {
            try {
                // Start an Activity that tries to resolve the error
                connectionResult.startResolutionForResult(this, 90000);
            } catch (IntentSender.SendIntentException e) {
                e.printStackTrace();
            }
        } else {
            Log.i("Current Location", "Location services connection failed with code " + connectionResult.getErrorCode());
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        mLastLocation = location;
        System.out.println("Latitude::::" + String.valueOf(mLastLocation.getLatitude()));
        System.out.println("Longitude::::" + String.valueOf(mLastLocation.getLongitude()));
        getAddress(mLastLocation.getLatitude(), mLastLocation.getLongitude());
    }

    private void getAddress(double latitude, double longitude) {
        Geocoder geocoder;
        List<Address> addresses;
        geocoder = new Geocoder(this, Locale.getDefault());

        try {
            addresses = geocoder.getFromLocation(latitude, longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
            String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
            String city = addresses.get(0).getLocality();
            String state = addresses.get(0).getAdminArea();
            String country = addresses.get(0).getCountryName();
            String postalCode = addresses.get(0).getPostalCode();
            String knownName = addresses.get(0).getFeatureName(); // Only if available else return NULL

            System.out.println("address:::" + address);
            System.out.println("city::" + city);
            System.out.println("postal code:::" + postalCode);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        Utils.removeSimpleSpinProgressDialog();
        try {
            if (result != null && !result.isEmpty()) {
                Log.i("TAG", "TAG Result : " + result);
                switch (request) {
                    case isVerified:
                        try {
                            JSONObject mainObj = new JSONObject(result);
                            if (mainObj.getString("error_code").equalsIgnoreCase("0")) {

                                JSONObject object = mainObj.getJSONArray("result").getJSONObject(0);

                                int IsVerified = object.getInt("IsVerified");

                                if (IsVerified == 1) {
                                    tvVerified.setVisibility(View.VISIBLE);
                                    btnSendEmail.setVisibility(View.GONE);
                                } else {
                                    //Utils.showToast("Please verify your account", SettingsActivity.this);
                                    tvVerified.setVisibility(View.GONE);
                                    btnSendEmail.setVisibility(View.VISIBLE);
                                }

                            } else {
                                Utils.showToast(mainObj.getString("error_string"), SettingsActivity.this);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;

                    case sendEmail:
                        try {
                            JSONObject mainObj = new JSONObject(result);
                            if (mainObj.getString("error_code").equalsIgnoreCase("0")) {
                                Utils.showToast(mainObj.getString("error_string"), SettingsActivity.this);
                            } else {
                                Utils.showToast(mainObj.getString("error_string"), SettingsActivity.this);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onProgressUpdate(String uniqueMessageId, int progres) {

    }

    @Override
    public void onProgressComplete(String uniqueMessageId, String result, Constant.REQUESTS request) {

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
