package com.connex.md.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.util.Log;
import android.widget.TextView;

import com.connex.md.R;
import com.connex.md.others.App;
import com.connex.md.utils.VersionHelper;
import com.connex.md.ws.MyConstants;
import com.onesignal.OneSignal;

import org.jsoup.Jsoup;

import io.fabric.sdk.android.services.concurrency.AsyncTask;

public class SplashActivity extends AppCompatActivity {

    private static int SPLASH_TIME_OUT = 2000;
    public String currentVersion = "";
    public int versionCode;
    App app;
    TextView tvDP;
    private SharedPreferences sharedpreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        app = App.getInstance();

        tvDP = findViewById(R.id.tvDP);

        String dpString = "ConnexMD <sup><small>TM</small></sup> 2018";
        tvDP.setText(Html.fromHtml(dpString));

        sharedpreferences = getSharedPreferences(MyConstants.PREF, Context.MODE_PRIVATE);

        // reload all data
        if (MyConstants.specialitiesList.size() == 0) {
            MyConstants.isLoadAgain = true;
        }

        if (MyConstants.bookingHistoryList.size() == 0) {
            MyConstants.isBookingHistoryLoad = true;
        }

        if (sharedpreferences.getBoolean(MyConstants.IS_LOGGED_IN, false)) {
            if (sharedpreferences.getString(MyConstants.USER_TYPE,"").equalsIgnoreCase(MyConstants.USER_PT)) {
                if (MyConstants.messageHistoryList.size() == 0) {
                    MyConstants.isMessageHistoryLoad = true;
                }
            } else if (sharedpreferences.getString(MyConstants.USER_TYPE,"").equalsIgnoreCase(MyConstants.USER_DR)){
                if (MyConstants.userChatList.size() == 0 || MyConstants.doctorChatList.size() == 0){
                    MyConstants.isMessageHistoryLoad = true;
                }
            } else {
                MyConstants.isMessageHistoryLoad = true;
            }
        } else {
            MyConstants.isMessageHistoryLoad = true;
        }

        if (MyConstants.userProfile == null){
            MyConstants.isProfileLoad = true;
        }

        if (MyConstants.doctorProfilePersonal == null){
            MyConstants.isDoctorProfileLoad = true;
        }

        OneSignal.idsAvailable(new OneSignal.IdsAvailableHandler() {
            @Override
            public void idsAvailable(String userId, String registrationId) {
                if (userId != null)
                    MyConstants.DEVICE_ID = userId;
                Log.d("debug", "User:" + userId);
            /*    if (registrationId != null)
                    MyConstants.DEVICE_ID = registrationId;*/
                Log.d("debug", "registrationId:" + registrationId);
            }
        });

//        MyConstants.DEVICE_ID = Secure.getString(getApplicationContext().getContentResolver(), Secure.ANDROID_ID);
        System.out.println("Device Id:::" + MyConstants.DEVICE_ID);

        App.user.setUser_DeviceID(MyConstants.DEVICE_ID);

        PackageInfo pInfo = null;
        try {
            pInfo = getPackageManager().getPackageInfo(this.getPackageName(), 0);

            currentVersion = pInfo.versionName;
            versionCode = pInfo.versionCode;
            new GetPlayStoreVersion().execute("");
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            getCompleted();
        }
    }

    private void getCompleted() {
        new Handler().postDelayed(new Runnable() {

            /*
             * Showing splash screen with a timer. This will be useful when you
             * want to show case your app logo / company
             */

            @Override
            public void run() {
                // This method will be executed once the timer is over
                // Start your app main activity
                Intent i = new Intent(SplashActivity.this, IntroActivity.class);
                startActivity(i);

                // close this activity
                finish();
            }
        }, SPLASH_TIME_OUT);
    }

    private class GetPlayStoreVersion extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... params) {
            try {
                return Jsoup.connect("https://play.google.com/store/apps/details?id=" + SplashActivity.this.getPackageName() + "&hl=en").timeout(30000).userAgent("Mozilla/5.0 (Windows; U; WindowsNT 5.1; en-US; rv1.8.1.6) Gecko/20070725 Firefox/2.0.0.6")
                        .referrer("http://www.google.com").get().select("div[itemprop=softwareVersion]").first().ownText();
            } catch (Exception e) {
                e.printStackTrace();
                return "";
            }

        }

        @Override
        protected void onPostExecute(String result) {

            Log.i("CURRENT VERSION : ", currentVersion);
            Log.i("CURRENT VERSION CODE : ", versionCode + "");
            Log.i("NEW VERSION : ", result);

            try {

                if (VersionHelper.compare(currentVersion, result) == -1) {
                    Intent i = new Intent(SplashActivity.this, UpdateActivity.class)
                            .putExtra("new", result).putExtra("current", currentVersion);
                    startActivity(i);
                    ActivityCompat.finishAffinity(SplashActivity.this);
                } else {
                    getCompleted();
                }

            } catch (NumberFormatException e) {
                e.printStackTrace();
                getCompleted();
            }
        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected void onProgressUpdate(Void... values) {
        }
    }
}
