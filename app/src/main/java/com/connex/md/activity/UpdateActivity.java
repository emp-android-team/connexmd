package com.connex.md.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;

import com.connex.md.R;
import com.connex.md.custom_views.RobottoTextView;
import com.connex.md.ws.MyConstants;


public class UpdateActivity extends Activity {


    // Splash screen timer
    private static int SPLASH_TIME_OUT = 2000;
    public SharedPreferences shared;
    public String IS_FIRST = "", IS_INTRO_SEEN = "";
    public RobottoTextView tvNewVersion, tvCurrentVersion, tvSignUp;
    public String newVersion = "", currentVersion = "";
    private SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_update);

        shared = getSharedPreferences(MyConstants.PREF, Context.MODE_PRIVATE);


        newVersion = getIntent().getStringExtra("new");
        currentVersion = getIntent().getStringExtra("current");

        tvNewVersion = findViewById(R.id.tvNewVersion);
        tvCurrentVersion = findViewById(R.id.tvCurrentVersion);
        tvSignUp = findViewById(R.id.tvSignUp);
        tvSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editor = shared.edit();
                editor.putBoolean("isUpdated", true);
                editor.commit();
                final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                } catch (android.content.ActivityNotFoundException anfe) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                }
            }
        });

        tvNewVersion.setText("New Version " + newVersion + " is Available");
        tvCurrentVersion.setText(currentVersion);

    }


}