package com.connex.md.audio_video_calling;

import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.connex.md.R;
import com.connex.md.others.PicassoTrustAll;
import com.github.siyamed.shapeimageview.CircularImageView;
import com.sinch.android.rtc.AudioController;
import com.sinch.android.rtc.PushPair;
import com.sinch.android.rtc.SinchClient;
import com.sinch.android.rtc.calling.Call;
import com.sinch.android.rtc.calling.CallEndCause;
import com.sinch.android.rtc.calling.CallListener;
import com.squareup.picasso.Callback;

import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;
import static com.connex.md.ws.MyConstants.CALL_ID;

public class AudioCallActivity extends BaseActivity {

    public static final int VOICE_CALL = 888;
    static final String TAG = AudioCallActivity.class.getSimpleName();
    static final String CALL_START_TIME = "callStartTime";
    static final String ADDED_LISTENER = "addedListener";
    public CircularImageView img_recv_Profile;
    public ImageView img_specker, img_mic;
    boolean isSpiker = true, isMute = false;
    private Call call;
    //private TextView callState;
    private SinchClient sinchClient;
    private Button button;
    private String callerId;
    private String recipientId;
    private String mCallId, recvImageUrl, recvName,uniqueChatId,senderID,receiverID;
    private AudioPlayer mAudioPlayer;
    private Timer mTimer;
    private long mCallStart = 0;
    private boolean mAddedListener = false;
    private TextView mCallDuration;
    private TextView mCallerName;
    private AudioCallActivity.UpdateCallDurationTask mDurationTask;
    private LinearLayout llEndCall, llSpeaker, llMute, llPlus, llMinus;
    private ProgressBar progressBar;
    private AudioManager audioManager = null;
    private String callEndCause;
    private boolean isCallEnded = false;
    private boolean isDialing = false;

    @Override
    protected void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        savedInstanceState.putLong(CALL_START_TIME, mCallStart);
        savedInstanceState.putBoolean(ADDED_LISTENER, mAddedListener);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        mCallStart = savedInstanceState.getLong(CALL_START_TIME);
        mAddedListener = savedInstanceState.getBoolean(ADDED_LISTENER);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.audio_call_activity);

        mAudioPlayer = new AudioPlayer(this);

        mCallId = getIntent().getStringExtra(CALL_ID);
        recvImageUrl = getIntent().getStringExtra("recvImageUrl");
        recvName = getIntent().getStringExtra("recvName");
        isDialing = getIntent().getBooleanExtra("isDailing", false);
        uniqueChatId= getIntent().getStringExtra("uniqueChatId");
        senderID = getIntent().getStringExtra("senderID");
        receiverID = getIntent().getStringExtra("receiverID");
        /*Bundle bundle = getIntent().getExtras();
        if (bundle != null && bundle.containsKey("UniqueId")) {
            uniqueChatId = bundle.getString("UniqueId");
        }*/

        mCallDuration = findViewById(R.id.callDuration);
        img_mic = findViewById(R.id.img_mic);
        img_specker = findViewById(R.id.img_specker);
        mCallerName = findViewById(R.id.remoteUser);
        img_recv_Profile = findViewById(R.id.img_recv_Profile);

        llEndCall = findViewById(R.id.llEndCall);
        llSpeaker = findViewById(R.id.llSpeaker);
        llMute = findViewById(R.id.llMute);
        llMinus = findViewById(R.id.llMinus);
        llPlus = findViewById(R.id.llPlus);
        progressBar = findViewById(R.id.progressBar);

        progressBar.getProgressDrawable().setColorFilter(
                getResources().getColor(R.color.theme_color), android.graphics.PorterDuff.Mode.SRC_IN);
        initVolumeControls();

        //img_recv_Profile.startAnimation(AnimationUtils.loadAnimation(AudioCallActivity.this, R.anim.shake));

        Log.i("TAG", "mCallId ID::-->" + mCallId);
        //sinchClient.getCallClient().addCallClientListener(new SinchCallClientListener());

        button = findViewById(R.id.button);
        //callState = (TextView) findViewById(R.id.callState);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                endCall();

            }
        });

        ImageView endCallButton = findViewById(R.id.hangupButton);

        llEndCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isCallEnded = true;
                endCall();
            }
        });

        mCallerName.setText(recvName);

        try {
            PicassoTrustAll.getInstance(AudioCallActivity.this)
                    .load(recvImageUrl)
                    .placeholder(R.drawable.avatar)
                    .error(R.drawable.avatar)
                    .into(img_recv_Profile, new Callback() {
                        @Override
                        public void onSuccess() {
                        }

                        @Override
                        public void onError() {
                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (savedInstanceState == null) {
            mCallStart = System.currentTimeMillis();
        }
        llMute.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switchAudio();
            }
        });
        llSpeaker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switchspeker();
            }
        });
      /*  Log.i("TAG","CALL ID::-->"+call);
        if (call == null) {
            call = sinchClient.getCallClient().callUser(recipientId);
            call.addCallListener(new SinchCallListener());
            button.setText("Hang Up");
        } else {
            finish();
        }*/
    }

    private void initVolumeControls() {
        try {
            audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
            if (audioManager != null) {
                progressBar.setMax(audioManager
                        .getStreamMaxVolume(AudioManager.STREAM_MUSIC));
                System.out.println("volume::" + audioManager.getStreamVolume(AudioManager.STREAM_MUSIC));
                progressBar.setProgress(audioManager
                        .getStreamVolume(AudioManager.STREAM_MUSIC));

                llPlus.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        audioManager.adjustStreamVolume(AudioManager.STREAM_MUSIC, AudioManager.ADJUST_RAISE, AudioManager.FLAG_PLAY_SOUND);
                        System.out.println("volume::" + audioManager.getStreamVolume(AudioManager.STREAM_MUSIC));
                        progressBar.setProgress(audioManager
                                .getStreamVolume(AudioManager.STREAM_MUSIC));
                    }
                });

                llMinus.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        audioManager.adjustStreamVolume(AudioManager.STREAM_MUSIC, AudioManager.ADJUST_LOWER, AudioManager.FLAG_PLAY_SOUND);
                        System.out.println("volume::" + audioManager.getStreamVolume(AudioManager.STREAM_MUSIC));
                        progressBar.setProgress(audioManager
                                .getStreamVolume(AudioManager.STREAM_MUSIC));
                    }
                });
            }


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void updateCallDuration() {
        Log.i("TAG", "Call ::-->" + mCallStart);
        if (mCallStart > 0) {
            mCallDuration.setText(formatTimespan(System.currentTimeMillis() - mCallStart));

            Log.i("TAG", "Call Duration ::-->" + mCallDuration);

        }
    }

    private String formatTimespan(long timespan) {
        long totalSeconds = timespan / 1000;
        long minutes = totalSeconds / 60;
        long seconds = totalSeconds % 60;
        return String.format(Locale.US, "%d:%02d", minutes, seconds);
    }

    //method to update live duration of the call
    @Override
    public void onStop() {
        super.onStop();
        try {
            if (mDurationTask != null) {
                mDurationTask.cancel();
            }
            if (mTimer != null) {
                mTimer.cancel();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //start the timer for the call duration here
    @Override
    public void onStart() {
        super.onStart();
        updateUI();
    }

    private void updateUI() {
        if (getSinchServiceInterface() == null) {
            return; // early
        }

        Call call = getSinchServiceInterface().getCall(mCallId);
        if (call != null) {
            mCallerName.setText(recvName);

            try {
                PicassoTrustAll.getInstance(AudioCallActivity.this)
                        .load(recvImageUrl)
                        .placeholder(R.drawable.avatar)
                        .error(R.drawable.avatar)
                        .into(img_recv_Profile, new Callback() {
                            @Override
                            public void onSuccess() {
                            }

                            @Override
                            public void onError() {
                            }
                        });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onBackPressed() {
        // UserDetail should exit activity by ending call, not by going back.
    }

    @Override
    public void onServiceConnected() {
        Call call = getSinchServiceInterface().getCall(mCallId);
        if (call != null) {

            call.addCallListener(new SinchCallListener());

        } else {
            Log.e("TAG", "Started with invalid callId, aborting.");
            finish();
        }


    }

    private void endCall() {
        mAudioPlayer.stopProgressTone();
        Call call = getSinchServiceInterface().getCall(mCallId);
        if (call != null) {
            call.hangup();
        }
        finish();
    }

    public void switchAudio() {
        AudioManager audioManager = ((AudioManager) AudioCallActivity.this.getApplicationContext().getSystemService(Context.AUDIO_SERVICE));

        if (isMute) {
            img_mic.setImageResource(R.drawable.mic_mute);
            BaseActivity.getSinchServiceInterface().getAudioController().mute();
        } else {
            img_mic.setImageResource(R.drawable.mic_unmute);
            BaseActivity.getSinchServiceInterface().getAudioController().unmute();
        }

        isMute = !isMute;
    }

    public void switchspeker() {
        AudioManager audioManager = ((AudioManager) AudioCallActivity.this.getApplicationContext().getSystemService(Context.AUDIO_SERVICE));
        if (audioManager != null) {
            audioManager.setMode(AudioManager.MODE_IN_CALL);
            if (isSpiker) {
                audioManager.setSpeakerphoneOn(false);

            } else {
                audioManager.setSpeakerphoneOn(true);
            }

            audioManager.setMode(AudioManager.MODE_NORMAL);
        }
        if (isSpiker) {
            img_specker.setImageResource(R.drawable.speaker_mute);
            // BaseActivity.getSinchServiceInterface().getAudioController().disableSpeaker();
        } else {
            img_specker.setImageResource(R.drawable.speaker);
            //BaseActivity.getSinchServiceInterface().getAudioController().enableSpeaker();
        }

        isSpiker = !isSpiker;
    }

    private void returnData(CallEndCause cause) {
        Intent resultIntent = new Intent();
        resultIntent.putExtra("IsAudio", "1");
        resultIntent.putExtra("CallEndCause", cause.toString());
        resultIntent.putExtra("uniqueChatId", uniqueChatId);
        resultIntent.putExtra("senderID", senderID);
        resultIntent.putExtra("receiverID", receiverID);
        setResult(RESULT_OK, resultIntent);
    }

    private class UpdateCallDurationTask extends TimerTask {

        @Override
        public void run() {
            AudioCallActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    updateCallDuration();
                }
            });
        }
    }

    private class SinchCallListener implements CallListener {
        @Override
        public void onCallEnded(Call endedCall) {
            CallEndCause cause = endedCall.getDetails().getEndCause();
            Log.d(TAG, "Call ended. Reason: in interface:::" + cause.toString());

            System.out.println("is call ended-------->" + isCallEnded);
            if (isCallEnded || (cause.toString().equalsIgnoreCase("TIMEOUT") && isDialing)
                    || (cause.toString().equalsIgnoreCase("NO_ANSWER") && isDialing)
                    || (cause.toString().equalsIgnoreCase("DENIED") && isDialing)) {
                returnData(cause);
            }

            mAudioPlayer.stopProgressTone();
            setVolumeControlStream(AudioManager.USE_DEFAULT_STREAM_TYPE);
            call = null;

            endCall();
            finish();
        }

        @Override
        public void onCallEstablished(Call establishedCall) {
            mAudioPlayer.stopProgressTone();
            mTimer = new Timer();
            mDurationTask = new UpdateCallDurationTask();
            mTimer.schedule(mDurationTask, 0, 500);
            setVolumeControlStream(AudioManager.STREAM_VOICE_CALL);
            AudioController audioController = getSinchServiceInterface().getAudioController();
            audioController.enableSpeaker();
            mCallStart = System.currentTimeMillis();

            //callState.setText("connected");
        }

        @Override
        public void onCallProgressing(Call progressingCall) {
            //callState.setText("ringing");

            mAudioPlayer.playProgressTone();
        }

        @Override
        public void onShouldSendPushNotification(Call call, List<PushPair> pushPairs) {
        }
    }
}

