package com.connex.md.audio_video_calling;

import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.connex.md.firebase_chat.activity.FireChatActivity;
import com.sinch.android.rtc.calling.Call;
import com.sinch.android.rtc.calling.CallClient;

import static com.connex.md.others.App.mSinchServiceInterface;
import static com.connex.md.ws.MyConstants.CALL_ID;
import static com.connex.md.ws.MyConstants.VIDEO_CALL_REQUEST_CODE;
import static com.connex.md.ws.MyConstants.VOICE_CALL_REQUEST_CODE;

public abstract class BaseActivity extends AppCompatActivity implements ServiceConnection, SinchService.IncomingCallLisnter {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (mSinchServiceInterface == null) {
            Intent serviceIntent = new Intent(this, SinchService.class);
            serviceIntent.putExtra(SinchService.MESSENGER, messenger);
            getApplicationContext().bindService(serviceIntent, this, BIND_AUTO_CREATE);


        }
    }

    @Override
    public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        if (SinchService.class.getName().equals(componentName.getClassName())) {
            System.out.println("Sinch SinchServiceInterface Service Null");
            mSinchServiceInterface = (SinchService.SinchServiceInterface) iBinder;
            onServiceConnected();
            getSinchServiceInterface().setIncomingLisnter(this);
        }
    }

    @Override
    public void onServiceDisconnected(ComponentName componentName) {
        if (SinchService.class.getName().equals(componentName.getClassName())) {
            System.out.println("Sinch Service Null");
            mSinchServiceInterface = null;
            onServiceDisconnected();
        }
    }

    protected void onServiceConnected() {
        // for subclasses
    }

    protected void onServiceDisconnected() {
        // for subclasses
    }

    public static SinchService.SinchServiceInterface getSinchServiceInterface() {
        return mSinchServiceInterface;
    }

    private Messenger messenger = new Messenger(new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case SinchService.MESSAGE_PERMISSIONS_NEEDED:
                    Bundle bundle = msg.getData();
                    String requiredPermission = bundle.getString(SinchService.REQUIRED_PERMISSION);
                    ActivityCompat.requestPermissions(BaseActivity.this, new String[]{requiredPermission}, 0);
                    break;
            }
        }
    });

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        boolean granted = grantResults.length > 0;
        for (int grantResult : grantResults) {
            granted &= grantResult == PackageManager.PERMISSION_GRANTED;
        }
        if (granted) {
            Toast.makeText(this, "You may now place a call", Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(this, "This application needs permission to use your microphone and camera to function properly.", Toast.LENGTH_LONG).show();
        }
        mSinchServiceInterface.retryStartAfterPermissionGranted();
    }




    @Override
    public void callIncomingScreen(CallClient callClient, Call call) {

        System.out.println("callIncomingScreen Base Activity --->"+ call.getHeaders().toString());

        if (call.getDetails().isVideoOffered()) {
            Intent intent = new Intent(BaseActivity.this, IncomingCallScreenActivity.class);
            intent.putExtra(CALL_ID, call.getCallId());
            intent.putExtra("TYPE_CALL", "Video");
            intent.putExtra("profile_path", call.getHeaders().get("profile_path"));
            intent.putExtra("name", call.getHeaders().get("name"));
            intent.putExtra("uniqueChatId", call.getHeaders().get("uniqueChatId"));
            intent.putExtra("senderID", call.getHeaders().get("senderID"));
            intent.putExtra("receiverID", call.getHeaders().get("receiverID"));

            BaseActivity.this.startActivityForResult(intent,
                    VIDEO_CALL_REQUEST_CODE);
        } else {
            Intent intent = new Intent(BaseActivity.this, IncomingCallScreenActivity.class);
            intent.putExtra(CALL_ID, call.getCallId());

            intent.putExtra("profile_path", call.getHeaders().get("profile_path"));
            intent.putExtra("name", call.getHeaders().get("name"));
            intent.putExtra("uniqueChatId", call.getHeaders().get("uniqueChatId"));
            intent.putExtra("senderID", call.getHeaders().get("senderID"));
            intent.putExtra("receiverID", call.getHeaders().get("receiverID"));


            if (call.getHeaders().containsKey("isWeb")) {
                intent.putExtra("TYPE_CALL", "Video");
                BaseActivity.this.startActivityForResult(intent, VIDEO_CALL_REQUEST_CODE);
            } else {
                intent.putExtra("TYPE_CALL", "Audio");
                BaseActivity.this.startActivityForResult(intent, VOICE_CALL_REQUEST_CODE);
            }

            // FireChatActivity.this.startActivityForResult(intent, VOICE_CALL_REQUEST_CODE);
        }
    }
}
