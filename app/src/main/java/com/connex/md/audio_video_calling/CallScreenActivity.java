package com.connex.md.audio_video_calling;

import android.content.Context;
import android.content.Intent;
import android.hardware.Camera;
import android.media.AudioManager;
import android.os.Bundle;
import android.util.Log;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.crashlytics.android.Crashlytics;
import com.connex.md.R;
import com.connex.md.custom_views.CircleImageView;
import com.connex.md.others.PicassoTrustAll;
import com.sinch.android.rtc.AudioController;
import com.sinch.android.rtc.PushPair;
import com.sinch.android.rtc.calling.Call;
import com.sinch.android.rtc.calling.CallEndCause;
import com.sinch.android.rtc.calling.CallState;
import com.sinch.android.rtc.video.VideoCallListener;
import com.sinch.android.rtc.video.VideoController;
import com.skyfishjy.library.RippleBackground;
import com.squareup.picasso.Callback;

import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import static com.connex.md.ws.MyConstants.CALL_ID;

import io.fabric.sdk.android.Fabric;

public class CallScreenActivity extends BaseActivity implements Camera.ErrorCallback {

    static final String TAG = CallScreenActivity.class.getSimpleName();

    static final String CALL_START_TIME = "callStartTime";
    static final String ADDED_LISTENER = "addedListener";
    public ImageView img_swap_cam, img_mic, img_specker;
    public AQuery aqList;

    boolean isMute = false;
    boolean isSpiker = true;
    ImageView ivHangUp;
    private boolean isCallEnded = false;

    /*remove all these*/

    private SurfaceHolder previewHolder = null;
    private Camera camera = null;
    private boolean inPreview = false;
    private boolean cameraConfigured = false;

    SurfaceHolder.Callback surfaceCallback = new SurfaceHolder.Callback() {
        public void surfaceCreated(SurfaceHolder holder) {
            // no-op -- wait until surfaceChanged()
        }

        public void surfaceChanged(SurfaceHolder holder,
                                   int format, int width,
                                   int height) {
            try {
                if (inPreview) {
                    camera.stopPreview();
                }

                android.hardware.Camera.CameraInfo info = new android.hardware.Camera.CameraInfo();
                android.hardware.Camera.getCameraInfo(Camera.CameraInfo.CAMERA_FACING_FRONT, info);
                int rotation = getWindowManager().getDefaultDisplay().getRotation();
                int degrees = 0;
                switch (rotation) {
                    case Surface.ROTATION_0:
                        degrees = 0;
                        break;
                    case Surface.ROTATION_90:
                        degrees = 90;
                        break;
                    case Surface.ROTATION_180:
                        degrees = 180;
                        break;
                    case Surface.ROTATION_270:
                        degrees = 270;
                        break;
                }

                int result;
                if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
                    result = (info.orientation + degrees) % 360;
                    result = (360 - result) % 360; // compensate the mirror

                } else { // back-facing
                    result = (info.orientation - degrees + 360) % 360;
                }

                camera.setDisplayOrientation(result);
                //  camera.setParameters(parameters);

                initPreview(width, height);
                startPreview();


            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        public void surfaceDestroyed(SurfaceHolder holder) {
            // no-op

            try {
                if (camera != null) {
                    camera.stopPreview();
                }
            } catch (Exception e) {
                System.out.println("Camera release failure.");
            }
        }
    };
    private String callEndCause;
    /*-------------*/
    private AudioPlayer mAudioPlayer;
    private Timer mTimer;
    private UpdateCallDurationTask mDurationTask;
    private String mCallId, recvImageUrl, recvName, uniqueChatId, senderID, receiverID;
    ;
    private long mCallStart = 0;
    private boolean mAddedListener = false;
    private boolean mVideoViewsAdded = false;
    private TextView mCallDuration;
    private TextView mCallState;
    private TextView mCallerName;
    private LinearLayout lin_hangupButton;
    private RelativeLayout rlDP,relDialling;
    private CircleImageView ivUser;
    private boolean isDialing = false;

    boolean mToggleVideoViewPositions = false;
    private boolean mLocalVideoViewAdded = false;
    private boolean mRemoteVideoViewAdded = false;


    static final String VIEWS_TOGGLED = "viewsToggled";


    @Override
    protected void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        savedInstanceState.putBoolean(ADDED_LISTENER, mAddedListener);
        savedInstanceState.putBoolean(VIEWS_TOGGLED, mToggleVideoViewPositions);

    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        mAddedListener = savedInstanceState.getBoolean(ADDED_LISTENER);
        mToggleVideoViewPositions = savedInstanceState.getBoolean(VIEWS_TOGGLED);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.callscreen);
        try {
            aqList = new AQuery(this);
            recvImageUrl = getIntent().getStringExtra("recvImageUrl");
            recvName = getIntent().getStringExtra("recvName");
            isDialing = getIntent().getBooleanExtra("isDailing", false);
            uniqueChatId = getIntent().getStringExtra("uniqueChatId");
            senderID = getIntent().getStringExtra("senderID");
            receiverID = getIntent().getStringExtra("receiverID");


            mAudioPlayer = new AudioPlayer(this);
            mCallDuration = findViewById(R.id.callDuration);
            mCallerName = findViewById(R.id.remoteUser);
            mCallerName.setText(recvName);
            mCallState = findViewById(R.id.callState);
            img_mic = findViewById(R.id.img_mic);
            img_specker = findViewById(R.id.img_specker);
            img_swap_cam = findViewById(R.id.img_swap_cam);
            ivHangUp = findViewById(R.id.ivHangUp);
            lin_hangupButton = findViewById(R.id.lin_hangupButton);

            rlDP = findViewById(R.id.rlDP);
            relDialling = findViewById(R.id.relDialling);
            ivUser = findViewById(R.id.img_recv_Profile);

            ImageView endCallButton = findViewById(R.id.hangupButton);

            endCallButton.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    isCallEnded = true;
                    endCall();
                }
            });

            ivHangUp.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    isCallEnded = true;
                    endCall();
                }
            });

            final RippleBackground rippleBackground = findViewById(R.id.content);
            rippleBackground.startRippleAnimation();

            mCallerName.setText(recvName);
            img_mic.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    switchAudio();
                }
            });
            img_specker.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    switchspeker();
                }
            });
            mCallState.setVisibility(View.VISIBLE);
            mCallDuration.setVisibility(View.GONE);
            mCallId = getIntent().getStringExtra(CALL_ID);
            if (savedInstanceState == null) {
                mCallStart = System.currentTimeMillis();
            }


            if (getIntent().getBooleanExtra("isDailing", false)) {
                System.out.println("dialing video...");
                //     startLocalCameraPreview();

                relDialling.setVisibility(View.VISIBLE);

            } else {
                System.out.println("answering...");

                relDialling.setVisibility(View.GONE);

                findViewById(R.id.remoteVideo).setVisibility(View.VISIBLE);
                findViewById(R.id.localVideo).setVisibility(View.VISIBLE);
                // preview.setVisibility(View.GONE);
                lin_hangupButton.setVisibility(View.VISIBLE);
                ivHangUp.setVisibility(View.GONE);
                rlDP.setVisibility(View.GONE);


            }

            Call call = getSinchServiceInterface().getCall(mCallId);
            if (call != null) {
                if (!mAddedListener) {
                    call.addCallListener(new SinchCallListener());
                    mAddedListener = true;
                }
            } else {
                Log.e(TAG, "Started with invalid callId, aborting.");
                finish();
            }

            updateUI();

        } catch (RuntimeException e) {
            e.printStackTrace();
        }
    }


    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    public void onPause() {
        try {
            if (inPreview) {
                camera.stopPreview();
            }

            if (camera != null) {
                camera.release();
                camera = null;
                inPreview = false;
                previewHolder.removeCallback(surfaceCallback);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        super.onPause();
    }

    @Override
    protected void onDestroy() {

        super.onDestroy();
    }

    private Camera.Size getBestPreviewSize(int width, int height,
                                           Camera.Parameters parameters) {
        Camera.Size result = null;

        for (Camera.Size size : parameters.getSupportedPreviewSizes()) {
            if (size.width <= width && size.height <= height) {
                if (result == null) {
                    result = size;
                } else {
                    int resultArea = result.width * result.height;
                    int newArea = size.width * size.height;

                    if (newArea > resultArea) {
                        result = size;
                    }
                }
            }
        }

        return (result);
    }

    private void initPreview(int width, int height) {
        if (camera != null && previewHolder.getSurface() != null) {
            try {
                camera.setPreviewDisplay(previewHolder);
            } catch (Throwable t) {
                Log.e("Preview-surfaceCallback",
                        "Exception in setPreviewDisplay()", t);
                Toast.makeText(CallScreenActivity.this, t.getMessage(), Toast.LENGTH_LONG)
                        .show();
            }

            if (!cameraConfigured) {
                Camera.Parameters parameters = camera.getParameters();
                Camera.Size size = getBestPreviewSize(width, height,
                        parameters);

                if (size != null) {
                    parameters.setPreviewSize(size.width, size.height);
                    camera.setParameters(parameters);
                    cameraConfigured = true;
                }
            }
        }
    }

    private void startPreview() {
        try {
            if (cameraConfigured && camera != null) {
                camera.startPreview();
                inPreview = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onServiceConnected() {
        Call call = getSinchServiceInterface().getCall(mCallId);
        if (call != null) {
            if (!mAddedListener) {
                call.addCallListener(new SinchCallListener());
                mAddedListener = true;
            }
        } else {
            Log.e(TAG, "Started with invalid callId, aborting.");
            finish();
        }

        updateUI();
    }


    private void setVideoViewsVisibility(final boolean localVideoVisibile, final boolean remoteVideoVisible) {
        if (getSinchServiceInterface() == null)
            return;
        if (mRemoteVideoViewAdded == false) {
            addRemoteView();
        }
        if (mLocalVideoViewAdded == false) {
            addVideoViewsLocal();
        }
        final VideoController vc = getSinchServiceInterface().getVideoController();
        if (vc != null) {
            runOnUiThread(() -> {
                vc.getLocalView().setVisibility(localVideoVisibile ? View.VISIBLE : View.GONE);
                vc.getRemoteView().setVisibility(remoteVideoVisible ? View.VISIBLE : View.GONE);
            });
        }
    }


    //method to update video feeds in the UI
    private void updateUI() {
        if (getSinchServiceInterface() == null) {
            Log.d(TAG, "updateUI: Return early");
            return; // early
        }

        Log.d(TAG, "updateUI: ivUser Profile -->" + recvImageUrl);

        Call call = getSinchServiceInterface().getCall(mCallId);
        if (call != null) {
            mCallerName.setText(recvName);
            aqList = new AQuery(this);

            try {
                PicassoTrustAll.getInstance(CallScreenActivity.this)
                        .load(recvImageUrl)
                        .error(R.drawable.avatar)
                        .into(ivUser, new Callback() {
                            @Override
                            public void onSuccess() {
                            }

                            @Override
                            public void onError() {
                            }
                        });
            } catch (Exception e) {
                e.printStackTrace();
            }


            //mCallState.setText(call.getState().toString());
            if (call.getState() == CallState.ESTABLISHED) {

                if (call.getDetails().isVideoOffered()) {
                    if (call.getState() == CallState.ESTABLISHED) {
                        setVideoViewsVisibility(true, true);
                    } else {
                        setVideoViewsVisibility(true, false);
                    }
                }
                try {
                    //    addVideoViews();
                } catch (Exception e) {
                    e.printStackTrace();
                    endCall();
                }
            }
        } else {

            setVideoViewsVisibility(false, false);

        }
    }

    //stop the timer when call is ended
    @Override
    public void onStop() {
        super.onStop();
        try {
            if (mDurationTask != null) {
                mDurationTask.cancel();
            }
            if (mTimer != null) {
                mTimer.cancel();
            }
            removeVideoViews();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    //start the timer for the call duration here
    @Override
    public void onStart() {
        super.onStart();
        mTimer = new Timer();
        mDurationTask = new UpdateCallDurationTask();
        mTimer.schedule(mDurationTask, 0, 500);
        updateUI();
    }

    @Override
    public void onBackPressed() {
        // UserDetail should exit activity by ending call, not by going back.
    }

    //method to end the call
    private void endCall() {
        mAudioPlayer.stopProgressTone();
        Call call = getSinchServiceInterface().getCall(mCallId);
        if (call != null) {
            call.hangup();
        }
        //finish();
    }

    private String formatTimespan(long timespan) {
        long totalSeconds = timespan / 1000;
        long minutes = totalSeconds / 60;
        long seconds = totalSeconds % 60;
        return String.format(Locale.US, "%d:%02d", minutes, seconds);
    }

    //method to update live duration of the call
    private void updateCallDuration() {
        if (mCallStart > 0) {
            mCallDuration.setText(formatTimespan(System.currentTimeMillis() - mCallStart));
        }
    }

    //method which sets up the video feeds from the server to the UI of the activity
    private void addRemoteView() throws RuntimeException {
        try {
            if (mVideoViewsAdded || getSinchServiceInterface() == null) {
                return; //early
            }

            final VideoController vc = getSinchServiceInterface().getVideoController();
            System.out.println("vc::" + vc);
            if (vc != null) {

                try {
                    if (mRemoteVideoViewAdded || getSinchServiceInterface() == null) {
                        return; //early
                    }

                    if (vc != null) {
                        runOnUiThread(() -> {
                            ViewGroup remoteView = getVideoView(false);
                            remoteView.addView(vc.getRemoteView());
                          /*  remoteView.setOnClickListener((View v) -> {
                                removeVideoViews();
                                mToggleVideoViewPositions = !mToggleVideoViewPositions;
                                addRemoteView();
                                addVideoViewsLocal();
                            });*/
                            mRemoteVideoViewAdded = true;
                            vc.setLocalVideoZOrder(!mToggleVideoViewPositions);
                        });
                    }
                    img_swap_cam.setOnClickListener(new OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            vc.toggleCaptureDevicePosition();
                        }
                    });

                    LinearLayout view = findViewById(R.id.remoteVideo);
                    //view.removeAllViews();
                    view.addView(vc.getRemoteView());

                    mVideoViewsAdded = true;
                } catch (Throwable t) {
                    System.out.println("Error:::***" + t);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private ViewGroup getVideoView(boolean localView) {
        if (mToggleVideoViewPositions) {
            localView = !localView;
        }
        return localView ? findViewById(R.id.localVideo) : findViewById(R.id.remoteVideo);
    }

    // display local video
    private void addVideoViewsLocal() throws RuntimeException {
        try {
            if (mVideoViewsAdded || getSinchServiceInterface() == null) {
                return; //early
            }

            final VideoController vc = getSinchServiceInterface().getVideoController();
            System.out.println("vc::" + vc);
            if (vc != null) {

                try {

                    if (mLocalVideoViewAdded || getSinchServiceInterface() == null) {
                        return; //early
                    }

                    if (vc != null) {
                        runOnUiThread(() -> {
                            ViewGroup localView = getVideoView(true);
                            localView.addView(vc.getLocalView());
                         //   localView.setOnClickListener(v -> vc.toggleCaptureDevicePosition());
                            mLocalVideoViewAdded = true;
                            vc.setLocalVideoZOrder(!mToggleVideoViewPositions);
                        });
                    }


                } catch (Throwable t) {
                    System.out.println("Error:::***" + t);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //removes video feeds from the app once the call is terminated
    private void removeVideoViews() {
        try {
            if (getSinchServiceInterface() == null) {
                return; // early
            }

            VideoController vc = getSinchServiceInterface().getVideoController();
            if (vc != null) {
                LinearLayout view = findViewById(R.id.remoteVideo);
                view.removeView(vc.getRemoteView());

                RelativeLayout localView = findViewById(R.id.localVideo);
                localView.removeView(vc.getLocalView());
                mVideoViewsAdded = false;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void switchAudio() {
        if (isMute) {
            img_mic.setImageResource(R.drawable.mic_off);
            BaseActivity.getSinchServiceInterface().getAudioController().mute();
        } else {
            img_mic.setImageResource(R.drawable.mic_on);
            BaseActivity.getSinchServiceInterface().getAudioController().unmute();
        }

        isMute = !isMute;
    }

    public void switchspeker() {
        AudioManager audioManager = ((AudioManager) CallScreenActivity.this.getApplicationContext().getSystemService(Context.AUDIO_SERVICE));
        if (audioManager != null) {
            audioManager.setMode(AudioManager.MODE_IN_CALL);
            if (isSpiker) {
                audioManager.setSpeakerphoneOn(false);

            } else {
                audioManager.setSpeakerphoneOn(true);
            }

            audioManager.setMode(AudioManager.MODE_NORMAL);
        }

        if (isSpiker) {
            img_specker.setImageResource(R.drawable.speaker_off);
            //BaseActivity.getSinchServiceInterface().getAudioController().disableSpeaker();
        } else {
            img_specker.setImageResource(R.drawable.speaker_on);
            //BaseActivity.getSinchServiceInterface().getAudioController().enableSpeaker();

        }

        isSpiker = !isSpiker;
    }

    @Override
    public void onError(int error, Camera camera) {
        System.out.println("error::::" + error);
        System.out.println("camera::" + camera);
    }

    private void returnData(CallEndCause cause) {
        Intent resultIntent = new Intent();
        resultIntent.putExtra("IsAudio", "0");
        resultIntent.putExtra("CallEndCause", cause.toString());
        resultIntent.putExtra("uniqueChatId", uniqueChatId);
        resultIntent.putExtra("senderID", senderID);
        resultIntent.putExtra("receiverID", receiverID);

        setResult(RESULT_OK, resultIntent);
    }

    private class UpdateCallDurationTask extends TimerTask {

        @Override
        public void run() {
            CallScreenActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    updateCallDuration();
                }
            });
        }
    }

    public class SinchCallListener implements VideoCallListener {

        @Override
        public void onCallEnded(Call call) {
            CallEndCause cause = call.getDetails().getEndCause();
            Log.d(TAG, "Call ended. Reason: interdace " + cause.toString());

            System.out.println("is call ended-------->" + isCallEnded);
            if (isCallEnded || (cause.toString().equalsIgnoreCase("TIMEOUT") && isDialing)
                    || (cause.toString().equalsIgnoreCase("NO_ANSWER") && isDialing)
                    || (cause.toString().equalsIgnoreCase("DENIED") && isDialing)) {
                returnData(cause);
            }

            mAudioPlayer.stopProgressTone();
            setVolumeControlStream(AudioManager.USE_DEFAULT_STREAM_TYPE);
            String endMsg = "Call ended: " + call.getDetails().toString();

            endCall();
            finish();
        }

        @Override
        public void onCallEstablished(Call call) {
            Log.d(TAG, "Call established");

            try {
                if (inPreview) {
                    camera.stopPreview();
                }

                if (camera != null) {
                    //camera.release();
                    camera = null;
                    inPreview = false;
                    previewHolder.removeCallback(surfaceCallback);

                }

                lin_hangupButton.setVisibility(View.VISIBLE);
                ivHangUp.setVisibility(View.GONE);
                rlDP.setVisibility(View.GONE);

                mAudioPlayer.stopProgressTone();
                mTimer = new Timer();
                mDurationTask = new UpdateCallDurationTask();
                mTimer.schedule(mDurationTask, 0, 500);
                mCallState.setText(call.getState().toString());
                mCallState.setVisibility(View.GONE);
                mCallDuration.setVisibility(View.VISIBLE);
                setVolumeControlStream(AudioManager.STREAM_VOICE_CALL);
                AudioController audioController = getSinchServiceInterface().getAudioController();
                audioController.enableSpeaker();
                mCallStart = System.currentTimeMillis();

                if (call.getDetails().isVideoOffered()) {
                    relDialling.setVisibility(View.GONE);
                    setVideoViewsVisibility(true, true);
                }

                Log.d(TAG, "Call offered video: " + call.getDetails().isVideoOffered());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onCallProgressing(Call call) {
            Log.d(TAG, "Call progressing");
            mAudioPlayer.playProgressTone();
        }

        @Override
        public void onShouldSendPushNotification(Call call, List<PushPair> pushPairs) {
            // Send a push through your push provider here, e.g. GCM
        }

        @Override
        public void onVideoTrackAdded(Call call) {


            Log.d(TAG, "Video track added");

            try {
                if (inPreview) {
                    camera.stopPreview();
                }

                if (camera != null) {

                    //camera.release();
                    camera = null;
                    inPreview = false;
                    previewHolder.removeCallback(surfaceCallback);

                }


                /*findViewById(R.id.remoteVideo).setVisibility(View.VISIBLE);
                findViewById(R.id.localVideo).setVisibility(View.VISIBLE);
                findViewById(R.id.surfaceLocal).setVisibility(View.GONE);*/
                lin_hangupButton.setVisibility(View.VISIBLE);
                ivHangUp.setVisibility(View.GONE);
                rlDP.setVisibility(View.GONE);

                if (call.getDetails().isVideoOffered()) {
                    setVideoViewsVisibility(true, true);
                }
            } catch (Exception e) {
                e.printStackTrace();
                endCall();
            }
        }

        @Override
        public void onVideoTrackPaused(Call call) {

        }

        @Override
        public void onVideoTrackResumed(Call call) {

        }
    }
}
