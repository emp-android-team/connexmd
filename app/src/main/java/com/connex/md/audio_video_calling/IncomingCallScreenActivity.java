package com.connex.md.audio_video_calling;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CaptureRequest;
import android.media.ImageReader;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.support.annotation.NonNull;
import android.util.Log;
import android.util.Size;
import android.util.SparseIntArray;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.TextureView;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.connex.md.R;
import com.connex.md.custom_views.CircleImageView;
import com.connex.md.others.PicassoTrustAll;
import com.connex.md.ws.Utils;
import com.sinch.android.rtc.PushPair;
import com.sinch.android.rtc.calling.Call;
import com.sinch.android.rtc.calling.CallEndCause;
import com.sinch.android.rtc.calling.CallListener;
import com.sinch.android.rtc.video.VideoCallListener;
import com.skyfishjy.library.RippleBackground;
import com.squareup.picasso.Callback;

import java.io.File;
import java.util.List;

import static com.connex.md.ws.MyConstants.CALL_ID;

public class IncomingCallScreenActivity extends BaseActivity {

    static final String TAG = IncomingCallScreenActivity.class.getSimpleName();
    private static final SparseIntArray ORIENTATIONS = new SparseIntArray();
    private static final int REQUEST_CAMERA_PERMISSION = 200;

    static {
        ORIENTATIONS.append(Surface.ROTATION_0, 90);
        ORIENTATIONS.append(Surface.ROTATION_90, 0);
        ORIENTATIONS.append(Surface.ROTATION_180, 270);
        ORIENTATIONS.append(Surface.ROTATION_270, 180);
    }

    public CircleImageView img_recv_Profile;
    public TextView remoteUser;
    public AQuery aqList;
    public AQuery aq;
    protected CameraDevice cameraDevice;
    protected CameraCaptureSession cameraCaptureSessions;
    protected CaptureRequest captureRequest;
    protected CaptureRequest.Builder captureRequestBuilder;
    SeekBar simpleSeekBar;
    boolean visible;
    ImageView call_ans, call_end;
    String TYPE_CALL = "";
    private String mCallId, recvImageUrl, recvName, uniqueChatId, senderID, receiverID;
    private AudioPlayer mAudioPlayer;

    private boolean mVideoViewsAdded = false;
    private SurfaceView preview = null;
    private SurfaceHolder previewHolder = null;
    private Camera camera = null;
    private boolean inPreview = false;
    private boolean isCallEnded = false;
    private boolean cameraConfigured = false;

    SurfaceHolder.Callback surfaceCallback = new SurfaceHolder.Callback() {
        public void surfaceCreated(SurfaceHolder holder) {
            // no-op -- wait until surfaceChanged()
        }

        public void surfaceChanged(SurfaceHolder holder,
                                   int format, int width,
                                   int height) {
            try {
                if (inPreview) {
                    camera.stopPreview();
                }

                android.hardware.Camera.CameraInfo info = new android.hardware.Camera.CameraInfo();
                android.hardware.Camera.getCameraInfo(Camera.CameraInfo.CAMERA_FACING_FRONT, info);
                int rotation = getWindowManager().getDefaultDisplay().getRotation();
                int degrees = 0;
                switch (rotation) {
                    case Surface.ROTATION_0:
                        degrees = 0;
                        break;
                    case Surface.ROTATION_90:
                        degrees = 90;
                        break;
                    case Surface.ROTATION_180:
                        degrees = 180;
                        break;
                    case Surface.ROTATION_270:
                        degrees = 270;
                        break;
                }

                int result;
                //int currentapiVersion = android.os.Build.VERSION.SDK_INT;
                // do something for phones running an SDK before lollipop
                if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
                    result = (info.orientation + degrees) % 360;
                    result = (360 - result) % 360; // compensate the mirror
                } else { // back-facing
                    result = (info.orientation - degrees + 360) % 360;
                }

                camera.setDisplayOrientation(result);
                //  camera.setParameters(parameters);

                initPreview(width, height);
                startPreview();

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        public void surfaceDestroyed(SurfaceHolder holder) {
            // no-op
        }
    };

    private LinearLayout llAccept, llDecline;

    private boolean isAccept = false;
    private OnClickListener mClickListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.llAccept:
                    isAccept = true;
                    if (Utils.checkAudioPermission(IncomingCallScreenActivity.this)) {
                        answerClicked();
                    }
                    break;
                case R.id.llDecline:
                    isAccept = false;
                    if (Utils.checkAudioPermission(IncomingCallScreenActivity.this)) {
                        declineClicked();
                    }
                    break;
            }
        }
    };

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        //Checking the request code of our request
        if (requestCode == Utils.RECORD_AUDDIO) {
            //If permission is granted
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                if (isAccept) {
                    answerClicked();
                } else {
                    declineClicked();
                }
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.incoming);

        recvImageUrl = getIntent().getStringExtra("profile_path");
        recvName = getIntent().getStringExtra("name");
        uniqueChatId = getIntent().getStringExtra("uniqueChatId");
        senderID = getIntent().getStringExtra("senderID");
        receiverID = getIntent().getStringExtra("receiverID");

        Log.i("TAG", "recvName ::->" + recvName);
        Log.i("TAG", "recvImageUrl _path ::->" + recvImageUrl);
        aqList = new AQuery(this);

        TYPE_CALL = getIntent().getStringExtra("TYPE_CALL");

        if (TYPE_CALL.equalsIgnoreCase("Audio")) {
            findViewById(R.id.surfaceLocal).setVisibility(View.GONE);
        } else {
            findViewById(R.id.surfaceLocal).setVisibility(View.GONE);
            //startLocalCameraPreview();
        }

        llAccept = findViewById(R.id.llAccept);
        llDecline = findViewById(R.id.llDecline);

        //TextView answer = (TextView) findViewById(R.id.tv_ans);

        llAccept.setOnClickListener(mClickListener);
        //TextView decline = (TextView) findViewById(R.id.tv_decline);
        llDecline.setOnClickListener(mClickListener);

        final RippleBackground rippleBackground = findViewById(R.id.content);
        rippleBackground.startRippleAnimation();

        mAudioPlayer = new AudioPlayer(this);

        mAudioPlayer.playRingtone();
        mCallId = getIntent().getStringExtra(CALL_ID);


        img_recv_Profile = findViewById(R.id.img_recv_Profile);
        remoteUser = findViewById(R.id.remoteUser);

        updateUI();
        //img_recv_Profile.startAnimation(AnimationUtils.loadAnimation(IncomingCallScreenActivity.this, R.anim.shake));
    }


    private void initPreview(int width, int height) {
        System.out.println("Call Test :  initPreview");
        if (camera != null && previewHolder.getSurface() != null) {
            try {
                camera.setPreviewDisplay(previewHolder);
            } catch (Throwable t) {
                Log.e("Preview-surfaceCallback",
                        "Exception in setPreviewDisplay()", t);
                Toast.makeText(IncomingCallScreenActivity.this, t.getMessage(), Toast.LENGTH_LONG)
                        .show();
            }

            if (!cameraConfigured) {
                Camera.Parameters parameters = camera.getParameters();
                Camera.Size size = getBestPreviewSize(width, height,
                        parameters);

                if (size != null) {
                    parameters.setPreviewSize(size.width, size.height);
                    camera.setParameters(parameters);
                    cameraConfigured = true;
                }
            }
        }
    }

    private void startPreview() {
        try {
            if (cameraConfigured && camera != null) {
                camera.startPreview();
                //camera.release();
                inPreview = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private Camera.Size getBestPreviewSize(int width, int height, Camera.Parameters parameters) {
        Camera.Size result = null;

        for (Camera.Size size : parameters.getSupportedPreviewSizes()) {
            if (size.width <= width && size.height <= height) {
                if (result == null) {
                    result = size;
                } else {
                    int resultArea = result.width * result.height;
                    int newArea = size.width * size.height;

                    if (newArea > resultArea) {
                        result = size;
                    }
                }
            }
        }

        return (result);
    }


    @Override
    protected void onServiceConnected() {
        updateUI();

    }

    public void updateUI() {
        System.out.println("Call Test :  Sinch Service conncted");
        Call call = getSinchServiceInterface().getCall(mCallId);
        if (call != null) {

            if (TYPE_CALL.equalsIgnoreCase("Audio")) {
                call.addCallListener(new SinchAudioCallListener());
            } else {
                call.addCallListener(new SinchCallListener());
            }
            remoteUser = findViewById(R.id.remoteUser);
            remoteUser.setText(recvName);

            try {
                PicassoTrustAll.getInstance(IncomingCallScreenActivity.this)
                        .load(recvImageUrl)
                        .error(R.drawable.avatar)
                        .into(img_recv_Profile, new Callback() {
                            @Override
                            public void onSuccess() {
                            }

                            @Override
                            public void onError() {
                            }
                        });
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {
            Log.e(TAG, "Started with invalid callId, aborting");
            finish();
        }
    }

    private void answerClicked() {
        mAudioPlayer.stopRingtone();
        Call call = getSinchServiceInterface().getCall(mCallId);

        String recvImageUrl = this.recvImageUrl;
        String recvName = this.recvName;
        if (call != null) {
            call.answer();
            if (TYPE_CALL.equalsIgnoreCase("Audio")) {
                Intent intent = new Intent(this, AudioCallActivity.class);
                intent.putExtra("recvImageUrl", recvImageUrl);
                intent.putExtra("recvName", recvName);
                intent.putExtra("uniqueChatId", uniqueChatId);
                intent.putExtra("senderID", senderID);
                intent.putExtra("receiverID", receiverID);
                intent.putExtra(CALL_ID, mCallId);
                intent.addFlags(Intent.FLAG_ACTIVITY_FORWARD_RESULT);
                startActivity(intent);
            } else {
                //stopLocalCameraPreview();

                Intent intent = new Intent(this, CallScreenActivity.class);
                intent.putExtra("recvImageUrl", recvImageUrl);
                intent.putExtra("recvName", recvName);
                intent.putExtra("isDailing", false);
                intent.putExtra("uniqueChatId", uniqueChatId);
                intent.putExtra("senderID", senderID);
                intent.putExtra("receiverID", receiverID);

                intent.putExtra(CALL_ID, mCallId);
                intent.addFlags(Intent.FLAG_ACTIVITY_FORWARD_RESULT);
                startActivity(intent);

            }
        } else {
            finish();
        }
    }

    public void stopLocalCameraPreview() {
        try {
            if (inPreview) {
                camera.stopPreview();
            }

            if (camera != null) {
                camera.release();
                camera = null;
                inPreview = false;
                previewHolder.removeCallback(surfaceCallback);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void declineClicked() {
        mAudioPlayer.stopRingtone();
        Call call = getSinchServiceInterface().getCall(mCallId);
        if (call != null) {
            call.hangup();
        }
        isCallEnded = true;
        //stopLocalCameraPreview();
        finish();

    }

    private void returnData(CallEndCause cause) {
        System.out.println("return data...................");
        Intent resultIntent = new Intent();
        resultIntent.putExtra("IsAudio", "1");
        resultIntent.putExtra("CallEndCause", cause.toString());
        resultIntent.putExtra("uniqueChatId", uniqueChatId);
        resultIntent.putExtra("senderID", senderID);
        resultIntent.putExtra("receiverID", receiverID);

        setResult(RESULT_OK, resultIntent);
        finish();
    }

    private void returnDataVideo(CallEndCause cause) {
        Intent resultIntent = new Intent();
        resultIntent.putExtra("IsAudio", "0");
        resultIntent.putExtra("CallEndCause", cause.toString());
        resultIntent.putExtra("uniqueChatId", uniqueChatId);
        resultIntent.putExtra("senderID", senderID);
        resultIntent.putExtra("receiverID", receiverID);

        setResult(RESULT_OK, resultIntent);
        finish();
    }

    public class SinchCallListener implements VideoCallListener {

        @Override
        public void onCallEnded(Call call) {
            /*CallEndCause cause = call.getDetails().getEndCause();
            Log.d(TAG, "Call ended, cause: " + cause.toString());*/
            CallEndCause cause = call.getDetails().getEndCause();
            Log.d(TAG, "Call ended. Reason: in interface:::" + cause.toString());
            mAudioPlayer.stopRingtone();
            System.out.println("is call ended in incoming video-------->" + isCallEnded);
            if (isCallEnded || cause.toString().equalsIgnoreCase("TIMEOUT")
                    || cause.toString().equalsIgnoreCase("NO_ANSWER")
                    || (cause.toString().equalsIgnoreCase("DENIED"))) {
                returnDataVideo(cause);
            } else {
                finish();
            }

            // remove here
            //  finish();
        }

        @Override
        public void onCallEstablished(Call call) {
            Log.d(TAG, "Call established");
        }

        @Override
        public void onCallProgressing(Call call) {
            Log.d(TAG, "Call progressing");
        }

        @Override
        public void onShouldSendPushNotification(Call call, List<PushPair> pushPairs) {
            // Send a push through your push provider here, e.g. GCM
        }

        @Override
        public void onVideoTrackAdded(Call call) {
            // Display some kind of icon showing it's a video call
        }

        @Override
        public void onVideoTrackPaused(Call call) {

        }

        @Override
        public void onVideoTrackResumed(Call call) {

        }

    }

    public class SinchAudioCallListener implements CallListener {

        @Override
        public void onCallEnded(Call call) {
            /*CallEndCause cause = call.getDetails().getEndCause();
            Log.d(TAG, "Call ended, cause: " + cause.toString());*/
            CallEndCause cause = call.getDetails().getEndCause();
            Log.d(TAG, "Call ended. Reason: in interface:::" + cause.toString());
            mAudioPlayer.stopRingtone();
            System.out.println("is call ended in incoming audio-------->" + isCallEnded);
            if (isCallEnded || cause.toString().equalsIgnoreCase("TIMEOUT")
                    || cause.toString().equalsIgnoreCase("NO_ANSWER") ||
                    (cause.toString().equalsIgnoreCase("DENIED"))) {
                returnData(cause);
            } else {
                finish();
            }

        }

        @Override
        public void onCallEstablished(Call call) {
            Log.d(TAG, "Call established");
        }

        @Override
        public void onCallProgressing(Call call) {
            Log.d(TAG, "Call progressing");
        }

        @Override
        public void onShouldSendPushNotification(Call call, List<PushPair> pushPairs) {
            // Send a push through your push provider here, e.g. GCM
        }

    }

}
