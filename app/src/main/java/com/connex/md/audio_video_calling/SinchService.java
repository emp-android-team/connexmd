package com.connex.md.audio_video_calling;

import android.Manifest;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.util.Log;
import android.widget.Toast;

import com.connex.md.others.App;
import com.connex.md.ws.MyConstants;
import com.sinch.android.rtc.AudioController;
import com.sinch.android.rtc.Beta;
import com.sinch.android.rtc.ClientRegistration;
import com.sinch.android.rtc.ManagedPush;
import com.sinch.android.rtc.MissingPermissionException;
import com.sinch.android.rtc.NotificationResult;
import com.sinch.android.rtc.PushTokenRegistrationCallback;
import com.sinch.android.rtc.Sinch;

import com.sinch.android.rtc.SinchClient;
import com.sinch.android.rtc.SinchClientListener;
import com.sinch.android.rtc.SinchError;
import com.sinch.android.rtc.calling.Call;
import com.sinch.android.rtc.calling.CallClient;
import com.sinch.android.rtc.calling.CallClientListener;
import com.sinch.android.rtc.video.VideoController;
import com.sinch.android.rtc.video.VideoScalingType;

import java.util.Map;

public class SinchService extends Service {/*
    _client = [Sinch clientWithApplicationKey:@"aeb371b1-0f24-4211-a123-e62c61ff5204"
    applicationSecret:@"8dYa5KtV10+/sH+VwjrXzA=="
    environmentHost:@"sandbox.sinch.com"
    userId:userId];*/


    static final String TAG = SinchService.class.getSimpleName();

    private SinchServiceInterface mSinchServiceInterface = new SinchServiceInterface();
    private SinchClient mSinchClient;
    private String mUserId;

    private StartFailedListener mListener;
    private IncomingCallLisnter incomingListner;
    private Messenger messenger;

    public static final int MESSAGE_PERMISSIONS_NEEDED = 1;
    public static final String REQUIRED_PERMISSION = "REQUIRED_PESMISSION";
    public static final String MESSENGER = "MESSENGER";
    public SharedPreferences mSharedPref;

    @Override
    public void onCreate() {
        super.onCreate();
    }


    private void attemptAutoStart() {

        start("");

    }

    @Override
    public void onDestroy() {
        if (mSinchClient != null && mSinchClient.isStarted()) {
            mSinchClient.terminateGracefully();
        }
        super.onDestroy();
    }

    private void start(String userName) {
        boolean permissionsGranted = true;
        if (mSinchClient == null) {

            mUserId = userName;
            createClient();
        }
        try {
            //mandatory checks
            mSinchClient.checkManifest();
            //auxiliary check
            if (getApplicationContext().checkCallingOrSelfPermission(android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                throw new MissingPermissionException(Manifest.permission.CAMERA);
            }
        } catch (MissingPermissionException e) {
            permissionsGranted = false;
            if (messenger != null) {
                Message message = Message.obtain();
                Bundle bundle = new Bundle();
                bundle.putString(REQUIRED_PERMISSION, e.getRequiredPermission());
                message.setData(bundle);
                message.what = MESSAGE_PERMISSIONS_NEEDED;
                try {
                    messenger.send(message);
                } catch (RemoteException e1) {
                    e1.printStackTrace();
                }
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        try {
            if (permissionsGranted) {
                Log.d(TAG, "Starting SinchClient");
                if (!mSinchClient.isStarted())
                    mSinchClient.start();
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }


    private void createClient() {

        //System.loadLibrary("sinch-android-rtc-3.17.0");
        try {
            mSharedPref = getSharedPreferences(MyConstants.PREF, Context.MODE_PRIVATE);
            mUserId = mSharedPref.getString(MyConstants.SINCH_ID, "");
            mSinchClient = Sinch.getSinchClientBuilder().context(getApplicationContext()).userId(mUserId).enableVideoCalls(true)
                    .applicationKey(MyConstants.APP_KEY)
                    .applicationSecret(MyConstants.APP_SECRET)

                    .environmentHost(MyConstants.ENVIRONMENT).build();

            mSinchClient.setSupportCalling(true);
            mSinchClient.setSupportManagedPush(true);
            mSinchClient.setSupportActiveConnectionInBackground(true);
            mSinchClient.getVideoController().setResizeBehaviour(VideoScalingType.ASPECT_FILL);

            mSinchClient.startListeningOnActiveConnection();


            mSinchClient.addSinchClientListener(new MySinchClientListener());
            mSinchClient.getCallClient().addCallClientListener(new SinchCallClientListener());


            String firstName = mSharedPref.getString(MyConstants.PT_FIRST_NAME, "");
            String lastName = mSharedPref.getString(MyConstants.PT_LAST_NAME, "");


            if (!mSharedPref.getString(MyConstants.USER_TYPE, "").equalsIgnoreCase(MyConstants.USER_PT)) {
                mSinchClient.setPushNotificationDisplayName("Dr. " + firstName + " " + lastName);
            } else {
                mSinchClient.setPushNotificationDisplayName(firstName + " " + lastName);
            }

            mSinchClient.start();
        } catch (UnsatisfiedLinkError e) {
            e.printStackTrace();
        }
    }

    private void stop() {
        if (mSinchClient != null) {
            mSinchClient.terminateGracefully();

        }
    }

    private boolean isStarted() {
        return (mSinchClient != null && mSinchClient.isStarted());
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mSinchServiceInterface;
    }

    public interface StartFailedListener {

        void onStartFailed(SinchError error);

        void onStarted();

    }

    public interface IncomingCallLisnter {
        void callIncomingScreen(CallClient callClient, Call call);
    }


    private void createClientIfNecessary() {
        if (mSinchClient != null)
            return;

        if (mUserId.equals("")) {
            throw new IllegalStateException("Can't create a SinchClient as no username is available!");
        }
        start("");
    }


    public class SinchServiceInterface extends Binder {


        public Call callUser(String userId, Map<String, String> headers) {
            return mSinchClient.getCallClient().callUser(userId, headers);

        }


        public Call callUserVideo(String userId, Map<String, String> map) {
            return mSinchClient.getCallClient().callUserVideo(userId, map);
        }

        public String getUserName() {
            return mUserId;
        }

        public boolean isStarted() {
            return SinchService.this.isStarted();
        }

        public void startClient(String userName) {

            mSharedPref = getSharedPreferences(MyConstants.PREF, Context.MODE_PRIVATE);
            mUserId = mSharedPref.getString(MyConstants.SINCH_ID, "");
            start(mUserId);
        }

        public void stopClient() {
            try {
                stop();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        public void setStartListener(StartFailedListener listener) {
            mListener = listener;
        }

        public void setIncomingLisnter(IncomingCallLisnter listener) {
            incomingListner = listener;
        }

        public Call getCall(String callId) {
            return mSinchClient.getCallClient().getCall(callId);
        }

        public VideoController getVideoController() {
            if (!isStarted()) {
                return null;
            }
            return mSinchClient.getVideoController();
        }

        public AudioController getAudioController() {
            if (!isStarted()) {
                return null;
            }
            return mSinchClient.getAudioController();
        }


        public void retryStartAfterPermissionGranted() {
            SinchService.this.attemptAutoStart();
        }


        public NotificationResult relayRemotePushNotificationPayload(final Map payload) {


            mSharedPref = getSharedPreferences(MyConstants.PREF, Context.MODE_PRIVATE);
            mUserId = mSharedPref.getString(MyConstants.SINCH_ID, "");
            if (mUserId.isEmpty()) {
                Log.e(TAG, "Unable to relay the push notification!");
                return null;
            }
            createClientIfNecessary();
            return mSinchClient.relayRemotePushNotificationPayload(payload);
        }


        public void registerPushToken(PushTokenRegistrationCallback callback) {
            try {
                getManagedPush().registerPushToken(callback);
            } catch (NullPointerException e){
                e.printStackTrace();
            }
        }


        public void unregisterPushToken() {
            getManagedPush().unregisterPushToken();
        }


        public ManagedPush getManagedPush() {
            if (mSinchClient != null) {
                return Beta.createManagedPush(mSinchClient);
            }

            createClientIfNecessary();
            return Beta.createManagedPush(mSinchClient);
        }
    }

    private class MySinchClientListener implements SinchClientListener {
        @Override
        public void onClientFailed(SinchClient client, SinchError error) {
            Log.i("Sinch client", "Error :" + error.getMessage());
            if (mListener != null) {
                mListener.onStartFailed(error);

            }
            mSinchClient.stopListeningOnActiveConnection();
            mSinchClient.terminate();
            mSinchClient = null;
        }

        @Override
        public void onClientStarted(SinchClient client) {
            Log.d(TAG, "SinchClient started");
            Log.i("Sinch client", "Sinch client strted");

            if (mListener != null) {
                mListener.onStarted();
            }
        }

        @Override
        public void onClientStopped(SinchClient client) {
            Log.d(TAG, "SinchClient stopped");
        }

        @Override
        public void onLogMessage(int level, String area, String message) {
            switch (level) {
                case Log.DEBUG:
                    Log.d(area, message);
                    break;
                case Log.ERROR:
                    Log.e(area, message);
                    break;
                case Log.INFO:
                    Log.i(area, message);
                    break;
                case Log.VERBOSE:
                    Log.v(area, message);
                    break;
                case Log.WARN:
                    Log.w(area, message);
                    break;
            }
        }

        @Override
        public void onRegistrationCredentialsRequired(SinchClient client, ClientRegistration clientRegistration) {

        }
    }

    private class SinchCallClientListener implements CallClientListener {
        @Override
        public void onIncomingCall(CallClient callClient, Call call) {
            System.out.println("callIncomingScreen Sinch Service --->" + call.getHeaders().toString());

            if (incomingListner != null) {
                incomingListner.callIncomingScreen(callClient, call);

            } else {
                System.out.println("DP here");
            }
        }
    }
}
