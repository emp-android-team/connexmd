package com.connex.md.doctor.activity;

import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;

import com.connex.md.R;
import com.connex.md.doctor.fragment.AboutFragment;
import com.connex.md.doctor.fragment.EducationFragment;
import com.connex.md.doctor.fragment.HospitalsFragment;
import com.connex.md.doctor.fragment.MembershipFragment;
import com.connex.md.doctor.fragment.ProfessionFragment;
import com.connex.md.doctor.fragment.PublicationsFragment;
import com.connex.md.model.DoctorProfile;
import com.connex.md.ws.MyConstants;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DREditProfileActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    private TabLayout tabLayout;
    private ViewPager viewPager;
    private DoctorProfile doctorProfile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dr_edit_profile);
        ButterKnife.bind(this);

        setupToolbar();

        Bundle b = getIntent().getExtras();
        if (b != null) {
            doctorProfile = (DoctorProfile) b.getSerializable("doctor_profile");
            MyConstants.editDoctorProfile = doctorProfile;
        }

        viewPager = findViewById(R.id.viewpager);
        tabLayout = findViewById(R.id.tabs);

        setupViewPager(viewPager);
        tabLayout.setupWithViewPager(viewPager);
    }


    private void setupToolbar() {
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle("Edit Profile");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new AboutFragment(), "About");
        //adapter.addFragment(new ScopeFragment(), "Scope");

        //if (doctorProfile.getEducation().size() > 0) {
            adapter.addFragment(new EducationFragment(), "Education");
        //}
        //if (doctorProfile.getProfessional_rotation().size() > 0) {
            adapter.addFragment(new ProfessionFragment(), "Profession");
        //}
        //if (doctorProfile.getMembership().size() > 0) {
            adapter.addFragment(new MembershipFragment(), "Membership");
        //}
        //if (doctorProfile.getHospital_affiliation().size() > 0) {
            adapter.addFragment(new HospitalsFragment(), "Hospitals");
        //}
        //if (doctorProfile.getPublications().size() > 0) {
            adapter.addFragment(new PublicationsFragment(), "Publications");
        //}
            viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public int getItemPosition(Object object) {
            // POSITION_NONE makes it possible to reload the PagerAdapter
            return POSITION_NONE;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        MyConstants.isBackPressed = true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
