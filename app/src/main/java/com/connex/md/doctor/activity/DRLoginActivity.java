package com.connex.md.doctor.activity;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.KeyguardManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Build;
import android.os.Bundle;
import android.security.keystore.KeyGenParameterSpec;
import android.security.keystore.KeyPermanentlyInvalidatedException;
import android.security.keystore.KeyProperties;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.connex.md.R;
import com.connex.md.activity.ResetPasswordActivity;
import com.connex.md.activity.TermsConditionActivity;
import com.connex.md.interfaces.AsyncTaskListner;
import com.connex.md.others.App;
import com.connex.md.others.Internet;
import com.connex.md.patient.activity.PTDashboardActivity;
import com.connex.md.utils.FingerprintHandler;
import com.connex.md.ws.CallRequest;
import com.connex.md.ws.Constant;
import com.connex.md.ws.MyConstants;
import com.connex.md.ws.Utils;
import com.sinch.android.rtc.PushTokenRegistrationCallback;
import com.sinch.android.rtc.SinchError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;

import static com.connex.md.audio_video_calling.BaseActivity.getSinchServiceInterface;

public class DRLoginActivity extends AppCompatActivity implements AsyncTaskListner, PushTokenRegistrationCallback {

    EditText etEmail, etPassword;
    Button btnLogin;
    TextView tvForgotPassword, tvTerms;
    CheckBox cbRememberMe;
    DRLoginActivity instance;
    String name;
    SharedPreferences sharedpreferences;
    SharedPreferences.Editor editor;
    public static Activity mActivity;
    private boolean isRememberMe = false;
    private KeyStore keyStore;
    // Variable used for storing the key in the Android Keystore container
    private static final String KEY_NAME = "DoctorPocket";
    private Cipher cipher;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drlogin);

        instance = this;
        mActivity = this;

        etEmail = findViewById(R.id.etEmail);
        etPassword = findViewById(R.id.etPassword);
        btnLogin = findViewById(R.id.btnLogin);
        tvForgotPassword = findViewById(R.id.tvForgotPassword);
        tvTerms = findViewById(R.id.tvTerms);
        cbRememberMe = findViewById(R.id.cbRememberMe);

        sharedpreferences = getSharedPreferences(MyConstants.PREF, MODE_PRIVATE);

        if (sharedpreferences.getString(MyConstants.USER_TYPE, "").equalsIgnoreCase(MyConstants.USER_DR)) {
            if (!TextUtils.isEmpty(sharedpreferences.getString(MyConstants.DR_PASSWORD, ""))) {
                if (sharedpreferences.getBoolean(MyConstants.FIRST_LOGIN, false)) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        System.out.println("fingerprint");
                        fingerprintLogin();
                    }
                }
            }
        }

        if (sharedpreferences.getString(MyConstants.USER_TYPE,"").equalsIgnoreCase(MyConstants.USER_DR)) {
            if (!TextUtils.isEmpty(sharedpreferences.getString(MyConstants.DR_PASSWORD, ""))) {
                etEmail.setText(sharedpreferences.getString(MyConstants.USER_EMAIL, ""));
                etPassword.setText(sharedpreferences.getString(MyConstants.DR_PASSWORD, ""));
                cbRememberMe.setChecked(true);
            }
        }

        tvForgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(DRLoginActivity.this, ResetPasswordActivity.class);
                intent.putExtra("user_type","doctor");
                startActivity(intent);
            }
        });

        tvTerms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(DRLoginActivity.this, TermsConditionActivity.class);
                startActivity(intent);
            }
        });

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                doLogin();
            }
        });
    }

    private void fingerprintLogin() {
        // Initializing both Android Keyguard Manager and Fingerprint Manager
        KeyguardManager keyguardManager = (KeyguardManager) getSystemService(KEYGUARD_SERVICE);
        FingerprintManager fingerprintManager = (FingerprintManager) getSystemService(FINGERPRINT_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (fingerprintManager != null) {
                if (!fingerprintManager.isHardwareDetected()) {
                    /**
                     * An error message will be displayed if the device does not contain the fingerprint hardware.
                     * However if you plan to implement a default authentication method,
                     * you can redirect the user to a default authentication activity from here.
                     * Example:
                     * Intent intent = new Intent(this, DefaultAuthenticationActivity.class);
                     * startActivity(intent);
                     */
                    Toast.makeText(instance, "Your Device does not have a Fingerprint Sensor", Toast.LENGTH_SHORT).show();
                    //textView.setText("Your Device does not have a Fingerprint Sensor");
                } else {
                    // Checks whether fingerprint permission is set on manifest
                    if (ActivityCompat.checkSelfPermission(instance, Manifest.permission.USE_FINGERPRINT) != PackageManager.PERMISSION_GRANTED) {
                        Toast.makeText(instance, "Fingerprint authentication permission not enabled", Toast.LENGTH_SHORT).show();
                        //textView.setText("Fingerprint authentication permission not enabled");
                    } else {
                        // Check whether at least one fingerprint is registered
                        if (!fingerprintManager.hasEnrolledFingerprints()) {
                            Toast.makeText(instance, "Register at least one fingerprint in Settings", Toast.LENGTH_SHORT).show();
                            //textView.setText("Register at least one fingerprint in Settings");
                        } else {
                            // Checks whether lock screen security is enabled or not
                            if (!keyguardManager.isKeyguardSecure()) {
                                Toast.makeText(instance, "Lock screen security not enabled in Settings", Toast.LENGTH_SHORT).show();
                                //textView.setText("Lock screen security not enabled in Settings");
                            } else {
                                generateKey();

                                if (cipherInit()) {
                                    FingerprintManager.CryptoObject cryptoObject = new FingerprintManager.CryptoObject(cipher);
                                    FingerprintHandler helper = new FingerprintHandler(DRLoginActivity.this, MyConstants.USER_DR);
                                    helper.startAuth(fingerprintManager, cryptoObject);
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    protected void generateKey() {
        try {
            keyStore = KeyStore.getInstance("AndroidKeyStore");
        } catch (Exception e) {
            e.printStackTrace();
        }

        KeyGenerator keyGenerator;
        try {
            keyGenerator = KeyGenerator.getInstance(KeyProperties.KEY_ALGORITHM_AES, "AndroidKeyStore");
        } catch (NoSuchAlgorithmException | NoSuchProviderException e) {
            throw new RuntimeException("Failed to get KeyGenerator instance", e);
        }

        try {
            keyStore.load(null);
            keyGenerator.init(new
                    KeyGenParameterSpec.Builder(KEY_NAME,
                    KeyProperties.PURPOSE_ENCRYPT |
                            KeyProperties.PURPOSE_DECRYPT)
                    .setBlockModes(KeyProperties.BLOCK_MODE_CBC)
                    .setUserAuthenticationRequired(true)
                    .setEncryptionPaddings(
                            KeyProperties.ENCRYPTION_PADDING_PKCS7)
                    .build());
            keyGenerator.generateKey();
        } catch (NoSuchAlgorithmException |
                InvalidAlgorithmParameterException
                | CertificateException | IOException e) {
            throw new RuntimeException(e);
        }
    }


    @TargetApi(Build.VERSION_CODES.M)
    public boolean cipherInit() {
        try {
            cipher = Cipher.getInstance(KeyProperties.KEY_ALGORITHM_AES + "/" + KeyProperties.BLOCK_MODE_CBC + "/" + KeyProperties.ENCRYPTION_PADDING_PKCS7);
        } catch (NoSuchAlgorithmException | NoSuchPaddingException e) {
            throw new RuntimeException("Failed to get Cipher", e);
        }

        try {
            keyStore.load(null);
            SecretKey key = (SecretKey) keyStore.getKey(KEY_NAME,
                    null);
            cipher.init(Cipher.ENCRYPT_MODE, key);
            return true;
        } catch (KeyPermanentlyInvalidatedException e) {
            return false;
        } catch (KeyStoreException | CertificateException | UnrecoverableKeyException | IOException | NoSuchAlgorithmException | InvalidKeyException e) {
            throw new RuntimeException("Failed to init Cipher", e);
        }
    }

    private void doLogin() {
        String email = etEmail.getText().toString().trim();
        String password = etPassword.getText().toString().trim();
        isRememberMe = cbRememberMe.isChecked();

        if (TextUtils.isEmpty(email)) {
            etEmail.requestFocus();
            etEmail.setError("Email can't be empty");
        } else if (TextUtils.isEmpty(password)) {
            etPassword.requestFocus();
            etPassword.setError("Password can't be empty");
        } else if (!Utils.isValidEmail(email)) {
            etEmail.requestFocus();
            etEmail.setError("Email is not valid");
        } else if (password.length() < 6) {
            etPassword.requestFocus();
            etPassword.setError("Password must be at least 6 characters");
        } else {
            if (!Internet.isAvailable(DRLoginActivity.this)) {
                Internet.showAlertDialog(DRLoginActivity.this, "Error!", "No Internet Connection", false);
            } else {
                new CallRequest(DRLoginActivity.this).doctorLogin(email, password);
            }
        }
    }

    public void fingerprintLoginCall() {
        String email = sharedpreferences.getString(MyConstants.USER_EMAIL, "");
        String password = sharedpreferences.getString(MyConstants.DR_PASSWORD, "");
        isRememberMe = cbRememberMe.isChecked();

        new CallRequest(DRLoginActivity.this).doctorLogin(email, password);
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        try {

            Utils.hideProgressDialog();
            if (result != null && !result.isEmpty()) {
                Log.i("TAG", "TAG Result : " + result);

                switch (request) {
                    case doctorLogin:

                        try {
                            JSONObject object = new JSONObject(result);
                            String error_code = object.getString("error_code");
                            if (error_code.equalsIgnoreCase("0")) {
                                JSONObject resultObj = object.getJSONObject("result");
                                setLoginData(resultObj);
                            } else if (error_code.equalsIgnoreCase("5")) {

                                JSONObject resultObj = object.getJSONObject("result");
                                JSONArray arrayUser = resultObj.getJSONArray("user");
                                JSONObject obj = arrayUser.getJSONObject(0);

                                String id = obj.getString("id");

                                String error_string = object.getString("error_string");
                                Toast.makeText(instance, error_string, Toast.LENGTH_SHORT).show();
                            } else {
                                String error_string = object.getString("error_string");
                                Toast.makeText(instance, error_string, Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;
                }
            } else {
                Utils.showToast("Please try again later", this);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setLoginData(JSONObject obj) {

        try {
            JSONArray doctorArray = obj.getJSONArray("doctor");
            JSONObject doctor_Object = doctorArray.getJSONObject(0);
            String user_id = doctor_Object.getString("id");
            String email = doctor_Object.getString("email");
            String ApiToken = doctor_Object.getString("ApiToken");
            String Latitude = doctor_Object.getString("Latitude");
            String Longitude = doctor_Object.getString("Longitude");
//            String ReferCode = doctor_Object.getString("ReferCode");

            JSONArray doctor_detailArray = obj.getJSONArray("doctor_detail");
            JSONObject doctor_detail_object = doctor_detailArray.getJSONObject(0);
            String FirstName = doctor_detail_object.getString("FirstName");
            String LastName = doctor_detail_object.getString("LastName");
            String Gender = doctor_detail_object.getString("Gender");
            String ConsultCharge = doctor_detail_object.getString("CosultCharge");
            String CountryId = doctor_detail_object.getString("CountryId");
            String StateId = doctor_detail_object.getString("StateId");
            String CountryCode = doctor_detail_object.getString("CountryCode");
            String Country = doctor_detail_object.getString("Country");
            String State = doctor_detail_object.getString("State");
            String Phone = doctor_detail_object.getString("Phone");
            String BirthDate = doctor_detail_object.getString("BirthDate");
            String Age = doctor_detail_object.getString("Age");
            String Address = doctor_detail_object.getString("Address");
            String Timezone = doctor_detail_object.getString("Timezone");
            String ProfilePic = doctor_detail_object.getString("ProfilePic");
//            String ProfilePicSocial = doctor_detail_object.getString("ProfilePicSocial");
            String CoverPic = doctor_detail_object.getString("CoverPic");
//            String CurrentDiscount = doctor_detail_object.getString("CurrentDiscount");
            String WalletBalance = doctor_detail_object.getString("WalletBalance");


            JSONArray doctor_device = obj.getJSONArray("doctor_device");

            name = FirstName + " " + LastName;

            SharedPreferences sharedpreferences = getSharedPreferences(MyConstants.PREF, Context.MODE_PRIVATE);

            SharedPreferences.Editor editor = sharedpreferences.edit();

            editor.putString(MyConstants.USER_TYPE, MyConstants.USER_DR);
            editor.putString(MyConstants.USER_EMAIL, email);
            editor.putString(MyConstants.USER_ID, user_id);
            editor.putString(MyConstants.PROFILE_PIC, ProfilePic);
            editor.putString(MyConstants.PT_ZONE, Timezone);
            editor.putString(MyConstants.PT_NAME, name);
            editor.putString(MyConstants.PT_FIRST_NAME, FirstName);
            editor.putString(MyConstants.PT_LAST_NAME, LastName);
            editor.putString(MyConstants.GENDER, Gender);
            editor.putString(MyConstants.DATE_OF_BIRTH, BirthDate);
            editor.putString(MyConstants.MOBILE, Phone);
            editor.putString(MyConstants.COUNTRY, Country);
            editor.putString(MyConstants.ADDRESS, Address);
            editor.putString(MyConstants.AGE, Age);
//            editor.putString(MyConstants.CURRENT_DISCOUNT, CurrentDiscount);
            editor.putString(MyConstants.WALLET_BALANCE, WalletBalance);
//            editor.putString(MyConstants.REFER_CODE, ReferCode);
            editor.putString(MyConstants.TOKEN, ApiToken);
            editor.putString(MyConstants.SINCH_ID, "dddd" + user_id);
            editor.putBoolean(MyConstants.IS_GUEST, false);
            editor.putBoolean(MyConstants.FIRST_LOGIN, true);
            editor.putBoolean(MyConstants.IS_LOGGED_IN, true);
            editor.putString(MyConstants.LOGIN_TYPE, "0");
            editor.putString(MyConstants.SOCIAL_ID, "0");
            editor.putString(MyConstants.CONSULT_CHARGE, ConsultCharge);
            MyConstants.isGuest = "0";
            if (isRememberMe) {
                editor.putString(MyConstants.DR_PASSWORD, etPassword.getText().toString());
            } else {
                editor.putString(MyConstants.DR_PASSWORD, "");
            }
            editor.commit();

            App.user.setUserID(user_id);
            App.user.setName(name);
            App.user.setUserEmail(email);
            App.user.setUser_Type(MyConstants.USER_DR);
            App.user.setProfileUrl(sharedpreferences.getString(MyConstants.PROFILE_PIC, ""));
            App.user.setSinch_id(sharedpreferences.getString(MyConstants.SINCH_ID, ""));
            MyConstants.API_TOKEN = ApiToken;
            getSinchServiceInterface().startClient(App.user.getSinch_id());
            getSinchServiceInterface().registerPushToken(this);

            System.out.println("data saved successfully");


            // reload all data
            MyConstants.specialitiesList.clear();
            MyConstants.isLoadAgain = true;
            MyConstants.bookingHistoryList.clear();
            MyConstants.messageHistoryList.clear();
            MyConstants.userChatList.clear();
            MyConstants.doctorChatList.clear();
            MyConstants.isBookingHistoryLoad = true;
            MyConstants.isMessageHistoryLoad = true;
            MyConstants.userProfile = null;
            MyConstants.isProfileLoad = true;
            MyConstants.doctorProfilePersonal = null;
            MyConstants.isDoctorProfileLoad = true;

            Intent intent = new Intent(DRLoginActivity.this, PTDashboardActivity.class);
            startActivity(intent);
            ActivityCompat.finishAffinity(DRLoginActivity.this);

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onProgressUpdate(String uniqueMessageId, int progres) {

    }

    @Override
    public void onProgressComplete(String uniqueMessageId, String result, Constant.REQUESTS request) {

    }

    @Override
    public void tokenRegistered() {

    }

    @Override
    public void tokenRegistrationFailed(SinchError sinchError) {

    }
}
