package com.connex.md.doctor.adapter;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.support.v4.app.Fragment;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.widget.RecyclerView;
import android.text.InputType;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.connex.md.R;
import com.connex.md.interfaces.DoctorListener;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

/**
 * Created by abc on 11/21/2017.
 */

public class EducationAdapter extends RecyclerView.Adapter<EducationAdapter.MyViewHolder> {

    private Context mContext;
    private List<HashMap<String, String>> educationList;
    private Dialog dialog;
    private ImageView ivClose;
    private Button btnSave;
    private EditText etUniversityName, etFrom, etTo;
    TextView tvUniversityName;
    String universityName, startYear, endYear, type;
    DoctorListener doctorListener;
    DatePickerDialog DialogDOBFrom, DialogDOBTo;
    private SimpleDateFormat dateFormatter;
    int fromYear, fromMonth, fromDay;

    public EducationAdapter(Fragment mContext, List<HashMap<String, String>> educationList, String type) {
        this.mContext = mContext.getActivity();
        this.educationList = educationList;
        this.type = type;
        doctorListener = (DoctorListener) mContext;
    }

    @Override
    public EducationAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_education, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final EducationAdapter.MyViewHolder holder, int position) {
        if (!TextUtils.isEmpty(educationList.get(position).get("StartYear")) && !TextUtils.isEmpty(educationList.get(position).get("EndYear"))) {
            holder.tvEducation.setText(educationList.get(position).get("Description") + ", " + educationList.get(position).get("StartYear") +
                    " - " + educationList.get(position).get("EndYear"));
        } else if (!TextUtils.isEmpty(educationList.get(position).get("StartYear"))) {
            holder.tvEducation.setText(educationList.get(position).get("Description") + ", " + educationList.get(position).get("StartYear"));
        } else if (!TextUtils.isEmpty(educationList.get(position).get("EndYear"))) {
            holder.tvEducation.setText(educationList.get(position).get("Description") + ", " + educationList.get(position).get("EndYear"));
        } else {
            holder.tvEducation.setText(educationList.get(position).get("Description"));
        }

        holder.ivEdit.setTag(position);
        holder.ivEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int position = (int) view.getTag();

                openEditDialog(position);
            }
        });

    }

    private void openEditDialog(final int position) {
        createDialog();
        initDialogComponents();

        etUniversityName.setText(educationList.get(position).get("Description"));
        etFrom.setText(educationList.get(position).get("StartYear"));
        etTo.setText(educationList.get(position).get("EndYear"));

        dateFormatter = new SimpleDateFormat("yyyy", Locale.US);
        Calendar newCalendar = Calendar.getInstance();
        DialogDOBFrom = new DatePickerDialog(mContext, R.style.DialogTheme, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                etFrom.setText(dateFormatter.format(newDate.getTime()));
                String from = dateFormatter.format(newDate.getTime());
                System.out.println("date of birth" + from);
                fromYear = year;
                fromMonth = monthOfYear;
                fromDay = dayOfMonth;

                etTo.setEnabled(true);
                //etTo.setFocusableInTouchMode(true);
                etTo.setCursorVisible(true);

                Calendar c = Calendar.getInstance();
                c.set(Calendar.MONTH, fromMonth);
                c.set(Calendar.DAY_OF_MONTH, fromDay);
                c.set(Calendar.YEAR, fromYear);
                DialogDOBTo.getDatePicker().setMinDate(c.getTimeInMillis());
                DialogDOBTo.getDatePicker().setMaxDate(System.currentTimeMillis());
            }

        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
        DialogDOBFrom.getDatePicker().setMaxDate(System.currentTimeMillis());

        DialogDOBTo = new DatePickerDialog(mContext, R.style.DialogTheme, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                etTo.setText(dateFormatter.format(newDate.getTime()));
                String to = dateFormatter.format(newDate.getTime());
                System.out.println("date of birth" + to);
            }

        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));


        etFrom.setInputType(InputType.TYPE_NULL);
        etFrom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                etFrom.requestFocus();
                try {
                    InputMethodManager inputMethodManager = (InputMethodManager)
                            mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
                    if (inputMethodManager != null) {
                        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
                    }
                } catch (Exception e){
                    e.printStackTrace();
                }
                DialogDOBFrom.show();
            }
        });

        etTo.setInputType(InputType.TYPE_NULL);
        etTo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                etTo.requestFocus();
                try {
                    InputMethodManager inputMethodManager = (InputMethodManager)
                            mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
                    if (inputMethodManager != null) {
                        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
                    }
                } catch (Exception e){
                    e.printStackTrace();
                }
                DialogDOBTo.show();
            }
        });

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                universityName = etUniversityName.getText().toString().trim();
                startYear = etFrom.getText().toString().trim();
                endYear = etTo.getText().toString().trim();

                if (TextUtils.isEmpty(universityName)) {
                    etUniversityName.requestFocus();
                    etUniversityName.setError("University name is empty");
                    return;
                } else if (TextUtils.isEmpty(startYear)) {
                    etFrom.requestFocus();
                    etFrom.setError("Year is empty");
                    return;
                }

               /* educationList.get(position).put("Description", universityName);
                educationList.get(position).put("StartYear", startYear);
                educationList.get(position).put("EndYear", endYear);

                notifyDataSetChanged();*/

                dialog.dismiss();

                doctorListener.editDetails(position, universityName, startYear, endYear);
            }
        });

        ivClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
    }

    private void createDialog() {
        dialog = new Dialog(mContext, R.style.CustomDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        // Include dialog.xml file
        dialog.setContentView(R.layout.dialog_edit_doctor);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
        dialog.show();
    }

    private void initDialogComponents() {
        btnSave = dialog.findViewById(R.id.btnSave);
        ivClose = dialog.findViewById(R.id.ivClose);
        etUniversityName = dialog.findViewById(R.id.etUniversityName);
        tvUniversityName = dialog.findViewById(R.id.tvUniversityName);
        etFrom = dialog.findViewById(R.id.etFrom);
        etTo = dialog.findViewById(R.id.etTo);

        if (type.equalsIgnoreCase("Profession")){
            tvUniversityName.setText("PROFESSION NAME");
            etUniversityName.setHint("Profession name");
        }
    }

    @Override
    public int getItemCount() {
        return educationList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView tvEducation;
        ImageView ivEdit;

        public MyViewHolder(View itemView) {
            super(itemView);

            tvEducation = itemView.findViewById(R.id.tvEducation);
            ivEdit = itemView.findViewById(R.id.ivEdit);
        }
    }
}
