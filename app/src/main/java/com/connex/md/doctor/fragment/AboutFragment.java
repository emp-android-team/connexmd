package com.connex.md.doctor.fragment;


import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.connex.md.R;
import com.connex.md.interfaces.AsyncTaskListner;
import com.connex.md.others.App;
import com.connex.md.others.Internet;
import com.connex.md.others.PicassoTrustAll;
import com.connex.md.ws.CallRequest;
import com.connex.md.ws.Constant;
import com.connex.md.ws.MyConstants;
import com.connex.md.ws.Utils;
import com.squareup.picasso.Callback;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import static android.app.Activity.RESULT_OK;

/**
 * A simple {@link Fragment} subclass.
 */
public class AboutFragment extends Fragment implements AsyncTaskListner{

    public LinearLayout llUploadPic;
    public ImageView ivDoctor, ivEditTop, ivEditBottom;
    public EditText etFirstName, etLastName, etSpecialist, etFee, etLocation, etAboutDoctor;
    public Button btnSaveDoctor;
    private static int RESULT_LOAD_IMG_DP = 1;
    private static int RESULT_CROP_DP = 3;
    String filePath = "";
    String filePathFirst = "";
    String firstName, lastName, specialist, fee, location, aboutDoctor;

    public AboutFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_about, container, false);

        llUploadPic = view.findViewById(R.id.ll_upload_pic);
        ivDoctor = view.findViewById(R.id.ivDoctor);
        ivEditTop = view.findViewById(R.id.ivEditTop);
        ivEditBottom = view.findViewById(R.id.ivEditBottom);
        etFirstName = view.findViewById(R.id.etFirstName);
        etLastName = view.findViewById(R.id.etLastName);
        etSpecialist = view.findViewById(R.id.etSpecialist);
        etFee = view.findViewById(R.id.etFee);
        etLocation = view.findViewById(R.id.etLocation);
        etAboutDoctor = view.findViewById(R.id.etAboutDoctor);
        btnSaveDoctor = view.findViewById(R.id.btnSaveDoctor);

        etFirstName.setText(MyConstants.editDoctorProfile.getFirstName());
        etLastName.setText(MyConstants.editDoctorProfile.getLastName());
        etFee.setText(MyConstants.editDoctorProfile.getCosultCharge());
        etSpecialist.setText(MyConstants.editDoctorProfile.getSpeciality());
        etLocation.setText(MyConstants.editDoctorProfile.getCountry());
        etAboutDoctor.setText(MyConstants.editDoctorProfile.getAbout_doctor());

        try {
            PicassoTrustAll.getInstance(getActivity())
                    .load(MyConstants.editDoctorProfile.getProfilePic())
                    .placeholder(R.drawable.avatar)
                    .error(R.drawable.avatar)
                    .into(ivDoctor, new Callback() {
                        @Override
                        public void onSuccess() {
                        }

                        @Override
                        public void onError() {
                        }
                    });
        } catch (Exception e){
            e.printStackTrace();
        }

        /*etFirstName.setFocusable(false);
        etFirstName.setEnabled(false);
        etFirstName.setCursorVisible(false);

        etLastName.setFocusable(false);
        etLastName.setEnabled(false);
        etLastName.setCursorVisible(false);

        etFee.setFocusable(false);
        etFee.setEnabled(false);
        etFee.setCursorVisible(false);*/

        etSpecialist.setFocusable(false);
        etSpecialist.setEnabled(false);
        etSpecialist.setCursorVisible(false);

        /*etLocation.setFocusable(false);
        etLocation.setEnabled(false);
        etLocation.setCursorVisible(false);

        etAboutDoctor.setFocusable(false);
        etAboutDoctor.setEnabled(false);
        etAboutDoctor.setCursorVisible(false);*/

        /*ivEditTop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                etFirstName.setFocusableInTouchMode(true);
                etFirstName.setEnabled(true);
                etFirstName.setCursorVisible(true);

                etLastName.setFocusableInTouchMode(true);
                etLastName.setEnabled(true);
                etLastName.setCursorVisible(true);

                etFee.setFocusableInTouchMode(true);
                etFee.setEnabled(true);
                etFee.setCursorVisible(true);

                *//*etSpecialist.setFocusableInTouchMode(true);
                etSpecialist.setEnabled(true);
                etSpecialist.setCursorVisible(true);*//*

                etLocation.setFocusableInTouchMode(true);
                etLocation.setEnabled(true);
                etLocation.setCursorVisible(true);
            }
        });

        ivEditBottom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                etAboutDoctor.setFocusableInTouchMode(true);
                etAboutDoctor.setEnabled(true);
                etAboutDoctor.setCursorVisible(true);
            }
        });*/

        llUploadPic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(Utils.checkPermission(getActivity())) {
                    loadImageFromGalleryForDp();
                }
            }
        });

        btnSaveDoctor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                firstName = etFirstName.getText().toString().trim();
                lastName = etLastName.getText().toString().trim();
                specialist = etSpecialist.getText().toString().trim();
                fee = etFee.getText().toString().trim();
                location = etLocation.getText().toString().trim();
                aboutDoctor = etAboutDoctor.getText().toString().trim();

                if (TextUtils.isEmpty(firstName)){
                    etFirstName.setError("First name is empty");
                    etFirstName.requestFocus();
                    return;
                } else if (TextUtils.isEmpty(lastName)){
                    etLastName.setError("Last name is empty");
                    etLastName.requestFocus();
                    return;
                } else if (TextUtils.isEmpty(specialist)){
                    etSpecialist.setError("Specialist is empty");
                    etSpecialist.requestFocus();
                    return;
                } else if (TextUtils.isEmpty(fee)){
                    etFee.setError("Fee is empty");
                    etFee.requestFocus();
                    return;
                } else if (TextUtils.isEmpty(location)){
                    etLocation.setError("Location is empty");
                    etLocation.requestFocus();
                    return;
                } else if (TextUtils.isEmpty(aboutDoctor)){
                    etAboutDoctor.setError("About doctor is empty");
                    etAboutDoctor.requestFocus();
                    return;
                }

                saveDetails();
            }
        });

        return view;
    }

    private void saveDetails() {

        if (!Internet.isAvailable(getActivity())) {
            Internet.showAlertDialog(getActivity(), "Error!", "No Internet Connection", false);

            return;
        }

        Map<String, String> map = new HashMap<String, String>();
        map.put("url", MyConstants.DOCTOR_BASE_URL + "doctorProfileUpdate");
        map.put("ApiToken", MyConstants.API_TOKEN);
        map.put("Version", MyConstants.WS_VERSION);
        map.put("DoctorId", App.user.getUserID());
        map.put("Type", "About");
        map.put("FirstName", firstName);
        map.put("LastName", lastName);
        map.put("Description", aboutDoctor);
        map.put("ConsultCharge", fee);
        map.put("Location", location);
        map.put("IsEdit", "0");
        if (!TextUtils.isEmpty(filePath)){
            map.put("ProfilePic", filePath);
        }

        new CallRequest(this).saveDoctorDetails(map);

    }

    public void loadImageFromGalleryForDp() {
        // Create intent to Open Image applications like Gallery, Google Photos
        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        // Start the Intent
        startActivityForResult(galleryIntent, RESULT_LOAD_IMG_DP);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        try {
            // When an Image is picked
            if (requestCode == RESULT_LOAD_IMG_DP && resultCode == RESULT_OK
                    && null != data) {
                // Get the Image from data

                Uri selectedImage = data.getData();
                String[] filePathColumn = {MediaStore.Images.Media.DATA};

                // Get the cursor
                Cursor cursor = getActivity().getContentResolver().query(selectedImage,
                        filePathColumn, null, null, null);
                // Move to first row
                int columnIndex = 0;
                if (cursor != null) {
                    cursor.moveToFirst();
                    columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    filePathFirst = cursor.getString(columnIndex);
                    System.out.println(filePathFirst);
                    cursor.close();


                    /*ivUser.setImageBitmap(BitmapFactory
                            .decodeFile(filePath));*/
                }

                doCropDP(filePathFirst);

            }

            if (requestCode == RESULT_CROP_DP) {
                if (resultCode == Activity.RESULT_OK) {
                    if (!TextUtils.isEmpty(filePath)) {

                        System.out.println("path***" + filePath);
                        Bitmap selectedBitmap = decodeSampledBitmapFromPath(filePath, 100, 100);
                        System.out.println("selectedBitmap:::::" + selectedBitmap);

                        // Set The Bitmap Data To ImageView
                        ivDoctor.setImageBitmap(selectedBitmap);
                        //ivPageDp.setScaleType(ImageView.ScaleType.FIT_XY);

                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static Bitmap decodeSampledBitmapFromPath(String path, int reqWidth,
                                                     int reqHeight) {

        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, options);

        options.inSampleSize = calculateInSampleSize(options, reqWidth,
                reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        Bitmap bmp = BitmapFactory.decodeFile(path, options);
        return bmp;
    }

    public static int calculateInSampleSize(BitmapFactory.Options options,
                                            int reqWidth, int reqHeight) {

        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            if (width > height) {
                inSampleSize = Math.round((float) height / (float) reqHeight);
            } else {
                inSampleSize = Math.round((float) width / (float) reqWidth);
            }
        }
        return inSampleSize;
    }

    private void doCropDP(String picPath) {
        try {

            Intent cropIntent = new Intent("com.android.camera.action.CROP");

            File f = new File(picPath);
            Uri contentUri = Uri.fromFile(f);

            cropIntent.setDataAndType(contentUri, "image/*");

            cropIntent.putExtra("crop", "true");
            // indicate aspect of desired crop
            cropIntent.putExtra("aspectX", 1);
            cropIntent.putExtra("aspectY", 1);
            // indicate output X and Y
            cropIntent.putExtra("outputX", 256);
            cropIntent.putExtra("outputY", 256);
            // retrieve data on return
            cropIntent.putExtra("return-data", false);

            File sdCardDirectory = new File(Environment.getExternalStorageDirectory().getPath() + "/ConnexMd/profile_pictures");

            if (!sdCardDirectory.exists()) {
                sdCardDirectory.mkdirs();
            }

            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.US)
                    .format(new Date());

            String nw = "Profile_" + timeStamp + ".jpeg";

            File image = new File(sdCardDirectory, nw);

            //uploadPathDp = sdCardDirectory + "/" + nw;
            filePath = new File(sdCardDirectory, nw).getAbsolutePath();
            System.out.println("UploadPath:::" + filePath);

            try {
                image.createNewFile();
            } catch (IOException ex) {
                Log.e("io", ex.getMessage());
            }

            Uri uri;
            /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                uri = FileProvider.getUriForFile(getActivity(),
                        BuildConfig.APPLICATION_ID + ".provider",
                        image);
            } else {*/
            uri = Uri.fromFile(image);
            //}

            cropIntent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
            // start the activity - we handle returning in onActivityResult
            startActivityForResult(cropIntent, RESULT_CROP_DP);
        } catch (ActivityNotFoundException anfe) {
            String errorMessage = "Your device doesn't support the crop action!";
            Toast toast = Toast.makeText(getActivity(), errorMessage, Toast.LENGTH_SHORT);
            toast.show();
        }
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        Utils.removeSimpleSpinProgressDialog();
        try {
            if (result != null && !result.isEmpty()) {
                Log.i("TAG", "TAG Result : " + result);
                //      {"document":{"response":{"status":1,"message":"Success."}}}
                switch (request) {
                    case saveDoctorDetails:
                        Utils.removeSimpleSpinProgressDialog();

                        try {
                            JSONObject mainObj = new JSONObject(result);
                            if (mainObj.getString("error_code").equalsIgnoreCase("0")) {

                                if (mainObj.getJSONObject("result")!= null) {

                                    JSONObject object = mainObj.getJSONObject("result");

                                    MyConstants.editDoctorProfile.setFirstName(firstName);
                                    MyConstants.editDoctorProfile.setLastName(lastName);
                                    MyConstants.editDoctorProfile.setSpeciality(specialist);
                                    MyConstants.editDoctorProfile.setCosultCharge(fee);
                                    MyConstants.editDoctorProfile.setCountry(location);
                                    MyConstants.editDoctorProfile.setAbout_doctor(aboutDoctor);
                                    MyConstants.editDoctorProfile.setProfilePic(object.getString("ProfilePic"));

                                    String name = firstName + lastName;
                                    SharedPreferences sharedpreferences = getActivity().getSharedPreferences(MyConstants.PREF, Context.MODE_PRIVATE);
                                    SharedPreferences.Editor editor = sharedpreferences.edit();
                                    editor.putString(MyConstants.PT_FIRST_NAME, firstName);
                                    editor.putString(MyConstants.PT_LAST_NAME, lastName);
                                    editor.putString(MyConstants.PT_NAME, name);
                                    editor.putString(MyConstants.COUNTRY, location);
                                    editor.putString(MyConstants.CONSULT_CHARGE, fee);
                                    editor.putString(MyConstants.PROFILE_PIC, MyConstants.editDoctorProfile.getProfilePic());
                                    editor.commit();
                                    App.user.setFirstName(MyConstants.editDoctorProfile.getFirstName());
                                    App.user.setLastName(MyConstants.editDoctorProfile.getLastName());
                                    App.user.setName(name);
                                    App.user.setProfileUrl(MyConstants.editDoctorProfile.getProfilePic());

                                    /*etFirstName.setFocusable(false);
                                    etFirstName.setEnabled(false);
                                    etFirstName.setCursorVisible(false);

                                    etLastName.setFocusable(false);
                                    etLastName.setEnabled(false);
                                    etLastName.setCursorVisible(false);

                                    etFee.setFocusable(false);
                                    etFee.setEnabled(false);
                                    etFee.setCursorVisible(false);*/

                                    etSpecialist.setFocusable(false);
                                    etSpecialist.setEnabled(false);
                                    etSpecialist.setCursorVisible(false);

                                    /*etLocation.setFocusable(false);
                                    etLocation.setEnabled(false);
                                    etLocation.setCursorVisible(false);

                                    etAboutDoctor.setFocusable(false);
                                    etAboutDoctor.setEnabled(false);
                                    etAboutDoctor.setCursorVisible(false);*/

                                    Utils.showToast("Profile saved successfully", getActivity());
                                }
                            } else {
                                Utils.showToast(mainObj.getString("error_string"), getActivity());
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;
                }
            }
        } catch (Exception e) {
            Utils.removeSimpleSpinProgressDialog();
            e.printStackTrace();
            Utils.showToast("Please try again later", getActivity());
        }
    }

    @Override
    public void onProgressUpdate(String uniqueMessageId, int progres) {

    }

    @Override
    public void onProgressComplete(String uniqueMessageId, String result, Constant.REQUESTS request) {

    }
}
