package com.connex.md.doctor.fragment;


import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.connex.md.R;
import com.connex.md.custom_views.CenteredToolbar;
import com.connex.md.interfaces.AsyncTaskListner;
import com.connex.md.model.MessageHistory;
import com.connex.md.others.App;
import com.connex.md.others.Internet;
import com.connex.md.patient.activity.PTDashboardActivity;
import com.connex.md.ws.CallRequest;
import com.connex.md.ws.Constant;
import com.connex.md.ws.MyConstants;
import com.connex.md.ws.Utils;
import com.onesignal.OneSignal;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 */
public class DRMessagesFragment extends Fragment implements AsyncTaskListner {

    public SearchView searchView;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private MenuItem item;
    private View view;
    private Fragment f;
    private ViewPagerAdapter adapter;
    private SwipeRefreshLayout swipe_container;

    public DRMessagesFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_dr_messages, container, false);

        CenteredToolbar toolbar = view.findViewById(R.id.toolbar);
        if (getActivity() != null) {
            ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
            ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Messages");
            ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);

            /*if (((AppCompatActivity) getActivity()).getSupportActionBar() != null) {
                ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayShowTitleEnabled(false);
            }*/
        }

        OneSignal.clearOneSignalNotifications();

        SharedPreferences.Editor editor = PTDashboardActivity.preferences.edit();
        editor.putString(MyConstants.BADGE_COUNT, "0");
        editor.commit();
        PTDashboardActivity.setBadgeCount();

        viewPager = view.findViewById(R.id.viewpager);
        tabLayout = view.findViewById(R.id.tabs);
        swipe_container = view.findViewById(R.id.swipe_container);

        setupViewPager(viewPager);
        tabLayout.setupWithViewPager(viewPager);

        /*tabLayout.addTab(tabLayout.newTab().setText("User Chats"));
        tabLayout.addTab(tabLayout.newTab().setText("Doctor Chats"));

        adapter = new ViewPagerAdapter(getChildFragmentManager());
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));*/

        swipe_container.setColorScheme(android.R.color.holo_blue_dark,
                android.R.color.holo_blue_light,
                android.R.color.holo_green_dark,
                android.R.color.holo_green_light);

        swipe_container.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getDoctorMessageHistory();
            }
        });

        if (MyConstants.isMessageHistoryLoad) {
            getDoctorMessageHistory();
            MyConstants.isMessageHistoryLoad = false;
        }

        setHasOptionsMenu(true);

        return view;
    }

    private void getDoctorMessageHistory() {

        if (!Internet.isAvailable(getActivity())) {
            Internet.showAlertDialog(getActivity(), "Error!", "No Internet Connection", false);
            return;
        }

        Map<String, String> map = new HashMap<String, String>();
        map.put("url", MyConstants.DOCTOR_BASE_URL + "messageHistory");
        map.put("ApiToken", MyConstants.API_TOKEN);
        map.put("Version", MyConstants.WS_VERSION);
        map.put("DoctorId", App.user.getUserID());

        new CallRequest(DRMessagesFragment.this).getChatHistory(map);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        item = menu.findItem(R.id.search);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        //super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.main_messages, menu);

        item = menu.findItem(R.id.search);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(menu.findItem(R.id.search));

        MenuItemCompat.setOnActionExpandListener(item, new MenuItemCompat.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                tabLayout.setVisibility(View.GONE);
                return true;
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                tabLayout.setVisibility(View.VISIBLE);
                return true;
            }
        });

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                try {
                    if (!TextUtils.isEmpty(newText)) {
                        if (MyConstants.TAB_POSITION == 0) {
                            if (f != null) {
                                if (((UserChatFragment) f).messagesAdapter != null) {
                                    ((UserChatFragment) f).messagesAdapter.filters(newText);
                                }
                            } else {
                                ViewPagerAdapter adapter = (ViewPagerAdapter) viewPager.getAdapter();
                                if (adapter != null) {
                                    f = adapter.getItem(0);
                                    ((UserChatFragment) f).messagesAdapter.filters(newText);
                                }
                            }
                        } else if (MyConstants.TAB_POSITION == 1) {
                            if (f != null) {
                                if (((DoctorChatFragment) f).messagesAdapter != null) {
                                    ((DoctorChatFragment) f).messagesAdapter.filters(newText);
                                }
                            } else {
                                ViewPagerAdapter adapter = (ViewPagerAdapter) viewPager.getAdapter();
                                if (adapter != null) {
                                    f = adapter.getItem(1);
                                    ((DoctorChatFragment) f).messagesAdapter.filters(newText);
                                }
                            }
                        }
                    } else {
                        if (MyConstants.TAB_POSITION == 0) {
                            if (f != null) {
                                if (((UserChatFragment) f).messagesAdapter != null) {
                                    ((UserChatFragment) f).messagesAdapter.filters("");
                                }
                            } else {
                                ViewPagerAdapter adapter = (ViewPagerAdapter) viewPager.getAdapter();
                                if (adapter != null) {
                                    f = adapter.getItem(0);
                                    ((UserChatFragment) f).messagesAdapter.filters("");
                                }
                            }
                        } else if (MyConstants.TAB_POSITION == 1) {
                            if (f != null) {
                                if (((DoctorChatFragment) f).messagesAdapter != null) {
                                    ((DoctorChatFragment) f).messagesAdapter.filters("");
                                }
                            } else {
                                ViewPagerAdapter adapter = (ViewPagerAdapter) viewPager.getAdapter();
                                if (adapter != null) {
                                    f = adapter.getItem(1);
                                    ((DoctorChatFragment) f).messagesAdapter.filters("");
                                }
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return true;
            }
        });
        super.onCreateOptionsMenu(menu, inflater);
    }

   /* @Override
    public void onDestroyOptionsMenu() {
        super.onDestroyOptionsMenu();

        if (searchView != null &&
                !searchView.getQuery().toString().isEmpty()) {

            searchView.setIconified(true);
            searchView.setIconified(true);
        }
    }*/

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getChildFragmentManager());

        adapter.addFragment(new UserChatFragment(), "User Chats");
        adapter.addFragment(new DoctorChatFragment(), "Doctor Chats");

        viewPager.setAdapter(adapter);
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        Utils.removeSimpleSpinProgressDialog();
        try {
            if (result != null && !result.isEmpty()) {
                Log.i("TAG", "TAG Result : " + result);
                //      {"document":{"response":{"status":1,"message":"Success."}}}
                switch (request) {
                    case chatHistory:
                        Utils.removeSimpleSpinProgressDialog();
                        try {
                            JSONObject mainObj = new JSONObject(result);
                            if (mainObj.getString("error_code").equalsIgnoreCase("0")) {
                                MyConstants.userChatList.clear();
                                MyConstants.doctorChatList.clear();
                                if (mainObj.getJSONObject("result") != null && mainObj.getJSONObject("result").length() > 0) {
                                    JSONObject object = mainObj.getJSONObject("result");

                                    JSONArray User = object.getJSONArray("User");
                                    JSONArray Doctor = object.getJSONArray("Doctor");

                                    String keys = "";
                                    for (int i = 0; i < User.length(); i++) {
                                        JSONObject jsonObject = User.getJSONObject(i);

                                        final MessageHistory messageHistory = new MessageHistory();
                                        messageHistory.setDoctorId(jsonObject.getString("DoctorId"));
                                        messageHistory.setBookingId(jsonObject.getString("UniqueId"));
                                        messageHistory.setUserId(jsonObject.getString("UserId"));
                                        messageHistory.setConsultTime(jsonObject.getString("ConsultTime"));
                                        messageHistory.setFirstName(jsonObject.getString("FirstName"));
                                        messageHistory.setLastName(jsonObject.getString("LastName"));
                                        if (App.user.getUser_Type().equalsIgnoreCase(MyConstants.USER_DR)) {
                                            messageHistory.setIsGuest(jsonObject.getString("IsGuest"));
                                        }
                                        messageHistory.setProfilePic(jsonObject.getString("ProfilePic"));

                                        String receiverID;

                                        if (App.user.getUser_Type().equalsIgnoreCase(MyConstants.USER_PT)) {
                                            receiverID = jsonObject.getString("DoctorId");
                                        } else {
                                            receiverID = jsonObject.getString("UserId");
                                        }

                                        MyConstants.userChatList.add(messageHistory);

                                    }

                                    for (int i = 0; i < Doctor.length(); i++) {
                                        JSONObject jsonObject = Doctor.getJSONObject(i);

                                        MessageHistory messageHistory = new MessageHistory();
                                        messageHistory.setDoctorId(jsonObject.getString("DoctorId"));
                                        messageHistory.setFirstName(jsonObject.getString("FirstName"));
                                        messageHistory.setLastName(jsonObject.getString("LastName"));
                                        messageHistory.setProfilePic(jsonObject.getString("ProfilePic"));
                                        messageHistory.setBookingId(jsonObject.getString("UniqueId"));
                                        messageHistory.setConsultCharge(jsonObject.getString("ConsultCharge"));
                                        messageHistory.setIsGuest("0");
                                        messageHistory.setUserId(App.user.getUserID());

                                        MyConstants.doctorChatList.add(messageHistory);
                                    }

                                    viewPager = view.findViewById(R.id.viewpager);
                                    ViewPagerAdapter adapter = (ViewPagerAdapter) viewPager.getAdapter();
                                    if (adapter != null) {
                                        adapter.notifyDataSetChanged();
                                    }

                                    tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
                                        @Override
                                        public void onTabSelected(TabLayout.Tab tab) {
                                            //viewPager.setCurrentItem(tab.getPosition());

                                            MyConstants.TAB_POSITION = tab.getPosition();
                                            System.out.println("tab pos::::" + MyConstants.TAB_POSITION);

                                            ViewPagerAdapter adapter = (ViewPagerAdapter) viewPager.getAdapter();
                                            if (adapter != null) {
                                                f = adapter.getItem(tab.getPosition());
                                            }

                                            try {
                                                InputMethodManager inputMethodManager = (InputMethodManager)
                                                        getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                                                if (inputMethodManager != null) {
                                                    inputMethodManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
                                                }
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                        }

                                        @Override
                                        public void onTabUnselected(TabLayout.Tab tab) {
                                            //item.collapseActionView();
                                        }

                                        @Override
                                        public void onTabReselected(TabLayout.Tab tab) {
                                        }
                                    });

                                } else {
                                    Utils.showToast(mainObj.getString("error_string"), getActivity());
                                }
                            } else if (mainObj.getString("error_code").equalsIgnoreCase("5")) {
                                MyConstants.userChatList.clear();
                                MyConstants.doctorChatList.clear();

                                viewPager = view.findViewById(R.id.viewpager);
                                ViewPagerAdapter adapter = (ViewPagerAdapter) viewPager.getAdapter();
                                if (adapter != null) {
                                    adapter.notifyDataSetChanged();
                                }

                                tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
                                    @Override
                                    public void onTabSelected(TabLayout.Tab tab) {
                                        //viewPager.setCurrentItem(tab.getPosition());

                                        MyConstants.TAB_POSITION = tab.getPosition();
                                        System.out.println("tab pos::::" + MyConstants.TAB_POSITION);

                                        ViewPagerAdapter adapter = (ViewPagerAdapter) viewPager.getAdapter();
                                        if (adapter != null) {
                                            f = adapter.getItem(tab.getPosition());
                                        }

                                        try {
                                            InputMethodManager inputMethodManager = (InputMethodManager)
                                                    getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                                            if (inputMethodManager != null) {
                                                inputMethodManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
                                            }
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }

                                    @Override
                                    public void onTabUnselected(TabLayout.Tab tab) {
                                        //item.collapseActionView();
                                    }

                                    @Override
                                    public void onTabReselected(TabLayout.Tab tab) {

                                    }
                                });
                            } else {
                                Utils.showToast(mainObj.getString("error_string"), getActivity());
                            }
                            swipe_container.setRefreshing(false);
                        } catch (JSONException e) {
                            e.printStackTrace();
                            swipe_container.setRefreshing(false);
                        }
                        break;
                }
            }
        } catch (Exception e) {
            Utils.removeSimpleSpinProgressDialog();
            e.printStackTrace();
            Utils.showToast("Please try again later", getActivity());
        }
    }

    @Override
    public void onProgressUpdate(String uniqueMessageId, int progres) {

    }

    @Override
    public void onProgressComplete(String uniqueMessageId, String result, Constant.REQUESTS request) {

    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
           /* mFragmentList.add(new UserChatFragment());
            mFragmentList.add(new DoctorChatFragment());

            mFragmentTitleList.add("User Chats");
            mFragmentTitleList.add("Doctor Chats");*/
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public int getItemPosition(Object object) {
            // POSITION_NONE makes it possible to reload the PagerAdapter
            return POSITION_NONE;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                getActivity().onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
