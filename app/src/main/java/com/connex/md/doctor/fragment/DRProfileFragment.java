package com.connex.md.doctor.fragment;


import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.crashlytics.android.Crashlytics;
import com.connex.md.R;
import com.connex.md.custom_views.CircleImageView;
import com.connex.md.custom_views.StaggeredTextGridView;
import com.connex.md.doctor.activity.DREditProfileActivity;
import com.connex.md.interfaces.AsyncTaskListner;
import com.connex.md.model.DoctorProfile;
import com.connex.md.model.SearchDoctor;
import com.connex.md.others.App;
import com.connex.md.others.Internet;
import com.connex.md.others.PicassoTrustAll;
import com.connex.md.patient.adapter.DRProfileCommentsAdapter;
import com.connex.md.patient.adapter.DRProfileScopeOfPracticeAdapter;
import com.connex.md.ws.CallRequest;
import com.connex.md.ws.Constant;
import com.connex.md.ws.MyConstants;
import com.connex.md.ws.Utils;
import com.github.paolorotolo.expandableheightlistview.ExpandableHeightListView;
import com.squareup.picasso.Callback;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import co.lujun.androidtagview.TagContainerLayout;
import io.fabric.sdk.android.Fabric;

/**
 * A simple {@link Fragment} subclass.
 */
public class DRProfileFragment extends Fragment implements AsyncTaskListner {

    public Dialog dialog;
    TextView tvAboutDoctor, tvEducation, tvMembership, tvHospitalAffiliation, tvPublications;
    LinearLayout llProfession;
    //GridLayout llScopeOfPractice;
    ExpandableHeightListView lvComments;
    DRProfileCommentsAdapter drProfileCommentsAdapter;
    StaggeredTextGridView tvScopeOfPractice;
    DRProfileScopeOfPracticeAdapter adapter;
    LinearLayout ll_about_doctor;
    ImageView ivArrow;
    LinearLayout ivBack;
    SearchDoctor searchDoctor;
    DoctorProfile doctorProfile;
    Button btnBookNow;
    CircleImageView ivDRImage;
    TextView tvDRName, tvSpeciality, tvRating, tvTotalRating, tvFee, tvWaitingTime, tvLocation;
    RatingBar ratingBar;
    ToggleButton toggleFavourite;
    View focusView;
    TextView tvAboutDoctorLabel, tvScopeOfPracticeLabel, tvEducationLabel, tvProfessionLabel, tvMembershipLabel, tvHospitalLabel, tvPublicationLabel, tvComments;
    Button btnLoginNow, btnBookAsGuest;
    LinearLayout llBookNow, llChatNow, llFavourite;
    View line;
    View line1;
    LinearLayout llEdit;
    TagContainerLayout tags;
    private SwipeRefreshLayout swipe_container;

    public DRProfileFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_dr_profile, container, false);

        Fabric.with(getActivity(), new Crashlytics());

        focusView = view.findViewById(R.id.focusView);
        focusView.requestFocus();

        tvAboutDoctorLabel = view.findViewById(R.id.tvAboutDoctorLabel);
        tvScopeOfPracticeLabel = view.findViewById(R.id.tvScopeOfPracticeLabel);
        tvEducationLabel = view.findViewById(R.id.tvEducationLabel);
        tvProfessionLabel = view.findViewById(R.id.tvProfessionLabel);
        tvMembershipLabel = view.findViewById(R.id.tvMembershipLabel);
        tvHospitalLabel = view.findViewById(R.id.tvHospitalLabel);
        tvPublicationLabel = view.findViewById(R.id.tvPublicationLabel);
        tvComments = view.findViewById(R.id.tvComments);
        line = view.findViewById(R.id.line);
        line1 = view.findViewById(R.id.line1);
        btnBookNow = view.findViewById(R.id.btnBookNow);
        ivDRImage = view.findViewById(R.id.ivDrImage);
        ratingBar = view.findViewById(R.id.ratingBar);
        toggleFavourite = view.findViewById(R.id.toggle);
        tvDRName = view.findViewById(R.id.tvDrName);
        tvSpeciality = view.findViewById(R.id.tvSpeciality);
        tvRating = view.findViewById(R.id.tvRating);
        tvTotalRating = view.findViewById(R.id.tvTotalRatings);
        tvFee = view.findViewById(R.id.tvFee);
        tvWaitingTime = view.findViewById(R.id.tvWaitingTime);
        tvLocation = view.findViewById(R.id.tvLocation);
        tvAboutDoctor = view.findViewById(R.id.tvAboutDoctor);
        tvEducation = view.findViewById(R.id.tvEducation);
        llProfession = view.findViewById(R.id.ll_profession);
        ll_about_doctor = view.findViewById(R.id.ll_about_doctor);
        llBookNow = view.findViewById(R.id.llBookNow);
        llFavourite = view.findViewById(R.id.llFavourite);
        //llScopeOfPractice = findViewById(R.id.ll_scope_of_practice);
        tvMembership = view.findViewById(R.id.tvMembership);
        tvHospitalAffiliation = view.findViewById(R.id.tvHospitalAffiliation);
        tvPublications = view.findViewById(R.id.tvPublication);
        lvComments = view.findViewById(R.id.lvComments);
        tvScopeOfPractice = view.findViewById(R.id.staggeredTextView);
        ivArrow = view.findViewById(R.id.ivArrow);
        ivBack = view.findViewById(R.id.ivBack);
        llEdit = view.findViewById(R.id.llEdit);
        tags = view.findViewById(R.id.tags);
        llChatNow = view.findViewById(R.id.llChatNow);
        swipe_container = view.findViewById(R.id.swipe_container);
        /*Bundle b = getIntent().getExtras();
        searchDoctor = (SearchDoctor) b.getSerializable("search_doctor");*/

        ivBack.setVisibility(View.GONE);

        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();
            }
        });

        toggleFavourite.setEnabled(false);
        llChatNow.setVisibility(View.GONE);
        llBookNow.setVisibility(View.GONE);
        line.setVisibility(View.GONE);
        llFavourite.setVisibility(View.GONE);

        swipe_container.setColorScheme(android.R.color.holo_blue_dark,
                android.R.color.holo_blue_light,
                android.R.color.holo_green_dark,
                android.R.color.holo_green_light);

        swipe_container.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getDRProfileData();
            }
        });

        if (MyConstants.doctorProfilePersonal != null) {
            setProfileData();
        }

        if (MyConstants.isDoctorProfileLoad) {
            getDRProfileData();
            MyConstants.isDoctorProfileLoad = false;
        }

        return view;
    }

    @Override
    public void onResume() {
        if (MyConstants.isBackPressed) {
            getDRProfileData();
            MyConstants.isBackPressed = false;
        }
        super.onResume();
    }

    private void getDRProfileData() {

        if (!Internet.isAvailable(getActivity())) {
            Internet.showAlertDialog(getActivity(), "Error!", "No Internet Connection", false);

            return;
        }

        Map<String, String> map = new HashMap<String, String>();
        map.put("url", MyConstants.BASE_URL + "doctorProfile");
        map.put("ApiToken", MyConstants.API_TOKEN);
        map.put("Version", MyConstants.WS_VERSION);
        map.put("DoctorId", App.user.getUserID());
        map.put("UserId", "");

        new CallRequest(this).getDRProfile(map);
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        Utils.removeSimpleSpinProgressDialog();
        try {
            if (result != null && !result.isEmpty()) {
                Log.i("TAG", "TAG Result : " + result);
                //      {"document":{"response":{"status":1,"message":"Success."}}}
                switch (request) {
                    case getDoctorProfile:
                        Utils.removeSimpleSpinProgressDialog();

                        List<HashMap<String, String>> professionalRotationList, scopeOfPracticeList, membershipList, educationList, hospitalAffiliationList, publicationsList, feedbackList;
                        professionalRotationList = new ArrayList<>();
                        scopeOfPracticeList = new ArrayList<>();
                        membershipList = new ArrayList<>();
                        educationList = new ArrayList<>();
                        hospitalAffiliationList = new ArrayList<>();
                        publicationsList = new ArrayList<>();
                        feedbackList = new ArrayList<>();

                        try {
                            JSONObject mainObj = new JSONObject(result);
                            if (mainObj.getString("error_code").equalsIgnoreCase("0")) {
                                if (mainObj.getJSONArray("result") != null && mainObj.getJSONArray("result").length() > 0) {
                                    JSONArray jsonArray = mainObj.getJSONArray("result");
                                    JSONObject object = jsonArray.getJSONObject(0);

                                    String speciality = "";

                                    JSONObject doctor = object.getJSONObject("doctor");

                                    // speciality
                                    JSONArray Speciality = doctor.getJSONArray("Speciality");
                                    for (int i = 0; i < Speciality.length(); i++) {
                                        JSONObject specialityObj = Speciality.getJSONObject(i);

                                        if (i == Speciality.length() - 1) {
                                            speciality += specialityObj.getString("Speciality");
                                        } else {
                                            speciality += specialityObj.getString("Speciality") + " and ";
                                        }
                                    }
                                    System.out.println("speciality:::" + speciality);

                                    // rating
                                    JSONObject Rating = doctor.getJSONObject("Rating");

                                    // scope of practice
                                    for (int i = 0; i < Speciality.length(); i++) {
                                        JSONObject specialityObj = Speciality.getJSONObject(i);

                                        HashMap<String, String> map = new HashMap<>();
                                        map.put("Speciality", specialityObj.getString("Speciality"));

                                        scopeOfPracticeList.add(map);
                                    }

                                    // education
                                    JSONArray Education = doctor.getJSONArray("Education");
                                    for (int i = 0; i < Education.length(); i++) {
                                        JSONObject educationObj = Education.getJSONObject(i);

                                        HashMap<String, String> map = new HashMap<>();
                                        map.put("id", educationObj.getString("id"));
                                        map.put("Description", educationObj.getString("Description"));
                                        map.put("StartYear", educationObj.getString("StartYear"));
                                        map.put("EndYear", educationObj.getString("EndYear"));

                                        educationList.add(map);
                                    }

                                    // professional rotation
                                    JSONArray Professional_Rotation = doctor.getJSONArray("Experience");
                                    for (int i = 0; i < Professional_Rotation.length(); i++) {
                                        JSONObject professionalObj = Professional_Rotation.getJSONObject(i);

                                        HashMap<String, String> map = new HashMap<>();
                                        map.put("id", professionalObj.getString("id"));
                                        map.put("Description", professionalObj.getString("Description"));
                                        map.put("StartYear", professionalObj.getString("StartYear"));
                                        map.put("EndYear", professionalObj.getString("EndYear"));

                                        professionalRotationList.add(map);
                                    }

                                    // membership
                                    JSONArray Membership = doctor.getJSONArray("Membership");
                                    for (int i = 0; i < Membership.length(); i++) {
                                        JSONObject membershipObj = Membership.getJSONObject(i);

                                        HashMap<String, String> map = new HashMap<>();
                                        map.put("id", membershipObj.getString("id"));
                                        map.put("Description", membershipObj.getString("Description"));
                                        map.put("StartYear", membershipObj.getString("StartYear"));
                                        map.put("EndYear", membershipObj.getString("EndYear"));

                                        membershipList.add(map);
                                    }

                                    // hospital affiliation
                                    JSONArray Affiliation = doctor.getJSONArray("Affiliation");
                                    for (int i = 0; i < Affiliation.length(); i++) {
                                        JSONObject affiliationObj = Affiliation.getJSONObject(i);

                                        HashMap<String, String> map = new HashMap<>();
                                        map.put("id", affiliationObj.getString("id"));
                                        map.put("Description", affiliationObj.getString("Description"));
                                        map.put("StartYear", affiliationObj.getString("StartYear"));
                                        map.put("EndYear", affiliationObj.getString("EndYear"));

                                        hospitalAffiliationList.add(map);
                                    }

                                    // publications
                                    JSONArray Publication = doctor.getJSONArray("Publication");
                                    for (int i = 0; i < Publication.length(); i++) {
                                        JSONObject publicationObj = Publication.getJSONObject(i);

                                        HashMap<String, String> map = new HashMap<>();
                                        map.put("id", publicationObj.getString("id"));
                                        map.put("Description", publicationObj.getString("Description"));
                                        map.put("StartYear", publicationObj.getString("StartYear"));
                                        map.put("EndYear", publicationObj.getString("EndYear"));

                                        publicationsList.add(map);
                                    }

                                    // feedback
                                    JSONArray Feedback = doctor.getJSONArray("Feedback");
                                    for (int i = 0; i < Feedback.length(); i++) {
                                        //JSONObject feedbackObj = Feedback.getJSONObject(i);
                                    }

                                    doctorProfile = new DoctorProfile();
                                    doctorProfile.setDoctorId(doctor.getString("DoctorId"));
                                    doctorProfile.setFirstName(doctor.getString("FirstName"));
                                    doctorProfile.setLastName(doctor.getString("LastName"));
                                    doctorProfile.setSpeciality(doctor.getString("DoctorSpeciality"));
                                    doctorProfile.setAbout_doctor(doctor.getString("Description"));
                                    doctorProfile.setCosultCharge(doctor.getString("CosultCharge"));
                                    doctorProfile.setCountry(doctor.getString("Country"));
                                    doctorProfile.setResponseTime(doctor.getString("ResponseTime"));
                                    doctorProfile.setProfilePic(doctor.getString("ProfilePic"));
                                    doctorProfile.setIsSaved(doctor.getString("IsSaved"));
                                    //doctorProfile.setSpeciality(speciality);
                                    doctorProfile.setRating(Rating.getString("Rating"));
                                    doctorProfile.setTotalRating(Rating.getString("TotalRating"));
                                    doctorProfile.setScope_of_practice(scopeOfPracticeList);
                                    doctorProfile.setEducation(educationList);
                                    doctorProfile.setProfessional_rotation(professionalRotationList);
                                    doctorProfile.setMembership(membershipList);
                                    doctorProfile.setHospital_affiliation(hospitalAffiliationList);
                                    doctorProfile.setPublications(publicationsList);
                                    doctorProfile.setFeedback(feedbackList);


                                    MyConstants.doctorProfilePersonal = new DoctorProfile();
                                    MyConstants.doctorProfilePersonal = doctorProfile;

                                    setProfileData();

                                    swipe_container.setRefreshing(false);

                                } else {
                                    Utils.showToast(mainObj.getString("error_string"), getActivity());
                                }
                            } else {
                                Utils.showToast(mainObj.getString("error_string"), getActivity());
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;
                }
            }
        } catch (Exception e) {
            Utils.removeSimpleSpinProgressDialog();
            e.printStackTrace();
            Utils.showToast("Please try again later", getActivity());
        }
    }

    @Override
    public void onProgressUpdate(String uniqueMessageId, int progres) {

    }

    @Override
    public void onProgressComplete(String uniqueMessageId, String result, Constant.REQUESTS request) {

    }

    private void setProfileData() {

        tvDRName.setText(MyConstants.doctorProfilePersonal.getFirstName() + " " + MyConstants.doctorProfilePersonal.getLastName());
        tvSpeciality.setText(MyConstants.doctorProfilePersonal.getSpeciality());
        tvRating.setText(MyConstants.doctorProfilePersonal.getRating());
        tvTotalRating.setText("(" + MyConstants.doctorProfilePersonal.getTotalRating() + ") Ratings");
        tvFee.setText("$" + MyConstants.doctorProfilePersonal.getCosultCharge());
        tvWaitingTime.setText(MyConstants.doctorProfilePersonal.getResponseTime());
        tvLocation.setText(MyConstants.doctorProfilePersonal.getCountry());
        ratingBar.setRating(Float.valueOf(MyConstants.doctorProfilePersonal.getRating()));

        if (MyConstants.doctorProfilePersonal.getIsSaved().equalsIgnoreCase("1")) {
            toggleFavourite.setChecked(true);
        } else {
            toggleFavourite.setChecked(false);
        }

        try {
            PicassoTrustAll.getInstance(getActivity())
                    .load(MyConstants.doctorProfilePersonal.getProfilePic())
                    .error(R.drawable.avatar)
                    .into(ivDRImage, new Callback() {
                        @Override
                        public void onSuccess() {
                        }

                        @Override
                        public void onError() {
                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }

        tvAboutDoctor.setText(MyConstants.doctorProfilePersonal.getAbout_doctor());

        if (TextUtils.isEmpty(MyConstants.doctorProfilePersonal.getAbout_doctor())) {
            tvAboutDoctorLabel.setVisibility(View.GONE);
            ll_about_doctor.setVisibility(View.GONE);
        } else {
            tvAboutDoctorLabel.setVisibility(View.VISIBLE);
            ll_about_doctor.setVisibility(View.VISIBLE);
        }

        if (tvAboutDoctor.getLineCount() > 1) {
            ivArrow.setVisibility(View.VISIBLE);
        } else {
            ivArrow.setVisibility(View.GONE);
        }

        if (MyConstants.doctorProfilePersonal.getEducation().size() == 0 && MyConstants.doctorProfilePersonal.getProfessional_rotation().size() == 0 && MyConstants.doctorProfilePersonal.getMembership().size() == 0
                && MyConstants.doctorProfilePersonal.getHospital_affiliation().size() == 0 && MyConstants.doctorProfilePersonal.getPublications().size() == 0 && MyConstants.doctorProfilePersonal.getFeedback().size() == 0) {
            tvAboutDoctor.setSingleLine(false);
            ivArrow.setVisibility(View.GONE);
        } else {
            ll_about_doctor.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (tvAboutDoctor.getLineCount() > 1) {
                        ivArrow.setVisibility(View.VISIBLE);
                        if (tvAboutDoctor.getLineCount() == 1) {
                            tvAboutDoctor.setSingleLine(false);
                            ivArrow.setImageResource(R.drawable.up_aero);

                        } else {
                            tvAboutDoctor.setSingleLine(true);
                            ivArrow.setImageResource(R.drawable.down_aero);
                        }
                    } else {
                        ivArrow.setVisibility(View.GONE);
                    }
                }
            });
        }

        if (MyConstants.doctorProfilePersonal.getEducation().size() > 0) {
            tvEducationLabel.setVisibility(View.VISIBLE);
            tvEducation.setVisibility(View.VISIBLE);
            StringBuilder educationStr = new StringBuilder();
            for (int i = 0; i < MyConstants.doctorProfilePersonal.getEducation().size(); i++) {
                if (!TextUtils.isEmpty(MyConstants.doctorProfilePersonal.getEducation().get(i).get("StartYear").trim()) && !TextUtils.isEmpty(MyConstants.doctorProfilePersonal.getEducation().get(i).get("EndYear").trim())) {
                    educationStr.append("<font color='#39c3f6'>&#62;&nbsp;&nbsp;</font>")
                            .append(MyConstants.doctorProfilePersonal.getEducation().get(i).get("Description"))
                            .append(", ")
                            .append(MyConstants.doctorProfilePersonal.getEducation().get(i).get("StartYear"))
                            .append(" - ")
                            .append(MyConstants.doctorProfilePersonal.getEducation().get(i).get("EndYear"))
                            .append("<br/>");
                } else if (TextUtils.isEmpty(MyConstants.doctorProfilePersonal.getEducation().get(i).get("EndYear").trim())) {
                    educationStr.append("<font color='#39c3f6'>&#62;&nbsp;&nbsp;</font>")
                            .append(MyConstants.doctorProfilePersonal.getEducation().get(i).get("Description"))
                            .append(", ")
                            .append(MyConstants.doctorProfilePersonal.getEducation().get(i).get("StartYear"))
                            .append("<br/>");
                } else if (TextUtils.isEmpty(MyConstants.doctorProfilePersonal.getEducation().get(i).get("StartYear").trim()) && TextUtils.isEmpty(MyConstants.doctorProfilePersonal.getEducation().get(i).get("EndYear").trim())) {
                    educationStr.append("<font color='#39c3f6'>&#62;&nbsp;&nbsp;</font>")
                            .append(MyConstants.doctorProfilePersonal.getEducation().get(i).get("Description"))
                            .append("<br/>");
                }
            }

            System.out.println("string:::" + educationStr);

            tvEducation.setText(Html.fromHtml(educationStr.toString()), TextView.BufferType.SPANNABLE);
        } else {
            tvEducation.setVisibility(View.GONE);
            tvEducationLabel.setVisibility(View.GONE);
        }

        if (MyConstants.doctorProfilePersonal.getScope_of_practice().size() > 0) {
            tvScopeOfPracticeLabel.setVisibility(View.VISIBLE);
            List<String> scopeOfPractice = new ArrayList<>();
            for (int i = 0; i < MyConstants.doctorProfilePersonal.getScope_of_practice().size(); i++) {
                scopeOfPractice.add(MyConstants.doctorProfilePersonal.getScope_of_practice().get(i).get("Speciality"));
            }
            tags.setTags(scopeOfPractice);
        } else {
            tvScopeOfPracticeLabel.setVisibility(View.GONE);
        }

        if (MyConstants.doctorProfilePersonal.getProfessional_rotation().size() > 0) {
            tvProfessionLabel.setVisibility(View.VISIBLE);
            llProfession.removeAllViews();
            for (int i = 0; i < MyConstants.doctorProfilePersonal.getProfessional_rotation().size(); i++) {
                View layout = LayoutInflater.from(getActivity()).inflate(R.layout.layout_dr_profile_profession, llProfession, false);
                TextView tvDescription = layout.findViewById(R.id.tvDescription);
                TextView tvTitle = layout.findViewById(R.id.tvTitle);
                View line = layout.findViewById(R.id.line_view);

                if (TextUtils.isEmpty(MyConstants.doctorProfilePersonal.getProfessional_rotation().get(i).get("EndYear"))) {
                    tvTitle.setText(MyConstants.doctorProfilePersonal.getProfessional_rotation().get(i).get("StartYear"));
                } else {
                    tvTitle.setText(MyConstants.doctorProfilePersonal.getProfessional_rotation().get(i).get("StartYear") + " - " + MyConstants.doctorProfilePersonal.getProfessional_rotation().get(i).get("EndYear"));
                }
                tvDescription.setText(MyConstants.doctorProfilePersonal.getProfessional_rotation().get(i).get("Description"));

                if (i == MyConstants.doctorProfilePersonal.getProfessional_rotation().size() - 1) {
                    line.setVisibility(View.GONE);
                }

                llProfession.addView(layout);
            }
        } else {
            tvProfessionLabel.setVisibility(View.GONE);
        }

        if (MyConstants.doctorProfilePersonal.getMembership().size() > 0) {
            tvMembership.setVisibility(View.VISIBLE);
            tvMembershipLabel.setVisibility(View.VISIBLE);
            StringBuilder membershipStr = new StringBuilder();
            for (int i = 0; i < MyConstants.doctorProfilePersonal.getMembership().size(); i++) {
                membershipStr.append("<font color='#39c3f6'>&#62;&nbsp;&nbsp;</font>").append(MyConstants.doctorProfilePersonal.getMembership().get(i).get("Description")).append("<br/>");
            }

            tvMembership.setText(Html.fromHtml(membershipStr.toString()), TextView.BufferType.SPANNABLE);
        } else {
            tvMembership.setVisibility(View.GONE);
            tvMembershipLabel.setVisibility(View.GONE);
            /*tvMembership.setText("N/A");*/
        }

        if (MyConstants.doctorProfilePersonal.getHospital_affiliation().size() > 0) {
            tvHospitalAffiliation.setVisibility(View.VISIBLE);
            tvHospitalLabel.setVisibility(View.VISIBLE);
            StringBuilder hospitalAffiliationStr = new StringBuilder();
            for (int i = 0; i < MyConstants.doctorProfilePersonal.getHospital_affiliation().size(); i++) {
                hospitalAffiliationStr.append("<font color='#39c3f6'>&#62;&nbsp;&nbsp;</font>").append(MyConstants.doctorProfilePersonal.getHospital_affiliation().get(i).get("Description")).append("<br/>");
            }

            tvHospitalAffiliation.setText(Html.fromHtml(hospitalAffiliationStr.toString()), TextView.BufferType.SPANNABLE);
        } else {
            tvHospitalAffiliation.setVisibility(View.GONE);
            tvHospitalLabel.setVisibility(View.GONE);
//            tvHospitalAffiliation.setText("N/A");
        }

        if (MyConstants.doctorProfilePersonal.getPublications().size() > 0) {
            tvPublications.setVisibility(View.VISIBLE);
            tvPublicationLabel.setVisibility(View.VISIBLE);
            StringBuilder publicationStr = new StringBuilder();
            for (int i = 0; i < MyConstants.doctorProfilePersonal.getPublications().size(); i++) {
                publicationStr.append("<font color='#39c3f6'>&#62;&nbsp;&nbsp;</font>").append(MyConstants.doctorProfilePersonal.getPublications().get(i).get("Description")).append("<br/>");
            }

            tvPublications.setText(Html.fromHtml(publicationStr.toString()), TextView.BufferType.SPANNABLE);
        } else {
//            tvPublications.setText("N/A");
            tvPublications.setVisibility(View.GONE);
            tvPublicationLabel.setVisibility(View.GONE);
        }

        llEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Bundle b = new Bundle();
                b.putSerializable("doctor_profile", doctorProfile);

                Intent intent = new Intent(getActivity(), DREditProfileActivity.class);
                intent.putExtras(b);
                startActivity(intent);
            }
        });

        if (MyConstants.doctorProfilePersonal.getFeedback().size() > 0) {
            tvComments.setVisibility(View.VISIBLE);
            lvComments.setVisibility(View.VISIBLE);

            List<HashMap<String, String>> commentList = new ArrayList<>();

            for (int i = 0; i < 3; i++) {
                HashMap<String, String> comment = new HashMap<>();
                comment.put("name", "Sagar Sojitra");
                comment.put("date", "Nov 14 2017");
                comment.put("comment", "A search for 'lorem ipsum' will uncover many web sites still in their infancy");

                commentList.add(comment);
            }


            drProfileCommentsAdapter = new DRProfileCommentsAdapter(getActivity(), commentList);
            lvComments.setAdapter(drProfileCommentsAdapter);

            lvComments.setExpanded(true);
        } else {
            tvComments.setVisibility(View.GONE);
            lvComments.setVisibility(View.GONE);
            line1.setVisibility(View.GONE);
        }
    }
}
