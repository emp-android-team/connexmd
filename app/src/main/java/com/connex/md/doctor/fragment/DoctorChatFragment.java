package com.connex.md.doctor.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.ImageView;

import com.connex.md.R;
import com.connex.md.doctor.adapter.MessagesAdapter;
import com.connex.md.firebase_chat.others.ChatConstants;
import com.connex.md.utils.DividerItemDecoration;
import com.connex.md.ws.MyConstants;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

/**
 * A simple {@link Fragment} subclass.
 */
public class DoctorChatFragment extends Fragment {

    public RecyclerView rvMessages;
    ImageView ivNoMessages;
    public MessagesAdapter messagesAdapter;
    public DatabaseReference fireDB;

    public DoctorChatFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_doctor_chat, container, false);

        rvMessages = view.findViewById(R.id.rvMessages);
        ivNoMessages = view.findViewById(R.id.iv_no_messages);

        fireDB = FirebaseDatabase.getInstance().getReference().child(MyConstants.DOCTOR_CHANNEL);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rvMessages.setLayoutManager(layoutManager);

        LayoutAnimationController controller = AnimationUtils.loadLayoutAnimation(rvMessages.getContext(), R.anim.layout_animation_fall_down);
        rvMessages.setLayoutAnimation(controller);
        rvMessages.scheduleLayoutAnimation();

        if (MyConstants.doctorChatList.size() > 0) {
            getUpdatedDB();
            rvMessages.setVisibility(View.VISIBLE);
            ivNoMessages.setVisibility(View.GONE);
        } else {
            ivNoMessages.setVisibility(View.VISIBLE);
            rvMessages.setVisibility(View.GONE);
        }

        return view;
    }

    public void getUpdatedDB() {

        try {
            for (int i = 0; i < MyConstants.doctorChatList.size(); i++) {
                new ValueEventsListner().setListner(i);
            }

            messagesAdapter = new MessagesAdapter(getActivity(), MyConstants.doctorChatList, "doctor");
            rvMessages.setHasFixedSize(true);
            //rvPendingPatients.addItemDecoration(new VerticalSpacingDecoration(20));
            rvMessages.addItemDecoration(new DividerItemDecoration(getActivity(), R.drawable.divider));
            rvMessages.setItemViewCacheSize(20);
            rvMessages.setDrawingCacheEnabled(true);
            rvMessages.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);

            rvMessages.setAdapter(messagesAdapter);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public class ValueEventsListner {
        public DatabaseReference dbRef;

        public void setListner(final int pos) {

            try {
                dbRef = fireDB.child(MyConstants.doctorChatList.get(pos).BookingId);
                final String uniqueId = MyConstants.doctorChatList.get(pos).BookingId;
                dbRef.child("last_message").addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if (dataSnapshot != null) {
                            System.out.println("datasnapshot::" + dataSnapshot.toString());
                            String last_message = dataSnapshot.getValue(String.class);
                            System.out.println("last message" + last_message);
                            //Log.i("Last Messages : Log : ", last_message);
                            MyConstants.doctorChatList.get(pos).lastMessage = last_message;
                            messagesAdapter.notifyDataSetChanged();
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

                if (MyConstants.doctorChatList.size() > 0) {
                    dbRef.child("last_message").addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            try {
                                if (dataSnapshot != null) {
                                    String last_message = dataSnapshot.getValue(String.class);
                                    System.out.println("last message" + last_message);
                                    //Log.i("Last Messages : Log : ", last_message);
                                    MyConstants.doctorChatList.get(pos).lastMessage = last_message;
                                    messagesAdapter.notifyDataSetChanged();
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });
                }

                dbRef.child("last_date").addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        String time = "";
                        Date inputDate;
                        if (dataSnapshot != null) {
                            String last_date = dataSnapshot.getValue(String.class);
                            System.out.println("last date::" + last_date);
                            //Log.i("Last date : Log : ", last_date);

                            // "2017-10-23T15:48:04.GMT"

                            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm a", Locale.US);
                            SimpleDateFormat df = new SimpleDateFormat("yyyy/MM/dd h:mm a", Locale.US);

                            if (last_date != null) {
                                try {
                                    format.setTimeZone(TimeZone.getTimeZone("UTC"));
                                    inputDate = format.parse(last_date);
                                    df.setTimeZone(TimeZone.getDefault());
                                    time = df.format(inputDate);
                                    System.out.println("time::" + time);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                        MyConstants.doctorChatList.get(pos).time = time;
                        messagesAdapter.notifyDataSetChanged();
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

                if (MyConstants.doctorChatList.size() > 0) {
                    dbRef.child("last_date").addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            try {
                                String time = "";
                                Date inputDate;
                                if (dataSnapshot != null) {
                                    String last_date = dataSnapshot.getValue(String.class);
                                    System.out.println("last date::" + last_date);
                                    //Log.i("Last date : Log : ", last_date);

                                    // "2017-10-23T15:48:04.GMT"

                                    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm a", Locale.US);
                                    SimpleDateFormat df = new SimpleDateFormat("yyyy/MM/dd h:mm a", Locale.US);

                                    if (last_date != null) {
                                        try {
                                            format.setTimeZone(TimeZone.getTimeZone("UTC"));
                                            inputDate = format.parse(last_date);
                                            df.setTimeZone(TimeZone.getDefault());
                                            time = df.format(inputDate);
                                            System.out.println("time::" + time);
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }
                                }
                                MyConstants.doctorChatList.get(pos).time = time;
                                messagesAdapter.notifyDataSetChanged();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });
                }

                String receiverID = "";
                receiverID = MyConstants.doctorChatList.get(pos).getDoctorId();


                dbRef.child(ChatConstants.FIRE_UNREAD_COUNTER).child(receiverID)
                        .addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                if (dataSnapshot != null) {

                                    String count = String.valueOf(dataSnapshot.getValue(Integer.class));
                                    System.out.println("count::" + count);

                                    if (count.equalsIgnoreCase("null") || count == null || TextUtils.isEmpty(count)) {
                                        MyConstants.doctorChatList.get(pos).counter = "0";
                                        /*if (MyConstants.pendingPatientCount.contains(uniqueId)) {
                                            MyConstants.pendingPatientCount.remove(uniqueId);
                                        }*/
                                    } else {
                                        MyConstants.doctorChatList.get(pos).counter = count;
                                        /*if (Integer.parseInt(count) > 0) {
                                            if (!MyConstants.pendingPatientCount.contains(uniqueId)) {
                                                MyConstants.pendingPatientCount.add(uniqueId);
                                            }
                                        }*/
                                    }
                                    //((NurseDashboardActivity)getActivity()).setTabBadge(0, String.valueOf(MyConstants.pendingPatientCount.size()));
                                    messagesAdapter.notifyDataSetChanged();
                                }
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {

                            }
                        });

                if (MyConstants.doctorChatList.size() > 0) {
                    dbRef.child(ChatConstants.FIRE_UNREAD_COUNTER).child(receiverID).
                            addValueEventListener(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    if (dataSnapshot != null) {

                                        try {
                                            String count = String.valueOf(dataSnapshot.getValue(Integer.class));
                                            System.out.println("count:: in change : " + count);

                                            System.out.println("size:***" + MyConstants.doctorChatList.size());
                                            if (count.equalsIgnoreCase("null") || count == null || TextUtils.isEmpty(count)) {
                                                MyConstants.doctorChatList.get(pos).counter = "0";
                                            } else {
                                                MyConstants.doctorChatList.get(pos).counter = count;
                                            }
                                            messagesAdapter.notifyDataSetChanged();
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }
                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {

                                }
                            });
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

}
