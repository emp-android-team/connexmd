package com.connex.md.doctor.fragment;


import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;

import com.connex.md.R;
import com.connex.md.doctor.adapter.EducationAdapter;
import com.connex.md.interfaces.AsyncTaskListner;
import com.connex.md.interfaces.DoctorListener;
import com.connex.md.others.App;
import com.connex.md.others.Internet;
import com.connex.md.ws.CallRequest;
import com.connex.md.ws.Constant;
import com.connex.md.ws.MyConstants;
import com.connex.md.ws.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 */
public class EducationFragment extends Fragment implements AsyncTaskListner, DoctorListener {

    public Button btnAddEducation;
    public RecyclerView rvEducation;
    EducationAdapter educationAdapter;
    private Dialog dialog;
    private ImageView ivClose;
    private Button btnSave;
    private EditText etUniversityName, etFrom, etTo;
    public String universityName, startYear, endYear;
    DatePickerDialog DialogDOBFrom, DialogDOBTo;
    private SimpleDateFormat dateFormatter;
    int fromYear, fromMonth, fromDay;
    int position;
    String editUniversityName, editStartYear, editEndYear;

    public EducationFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_education, container, false);

        rvEducation = view.findViewById(R.id.rvEducation);
        btnAddEducation = view.findViewById(R.id.btnAddEducation);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        layoutManager.setAutoMeasureEnabled(true);
        rvEducation.setLayoutManager(layoutManager);
        rvEducation.setNestedScrollingEnabled(false);


        educationAdapter = new EducationAdapter(EducationFragment.this, MyConstants.editDoctorProfile.getEducation(), "Education");
        //rvSearchDoctor.addItemDecoration(new VerticalSpacingDecoration(20));
        rvEducation.setItemViewCacheSize(20);
        rvEducation.setDrawingCacheEnabled(true);
        rvEducation.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        rvEducation.setHasFixedSize(false);

        rvEducation.setAdapter(educationAdapter);

        btnAddEducation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openAddDialog();
            }
        });

        return view;
    }

    private void openAddDialog() {
        createDialog();
        initDialogComponents();

        dateFormatter = new SimpleDateFormat("yyyy", Locale.US);
        Calendar newCalendar = Calendar.getInstance();
        DialogDOBFrom = new DatePickerDialog(getActivity(), R.style.DialogTheme, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                etFrom.setText(dateFormatter.format(newDate.getTime()));
                String from = dateFormatter.format(newDate.getTime());
                System.out.println("date of birth" + from);
                fromYear = year;
                fromMonth = monthOfYear;
                fromDay = dayOfMonth;

                etTo.setEnabled(true);
                //etTo.setFocusableInTouchMode(true);
                etTo.setCursorVisible(true);

                Calendar c = Calendar.getInstance();
                c.set(Calendar.MONTH, fromMonth);
                c.set(Calendar.DAY_OF_MONTH, fromDay);
                c.set(Calendar.YEAR, fromYear);
                DialogDOBTo.getDatePicker().setMinDate(c.getTimeInMillis());
                DialogDOBTo.getDatePicker().setMaxDate(System.currentTimeMillis());
            }

        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
        DialogDOBFrom.getDatePicker().setMaxDate(System.currentTimeMillis());

        DialogDOBTo = new DatePickerDialog(getActivity(), R.style.DialogTheme, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                etTo.setText(dateFormatter.format(newDate.getTime()));
                String to = dateFormatter.format(newDate.getTime());
                System.out.println("date of birth" + to);
            }

        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));


        etFrom.setInputType(InputType.TYPE_NULL);
        etFrom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                etFrom.requestFocus();
                try {
                    InputMethodManager inputMethodManager = (InputMethodManager)
                            getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    if (inputMethodManager != null) {
                        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
                    }
                } catch (Exception e){
                    e.printStackTrace();
                }
                DialogDOBFrom.show();
            }
        });

        etTo.setInputType(InputType.TYPE_NULL);
        etTo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                etTo.requestFocus();
                try {
                    InputMethodManager inputMethodManager = (InputMethodManager)
                            getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    if (inputMethodManager != null) {
                        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
                    }
                } catch (Exception e){
                    e.printStackTrace();
                }
                DialogDOBTo.show();
            }
        });

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                universityName = etUniversityName.getText().toString().trim();
                startYear = etFrom.getText().toString().trim();
                endYear = etTo.getText().toString().trim();

                if (TextUtils.isEmpty(universityName)) {
                    etUniversityName.requestFocus();
                    etUniversityName.setError("University name is empty");
                    return;
                } else if (TextUtils.isEmpty(startYear)) {
                    etFrom.requestFocus();
                    etFrom.setError("Year is empty");
                    return;
                }

                dialog.dismiss();

                saveDetails();
            }
        });

        ivClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
    }

    private void saveDetails() {

        if (!Internet.isAvailable(getActivity())) {
            Internet.showAlertDialog(getActivity(), "Error!", "No Internet Connection", false);

            return;
        }

        Map<String, String> map = new HashMap<String, String>();
        map.put("url", MyConstants.DOCTOR_BASE_URL + "doctorProfileUpdate");
        map.put("ApiToken", MyConstants.API_TOKEN);
        map.put("Version", MyConstants.WS_VERSION);
        map.put("DoctorId", App.user.getUserID());
        map.put("Type", "Education");
        map.put("IsEdit", "0");
        map.put("Description", universityName);
        map.put("StartYear", startYear);
        map.put("EndYear", endYear);
        map.put("EditId", "");

        new CallRequest(this).saveDoctorDetails(map);

    }

    private void createDialog() {
        dialog = new Dialog(getActivity(), R.style.CustomDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        // Include dialog.xml file
        dialog.setContentView(R.layout.dialog_edit_doctor);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
        dialog.show();
    }

    private void initDialogComponents() {
        btnSave = dialog.findViewById(R.id.btnSave);
        ivClose = dialog.findViewById(R.id.ivClose);
        etUniversityName = dialog.findViewById(R.id.etUniversityName);
        etFrom = dialog.findViewById(R.id.etFrom);
        etTo = dialog.findViewById(R.id.etTo);

        etTo.setEnabled(false);
        etTo.setFocusable(false);
        etTo.setCursorVisible(false);
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        Utils.removeSimpleSpinProgressDialog();
        try {
            if (result != null && !result.isEmpty()) {
                Log.i("TAG", "TAG Result : " + result);
                //      {"document":{"response":{"status":1,"message":"Success."}}}
                switch (request) {
                    case saveDoctorDetails:
                        Utils.removeSimpleSpinProgressDialog();

                        try {
                            JSONObject mainObj = new JSONObject(result);
                            if (mainObj.getString("error_code").equalsIgnoreCase("0")) {

                                JSONObject resultObj = mainObj.getJSONArray("result").getJSONObject(0);

                                HashMap<String, String> map = new HashMap<>();
                                map.put("id", resultObj.getString("id"));
                                map.put("Description", universityName);
                                map.put("StartYear", startYear);
                                map.put("EndYear", endYear);

                                MyConstants.editDoctorProfile.getEducation().add(map);

                                educationAdapter.notifyDataSetChanged();

                                Utils.showToast("Profile saved successfully", getActivity());
                            } else {
                                Utils.showToast(mainObj.getString("error_string"), getActivity());
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;

                    case editDoctorDetails:
                        Utils.removeSimpleSpinProgressDialog();

                        try {
                            JSONObject mainObj = new JSONObject(result);
                            if (mainObj.getString("error_code").equalsIgnoreCase("0")) {


                                MyConstants.editDoctorProfile.getEducation().get(position).put("Description", editUniversityName);
                                MyConstants.editDoctorProfile.getEducation().get(position).put("StartYear", editStartYear);
                                MyConstants.editDoctorProfile.getEducation().get(position).put("EndYear", editEndYear);

                                educationAdapter.notifyDataSetChanged();

                                Utils.showToast("Profile saved successfully", getActivity());
                            } else {
                                Utils.showToast(mainObj.getString("error_string"), getActivity());
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;
                }
            }
        } catch (Exception e) {
            Utils.removeSimpleSpinProgressDialog();
            e.printStackTrace();
            Utils.showToast("Please try again later", getActivity());
        }
    }

    @Override
    public void onProgressUpdate(String uniqueMessageId, int progres) {

    }

    @Override
    public void onProgressComplete(String uniqueMessageId, String result, Constant.REQUESTS request) {

    }

    @Override
    public void editDetails(int position, String universityName, String startYear, String endYear) {

        if (!Internet.isAvailable(getActivity())) {
            Internet.showAlertDialog(getActivity(), "Error!", "No Internet Connection", false);

            return;
        }

        this.position = position;
        editUniversityName = universityName;
        editStartYear = startYear;
        editEndYear = endYear;

        Map<String, String> map = new HashMap<String, String>();
        map.put("url", MyConstants.DOCTOR_BASE_URL + "doctorProfileUpdate");
        map.put("ApiToken", MyConstants.API_TOKEN);
        map.put("Version", MyConstants.WS_VERSION);
        map.put("DoctorId", App.user.getUserID());
        map.put("Type", "Education");
        map.put("IsEdit", "1");
        map.put("Description", editUniversityName);
        map.put("StartYear", editStartYear);
        map.put("EndYear", editEndYear);
        map.put("EditId", MyConstants.editDoctorProfile.getEducation().get(position).get("id"));

        new CallRequest(this).editDoctorDetails(map);
    }
}
