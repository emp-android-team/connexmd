package com.connex.md.doctor.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.connex.md.R;
import com.connex.md.custom_views.MultiSelectionSpinner;
import com.connex.md.custom_views.StaggeredTextGridView;

/**
 * A simple {@link Fragment} subclass.
 */
public class ScopeFragment extends Fragment {

    MultiSelectionSpinner multiSelectionSpinner;
    StaggeredTextGridView staggeredTextGridView;

    public ScopeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_scope, container, false);

        return view;
    }

}
