package com.connex.md.firebase_chat.activity;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.connex.md.R;
import com.connex.md.interfaces.AsyncTaskListner;
import com.connex.md.others.App;
import com.connex.md.others.Internet;
import com.connex.md.ws.CallRequest;
import com.connex.md.ws.Constant;
import com.connex.md.ws.MyConstants;
import com.connex.md.ws.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import io.fabric.sdk.android.Fabric;

public class AddNoteActivity extends AppCompatActivity implements AsyncTaskListner{

    private EditText etDescription;
    private LinearLayout llUploadAttachment;
    private LinearLayout ivBack, llSave;
    private ImageView ivImage, ivClose;
    private RelativeLayout rlImage;
    private String filePath = "";
    private String filePathFirst = "";
    private boolean withImage = false;
    private static int RESULT_LOAD_IMG_DP = 1;
    private static int RESULT_CROP_DP = 3;
    private String uniqueChatId = "", fromChat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_add_note);

        etDescription = findViewById(R.id.etDescription);
        llUploadAttachment = findViewById(R.id.ll_upload_attachment);
        ivClose = findViewById(R.id.ivClose);
        ivImage = findViewById(R.id.ivImage);
        rlImage = findViewById(R.id.rlImage);
        ivBack = findViewById(R.id.ivBack);
        llSave = findViewById(R.id.llSave);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            uniqueChatId = bundle.getString(MyConstants.UNIQUE_CHAT_ID);
            fromChat = bundle.getString("fromChat");
        }

        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        llUploadAttachment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Utils.checkPermission(AddNoteActivity.this)) {
                    loadImagefromGalleryForPageDp();
                }
            }
        });

        ivClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                filePath = "";
                rlImage.setVisibility(View.GONE);
            }
        });

        llSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String description = etDescription.getText().toString().trim();

                if (TextUtils.isEmpty(filePath) && TextUtils.isEmpty(description)) {
                    return;
                }

                addNote(description);
            }
        });

    }

    private void addNote(String description) {

        if (TextUtils.isEmpty(description)) {
            description = "";
        }

        if (!Internet.isAvailable(AddNoteActivity.this)) {
            Internet.showAlertDialog(AddNoteActivity.this, "Error!", "No Internet Connection", false);

            return;
        }

        String urlStr = "userChatNote";

        Map<String, String> map = new HashMap<String, String>();
        if (App.user.getUser_Type().equalsIgnoreCase(MyConstants.USER_PT)) {
            if (MyConstants.isGuest.equalsIgnoreCase("0")) {
                map.put("url", MyConstants.BASE_URL + urlStr);
                map.put("UserId", App.user.getUserID());
            } else {
                map.put("url", MyConstants.GUEST_BASE_URL + urlStr);
                map.put("GuestId", App.user.getUserID());
            }
        } else {
            map.put("url", MyConstants.DOCTOR_BASE_URL + urlStr);
            map.put("DoctorId", App.user.getUserID());
        }
        map.put("ApiToken", MyConstants.API_TOKEN);
        map.put("Version", MyConstants.WS_VERSION);
        map.put("UniqueId", uniqueChatId);
        map.put("SenderId", App.user.getUserID());
        map.put("Text", description);
        if (!TextUtils.isEmpty(filePath)) {
            map.put("Image", filePath);
        }

        new CallRequest(AddNoteActivity.this).addNote(map);
    }

    public void loadImagefromGalleryForPageDp() {
        // Create intent to Open Image applications like Gallery, Google Photos
        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        // Start the Intent
        startActivityForResult(galleryIntent, RESULT_LOAD_IMG_DP);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        try {
            // When an Image is picked
            if (requestCode == RESULT_LOAD_IMG_DP && resultCode == RESULT_OK
                    && null != data) {
                // Get the Image from data

                Uri selectedImage = data.getData();
                String[] filePathColumn = {MediaStore.Images.Media.DATA};

                // Get the cursor
                Cursor cursor = getContentResolver().query(selectedImage,
                        filePathColumn, null, null, null);
                // Move to first row
                int columnIndex = 0;
                if (cursor != null) {
                    cursor.moveToFirst();
                    columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    filePathFirst = cursor.getString(columnIndex);
                    System.out.println(filePathFirst);
                    cursor.close();
                }

                doCropDP(filePathFirst);

            }

            if (requestCode == RESULT_CROP_DP) {
                if (resultCode == Activity.RESULT_OK) {
                    if (!TextUtils.isEmpty(filePath)) {

                        System.out.println("path***" + filePath);
                        Bitmap selectedBitmap = decodeSampledBitmapFromPath(filePath, 200, 200);
                        System.out.println("selectedBitmap:::::" + selectedBitmap);

                        // Set The Bitmap Data To ImageView

                        ivImage.setImageBitmap(selectedBitmap);
                        ivImage.setScaleType(ImageView.ScaleType.FIT_XY);
                        rlImage.setVisibility(View.VISIBLE);

                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static Bitmap decodeSampledBitmapFromPath(String path, int reqWidth,
                                                     int reqHeight) {

        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, options);

        options.inSampleSize = calculateInSampleSize(options, reqWidth,
                reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        Bitmap bmp = BitmapFactory.decodeFile(path, options);
        return bmp;
    }

    public static int calculateInSampleSize(BitmapFactory.Options options,
                                            int reqWidth, int reqHeight) {

        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            if (width > height) {
                inSampleSize = Math.round((float) height / (float) reqHeight);
            } else {
                inSampleSize = Math.round((float) width / (float) reqWidth);
            }
        }
        return inSampleSize;
    }

    private void doCropDP(String picPath) {
        try {

            Intent cropIntent = new Intent("com.android.camera.action.CROP");

            File f = new File(picPath);
            Uri contentUri = Uri.fromFile(f);

            cropIntent.setDataAndType(contentUri, "image/*");

            cropIntent.putExtra("crop", "true");
            // indicate aspect of desired crop
            cropIntent.putExtra("aspectX", 1);
            cropIntent.putExtra("aspectY", 1);
            // indicate output X and Y
            cropIntent.putExtra("outputX", 256);
            cropIntent.putExtra("outputY", 256);
            // retrieve data on return
            cropIntent.putExtra("return-data", false);

            File sdCardDirectory = new File(Environment.getExternalStorageDirectory().getPath() + "/ConnexMd/profile_pictures");

            if (!sdCardDirectory.exists()) {
                sdCardDirectory.mkdirs();
            }

            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.US)
                    .format(new Date());

            String nw = "Profile_" + timeStamp + ".jpeg";

            File image = new File(sdCardDirectory, nw);

            //uploadPathDp = sdCardDirectory + "/" + nw;
            filePath = new File(sdCardDirectory, nw).getAbsolutePath();
            System.out.println("UploadPath:::" + filePath);

            try {
                image.createNewFile();
            } catch (IOException ex) {
                Log.e("io", ex.getMessage());
            }

            Uri uri;
            uri = Uri.fromFile(image);

            cropIntent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
            // start the activity - we handle returning in onActivityResult
            startActivityForResult(cropIntent, RESULT_CROP_DP);
        } catch (ActivityNotFoundException anfe) {
            String errorMessage = "your device doesn't support the crop action!";
            Toast toast = Toast.makeText(AddNoteActivity.this, errorMessage, Toast.LENGTH_SHORT);
            toast.show();
        }
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        Utils.removeSimpleSpinProgressDialog();
        try {
            if (result != null && !result.isEmpty()) {
                Log.i("TAG", "TAG Result : " + result);
                switch (request) {
                    case addNote:
                        Utils.removeSimpleSpinProgressDialog();
                        try {
                            JSONObject mainObj = new JSONObject(result);
                            if (mainObj.getString("error_code").equalsIgnoreCase("0")) {
                                if (fromChat.equalsIgnoreCase("1")) {
                                    Intent intent = new Intent(AddNoteActivity.this, DisplayNotesActivity.class);
                                    intent.putExtra(MyConstants.UNIQUE_CHAT_ID, uniqueChatId);
                                    startActivity(intent);
                                    finish();
                                } else {
                                    onBackPressed();
                                    MyConstants.isBackPressed = true;
                                }
                            } else {
                                Utils.showToast(mainObj.getString("error_string"), AddNoteActivity.this);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;
                }
            }
        } catch (Exception e) {
            Utils.removeSimpleSpinProgressDialog();
            e.printStackTrace();
            Utils.showToast("Please try again later", AddNoteActivity.this);
        }
    }

    @Override
    public void onProgressUpdate(String uniqueMessageId, int progres) {

    }

    @Override
    public void onProgressComplete(String uniqueMessageId, String result, Constant.REQUESTS request) {

    }
}
