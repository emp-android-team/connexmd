package com.connex.md.firebase_chat.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.crashlytics.android.Crashlytics;
import com.connex.md.R;
import com.connex.md.firebase_chat.adapter.DisplayNotesAdapter;
import com.connex.md.interfaces.AsyncTaskListner;
import com.connex.md.model.Notes;
import com.connex.md.others.App;
import com.connex.md.others.Internet;
import com.connex.md.utils.VerticalSpacingDecoration;
import com.connex.md.ws.CallRequest;
import com.connex.md.ws.Constant;
import com.connex.md.ws.JsonParserUniversal;
import com.connex.md.ws.MyConstants;
import com.connex.md.ws.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.fabric.sdk.android.Fabric;

public class DisplayNotesActivity extends AppCompatActivity implements AsyncTaskListner {

    private LinearLayout ivBack, llAdd;
    private RecyclerView rvNotes;
    private String uniqueChatId = "";
    public List<Notes> notesList = new ArrayList<>();
    public JsonParserUniversal jsonParserUniversal;
    private DisplayNotesAdapter displayNotesAdapter;
    public ImageView ivNoNotes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_display_notes);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            uniqueChatId = bundle.getString(MyConstants.UNIQUE_CHAT_ID);
        }

        ivBack = findViewById(R.id.ivBack);
        llAdd = findViewById(R.id.llAdd);
        rvNotes = findViewById(R.id.rvNotes);
        ivNoNotes = findViewById(R.id.iv_no_notes);

        jsonParserUniversal = new JsonParserUniversal();

        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        llAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(DisplayNotesActivity.this, AddNoteActivity.class);
                intent.putExtra(MyConstants.UNIQUE_CHAT_ID, uniqueChatId);
                intent.putExtra("fromChat", "0");
                startActivity(intent);
            }
        });

        getNotesList();

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(DisplayNotesActivity.this, LinearLayoutManager.VERTICAL, false);
        rvNotes.setLayoutManager(linearLayoutManager);


        rvNotes.setHasFixedSize(true);
        rvNotes.addItemDecoration(new VerticalSpacingDecoration(20));
        rvNotes.setItemViewCacheSize(20);
        rvNotes.setDrawingCacheEnabled(true);
        rvNotes.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);

    }

    private void getNotesList() {
        if (!Internet.isAvailable(DisplayNotesActivity.this)) {
            Internet.showAlertDialog(DisplayNotesActivity.this, "Error!", "No Internet Connection", false);

            return;
        }

        String urlStr = "userChatNoteList";

        Map<String, String> map = new HashMap<String, String>();
        if (App.user.getUser_Type().equalsIgnoreCase(MyConstants.USER_PT)) {
            if (MyConstants.isGuest.equalsIgnoreCase("0")) {
                map.put("url", MyConstants.BASE_URL + urlStr);
                map.put("UserId", App.user.getUserID());
            } else {
                map.put("url", MyConstants.GUEST_BASE_URL + urlStr);
                map.put("GuestId", App.user.getUserID());
            }
        } else {
            map.put("url", MyConstants.DOCTOR_BASE_URL + urlStr);
            map.put("DoctorId", App.user.getUserID());
        }
        map.put("ApiToken", MyConstants.API_TOKEN);
        map.put("Version", MyConstants.WS_VERSION);
        map.put("UniqueId", uniqueChatId);

        new CallRequest(DisplayNotesActivity.this).displayNotes(map);
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        Utils.removeSimpleSpinProgressDialog();
        try {
            if (result != null && !result.isEmpty()) {
                Log.i("TAG", "TAG Result : " + result);
                switch (request) {
                    case displayNote:
                        Utils.removeSimpleSpinProgressDialog();
                        notesList.clear();
                        try {
                            JSONObject mainObj = new JSONObject(result);
                            if (mainObj.getString("error_code").equalsIgnoreCase("0")) {
                                if (mainObj.getJSONArray("result") != null && mainObj.getJSONArray("result").length() > 0) {
                                    JSONArray jsonArray = mainObj.getJSONArray("result");
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject object = jsonArray.getJSONObject(i);

                                        Notes notes = (Notes) jsonParserUniversal.parseJson(object, new Notes());
                                        notesList.add(notes);
                                    }

                                    displayNotesAdapter = new DisplayNotesAdapter(DisplayNotesActivity.this, notesList);
                                    rvNotes.setAdapter(displayNotesAdapter);
                                    ivNoNotes.setVisibility(View.GONE);
                                    rvNotes.setVisibility(View.VISIBLE);
                                } else {
                                    Utils.showToast(mainObj.getString("error_string"), DisplayNotesActivity.this);
                                }
                            } else if (mainObj.getString("error_code").equalsIgnoreCase("7")) {
                                notesList.clear();
                                System.out.println("size:::"+notesList.size());
                                if (notesList.size() > 0) {
                                    ivNoNotes.setVisibility(View.GONE);
                                    rvNotes.setVisibility(View.VISIBLE);
                                } else {
                                    ivNoNotes.setVisibility(View.VISIBLE);
                                    rvNotes.setVisibility(View.GONE);
                                }
                            } else {
                                Utils.showToast(mainObj.getString("error_string"), DisplayNotesActivity.this);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;
                }
            }
        } catch (Exception e) {
            Utils.removeSimpleSpinProgressDialog();
            e.printStackTrace();
            Utils.showToast("Please try again later", DisplayNotesActivity.this);
        }
    }

    @Override
    public void onProgressUpdate(String uniqueMessageId, int progres) {

    }

    @Override
    public void onProgressComplete(String uniqueMessageId, String result, Constant.REQUESTS request) {

    }

    @Override
    protected void onResume() {
        if (MyConstants.isBackPressed) {
            getNotesList();
            MyConstants.isBackPressed = false;
        }
        super.onResume();
    }
}
