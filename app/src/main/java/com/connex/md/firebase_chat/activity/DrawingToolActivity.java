package com.connex.md.firebase_chat.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.connex.md.R;
import com.connex.md.custom_views.CustomView;
import com.connex.md.utils.SaveViewUtil;
import com.connex.md.ws.MyConstants;

public class DrawingToolActivity extends AppCompatActivity {

    CustomView customView;
    Button btnSave, btnDelete;
    public static final int DRAW_PICTURE = 666;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drawing_tool);

        customView = findViewById(R.id.custom_view);
        btnSave = findViewById(R.id.btnSave);
        btnDelete = findViewById(R.id.btnDelete);

        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                customView.clearDrawing();
            }
        });

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(SaveViewUtil.saveScreen(customView)){
                    Toast.makeText(DrawingToolActivity.this, "Save drawing succeed!", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent();
                    intent.putExtra("filepath", MyConstants.DRAWING_PATH);
                    setResult(DRAW_PICTURE,intent);
                    finish();
                }else{
                    Toast.makeText(DrawingToolActivity.this, "Save drawing fail. Please check your SD card", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
