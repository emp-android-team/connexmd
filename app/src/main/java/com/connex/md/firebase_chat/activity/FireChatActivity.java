package com.connex.md.firebase_chat.activity;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.crashlytics.android.Crashlytics;
import com.connex.md.R;
import com.connex.md.audio_video_calling.AudioCallActivity;
import com.connex.md.audio_video_calling.BaseActivity;
import com.connex.md.audio_video_calling.CallScreenActivity;
import com.connex.md.audio_video_calling.IncomingCallScreenActivity;
import com.connex.md.audio_video_calling.SinchService;
import com.connex.md.custom_views.RobottoTextView;
import com.connex.md.firebase_chat.adapter.ChatAdapter;
import com.connex.md.firebase_chat.model.FireMessage;
import com.connex.md.firebase_chat.others.ChatConstants;
import com.connex.md.firebase_chat.service.MyDownloadService;
import com.connex.md.firebase_chat.service.MyUploadService;
import com.connex.md.interfaces.AsyncTaskListner;
import com.connex.md.interfaces.ChatListener;
import com.connex.md.others.App;
import com.connex.md.others.Internet;
import com.connex.md.others.PicassoTrustAll;
import com.connex.md.patient.activity.ConfirmPaymentActivity;
import com.connex.md.patient.activity.PTDashboardActivity;
import com.connex.md.utils.MyLayoutManager;
import com.connex.md.ws.CallRequest;
import com.connex.md.ws.Constant;
import com.connex.md.ws.MyConstants;
import com.connex.md.ws.Utils;
import com.github.siyamed.shapeimageview.CircularImageView;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseException;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.sinch.android.rtc.SinchError;
import com.sinch.android.rtc.calling.Call;
import com.sinch.android.rtc.calling.CallClient;
import com.squareup.picasso.Callback;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

import io.fabric.sdk.android.Fabric;

import static com.connex.md.ws.MyConstants.MEDIA_RECORD_ERROR;
import static com.connex.md.ws.MyConstants.CALL_ID;
import static com.connex.md.ws.MyConstants.VIDEO_CALL_REQUEST_CODE;
import static com.connex.md.ws.MyConstants.VOICE_CALL_REQUEST_CODE;

public class FireChatActivity extends BaseActivity implements SinchService.StartFailedListener, SinchService.IncomingCallLisnter, AsyncTaskListner, ChatListener {


    public static final int PiCK_IMAGE = 111;
    public static final int PiCK_AUDIO = 222;
    public static final int PICK_VIDEO = 333;
    public static final int TAKE_PICTURE = 444;
    public static final int TAKE_VIDEO = 555;
    public static final int DRAW_PICTURE = 666;
    public static final int DOCTOR_SUGGEST = 777;

    private static final String AUDIO_RECORDER_FOLDER = "ConnexMd/audio/";
    private static final String AUDIO_RECORDER_FILE_EXT_3GP = ".m4a";
    public static String[] thumbColumns = {MediaStore.Video.Thumbnails.DATA};
    public static String[] mediaColumns = {MediaStore.Video.Media._ID};
    public static String file;
    private static Activity staticContext;
    public final String IMG_TAG = "imageTag";
    public final String VIDEO_TAG = "videoTag";
    public final String AUDIO_TAG = "audioTag";
    public final String TAG = "tag";
    public ImageView imgSend, imgMic, imgCamera;
    public EditText etChat;
    public RobottoTextView tvTimer;
    public ChatAdapter cAdapter;
    public RecyclerView recycleChatView;
    public RelativeLayout relAudioPanel, relChatPanel;
    public String imgDecodableString;
    public boolean isStart = false;
    public MediaRecorder myAudioRecorder;
    public Handler mHandler, mStatusHandler;
    public Runnable mAudioRunnable, mStatusRunnable;
    public byte[] bytes;
    public Uri selectedUri;
    public String FileType = "", FileName = "", booking_id = "", pt_id = "", pt_name = "", dr_name = "", dr_id = "", uniqueChatId = "";
    public boolean isLive = true;
    public Constant.REQUESTS request;
    public Map<String, String> map;
    public String selectedType = "";
    public long timer = 1;
    public LinearLayout img_back2;
    public ImageView img_audio, img_video, img_end_chat;
    public App app;
    public RelativeLayout relChat;
    public DatabaseReference fireDB, fireDbMessages, fireDbTyping, fireDbOnlineOffline, fireChaneel, fireUnreadCounter;
    public LinearLayout relCamera;
    public View greenView;
    public View greyView;
    public CircularImageView imgRecvr;
    public int drId = 0, ptId = 0;
    public RobottoTextView tvOppName, tvTyping;
    public LinearLayout imgBack;
    public Uri mFileUri;
    public FireMessage chatMessage;
    public String senderId = "";
    public String reciverId = "";
    public String str_name = "";
    public String myName = "";
    public String myPic = "";
    public String recvName = "";
    public SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm a", Locale.US);
    public boolean isPaymentDone;
    public SharedPreferences sharedpreferences;
    public int unreadMessageCount = 0;
    public Toolbar toolbar;
    public String IS_GUEST;
    public MenuItem itemEndChat, itemRateDoctor, itemSuggestDoctor, itemAddNote, itemNotes;
    public FireMessage chatBean;
    public boolean isCall = true;
    public boolean isCallCamera = true;
    public boolean retryAudio = true;
    public String uploadUniqueID;
    public ArrayList<FireMessage> fireChatArray = new ArrayList<>();

    public BroadcastReceiver mImageBroadCastreciver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {


            String type = intent.getStringExtra(ChatConstants.FILE_TYPE);

            switch (intent.getAction()) {
                case MyDownloadService.DOWNLOAD_COMPLETED:
                    // Get number of bytes downloaded
                    String downloadPath = intent.getStringExtra(MyDownloadService.EXTRA_DOWNLOAD_PATH);
                    String savePath = intent.getStringExtra(ChatConstants.SAVE_PATH);

                    for (FireMessage f : fireChatArray) {
                        if (f.photoURL.equalsIgnoreCase(downloadPath) || f.videoURL.equalsIgnoreCase(downloadPath) || f.voiceURL.equalsIgnoreCase(downloadPath)) {
                            final File downloadedFile = new File(savePath);
                            if (downloadedFile.exists()) {
                                f.localPath = savePath;
                                f.isSent = true;
                            }
                        }
                    }
                    notifyChatAdapter();

                    break;
                case MyDownloadService.DOWNLOAD_ERROR:
                    downloadPath = intent.getStringExtra(MyDownloadService.EXTRA_DOWNLOAD_PATH);

                    for (FireMessage f : fireChatArray) {
                        if (f.photoURL.equalsIgnoreCase(downloadPath) || f.videoURL.equalsIgnoreCase(downloadPath) || f.voiceURL.equalsIgnoreCase(downloadPath)) {
                            f.isError = true;
                        }
                    }

                    notifyChatAdapter();

                    break;
                case MyUploadService.UPLOAD_COMPLETED:


                    uploadUniqueID = intent.getStringExtra(ChatConstants.UNIQUE_ID);
                    Uri fileUri = intent.getParcelableExtra(MyUploadService.EXTRA_FILE_URI);
                    String donwnloadURL = intent.getStringExtra(MyUploadService.EXTRA_DOWNLOAD_URL);

                    type = intent.getStringExtra(ChatConstants.FILE_TYPE);
                    for (FireMessage f : fireChatArray) {
                        if (f.chatID.equalsIgnoreCase(uploadUniqueID)) {
                            f.isSent = true;

                            switch (type) {
                                case "audio":
                                    if (donwnloadURL.contains(".mp3")) {
                                        donwnloadURL = donwnloadURL.replace(".mp3", "");
                                    }
                                    f.voiceURL = donwnloadURL.toString();
                                    sendAudioMessage(f);
                                    break;
                                case "video":
                                    f.videoURL = donwnloadURL.toString();
                                    sendVideoMessage(f);
                                    break;
                                case "image":
                                    Log.i(IMG_TAG, "Image is going to send message :" + donwnloadURL.toString());
                                    f.photoURL = donwnloadURL.toString();
                                    sendImageMessage(f);
                                    break;
                            }
                        }
                    }

                    notifyChatAdapter();
                    break;
                case MyUploadService.UPLOAD_ERROR:

                    Utils.removeSimpleSpinProgressDialog();

                    for (FireMessage f : fireChatArray) {
                        if (f.chatID.equalsIgnoreCase(uploadUniqueID)) {
                            f.isError = true;
                        }
                    }
                    notifyChatAdapter();
                    break;
                case MyUploadService.TRANSFERING:
                    int percentage = intent.getIntExtra(ChatConstants.PERCENTAGE, 0);
                    for (FireMessage f : fireChatArray) {
                        if (f.chatID.equalsIgnoreCase(uploadUniqueID)) {
                            f.percentage = percentage;
                        }
                    }
                    notifyChatAdapter();
                    break;
            }
        }
    };
    String senderImageUrl, recvImageUrl;
    private FirebaseAuth mAuth;
    private MediaRecorder.OnErrorListener errorListener = new MediaRecorder.OnErrorListener() {
        @Override
        public void onError(MediaRecorder mr, int what, int extra) {
            System.out.println("what::" + what + "extra::::" + extra);
        }

    };
    private MediaRecorder.OnInfoListener infoListener = new MediaRecorder.OnInfoListener() {
        @Override
        public void onInfo(MediaRecorder mr, int what, int extra) {
            System.out.println("what::" + what + "extra::::" + extra);
        }
    };


    public static long getFileId(Activity context, Uri fileUri) {

        Cursor cursor = context.managedQuery(fileUri, mediaColumns, null, null,
                null);

        if (cursor.moveToFirst()) {
            int columnIndex = cursor
                    .getColumnIndexOrThrow(MediaStore.Video.Media._ID);
            int id = cursor.getInt(columnIndex);

            return id;
        }

        return 0;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
     /*   getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);*/
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_chat_new);
        Utils.logUser();

        staticContext = FireChatActivity.this;

        app = App.getInstance();
        //   mRecorder = WavAudioRecorder.getInstanse();
        sharedpreferences = getSharedPreferences(MyConstants.PREF, Context.MODE_PRIVATE);
        if (getIntent().hasExtra(MyConstants.IS_PAYMENT_DONE)) {
            isPaymentDone = getIntent().getBooleanExtra(MyConstants.IS_PAYMENT_DONE, false);
        }

        recvImageUrl = getIntent().getStringExtra(MyConstants.RECEIVER_IMAGE_URL);
        pt_id = getIntent().getStringExtra(MyConstants.PT_ID);
        dr_id = getIntent().getStringExtra(MyConstants.DR_ID);
        dr_name = getIntent().getStringExtra(MyConstants.DR_NAME);
        pt_name = getIntent().getStringExtra(MyConstants.Pt_NAME);
        uniqueChatId = getIntent().getStringExtra(MyConstants.UNIQUE_CHAT_ID);
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            if (bundle.containsKey("IS_GUEST")) {
                IS_GUEST = bundle.getString("IS_GUEST");
            }
        }

        tvOppName = findViewById(R.id.tvOppName);
        tvTyping = findViewById(R.id.tvTyping);
        //img_end_chat = findViewById(R.id.img_end_chat);
        etChat = findViewById(R.id.etChat);
        recycleChatView = findViewById(R.id.lstChat);
        relChat = findViewById(R.id.relChat);
        imgBack = findViewById(R.id.img_left_arrow);
        imgSend = findViewById(R.id.imgSend);
        imgCamera = findViewById(R.id.imgCamera);
        img_back2 = findViewById(R.id.img_left_arrow);
        img_audio = findViewById(R.id.img_audio);
        img_video = findViewById(R.id.img_video);
        imgMic = findViewById(R.id.imgMic);
        tvTimer = findViewById(R.id.tvTimer);
        relAudioPanel = findViewById(R.id.relAudioPanel);
        relChatPanel = findViewById(R.id.relChatPanel);
        relCamera = findViewById(R.id.relCameraButtons);
        imgRecvr = findViewById(R.id.imgRecvr);
        greenView = findViewById(R.id.greenView);
        greyView = findViewById(R.id.greyView);
        toolbar = findViewById(R.id.toolbar);


        try {
            drId = Integer.parseInt(dr_id);
            ptId = Integer.parseInt(pt_id);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }

        Utils.checkPermission(this);

        toolbar.inflateMenu(R.menu.menu_chat);
        Menu menu = toolbar.getMenu();
        itemEndChat = menu.findItem(R.id.imgEndChat);
        itemRateDoctor = menu.findItem(R.id.rateDoctor);
        itemSuggestDoctor = menu.findItem(R.id.suggestDoctor);
        itemAddNote = menu.findItem(R.id.addNote);
        itemNotes = menu.findItem(R.id.displayNote);


        if (App.user.getUser_Type() != null) {
            if (App.user.getUser_Type().equalsIgnoreCase(MyConstants.USER_DR)) {

                tvOppName.setText(pt_name + "");
                tvOppName.setSelected(true);
                str_name = "pppp" + pt_id;
                senderId = dr_id;
                reciverId = pt_id;
                myName = dr_name;
                recvName = pt_name;
                myPic = senderId;
                itemEndChat.setVisible(false);
                itemRateDoctor.setVisible(false);
                itemSuggestDoctor.setVisible(false);
                itemAddNote.setVisible(false);
                itemNotes.setVisible(false);
                //img_end_chat.setVisibility(View.GONE);

            } else {
                senderId = pt_id;
                reciverId = dr_id;
                myName = pt_name;
                recvName = dr_name;
                str_name = "dddd" + dr_id;
                tvOppName.setText(dr_name + "");
                myPic = senderId;
                tvOppName.setSelected(true);
                itemEndChat.setVisible(false);
                itemEndChat.setVisible(false);
                itemRateDoctor.setVisible(false);
                itemSuggestDoctor.setVisible(false);
                itemAddNote.setVisible(false);
                itemNotes.setVisible(false);
                //img_end_chat.setVisibility(View.GONE);
            }
        }

        System.out.println("user type::::" + App.user.getUser_Type());
        try {
            new CallRequest(FireChatActivity.this).checkCancelStatus(uniqueChatId, App.user.getUser_Type(), senderId);
        } catch (Exception e) {
            e.printStackTrace();
        }


        mAuth = FirebaseAuth.getInstance();
        signInAnonymously();
        senderImageUrl = App.user.getProfileUrl();
        System.out.println("profile url::" + recvImageUrl);

        try {
            if (recvImageUrl != null || !TextUtils.isEmpty(recvImageUrl)) {
                PicassoTrustAll.getInstance(FireChatActivity.this)
                        .load(recvImageUrl)
                        .placeholder(R.drawable.avatar)
                        .error(R.drawable.avatar)
                        .into(imgRecvr, new Callback() {
                            @Override
                            public void onSuccess() {
                            }

                            @Override
                            public void onError() {
                            }
                        });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        // overflow menu
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                if (item.getItemId() == R.id.imgEndChat) {
                    new AlertDialog.Builder(FireChatActivity.this)
                            .setTitle("Alert!")
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .setMessage("Do you want to end consult with " + recvName + "?")
                            .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    new CallRequest(FireChatActivity.this).endConsult(senderId, uniqueChatId);
                                    dialog.dismiss();
                                }
                            })
                            .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            })
                            .show();
                } else if (item.getItemId() == R.id.addNote) {
                    Intent intent = new Intent(FireChatActivity.this, AddNoteActivity.class);
                    intent.putExtra(MyConstants.UNIQUE_CHAT_ID, uniqueChatId);
                    intent.putExtra("fromChat", "1");
                    startActivity(intent);
                } else if (item.getItemId() == R.id.displayNote) {
                    Intent intent = new Intent(FireChatActivity.this, DisplayNotesActivity.class);
                    intent.putExtra(MyConstants.UNIQUE_CHAT_ID, uniqueChatId);
                    startActivity(intent);
                } else if (item.getItemId() == R.id.rateDoctor) {
                    Intent intent = new Intent(FireChatActivity.this, RatingDoctorActivity.class);
                    intent.putExtra("doctor_id", dr_id);
                    intent.putExtra(MyConstants.UNIQUE_CHAT_ID, uniqueChatId);
                    startActivity(intent);
                } else if (item.getItemId() == R.id.suggestDoctor) {
                    Intent intent = new Intent(FireChatActivity.this, SuggestDoctorActivity.class);
                    startActivityForResult(intent, DOCTOR_SUGGEST);
                }
                return false;
            }
        });

        fireDB = FirebaseDatabase.getInstance().getReference().child(MyConstants.CHANNEL);

        fireChaneel = fireDB.child(uniqueChatId);
        fireDbMessages = fireChaneel.child(ChatConstants.FIRE_DB_MESSAGES);
        fireDbTyping = fireChaneel.child(ChatConstants.FIRE_DB_TYPING);
        fireDbOnlineOffline = fireChaneel.child(ChatConstants.FIRE_DB_ONLINE);
        fireUnreadCounter = fireChaneel.child(ChatConstants.FIRE_UNREAD_COUNTER);


        img_audio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isCall = true;
                if (Utils.checkAudioPermission(FireChatActivity.this)) {

                    HashMap<String, String> headers = new HashMap<>();
                    headers.put("name", App.user.getName());
                    headers.put("profile_path", App.user.getProfileUrl());
                    headers.put("uniqueChatId", uniqueChatId);
                    headers.put("senderID", senderId);
                    headers.put("receiverID", reciverId);

                    try {
                        Call call = getSinchServiceInterface().callUser(str_name, headers);

                        String callId = call.getCallId();
                        Intent callScreen = new Intent(FireChatActivity.this, AudioCallActivity.class);
                        callScreen.putExtra("recvImageUrl", recvImageUrl);
                        callScreen.putExtra("recvName", tvOppName.getText().toString());
                        //callScreen.putExtra("UniqueId", uniqueChatId);
                        callScreen.putExtra("isDailing", true);
                        callScreen.putExtra("uniqueChatId", uniqueChatId);
                        callScreen.putExtra("senderID", senderId);
                        callScreen.putExtra("receiverID", reciverId);
                        callScreen.putExtra(CALL_ID, callId);
                        startActivityForResult(callScreen, VOICE_CALL_REQUEST_CODE);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        img_video.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isCall = true;
                isCallCamera = true;
                if (Utils.checkAudioPermission(FireChatActivity.this) && Utils.checkCameraPermission(FireChatActivity.this)) {

                    HashMap<String, String> map = new HashMap<>();
                    map.put("name", App.user.getName());
                    map.put("profile_path", App.user.getProfileUrl());
                    map.put("uniqueChatId", uniqueChatId);
                    map.put("senderID", senderId);
                    map.put("receiverID", reciverId);


                    try {
                        Call call = getSinchServiceInterface().callUserVideo(str_name, map);
                        String callId = call.getCallId();

                        Intent callScreen = new Intent(FireChatActivity.this, CallScreenActivity.class);
                        callScreen.putExtra("recvImageUrl", recvImageUrl);
                        callScreen.putExtra("recvName", tvOppName.getText().toString());
                        //callScreen.putExtra("UniqueId", uniqueChatId);
                        callScreen.putExtra("isDailing", true);
                        callScreen.putExtra("uniqueChatId", uniqueChatId);
                        callScreen.putExtra("senderID", senderId);
                        callScreen.putExtra("receiverID", reciverId);
                        callScreen.putExtra(CALL_ID, callId);
                        startActivityForResult(callScreen, VIDEO_CALL_REQUEST_CODE);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        setZeroCounter(reciverId);
        setZeroCounter(reciverId);

        cAdapter = new ChatAdapter(this, fireChatArray, senderId, "user", dr_name);
        recycleChatView.setAdapter(cAdapter);

        LayoutAnimationController controller = AnimationUtils.loadLayoutAnimation(recycleChatView.getContext(), R.anim.layout_animation_fall_down);
        recycleChatView.setLayoutAnimation(controller);
        recycleChatView.scheduleLayoutAnimation();

        MyLayoutManager linearLayoutManager = new MyLayoutManager(this);
        recycleChatView.setLayoutManager(linearLayoutManager);
        //recycleChatView.smoothScrollToPosition(position);

        createDirectories();
        Utils.showProgressDialog(this);
        setDatabaseListner();

        mHandler = new Handler();

        mAudioRunnable = new Runnable() {
            @Override
            public void run() {

                long sec = timer % 60;
                long min = timer / 60;

                tvTimer.setText(String.format("%02d", min) + ":" + String.format("%02d", sec));

                timer++;
                mHandler.postDelayed(this, 1000);

            }
        };


        relAudioPanel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                timer = 0;
                tvTimer.setText("");
                mHandler.removeCallbacks(mAudioRunnable);

                relChatPanel.setVisibility(View.VISIBLE);
                relAudioPanel.setVisibility(View.GONE);

                stopRecording();
            }
        });


        if (isPaymentDone) {
            String messageDefault1 = "Automatic Notification: Hi, we hope to make things super easy for both the patient (YOU!) and the Doctor. You can start typing answers or send things like, how long have you been experiencing these symptoms? Do you have pictures that would help explain your situation better? Or would you like to send a short video recording?";
            String messageDefault2 = "Automatic Notification: Don't worry your messages will be saved here, and your doctor will respond within his usual response time!";

            sendDefaultMessage(messageDefault1);
            sendDefaultMessage(messageDefault2);

        }

        imgSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (!etChat.getText().toString().trim().isEmpty())
                    sendMessage(etChat.getText().toString().trim());


            }
        });

        imgCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(etChat.getWindowToken(), 0);
                selectImage();
            }
        });


        imgMic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                retryAudio = true;
                startRecording();
            }
        });

        /*imgMic.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:

                        startRecording();
                        return true; // if you want to handle the touch event
                    case MotionEvent.ACTION_UP:

                        timer = 0;
                        tvTimer.setText("");
                        mHandler.removeCallbacks(mAudioRunnable);

                        relChatPanel.setVisibility(View.VISIBLE);
                        relAudioPanel.setVisibility(View.GONE);

                        stopRecording();
                        return true; // if you want to handle the touch event
                }
                return false;
            }
        });*/

        img_back2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        etChat.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (s.toString().trim().length() > 0) {
                    relCamera.setVisibility(View.GONE);
                    imgSend.setVisibility(View.VISIBLE);
                } else {
                    relCamera.setVisibility(View.VISIBLE);
                    imgSend.setVisibility(View.GONE);
                }

                if (s.length() > 0) {
                    setTypingIndicator(true);
                } else {
                    setTypingIndicator(false);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        /*recycleChatView.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
            @Override

            public void onLayoutChange(View v, int left, int top, int right,int bottom, int oldLeft, int oldTop,int oldRight, int oldBottom)
            {

                recycleChatView.scrollToPosition(cAdapter.getItemCount()-1);

            }
        });*/


    }

    @Override
    public void onBackPressed() {
        try {
            InputMethodManager inputMethodManager = (InputMethodManager)
                    getSystemService(Context.INPUT_METHOD_SERVICE);
            if (inputMethodManager != null) {
                inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (isPaymentDone) {
            Intent intent = new Intent(FireChatActivity.this, PTDashboardActivity.class);
            intent.putExtra("firstTimeChat", "1");
            startActivity(intent);
            ActivityCompat.finishAffinity(FireChatActivity.this);
        } else {
            super.onBackPressed();
        }
    }

    public void setZeroCounter(String reciverId) {
        Map<String, Object> unreadCounter = new HashMap<>();
        unreadCounter.put(reciverId, 0);
        fireUnreadCounter.updateChildren(unreadCounter);
    }

    @Override
    protected void onServiceConnected() {
        img_audio.setEnabled(true);
        img_video.setEnabled(true);

        try {
            if (!TextUtils.isEmpty(App.user.getUserID())) {
                if (MyConstants.isGuest.equalsIgnoreCase("0")) {
                    App.user.setSinch_id(sharedpreferences.getString(MyConstants.SINCH_ID, ""));
                }

                if (!getSinchServiceInterface().isStarted()) {
                    getSinchServiceInterface().startClient(App.user.getSinch_id());
                }
                getSinchServiceInterface().setIncomingLisnter(this);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroy() {
        /*if (getSinchServiceInterface() != null) {
            getSinchServiceInterface().stopClient();
        }*/
        super.onDestroy();
    }

    public void sendNewMessage(FireMessage chat) {
        try {
            String key = fireDbMessages.push().getKey();

            Map<String, Object> postValues = chat.toMap();
            Map<String, Object> childUpdates = new HashMap<>();
            childUpdates.put(key, postValues);
            fireDbMessages.updateChildren(childUpdates);

            Map<String, Object> lastValue = new HashMap<>();
            lastValue.put("last_message", chat.text);
            lastValue.put("last_date", chat.date);
            fireChaneel.updateChildren(lastValue);


            if (!isLive) {
                unreadMessageCount++;
            } else {
                unreadMessageCount = 0;
            }

            Map<String, Object> unreadCounter = new HashMap<>();
            unreadCounter.put(senderId, unreadMessageCount);
            fireUnreadCounter.updateChildren(unreadCounter);

            etChat.setText("");
            notifyChatAdapter();
            recycleChatView.scrollToPosition(cAdapter.getItemCount() - 1);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void notifyChatAdapter() {
        recycleChatView.getRecycledViewPool().clear();
        cAdapter.notifyDataSetChanged();
    }

    public void setOnlineOffline(boolean isLive) {
        try {
            String key = fireDbOnlineOffline.child(senderId).getKey();


            Map<String, Object> postValues = new HashMap<>();
            if (isLive) {
                postValues.put(senderId, "1");
            } else {
                postValues.put(senderId, "0");
            }
            Map<String, Object> childUpdates = new HashMap<>();
            childUpdates.put(key, postValues);
            fireDbOnlineOffline.updateChildren(postValues);


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void signInAnonymously() {
        // Sign in anonymously. Authentication is required to read or write from Firebase Storage.
        Utils.showProgressDialog(this, "Accessing Databse");
        mAuth.signInAnonymously()
                .addOnSuccessListener(this, new OnSuccessListener<AuthResult>() {
                    @Override
                    public void onSuccess(AuthResult authResult) {
                        Log.d(TAG, "signInAnonymously:SUCCESS");
                        Utils.hideProgressDialog();

                    }
                })
                .addOnFailureListener(this, new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception exception) {
                        Log.e(TAG, "signInAnonymously:FAILURE", exception);
                        Utils.hideProgressDialog();

                    }
                });
    }

    public void setTypingIndicator(boolean isLive) {
        try {
            String key = fireDbTyping.child(senderId).getKey();
            Map<String, Object> postValues = new HashMap<>();
            if (isLive) {
                postValues.put(senderId, "1");
            } else {
                postValues.put(senderId, "0");
            }

            fireDbTyping.updateChildren(postValues);
            recycleChatView.scrollToPosition(cAdapter.getItemCount() - 1);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setDatabaseListner() {

        fireDbMessages.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(com.google.firebase.database.DataSnapshot dataSnapshot, String s) {
                try {

                    Log.i(AUDIO_TAG, "FireMessage: getting data snapstp : child added " + dataSnapshot.getChildrenCount());


                    FireMessage msgAdded = dataSnapshot.getValue(FireMessage.class);
                    if (!msgAdded.senderId.equalsIgnoreCase(senderId)) {
                        fireChatArray.add(msgAdded);
                    }

                } catch (DatabaseException e) {
                    e.printStackTrace();
                }

                if (cAdapter != null && fireChatArray.size() > 0) {
                    notifyChatAdapter();
                }

                recycleChatView.scrollToPosition(cAdapter.getItemCount() - 1);
            }

            @Override
            public void onChildChanged(com.google.firebase.database.DataSnapshot dataSnapshot, String s) {
            }

            @Override
            public void onChildRemoved(com.google.firebase.database.DataSnapshot dataSnapshot) {
            }

            @Override
            public void onChildMoved(com.google.firebase.database.DataSnapshot dataSnapshot, String s) {
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });

        fireDbMessages.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Utils.hideProgressDialog();
                try {
                    // Get user value
                    Log.i(AUDIO_TAG, "FireMessage: getting data snapstp : everytime " + dataSnapshot.getChildrenCount());

                    fireChatArray.clear();
                    for (com.google.firebase.database.DataSnapshot dsp : dataSnapshot.getChildren()) {
                        Log.i(AUDIO_TAG, "FireMessage: getting data snapstp everytime in for loop ");
                        try {
                            fireChatArray.add(dsp.getValue(FireMessage.class));
                        } catch (DatabaseException e) {
                            e.printStackTrace();
                        }
                    }
                    if (cAdapter != null && fireChatArray.size() > 0) {
                        notifyChatAdapter();
                    }
                    recycleChatView.scrollToPosition(cAdapter.getItemCount() - 1);


                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Utils.hideProgressDialog();
                Log.w(AUDIO_TAG, "getUser:onCancelled", databaseError.toException());
            }
        });
//        fireDbMessages.addValueEventListener(
//                new com.google.firebase.database.ValueEventListener() {
//                    @Override
//                    public void onDataChange(com.google.firebase.database.DataSnapshot dataSnapshot) {
//
//
//                    }
//
//                    @Override
//                    public void onCancelled(DatabaseError databaseError) {
//
//
//                    }
//                });

        fireDbTyping.addValueEventListener(
                new com.google.firebase.database.ValueEventListener() {
                    @Override
                    public void onDataChange(com.google.firebase.database.DataSnapshot dataSnapshot) {
                        Log.i(AUDIO_TAG, "FireMessage:  dbTyping  getting data snapstp : " + dataSnapshot.getChildrenCount());

                        try {
                            if (dataSnapshot.hasChild(reciverId)) {
                                String onOff = dataSnapshot.child(reciverId).getValue(String.class);
                                if (onOff.equals("1")) {
                                    tvTyping.setVisibility(View.VISIBLE);
                                } else {
                                    tvTyping.setVisibility(View.GONE);
                                }
                            }
                            recycleChatView.scrollToPosition(cAdapter.getItemCount() - 1);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        Log.w(AUDIO_TAG, "getUser:onCancelled", databaseError.toException());

                    }
                });

        fireDbOnlineOffline.addValueEventListener(
                new com.google.firebase.database.ValueEventListener() {
                    @Override
                    public void onDataChange(com.google.firebase.database.DataSnapshot dataSnapshot) {

                        Log.i(AUDIO_TAG, "FireMessage:  dbOnIff  getting data snapstp : " + dataSnapshot.toString());

                        Log.i(AUDIO_TAG, "FireMessage:  dbOnIff  getting data snapstp : " + reciverId);

                        if (dataSnapshot.hasChild(reciverId) && !reciverId.isEmpty()) {
                            try {
                                Log.i(AUDIO_TAG, "FireMessage:  dbOnIff  : " + dataSnapshot.child(reciverId).getValue(String.class));
                                String onOff = dataSnapshot.child(reciverId).getValue(String.class);
                                if (onOff.equalsIgnoreCase("1")) {
                                    isLive = true;
                                    unreadMessageCount = 0;
                                    greenView.setVisibility(View.VISIBLE);
                                    greyView.setVisibility(View.GONE);
                                } else {
                                    isLive = false;
                                    greenView.setVisibility(View.GONE);
                                    greyView.setVisibility(View.VISIBLE);
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                            isLive = false;
                            greenView.setVisibility(View.GONE);
                            greyView.setVisibility(View.VISIBLE);
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        Log.w(AUDIO_TAG, "getUser:onCancelled", databaseError.toException());

                    }
                });
        setOnlineOffline(true);

        try {
            fireChaneel.child("isEnded").addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    if (dataSnapshot != null) {
                        System.out.println("datasnapshot::" + dataSnapshot.toString());
                        if (dataSnapshot.getValue(false) != null) {
                            String isEnded = dataSnapshot.getValue(String.class);
                            System.out.println("isEnded" + isEnded);

                            if (isEnded != null) {
                                if (isEnded.equalsIgnoreCase("1")) {
                                    relChatPanel.setVisibility(View.GONE);
                                    img_audio.setVisibility(View.GONE);
                                    itemSuggestDoctor.setVisible(false);
                                    itemNotes.setVisible(false);
                                    itemAddNote.setVisible(false);
                                    //img_end_chat.setVisibility(View.GONE);
                                    itemEndChat.setVisible(false);
                                    if (App.user.getUser_Type().equalsIgnoreCase(MyConstants.USER_PT)) {
                                        itemRateDoctor.setVisible(true);
                                        toolbar.setVisibility(View.VISIBLE);
                                    } else {
                                        toolbar.setVisibility(View.GONE);
                                    }
                                    img_video.setVisibility(View.GONE);

                                    /*AlertDialog.Builder builder = new AlertDialog.Builder(staticContext);
                                    builder.setTitle("Thank you for using ConnexMd");
                                    builder.setMessage("You've ended your virtual consult.")
                                            .setCancelable(false)
                                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int id) {

                                                    *//*Intent intent = new Intent(FireChatActivity.this, PTDashboardActivity.class);
                                                    startActivity(intent);
                                                    ActivityCompat.finishAffinity(FireChatActivity.this);*//*
                                                }
                                            });
                                    AlertDialog alert = builder.create();
                                    alert.show();*/
                                } else {
                                    relChatPanel.setVisibility(View.VISIBLE);
                                    img_audio.setVisibility(View.VISIBLE);
                                    if (App.user.getUser_Type().equalsIgnoreCase(MyConstants.USER_DR)) {
                                        itemEndChat.setVisible(true);
                                        // TODO itemSuggestDoctor.setVisible(true);
                                        itemSuggestDoctor.setVisible(true);
                                        itemRateDoctor.setVisible(false);
                                        itemNotes.setVisible(true);
                                        itemAddNote.setVisible(true);
                                        toolbar.setVisibility(View.VISIBLE);
                                        //img_end_chat.setVisibility(View.VISIBLE);
                                    } else {
                                        itemEndChat.setVisible(false);
                                        itemSuggestDoctor.setVisible(false);
                                        itemRateDoctor.setVisible(false);
                                        itemNotes.setVisible(true);
                                        itemAddNote.setVisible(true);
                                        toolbar.setVisibility(View.VISIBLE);
                                        //img_end_chat.setVisibility(View.GONE);
                                    }
                                    img_video.setVisibility(View.VISIBLE);
                                }
                            }

                        } else {
                            System.out.println("isEnded::: single value event ::: null ");
                            /*Map<String, Object> isEndedConsult = new HashMap<>();
                            isEndedConsult.put("isEnded", false);
                            fireChaneel.updateChildren(isEndedConsult);*/
                        }
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });

            fireChaneel.child("isEnded").addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    if (dataSnapshot != null) {
                        System.out.println("datasnapshot::  addValueEventListener:::" + dataSnapshot.toString());
                        if (dataSnapshot.getValue(false) != null) {
                            String isEnded = dataSnapshot.getValue(String.class);
                            System.out.println("isEnded" + isEnded);

                            if (isEnded != null) {
                                if (isEnded.equalsIgnoreCase("1")) {
                                    String text;
                                    relChatPanel.setVisibility(View.GONE);
                                    img_audio.setVisibility(View.GONE);
                                    itemSuggestDoctor.setVisible(false);
                                    itemNotes.setVisible(false);
                                    itemAddNote.setVisible(false);
                                    //img_end_chat.setVisibility(View.GONE);
                                    itemEndChat.setVisible(false);
                                    if (App.user.getUser_Type().equalsIgnoreCase(MyConstants.USER_PT)) {
                                        itemRateDoctor.setVisible(true);
                                        toolbar.setVisibility(View.VISIBLE);
                                        text = "The doctor has now ended the consult.";
                                    } else {
                                        toolbar.setVisibility(View.GONE);
                                        text = "You've ended your virtual consult.";
                                    }
                                    img_video.setVisibility(View.GONE);

                                    try {

                                        AlertDialog.Builder builder = new AlertDialog.Builder(staticContext);
                                        builder.setTitle("Thank you for using ConnexMD");
                                        builder.setMessage(text)
                                                .setCancelable(false)
                                                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                                    public void onClick(DialogInterface dialog, int id) {

                                                    /*Intent intent = new Intent(FireChatActivity.this, PTDashboardActivity.class);
                                                    startActivity(intent);
                                                    ActivityCompat.finishAffinity(FireChatActivity.this);*/
                                                    }
                                                });
                                        AlertDialog alert = builder.create();
                                        alert.show();
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                } else {
                                    relChatPanel.setVisibility(View.VISIBLE);
                                    img_audio.setVisibility(View.VISIBLE);
                                    if (App.user.getUser_Type().equalsIgnoreCase(MyConstants.USER_DR)) {
                                        itemEndChat.setVisible(true);
                                        // TODO itemSuggestDoctor.setVisible(true);
                                        itemSuggestDoctor.setVisible(true);
                                        itemRateDoctor.setVisible(false);
                                        itemNotes.setVisible(true);
                                        itemAddNote.setVisible(true);
                                        toolbar.setVisibility(View.VISIBLE);
                                        //img_end_chat.setVisibility(View.VISIBLE);
                                    } else {
                                        itemEndChat.setVisible(false);
                                        itemSuggestDoctor.setVisible(false);
                                        itemRateDoctor.setVisible(false);
                                        itemNotes.setVisible(true);
                                        itemAddNote.setVisible(true);
                                        toolbar.setVisibility(View.VISIBLE);
                                        //img_end_chat.setVisibility(View.GONE);
                                    }
                                    img_video.setVisibility(View.VISIBLE);
                                }
                            }

                        } else {
                            System.out.println("isEnded ::: null ");
                             /*Map<String, Object> isEndedConsult = new HashMap<>();
                            isEndedConsult.put("isEnded", false);
                            fireChaneel.updateChildren(isEndedConsult);*/
                        }

                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        //Checking the request code of our request
        if (requestCode == Utils.RECORD_AUDDIO) {

            //If permission is granted
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                if (!isCall) {
                    retryAudio = true;
                    startRecording();
                }
            } else {
                //Displaying another toast if permission is not granted
                Utils.showAlert("Please grant audio permission to record", this);
            }
        } else if (requestCode == Utils.CAMERA_PERMISSION) {

            //If permission is granted
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                if (!isCallCamera) {
                    if (selectedType.equalsIgnoreCase("Take Photo")) {
                        File f = new File(Environment.getExternalStorageDirectory() + "/ConnexMd/images");
                        if (!f.exists()) {
                            f.mkdirs();
                        }
                        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        File file = new File(Environment.getExternalStorageDirectory(), "ConnexMd/images/img_" + System.currentTimeMillis() + ".jpg");
                        selectedUri = Uri.fromFile(file);
                        intent.putExtra(MediaStore.EXTRA_OUTPUT, selectedUri);

                        startActivityForResult(intent, TAKE_PICTURE);
                    } else {

                        File f = new File(Environment.getExternalStorageDirectory() + "/ConnexMd/video");
                        if (!f.exists()) {
                            f.mkdirs();

                        }
                        Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
                        File file = new File(Environment.getExternalStorageDirectory(), "ConnexMd/video/vid__" + System.currentTimeMillis() + ".mp4");
                        selectedUri = Uri.fromFile(file);
                        intent.putExtra(MediaStore.EXTRA_OUTPUT, selectedUri);

                        startActivityForResult(intent, TAKE_VIDEO);
                    }
                }
            } else {
                Utils.showToast("Permission not granted", FireChatActivity.this);
            }
        }
    }
    @Override
    protected void onResume() {
        super.onResume();

        //new CallRequest(FireChatActivity.this).checkCancelStatus(uniqueChatId, App.user.getUser_Type(), senderId);

        setOnlineOffline(true);
        setZeroCounter(reciverId);

    }

    @Override
    protected void onPause() {
        if (mStatusHandler != null && mStatusRunnable != null) {
            mStatusHandler.removeCallbacks(mStatusRunnable);
        }

        setOnlineOffline(false);
        setTypingIndicator(false);

        super.onPause();
    }




    public void startRecording() {
        try {

            boolean result2 = Utils.checkPermission(FireChatActivity.this);
            boolean result = Utils.checkAudioPermission(FireChatActivity.this);
            isCall = false;
            if (result && result2) {
                isStart = true;
                relChatPanel.setVisibility(View.GONE);
                relAudioPanel.setVisibility(View.VISIBLE);
                timer = 0;
                tvTimer.setText("00:00");
                mHandler.postDelayed(mAudioRunnable, 0);
                //mHandler.postDelayed(mAudioRunnable, 1000);
                file = getFilename();

                myAudioRecorder = new MediaRecorder();
                myAudioRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
                myAudioRecorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
                myAudioRecorder.setOutputFile(file);
                myAudioRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);
//                myAudioRecorder.setAudioEncoder(MediaRecorder.getAudioSourceMax());
                myAudioRecorder.setAudioEncodingBitRate(16);
                myAudioRecorder.setAudioSamplingRate(44100);
                myAudioRecorder.setAudioChannels(1);


                myAudioRecorder.setOnErrorListener(errorListener);
                myAudioRecorder.setOnInfoListener(infoListener);

                myAudioRecorder.prepare();
                myAudioRecorder.start();

            } else {
                //Utils.showToast("Permission not granted", FireChatActivity.this);
            }

        } catch (IllegalStateException e) {
            e.printStackTrace();
            String msg = e.getMessage();

            if (msg.contains(MEDIA_RECORD_ERROR)) {
                Utils.showAlert("Getting microphone ready, Please try again", this);
                stopRecording();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

    }
    public void stopRecording() {
        try {
            if (null != myAudioRecorder) {
                myAudioRecorder.stop();

                myAudioRecorder.reset();

                isStart = false;
                // m.start();
                imgDecodableString = file;
                Log.i("Tag", "Path :" + file);
                String pure = file.replace("/storage", "");
                int bytesRead;

                File f = new File(file);
                FileInputStream is = new FileInputStream(f);
                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                int size = is.available();
                byte[] buffer = new byte[size];

                while ((bytesRead = is.read(buffer)) != -1) {
                    bos.write(buffer, 0, bytesRead);
                }
                is.close();
                bytes = bos.toByteArray();


                FileType = "AudioFiles";

                chatBean = new FireMessage();
                chatBean.date = getUTCDate();
                chatBean.localPath = imgDecodableString;
                chatBean.isReceived = false;
                chatBean.senderId = senderId;
                chatBean.isSent = true;
                chatBean.chatID = System.currentTimeMillis() + "";
                fireChatArray.add(chatBean);
                selectedUri = Uri.fromFile(f);
                if (selectedUri != null) {
                    uploadFromUri(chatBean, selectedUri, ChatConstants.FILE_TYPE_AUDIO);
                } else {
                    Log.w(IMG_TAG, "File URI is null");
                }
                notifyChatAdapter();
                relAudioPanel.setVisibility(View.GONE);
                relChatPanel.setVisibility(View.VISIBLE);
                ///recycleChatView.scrollToPosition(cAdapter.getItemCount() - 1);
            }

        } catch (IllegalStateException | IOException | IllegalThreadStateException | NullPointerException e) {
            e.printStackTrace();
        } catch (RuntimeException e) {
            //Ignore
            e.printStackTrace();
        }

    }
    private String getFilename() {
        String filepath = Environment.getExternalStorageDirectory().getPath();
        File file = new File(filepath, AUDIO_RECORDER_FOLDER);

        if (!file.exists()) {
            file.mkdirs();
        }
        FileName = System.currentTimeMillis() + AUDIO_RECORDER_FILE_EXT_3GP;
        return (file.getAbsolutePath() + "/" + System.currentTimeMillis() + AUDIO_RECORDER_FILE_EXT_3GP);
    }
    private void sendDefaultMessage(String message) {
        try {

            chatBean = new FireMessage();
            chatBean.text = message;
            chatBean.date = getUTCDate();
            chatBean.isSent = true;
            chatBean.isReceived = false;
            chatBean.chatID = System.currentTimeMillis() + "";
            chatBean.senderId = dr_id;
            //   fireChatArray.add(chatBean);

            try {
                String key = fireDbMessages.push().getKey();

                Map<String, Object> postValues = chatBean.toMap();
                Map<String, Object> childUpdates = new HashMap<>();
                childUpdates.put(key, postValues);
                fireDbMessages.updateChildren(childUpdates);

                Map<String, Object> lastValue = new HashMap<>();
                lastValue.put("last_message", chatBean.text);
                lastValue.put("last_date", chatBean.date);
                lastValue.put("profile_pic", senderImageUrl);
                lastValue.put("is_asked", "y");
                lastValue.put("ConsultId", uniqueChatId);
                fireChaneel.updateChildren(lastValue);

                if (!isLive) {
                    unreadMessageCount++;
                } else {
                    unreadMessageCount = 0;
                }

                Map<String, Object> unreadCounter = new HashMap<>();
                unreadCounter.put(senderId, unreadMessageCount);
                fireUnreadCounter.updateChildren(unreadCounter);

                etChat.setText("");
                notifyChatAdapter();
                recycleChatView.scrollToPosition(cAdapter.getItemCount() - 1);
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (!isLive) {
                sendNotificationMessage(message, false);
            }

            playTone(false);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void sendMessage(String message) {
        sendTextMessage(message);
    }
    public void sendTextMessage(String message) {
        try {

            chatBean = new FireMessage();
            chatBean.text = message;
            chatBean.date = getUTCDate();
            chatBean.isSent = true;
            chatBean.isReceived = false;
            chatBean.senderId = senderId;

            fireChatArray.add(chatBean);

            sendNewMessage(chatBean);

            if (!isLive) {
                sendNotificationMessage(message, false);
            }

            playTone(false);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    public void selectImage() {
        final CharSequence[] items = {"Take Photo", "Capture Video", "Choose Photo from Library", "Choose Video from Library", "Drawing Tool",
                "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(FireChatActivity.this, R.style.MyDialogTheme);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                isCallCamera = false;
                boolean result = Utils.checkCameraPermission(FireChatActivity.this);
                if (items[item].equals("Take Photo")) {
                    selectedType = "Take Photo";
                    boolean result2 = Utils.checkPermission(FireChatActivity.this);
                    if (result && result2) {

                        File f2 = new File(Environment.getExternalStorageDirectory() + "/ConnexMd/images");
                        if (!f2.exists()) {
                            f2.mkdirs();
                        }

                        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        File file = new File(Environment.getExternalStorageDirectory(), "ConnexMd/images/img_" + System.currentTimeMillis() + ".jpg");
                        selectedUri = Uri.fromFile(file);
                        intent.putExtra(MediaStore.EXTRA_OUTPUT, selectedUri);
                        startActivityForResult(intent, TAKE_PICTURE);
                    }

                } else if (items[item].equals("Capture Video")) {
                    selectedType = "Take Photo";
                    boolean result2 = Utils.checkPermission(FireChatActivity.this);
                    if (result && result2) {

                        File f = new File(Environment.getExternalStorageDirectory() + "/ConnexMd/video");
                        if (!f.exists()) {
                            f.mkdirs();

                        }

                        Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
                        File file = new File(Environment.getExternalStorageDirectory(), "ConnexMd/video/vid__" + System.currentTimeMillis() + ".mp4");
                        selectedUri = Uri.fromFile(file);
                        intent.putExtra(MediaStore.EXTRA_OUTPUT, selectedUri);
                        startActivityForResult(intent, TAKE_VIDEO);
                    }
                } else if (items[item].equals("Choose Video from Library")) {
                    if (result) {
                        try {
                            Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Video.Media.EXTERNAL_CONTENT_URI);
                            intent.setType("video/*");
                            startActivityForResult(Intent.createChooser(intent, "Select Video"), PICK_VIDEO);
                        } catch (Exception e) {
                            Log.d("EXChooseVideo", e.getMessage());
                        }
                    }
                } else if (items[item].equals("Choose Photo from Library")) {

                    if (result) {
                        Intent intent = new Intent(Intent.ACTION_PICK,
                                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        startActivityForResult(Intent.createChooser(intent, "Select File"), PiCK_IMAGE);

                    }

                } else if (items[item].equals("Drawing Tool")) {
                    Intent intent = new Intent(FireChatActivity.this, DrawingToolActivity.class);
                    startActivityForResult(intent, DRAW_PICTURE);
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == TAKE_VIDEO && resultCode == RESULT_OK) {


            chatBean = new FireMessage();
            chatBean.videoURL = "video";
            chatBean.date = getUTCDate();
            chatBean.localPath = getPath(selectedUri);
            chatBean.isReceived = false;
            chatBean.isSent = false;
            chatBean.senderId = senderId;
            chatBean.chatID = System.currentTimeMillis() + "";
            fireChatArray.add(chatBean);


            if (selectedUri != null) {
                uploadFromUri(chatBean, selectedUri, ChatConstants.FILE_TYPE_VIDEO);
            } else {
                Log.w(IMG_TAG, "File URI is null");
            }
            notifyChatAdapter();
            ///recycleChatView.scrollToPosition(cAdapter.getItemCount() - 1);


        }


        if (requestCode == TAKE_PICTURE && resultCode == RESULT_OK) {

            chatBean = new FireMessage();
            chatBean.photoURL = "image";
            chatBean.date = getUTCDate();
            chatBean.localPath = getPath(selectedUri);
            chatBean.isReceived = false;
            chatBean.isSent = false;
            chatBean.senderId = senderId;
            chatBean.chatID = System.currentTimeMillis() + "";
            fireChatArray.add(chatBean);

            Log.i(IMG_TAG, "Image is going to Select : " + selectedUri);
            if (selectedUri != null) {
                uploadFromUri(chatBean, selectedUri, ChatConstants.FILE_TYPE_IMAGE);
            } else {
                Log.w(IMG_TAG, "File URI is null");
            }

            notifyChatAdapter();
            ///recycleChatView.scrollToPosition(cAdapter.getItemCount() - 1);

        }

        if (requestCode == DRAW_PICTURE) {
            if (data != null) {
                String filepath = data.getStringExtra("filepath");

                chatBean = new FireMessage();
                chatBean.photoURL = "image";
                chatBean.date = getUTCDate();
                ;
                chatBean.localPath = filepath;
                chatBean.isReceived = false;
                chatBean.isSent = false;
                chatBean.senderId = senderId;
                chatBean.chatID = System.currentTimeMillis() + "";
                fireChatArray.add(chatBean);
                selectedUri = Uri.fromFile(new File(filepath));
                if (selectedUri != null) {
                    uploadFromUri(chatBean, selectedUri, ChatConstants.FILE_TYPE_IMAGE);
                } else {
                    Log.w(IMG_TAG, "File URI is null");
                }

                notifyChatAdapter();
            }
        }

        if (requestCode == DOCTOR_SUGGEST) {
            if (data != null) {
                String doctorId = data.getStringExtra("doctorId");
                String doctorURL = data.getStringExtra("doctorURL");
                String doctorName = data.getStringExtra("doctorName");
                String rating = data.getStringExtra("rating");
                String responseTime = data.getStringExtra("response_time");
                String speciality = data.getStringExtra("speciality");
                String consult_charge = data.getStringExtra("consult_charge");
                chatBean = new FireMessage();
                chatBean.date = getUTCDate();
                chatBean.isReceived = false;
                chatBean.isSent = false;
                chatBean.doctorId = doctorId;
                chatBean.doctorName = doctorName;
                chatBean.doctorURL = doctorURL;
                chatBean.consultCharge = consult_charge;
                chatBean.rating = rating;
                chatBean.speciality = speciality;
                chatBean.responseTime = responseTime;
                chatBean.sended = "DoctorSuggest";
                chatBean.chatID = System.currentTimeMillis() + "";
                chatBean.senderId = senderId;
                fireChatArray.add(chatBean);

                sendSuggestDoctorMessage(chatBean);
                notifyChatAdapter();
            }
        }

        if ((requestCode == PiCK_IMAGE || requestCode == PICK_VIDEO) && resultCode == RESULT_OK) {

            selectedUri = data.getData();
            chatBean = new FireMessage();
            if (requestCode == PICK_VIDEO) {
                chatBean.videoURL = "video";
            } else {
                chatBean.photoURL = "image";
            }
            chatBean.date = getUTCDate();
            chatBean.localPath = getRealVideoPathFromUri(selectedUri);
            chatBean.isReceived = false;
            chatBean.isSent = false;
            chatBean.senderId = senderId;
            chatBean.chatID = System.currentTimeMillis() + "";
            fireChatArray.add(chatBean);

            if (requestCode == PICK_VIDEO) {
                uploadFromUri(chatBean, selectedUri, ChatConstants.FILE_TYPE_VIDEO);
            } else {
                uploadFromUri(chatBean, selectedUri, ChatConstants.FILE_TYPE_IMAGE);
            }
            notifyChatAdapter();
            ///recycleChatView.scrollToPosition(cAdapter.getItemCount() - 1);

        }

        if (requestCode == VOICE_CALL_REQUEST_CODE && resultCode == RESULT_OK) {
            System.out.println("return data");
            if (data != null) {

                String isAudio = data.getStringExtra("IsAudio");
                String callEndCause = data.getStringExtra("CallEndCause");

                String callUniqueChatID = data.getStringExtra("uniqueChatId");
                String callSenderID = data.getStringExtra("senderID");
                String callReceiverID = data.getStringExtra("receiverID");

                chatBean = new FireMessage();
                chatBean.date = getUTCDate();
                chatBean.isSent = true;
                chatBean.isReceived = false;
                chatBean.isAudio = isAudio;
                chatBean.callEndCause = callEndCause;
                chatBean.call = "1";
                chatBean.callerId = senderId;
                chatBean.calleeId = reciverId;
                if (callEndCause.equalsIgnoreCase("HUNG_UP")) {
                    chatBean.callNotificationMessage = "Made a voice call";
                } else {
                    chatBean.callNotificationMessage = "Missed a voice call";
                }

                chatBean.senderId = senderId;

                if (uniqueChatId.equalsIgnoreCase(callUniqueChatID)) {
                    fireChatArray.add(chatBean);
                    sendCallMessage(chatBean);
                } else {
                    chatBean.callerId = callSenderID;
                    chatBean.calleeId = callReceiverID;
                    sendOtherChatCallMessage(chatBean, callUniqueChatID);
                }
                notifyChatAdapter();
            }
        }

        if (requestCode == VIDEO_CALL_REQUEST_CODE && resultCode == RESULT_OK) {
            System.out.println("return data");
            if (data != null) {

                String isAudio = data.getStringExtra("IsAudio");
                String callEndCause = data.getStringExtra("CallEndCause");

                String callUniqueChatID = data.getStringExtra("uniqueChatId");
                String callSenderID = data.getStringExtra("senderID");
                String callReceiverID = data.getStringExtra("receiverID");

                chatBean = new FireMessage();
                chatBean.date = getUTCDate();
                chatBean.isSent = true;
                chatBean.isReceived = false;
                chatBean.isAudio = isAudio;
                chatBean.callEndCause = callEndCause;

                chatBean.call = "1";
                chatBean.callerId = senderId;
                chatBean.calleeId = reciverId;
                if (callEndCause.equalsIgnoreCase("HUNG_UP")) {
                    chatBean.callNotificationMessage = "Made a video call";
                } else {
                    chatBean.callNotificationMessage = "Missed a video call";
                }

                chatBean.senderId = senderId;


                if (uniqueChatId.equalsIgnoreCase(callUniqueChatID)) {
                    fireChatArray.add(chatBean);
                    sendCallMessage(chatBean);
                } else {
                    chatBean.callerId = callSenderID;
                    chatBean.calleeId = callReceiverID;
                    sendOtherChatCallMessage(chatBean, callUniqueChatID);
                }
                notifyChatAdapter();
            }
        }
    }
    public String getPath(Uri uri) {
        File myFile = new File(uri.getPath());
        myFile.getAbsolutePath();
        return myFile.getAbsolutePath();
    }
    @TargetApi(Build.VERSION_CODES.KITKAT)
    public String getRealVideoPathFromUri(Uri contentURI) {
        String[] projection = {MediaStore.Video.Media.DATA};
        Cursor cursor = managedQuery(contentURI, projection, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Video.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);

    }
    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        try {
            if (result != null && !result.isEmpty()) {
                Log.i("TAG", "TAG Result : " + result);
                switch (request) {
                    case cancelChat:
                        Utils.removeSimpleSpinProgressDialog();
                        try {
                            JSONObject obj = new JSONObject(result);
                            String error_code = obj.getString("error_code");

                            if (error_code.equalsIgnoreCase("0")) {

                                Map<String, Object> isEndedConsult = new HashMap<>();
                                isEndedConsult.put("isEnded", "1");
                                fireChaneel.updateChildren(isEndedConsult);

                            } else {
                                Utils.showToast(obj.getString("error_string"), FireChatActivity.this);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;

                    case rate_doctor:
                        Utils.removeSimpleSpinProgressDialog();
                        JSONObject obj = new JSONObject(result);
                        try {

                            if (obj.getJSONObject("document").getJSONObject("response").getString("status").equals("1")) {
                                Utils.showToast(obj.getJSONObject("document").getJSONObject("response").getString("message"), this);
                            } else {
                                Utils.showToast(obj.getJSONObject("document").getJSONObject("response").getString("message"), this);
                            }

                        } catch (JSONException e) {
                            Utils.showToast(obj.getJSONObject("document").getJSONObject("response").getString("message"), this);

                            e.printStackTrace();
                        }
                        break;

                    case checkCancelStatus:
                        Utils.removeSimpleSpinProgressDialog();
                        try {
                            obj = new JSONObject(result);

                            if (obj.getString("error_code").equalsIgnoreCase("0")) {

                                JSONObject object = obj.getJSONArray("result").getJSONObject(0);

                                String Status = object.getString("Status");

                                if (Status.equalsIgnoreCase("0")) {
                                    relChatPanel.setVisibility(View.VISIBLE);
                                    img_audio.setVisibility(View.VISIBLE);
                                    if (App.user.getUser_Type().equalsIgnoreCase(MyConstants.USER_DR)) {
                                        itemEndChat.setVisible(true);
                                        // TODO itemSuggestDoctor.setVisible(true);
                                        itemSuggestDoctor.setVisible(true);
                                        itemRateDoctor.setVisible(false);
                                        itemNotes.setVisible(true);
                                        itemAddNote.setVisible(true);
                                        toolbar.setVisibility(View.VISIBLE);
                                        //img_end_chat.setVisibility(View.VISIBLE);
                                    } else {
                                        itemEndChat.setVisible(false);
                                        itemSuggestDoctor.setVisible(false);
                                        itemRateDoctor.setVisible(false);
                                        itemNotes.setVisible(true);
                                        itemAddNote.setVisible(true);
                                        toolbar.setVisibility(View.VISIBLE);
                                        //img_end_chat.setVisibility(View.GONE);
                                    }
                                    img_video.setVisibility(View.VISIBLE);

                                    Map<String, Object> isEndedConsult = new HashMap<>();
                                    isEndedConsult.put("isEnded", "0");
                                    fireChaneel.updateChildren(isEndedConsult);

                                } else {
                                    relChatPanel.setVisibility(View.GONE);
                                    img_audio.setVisibility(View.GONE);
                                    itemSuggestDoctor.setVisible(false);
                                    itemNotes.setVisible(false);
                                    itemAddNote.setVisible(false);
                                    //img_end_chat.setVisibility(View.GONE);
                                    itemEndChat.setVisible(false);
                                    if (App.user.getUser_Type().equalsIgnoreCase(MyConstants.USER_PT)) {
                                        itemRateDoctor.setVisible(true);
                                        toolbar.setVisibility(View.VISIBLE);
                                    } else {
                                        toolbar.setVisibility(View.GONE);
                                    }
                                    img_video.setVisibility(View.GONE);

                                    Map<String, Object> isEndedConsult = new HashMap<>();
                                    isEndedConsult.put("isEnded", "1");
                                    fireChaneel.updateChildren(isEndedConsult);

                                }
                            } else {
                                System.out.println("error:::" + obj.getString("error_string"));
                                //Utils.showToast(obj.getString("error_string"), FireChatActivity.this);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;

                    case changeDoctor:
                        Utils.removeSimpleSpinProgressDialog();
                        try {
                            JSONObject object = new JSONObject(result);

                            String error_code = object.getString("error_code");

                            if (error_code.equalsIgnoreCase("0")) {

                                JSONArray resultObj = object.getJSONArray("result");
                                JSONObject jsonObject = resultObj.getJSONObject(0);
                                MyConstants.consultId = jsonObject.getString("ConsultId");
                                MyConstants.Wallet_Balance = jsonObject.getString("WalletBalance");

                                MyConstants.isBooked = false;
                                Intent intent = new Intent(FireChatActivity.this, ConfirmPaymentActivity.class);
                                startActivity(intent);
                            } else {
                                Utils.showToast(object.getString("error_string"), FireChatActivity.this);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;
                }

            } else {
                //  Utils.showToast("Please try again later", this);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    public void sendNotificationMessage(String message, boolean isShow) {

        String sender_id = "", user_type = "", is_live = "", reciver_id = "", is_guest = "";
        if (App.user.getUser_Type().equalsIgnoreCase(MyConstants.USER_DR)) {
            user_type = "D";
            reciver_id = pt_id;
            sender_id = dr_id;


        } else {

            if (MyConstants.isGuest.equalsIgnoreCase("1")) {
                sender_id = pt_id;
                user_type = "G";
                reciver_id = dr_id;
            } else {
                sender_id = pt_id;
                user_type = "P";
                reciver_id = dr_id;
            }
        }

        String social_id = sharedpreferences.getString(MyConstants.SOCIAL_ID, "0");
        String social_type = sharedpreferences.getString(MyConstants.LOGIN_TYPE, "0");

        new CallRequest(this).sendOfflineNotification(message, sender_id, reciver_id, user_type, social_id, social_type, IS_GUEST, isShow);
    }
    @Override
    public void onProgressUpdate(String uniqueMessageId, int progres) {
    }
    @Override
    public void onProgressComplete(String uniqueMessageId, String result, Constant.REQUESTS
            request) {
    }
    public void sendSuggestDoctorMessage(FireMessage chatBean) {
        Log.i("Suggest doc", "Suggest doctor is going to send message firebase");
        try {
            sendNewMessage(chatBean);
            if (!isLive) {
                if (App.user.getUser_Type().equalsIgnoreCase(MyConstants.USER_DR)) {
                    sendNotificationMessage("Your patient send an image", false);
                } else {
                    sendNotificationMessage(dr_name + " suggested a doctor", false);
                }
            }

            Map<String, Object> lastValue = new HashMap<>();
            lastValue.put("last_message", "Suggested a doctor");
            lastValue.put("last_date", chatBean.date);

            fireChaneel.updateChildren(lastValue);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void sendCallMessage(FireMessage chatBean) {

        try {
            sendNewMessage(chatBean);

            String message = "";
            if (chatBean.isAudio.equalsIgnoreCase("1")) {
                if (chatBean.callEndCause.equalsIgnoreCase("HUNG_UP")) {
                    message = "Made a voice call";
                } else {
                    message = "Missed a voice call";
                }
            } else {
                if (chatBean.callEndCause.equalsIgnoreCase("HUNG_UP")) {
                    message = "Made a video call";
                } else {
                    message = "Missed a video call";
                }
            }

            if (!isLive) {
                sendNotificationMessage(message, true);
            }

            Map<String, Object> lastValue = new HashMap<>();
            lastValue.put("last_message", message);
            lastValue.put("last_date", chatBean.date);

            fireChaneel.updateChildren(lastValue);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void sendOtherChatCallMessage(FireMessage CallchatBean, String OtherCallUniqueChatID) {

        try {
            DatabaseReference CallFireChaneel = fireDB.child(OtherCallUniqueChatID);
            DatabaseReference CallfireDbMessages = CallFireChaneel.child(ChatConstants.FIRE_DB_MESSAGES);
            try {
                String key = CallfireDbMessages.push().getKey();

                Map<String, Object> postValues = CallchatBean.toMap();
                Map<String, Object> childUpdates = new HashMap<>();
                childUpdates.put(key, postValues);
                CallfireDbMessages.updateChildren(childUpdates);

//                if (!isLive) {
//                    unreadMessageCount++;
//                } else {
//                    unreadMessageCount = 0;
//                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            String message = "";
            if (CallchatBean.isAudio.equalsIgnoreCase("1")) {
                if (CallchatBean.callEndCause.equalsIgnoreCase("HUNG_UP")) {
                    message = "Made a voice call";
                } else {
                    message = "Missed a voice call";
                }
            } else {
                if (CallchatBean.callEndCause.equalsIgnoreCase("HUNG_UP")) {
                    message = "Made a video call";
                } else {
                    message = "Missed a video call";
                }
            }

//            if (!isLive) {
//                sendNotificationMessage(message, true);
//            }

            Map<String, Object> lastValue = new HashMap<>();
            lastValue.put("last_message", message);
            lastValue.put("last_date", CallchatBean.date);

            CallFireChaneel.updateChildren(lastValue);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void sendImageMessage(FireMessage chatBean) {

        try {
            chatBean.date = getUTCDate();
            chatBean.senderId = senderId;
            chatBean.isSent = true;
            chatBean.isReceived = false;

            sendNewMessage(chatBean);
            if (!isLive) {
                if (App.user.getUser_Type().equalsIgnoreCase(MyConstants.USER_DR)) {
                    sendNotificationMessage("Your patient send an image", false);
                } else {
                    sendNotificationMessage(dr_name + " send an image", false);
                }
            }

            Map<String, Object> lastValue = new HashMap<>();
            lastValue.put("last_message", "Sent Photo");
            lastValue.put("last_date", chatBean.date);
            fireChaneel.updateChildren(lastValue);
            Utils.removeSimpleSpinProgressDialog();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void sendVideoMessage(FireMessage chatBean) {

        try {

            chatBean.date = getUTCDate();
            chatBean.isSent = true;
            chatBean.isReceived = false;
            chatBean.senderId = senderId;

            sendNewMessage(chatBean);
            if (!isLive) {
                if (App.user.getUser_Type().equalsIgnoreCase(MyConstants.USER_DR)) {
                    sendNotificationMessage("Your patient send a Video", false);
                } else {
                    sendNotificationMessage(dr_name + " send a Video", false);
                }
            }
            Map<String, Object> lastValue = new HashMap<>();
            lastValue.put("last_message", "Sent Video");
            lastValue.put("last_date", chatBean.date);
            fireChaneel.updateChildren(lastValue);

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    notifyChatAdapter();
                    ///recycleChatView.scrollToPosition(cAdapter.getItemCount() - 1);
                    etChat.setText("");
                }
            });

            Utils.removeSimpleSpinProgressDialog();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void sendAudioMessage(FireMessage chatBean) {
        try {
            chatBean.date = getUTCDate();
            chatBean.isSent = true;
            chatBean.isReceived = false;

            sendNewMessage(chatBean);
            if (!isLive) {
                if (App.user.getUser_Type().equalsIgnoreCase(MyConstants.USER_DR)) {
                    sendNotificationMessage("Your patient send a Voice note", false);
                } else {
                    sendNotificationMessage(dr_name + " send a Voice note", false);
                }
            }


            Map<String, Object> lastValue = new HashMap<>();
            lastValue.put("last_message", "Sent Audio");
            lastValue.put("last_date", chatBean.date);
            fireChaneel.updateChildren(lastValue);

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    notifyChatAdapter();
                    ///recycleChatView.scrollToPosition(cAdapter.getItemCount() - 1);

                    etChat.setText("");
                }
            });
            Utils.removeSimpleSpinProgressDialog();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private void uploadFromUri(FireMessage fireMessage, Uri fileUri, String fileType) {

        Utils.showSimpleSpinProgressDialog(FireChatActivity.this, "Uploading");

        Log.i(IMG_TAG, "Image is going to upload");
        Log.d(IMG_TAG, "uploadFromUri:src:" + fileUri.toString());

        // Save the File URI
        mFileUri = fileUri;
        startService(new Intent(this, MyUploadService.class)
                .putExtra(MyUploadService.EXTRA_FILE_URI, fileUri)
                .putExtra(ChatConstants.FILE_TYPE, fileType)
                .putExtra(ChatConstants.UNIQUE_ID, fireMessage.chatID)
                .setAction(MyUploadService.ACTION_UPLOAD));
    }
    @Override
    public void onStart() {
        super.onStart();

        // Register receiver for uploads and downloads
        LocalBroadcastManager manager = LocalBroadcastManager.getInstance(this);
        manager.registerReceiver(mImageBroadCastreciver, MyDownloadService.getIntentFilter());
        manager.registerReceiver(mImageBroadCastreciver, MyUploadService.getIntentFilter());
    }
    @Override
    public void onStop() {
        super.onStop();

        // Unregister download receiver
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mImageBroadCastreciver);
    }
    public void createDirectories() {
        try {
            File f = new File(Environment.getExternalStorageDirectory() + "/ConnexMd/images");
            if (!f.exists()) {
                f.mkdirs();
            }

            File f2 = new File(Environment.getExternalStorageDirectory() + "/ConnexMd/video");
            if (!f2.exists()) {
                f2.mkdirs();

            }

            File f3 = new File(Environment.getExternalStorageDirectory() + "/ConnexMd/audio");
            if (!f3.exists()) {
                f3.mkdirs();

            }

            File f4 = new File(Environment.getExternalStorageDirectory() + "/ConnexMd/drawings");
            if (!f4.exists()) {
                f4.mkdirs();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void playTone(boolean isRecieve) {
      /*  MediaPlayer mediaPlayer;

        if (isRecieve) {
            mediaPlayer = MediaPlayer.create(getApplicationContext(), R.raw.recive);
        } else {
            mediaPlayer = MediaPlayer.create(getApplicationContext(), R.raw.send);
        }
        try {
            mediaPlayer.setAudioStreamType(AudioManager.STREAM_NOTIFICATION);
            mediaPlayer.prepare();
            mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {

                @Override
                public void onCompletion(MediaPlayer mp) {
                    mp.release();
                }
            });
            mediaPlayer.start();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (SecurityException e) {
            e.printStackTrace();
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }*/
    }
    @Override
    public void onStartFailed(SinchError error) {

    }
    @Override
    public void onStarted() {

    }







    @Override
    public void callIncomingScreen(CallClient callClient, Call call) {

        System.out.println("callIncomingScreen FireChat --->"+ call.getHeaders().toString());

        if (call.getDetails().isVideoOffered()) {
            Intent intent = new Intent(FireChatActivity.this, IncomingCallScreenActivity.class);
            intent.putExtra(CALL_ID, call.getCallId());
            intent.putExtra("TYPE_CALL", "Video");
            intent.putExtra("profile_path", call.getHeaders().get("profile_path"));
            intent.putExtra("name", call.getHeaders().get("name"));
            intent.putExtra("uniqueChatId", call.getHeaders().get("uniqueChatId"));
            intent.putExtra("senderID", call.getHeaders().get("senderID"));
            intent.putExtra("receiverID", call.getHeaders().get("receiverID"));
            //intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            FireChatActivity.this.startActivityForResult(intent,
                    VIDEO_CALL_REQUEST_CODE);
        } else {
            Intent intent = new Intent(FireChatActivity.this, IncomingCallScreenActivity.class);
            intent.putExtra(CALL_ID, call.getCallId());

            intent.putExtra("profile_path", call.getHeaders().get("profile_path"));
            intent.putExtra("name", call.getHeaders().get("name"));
            intent.putExtra("uniqueChatId", call.getHeaders().get("uniqueChatId"));
            intent.putExtra("senderID", call.getHeaders().get("senderID"));
            intent.putExtra("receiverID", call.getHeaders().get("receiverID"));


            if(call.getHeaders().containsKey("isWeb")){
                intent.putExtra("TYPE_CALL", "Video");
                FireChatActivity.this.startActivityForResult(intent, VIDEO_CALL_REQUEST_CODE);
            } else {
                intent.putExtra("TYPE_CALL", "Audio");
                FireChatActivity.this.startActivityForResult(intent, VOICE_CALL_REQUEST_CODE);
            }

           // FireChatActivity.this.startActivityForResult(intent, VOICE_CALL_REQUEST_CODE);
        }
    }
//intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);


    @Override
    public void bookNow(int position) {

        changeDoctor();
    }

    private void changeDoctor() {
        if (!Internet.isAvailable(this)) {
            Internet.showAlertDialog(this, "Error!", "No Internet Connection", false);

            return;
        }

        String social_id = sharedpreferences.getString(MyConstants.SOCIAL_ID, "0");
        String social_type = sharedpreferences.getString(MyConstants.LOGIN_TYPE, "0");

        Map<String, String> map = new HashMap<String, String>();
        if (MyConstants.isGuest.equalsIgnoreCase("0") && App.user.getUser_Type().equalsIgnoreCase(MyConstants.USER_PT)) {
            map.put("url", MyConstants.BASE_URL + "changeDoctor");
            map.put("UserId", App.user.getUserID());
            map.put("SocialType", social_type);
            map.put("SocialId", social_id);
        } else {
            map.put("url", MyConstants.GUEST_BASE_URL + "changeDoctor");
            map.put("GuestId", App.user.getUserID());
            map.put("SocialType", "0");
            map.put("SocialId", "0");
        }
        map.put("ApiToken", MyConstants.API_TOKEN);
        map.put("Version", MyConstants.WS_VERSION);
        map.put("DoctorId", MyConstants.doctorProfile.getDoctorId());
        map.put("UniqueId", uniqueChatId);

        new CallRequest(this).changeDoctor(map);
    }

    public String getUTCDate() {
        try {
            Calendar c = Calendar.getInstance();
            df.setTimeZone(TimeZone.getTimeZone("UTC"));
            return df.format(c.getTime());
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }


}
