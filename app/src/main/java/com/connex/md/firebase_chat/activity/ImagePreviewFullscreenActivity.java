package com.connex.md.firebase_chat.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ProgressBar;

import com.connex.md.R;
import com.connex.md.others.PicassoTrustAll;
import com.connex.md.custom_views.TouchImageView;
import com.squareup.picasso.Callback;

public class ImagePreviewFullscreenActivity extends AppCompatActivity implements View.OnClickListener {

    private TouchImageView mPreviewImg;
    private ProgressBar mProgressbar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_preview_fullscreen);

        initControls();

        if (getIntent().getExtras() != null) {
            setData(getIntent().getExtras().getString("IMAGE"));
        }
    }


    /**
     * initialize all the controls..
     */
    private void initControls() {
        mPreviewImg = findViewById(R.id.img_preview);
        mProgressbar = findViewById(R.id.pBar);

        findViewById(R.id.img_close).setOnClickListener(this);
    }


    /**
     * Display image data...
     */
    private void setData(String image) {
        try {
            PicassoTrustAll.getInstance(this)
                    .load(image)
                    .error(R.drawable.no_img)
                    .into(mPreviewImg, new Callback() {
                        @Override
                        public void onSuccess() {
                            mProgressbar.setVisibility(View.GONE);
                        }

                        @Override
                        public void onError() {
                            mProgressbar.setVisibility(View.GONE);
                            mPreviewImg.setImageDrawable(getResources().getDrawable(R.drawable.no_img));
                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.img_close:
                finish();
                break;
        }
    }
}
