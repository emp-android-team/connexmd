package com.connex.md.firebase_chat.activity;

import android.content.Intent;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RatingBar;
import android.widget.TextView;

import com.connex.md.R;
import com.connex.md.interfaces.AsyncTaskListner;
import com.connex.md.others.App;
import com.connex.md.others.Internet;
import com.connex.md.others.PicassoTrustAll;
import com.connex.md.patient.activity.PTDashboardActivity;
import com.connex.md.ws.CallRequest;
import com.connex.md.ws.Constant;
import com.connex.md.ws.MyConstants;
import com.connex.md.ws.Utils;
import com.squareup.picasso.Callback;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


public class RatingDoctorActivity extends AppCompatActivity implements AsyncTaskListner {

    LinearLayout ivBack;
    RatingBar ratingBar;
    EditText etComment;
    View focusView;
    ImageView ivUser;
    TextView tvName, tvRating, tvTotalRating;
    RatingBar ratingBarIndicator;
    RadioGroup radioGroup;
    RadioButton radio1, radio2, radio3, radio4, radio5, radio6, radioButton;
    Button btnSubmitFeedback;
    String doctor_id, doctorName, rating, totalRating, profilePic, firstName, lastName, comment = "", uniqueChatId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rating_doctor);

        ivBack = findViewById(R.id.ivBack);
        ratingBar = findViewById(R.id.ratingBar1);
        focusView = findViewById(R.id.focusview);
        etComment = findViewById(R.id.etComment);
        ivUser = findViewById(R.id.ivUser);
        tvName = findViewById(R.id.tvName);
        tvRating = findViewById(R.id.tvRating);
        tvTotalRating = findViewById(R.id.tvTotalRatings);
        ratingBarIndicator = findViewById(R.id.ratingBar);
        radioGroup = findViewById(R.id.radioGroup);
        radio1 = findViewById(R.id.radio1);
        radio2 = findViewById(R.id.radio2);
        radio3 = findViewById(R.id.radio3);
        radio4 = findViewById(R.id.radio4);
        radio5 = findViewById(R.id.radio5);
        radio6 = findViewById(R.id.radio6);
        btnSubmitFeedback = findViewById(R.id.btnSubmitFeedback);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            doctor_id = bundle.getString("doctor_id");
            uniqueChatId = bundle.getString(MyConstants.UNIQUE_CHAT_ID);
        }

        focusView.requestFocus();

        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        etComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                etComment.setCursorVisible(true);
            }
        });

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                int selectedId = radioGroup.getCheckedRadioButtonId();
                radioButton = findViewById(selectedId);

                if (radioButton == radio1) {
                    etComment.setVisibility(View.GONE);
                } else if (radioButton == radio2) {
                    etComment.setVisibility(View.GONE);
                } else if (radioButton == radio3) {
                    etComment.setVisibility(View.GONE);
                } else if (radioButton == radio4) {
                    etComment.setVisibility(View.GONE);
                } else if (radioButton == radio5) {
                    etComment.setVisibility(View.GONE);
                } else if (radioButton == radio6) {
                    etComment.setVisibility(View.VISIBLE);
                }
            }
        });

        btnSubmitFeedback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                int selectedId = radioGroup.getCheckedRadioButtonId();
                radioButton = findViewById(selectedId);

                if (radioButton == radio1) {
                    comment = radio1.getText().toString().trim();
                } else if (radioButton == radio2) {
                    comment = radio2.getText().toString().trim();
                } else if (radioButton == radio3) {
                    comment = radio3.getText().toString().trim();
                } else if (radioButton == radio4) {
                    comment = radio4.getText().toString().trim();
                } else if (radioButton == radio5) {
                    comment = radio5.getText().toString().trim();
                } else if (radioButton == radio6) {
                    comment = etComment.getText().toString().trim();
                }

                float rating = ratingBar.getRating();
                if (rating == 0) {
                    Utils.showToast("Please give ratings first", RatingDoctorActivity.this);
                    return;
                }

                giveRatings(rating);

            }
        });

        getDoctorDetails();
    }

    private void giveRatings(float rating) {
        if (!Internet.isAvailable(this)) {
            Internet.showAlertDialog(this, "Error!", "No Internet Connection", false);

            return;
        }

        Map<String, String> map = new HashMap<String, String>();
        if (MyConstants.isGuest.equalsIgnoreCase("0")) {
            map.put("url", MyConstants.BASE_URL + "userConsultRating");
            map.put("UserId", App.user.getUserID());
        } else {
            map.put("url", MyConstants.GUEST_BASE_URL + "guestConsultRating");
            map.put("GuestId", App.user.getUserID());
        }
        map.put("ApiToken", MyConstants.API_TOKEN);
        map.put("Version", MyConstants.WS_VERSION);
        //map.put("DoctorId", doctor_id);
        map.put("UniqueId", uniqueChatId);
        map.put("Rating", String.valueOf(rating));
        map.put("Description", comment);

        new CallRequest(RatingDoctorActivity.this).giveRating(map);
    }

    private void getDoctorDetails() {
        if (!Internet.isAvailable(this)) {
            Internet.showAlertDialog(this, "Error!", "No Internet Connection", false);

            return;
        }

        Map<String, String> map = new HashMap<String, String>();
        map.put("url", MyConstants.BASE_URL + "doctorProfile");
        map.put("ApiToken", MyConstants.API_TOKEN);
        map.put("Version", MyConstants.WS_VERSION);
        map.put("DoctorId", doctor_id);
        map.put("UserId", App.user.getUserID());

        new CallRequest(this).getDoctorProfile(map);
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        Utils.removeSimpleSpinProgressDialog();
        try {
            if (result != null && !result.isEmpty()) {
                Log.i("TAG", "TAG Result : " + result);
                switch (request) {
                    case getDoctorProfile:
                        Utils.removeSimpleSpinProgressDialog();

                        try {
                            JSONObject mainObj = new JSONObject(result);
                            if (mainObj.getString("error_code").equalsIgnoreCase("0")) {
                                if (mainObj.getJSONArray("result") != null && mainObj.getJSONArray("result").length() > 0) {
                                    JSONArray jsonArray = mainObj.getJSONArray("result");
                                    JSONObject object = jsonArray.getJSONObject(0);

                                    JSONObject doctor = object.getJSONObject("doctor");
                                    // rating
                                    JSONObject Rating = doctor.getJSONObject("Rating");

                                    firstName = doctor.getString("FirstName");
                                    lastName = doctor.getString("LastName");
                                    doctorName = firstName + " " + lastName;
                                    profilePic = doctor.getString("ProfilePic");
                                    rating = Rating.getString("Rating");
                                    totalRating = Rating.getString("TotalRating");

                                    tvName.setText(doctorName);
                                    tvRating.setText(rating);
                                    tvTotalRating.setText("("+totalRating+") Ratings");
                                    ratingBarIndicator.setRating(Float.parseFloat(rating));
                                    try {
                                        PicassoTrustAll.getInstance(RatingDoctorActivity.this)
                                                .load(profilePic)
                                                .placeholder(R.drawable.avatar)
                                                .error(R.drawable.avatar)
                                                .into(ivUser, new Callback() {
                                                    @Override
                                                    public void onSuccess() {
                                                    }

                                                    @Override
                                                    public void onError() {
                                                    }
                                                });
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                } else {
                                    Utils.showToast(mainObj.getString("error_string"), RatingDoctorActivity.this);
                                }
                            } else {
                                Utils.showToast(mainObj.getString("error_string"), RatingDoctorActivity.this);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;

                    case rate_doctor:
                        Utils.removeSimpleSpinProgressDialog();

                        try {
                            JSONObject mainObj = new JSONObject(result);
                            if (mainObj.getString("error_code").equalsIgnoreCase("0")) {
                                Intent intent = new Intent(RatingDoctorActivity.this, PTDashboardActivity.class);
                                startActivity(intent);
                                ActivityCompat.finishAffinity(RatingDoctorActivity.this);
                            } else {
                                Utils.showToast(mainObj.getString("error_string"), RatingDoctorActivity.this);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;
                }
            }
        } catch (Exception e) {
            Utils.removeSimpleSpinProgressDialog();
            e.printStackTrace();
            Utils.showToast("Please try again later", this);
        }
    }

    @Override
    public void onProgressUpdate(String uniqueMessageId, int progres) {

    }

    @Override
    public void onProgressComplete(String uniqueMessageId, String result, Constant.REQUESTS request) {

    }
}
