package com.connex.md.firebase_chat.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;

import com.connex.md.R;
import com.connex.md.firebase_chat.interfaces.ItemClickListener;
import com.connex.md.firebase_chat.model.Item;
import com.connex.md.firebase_chat.model.Section;
import com.connex.md.firebase_chat.model.SuggestDoctor;
import com.connex.md.firebase_chat.others.SectionedExpandableLayoutHelper;
import com.connex.md.interfaces.AsyncTaskListner;
import com.connex.md.others.App;
import com.connex.md.others.Internet;
import com.connex.md.ws.CallRequest;
import com.connex.md.ws.Constant;
import com.connex.md.ws.MyConstants;
import com.connex.md.ws.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SuggestDoctorActivity extends AppCompatActivity implements ItemClickListener, AsyncTaskListner {

    RecyclerView rvSuggestDoctors;
    LinearLayout ivBack;
    List<SuggestDoctor> suggestDoctorList = new ArrayList<>();
    SectionedExpandableLayoutHelper sectionedExpandableLayoutHelper;
    public static final int DOCTOR_SUGGEST = 777;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_suggest_doctor);

        rvSuggestDoctors = findViewById(R.id.rvSuggestDoctors);
        ivBack = findViewById(R.id.ivBack);

        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        sectionedExpandableLayoutHelper = new SectionedExpandableLayoutHelper(this,
                rvSuggestDoctors, this, 2);

        getDoctors();

    }

    private void getDoctors() {
        if (!Internet.isAvailable(SuggestDoctorActivity.this)) {
            Internet.showAlertDialog(SuggestDoctorActivity.this, "Error!", "No Internet Connection", false);
            return;
        }

        Map<String, String> map = new HashMap<String, String>();
        map.put("url", MyConstants.DOCTOR_BASE_URL + "referDoctor");
        map.put("DoctorId", App.user.getUserID());
        map.put("ApiToken", MyConstants.API_TOKEN);
        map.put("Version", MyConstants.WS_VERSION);

        new CallRequest(SuggestDoctorActivity.this).getSuggestDoctors(map);
    }

    @Override
    public void itemClicked(Item item) {
        Intent intent = new Intent();
        intent.putExtra("doctorId", item.getId());
        intent.putExtra("doctorName", item.getDoctorName());
        intent.putExtra("doctorURL", item.getProfilePic());
        intent.putExtra("rating", item.getRating());
        intent.putExtra("speciality", item.getDoctorSpeciality());
        intent.putExtra("consult_charge", item.getConsultCharge());
        intent.putExtra("response_time", item.getResponseTime());
        setResult(DOCTOR_SUGGEST, intent);
        finish();
    }

    @Override
    public void itemClicked(Section section) {

    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        try {
            Utils.hideProgressDialog();
            if (result != null && !result.isEmpty()) {
                Log.i("TAG", "TAG Result : " + result);
                suggestDoctorList.clear();
                switch (request) {

                    case suggestDoctors:
                        Utils.removeSimpleSpinProgressDialog();
                        try {
                            JSONObject mainObj = new JSONObject(result);
                            if (mainObj.getString("error_code").equalsIgnoreCase("0")) {

                                if (mainObj.getJSONArray("result") != null && mainObj.getJSONArray("result").length() > 0) {
                                    JSONArray array = mainObj.getJSONArray("result");
                                    for (int i = 0; i < array.length(); i++) {
                                        JSONObject object = array.getJSONObject(i);

                                        JSONObject specialityObj = object.getJSONObject("Speciality");
                                        SuggestDoctor suggestDoctor = new SuggestDoctor();
                                        suggestDoctor.setSpeciality_id(specialityObj.getString("id"));
                                        suggestDoctor.setSpeciality(specialityObj.getString("Speciality"));

                                        JSONArray doctorArray = specialityObj.getJSONArray("Doctor");

                                        List<HashMap<String, String>> doctorList = new ArrayList<>();
                                        if (doctorArray.length() > 0) {
                                            for (int j = 0; j < doctorArray.length(); j++) {
                                                JSONObject doctorObj = doctorArray.getJSONObject(j);

                                                HashMap<String, String> map = new HashMap<>();
                                                map.put("DoctorId", doctorObj.getString("DoctorId"));
                                                map.put("FirstName", doctorObj.getString("FirstName"));
                                                map.put("LastName", doctorObj.getString("LastName"));
                                                map.put("Speciality", doctorObj.getString("Speciality"));
                                                map.put("Rating", doctorObj.getString("Rating"));
                                                map.put("ProfilePic", doctorObj.getString("ProfilePic"));
                                                map.put("IsFeatured", doctorObj.getString("IsFeatured"));
                                                map.put("ConsultCharge", doctorObj.getString("CosultCharge"));
                                                map.put("ResponseTime", doctorObj.getString("ResponseTime"));

                                                doctorList.add(map);
                                            }
                                        }

                                        suggestDoctor.setDoctors(doctorList);

                                        suggestDoctorList.add(suggestDoctor);
                                    }

                                    for (int i = 0; i < suggestDoctorList.size(); i++) {
                                        ArrayList<Item> arrayList = new ArrayList<>();
                                        for (int j = 0; j < suggestDoctorList.get(i).getDoctors().size(); j++) {
                                            arrayList.add(new Item(suggestDoctorList.get(i).getDoctors().get(j).get("FirstName") + " "
                                                    + suggestDoctorList.get(i).getDoctors().get(j).get("LastName"),
                                                    suggestDoctorList.get(i).getDoctors().get(j).get("DoctorId"),
                                                    suggestDoctorList.get(i).getDoctors().get(j).get("Speciality"),
                                                    suggestDoctorList.get(i).getDoctors().get(j).get("Rating"),
                                                    suggestDoctorList.get(i).getDoctors().get(j).get("ProfilePic"),
                                                    suggestDoctorList.get(i).getDoctors().get(j).get("IsFeatured"),
                                                    suggestDoctorList.get(i).getDoctors().get(j).get("ConsultCharge"),
                                                    suggestDoctorList.get(i).getDoctors().get(j).get("ResponseTime")));
                                        }
                                        sectionedExpandableLayoutHelper.addSection(suggestDoctorList.get(i).getSpeciality(), arrayList);
                                    }
                                    sectionedExpandableLayoutHelper.notifyDataSetChanged();

                                } else {
                                    Utils.showToast(mainObj.getString("error_string"), SuggestDoctorActivity.this);
                                }
                            } else {
                                Utils.showToast(mainObj.getString("error_string"), SuggestDoctorActivity.this);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onProgressUpdate(String uniqueMessageId, int progres) {

    }

    @Override
    public void onProgressComplete(String uniqueMessageId, String result, Constant.REQUESTS request) {

    }
}
