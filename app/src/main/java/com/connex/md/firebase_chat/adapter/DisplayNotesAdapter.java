package com.connex.md.firebase_chat.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.connex.md.R;
import com.connex.md.firebase_chat.activity.ImagePreviewFullscreenActivity;
import com.connex.md.model.Notes;
import com.connex.md.others.PicassoTrustAll;
import com.squareup.picasso.Callback;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Created by abc on 11/28/2017.
 */

public class DisplayNotesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Context mContext;
    LayoutInflater inflater;
    private List<Notes> notesList;

    public DisplayNotesAdapter(Context mContext, List<Notes> notesList) {
        this.mContext = mContext;
        this.notesList = notesList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //Inflate the layout, initialize the View Holder
        RecyclerView.ViewHolder viewHolder = null;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        switch (viewType) {
            case 0:
                View v1 = inflater.inflate(R.layout.list_item_note_text, parent, false);
                viewHolder = new ViewHolderText(v1);
                break;

            case 1:
                View v2 = inflater.inflate(R.layout.list_item_note_image, parent, false);
                viewHolder = new ViewHolderImages(v2);
                break;
        }

        return viewHolder;
    }

    @Override
    public int getItemCount() {
        //returns the number of elements the RecyclerView will display
        return (null != notesList ? notesList.size() : 0);
    }

    @Override
    public int getItemViewType(int position) {
        if (!TextUtils.isEmpty(getItem(position).getText())) {
            return 0;
        } else {
            return 1;
        }
    }

    public Notes getItem(int position) {
        return notesList.get(position);
    }

    public class ViewHolderText extends RecyclerView.ViewHolder {

        TextView tvMessage, tvName, tvTime;

        ViewHolderText(View v) {
            super(v);
            tvMessage = v.findViewById(R.id.tvMessage);
            tvName = v.findViewById(R.id.tvName);
            tvTime = v.findViewById(R.id.tvTime);
        }
    }

    public class ViewHolderImages extends RecyclerView.ViewHolder {

        ImageView ivImage;
        ProgressBar progressBar;
        TextView tvName, tvTime;
        View mainView;

        ViewHolderImages(View v) {
            super(v);
            ivImage = v.findViewById(R.id.imgMessage);
            progressBar = v.findViewById(R.id.pBar);
            tvName = v.findViewById(R.id.tvName);
            tvTime = v.findViewById(R.id.tvTime);

            mainView = v;
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        switch (holder.getItemViewType()) {
            case 0:
                ViewHolderText viewHolderText = (ViewHolderText) holder;
                bindTextHolder(viewHolderText, position);
                break;
            case 1:
                ViewHolderImages holderImages = (ViewHolderImages) holder;
                bindImagesHolder(holderImages, position);
                break;
        }
    }

    private void bindImagesHolder(final ViewHolderImages holder, final int position) {
        holder.tvName.setText(notesList.get(position).getFirstName() + " " + notesList.get(position).getLastName());
        System.out.println("date::"+notesList.get(position).getCreated_at());
        holder.tvTime.setText(getFormatedTime(notesList.get(position).getCreated_at()));
        holder.progressBar.setVisibility(View.VISIBLE);
        try {
            PicassoTrustAll.getInstance(mContext)
                    .load(notesList.get(position).getImage())
                    .error(R.drawable.no_img)
                    .into(holder.ivImage, new Callback() {
                        @Override
                        public void onSuccess() {
                            holder.progressBar.setVisibility(View.GONE);
                        }

                        @Override
                        public void onError() {
                            holder.progressBar.setVisibility(View.GONE);
                            holder.ivImage.setImageDrawable(mContext.getResources().getDrawable(R.drawable.no_img));
                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }

        holder.mainView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mContext.startActivity(new Intent(mContext, ImagePreviewFullscreenActivity.class)
                    .putExtra("IMAGE", notesList.get(position).getImage()));
            }
        });
    }


    private void bindTextHolder(ViewHolderText holder, int position) {
        holder.tvName.setText(notesList.get(position).getFirstName() + " " + notesList.get(position).getLastName());
        holder.tvTime.setText(getFormatedTime(notesList.get(position).getCreated_at()));
        holder.tvMessage.setText(notesList.get(position).getText());
         //holder.tvMessage.setText(StringEscapeUtils.unescapeJava(notesList.get(position).getText()));
    }

    public String getFormatedTime(String date) {
        try {
            Log.i("DATE", "IN Android FORMAT");
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);

            SimpleDateFormat newFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm a", Locale.US);
            format.setTimeZone(TimeZone.getTimeZone("UTC"));
            Date currentDate;

            currentDate = format.parse(date);
            newFormat.setTimeZone(TimeZone.getDefault());
            return newFormat.format(currentDate);
        } catch (ParseException e) {
            e.printStackTrace();
            return "";
        }
    }
}
