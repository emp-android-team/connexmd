package com.connex.md.firebase_chat.interfaces;

public interface KeyboardVisibilityListener {
    void onKeyboardVisibilityChanged(boolean keyboardVisible);
}