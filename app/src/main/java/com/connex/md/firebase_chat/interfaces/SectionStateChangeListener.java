package com.connex.md.firebase_chat.interfaces;

/**
 * Created by bpncool on 2/24/2016.
 */


import com.connex.md.firebase_chat.model.Section;

/**
 * interface to listen changes in state of sections
 */
public interface SectionStateChangeListener {
    void onSectionStateChanged(Section section, boolean isOpen);
}