package com.connex.md.firebase_chat.model;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Sagar Sojitra on 2/10/2017.
 */

public class FireMessage {

    public boolean isReceived = false;
    public boolean isSent = false;
    public boolean isTyping = false;
    public boolean isError = false;
    public String isEnded = "0";
    public int percentage = 0;


    public String date = "";
    public String senderId = "";
    public String duration = "";
    public String senderName = "";
    public String text = "";
    public String videoURL = "";
    public String voiceURL = "";
    public String photoURL = "";
    public String localPath = "";
    public String chatID = "";
    public String doctorURL = "";
    public String doctorName = "";
    public String rating = "";
    public String doctorId = "";
    public String consultCharge = "";
    public String sended = "";
    public String responseTime = "";
    public String speciality = "";
    public String isAudio = "";
    public String callEndCause = "";
    public String callerId = "";
    public String calleeId = "";
    public String call = "";
    public String callNotificationMessage;

    public String getCall() {
        return call;
    }

    public void setCall(String call) {
        this.call = call;
    }

    public String getIsAudio() {
        return isAudio;
    }

    public void setIsAudio(String isAudio) {
        this.isAudio = isAudio;
    }

    public String getCallEndCause() {
        return callEndCause;
    }

    public void setCallEndCause(String callEndCause) {
        this.callEndCause = callEndCause;
    }

    public String getCallerId() {
        return callerId;
    }

    public void setCallerId(String callerId) {
        this.callerId = callerId;
    }

    public String getCalleeId() {
        return calleeId;
    }

    public void setCalleeId(String calleeId) {
        this.calleeId = calleeId;
    }

    public String getCallNotificationMessage() {
        return callNotificationMessage;
    }

    public void setCallNotificationMessage(String callNotificationMessage) {
        this.callNotificationMessage = callNotificationMessage;
    }

    public String getResponseTime() {
        return responseTime;
    }

    public void setResponseTime(String responseTime) {
        this.responseTime = responseTime;
    }

    public String getSpeciality() {
        return speciality;
    }

    public void setSpeciality(String speciality) {
        this.speciality = speciality;
    }

    public String getSended() {
        return sended;
    }

    public void setSended(String sended) {
        this.sended = sended;
    }

    public String getConsultCharge() {
        return consultCharge;
    }

    public void setConsultCharge(String consultCharge) {
        this.consultCharge = consultCharge;
    }

    public String getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(String doctorId) {
        this.doctorId = doctorId;
    }

    public String getDoctorURL() {
        return doctorURL;
    }

    public void setDoctorURL(String doctorURL) {
        this.doctorURL = doctorURL;
    }

    public String getDoctorName() {
        return doctorName;
    }

    public void setDoctorName(String doctorName) {
        this.doctorName = doctorName;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getSenderId() {
        return senderId;
    }

    public void setSenderId(String senderId) {
        this.senderId = senderId;
    }

    public String getSenderName() {
        return senderName;
    }

    public void setSenderName(String senderName) {
        this.senderName = senderName;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getVideoURL() {
        return videoURL;
    }

    public void setVideoURL(String videoURL) {
        this.videoURL = videoURL;
    }

    public String getVoiceURL() {
        return voiceURL;
    }

    public void setVoiceURL(String voiceURL) {
        this.voiceURL = voiceURL;
    }

    public String getPhotoURL() {
        return photoURL;
    }

    public void setPhotoURL(String photoURL) {
        this.photoURL = photoURL;
    }


    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("date", date);
        result.put("senderId", senderId);
        result.put("senderName", senderName);
        result.put("isReceived", isReceived);
        result.put("isSent", isSent);
        result.put("isEnded", isEnded);
        result.put("localPath", localPath);
        result.put("isTyping", isTyping);
        result.put("chatID", chatID);
        result.put("percentage", 0);
        result.put("isError", isError);
        result.put("duration", duration);
        if (text != null && !text.isEmpty())
            result.put("text", text);
        if (videoURL != null && !videoURL.isEmpty())
            result.put("videoURL", videoURL);
        if (voiceURL != null && !voiceURL.isEmpty())
            result.put("voiceURL", voiceURL);
        if (photoURL != null && !photoURL.isEmpty())
            result.put("photoURL", photoURL);
        if (sended != null && !sended.isEmpty()) {
            result.put("doctorURL", doctorURL);
            result.put("doctorId", doctorId);
            result.put("doctorName", doctorName);
            result.put("rating", rating);
            result.put("consultCharge", consultCharge);
            result.put("responseTime", responseTime);
            result.put("speciality", speciality);
            result.put("sended", sended);
        }
        if (call != null && !call.isEmpty()) {
            result.put("isAudio", isAudio);
            result.put("callEndCause", callEndCause);
            result.put("call", call);
            result.put("callerId", callerId);
            result.put("calleeId", calleeId);
            result.put("callNotificationMessage", callNotificationMessage);
        }

        return result;
    }
}
