package com.connex.md.firebase_chat.model;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;

/**
 * Created by abc on 1/5/2018.
 */

public class SuggestDoctor implements Serializable {

    String speciality_id, speciality;
    List<HashMap<String, String>> doctors;

    public String getSpeciality_id() {
        return speciality_id;
    }

    public void setSpeciality_id(String speciality_id) {
        this.speciality_id = speciality_id;
    }

    public String getSpeciality() {
        return speciality;
    }

    public void setSpeciality(String speciality) {
        this.speciality = speciality;
    }

    public List<HashMap<String, String>> getDoctors() {
        return doctors;
    }

    public void setDoctors(List<HashMap<String, String>> doctors) {
        this.doctors = doctors;
    }
}
