package com.connex.md.firebase_chat.others;

import android.os.Environment;

/**
 * Created by Sagar Sojitra on 2/10/2017.
 */

public class ChatConstants {

    public static String FIRE_DB_MESSAGES = "messages";
    public static String FIRE_DB_ONLINE = "online";
    public static String FIRE_DB_TYPING = "typingIndicator";
    public static String FIRE_UNREAD_COUNTER = "unread_counter";
    public static String DEFULT_IMAGE_DOWNLOAD_PATH = Environment.getExternalStorageDirectory() + "/ConnexMd/images/";
    public static String DEFULT_AUDIO_DOWNLOAD_PATH = Environment.getExternalStorageDirectory() + "/ConnexMd/audio/";
    public static String DEFULT_VIDEO_DOWNLOAD_PATH = Environment.getExternalStorageDirectory() + "/ConnexMd/video/";


    public static String PERCENTAGE = "percent";
    public static String FILE_TYPE = "type";
    public static String SAVE_PATH = "SAVE-PATH";
    public static String UNIQUE_ID = "uniqueID";
    public static String FILE_TYPE_IMAGE = "image";
    public static String FILE_TYPE_AUDIO = "audio";
    public static String FILE_TYPE_VIDEO = "video";
}
