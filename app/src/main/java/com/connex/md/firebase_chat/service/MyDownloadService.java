package com.connex.md.firebase_chat.service;

import android.content.Intent;
import android.content.IntentFilter;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.connex.md.firebase_chat.others.ChatConstants;
import com.connex.md.firebase_chat.activity.FireChatActivity;
import com.connex.md.R;
import com.firebase.client.annotations.Nullable;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.io.File;

public class MyDownloadService extends MyBaseTaskService {

    private static final String TAG = "Storage#DownloadService";

    /**
     * Actions
     **/
    public static final String ACTION_DOWNLOAD = "action_download";
    public static final String DOWNLOAD_COMPLETED = "download_completed";
    public static final String DOWNLOAD_ERROR = "download_error";

    /**
     * Extras
     **/
    public static final String EXTRA_DOWNLOAD_PATH = "extra_download_path";
    public static final String EXTRA_BYTES_DOWNLOADED = "extra_bytes_downloaded";

    private StorageReference mStorageRef, mDownloadRef;
    public static final String EXTRA_FILE_URI = "extra_file_uri";
    public static final String EXTRA_DOWNLOAD_URL = "extra_download_url";

    public String UNIQUE_CHAT_ID = "";
    public String SAVE_PATH = "";
    public String FILE_TYPE = "";

    public StorageReference photoRef;

    @Override
    public void onCreate() {
        super.onCreate();

        // Initialize Storage

    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "onStartCommand:" + intent + ":" + startId);

        if (ACTION_DOWNLOAD.equals(intent.getAction())) {
            // Get the path to download from the intent
            String downloadPath = intent.getStringExtra(EXTRA_DOWNLOAD_PATH);

            SAVE_PATH = intent.getStringExtra(ChatConstants.SAVE_PATH);
            UNIQUE_CHAT_ID = intent.getStringExtra(ChatConstants.UNIQUE_ID);
            FILE_TYPE  =  intent.getStringExtra(ChatConstants.FILE_TYPE);
            downloadFromPath(downloadPath);
        }

        return START_REDELIVER_INTENT;
    }

    private void downloadFromPath(final String downloadPath) {
        try {
        Log.d(TAG, "downloadFromPath:" + downloadPath);

        String basePath = downloadPath.substring(0, downloadPath.lastIndexOf("/"));
        Log.d(TAG, "downloadFromPath:  base path  :::" + basePath);
        String child = downloadPath.substring(downloadPath.lastIndexOf("/") + 1, downloadPath.length());
        Log.d(TAG, "downloadFromPath:  child  :::" + child);
        mStorageRef = FirebaseStorage.getInstance().getReferenceFromUrl(basePath);
        mDownloadRef = mStorageRef.child(child);
        // Mark task started
        taskStarted();


        Log.d(TAG, "downloadFromPath:  save PATH  :::" + SAVE_PATH.substring(0,SAVE_PATH.lastIndexOf("/")));
       /* File f = new File(SAVE_PATH.substring(0,SAVE_PATH.lastIndexOf("/")));

        if(f.exists()){
            f.mkdirs();
        }*/

        File newFile =new File(SAVE_PATH.substring(0,SAVE_PATH.lastIndexOf("/")), child);
        mDownloadRef.getFile(newFile).addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
                // broadcastDownloadFinished(downloadPath, taskSnapshot.getTotalByteCount());
                showDownloadFinishedNotification(downloadPath, (int) taskSnapshot.getTotalByteCount());

                // Mark task completed
                taskCompleted();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                // Send failure broadcast
                broadcastDownloadFinished(downloadPath, -1);
                showDownloadFinishedNotification(downloadPath, -1);

                // Mark task completed
                taskCompleted();
            }
        });
    } catch (Exception e){
        e.printStackTrace();
    }
    }

    /**
     * Broadcast finished download (success or failure).
     *
     * @return true if a running receiver received the broadcast.
     */
    private boolean broadcastDownloadFinished(String downloadPath, long bytesDownloaded) {
        boolean success = bytesDownloaded != -1;
        String action = success ? DOWNLOAD_COMPLETED : DOWNLOAD_ERROR;

        Intent broadcast = new Intent(action)
                .putExtra(EXTRA_DOWNLOAD_PATH, downloadPath)
                .putExtra(EXTRA_BYTES_DOWNLOADED, bytesDownloaded)
                .putExtra(ChatConstants.SAVE_PATH, SAVE_PATH);

        return LocalBroadcastManager.getInstance(getApplicationContext())
                .sendBroadcast(broadcast);
    }

    /**
     * Show a notification for a finished download.
     */
    private void showDownloadFinishedNotification(String downloadPath, int bytesDownloaded) {

        // Hide the progress notification
        dismissProgressNotification();
        boolean success = bytesDownloaded != -1;
        String caption = success ? getString(R.string.download_success) : getString(R.string.download_failure);

        String action = success ? DOWNLOAD_COMPLETED : DOWNLOAD_ERROR;

        Intent broadcast = new Intent(action)
                .putExtra(EXTRA_DOWNLOAD_PATH, downloadPath)
                .putExtra(EXTRA_BYTES_DOWNLOADED, bytesDownloaded)
                .putExtra(ChatConstants.SAVE_PATH, SAVE_PATH);
        LocalBroadcastManager.getInstance(getApplicationContext())
                .sendBroadcast(broadcast);

        // Make Intent to MainActivity
        Intent intent = new Intent(this, FireChatActivity.class)
                .putExtra(EXTRA_DOWNLOAD_PATH, downloadPath)
                .putExtra(EXTRA_BYTES_DOWNLOADED, bytesDownloaded)
                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);

        showFinishedNotification(caption, intent, true);
    }


    public static IntentFilter getIntentFilter() {
        IntentFilter filter = new IntentFilter();
        filter.addAction(DOWNLOAD_COMPLETED);
        filter.addAction(DOWNLOAD_ERROR);

        return filter;
    }
}