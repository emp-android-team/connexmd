package com.connex.md.firebase_chat.view_holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.connex.md.R;
import com.connex.md.custom_views.RobottoTextView;
import com.connex.md.custom_views.RoundedImageView;

public class ViewHolder extends RecyclerView.ViewHolder {

        public RobottoTextView tvMessage;
        public RobottoTextView date_time;
        public RoundedImageView imgPerson;
        public ImageView imgMessage;
        public ImageView imgDownload;
        public ProgressBar pBar;
        public RobottoTextView tvTime;
        public View viewTranspernt;
        public RobottoTextView tvHeader;


        public ViewHolder(View itemView) {
                super(itemView);

                tvTime = itemView.findViewById(R.id.tvTime);
                tvHeader = itemView.findViewById(R.id.tvHeader);
                imgMessage = itemView.findViewById(R.id.imgMessage);
                imgDownload = itemView.findViewById(R.id.imgDownload);
                pBar = itemView.findViewById(R.id.pBar);
                viewTranspernt = itemView.findViewById(R.id.viewTransprent);
                pBar.setMax(100);
        }
}