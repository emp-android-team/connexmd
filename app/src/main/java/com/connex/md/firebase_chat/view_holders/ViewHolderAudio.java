package com.connex.md.firebase_chat.view_holders;

import android.media.MediaPlayer;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.SeekBar;

import com.connex.md.R;
import com.connex.md.custom_views.RobottoTextView;


public class ViewHolderAudio extends RecyclerView.ViewHolder {

    public RobottoTextView tvDuration;
    public SeekBar seekBar;
    public ImageView imgPlay,imgMic,imgPause;
    public ProgressBar pBar;
    public MediaPlayer player ;
    public RobottoTextView tvTime;
    public RobottoTextView tvHeader;


    public ViewHolderAudio(View itemView) {
        super(itemView);
        seekBar = itemView.findViewById(R.id.seekBar);
        tvDuration = itemView.findViewById(R.id.tvDuration);
        pBar = itemView.findViewById(R.id.pBar);
        imgPause = itemView.findViewById(R.id.imgPause);
        imgPlay = itemView.findViewById(R.id.imgPlay);
        imgMic = itemView.findViewById(R.id.imgSpeaker);
        tvHeader = itemView.findViewById(R.id.tvHeader);
        tvTime = itemView.findViewById(R.id.tvTime);
        player = new MediaPlayer();

    }
}