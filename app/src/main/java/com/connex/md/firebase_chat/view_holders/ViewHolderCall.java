package com.connex.md.firebase_chat.view_holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.connex.md.R;
import com.connex.md.custom_views.RobottoTextView;


public class ViewHolderCall extends RecyclerView.ViewHolder {

    public RobottoTextView tvCallMessage;
    public ImageView ivCall;
    public RobottoTextView tvHeader;

    public ViewHolderCall(View itemView) {
        super(itemView);

        ivCall = itemView.findViewById(R.id.ivCall);
        tvCallMessage = itemView.findViewById(R.id.tvCallMessage);
        tvHeader = itemView.findViewById(R.id.tvHeader);

    }
}