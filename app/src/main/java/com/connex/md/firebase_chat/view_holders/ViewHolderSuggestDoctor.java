package com.connex.md.firebase_chat.view_holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.connex.md.R;
import com.connex.md.custom_views.RobottoTextView;


/**
 * Created by abc on 1/6/2018.
 */

public class ViewHolderSuggestDoctor extends RecyclerView.ViewHolder {

    public TextView tvDoctorName, tvRating, tvTime, tvSpeciality;
    public ImageView ivDoctor,ivDRProfile;
    public RatingBar ratingBar;
    public Button btnBook;
    public RobottoTextView tvHeader;

    public ViewHolderSuggestDoctor(View itemView) {
        super(itemView);

        tvDoctorName = itemView.findViewById(R.id.tvDoctorName);
        tvRating = itemView.findViewById(R.id.tvRating);
        tvTime = itemView.findViewById(R.id.tvTime);
        tvSpeciality = itemView.findViewById(R.id.tvSpeciality);
        ivDoctor = itemView.findViewById(R.id.ivDoctor);
        tvHeader = itemView.findViewById(R.id.tvHeader);
        ivDRProfile = itemView.findViewById(R.id.ivDRProfile);
        ratingBar = itemView.findViewById(R.id.ratingBar);
        btnBook = itemView.findViewById(R.id.btnBook);
    }
}
