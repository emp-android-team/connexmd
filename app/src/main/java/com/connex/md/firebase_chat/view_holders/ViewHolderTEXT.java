package com.connex.md.firebase_chat.view_holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.connex.md.R;
import com.connex.md.custom_views.RobottoTextView;


public class ViewHolderTEXT extends RecyclerView.ViewHolder {

    public RobottoTextView tvMessage;
    public RobottoTextView tvTime;
    public RobottoTextView tvHeader;


    public ViewHolderTEXT(View itemView) {
        super(itemView);

        tvTime = itemView.findViewById(R.id.tvTime);
        tvHeader = itemView.findViewById(R.id.tvHeader);
        tvMessage = itemView.findViewById(R.id.tvMessage);

    }
}