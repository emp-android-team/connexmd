package com.connex.md.firebase_chat.view_holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.connex.md.firebase_chat.typing_indicator.DotsTextView;
import com.connex.md.R;

import com.connex.md.custom_views.RobottoTextView;
import com.github.channguyen.adv.AnimatedDotsView;

public class ViewHolderTyping extends RecyclerView.ViewHolder {


    DotsTextView dotsView;
    public RobottoTextView tvMessage;
    public final AnimatedDotsView red;

    public ViewHolderTyping(View itemView) {
        super(itemView);
        red = itemView.findViewById(R.id.adv_1);

        //  tvMessage= (RobottoTextView) itemView.findViewById(R.id.tvMessage);
        // dotsView = (DotsTextView) itemView.findViewById(R.id.tvDots);


    }
}