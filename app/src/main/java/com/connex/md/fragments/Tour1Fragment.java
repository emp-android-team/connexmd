package com.connex.md.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.connex.md.R;
import com.connex.md.activity.PermissionActivity;
import com.connex.md.interfaces.FragmentChangeListener;

/**
 * A simple {@link Fragment} subclass.
 */
public class Tour1Fragment extends Fragment {

    TextView tvSkip;

    public Tour1Fragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_tour1, container, false);

//        btnGotIt = view.findViewById(R.id.btnGotIt);
        tvSkip = view.findViewById(R.id.tvSkip);

        tvSkip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), PermissionActivity.class);
                startActivity(intent);
                ActivityCompat.finishAffinity(getActivity());
            }
        });

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Fragment fr = new Tour2Fragment();
                FragmentChangeListener fc = (FragmentChangeListener) getActivity();
                if (fc != null) {
                    try {
                        fc.replaceFragment(fr);
                    } catch (Exception e){
                        e.printStackTrace();
                    }
                }
            }
        });

        return view;
    }

}
