package com.connex.md.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.connex.md.R;
import com.connex.md.activity.PermissionActivity;
import com.connex.md.ws.MyConstants;
import com.onesignal.OneSignal;

/**
 * A simple {@link Fragment} subclass.
 */
public class Tour3Fragment extends Fragment {

    TextView tvSkip;

    public Tour3Fragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_tour3, container, false);


        OneSignal.idsAvailable(new OneSignal.IdsAvailableHandler() {
            @Override
            public void idsAvailable(String userId, String registrationId) {
                if (userId != null)
                    MyConstants.DEVICE_ID = userId;
                Log.d("debug", "User:" + userId);
            /*    if (registrationId != null)
                    MyConstants.DEVICE_ID = registrationId;*/
                Log.d("debug", "registrationId:" + registrationId);
            }
        });

        System.out.println("Device Id:::" + MyConstants.DEVICE_ID);

        tvSkip = view.findViewById(R.id.tvSkip);

        tvSkip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), PermissionActivity.class);
                startActivity(intent);
                ActivityCompat.finishAffinity(getActivity());
            }
        });

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), PermissionActivity.class);
                startActivity(intent);
                ActivityCompat.finishAffinity(getActivity());
            }
        });

        return view;
    }

    public int pxToDp(int px) {
        DisplayMetrics displayMetrics = getContext().getResources().getDisplayMetrics();
        return Math.round(px / (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
    }

}
