package com.connex.md.interfaces;

public interface AdapterClickListner {
    public void onRowClick(int pos, String message);
    public void onRowClick(int pos);
}
