package com.connex.md.interfaces;


import com.connex.md.ws.Constant;

/**
 * Created by Sagar Sojitra on 3/4/2016.
 */
public interface AsyncTaskListner {

    void onTaskCompleted(String result, Constant.REQUESTS request);

    void onProgressUpdate(String uniqueMessageId, int progres);

    void onProgressComplete(String uniqueMessageId, String result, Constant.REQUESTS request);
}
