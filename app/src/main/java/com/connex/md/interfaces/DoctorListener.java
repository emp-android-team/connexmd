package com.connex.md.interfaces;

/**
 * Created by abc on 12/20/2017.
 */

public interface DoctorListener {
    void editDetails(int position, String universityName, String startYear, String endYear);
}
