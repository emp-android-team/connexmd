package com.connex.md.interfaces;

import android.support.v4.app.Fragment;

/**
 * Created by abc on 12/5/2017.
 */

public interface FragmentChangeListener {
    void replaceFragment(Fragment fragment);
}
