package com.connex.md.interfaces;

import com.connex.md.model.Specialities;

/**
 * Created by abc on 12/20/2017.
 */

public interface HomeListener {
    void saveRemoveCategory(Specialities specialities, int position);
}
