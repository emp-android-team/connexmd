package com.connex.md.model;

/**
 * Created by abc on 11/23/2017.
 */

public class BookingHistory {

    private String DoctorId, FirstName, LastName, ConsultTime, DoctorCharge, Date, Month, Status, Speciality, ConsultId, ResponseTime, FinalAmount, ProfilePic, IsFree,
            Type, ConsultCharge, Address, ConsultTimeOrignal;

    public String getIsFree() {
        return IsFree;
    }

    public void setIsFree(String isFree) {
        IsFree = isFree;
    }

    public String getProfilePic() {
        return ProfilePic;
    }

    public void setProfilePic(String profilePic) {
        ProfilePic = profilePic;
    }

    public String getFinalAmount() {
        return FinalAmount;
    }

    public void setFinalAmount(String finalAmount) {
        FinalAmount = finalAmount;
    }

    public String getResponseTime() {
        return ResponseTime;
    }

    public void setResponseTime(String responseTime) {
        ResponseTime = responseTime;
    }

    public String getConsultId() {
        return ConsultId;
    }

    public void setConsultId(String consultId) {
        ConsultId = consultId;
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String date) {
        Date = date;
    }

    public String getMonth() {
        return Month;
    }

    public void setMonth(String month) {
        Month = month;
    }

    public String getDoctorId() {
        return DoctorId;
    }

    public void setDoctorId(String doctorId) {
        DoctorId = doctorId;
    }

    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(String firstName) {
        FirstName = firstName;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String lastName) {
        LastName = lastName;
    }

    public String getConsultTime() {
        return ConsultTime;
    }

    public void setConsultTime(String consultTime) {
        ConsultTime = consultTime;
    }

    public String getDoctorCharge() {
        return DoctorCharge;
    }

    public void setDoctorCharge(String doctorCharge) {
        DoctorCharge = doctorCharge;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public String getSpeciality() {
        return Speciality;
    }

    public void setSpeciality(String speciality) {
        Speciality = speciality;
    }

    public String getType() {
        return Type;
    }

    public void setType(String type) {
        Type = type;
    }

    public String getConsultCharge() {
        return ConsultCharge;
    }

    public void setConsultCharge(String consultCharge) {
        ConsultCharge = consultCharge;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public String getConsultTimeOrignal() {
        return ConsultTimeOrignal;
    }

    public void setConsultTimeOrignal(String consultTimeOrignal) {
        ConsultTimeOrignal = consultTimeOrignal;
    }
}
