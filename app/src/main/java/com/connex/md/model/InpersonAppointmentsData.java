package com.connex.md.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class InpersonAppointmentsData implements Serializable {

    @SerializedName("DoctorProfilePic")
    String doctorProfilePic;

    @SerializedName("DoctorFirstName")
    String doctorFirstName;

    @SerializedName("DoctorLastName")
    String doctorLastName;

    @SerializedName("ConsultCharge")
    int consultCharge;

    @SerializedName("ConsultTime")
    String consultTime;

    @SerializedName("ConsultId")
    String consultId;

    @SerializedName("Speciality")
    String speciality;

    @SerializedName("FirstName")
    String firstName;

    @SerializedName("LastName")
    String lastName;

    @SerializedName("DoctorAddress")
    String doctorAddress;

    public String getDoctorProfilePic() {
        return doctorProfilePic;
    }

    public void setDoctorProfilePic(String doctorProfilePic) {
        this.doctorProfilePic = doctorProfilePic;
    }

    public String getDoctorFirstName() {
        return doctorFirstName;
    }

    public void setDoctorFirstName(String doctorFirstName) {
        this.doctorFirstName = doctorFirstName;
    }

    public String getDoctorLastName() {
        return doctorLastName;
    }

    public void setDoctorLastName(String doctorLastName) {
        this.doctorLastName = doctorLastName;
    }

    public int getConsultCharge() {
        return consultCharge;
    }

    public void setConsultCharge(int consultCharge) {
        this.consultCharge = consultCharge;
    }

    public String getConsultTime() {
        return consultTime;
    }

    public void setConsultTime(String consultTime) {
        this.consultTime = consultTime;
    }

    public String getConsultId() {
        return consultId;
    }

    public void setConsultId(String consultId) {
        this.consultId = consultId;
    }

    public String getSpeciality() {
        return speciality;
    }

    public void setSpeciality(String speciality) {
        this.speciality = speciality;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getDoctorAddress() {
        return doctorAddress;
    }

    public void setDoctorAddress(String doctorAddress) {
        this.doctorAddress = doctorAddress;
    }


}
