package com.connex.md.model;

import java.io.Serializable;

/**
 * Created by abc on 12/2/2017.
 */

public class MessageHistory implements Serializable {

    public String DoctorId, BookingId, ConsultTime, FirstName, LastName, ProfilePic, UserId, lastMessage, time, counter, IsGuest, ConsultCharge;

    public String getConsultCharge() {
        return ConsultCharge;
    }

    public void setConsultCharge(String consultCharge) {
        ConsultCharge = consultCharge;
    }

    public String getIsGuest() {
        return IsGuest;
    }

    public void setIsGuest(String isGuest) {
        IsGuest = isGuest;
    }

    public String getLastMessage() {
        return lastMessage;
    }

    public void setLastMessage(String lastMessage) {
        this.lastMessage = lastMessage;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getCounter() {
        return counter;
    }

    public void setCounter(String counter) {
        this.counter = counter;
    }

    public String getUserId() {
        return UserId;
    }

    public void setUserId(String userId) {
        UserId = userId;
    }

    public String getDoctorId() {
        return DoctorId;
    }

    public void setDoctorId(String doctorId) {
        DoctorId = doctorId;
    }

    public String getBookingId() {
        return BookingId;
    }

    public void setBookingId(String bookingId) {
        BookingId = bookingId;
    }

    public String getConsultTime() {
        return ConsultTime;
    }

    public void setConsultTime(String consultTime) {
        ConsultTime = consultTime;
    }

    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(String firstName) {
        FirstName = firstName;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String lastName) {
        LastName = lastName;
    }

    public String getProfilePic() {
        return ProfilePic;
    }

    public void setProfilePic(String profilePic) {
        ProfilePic = profilePic;
    }
}
