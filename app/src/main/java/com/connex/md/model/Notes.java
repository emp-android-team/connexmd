package com.connex.md.model;

import java.io.Serializable;

/**
 * Created by abc on 1/16/2018.
 */

public class Notes implements Serializable {

    public String Text="", Image="",created_at="", FirstName="", LastName="";
    public int id, ConsultId;

    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(String firstName) {
        FirstName = firstName;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String lastName) {
        LastName = lastName;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getConsultId() {
        return ConsultId;
    }

    public void setConsultId(int consultId) {
        ConsultId = consultId;
    }

    public String getText() {
        return Text;
    }

    public void setText(String text) {
        Text = text;
    }

    public String getImage() {
        return Image;
    }

    public void setImage(String image) {
        Image = image;
    }
}
