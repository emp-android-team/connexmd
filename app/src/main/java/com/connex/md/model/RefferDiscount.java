package com.connex.md.model;

/**
 * Created by abc on 12/16/2017.
 */

public class RefferDiscount {

    public String UserId,UserDiscount,UserDiscountDesc;

    public String getUserId() {
        return UserId;
    }

    public void setUserId(String userId) {
        UserId = userId;
    }

    public String getUserDiscount() {
        return UserDiscount;
    }

    public void setUserDiscount(String userDiscount) {
        UserDiscount = userDiscount;
    }

    public String getUserDiscountDesc() {
        return UserDiscountDesc;
    }

    public void setUserDiscountDesc(String userDiscountDesc) {
        UserDiscountDesc = userDiscountDesc;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUserDiscountType() {
        return UserDiscountType;
    }

    public void setUserDiscountType(int userDiscountType) {
        UserDiscountType = userDiscountType;
    }

    public int getUserDiscountActivated() {
        return UserDiscountActivated;
    }

    public void setUserDiscountActivated(int userDiscountActivated) {
        UserDiscountActivated = userDiscountActivated;
    }

    public int getUserDiscountUsed() {
        return UserDiscountUsed;
    }

    public void setUserDiscountUsed(int userDiscountUsed) {
        UserDiscountUsed = userDiscountUsed;
    }

    public int id,UserDiscountType,UserDiscountActivated,UserDiscountUsed;


}
