package com.connex.md.model;

import java.io.Serializable;

/**
 * Created by abc on 11/21/2017.
 */

public class SearchDoctor implements Serializable {

    //String speciality_id, speciality, image, banner_image, description;

    String DoctorId = "", FirstName = "", LastName = "", DotorType = "", CosultCharge = "", Country = "", ProfilePic = "", LocalCharge = "", isNutrition="", IsFree="", AvailableFrom = "";

    public String getIsFree() {
        return IsFree;
    }

    public void setIsFree(String isFree) {
        IsFree = isFree;
    }

    public String getIsNutrition() {
        return isNutrition;
    }

    public void setIsNutrition(String isNutrition) {
        this.isNutrition = isNutrition;
    }

    public String getDoctorId() {
        return DoctorId;
    }

    public void setDoctorId(String doctorId) {
        DoctorId = doctorId;
    }

    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(String firstName) {
        FirstName = firstName;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String lastName) {
        LastName = lastName;
    }

    public String getCosultCharge() {
        return CosultCharge;
    }

    public void setCosultCharge(String cosultCharge) {
        CosultCharge = cosultCharge;
    }

    public String getCountry() {
        return Country;
    }

    public void setCountry(String country) {
        Country = country;
    }

    public String getProfilePic() {
        return ProfilePic;
    }

    public void setProfilePic(String profilePic) {
        ProfilePic = profilePic;
    }

    public String getLocalCharge() {
        return LocalCharge;
    }

    public void setLocalCharge(String localCharge) {
        LocalCharge = localCharge;
    }

    public String getDotorType() {
        return DotorType;
    }

    public void setDotorType(String dotorType) {
        DotorType = dotorType;
    }

    public String getAvailableFrom() {
        return AvailableFrom;
    }

    public void setAvailableFrom(String availableFrom) {
        AvailableFrom = availableFrom;
    }
}
