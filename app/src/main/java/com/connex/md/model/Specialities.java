package com.connex.md.model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Karan - Empiere on 9/13/2017.
 */

public class Specialities implements Serializable {

    public boolean isRandom = false;
    public boolean isHeader = false;

    private boolean isShowSeeMore = false;
    private int seeMorePosition;
    public int id , IsSaved;
    public String Speciality = "";
    public String Image = "";
    public String Desc = "";
    public String Symptoms = "";
    public String IsPartner = "";
    public String ClinicName = "";
    public String DoctorName = "";
    public String Location = "";
    public ArrayList<SearchDoctor> searchDoctorList;

    public ArrayList<SearchDoctor> getSearchDoctorList() {
        return searchDoctorList;
    }

    public void setSearchDoctorList(ArrayList<SearchDoctor> searchDoctorList) {
        this.searchDoctorList = searchDoctorList;
    }

    public int getIsSaved() {
        return IsSaved;
    }

    public void setIsSaved(int isSaved) {
        IsSaved = isSaved;
    }

    public String getSymptoms() {
        return Symptoms;
    }

    public void setSymptoms(String symptoms) {
        Symptoms = symptoms;
    }

    public String getDesc() {
        return Desc;
    }

    public void setDesc(String desc) {
        this.Desc = desc;
    }

    public String created_at = "";
    public String updated_at = "";

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSpeciality() {
        return Speciality;
    }

    public void setSpeciality(String speciality) {
        this.Speciality = speciality;
    }

    public String getImage() {
        return Image;
    }

    public void setImage(String image) {
        this.Image = image;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getIs_partner() {
        return IsPartner;
    }

    public void setIs_partner(String is_partner) {
        this.IsPartner = is_partner;
    }

    public String getClinic_name() {
        return ClinicName;
    }

    public void setClinic_name(String clinic_name) {
        this.ClinicName = clinic_name;
    }

    public String getDoctor_name() {
        return DoctorName;
    }

    public void setDoctor_name(String doctor_name) {
        this.DoctorName = doctor_name;
    }

    public String getLocation() {
        return Location;
    }

    public void setLocation(String location) {
        this.Location = location;
    }

    public boolean isShowSeeMore() {
        return isShowSeeMore;
    }

    public void setShowSeeMore(boolean showSeeMore) {
        isShowSeeMore = showSeeMore;
    }

    public int getSeeMorePosition() {
        return seeMorePosition;
    }

    public void setSeeMorePosition(int seeMorePosition) {
        this.seeMorePosition = seeMorePosition;
    }
}
