package com.connex.md.model;

/**
 * Created by Sagar Sojitra on 5/1/2017.
 */

public class Symptoms {
    public String Symptoms;
    public int id;
    public int SpecialityId;
    public boolean selected = false;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getSpeciality_id() {
        return SpecialityId;
    }

    public void setSpeciality_id(int speciality_id) {
        this.SpecialityId = speciality_id;
    }

    public String getSymptoms() {
        return Symptoms;
    }

    public void setSymptoms(String symptoms) {
        this.Symptoms = symptoms;
    }
}
