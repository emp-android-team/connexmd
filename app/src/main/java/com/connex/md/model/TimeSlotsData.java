package com.connex.md.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class TimeSlotsData implements Serializable {

    @SerializedName("id")
    String id;

    @SerializedName("DoctorId")
    String doctorId;

    @SerializedName("SpecialityId")
    String specialityId;

    @SerializedName("Date")
    String date;

    @SerializedName("StartTime")
    String startTime;

    @SerializedName("EndTime")
    String endTime;

    @SerializedName("Status")
    String status;

    @SerializedName("is_selected")
    boolean isSelected;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(String doctorId) {
        this.doctorId = doctorId;
    }

    public String getSpecialityId() {
        return specialityId;
    }

    public void setSpecialityId(String specialityId) {
        this.specialityId = specialityId;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}
