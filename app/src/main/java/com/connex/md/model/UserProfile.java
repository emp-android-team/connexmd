package com.connex.md.model;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;

/**
 * Created by abc on 12/6/2017.
 */

public class UserProfile implements Serializable {

    String email, phone, Gender="", Smoke="", Allergies, Medications, BirthDate, ProfilePic, FirstName, LastName, BookingCount, SavedCount, height, weight, unit="", unitHeight="";

    List<HashMap<String,String>> allergy, reports, medicines, symptoms;

    public List<HashMap<String, String>> getAllergy() {
        return allergy;
    }

    public void setAllergy(List<HashMap<String, String>> allergy) {
        this.allergy = allergy;
    }

    public List<HashMap<String, String>> getReports() {
        return reports;
    }

    public void setReports(List<HashMap<String, String>> reports) {
        this.reports = reports;
    }

    public List<HashMap<String, String>> getMedicines() {
        return medicines;
    }

    public void setMedicines(List<HashMap<String, String>> medicines) {
        this.medicines = medicines;
    }

    public List<HashMap<String, String>> getSymptoms() {
        return symptoms;
    }

    public void setSymptoms(List<HashMap<String, String>> symptoms) {
        this.symptoms = symptoms;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getBookingCount() {
        return BookingCount;
    }

    public void setBookingCount(String bookingCount) {
        BookingCount = bookingCount;
    }

    public String getSavedCount() {
        return SavedCount;
    }

    public void setSavedCount(String savedCount) {
        SavedCount = savedCount;
    }

    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(String firstName) {
        FirstName = firstName;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String lastName) {
        LastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getGender() {
        return Gender;
    }

    public void setGender(String gender) {
        Gender = gender;
    }

    public String getSmoke() {
        return Smoke;
    }

    public void setSmoke(String smoke) {
        Smoke = smoke;
    }

    public String getAllergies() {
        return Allergies;
    }

    public void setAllergies(String allergies) {
        Allergies = allergies;
    }

    public String getMedications() {
        return Medications;
    }

    public void setMedications(String medications) {
        Medications = medications;
    }

    public String getBirthDate() {
        return BirthDate;
    }

    public void setBirthDate(String birthDate) {
        BirthDate = birthDate;
    }

    public String getProfilePic() {
        return ProfilePic;
    }

    public void setProfilePic(String profilePic) {
        ProfilePic = profilePic;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getUnitHeight() {
        return unitHeight;
    }

    public void setUnitHeight(String unitHeight) {
        this.unitHeight = unitHeight;
    }
}
