package com.connex.md.model;

/**
 * Created by abc on 2/12/2018.
 */

public class WalletHistory {

    String Title, Description, Date, Amount, TxnId, DpReferenceId;

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String date) {
        Date = date;
    }

    public String getAmount() {
        return Amount;
    }

    public void setAmount(String amount) {
        Amount = amount;
    }

    public String getTxnId() {
        return TxnId;
    }

    public void setTxnId(String txnId) {
        TxnId = txnId;
    }

    public String getDpReferenceId() {
        return DpReferenceId;
    }

    public void setDpReferenceId(String dpReferenceId) {
        DpReferenceId = dpReferenceId;
    }
}
