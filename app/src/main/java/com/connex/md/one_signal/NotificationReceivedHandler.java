package com.connex.md.one_signal;

import android.content.SharedPreferences;
import android.util.Log;

import com.connex.md.patient.activity.PTDashboardActivity;
import com.connex.md.ws.MyConstants;
import com.onesignal.OSNotification;
import com.onesignal.OneSignal;

import org.json.JSONObject;

/**
 * Created by abc on 12/22/2017.
 */

public class NotificationReceivedHandler implements OneSignal.NotificationReceivedHandler {

    private SharedPreferences.Editor editor;

    public NotificationReceivedHandler() {
    }

    @Override
    public void notificationReceived(OSNotification notification) {


        JSONObject data = notification.payload.additionalData;
        String customKey;
        System.out.println("data:::" + data); // {"badge":1}

        if (data != null) {
            customKey = data.optString("IsOnline", null);
            if (customKey != null) {
                Log.i("OneSignalExample", "customkey set with value: " + customKey);

                if (PTDashboardActivity.active) {
                    PTDashboardActivity.openChatDialog(data);
                }
            }

            String badge = data.optString("badge", null);
            if (badge != null) {
                Log.i("OneSignalExample", "bade set with value: " + badge);

                String appBadgeCount = PTDashboardActivity.preferences.getString(MyConstants.BADGE_COUNT, "0");
                System.out.println("app badge count:::::" + appBadgeCount);
                if (appBadgeCount.equalsIgnoreCase("0")) {
                    editor = PTDashboardActivity.preferences.edit();
                    editor.putString(MyConstants.BADGE_COUNT, badge);
                    editor.commit();
                    PTDashboardActivity.setBadgeCount();
                } else {
                    int badgeCount = Integer.parseInt(appBadgeCount);
                    badgeCount = badgeCount + 1;
                    editor = PTDashboardActivity.preferences.edit();
                    editor.putString(MyConstants.BADGE_COUNT, String.valueOf(badgeCount));
                    editor.commit();
                    PTDashboardActivity.setBadgeCount();
                }
            }
        }
    }
}
