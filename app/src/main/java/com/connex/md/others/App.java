package com.connex.md.others;

import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.StrictMode;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;
import android.util.Log;
import android.widget.Toast;

import com.connex.md.audio_video_calling.SinchService;
import com.connex.md.one_signal.NotificationOpenedHandler;
import com.connex.md.one_signal.NotificationReceivedHandler;
import com.connex.md.service.GPSTracker;
import com.connex.md.ws.MyConstants;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.instabug.library.Instabug;
import com.instabug.library.invocation.InstabugInvocationEvent;
import com.onesignal.OneSignal;

import java.lang.reflect.Method;
import java.util.List;

/**
 * Created by Sagar Sojitra on 5/16/2016.
 */
public class App extends MultiDexApplication {

    public static App instance;
    public static User user = null;
    public SharedPreferences sharedPref;
    private SharedPreferences.Editor editor;

    public static SinchService.SinchServiceInterface mSinchServiceInterface;


    public App() {
        setInstance(this);
    }

    public static App getInstance() {
        return instance;
    }

    public static void setInstance(App instance) {
        App.instance = instance;
    }

    public static void restart(Context context, int delay) {
        if (delay == 0) {
            delay = 1;
        }
        Log.e("", "restarting app");
        Intent restartIntent = context.getPackageManager()
                .getLaunchIntentForPackage(context.getPackageName());
        @SuppressLint("WrongConstant") PendingIntent intent = PendingIntent.getActivity(
                context, 0,
                restartIntent, Intent.FLAG_ACTIVITY_CLEAR_TOP);

        AlarmManager manager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        manager.set(AlarmManager.RTC, System.currentTimeMillis() + delay, intent);
        System.exit(2);
    }


    @Override
    public void onCreate() {
        super.onCreate();

        OneSignal.startInit(this)
                .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
                .setNotificationReceivedHandler(new NotificationReceivedHandler())
                .setNotificationOpenedHandler(new NotificationOpenedHandler(getApplicationContext()))
                .unsubscribeWhenNotificationsAreDisabled(true)
                .init();

        new Instabug.Builder(this, "07025e9abe6ae3cf75cd6109f3c8ab32")
                .setInvocationEvents(InstabugInvocationEvent.SHAKE, InstabugInvocationEvent.SCREENSHOT_GESTURE)
                .build();

        //ReLinker.loadLibrary(this, "libsinch-android-rtc");
        //FirebaseDatabase.getInstance().setPersistenceEnabled(true);


        if (Build.VERSION.SDK_INT >= 24) {
            try {
                Method m = StrictMode.class.getMethod("disableDeathOnFileUriExposure");
                m.invoke(null);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        GPSTracker gpsTracker = new GPSTracker(App.this);
        gpsTracker.getLocation();

        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
            builder.detectFileUriExposure();
        }

        try {

            FacebookSdk.sdkInitialize(getApplicationContext());
            AppEventsLogger.activateApp(this);

            instance = this;
            user = new User();
            sharedPref = getSharedPreferences(MyConstants.PREF, Context.MODE_PRIVATE);
            MultiDex.install(this);


        } catch (Exception e) {
            e.printStackTrace();
        }

        // Setup handler for uncaught exceptions.
        Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
            @Override
            public void uncaughtException(Thread thread, Throwable e) {
                //handleUncaughtException(thread, e);
            }
        });
    }

    public void handleUncaughtException(Thread thread, Throwable e) {
        e.printStackTrace(); // not all Android versions will print the stack trace automatically

        editor = sharedPref.edit();
        editor.remove(MyConstants.ADDRESS);
        editor.remove(MyConstants.REFER_CODE);
        editor.remove(MyConstants.TOKEN);
        editor.remove(MyConstants.MOBILE);
        editor.remove(MyConstants.SINCH_ID);
        editor.remove(MyConstants.PT_LAST_NAME);
        editor.remove(MyConstants.PT_NAME);
        editor.remove(MyConstants.GENDER);
        editor.remove(MyConstants.DATE_OF_BIRTH);
        editor.remove(MyConstants.AGE);
        editor.remove(MyConstants.PROFILE_PIC);
        editor.remove(MyConstants.COUNTRY);
        editor.remove(MyConstants.LOGIN_TYPE);
        //editor.remove(MyConstants.USER_TYPE);
        editor.remove(MyConstants.WALLET_BALANCE);
        editor.remove(MyConstants.PT_ZONE);
        editor.remove(MyConstants.CURRENT_DISCOUNT);
        editor.remove(MyConstants.USER_ID);
        editor.remove(MyConstants.PT_FIRST_NAME);

        editor.putBoolean(MyConstants.IS_LOGGED_IN, false);
        editor.commit();

        // reload all data
        MyConstants.bookingHistoryList.clear();
        MyConstants.messageHistoryList.clear();
        MyConstants.userChatList.clear();
        MyConstants.doctorChatList.clear();
        MyConstants.isBookingHistoryLoad = true;
        MyConstants.isMessageHistoryLoad = true;
        MyConstants.userProfile = null;
        MyConstants.isProfileLoad = true;
        MyConstants.doctorProfilePersonal = null;
        MyConstants.isDoctorProfileLoad = true;

        restart(this, 2);
    }

    @Override
    public void onTerminate() {

        super.onTerminate();
        AppEventsLogger.deactivateApp(this);
    }


    public static boolean isActivityRunning(Context context, Class activityClass) {
        ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> tasks = activityManager.getRunningTasks(Integer.MAX_VALUE);

        for (ActivityManager.RunningTaskInfo task : tasks) {
            if (activityClass.getCanonicalName().equalsIgnoreCase(task.baseActivity.getClassName()))
                return true;
        }

        return false;
    }


}
