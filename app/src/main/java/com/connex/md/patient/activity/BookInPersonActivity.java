package com.connex.md.patient.activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.connex.md.R;
import com.connex.md.custom_views.CircleImageView;
import com.connex.md.custom_views.RobottoTextView;
import com.connex.md.custom_views.RobottoTextViewBold;
import com.connex.md.interfaces.AsyncTaskListner;
import com.connex.md.model.DoctorProfile;
import com.connex.md.model.TimeSlotsData;
import com.connex.md.others.App;
import com.connex.md.others.Internet;
import com.connex.md.patient.adapter.AvailableSlotsAdapter;
import com.connex.md.ws.CallRequest;
import com.connex.md.ws.Constant;
import com.connex.md.ws.MyConstants;
import com.connex.md.ws.Utils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.sql.Time;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.blackbox_vision.materialcalendarview.internal.data.Day;
import io.blackbox_vision.materialcalendarview.view.CalendarView;

public class BookInPersonActivity extends AppCompatActivity implements AsyncTaskListner {

    @BindView(R.id.rec_available_slots)
    RecyclerView mAvailableSlotsRec;

    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    @BindView(R.id.txt_doctor_name)
    RobottoTextViewBold mDoctorNameTxt;

    @BindView(R.id.txt_speciality)
    RobottoTextView mSpecialityTxt;

    @BindView(R.id.txt_response_time)
    RobottoTextViewBold mResponseTimeTxt;

    @BindView(R.id.txt_fee)
    RobottoTextViewBold mFeeTxt;

    @BindView(R.id.txt_location)
    RobottoTextViewBold mLocationTxt;

    @BindView(R.id.ratingbar)
    RatingBar mRatingBar;

    @BindView(R.id.txt_total_ratings)
    RobottoTextView mTotalRatingTxt;

    @BindView(R.id.calendar_view)
    CalendarView mCalendarView;

    @BindView(R.id.img_profile)
    CircleImageView mProfileImg;

    @BindView(R.id.txt_no_timeslot)
    RobottoTextViewBold mNoTimeslotFoundTxt;

    @BindView(R.id.btn_book_now)
    Button mBookNowBtn;

    private ArrayList<Day> allDisabledDay;

    public String mSelectedTimeStr = "", mTimeSlotId = "", mSelectedEndTime = "", mSelectedDate = "";

    private DoctorProfile mDoctorProfileData;
    private SharedPreferences mSharedPreferences;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bookin_person);

        ButterKnife.bind(this);
        mSharedPreferences = getSharedPreferences(MyConstants.PREF, Context.MODE_PRIVATE);

        setupToolbar();
        mSelectedDate = new SimpleDateFormat("yyyy-MM-dd").format(new Date());

        if (getIntent().getExtras() != null) {
            mDoctorProfileData = (DoctorProfile) getIntent().getExtras().get("DOCTOR_PROFILE");
            setDoctorData();
            getMonthlySlots(mSelectedDate);

        }

        mCalendarView.setOnDateClickListener(onDateClickListener);
        mCalendarView.setOnMonthChangeListener(onMonthChangeListener);
    }


    private void setupToolbar() {
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle("Book In-Person");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }


    @SuppressLint("SetTextI18n")
    private void setDoctorData() {
        mDoctorNameTxt.setText(mDoctorProfileData.getFirstName() + " " + mDoctorProfileData.getLastName());
        mSpecialityTxt.setText(mDoctorProfileData.getSpeciality());
        mResponseTimeTxt.setText(mDoctorProfileData.getResponseTime());
        mFeeTxt.setText("$" + mDoctorProfileData.getCosultCharge() + " (" + Html.fromHtml(mDoctorProfileData.getLocalCharge()) + ")");
        mLocationTxt.setText(mDoctorProfileData.getCountry());

        mRatingBar.setRating(Float.valueOf(mDoctorProfileData.getRating()));
        mTotalRatingTxt.setText("(" + mDoctorProfileData.getTotalRating() + ") Ratings");

        Picasso.with(BookInPersonActivity.this)
                .load(mDoctorProfileData.getProfilePic())
                .placeholder(R.drawable.avatar)
                .error(R.drawable.avatar)
                .into(mProfileImg);
    }


    private void setAvailableSlotsAdapter(List<TimeSlotsData> slotsData) {
        AvailableSlotsAdapter adapter = new AvailableSlotsAdapter(this, slotsData);
        mAvailableSlotsRec.setLayoutManager(new GridLayoutManager(this, 4));
        mAvailableSlotsRec.setAdapter(adapter);
    }



    private void setMonthlySlotsData(List<TimeSlotsData> monthlySlotsData) {
        allDisabledDay = new ArrayList<>();
        for (int i = 0; i < monthlySlotsData.size(); i++) {
            try {
                Date disabledDate = convertStringToDate(monthlySlotsData.get(i).getDate());

                Calendar disabledCalendar = Calendar.getInstance();
                disabledCalendar.setTime(disabledDate);
                Day currentDisableDay = new Day();
                currentDisableDay.setDay(disabledCalendar.get(Calendar.DAY_OF_MONTH))
                        .setMonth(disabledCalendar.get(Calendar.MONTH))
                        .setYear(disabledCalendar.get(Calendar.YEAR));
                allDisabledDay.add(currentDisableDay);

                //System.out.println("Sagar Adapter View  In Disabled -->:::"+ currentDisableDay.getDay() + "-" + currentDisableDay.getMonth() + "-" +currentDisableDay.getYear());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        mCalendarView.setAllDisabledDate(allDisabledDay);
        mCalendarView.update(Calendar.getInstance(Locale.getDefault()));
    }

    public Date convertStringToDate(String dtStart ){
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        try {
            return format.parse(dtStart);

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return  null;
    }


    /**
     * Used to get list of monthly slots..
     * @param month
     */
    private void getMonthlySlots(String month) {
        if (!Internet.isAvailable(this)) {
            Internet.showAlertDialog(this, "Error!", "No Internet Connection", false);
            return;
        }

        Map<String, String> map = new HashMap<String, String>();
        map.put("url", MyConstants.BASE_URL + "getMonthlySlots");
        map.put("ApiToken", MyConstants.API_TOKEN);
        map.put("UserId", App.user.getUserID());
        map.put("Version", MyConstants.WS_VERSION);
        map.put("DoctorId", mDoctorProfileData.getDoctorId());
        map.put("Month", month);
        map.put("SocialId", mSharedPreferences.getString(MyConstants.SOCIAL_ID, "0"));
        map.put("SocialType", mSharedPreferences.getString(MyConstants.LOGIN_TYPE, "0"));

        new CallRequest(BookInPersonActivity.this).getMonthlySlots(map);
    }


    private void getDailySlots(String date) {
        if (!Internet.isAvailable(this)) {
            Internet.showAlertDialog(this, "Error!", "No Internet Connection", false);
            return;
        }

        Map<String, String> map = new HashMap<String, String>();
        map.put("url", MyConstants.BASE_URL + "getDailySlots");
        map.put("ApiToken", MyConstants.API_TOKEN);
        map.put("UserId", App.user.getUserID());
        map.put("Version", MyConstants.WS_VERSION);
        map.put("DoctorId", mDoctorProfileData.getDoctorId());
        map.put("Date", date);
        map.put("SocialId", mSharedPreferences.getString(MyConstants.SOCIAL_ID, "0"));
        map.put("SocialType", mSharedPreferences.getString(MyConstants.LOGIN_TYPE, "0"));

        new CallRequest(BookInPersonActivity.this).getDailySlots(map);
    }

    CalendarView.OnDateClickListener onDateClickListener = new CalendarView.OnDateClickListener() {
        @Override
        public void onDateClick(@NonNull Date selectedDate) {
            try{
                Calendar selectedDay = Calendar.getInstance();
                selectedDay.setTime(selectedDate);
                System.out.println("SelectedDate---- " + selectedDate);

                Date selDate = new SimpleDateFormat("yyyy-MM-dd").parse(new SimpleDateFormat("yyyy-MM-dd").format(selectedDate));
                Date todayDate = new SimpleDateFormat("yyyy-MM-dd").parse(new SimpleDateFormat("yyyy-MM-dd").format(new Date()));

                Day currentDisableDay = new Day();
                currentDisableDay.setDay(selectedDay.get(Calendar.DAY_OF_MONTH))
                        .setMonth(selectedDay.get(Calendar.MONTH))
                        .setYear(selectedDay.get(Calendar.YEAR));

                System.out.println("currentDisableDay-- " + currentDisableDay.getMonth());

                if (selDate.before(todayDate)) {
                    Utils.showAlert("Sorry!! You cannot book an appointment with previous date", BookInPersonActivity.this);

                }else if (!mCalendarView.checkIfDateDisabled(allDisabledDay, currentDisableDay)) {
                    Utils.showAlert("There are no openings on this date please pick another day.", BookInPersonActivity.this);

                }else {
                    //(selDate.equals(todayDate) || selDate.after(todayDate))
                    mSelectedDate = new SimpleDateFormat("yyyy-MM-dd").format(selectedDate);
                    getDailySlots(mSelectedDate);

                }

            }catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    CalendarView.OnMonthChangeListener onMonthChangeListener = new CalendarView.OnMonthChangeListener() {
        @Override
        public void onMonthChange(@NonNull Date monthDate) {
            try{
                Calendar cal = Calendar.getInstance();
                cal.setTime(monthDate);
                cal.set(Calendar.DAY_OF_MONTH, cal.getActualMinimum(Calendar.DAY_OF_MONTH));

                Calendar today = Calendar.getInstance();
                if (cal.get(Calendar.MONTH) == today.get(Calendar.MONTH) && cal.get(Calendar.YEAR) == today.get(Calendar.YEAR)) {
                    getDailySlots(new SimpleDateFormat("yyyy-MM-dd").format(new Date()));
                    mAvailableSlotsRec.setVisibility(View.VISIBLE);
                    mBookNowBtn.setVisibility(View.VISIBLE);

                }else if (cal.before(today)) {
                    mAvailableSlotsRec.setVisibility(View.GONE);
                    mBookNowBtn.setVisibility(View.GONE);

                }else if (cal.after(today)) {
                    getDailySlots(new SimpleDateFormat("yyyy-MM-dd").format(cal.getTime()));
                    mAvailableSlotsRec.setVisibility(View.VISIBLE);
                    mBookNowBtn.setVisibility(View.VISIBLE);
                }

            }catch (Exception e) {
                e.printStackTrace();
            }
        }
    };



    @OnClick(R.id.btn_book_now)
    void onBookNowClick() {
        if (mSelectedTimeStr.equals("")) {
            Utils.showAlert("Please select time first", this);
            return;
        }

        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("hh:mm:ss");
            Date date1 = simpleDateFormat.parse(mSelectedTimeStr);
            Date date2 = simpleDateFormat.parse(mSelectedEndTime);

            long difference = date2.getTime() - date1.getTime();
            int days = (int) (difference / (1000*60*60*24));
            int hours = (int) ((difference - (1000*60*60*24*days)) / (1000*60*60));
            int min = (int) (difference - (1000*60*60*24*days) - (1000*60*60*hours)) / (1000*60);
            hours = (hours < 0 ? -hours : hours);
            Log.i("======= min"," :: "+min);

            startActivity(new Intent(BookInPersonActivity.this, ConfirmAppointmentActivity.class)
                    .putExtra("DOCTOR_PROFILE", mDoctorProfileData)
                    .putExtra("DATE", mSelectedDate)
                    .putExtra("TIME", mSelectedTimeStr)
                    .putExtra("TIME_SLOT_ID", mTimeSlotId)
                    .putExtra("duration", String.valueOf(min)));

        }catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        Utils.removeSimpleSpinProgressDialog();

        try {
            if (result != null && !result.isEmpty()) {
                JSONObject jsonObject = new JSONObject(result);
                Gson gson = new Gson();

                Log.i("TAG", "TAG Result : " + result);

                switch (request) {
                    case getMonthlySlots:
                        List<TimeSlotsData> monthySlots = (List<TimeSlotsData>) gson.fromJson(jsonObject.get("result").toString(), new TypeToken<List<TimeSlotsData>>(){}.getType());

                        getDailySlots(mSelectedDate);
                        setMonthlySlotsData(monthySlots);
                        break;

                    case getDailySlots:
                        List<TimeSlotsData> slotsData = (List<TimeSlotsData>) gson.fromJson(jsonObject.get("result").toString(), new TypeToken<List<TimeSlotsData>>(){}.getType());

                        setAvailableSlotsAdapter(slotsData);
                        if (slotsData.size() > 0) {
                            mNoTimeslotFoundTxt.setVisibility(View.GONE);
                            mAvailableSlotsRec.setVisibility(View.VISIBLE);
                            mBookNowBtn.setVisibility(View.VISIBLE);

                        }else {
                            mNoTimeslotFoundTxt.setVisibility(View.VISIBLE);
                            mAvailableSlotsRec.setVisibility(View.GONE);
                            mBookNowBtn.setVisibility(View.GONE);
                        }
                        break;
                }
            }
        }catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onProgressUpdate(String uniqueMessageId, int progres) {

    }

    @Override
    public void onProgressComplete(String uniqueMessageId, String result, Constant.REQUESTS request) {

    }
}
