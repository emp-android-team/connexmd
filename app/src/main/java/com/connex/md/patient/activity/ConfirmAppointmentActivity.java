package com.connex.md.patient.activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.ContentFrameLayout;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.connex.md.R;
import com.connex.md.custom_views.CircleImageView;
import com.connex.md.custom_views.RobottoEditTextView;
import com.connex.md.custom_views.RobottoTextView;
import com.connex.md.custom_views.RobottoTextViewBold;
import com.connex.md.interfaces.AsyncTaskListner;
import com.connex.md.model.DoctorProfile;
import com.connex.md.model.RefferDiscount;
import com.connex.md.model.TimeSlotsData;
import com.connex.md.others.App;
import com.connex.md.others.Internet;
import com.connex.md.patient.adapter.ConfirmPaymentCommentsAdapter;
import com.connex.md.ws.CallRequest;
import com.connex.md.ws.Constant;
import com.connex.md.ws.JsonParserUniversal;
import com.connex.md.ws.MyConstants;
import com.connex.md.ws.Utils;
import com.github.paolorotolo.expandableheightlistview.ExpandableHeightListView;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ConfirmAppointmentActivity extends AppCompatActivity implements AsyncTaskListner {

    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    @BindView(R.id.txt_doctor_name)
    RobottoTextViewBold mDrNameTxt;

    @BindView(R.id.txt_speciality)
    RobottoTextView mSpecialityTxt;

    @BindView(R.id.edt_time)
    RobottoEditTextView mTimeEdt;

    @BindView(R.id.edt_duration)
    RobottoEditTextView mDurationEdt;

    @BindView(R.id.edt_reason)
    RobottoEditTextView mReasonEdt;

    @BindView(R.id.edt_phone)
    RobottoEditTextView mPhoneEdt;

    @BindView(R.id.img_profile)
    CircleImageView mProfileImg;

    @BindView(R.id.txt_fee)
    RobottoTextViewBold mFeeTxt;

    @BindView(R.id.txt_location)
    RobottoTextViewBold mLocationTxt;

    @BindView(R.id.ratingbar)
    RatingBar mRatingBar;

    @BindView(R.id.txt_total_ratings)
    RobottoTextView mTotalRatingTxt;

    @BindView(R.id.tvInstantBook)
    RobottoTextViewBold mInstantBookTxt;

    @BindView(R.id.tvDiscount)
    RobottoTextViewBold mDiscountTxt;

    @BindView(R.id.tvTotal)
    RobottoTextViewBold mTotalTxt;

    @BindView(R.id.tvWalletBalance)
    RobottoTextView mWalletBalanceTxt;

    @BindView(R.id.cbWallet)
    CheckBox mWalletCb;

    @BindView(R.id.lvComments)
    ExpandableHeightListView mCommentsList;

    @BindView(R.id.btnPayNow)
    Button mPayNowBtn;

    @BindView(R.id.btnCheckout)
    Button mCheckoutBtn;


    private String mSelectedDate = "", mSelectedTimeSlot = "", mDuration = "", mTimeSloatId = "";
    private String ResponseTime, ConsultCharge, walletCharge = "";
    private int finalAmount = 0, finalDiscountAmount = 0;

    public List<RefferDiscount> refferList = new ArrayList<>();

    private ConfirmPaymentCommentsAdapter confirmPaymentCommentsAdapter;
    private JsonParserUniversal jParser;

    private DoctorProfile mDoctorProfileData;
    private SharedPreferences mSharedPreferences;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirm_appointment);

        jParser = new JsonParserUniversal();
        mSharedPreferences = getSharedPreferences(MyConstants.PREF, Context.MODE_PRIVATE);
        ButterKnife.bind(this);
        setupToolbar();

        if (getIntent().getExtras() != null) {
            mDoctorProfileData = (DoctorProfile) getIntent().getExtras().get("DOCTOR_PROFILE");
            mSelectedDate = getIntent().getStringExtra("DATE");
            mSelectedTimeSlot = getIntent().getStringExtra("TIME");
            mDuration = getIntent().getStringExtra("duration");
            mTimeSloatId = getIntent().getStringExtra("TIME_SLOT_ID");
            ConsultCharge = "18";

            setData();
        }

        if (MyConstants.isGuest.equalsIgnoreCase("0") && MyConstants.isFree.equalsIgnoreCase("0")) {
            new CallRequest(this).getReferredDiscount(App.user.getUserID());
        } else {
            setData();
        }
    }


    private void setupToolbar() {
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle("Confirm Your Appointment");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }


    private boolean isValid() {
        if (mReasonEdt.getText().toString().trim().equals("")) {
            mReasonEdt.setError("Please enter reason");
            mReasonEdt.requestFocus();
            return false;

        }else if (mPhoneEdt.getText().toString().trim().equals("")) {
            mPhoneEdt.setError("Please enter phone");
            mPhoneEdt.requestFocus();
            return false;
        }
        return true;
    }


    @SuppressLint("SetTextI18n")
    private void setData() {
        mDrNameTxt.setText(mDoctorProfileData.getFirstName() + " " + mDoctorProfileData.getLastName());
        mSpecialityTxt.setText(mDoctorProfileData.getSpeciality());
        mFeeTxt.setText("$" + mDoctorProfileData.getCosultCharge() + " (" + Html.fromHtml(mDoctorProfileData.getLocalCharge()) + ")");
        mLocationTxt.setText(mDoctorProfileData.getCountry());
        mDurationEdt.setText("Consultation - " + mDuration + " mins");

        mRatingBar.setRating(Float.valueOf(mDoctorProfileData.getRating()));
        mTotalRatingTxt.setText("(" + mDoctorProfileData.getTotalRating() + ") Ratings");

        try {
            String day = new SimpleDateFormat("EEEE").format(new SimpleDateFormat("yyyy-MM-dd").parse(mSelectedDate));
            String time = new SimpleDateFormat("hh:mm a").format(new SimpleDateFormat("hh:mm:ss").parse(mSelectedTimeSlot));
            String date = new SimpleDateFormat("MMM dd, yyyy").format(new SimpleDateFormat("yyyy-MM-dd").parse(mSelectedDate));
            mTimeEdt.setText(day + " at " + time + "(" + date + ")");

        }catch (Exception e) {
            e.printStackTrace();
        }


        Picasso.with(ConfirmAppointmentActivity.this)
                .load(mDoctorProfileData.getProfilePic())
                .placeholder(R.drawable.avatar)
                .error(R.drawable.avatar)
                .into(mProfileImg);


        int discount = 0;
        int counsultCharge = 0;

        int discountAmount = 0;
        try {
            counsultCharge = Integer.parseInt(ConsultCharge);
            if (refferList.size() > 0) {
                discount = Integer.parseInt(refferList.get(0).getUserDiscount());
                discountAmount = (counsultCharge * discount / 100);
                finalAmount = counsultCharge - discountAmount;
            } else {
                finalAmount = counsultCharge;
            }

            mInstantBookTxt.setText("$" + counsultCharge);
            mDiscountTxt.setText("-$" + discountAmount);
            mTotalTxt.setText("$" + String.valueOf(finalAmount));

        } catch (NumberFormatException e) {
            e.printStackTrace();

            finalAmount = Integer.parseInt(MyConstants.doctorProfile.getCosultCharge());
            mInstantBookTxt.setText("$" + MyConstants.doctorProfile.getCosultCharge());
            mDiscountTxt.setText("-$0");
            mTotalTxt.setText("$" + MyConstants.doctorProfile.getCosultCharge());
            //btnCheckout.setText("Proceed Checkout for $" + finalAmount);
        }

        finalDiscountAmount = discountAmount;
        final int finalCharge = finalAmount;

        if (MyConstants.isFree.equalsIgnoreCase("1")) {
            mPayNowBtn.setVisibility(View.VISIBLE);
            mCheckoutBtn.setVisibility(View.GONE);
            mInstantBookTxt.setText("$0");
            mDiscountTxt.setText("-$0");
            mTotalTxt.setText("$0");

        } else {
            mPayNowBtn.setVisibility(View.GONE);
            mCheckoutBtn.setVisibility(View.VISIBLE);
        }

        mWalletCb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    int walletBalance = Integer.parseInt(MyConstants.Wallet_Balance);
                    if (walletBalance < finalAmount) {
                        int remainingAmount = finalAmount - walletBalance;

                        //btnCheckout.setText("Proceed Checkout for $" + remainingAmount);
                        mPayNowBtn.setVisibility(View.GONE);
                        mCheckoutBtn.setVisibility(View.VISIBLE);

                        finalAmount = remainingAmount;
                        walletCharge = String.valueOf(walletBalance);
                    } else {
                        mPayNowBtn.setVisibility(View.VISIBLE);
                        mCheckoutBtn.setVisibility(View.GONE);

                        finalAmount = finalCharge;
                        walletCharge = String.valueOf(finalCharge);
                    }

                } else {
                    //btnCheckout.setText("Proceed Checkout for $" + finalCharge);
                    mPayNowBtn.setVisibility(View.GONE);
                    mCheckoutBtn.setVisibility(View.VISIBLE);

                    finalAmount = finalCharge;
                    walletCharge = "";
                }

                mTotalTxt.setText("$" + String.valueOf(finalAmount));
            }
        });

        if (MyConstants.Wallet_Balance.equalsIgnoreCase("0") || MyConstants.isFree.equalsIgnoreCase("1")) {
            mWalletCb.setChecked(false);
        } else {
            mWalletCb.setChecked(true);
        }

        List<HashMap<String, String>> commentList = new ArrayList<>();
        for (int i = 0; i < 3; i++) {
            HashMap<String, String> comment = new HashMap<>();
            comment.put("name", "Sagar Sojitra");
//            comment.put("date", "Nov 14 2017");
            comment.put("location", "Brisbane, Australia");
            comment.put("comment", "A search for 'lorem ipsum' will uncover many web sites still in their infancy");

            commentList.add(comment);
        }


        confirmPaymentCommentsAdapter = new ConfirmPaymentCommentsAdapter(ConfirmAppointmentActivity.this, commentList);
        mCommentsList.setAdapter(confirmPaymentCommentsAdapter);

        mCommentsList.setExpanded(true);
    }


    private void bookInPersonConsult() {
        if (!Internet.isAvailable(this)) {
            Internet.showAlertDialog(this, "Error!", "No Internet Connection", false);
            return;
        }

        Map<String, String> map = new HashMap<String, String>();
        map.put("url", MyConstants.BASE_URL + "bookInPersonConsult");
        map.put("ApiToken", MyConstants.API_TOKEN);
        map.put("UserId", App.user.getUserID());
        map.put("Version", MyConstants.WS_VERSION);
        map.put("SocialId", mSharedPreferences.getString(MyConstants.SOCIAL_ID, "0"));
        map.put("SocialType", mSharedPreferences.getString(MyConstants.LOGIN_TYPE, "0"));
        map.put("DoctorId", mDoctorProfileData.getDoctorId());
        map.put("DoctorCharge", mDoctorProfileData.getCosultCharge());
        map.put("ConsultTime", mSelectedDate + " " + mSelectedTimeSlot);
        map.put("ReasonForConsult", mReasonEdt.getText().toString());
        map.put("PhoneNumberToReach", mPhoneEdt.getText().toString());

        new CallRequest(ConfirmAppointmentActivity.this).bookInPersonConsult(map);
    }


    private void doPayment(int finalDiscountAmount) {
        String timestamp = new SimpleDateFormat("yyyyMMddHHmmss", Locale.US).format(new Date());

        Map<String, String> map = new HashMap<String, String>();
        if (MyConstants.isGuest.equalsIgnoreCase("1")) {
            map.put("url", MyConstants.GUEST_BASE_URL + "guestConsultPayment");
            map.put("GuestId", App.user.getUserID());
        } else {
            map.put("url", MyConstants.BASE_URL + "consultPayment");
            map.put("UserId", App.user.getUserID());
        }
        map.put("ApiToken", MyConstants.API_TOKEN);
        map.put("Version", MyConstants.WS_VERSION);
        map.put("ConsultId", MyConstants.consultId);
        map.put("Payment", String.valueOf(finalAmount));
        map.put("DiscountAmount", String.valueOf(finalDiscountAmount));
        map.put("PaymentType", "4");
        map.put("WalletAmount", walletCharge);
        if (refferList.size() > 0) {
            map.put("OfferId", String.valueOf(refferList.get(0).getId()));
            map.put("OfferType", "1");
        } else {
            map.put("OfferId", "");
            map.put("OfferType", "");
        }
        map.put("TxnId", "wallet_" + timestamp);

        new CallRequest(ConfirmAppointmentActivity.this).createBookingPayment(map);
    }

    private void doFreePayment(int finalDiscountAmount) {

        String timestamp = new SimpleDateFormat("yyyyMMddHHmmss", Locale.US).format(new Date());

        Map<String, String> map = new HashMap<String, String>();
        if (MyConstants.isGuest.equalsIgnoreCase("1")) {
            map.put("url", MyConstants.GUEST_BASE_URL + "guestConsultPayment");
            map.put("GuestId", App.user.getUserID());
        } else {
            map.put("url", MyConstants.BASE_URL + "consultPayment");
            map.put("UserId", App.user.getUserID());
        }
        map.put("ApiToken", MyConstants.API_TOKEN);
        map.put("Version", MyConstants.WS_VERSION);
        map.put("ConsultId", MyConstants.consultId);
        map.put("Payment", String.valueOf(finalAmount));
        map.put("DiscountAmount", "0");
        map.put("PaymentType", "5");
        map.put("WalletAmount", "");
        map.put("OfferId", "");
        map.put("OfferType", "");
        map.put("TxnId", "free_" + timestamp);

        new CallRequest(ConfirmAppointmentActivity.this).createBookingPayment(map);
    }


    @OnClick(R.id.btnPayNow)
    void onPayNowClick() {
        if (!isValid())
            return;

        if (MyConstants.isFree.equalsIgnoreCase("1")) {
            doFreePayment(finalDiscountAmount);
        } else {
            doPayment(finalDiscountAmount);
        }
    }


    @OnClick(R.id.btnCheckout)
    void onCheckoutClick() {
        if (!isValid())
            return;

        Intent intent = new Intent(ConfirmAppointmentActivity.this, PaymentActivity.class);
        intent.putExtra("inPersonVisit", true);
        intent.putExtra("DOCTOR_PROFILE", mDoctorProfileData);
        intent.putExtra("ConsultTime", mSelectedDate + " " + mSelectedTimeSlot);
        intent.putExtra("TIME_SLOT_ID", mTimeSloatId);
        intent.putExtra("ReasonForConsult", mReasonEdt.getText().toString().trim());
        intent.putExtra("PhoneNumberToReach", mPhoneEdt.getText().toString().trim());
        startActivity(intent);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }



    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        Utils.removeSimpleSpinProgressDialog();

        try {
            if (result != null && !result.isEmpty()) {
                JSONObject jsonObject = new JSONObject(result);
                Gson gson = new Gson();

                Log.i("TAG", "TAG Result : " + result);

                switch (request) {
                    case bookInPersonConsult:
                        if (jsonObject.getString("error_code").equals("0")) {
                            Toast.makeText(ConfirmAppointmentActivity.this, jsonObject.getString("error_string"), Toast.LENGTH_LONG).show();
                            Intent intent = new Intent(ConfirmAppointmentActivity.this, PTDashboardActivity.class);
                            startActivity(intent);
                            ActivityCompat.finishAffinity(ConfirmAppointmentActivity.this);

                        }else if (jsonObject.getString("error_code").equals("")) {
                            Utils.showAlert(jsonObject.getString("error_string"), ConfirmAppointmentActivity.this);
                        }
                        break;

                    case getReferredDiscount:
                        try {
                            JSONObject mainObj = new JSONObject(result);
                            if (mainObj.getString("error_code").equalsIgnoreCase("0")) {
                                Log.i("TAG", " In ERROR 0 ");
                                JSONArray jsonArray = mainObj.getJSONArray("result");
                                refferList.clear();

                                for (int i = 0; i < jsonArray.length(); i++) {
                                    Log.i("TAG", " In For " + i);
                                    RefferDiscount desc = (RefferDiscount) jParser.parseJson(jsonArray.getJSONObject(i), new RefferDiscount());
                                    refferList.add(desc);
                                }
                                setData();

                            } else {
                                setData();
                                //Utils.showToast(mainObj.getString("error_string"), ConfirmPaymentActivity.this);

                            }
                        } catch (JSONException e) {
                            setData();
                            e.printStackTrace();

                        }
                        break;
                }
            }
        }catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onProgressUpdate(String uniqueMessageId, int progres) {

    }

    @Override
    public void onProgressComplete(String uniqueMessageId, String result, Constant.REQUESTS request) {

    }
}
