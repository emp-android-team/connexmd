package com.connex.md.patient.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;

import com.connex.md.R;
import com.connex.md.firebase_chat.activity.FireChatActivity;
import com.connex.md.interfaces.AsyncTaskListner;
import com.connex.md.model.DoctorProfile;
import com.connex.md.model.RefferDiscount;
import com.connex.md.others.App;
import com.connex.md.others.Internet;
import com.connex.md.others.PicassoTrustAll;
import com.connex.md.patient.adapter.ConfirmPaymentCommentsAdapter;
import com.connex.md.patient.adapter.MentionQueryAdapter;
import com.connex.md.utils.VerticalSpacingDecoration;
import com.connex.md.ws.CallRequest;
import com.connex.md.ws.Constant;
import com.connex.md.ws.JsonParserUniversal;
import com.connex.md.ws.MyConstants;
import com.connex.md.ws.Utils;
import com.github.paolorotolo.expandableheightlistview.ExpandableHeightListView;
import com.github.siyamed.shapeimageview.CircularImageView;
import com.squareup.picasso.Callback;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ConfirmEConsultBookingActivity extends AppCompatActivity implements AsyncTaskListner {

    private static int RESULT_LOAD_IMG = 1;

    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    @BindView(R.id.img_doctor)
    CircularImageView mDoctorImg;

    private TextView tvFirstDiscount, tvResponseTime, tvDoctorName, tvSpeciality, tvInstantBook, tvDiscount, tvTotal, tvDoctor, tvGotDiscount, tvPromocode, tvWalletBalance;
    private CheckBox cbWallet;
    private RecyclerView rvImages;
    private EditText etProblem;
    private Button btnCheckout, btnPayNow;
    private ExpandableHeightListView lvComments;
    private ConfirmPaymentCommentsAdapter confirmPaymentCommentsAdapter;
    private MentionQueryAdapter mentionQueryAdapter;

    public JsonParserUniversal jParser;

    private List<String> imagePath = new ArrayList<>();
    public List<RefferDiscount> refferList = new ArrayList<>();

    public int finalAmount = 0, finalDiscountAmount = 0;
    boolean isBooked, isFromInPersonVisit = false, isCheckoutClick = false, isPayNowClick = false;
    String DoctorName, Speciality, ResponseTime, ConsultCharge, walletCharge = "", ProfilePic, mConsultTime, mTimeSlotId, mReasonForConsult, mPhoneNumberToReach, filePathFirst = "";

    private SharedPreferences mSharedPreferences;
    private DoctorProfile mDoctorProfileData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirm_econsult_booking);

        ButterKnife.bind(this);
        setupToolbar();

        mSharedPreferences = getSharedPreferences(MyConstants.PREF, MODE_PRIVATE);

        jParser = new JsonParserUniversal();
        tvResponseTime = findViewById(R.id.tvResponseTime);
        tvDoctorName = findViewById(R.id.tvDoctorName);
        tvSpeciality = findViewById(R.id.tvSpeciality);
        tvInstantBook = findViewById(R.id.tvInstantBook);
        tvDiscount = findViewById(R.id.tvDiscount);
        tvTotal = findViewById(R.id.tvTotal);

        etProblem = findViewById(R.id.etProblem);
        rvImages = findViewById(R.id.rvImages);

        btnCheckout = findViewById(R.id.btn_checkout);
        lvComments = findViewById(R.id.lvComments);
        tvWalletBalance = findViewById(R.id.tvWalletBalance);
        cbWallet = findViewById(R.id.cbWallet);
        btnPayNow = findViewById(R.id.btnPayNow);

        lvComments.setVisibility(View.GONE);

        tvWalletBalance.setText("$" + MyConstants.Wallet_Balance);
        if (MyConstants.Wallet_Balance.equalsIgnoreCase("0") || MyConstants.isFree.equalsIgnoreCase("1")) {
            cbWallet.setEnabled(false);
            cbWallet.setVisibility(View.GONE);

        } else {
            cbWallet.setEnabled(true);
            cbWallet.setVisibility(View.VISIBLE);
        }

        //String firstDiscountText = "Enjoy <font color='#39c3f6'>20%</font> off your first consult and Save even more money";
        //tvFirstDiscount.setText(Html.fromHtml(firstDiscountText));

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            if (bundle.containsKey("inPersonVisit")) {
                isFromInPersonVisit = true;
                mDoctorProfileData = (DoctorProfile) bundle.getSerializable("doctor");
                DoctorName = mDoctorProfileData.getFirstName() + " " + mDoctorProfileData.getLastName();
                Speciality = mDoctorProfileData.getSpeciality();
                ResponseTime = mDoctorProfileData.getResponseTime();
                ConsultCharge = "18";
                ProfilePic = mDoctorProfileData.getProfilePic();
                mConsultTime = getIntent().getStringExtra("ConsultTime");
                mTimeSlotId = getIntent().getStringExtra("TIME_SLOT_ID");
                mReasonForConsult = getIntent().getStringExtra("ReasonForConsult");
                mPhoneNumberToReach = getIntent().getStringExtra("PhoneNumberToReach");

                tvDoctorName.setText(DoctorName);
                tvSpeciality.setText(Speciality);
                tvResponseTime.setText(ResponseTime);

                try {
                    PicassoTrustAll.getInstance(ConfirmEConsultBookingActivity.this)
                            .load(ProfilePic)
                            .placeholder(R.drawable.avatar)
                            .error(R.drawable.avatar)
                            .into(mDoctorImg, new Callback() {
                                @Override
                                public void onSuccess() {
                                }

                                @Override
                                public void onError() {
                                }
                            });
                } catch (Exception e) {
                    e.printStackTrace();
                }

                setData();

            }else {
                mDoctorProfileData = MyConstants.doctorProfile;
                isBooked = bundle.getBoolean("isBooked");

                if (isBooked) {
                    DoctorName = bundle.getString("DoctorName");
                    Speciality = bundle.getString("Speciality");
                    ResponseTime = bundle.getString("ResponseTime");
                    ConsultCharge = bundle.getString("ConsultCharge");
                    ProfilePic = bundle.getString("ProfilePic");
                }

                if (isBooked) {
                    tvDoctorName.setText(DoctorName);
                    tvSpeciality.setText(Speciality);
                    tvResponseTime.setText(ResponseTime);

                    try {
                        PicassoTrustAll.getInstance(ConfirmEConsultBookingActivity.this)
                                .load(ProfilePic)
                                .placeholder(R.drawable.avatar)
                                .error(R.drawable.avatar)
                                .into(mDoctorImg, new Callback() {
                                    @Override
                                    public void onSuccess() {
                                    }

                                    @Override
                                    public void onError() {
                                    }
                                });
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                } else {
                    tvDoctorName.setText(MyConstants.doctorProfile.getFirstName() + " " + MyConstants.doctorProfile.getLastName());
                    //tvDoctor.setText(MyConstants.doctorProfile.getFirstName() + " " + MyConstants.doctorProfile.getLastName());
                    tvSpeciality.setText(MyConstants.doctorProfile.getSpeciality());
                    tvResponseTime.setText(MyConstants.doctorProfile.getResponseTime());

                    try {
                        PicassoTrustAll.getInstance(ConfirmEConsultBookingActivity.this)
                                .load(MyConstants.doctorProfile.getProfilePic())
                                .placeholder(R.drawable.avatar)
                                .error(R.drawable.avatar)
                                .into(mDoctorImg, new Callback() {
                                    @Override
                                    public void onSuccess() {
                                    }

                                    @Override
                                    public void onError() {
                                    }
                                });
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    if (MyConstants.isGuest.equalsIgnoreCase("0") && MyConstants.isFree.equalsIgnoreCase("0")) {
                        new CallRequest(this).getReferredDiscount(App.user.getUserID());
                    } else {
                        setData();
                    }
                }
            }
        }else {
            mDoctorProfileData = MyConstants.doctorProfile;
            tvDoctorName.setText(MyConstants.doctorProfile.getFirstName() + " " + MyConstants.doctorProfile.getLastName());
            //tvDoctor.setText(MyConstants.doctorProfile.getFirstName() + " " + MyConstants.doctorProfile.getLastName());
            tvSpeciality.setText(MyConstants.doctorProfile.getSpeciality());
            tvResponseTime.setText(MyConstants.doctorProfile.getResponseTime());

            try {
                PicassoTrustAll.getInstance(ConfirmEConsultBookingActivity.this)
                        .load(MyConstants.doctorProfile.getProfilePic())
                        .placeholder(R.drawable.avatar)
                        .error(R.drawable.avatar)
                        .into(mDoctorImg, new Callback() {
                            @Override
                            public void onSuccess() {
                            }

                            @Override
                            public void onError() {
                            }
                        });
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (MyConstants.isGuest.equalsIgnoreCase("0") && MyConstants.isFree.equalsIgnoreCase("0")) {
                new CallRequest(this).getReferredDiscount(App.user.getUserID());
            } else {
                setData();
            }
        }

        setImagesAdapter();
        createConsultAPI();
    }


    private void setupToolbar() {
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle("Confirm Booking");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }


    private void setImagesAdapter() {
        imagePath.add("by default");
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(ConfirmEConsultBookingActivity.this, LinearLayoutManager.HORIZONTAL, false);
        rvImages.setLayoutManager(linearLayoutManager);

        mentionQueryAdapter = new MentionQueryAdapter(ConfirmEConsultBookingActivity.this, imagePath);
        rvImages.setHasFixedSize(true);
        rvImages.addItemDecoration(new VerticalSpacingDecoration(20));
        rvImages.setItemViewCacheSize(20);
        rvImages.setDrawingCacheEnabled(true);
        rvImages.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        rvImages.setAdapter(mentionQueryAdapter);
    }


    public void loadImageFromGallery() {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(galleryIntent, RESULT_LOAD_IMG);
    }


    private void createConsultAPI() {
        if (!Internet.isAvailable(ConfirmEConsultBookingActivity.this)) {
            Internet.showAlertDialog(ConfirmEConsultBookingActivity.this, "Error!", "No Internet Connection", false);

        } else {
            String problemDescription = etProblem.getText().toString().trim();

            Map<String, String> map = new HashMap<String, String>();
            if (MyConstants.isGuest.equalsIgnoreCase("1")) {
                map.put("url", MyConstants.GUEST_BASE_URL + "guestConsultBooking");
                map.put("GuestId", App.user.getUserID());
            } else {
                map.put("url", MyConstants.BASE_URL + "consultBooking");
                map.put("UserId", App.user.getUserID());
                map.put("SocialType", App.user.getLoginType());
            }
            map.put("ApiToken", MyConstants.API_TOKEN);
            map.put("Version", MyConstants.WS_VERSION);
            map.put("SocialId", mSharedPreferences.getString(MyConstants.SOCIAL_ID, "0"));
            map.put("DoctorId", mDoctorProfileData.getDoctorId());
            map.put("DoctorCharge", mDoctorProfileData.getCosultCharge());
            map.put("Description", problemDescription);
            map.put("ConsultTime", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US).format(new Date()));

            System.out.println("image list size:::" + imagePath.size());
            if (imagePath.size() > 1) {
                for (int i = 1; i < imagePath.size(); i++) {
                    map.put("Image" + i, imagePath.get(i));
                }
            }
            new CallRequest(ConfirmEConsultBookingActivity.this).createBooking(map);
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        try {
            if (requestCode == RESULT_LOAD_IMG && resultCode == RESULT_OK && null != data) {
                Uri selectedImage = data.getData();
                String[] filePathColumn = {MediaStore.Images.Media.DATA};

                Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
                int columnIndex = 0;
                if (cursor != null) {
                    cursor.moveToFirst();
                    columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    filePathFirst = cursor.getString(columnIndex);
                    System.out.println(filePathFirst);
                    imagePath.add(filePathFirst);
                    cursor.close();

                    mentionQueryAdapter.notifyDataSetChanged();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static Bitmap decodeSampledBitmapFromPath(String path, int reqWidth, int reqHeight) {

        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, options);

        options.inSampleSize = calculateInSampleSize(options, reqWidth,
                reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        Bitmap bmp = BitmapFactory.decodeFile(path, options);
        return bmp;
    }

    public static int calculateInSampleSize(BitmapFactory.Options options,int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            if (width > height) {
                inSampleSize = Math.round((float) height / (float) reqHeight);
            } else {
                inSampleSize = Math.round((float) width / (float) reqWidth);
            }
        }
        return inSampleSize;
    }


    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        Utils.removeSimpleSpinProgressDialog();
        try {
            if (result != null && !result.isEmpty()) {
                Log.i("TAG", "TAG Result : " + result);
                //      {"document":{"response":{"status":1,"message":"Success."}}}
                switch (request) {
                    case getReferredDiscount:
                        try {
                            JSONObject mainObj = new JSONObject(result);
                            if (mainObj.getString("error_code").equalsIgnoreCase("0")) {
                                Log.i("TAG", " In ERROR 0 ");
                                JSONArray jsonArray = mainObj.getJSONArray("result");
                                refferList.clear();
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    Log.i("TAG", " In For " + i);
                                    RefferDiscount desc = (RefferDiscount) jParser.parseJson(jsonArray.getJSONObject(i),
                                            new RefferDiscount());
                                    refferList.add(desc);
                                }
                                setData();

                            } else {
                                setData();
                            }

                        } catch (JSONException e) {
                            setData();
                            e.printStackTrace();
                        }
                        break;

                    case createBooking:
                        if (isPayNowClick) {
                            try {
                                JSONObject mainObj = new JSONObject(result);
                                if (mainObj.getString("error_code").equalsIgnoreCase("0")) {
                                    Utils.showToast(mainObj.getString("error_string"), ConfirmEConsultBookingActivity.this);

                                    JSONObject jsonObject = mainObj.getJSONArray("result").getJSONObject(0);

                                    String UniqueId = jsonObject.getString("UniqueId");

                                    MyConstants.uniqueChatId = UniqueId;

                                    Intent intent = new Intent(ConfirmEConsultBookingActivity.this, FireChatActivity.class);
                                    intent.putExtra(MyConstants.IS_PAYMENT_DONE, true);
                                    intent.putExtra(MyConstants.UNIQUE_CHAT_ID, MyConstants.uniqueChatId);
                                    intent.putExtra(MyConstants.Pt_NAME, App.user.getName());

                                    if (App.user.getUser_Type().equalsIgnoreCase(MyConstants.USER_PT) && MyConstants.isBooked) {
                                        intent.putExtra(MyConstants.DR_NAME, MyConstants.DOCTOR_NAME);
                                        intent.putExtra(MyConstants.DR_ID, MyConstants.DOCTOR_ID);
                                        intent.putExtra(MyConstants.RECEIVER_IMAGE_URL, MyConstants.DOCTOR_PROFILEPIC);

                                    } else {
                                        intent.putExtra(MyConstants.DR_NAME, MyConstants.doctorProfile.getFirstName() + " " + MyConstants.doctorProfile.getLastName());
                                        intent.putExtra(MyConstants.DR_ID, MyConstants.doctorProfile.getDoctorId());
                                        intent.putExtra(MyConstants.RECEIVER_IMAGE_URL, MyConstants.doctorProfile.getProfilePic());
                                    }
                                    intent.putExtra(MyConstants.PT_ID, App.user.getUserID());
                                    startActivity(intent);
                                    ActivityCompat.finishAffinity(ConfirmEConsultBookingActivity.this);

                                } else {
                                    Utils.showToast(mainObj.getString("error_string"), ConfirmEConsultBookingActivity.this);
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }else {
                            try {
                                JSONObject mainObj = new JSONObject(result);
                                if (mainObj.getString("error_code").equalsIgnoreCase("0")) {
                                    //Utils.showToast(mainObj.getString("error_string"), MentionQueryActivity.this);

                                    JSONObject resultObj = mainObj.getJSONObject("result");
                                    MyConstants.consultId = resultObj.getString("consult_id");
                                    MyConstants.Wallet_Balance = resultObj.getString("WalletBalance");
                                    MyConstants.isFree = resultObj.getString("IsFree");

                                    MyConstants.isBooked = false;

                                    // reload all data
                                    MyConstants.bookingHistoryList.clear();
                                    MyConstants.messageHistoryList.clear();
                                    MyConstants.userChatList.clear();
                                    MyConstants.doctorChatList.clear();
                                    MyConstants.isBookingHistoryLoad = true;
                                    MyConstants.isMessageHistoryLoad = true;

                                    if (isCheckoutClick) {
                                        Intent intent = new Intent(ConfirmEConsultBookingActivity.this, PaymentActivity.class);

                                        if (isFromInPersonVisit) {
                                            intent.putExtra("inPersonVisit", true);
                                            intent.putExtra("DOCTOR_PROFILE", mDoctorProfileData);
                                            intent.putExtra("ConsultTime", mConsultTime);
                                            intent.putExtra("TIME_SLOT_ID", mTimeSlotId);
                                            intent.putExtra("ReasonForConsult", mReasonForConsult);
                                            intent.putExtra("PhoneNumberToReach", mPhoneNumberToReach);

                                        }else {
                                            intent.putExtra("isOfferUsed", refferList.size() > 0);
                                            if (refferList.size() > 0) {
                                                System.out.println("offer id:::" + refferList.get(0).getId());
                                                intent.putExtra("OfferId", refferList.get(0).getId());
                                                intent.putExtra("OfferType", "1"); // need to change accordingly
                                                intent.putExtra("DiscountAmount", String.valueOf(finalDiscountAmount));
                                            }
                                            intent.putExtra("finalAmount", finalAmount);
                                            intent.putExtra("walletCharge", walletCharge);
                                        }
                                        startActivity(intent);
                                    }

                                } else {
                                    Utils.showToast(mainObj.getString("error_string"), ConfirmEConsultBookingActivity.this);
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        break;
                }
            } else {
                setData();
            }
        } catch (Exception e) {

            e.printStackTrace();
        }
    }


    @Override
    public void onProgressUpdate(String uniqueMessageId, int progres) {

    }

    @Override
    public void onProgressComplete(String uniqueMessageId, String result, Constant.REQUESTS request) {

    }

    @SuppressLint("SetTextI18n")
    private void setData() {
        int discount = 0;
        int counsultCharge = 0;

        int discountAmount = 0;
        try {
            if (isBooked) {
                counsultCharge = Integer.parseInt(ConsultCharge);
            } else {
                if (isFromInPersonVisit)
                    counsultCharge = Integer.parseInt(ConsultCharge);
                else
                    counsultCharge = Integer.parseInt(MyConstants.doctorProfile.getCosultCharge());
            }
            if (refferList.size() > 0) {
                discount = Integer.parseInt(refferList.get(0).getUserDiscount());
                discountAmount = (counsultCharge * discount / 100);
                finalAmount = counsultCharge - discountAmount;
            } else {
                finalAmount = counsultCharge;
            }

            tvInstantBook.setText("$" + counsultCharge);
            tvDiscount.setText("-$" + discountAmount);
            tvTotal.setText("$" + String.valueOf(finalAmount));

        } catch (NumberFormatException e) {
            e.printStackTrace();

            if (isBooked) {
                finalAmount = Integer.parseInt(ConsultCharge);
                tvInstantBook.setText("$" + ConsultCharge);
                tvDiscount.setText("-$0");
                tvTotal.setText("$" + ConsultCharge);

            } else {
                finalAmount = Integer.parseInt(MyConstants.doctorProfile.getCosultCharge());
                tvInstantBook.setText("$" + MyConstants.doctorProfile.getCosultCharge());
                tvDiscount.setText("-$0");
                tvTotal.setText("$" + MyConstants.doctorProfile.getCosultCharge());
            }
            //btnCheckout.setText("Proceed Checkout for $" + finalAmount);
        }

        finalDiscountAmount = discountAmount;
        final int finalCharge = finalAmount;

        if (MyConstants.isFree.equalsIgnoreCase("1")) {
            btnPayNow.setVisibility(View.VISIBLE);
            btnCheckout.setVisibility(View.GONE);
            tvInstantBook.setText("$0");
            tvDiscount.setText("-$0");
            tvTotal.setText("$0");

        } else {
            btnPayNow.setVisibility(View.GONE);
            btnCheckout.setVisibility(View.VISIBLE);
        }

        cbWallet.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    int walletBalance = Integer.parseInt(MyConstants.Wallet_Balance);
                    if (walletBalance < finalAmount) {
                        int remainingAmount = finalAmount - walletBalance;

                        //btnCheckout.setText("Proceed Checkout for $" + remainingAmount);
                        btnPayNow.setVisibility(View.GONE);
                        btnCheckout.setVisibility(View.VISIBLE);

                        finalAmount = remainingAmount;
                        walletCharge = String.valueOf(walletBalance);

                    } else {
                        btnPayNow.setVisibility(View.VISIBLE);
                        btnCheckout.setVisibility(View.GONE);

                        finalAmount = finalCharge;
                        walletCharge = String.valueOf(finalCharge);
                    }

                } else {
                    btnPayNow.setVisibility(View.GONE);
                    btnCheckout.setVisibility(View.VISIBLE);

                    finalAmount = finalCharge;
                    walletCharge = "";
                }
                tvTotal.setText("$" + String.valueOf(finalAmount));
            }
        });

        if (MyConstants.Wallet_Balance.equalsIgnoreCase("0") || MyConstants.isFree.equalsIgnoreCase("1")) {
            cbWallet.setChecked(false);
        } else {
            cbWallet.setChecked(true);
        }

        btnPayNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isPayNowClick = true;
                if (MyConstants.isFree.equalsIgnoreCase("1")) {
                    doFreePayment(finalDiscountAmount);
                } else {
                    doPayment(finalDiscountAmount);
                }
            }
        });

        List<HashMap<String, String>> commentList = new ArrayList<>();

        for (int i = 0; i < 3; i++) {
            HashMap<String, String> comment = new HashMap<>();
            comment.put("name", "Sagar Sojitra");
//            comment.put("date", "Nov 14 2017");
            comment.put("location", "Brisbane, Australia");
            comment.put("comment", "A search for 'lorem ipsum' will uncover many web sites still in their infancy");

            commentList.add(comment);
        }

        confirmPaymentCommentsAdapter = new ConfirmPaymentCommentsAdapter(ConfirmEConsultBookingActivity.this, commentList);
        lvComments.setAdapter(confirmPaymentCommentsAdapter);

        lvComments.setExpanded(true);
    }

    private void doPayment(int finalDiscountAmount) {
        String timestamp = new SimpleDateFormat("yyyyMMddHHmmss", Locale.US).format(new Date());

        Map<String, String> map = new HashMap<String, String>();
        if (MyConstants.isGuest.equalsIgnoreCase("1")) {
            map.put("url", MyConstants.GUEST_BASE_URL + "guestConsultPayment");
            map.put("GuestId", App.user.getUserID());
        } else {
            map.put("url", MyConstants.BASE_URL + "consultPayment");
            map.put("UserId", App.user.getUserID());
        }
        map.put("ApiToken", MyConstants.API_TOKEN);
        map.put("Version", MyConstants.WS_VERSION);
        map.put("ConsultId", MyConstants.consultId);
        map.put("Payment", String.valueOf(finalAmount));
        map.put("DiscountAmount", String.valueOf(finalDiscountAmount));
        map.put("PaymentType", "4");
        map.put("WalletAmount", walletCharge);

        if (refferList.size() > 0) {
            map.put("OfferId", String.valueOf(refferList.get(0).getId()));
            map.put("OfferType", "1");

        } else {
            map.put("OfferId", "");
            map.put("OfferType", "");
        }
        map.put("TxnId", "wallet_" + timestamp);

        new CallRequest(ConfirmEConsultBookingActivity.this).createBookingPayment(map);
    }

    private void doFreePayment(int finalDiscountAmount) {
        String timestamp = new SimpleDateFormat("yyyyMMddHHmmss", Locale.US).format(new Date());

        Map<String, String> map = new HashMap<String, String>();
        if (MyConstants.isGuest.equalsIgnoreCase("1")) {
            map.put("url", MyConstants.GUEST_BASE_URL + "guestConsultPayment");
            map.put("GuestId", App.user.getUserID());

        } else {
            map.put("url", MyConstants.BASE_URL + "consultPayment");
            map.put("UserId", App.user.getUserID());
        }
        map.put("ApiToken", MyConstants.API_TOKEN);
        map.put("Version", MyConstants.WS_VERSION);
        map.put("ConsultId", MyConstants.consultId);
        map.put("Payment", String.valueOf(finalAmount));
        map.put("DiscountAmount", "0");
        map.put("PaymentType", "5");
        map.put("WalletAmount", "");
        map.put("OfferId", "");
        map.put("OfferType", "");
        map.put("TxnId", "free_" + timestamp);

        new CallRequest(ConfirmEConsultBookingActivity.this).createBookingPayment(map);
    }


    @OnClick(R.id.btn_checkout)
    void onCheckoutClick() {
        isCheckoutClick = true;
        createConsultAPI();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent intent = new Intent(ConfirmEConsultBookingActivity.this, PTDashboardActivity.class);
                startActivity(intent);
                ActivityCompat.finishAffinity(ConfirmEConsultBookingActivity.this);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

}
