package com.connex.md.patient.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.connex.md.R;
import com.connex.md.firebase_chat.activity.FireChatActivity;
import com.connex.md.interfaces.AsyncTaskListner;
import com.connex.md.model.DoctorProfile;
import com.connex.md.model.RefferDiscount;
import com.connex.md.others.App;
import com.connex.md.others.PicassoTrustAll;
import com.connex.md.patient.adapter.ConfirmPaymentCommentsAdapter;
import com.connex.md.ws.CallRequest;
import com.connex.md.ws.Constant;
import com.connex.md.ws.JsonParserUniversal;
import com.connex.md.ws.MyConstants;
import com.connex.md.ws.Utils;
import com.github.paolorotolo.expandableheightlistview.ExpandableHeightListView;
import com.github.siyamed.shapeimageview.CircularImageView;
import com.squareup.picasso.Callback;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ConfirmPaymentActivity extends AppCompatActivity implements AsyncTaskListner {

    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    public JsonParserUniversal jParser;
    public int finalAmount = 0;
    public List<RefferDiscount> refferList = new ArrayList<>();
    TextView tvFirstDiscount, tvResponseTime, tvDoctorName, tvSpeciality, tvInstantBook, tvDiscount, tvTotal, tvDoctor, tvGotDiscount, tvPromocode, tvWalletBalance;
    CheckBox cbWallet;
    Button btnCheckout, btnPayNow;
    ExpandableHeightListView lvComments;
    ConfirmPaymentCommentsAdapter confirmPaymentCommentsAdapter;
    boolean isBooked, isFromInPersonVisit = false;
    CircularImageView ivDoctor;
    String DoctorName, Speciality, ResponseTime, ConsultCharge, walletCharge = "", ProfilePic, mConsultTime, mTimeSlotId, mReasonForConsult, mPhoneNumberToReach;

    private DoctorProfile mDoctorProfileData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirm_payment);

        ButterKnife.bind(this);

        setupToolbar();

        jParser = new JsonParserUniversal();
        //tvFirstDiscount = findViewById(R.id.tvFirstDiscount);
        tvResponseTime = findViewById(R.id.tvResponseTime);
        tvDoctorName = findViewById(R.id.tvDoctorName);
        tvSpeciality = findViewById(R.id.tvSpeciality);
        tvInstantBook = findViewById(R.id.tvInstantBook);
        tvDiscount = findViewById(R.id.tvDiscount);
        tvTotal = findViewById(R.id.tvTotal);
        //tvDoctor = findViewById(R.id.tvDoctor);
        /*tvGotDiscount = findViewById(R.id.tvGotDiscount);
        tvPromocode = findViewById(R.id.tvPromocode);*/
        btnCheckout = findViewById(R.id.btnCheckout);
        lvComments = findViewById(R.id.lvComments);
        tvWalletBalance = findViewById(R.id.tvWalletBalance);
        cbWallet = findViewById(R.id.cbWallet);
        btnPayNow = findViewById(R.id.btnPayNow);
        ivDoctor = findViewById(R.id.ivDoctor);

        lvComments.setVisibility(View.GONE);

        tvWalletBalance.setText("$" + MyConstants.Wallet_Balance);
        if (MyConstants.Wallet_Balance.equalsIgnoreCase("0") || MyConstants.isFree.equalsIgnoreCase("1")) {
            cbWallet.setEnabled(false);
            cbWallet.setVisibility(View.GONE);
        } else {
            cbWallet.setEnabled(true);
            cbWallet.setVisibility(View.VISIBLE);
        }

        //String firstDiscountText = "Enjoy <font color='#39c3f6'>20%</font> off your first consult and Save even more money";
        //tvFirstDiscount.setText(Html.fromHtml(firstDiscountText));

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            if (bundle.containsKey("inPersonVisit")) {
                isFromInPersonVisit = true;
                mDoctorProfileData = (DoctorProfile)getIntent().getExtras().get("DOCTOR_PROFILE");
                DoctorName = mDoctorProfileData.getFirstName() + " " + mDoctorProfileData.getLastName();
                Speciality = mDoctorProfileData.getSpeciality();
                ResponseTime = mDoctorProfileData.getResponseTime();
                ConsultCharge = "18";
                ProfilePic = mDoctorProfileData.getProfilePic();
                mConsultTime = getIntent().getStringExtra("ConsultTime");
                mTimeSlotId = getIntent().getStringExtra("TIME_SLOT_ID");
                mReasonForConsult = getIntent().getStringExtra("ReasonForConsult");
                mPhoneNumberToReach = getIntent().getStringExtra("PhoneNumberToReach");

                tvDoctorName.setText(DoctorName);
                //tvDoctor.setText(MyConstants.doctorProfile.getFirstName() + " " + MyConstants.doctorProfile.getLastName());
                tvSpeciality.setText(Speciality);
                tvResponseTime.setText(ResponseTime);

                try {
                    PicassoTrustAll.getInstance(ConfirmPaymentActivity.this)
                            .load(ProfilePic)
                            .placeholder(R.drawable.avatar)
                            .error(R.drawable.avatar)
                            .into(ivDoctor, new Callback() {
                                @Override
                                public void onSuccess() {
                                }

                                @Override
                                public void onError() {
                                }
                            });
                } catch (Exception e) {
                    e.printStackTrace();
                }

                setData();

            }else {
                isBooked = bundle.getBoolean("isBooked");

                if (isBooked) {
                    DoctorName = bundle.getString("DoctorName");
                    Speciality = bundle.getString("Speciality");
                    ResponseTime = bundle.getString("ResponseTime");
                    ConsultCharge = bundle.getString("ConsultCharge");
                    ProfilePic = bundle.getString("ProfilePic");
                }

                if (isBooked) {
                    tvDoctorName.setText(DoctorName);
                    //tvDoctor.setText(DoctorName);
                    tvSpeciality.setText(Speciality);
                    tvResponseTime.setText(ResponseTime);

                    try {
                        PicassoTrustAll.getInstance(ConfirmPaymentActivity.this)
                                .load(ProfilePic)
                                .placeholder(R.drawable.avatar)
                                .error(R.drawable.avatar)
                                .into(ivDoctor, new Callback() {
                                    @Override
                                    public void onSuccess() {
                                    }

                                    @Override
                                    public void onError() {
                                    }
                                });
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                } else {
                    tvDoctorName.setText(MyConstants.doctorProfile.getFirstName() + " " + MyConstants.doctorProfile.getLastName());
                    //tvDoctor.setText(MyConstants.doctorProfile.getFirstName() + " " + MyConstants.doctorProfile.getLastName());
                    tvSpeciality.setText(MyConstants.doctorProfile.getSpeciality());
                    tvResponseTime.setText(MyConstants.doctorProfile.getResponseTime());

                    try {
                        PicassoTrustAll.getInstance(ConfirmPaymentActivity.this)
                                .load(MyConstants.doctorProfile.getProfilePic())
                                .placeholder(R.drawable.avatar)
                                .error(R.drawable.avatar)
                                .into(ivDoctor, new Callback() {
                                    @Override
                                    public void onSuccess() {
                                    }

                                    @Override
                                    public void onError() {
                                    }
                                });
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    if (MyConstants.isGuest.equalsIgnoreCase("0") && MyConstants.isFree.equalsIgnoreCase("0")) {
                        new CallRequest(this).getReferredDiscount(App.user.getUserID());
                    } else {
                        setData();
                    }
                }
            }
        }else {
            tvDoctorName.setText(MyConstants.doctorProfile.getFirstName() + " " + MyConstants.doctorProfile.getLastName());
            //tvDoctor.setText(MyConstants.doctorProfile.getFirstName() + " " + MyConstants.doctorProfile.getLastName());
            tvSpeciality.setText(MyConstants.doctorProfile.getSpeciality());
            tvResponseTime.setText(MyConstants.doctorProfile.getResponseTime());

            try {
                PicassoTrustAll.getInstance(ConfirmPaymentActivity.this)
                        .load(MyConstants.doctorProfile.getProfilePic())
                        .placeholder(R.drawable.avatar)
                        .error(R.drawable.avatar)
                        .into(ivDoctor, new Callback() {
                            @Override
                            public void onSuccess() {
                            }

                            @Override
                            public void onError() {
                            }
                        });
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (MyConstants.isGuest.equalsIgnoreCase("0") && MyConstants.isFree.equalsIgnoreCase("0")) {
                new CallRequest(this).getReferredDiscount(App.user.getUserID());
            } else {
                setData();
            }
        }
    }


    private void setupToolbar() {
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle("Confirm Payment");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        Utils.removeSimpleSpinProgressDialog();
        try {
            if (result != null && !result.isEmpty()) {
                Log.i("TAG", "TAG Result : " + result);
                //      {"document":{"response":{"status":1,"message":"Success."}}}
                switch (request) {
                    case getReferredDiscount:
                        try {
                            JSONObject mainObj = new JSONObject(result);
                            if (mainObj.getString("error_code").equalsIgnoreCase("0")) {
                                Log.i("TAG", " In ERROR 0 ");
                                JSONArray jsonArray = mainObj.getJSONArray("result");
                                refferList.clear();

                                for (int i = 0; i < jsonArray.length(); i++) {
                                    Log.i("TAG", " In For " + i);
                                    RefferDiscount desc = (RefferDiscount) jParser.parseJson(jsonArray.getJSONObject(i), new RefferDiscount());
                                    refferList.add(desc);
                                }
                                setData();

                            } else {
                                setData();
                                //Utils.showToast(mainObj.getString("error_string"), ConfirmPaymentActivity.this);

                            }
                        } catch (JSONException e) {
                            setData();
                            e.printStackTrace();

                        }
                        break;

                    case createBooking:
                        try {
                            JSONObject mainObj = new JSONObject(result);
                            if (mainObj.getString("error_code").equalsIgnoreCase("0")) {
                                Utils.showToast(mainObj.getString("error_string"), ConfirmPaymentActivity.this);

                                JSONObject jsonObject = mainObj.getJSONArray("result").getJSONObject(0);

                                String UniqueId = jsonObject.getString("UniqueId");

                                MyConstants.uniqueChatId = UniqueId;

                                Intent intent = new Intent(ConfirmPaymentActivity.this, FireChatActivity.class);
                                intent.putExtra(MyConstants.IS_PAYMENT_DONE, true);
                                intent.putExtra(MyConstants.UNIQUE_CHAT_ID, MyConstants.uniqueChatId);
                                intent.putExtra(MyConstants.Pt_NAME, App.user.getName());
                                if (App.user.getUser_Type().equalsIgnoreCase(MyConstants.USER_PT) && MyConstants.isBooked) {
                                    intent.putExtra(MyConstants.DR_NAME, MyConstants.DOCTOR_NAME);
                                    intent.putExtra(MyConstants.DR_ID, MyConstants.DOCTOR_ID);
                                    intent.putExtra(MyConstants.RECEIVER_IMAGE_URL, MyConstants.DOCTOR_PROFILEPIC);
                                } else {
                                    intent.putExtra(MyConstants.DR_NAME, MyConstants.doctorProfile.getFirstName() + " " + MyConstants.doctorProfile.getLastName());
                                    intent.putExtra(MyConstants.DR_ID, MyConstants.doctorProfile.getDoctorId());
                                    intent.putExtra(MyConstants.RECEIVER_IMAGE_URL, MyConstants.doctorProfile.getProfilePic());
                                }
                                intent.putExtra(MyConstants.PT_ID, App.user.getUserID());
                                startActivity(intent);
                                ActivityCompat.finishAffinity(ConfirmPaymentActivity.this);

                            } else {
                                Utils.showToast(mainObj.getString("error_string"), ConfirmPaymentActivity.this);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;
                }
            } else {
                setData();
            }
        } catch (Exception e) {

            e.printStackTrace();
        }


    }

    @Override
    public void onProgressUpdate(String uniqueMessageId, int progres) {

    }

    @Override
    public void onProgressComplete(String uniqueMessageId, String result, Constant.REQUESTS request) {

    }

    private void setData() {

        int discount = 0;
        int counsultCharge = 0;

        int discountAmount = 0;
        try {
            if (isBooked) {
                counsultCharge = Integer.parseInt(ConsultCharge);
            } else {
                if (isFromInPersonVisit)
                    counsultCharge = Integer.parseInt(ConsultCharge);
                else
                    counsultCharge = Integer.parseInt(MyConstants.doctorProfile.getCosultCharge());
            }
            if (refferList.size() > 0) {
                discount = Integer.parseInt(refferList.get(0).getUserDiscount());
                discountAmount = (counsultCharge * discount / 100);
                finalAmount = counsultCharge - discountAmount;
            } else {
                finalAmount = counsultCharge;
            }

            tvInstantBook.setText("$" + counsultCharge);
            tvDiscount.setText("-$" + discountAmount);
            tvTotal.setText("$" + String.valueOf(finalAmount));
            //btnCheckout.setText("Proceed Checkout for $" + finalAmount);
        } catch (NumberFormatException e) {
            e.printStackTrace();

            if (isBooked) {
                finalAmount = Integer.parseInt(ConsultCharge);
                tvInstantBook.setText("$" + ConsultCharge);
                tvDiscount.setText("-$0");
                tvTotal.setText("$" + ConsultCharge);
            } else {
                finalAmount = Integer.parseInt(MyConstants.doctorProfile.getCosultCharge());
                tvInstantBook.setText("$" + MyConstants.doctorProfile.getCosultCharge());
                tvDiscount.setText("-$0");
                tvTotal.setText("$" + MyConstants.doctorProfile.getCosultCharge());
            }
            //btnCheckout.setText("Proceed Checkout for $" + finalAmount);
        }

        final int finalDiscountAmount = discountAmount;
        final int finalCharge = finalAmount;

        if (MyConstants.isFree.equalsIgnoreCase("1")) {
            btnPayNow.setVisibility(View.VISIBLE);
            btnCheckout.setVisibility(View.GONE);
            tvInstantBook.setText("$0");
            tvDiscount.setText("-$0");
            tvTotal.setText("$0");
        } else {
            btnPayNow.setVisibility(View.GONE);
            btnCheckout.setVisibility(View.VISIBLE);
        }

        cbWallet.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    int walletBalance = Integer.parseInt(MyConstants.Wallet_Balance);
                    if (walletBalance < finalAmount) {
                        int remainingAmount = finalAmount - walletBalance;

                        //btnCheckout.setText("Proceed Checkout for $" + remainingAmount);
                        btnPayNow.setVisibility(View.GONE);
                        btnCheckout.setVisibility(View.VISIBLE);

                        finalAmount = remainingAmount;
                        walletCharge = String.valueOf(walletBalance);
                    } else {
                        btnPayNow.setVisibility(View.VISIBLE);
                        btnCheckout.setVisibility(View.GONE);

                        finalAmount = finalCharge;
                        walletCharge = String.valueOf(finalCharge);
                    }

                } else {
                    //btnCheckout.setText("Proceed Checkout for $" + finalCharge);
                    btnPayNow.setVisibility(View.GONE);
                    btnCheckout.setVisibility(View.VISIBLE);

                    finalAmount = finalCharge;
                    walletCharge = "";
                }

                tvTotal.setText("$" + String.valueOf(finalAmount));
            }
        });

        if (MyConstants.Wallet_Balance.equalsIgnoreCase("0") || MyConstants.isFree.equalsIgnoreCase("1")) {
            cbWallet.setChecked(false);
        } else {
            cbWallet.setChecked(true);
        }

        btnPayNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (MyConstants.isFree.equalsIgnoreCase("1")) {
                    doFreePayment(finalDiscountAmount);
                } else {
                    doPayment(finalDiscountAmount);
                }
            }
        });

        btnCheckout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ConfirmPaymentActivity.this, PaymentActivity.class);

                if (isFromInPersonVisit) {
                    intent.putExtra("inPersonVisit", true);
                    intent.putExtra("DOCTOR_PROFILE", mDoctorProfileData);
                    intent.putExtra("ConsultTime", mConsultTime);
                    intent.putExtra("TIME_SLOT_ID", mTimeSlotId);
                    intent.putExtra("ReasonForConsult", mReasonForConsult);
                    intent.putExtra("PhoneNumberToReach", mPhoneNumberToReach);

                }else {
                    intent.putExtra("isOfferUsed", refferList.size() > 0);
                    if (refferList.size() > 0) {
                        System.out.println("offer id:::" + refferList.get(0).getId());
                        intent.putExtra("OfferId", refferList.get(0).getId());
                        intent.putExtra("OfferType", "1"); // need to change accordingly
                        intent.putExtra("DiscountAmount", String.valueOf(finalDiscountAmount));
                    }
                    intent.putExtra("finalAmount", finalAmount);
                    intent.putExtra("walletCharge", walletCharge);
                }
                startActivity(intent);
            }
        });

        List<HashMap<String, String>> commentList = new ArrayList<>();


        for (int i = 0; i < 3; i++) {
            HashMap<String, String> comment = new HashMap<>();
            comment.put("name", "Sagar Sojitra");
//            comment.put("date", "Nov 14 2017");
            comment.put("location", "Brisbane, Australia");
            comment.put("comment", "A search for 'lorem ipsum' will uncover many web sites still in their infancy");

            commentList.add(comment);
        }


        confirmPaymentCommentsAdapter = new ConfirmPaymentCommentsAdapter(ConfirmPaymentActivity.this, commentList);
        lvComments.setAdapter(confirmPaymentCommentsAdapter);

        lvComments.setExpanded(true);

    }

    private void doPayment(int finalDiscountAmount) {

        String timestamp = new SimpleDateFormat("yyyyMMddHHmmss", Locale.US).format(new Date());

        Map<String, String> map = new HashMap<String, String>();
        if (MyConstants.isGuest.equalsIgnoreCase("1")) {
            map.put("url", MyConstants.GUEST_BASE_URL + "guestConsultPayment");
            map.put("GuestId", App.user.getUserID());
        } else {
            map.put("url", MyConstants.BASE_URL + "consultPayment");
            map.put("UserId", App.user.getUserID());
        }
        map.put("ApiToken", MyConstants.API_TOKEN);
        map.put("Version", MyConstants.WS_VERSION);
        map.put("ConsultId", MyConstants.consultId);
        map.put("Payment", String.valueOf(finalAmount));
        map.put("DiscountAmount", String.valueOf(finalDiscountAmount));
        map.put("PaymentType", "4");
        map.put("WalletAmount", walletCharge);
        if (refferList.size() > 0) {
            map.put("OfferId", String.valueOf(refferList.get(0).getId()));
            map.put("OfferType", "1");
        } else {
            map.put("OfferId", "");
            map.put("OfferType", "");
        }
        map.put("TxnId", "wallet_" + timestamp);

        new CallRequest(ConfirmPaymentActivity.this).createBookingPayment(map);
    }

    private void doFreePayment(int finalDiscountAmount) {

        String timestamp = new SimpleDateFormat("yyyyMMddHHmmss", Locale.US).format(new Date());

        Map<String, String> map = new HashMap<String, String>();
        if (MyConstants.isGuest.equalsIgnoreCase("1")) {
            map.put("url", MyConstants.GUEST_BASE_URL + "guestConsultPayment");
            map.put("GuestId", App.user.getUserID());
        } else {
            map.put("url", MyConstants.BASE_URL + "consultPayment");
            map.put("UserId", App.user.getUserID());
        }
        map.put("ApiToken", MyConstants.API_TOKEN);
        map.put("Version", MyConstants.WS_VERSION);
        map.put("ConsultId", MyConstants.consultId);
        map.put("Payment", String.valueOf(finalAmount));
        map.put("DiscountAmount", "0");
        map.put("PaymentType", "5");
        map.put("WalletAmount", "");
        map.put("OfferId", "");
        map.put("OfferType", "");
        map.put("TxnId", "free_" + timestamp);

        new CallRequest(ConfirmPaymentActivity.this).createBookingPayment(map);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent intent = new Intent(ConfirmPaymentActivity.this, PTDashboardActivity.class);
                startActivity(intent);
                ActivityCompat.finishAffinity(ConfirmPaymentActivity.this);
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
