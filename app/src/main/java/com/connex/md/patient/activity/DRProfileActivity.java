package com.connex.md.patient.activity;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.connex.md.custom_views.RobottoTextViewBold;
import com.crashlytics.android.Crashlytics;
import com.connex.md.R;
import com.connex.md.custom_views.CircleImageView;
import com.connex.md.firebase_chat.activity.DoctorFireChatActivity;
import com.connex.md.interfaces.AsyncTaskListner;
import com.connex.md.model.DoctorProfile;
import com.connex.md.model.SearchDoctor;
import com.connex.md.others.App;
import com.connex.md.others.Internet;
import com.connex.md.others.PicassoTrustAll;
import com.connex.md.patient.adapter.DRProfileCommentsAdapter;
import com.connex.md.patient.adapter.DRProfileScopeOfPracticeAdapter;
import com.connex.md.ws.CallRequest;
import com.connex.md.ws.Constant;
import com.connex.md.ws.MyConstants;
import com.connex.md.ws.Utils;
import com.github.paolorotolo.expandableheightlistview.ExpandableHeightListView;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Callback;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import co.lujun.androidtagview.TagContainerLayout;
import io.fabric.sdk.android.Fabric;

public class DRProfileActivity extends AppCompatActivity implements AsyncTaskListner {

    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    @BindView(R.id.collapsing_toolbar)
    CollapsingToolbarLayout mToolbarLayout;

    @BindView(R.id.btn_bookin_person)
    Button mBookinPersonBtn;

    @BindView(R.id.btn_book_econsult)
    Button mBookEConsultBtn;

    @BindView(R.id.btn_request_information)
    Button mRequestInformationBtn;

    @BindView(R.id.ll_econsultation_fee)
    LinearLayout mEconsultationFeeLayout;

    @BindView(R.id.ll_clinic_visit_fee)
    LinearLayout mClinicVisitFeeLayout;

    @BindView(R.id.txt_location)
    RobottoTextViewBold mLocationTxt;

    @BindView(R.id.txt_clinic_visit_fee)
    RobottoTextViewBold mClinicVisitFeeTxt;


    public Dialog dialog;
    public DatabaseReference fireDB;
    TextView tvAboutDoctor, tvEducation, tvMembership, tvHospitalAffiliation, tvPublications;
    LinearLayout llProfession;
    //GridLayout llScopeOfPractice;
    ExpandableHeightListView lvComments;
    DRProfileCommentsAdapter drProfileCommentsAdapter;
    DRProfileScopeOfPracticeAdapter adapter;
    LinearLayout ll_about_doctor;
    ImageView ivArrow;
    SearchDoctor searchDoctor;
    DoctorProfile doctorProfile;
    Button btnBookNow, btnChatNow;
    CircleImageView ivDRImage;
    TextView tvDRName, tvRating, tvTotalRating, tvFee, tvWaitingTime;
    RatingBar ratingBar;
    ToggleButton toggleFavourite;
    TextView tvAboutDoctorLabel, tvScopeOfPracticeLabel, tvEducationLabel, tvProfessionLabel, tvMembershipLabel, tvHospitalLabel, tvPublicationLabel, tvComments;
    Button btnLoginNow, btnBookAsGuest;
    LinearLayout llBookNow, llChatNow, llFavourite;
    View line, line1;
    SharedPreferences sharedpreferences;
    TagContainerLayout tags;
    String doctorId = "";
    String isNutrition = "0";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dr_profile);

        Fabric.with(this, new Crashlytics());
        ButterKnife.bind(this);

        setupToolbar();

        sharedpreferences = getSharedPreferences(MyConstants.PREF, Context.MODE_PRIVATE);

        tvAboutDoctorLabel = findViewById(R.id.tvAboutDoctorLabel);
        tvScopeOfPracticeLabel = findViewById(R.id.tvScopeOfPracticeLabel);
        tvEducationLabel = findViewById(R.id.tvEducationLabel);
        tvProfessionLabel = findViewById(R.id.tvProfessionLabel);
        tvMembershipLabel = findViewById(R.id.tvMembershipLabel);
        tvHospitalLabel = findViewById(R.id.tvHospitalLabel);
        tvPublicationLabel = findViewById(R.id.tvPublicationLabel);
        tvComments = findViewById(R.id.tvComments);
        btnBookNow = findViewById(R.id.btnBookNow);
        ivDRImage = findViewById(R.id.ivDrImage);
        ratingBar = findViewById(R.id.ratingBar);
        toggleFavourite = findViewById(R.id.toggle);
        tvDRName = findViewById(R.id.tvDrName);
        tvRating = findViewById(R.id.tvRating);
        tvTotalRating = findViewById(R.id.tvTotalRatings);
        tvFee = findViewById(R.id.tvFee);
        tvWaitingTime = findViewById(R.id.tvWaitingTime);
        tvAboutDoctor = findViewById(R.id.tvAboutDoctor);
        tvEducation = findViewById(R.id.tvEducation);
        llProfession = findViewById(R.id.ll_profession);
        ll_about_doctor = findViewById(R.id.ll_about_doctor);
        //llScopeOfPractice = findViewById(R.id.ll_scope_of_practice);
        tvMembership = findViewById(R.id.tvMembership);
        tvHospitalAffiliation = findViewById(R.id.tvHospitalAffiliation);
        tvPublications = findViewById(R.id.tvPublication);
        lvComments = findViewById(R.id.lvComments);
        ivArrow = findViewById(R.id.ivArrow);
        llBookNow = findViewById(R.id.llBookNow);
        llFavourite = findViewById(R.id.llFavourite);
        line = findViewById(R.id.line);
        line1 = findViewById(R.id.line1);
        tags = findViewById(R.id.tags);
        llChatNow = findViewById(R.id.llChatNow);
        btnChatNow = findViewById(R.id.btnChatNow);


        Bundle b = getIntent().getExtras();
        if (b != null) {
            if (b.containsKey("search_doctor")) {
                searchDoctor = (SearchDoctor) b.getSerializable("search_doctor");
                if (searchDoctor != null) {
                    doctorId = searchDoctor.getDoctorId();
                    isNutrition = searchDoctor.getIsNutrition();
                }
            }
            if (b.containsKey("id")) {
                doctorId = b.getString("id");
                if (b.containsKey("speciality")) {
                    String speciality = b.getString("speciality");
                    System.out.println("speciality::" + speciality);
                    if (speciality != null && speciality.toLowerCase().contains("Nutrition".toLowerCase())) {
                        isNutrition = "1";
                    } else {
                        isNutrition = "0";
                    }
                }
            }

            if (b.containsKey("authToken")) {

                String base64 = getIntent().getData().getQueryParameter("authToken");

                byte[] data = Base64.decode(base64, Base64.DEFAULT);
                try {
                    doctorId = new String(data, "UTF-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }

                System.out.println("id:::" + doctorId);
            }
        }

        if (sharedpreferences.getBoolean(MyConstants.IS_LOGGED_IN, false)) {

            App.user.setUserID(sharedpreferences.getString(MyConstants.USER_ID, ""));
            App.user.setUser_Type(sharedpreferences.getString(MyConstants.USER_TYPE, ""));
            App.user.setName(sharedpreferences.getString(MyConstants.PT_NAME, ""));

            if (App.user.getUser_Type().equalsIgnoreCase(MyConstants.USER_PT)) {
                //llBookNow.setVisibility(View.VISIBLE);
                llChatNow.setVisibility(View.GONE);
                //line.setVisibility(View.VISIBLE);
                llFavourite.setVisibility(View.GONE);

                if (MyConstants.isSuggestDoctor) {
                    llBookNow.setVisibility(View.GONE);
                    line.setVisibility(View.GONE);
                } else {
                    llBookNow.setVisibility(View.VISIBLE);
                    line.setVisibility(View.VISIBLE);
                }

            } else {
                llBookNow.setVisibility(View.GONE);
                llFavourite.setVisibility(View.GONE);
                // TODO llChatNow.setVisibility(View.VISIBLE);
                llChatNow.setVisibility(View.VISIBLE);
                // TODO line.setVisibility(View.VISIBLE);
                line.setVisibility(View.VISIBLE);
            }
        }

        btnBookNow.setClickable(true);

        btnBookNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnBookNow.setClickable(false);

                enableClick();

                MyConstants.doctorProfile = doctorProfile;
                MyConstants.doctorLastName = doctorProfile.getLastName();

                if (TextUtils.isEmpty(App.user.getUserID())) {
                    openGuestDialog();

                } else {
                    Bundle b = new Bundle();
                    b.putSerializable("doctor", doctorProfile);

                    if (doctorProfile.getDoctorId().equalsIgnoreCase(MyConstants.doctorIdForQuestionnaire)) {
                        if (!App.isActivityRunning(DRProfileActivity.this, DRQuestionnaire1Activity.class)) {
                            Intent intent = new Intent(DRProfileActivity.this, DRQuestionnaire1Activity.class);
                            startActivity(intent);
                        }
                    } else {
                        if (!App.isActivityRunning(DRProfileActivity.this, ConfirmEConsultBookingActivity.class)) {
                            Intent intent = new Intent(DRProfileActivity.this, ConfirmEConsultBookingActivity.class);
                            intent.putExtras(b);
                            startActivity(intent);
                        }
                    }
                }
            }
        });

        btnChatNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getDoctorChatDetails();
            }
        });

        if (TextUtils.isEmpty(App.user.getUserID())) {
            toggleFavourite.setEnabled(false);
        } else if (MyConstants.isGuest.equalsIgnoreCase("0") && App.user.getUser_Type().equalsIgnoreCase(MyConstants.USER_PT)) {
            toggleFavourite.setEnabled(true);
        } else {
            toggleFavourite.setEnabled(false);
        }


        toggleFavourite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveRemoveDoctorAPI();
            }
        });

        getDRProfileData();
    }


    private void setupToolbar() {
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle("Profile");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }


    private void enableClick() {
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                btnBookNow.setClickable(true);
                handler.removeCallbacks(this);
            }
        }, 4000);
    }

    private void getDoctorChatDetails() {
        if (!Internet.isAvailable(this)) {
            Internet.showAlertDialog(this, "Error!", "No Internet Connection", false);

            return;
        }

        Map<String, String> map = new HashMap<String, String>();
        map.put("url", MyConstants.DOCTOR_BASE_URL + "doctorChat");
        map.put("ApiToken", MyConstants.API_TOKEN);
        map.put("Version", MyConstants.WS_VERSION);
        map.put("DoctorId", App.user.getUserID());
        map.put("ReceiverId", doctorId);

        new CallRequest(DRProfileActivity.this).getDoctorChatDetails(map);

    }

    private void saveRemoveDoctorAPI() {
        if (!Internet.isAvailable(this)) {
            Internet.showAlertDialog(this, "Error!", "No Internet Connection", false);

            return;
        }

        Map<String, String> map = new HashMap<String, String>();
        map.put("url", MyConstants.BASE_URL + "saveFavouriteDoctor");
        map.put("ApiToken", MyConstants.API_TOKEN);
        map.put("Version", MyConstants.WS_VERSION);
        map.put("DoctorId", doctorId);
        map.put("UserId", App.user.getUserID());
        map.put("show", "");

        new CallRequest(this).saveRemoveDoctor(map);
    }

    private void openGuestDialog() {
        createDialog();
        initDialogComponents();

        btnLoginNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                Intent intent = new Intent(DRProfileActivity.this, PTLoginActivity.class);
                intent.putExtra("is_logged_in", 1);
                startActivity(intent);
            }
        });

        btnBookAsGuest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();

                MyConstants.CHAT_AS_GUEST = false;

                Intent intent = new Intent(DRProfileActivity.this, PhoneAuthActivity.class);
                startActivity(intent);
            }
        });
    }

    private void createDialog() {
        dialog = new Dialog(DRProfileActivity.this, R.style.CustomDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        // Include dialog.xml file
        dialog.setContentView(R.layout.dialog_guest_popup);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
        dialog.show();
    }

    private void initDialogComponents() {
        btnLoginNow = dialog.findViewById(R.id.btnLoginNow);
        btnBookAsGuest = dialog.findViewById(R.id.btnBookAsGuest);
    }

    private void getDRProfileData() {
        if (!Internet.isAvailable(this)) {
            Internet.showAlertDialog(this, "Error!", "No Internet Connection", false);

            return;
        }

        Map<String, String> map = new HashMap<String, String>();
        map.put("url", MyConstants.BASE_URL + "doctorProfile");
        map.put("ApiToken", MyConstants.API_TOKEN);
        map.put("Version", MyConstants.WS_VERSION);
        map.put("DoctorId", doctorId);
        map.put("UserId", App.user.getUserID());
        if (sharedpreferences.getBoolean(MyConstants.IS_LOGGED_IN, false)) {
            if (App.user.getUser_Type().equalsIgnoreCase(MyConstants.USER_PT)) {
                map.put("IsGuest", MyConstants.isGuest);
            }
        }

        new CallRequest(this).getDoctorProfile(map);
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        Utils.removeSimpleSpinProgressDialog();
        try {
            if (result != null && !result.isEmpty()) {
                Log.i("TAG", "TAG Result : " + result);
                //      {"document":{"response":{"status":1,"message":"Success."}}}
                switch (request) {
                    case getDoctorProfile:
                        Utils.removeSimpleSpinProgressDialog();

                        List<HashMap<String, String>> professionalRotationList, scopeOfPracticeList, membershipList, educationList, hospitalAffiliationList, publicationsList, feedbackList;
                        professionalRotationList = new ArrayList<>();
                        scopeOfPracticeList = new ArrayList<>();
                        membershipList = new ArrayList<>();
                        educationList = new ArrayList<>();
                        hospitalAffiliationList = new ArrayList<>();
                        publicationsList = new ArrayList<>();
                        feedbackList = new ArrayList<>();

                        try {
                            JSONObject mainObj = new JSONObject(result);
                            if (mainObj.getString("error_code").equalsIgnoreCase("0")) {
                                if (mainObj.getJSONArray("result") != null && mainObj.getJSONArray("result").length() > 0) {
                                    JSONArray jsonArray = mainObj.getJSONArray("result");
                                    JSONObject object = jsonArray.getJSONObject(0);

                                    String speciality = "";

                                    JSONObject doctor = object.getJSONObject("doctor");

                                    // speciality
                                    JSONArray Speciality = doctor.getJSONArray("Speciality");
                                    for (int i = 0; i < Speciality.length(); i++) {
                                        JSONObject specialityObj = Speciality.getJSONObject(i);

                                        if (i == Speciality.length() - 1) {
                                            speciality += specialityObj.getString("Speciality");
                                        } else {
                                            speciality += specialityObj.getString("Speciality") + " and ";
                                        }
                                    }
                                    System.out.println("speciality:::" + speciality);

                                    // rating
                                    JSONObject Rating = doctor.getJSONObject("Rating");

                                    // scope of practice
                                    for (int i = 0; i < Speciality.length(); i++) {
                                        JSONObject specialityObj = Speciality.getJSONObject(i);

                                        HashMap<String, String> map = new HashMap<>();
                                        map.put("Speciality", specialityObj.getString("Speciality"));

                                        scopeOfPracticeList.add(map);
                                    }

                                    // education
                                    JSONArray Education = doctor.getJSONArray("Education");
                                    for (int i = 0; i < Education.length(); i++) {
                                        JSONObject educationObj = Education.getJSONObject(i);

                                        HashMap<String, String> map = new HashMap<>();
                                        map.put("Description", educationObj.getString("Description"));
                                        map.put("StartYear", educationObj.getString("StartYear"));
                                        map.put("EndYear", educationObj.getString("EndYear"));

                                        educationList.add(map);
                                    }

                                    // professional rotation
                                    JSONArray Professional_Rotation = doctor.getJSONArray("Experience");
                                    for (int i = 0; i < Professional_Rotation.length(); i++) {
                                        JSONObject professionalObj = Professional_Rotation.getJSONObject(i);

                                        HashMap<String, String> map = new HashMap<>();
                                        map.put("Description", professionalObj.getString("Description"));
                                        map.put("StartYear", professionalObj.getString("StartYear"));
                                        map.put("EndYear", professionalObj.getString("EndYear"));

                                        professionalRotationList.add(map);
                                    }

                                    // membership
                                    JSONArray Membership = doctor.getJSONArray("Membership");
                                    for (int i = 0; i < Membership.length(); i++) {
                                        JSONObject membershipObj = Membership.getJSONObject(i);

                                        HashMap<String, String> map = new HashMap<>();
                                        map.put("Description", membershipObj.getString("Description"));
                                        map.put("StartYear", membershipObj.getString("StartYear"));
                                        map.put("EndYear", membershipObj.getString("EndYear"));

                                        membershipList.add(map);
                                    }

                                    // hospital affiliation
                                    JSONArray Affiliation = doctor.getJSONArray("Affiliation");
                                    for (int i = 0; i < Affiliation.length(); i++) {
                                        JSONObject affiliationObj = Affiliation.getJSONObject(i);

                                        HashMap<String, String> map = new HashMap<>();
                                        map.put("Description", affiliationObj.getString("Description"));
                                        map.put("StartYear", affiliationObj.getString("StartYear"));
                                        map.put("EndYear", affiliationObj.getString("EndYear"));

                                        hospitalAffiliationList.add(map);
                                    }

                                    // publications
                                    JSONArray Publication = doctor.getJSONArray("Publication");
                                    for (int i = 0; i < Publication.length(); i++) {
                                        JSONObject publicationObj = Publication.getJSONObject(i);

                                        HashMap<String, String> map = new HashMap<>();
                                        map.put("Description", publicationObj.getString("Description"));
                                        map.put("StartYear", publicationObj.getString("StartYear"));
                                        map.put("EndYear", publicationObj.getString("EndYear"));

                                        publicationsList.add(map);
                                    }

                                    // feedback
                                    JSONArray Feedback = doctor.getJSONArray("Feedback");
                                    for (int i = 0; i < Feedback.length(); i++) {
                                        JSONObject feedbackObj = Feedback.getJSONObject(i);
                                        JSONObject userDetailObj = feedbackObj.getJSONObject("UserDetail");

                                        HashMap<String, String> map = new HashMap<>();
                                        map.put("name", userDetailObj.getString("FirstName") + " " + userDetailObj.getString("LastName"));
                                        map.put("date", getFormatedTime(userDetailObj.getString("updated_at")));
                                        map.put("comment", feedbackObj.getString("Feedback"));

                                        feedbackList.add(map);
                                    }

                                    doctorProfile = new DoctorProfile();
                                    doctorProfile.setDoctorId(doctor.getString("DoctorId"));
                                    doctorProfile.setFirstName(doctor.getString("FirstName"));
                                    doctorProfile.setLastName(doctor.getString("LastName"));
                                    doctorProfile.setLocalCharge(doctor.getString("LocalCharge"));
                                    doctorProfile.setSpeciality(doctor.getString("DoctorSpeciality"));
                                    doctorProfile.setAbout_doctor(doctor.getString("Description"));
                                    doctorProfile.setCosultCharge(doctor.getString("CosultCharge"));
                                    doctorProfile.setCountry(doctor.getString("Country"));
                                    doctorProfile.setResponseTime(doctor.getString("ResponseTime"));
                                    doctorProfile.setProfilePic(doctor.getString("ProfilePic"));
                                    doctorProfile.setIsSaved(doctor.getString("IsSaved"));
                                    doctorProfile.setIsFree(doctor.getString("IsFree"));
                                    doctorProfile.setDoctorType(doctor.getString("DoctorType"));
                                    //doctorProfile.setSpeciality(speciality);
                                    doctorProfile.setRating(Rating.getString("Rating"));
                                    doctorProfile.setTotalRating(Rating.getString("TotalRating"));
                                    doctorProfile.setScope_of_practice(scopeOfPracticeList);
                                    doctorProfile.setEducation(educationList);
                                    doctorProfile.setProfessional_rotation(professionalRotationList);
                                    doctorProfile.setMembership(membershipList);
                                    doctorProfile.setHospital_affiliation(hospitalAffiliationList);
                                    doctorProfile.setPublications(publicationsList);
                                    doctorProfile.setFeedback(feedbackList);

                                    setProfileData();

                                } else {
                                    Utils.showToast(mainObj.getString("error_string"), DRProfileActivity.this);
                                }
                            } else {
                                Utils.showToast(mainObj.getString("error_string"), DRProfileActivity.this);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;

                    case saveDoctor:
                        //Utils.removeSimpleSpinProgressDialog();
                        try {
                            JSONObject mainObj = new JSONObject(result);
                            if (mainObj.getString("error_code").equalsIgnoreCase("0")) {
                                int saved = mainObj.getInt("error_string");

                                try {
                                    if (saved == 1) {
                                        toggleFavourite.setChecked(true);
                                    } else {
                                        toggleFavourite.setChecked(false);
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;

                    case doctorChatDetails:
                        try {
                            JSONObject mainObj = new JSONObject(result);
                            if (mainObj.getString("error_code").equalsIgnoreCase("0")) {
                                //Utils.showToast(mainObj.getString("error_string"), GuestVerifyOTPActivity.this);
                                isChildAvailable(mainObj.getJSONObject("result"));

                            } else {
                                Utils.showToast(mainObj.getString("error_string"), DRProfileActivity.this);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;
                }
            }
        } catch (Exception e) {
            Utils.removeSimpleSpinProgressDialog();
            e.printStackTrace();
            Utils.showToast("Please try again later", this);
        }
    }

    @Override
    public void onProgressUpdate(String uniqueMessageId, int progres) {
    }

    @Override
    public void onProgressComplete(String uniqueMessageId, String result, Constant.REQUESTS request) {
    }

    private void isChildAvailable(JSONObject resultObj) {

        try {
            final String UniqueId = resultObj.getString("UniqueId");
            final String UserId = App.user.getUserID();
            final String DoctorId = resultObj.getString("DoctorId");
            final String FirstName = resultObj.getString("FirstName");
            final String LastName = resultObj.getString("LastName");
            final String ProfilePic = resultObj.getString("ProfilePic");

            fireDB = FirebaseDatabase.getInstance().getReference().child(MyConstants.DOCTOR_CHANNEL);

            fireDB.orderByChild(UniqueId).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    Log.i("Data snapshot DRchat", dataSnapshot.toString());
                    Utils.removeSimpleSpinProgressDialog();
                    Intent intent = new Intent(DRProfileActivity.this, DoctorFireChatActivity.class);


                    if (dataSnapshot.child(UniqueId).getValue() == null) {
                        intent.putExtra(MyConstants.IS_PAYMENT_DONE, true);
                    } else {
                        intent.putExtra(MyConstants.IS_PAYMENT_DONE, false);
                    }

                    intent.putExtra(MyConstants.PT_ID, UserId);
                    intent.putExtra(MyConstants.DR_ID, DoctorId);
                    intent.putExtra(MyConstants.UNIQUE_CHAT_ID, UniqueId);
                    intent.putExtra(MyConstants.DR_NAME, FirstName + " " + LastName);
                    intent.putExtra(MyConstants.Pt_NAME, App.user.getName());
                    intent.putExtra(MyConstants.RECEIVER_IMAGE_URL, ProfilePic);
                    startActivity(intent);
                    //Key does not exist
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });


        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    @SuppressLint("SetTextI18n")
    private void setProfileData() {
        tvDRName.setText(doctorProfile.getFirstName() + " " + doctorProfile.getLastName());
        mToolbarLayout.setTitle(doctorProfile.getFirstName() + " " + doctorProfile.getLastName());
        tvRating.setText(doctorProfile.getRating());
        tvTotalRating.setText("(" + doctorProfile.getTotalRating() + ") Ratings");

        if (doctorProfile.getIsFree().equalsIgnoreCase("1")) {
            tvFee.setText("Free");

        } else {
            if (!TextUtils.isEmpty(doctorProfile.getLocalCharge())) {
                tvFee.setText("$" + doctorProfile.getCosultCharge() + " (" + Html.fromHtml(doctorProfile.getLocalCharge()) + ")");
            } else {
                tvFee.setText("$" + doctorProfile.getCosultCharge());
            }
        }
        tvWaitingTime.setText(doctorProfile.getResponseTime());
        mLocationTxt.setText(doctorProfile.getCountry());
        mClinicVisitFeeTxt.setText("$" + doctorProfile.getCosultCharge() + " (" + Html.fromHtml(doctorProfile.getLocalCharge()) + ")");
        ratingBar.setRating(Float.valueOf(doctorProfile.getRating()));

        if (doctorProfile.getIsSaved().equalsIgnoreCase("1")) {
            toggleFavourite.setChecked(true);
        } else {
            toggleFavourite.setChecked(false);
        }

        try {
            PicassoTrustAll.getInstance(DRProfileActivity.this)
                    .load(doctorProfile.getProfilePic())
                    .placeholder(R.drawable.avatar)
                    .error(R.drawable.avatar)
                    .into(ivDRImage, new Callback() {
                        @Override
                        public void onSuccess() {

                        }

                        @Override
                        public void onError() {

                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (isNutrition.equalsIgnoreCase("1")) {
            tvAboutDoctorLabel.setText("About Dietitian");
        } else {
            tvAboutDoctorLabel.setText("About Doctor");
        }

        tvAboutDoctor.setText(doctorProfile.getAbout_doctor());
        ivArrow.setVisibility(View.GONE);

        if (TextUtils.isEmpty(doctorProfile.getAbout_doctor())) {
            tvAboutDoctorLabel.setVisibility(View.GONE);
            ll_about_doctor.setVisibility(View.GONE);
        } else {
            tvAboutDoctorLabel.setVisibility(View.VISIBLE);
            ll_about_doctor.setVisibility(View.VISIBLE);
        }

        if (doctorProfile.getEducation().size() > 0) {
            tvEducationLabel.setVisibility(View.VISIBLE);
            tvEducation.setVisibility(View.VISIBLE);
            StringBuilder educationStr = new StringBuilder();
            for (int i = 0; i < doctorProfile.getEducation().size(); i++) {
                if (!TextUtils.isEmpty(doctorProfile.getEducation().get(i).get("StartYear").trim()) && !TextUtils.isEmpty(doctorProfile.getEducation().get(i).get("EndYear").trim())) {
                    educationStr.append("<font color='#39c3f6'>&#62;&nbsp;&nbsp;</font>")
                            .append(doctorProfile.getEducation().get(i).get("Description"))
                            .append(", ")
                            .append(doctorProfile.getEducation().get(i).get("StartYear"))
                            .append(" - ")
                            .append(doctorProfile.getEducation().get(i).get("EndYear"))
                            .append("<br/>");
                } else if (TextUtils.isEmpty(doctorProfile.getEducation().get(i).get("EndYear").trim())) {
                    educationStr.append("<font color='#39c3f6'>&#62;&nbsp;&nbsp;</font>")
                            .append(doctorProfile.getEducation().get(i).get("Description"))
                            .append(", ")
                            .append(doctorProfile.getEducation().get(i).get("StartYear"))
                            .append("<br/>");
                } else if (TextUtils.isEmpty(doctorProfile.getEducation().get(i).get("StartYear").trim()) && TextUtils.isEmpty(doctorProfile.getEducation().get(i).get("EndYear").trim())) {
                    educationStr.append("<font color='#39c3f6'>&#62;&nbsp;&nbsp;</font>")
                            .append(doctorProfile.getEducation().get(i).get("Description"))
                            .append("<br/>");
                }
            }

            System.out.println("string:::" + educationStr);

            tvEducation.setText(noTrailingWhiteLines(Html.fromHtml(educationStr.toString())), TextView.BufferType.SPANNABLE);
        } else {
            tvEducation.setVisibility(View.GONE);
            tvEducationLabel.setVisibility(View.GONE);
        }

        if (doctorProfile.getScope_of_practice().size() > 0) {
            tvScopeOfPracticeLabel.setVisibility(View.VISIBLE);
            List<String> scopeOfPractice = new ArrayList<>();
            for (int i = 0; i < doctorProfile.getScope_of_practice().size(); i++) {
                scopeOfPractice.add(doctorProfile.getScope_of_practice().get(i).get("Speciality"));
            }
            tags.setTags(scopeOfPractice);
        } else {
            tvScopeOfPracticeLabel.setVisibility(View.GONE);
        }

        if (doctorProfile.getProfessional_rotation().size() > 0) {
            tvProfessionLabel.setVisibility(View.VISIBLE);
            for (int i = 0; i < doctorProfile.getProfessional_rotation().size(); i++) {
                View layout = LayoutInflater.from(DRProfileActivity.this).inflate(R.layout.layout_dr_profile_profession, llProfession, false);
                TextView tvDescription = layout.findViewById(R.id.tvDescription);
                TextView tvTitle = layout.findViewById(R.id.tvTitle);
                View line = layout.findViewById(R.id.line_view);

                if (TextUtils.isEmpty(doctorProfile.getProfessional_rotation().get(i).get("EndYear"))) {
                    tvTitle.setText(doctorProfile.getProfessional_rotation().get(i).get("StartYear"));
                } else {
                    tvTitle.setText(doctorProfile.getProfessional_rotation().get(i).get("StartYear") + " - " + doctorProfile.getProfessional_rotation().get(i).get("EndYear"));
                }
                tvDescription.setText(doctorProfile.getProfessional_rotation().get(i).get("Description"));

                if (i == doctorProfile.getProfessional_rotation().size() - 1) {
                    line.setVisibility(View.GONE);
                }

                llProfession.addView(layout);
            }
        } else {
            tvProfessionLabel.setVisibility(View.GONE);
        }

        if (doctorProfile.getMembership().size() > 0) {
            tvMembership.setVisibility(View.VISIBLE);
            tvMembershipLabel.setVisibility(View.VISIBLE);
            StringBuilder membershipStr = new StringBuilder();
            for (int i = 0; i < doctorProfile.getMembership().size(); i++) {
                membershipStr.append("<font color='#39c3f6'>&#62;&nbsp;&nbsp;</font>").append(doctorProfile.getMembership().get(i).get("Description")).append("<br/>");
            }

            tvMembership.setText(noTrailingWhiteLines(Html.fromHtml(membershipStr.toString())), TextView.BufferType.SPANNABLE);
        } else {
            tvMembership.setVisibility(View.GONE);
            tvMembershipLabel.setVisibility(View.GONE);
            /*tvMembership.setText("N/A");*/
        }

        if (doctorProfile.getHospital_affiliation().size() > 0) {
            tvHospitalAffiliation.setVisibility(View.VISIBLE);
            tvHospitalLabel.setVisibility(View.VISIBLE);
            StringBuilder hospitalAffiliationStr = new StringBuilder();
            for (int i = 0; i < doctorProfile.getHospital_affiliation().size(); i++) {
                hospitalAffiliationStr.append("<font color='#39c3f6'>&#62;&nbsp;&nbsp;</font>").append(doctorProfile.getHospital_affiliation().get(i).get("Description")).append("<br/>");
            }

            tvHospitalAffiliation.setText(noTrailingWhiteLines(Html.fromHtml(hospitalAffiliationStr.toString())), TextView.BufferType.SPANNABLE);
        } else {
            tvHospitalAffiliation.setVisibility(View.GONE);
            tvHospitalLabel.setVisibility(View.GONE);
//            tvHospitalAffiliation.setText("N/A");
        }

        if (doctorProfile.getPublications().size() > 0) {
            tvPublications.setVisibility(View.VISIBLE);
            tvPublicationLabel.setVisibility(View.VISIBLE);
            StringBuilder publicationStr = new StringBuilder();
            for (int i = 0; i < doctorProfile.getPublications().size(); i++) {
                publicationStr.append("<font color='#39c3f6'>&#62;&nbsp;&nbsp;</font>").append(doctorProfile.getPublications().get(i).get("Description")).append("<br/>");
            }

            tvPublications.setText(noTrailingWhiteLines(Html.fromHtml(publicationStr.toString())), TextView.BufferType.SPANNABLE);
        } else {
//            tvPublications.setText("N/A");
            tvPublications.setVisibility(View.GONE);
            tvPublicationLabel.setVisibility(View.GONE);
        }

        if (doctorProfile.getFeedback().size() > 0) {
            tvComments.setVisibility(View.VISIBLE);
            lvComments.setVisibility(View.VISIBLE);
            line1.setVisibility(View.VISIBLE);

            drProfileCommentsAdapter = new DRProfileCommentsAdapter(DRProfileActivity.this, doctorProfile.getFeedback());
            lvComments.setAdapter(drProfileCommentsAdapter);

            lvComments.setExpanded(true);
        } else {
            tvComments.setVisibility(View.GONE);
            lvComments.setVisibility(View.GONE);
            line1.setVisibility(View.GONE);
        }

        if (!doctorProfile.getDoctorType().equals("")) {
            String[] drType = doctorProfile.getDoctorType().split(",");
            ArrayList<String> drTypeArr = new ArrayList<>(Arrays.asList(drType));

            if (drTypeArr.contains("0")) {
                mRequestInformationBtn.setVisibility(View.VISIBLE);
                mClinicVisitFeeLayout.setVisibility(View.GONE);
                mEconsultationFeeLayout.setVisibility(View.GONE);

            } else {

                if (drTypeArr.contains("1")) {
                    //mBookinPersonBtn.setVisibility(View.VISIBLE);
                    //mRequestInformationBtn.setVisibility(View.VISIBLE);
                    mBookEConsultBtn.setVisibility(View.VISIBLE);
                    mEconsultationFeeLayout.setVisibility(View.VISIBLE);
                }

                if (drTypeArr.contains("2")) {
                    mBookinPersonBtn.setVisibility(View.VISIBLE);
                    mClinicVisitFeeLayout.setVisibility(View.VISIBLE);
                }
            }
        }
    }

    private CharSequence noTrailingWhiteLines(CharSequence text) {

        while (text.charAt(text.length() - 1) == '\n') {
            text = text.subSequence(0, text.length() - 1);
        }
        return text;//85CCkFfCYAWzHiMBvS1EMCd67JE=
    }

    public String getFormatedTime(String date) {
        try {
            Log.i("DATE", "IN Android FORMAT");
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);

            SimpleDateFormat newFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm a", Locale.US);
            Date currentDate;

            currentDate = format.parse(date);
            return newFormat.format(currentDate);
        } catch (ParseException e) {
            e.printStackTrace();
            return "";
        }
    }


    @OnClick(R.id.btn_book_econsult)
    void onBookEConsultClick() {
        MyConstants.doctorProfile = doctorProfile;
        MyConstants.doctorLastName = doctorProfile.getLastName();

        if (TextUtils.isEmpty(App.user.getUserID())) {
            openGuestDialog();

        } else {
            Bundle b = new Bundle();
            b.putSerializable("doctor", doctorProfile);

            if (doctorProfile.getDoctorId().equalsIgnoreCase(MyConstants.doctorIdForQuestionnaire)) {
                if (!App.isActivityRunning(DRProfileActivity.this, DRQuestionnaire1Activity.class)) {
                    Intent intent = new Intent(DRProfileActivity.this, DRQuestionnaire1Activity.class);
                    startActivity(intent);
                }
            } else {
                if (!App.isActivityRunning(DRProfileActivity.this, ConfirmEConsultBookingActivity.class)) {
                    Intent intent = new Intent(DRProfileActivity.this, ConfirmEConsultBookingActivity.class);
                    intent.putExtras(b);
                    startActivity(intent);
                }
            }
        }
    }


    @OnClick(R.id.btn_request_information)
    void onRequestInformationClick() {
        startActivity(new Intent(DRProfileActivity.this, RequestInformationActivity.class)
                .putExtra("DOCTOR_PROFILE", doctorProfile));
    }


    @OnClick(R.id.btn_bookin_person)
    void onBookinPersonClick() {
        startActivity(new Intent(DRProfileActivity.this, BookInPersonActivity.class)
                .putExtra("DOCTOR_PROFILE", doctorProfile));
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
