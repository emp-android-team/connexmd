package com.connex.md.patient.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.TextView;

import com.connex.md.R;
import com.connex.md.others.App;
import com.connex.md.ws.MyConstants;
import com.connex.md.ws.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DRQuestionnaire1Activity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    TextView tv1, tv2, tv3;
    LinearLayout llEmail, llPhone;
    Button btnNext;
    EditText etFullName, etEmail, etAge, etPhone;
    RadioGroup rgGender;
    private SharedPreferences sharedpreferences;
    ScrollView scrollView;
    private LinearLayout root;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dr_questionnaire1);

        ButterKnife.bind(this);

        setupToolbar();

        sharedpreferences = getSharedPreferences(MyConstants.PREF, Context.MODE_PRIVATE);

        tv1 = findViewById(R.id.tv1);
        tv2 = findViewById(R.id.tv2);
        tv3 = findViewById(R.id.tv3);
        tv1.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.zoom_in_out));

        btnNext = findViewById(R.id.btnNext);
        etFullName = findViewById(R.id.etFullName);
        etEmail = findViewById(R.id.etEmail);
        etPhone = findViewById(R.id.etPhone);
        etAge = findViewById(R.id.etAge);
        rgGender = findViewById(R.id.rgGender);
        scrollView = findViewById(R.id.scroll);

        llEmail = findViewById(R.id.llEmail);
        llPhone = findViewById(R.id.llPhone);
        root = findViewById(R.id.root);

        setupUI(root);

        etFullName.setEnabled(false);
        etFullName.setFocusable(false);
        etFullName.setCursorVisible(false);

        etEmail.setEnabled(false);
        etEmail.setFocusable(false);
        etEmail.setCursorVisible(false);

        etPhone.setEnabled(false);
        etPhone.setFocusable(false);
        etPhone.setCursorVisible(false);

        etFullName.setText(App.user.getFirstName() + " " + App.user.getLastName());

        if (MyConstants.isGuest.equalsIgnoreCase("0")){
            etEmail.setText(App.user.getUserEmail());
            llEmail.setVisibility(View.VISIBLE);
            llPhone.setVisibility(View.GONE);
        } else {
            etPhone.setText(sharedpreferences.getString(MyConstants.MOBILE, ""));
            llEmail.setVisibility(View.GONE);
            llPhone.setVisibility(View.VISIBLE);
        }

        tv2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnNext.performClick();
            }
        });


        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String fullName = etFullName.getText().toString().trim();
                String email = etEmail.getText().toString().trim();
                String age = etAge.getText().toString().trim();

                // get selected radio button from radioGroup
                int selectedId = rgGender.getCheckedRadioButtonId();
                // find the radiobutton by returned id
                RadioButton radioButton = findViewById(selectedId);

                String gender = radioButton.getText().toString().trim();
                System.out.println("gender::::" + gender);

                if (TextUtils.isEmpty(age)) {
                    etAge.setError("Age can't be empty");
                    etAge.requestFocus();
                } else if (TextUtils.isEmpty(gender)) {
                    Utils.showToast("Select gender", DRQuestionnaire1Activity.this);
                } else {
                    JSONObject Stage_1 = new JSONObject();
                    try {

                        Stage_1.put("Age", age);
                        Stage_1.put("Gender", gender);

                        MyConstants.questionnaire.put("Stage_1", Stage_1);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    System.out.println("json::::"+MyConstants.questionnaire);

                    startActivity(new Intent(DRQuestionnaire1Activity.this, DRQuestionnaire2Activity.class));
                    finish();
                }
            }
        });
    }


    private void setupToolbar() {
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle("Basic Details");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    public void setupUI(View view) {

        // Set up touch listener for non-text box views to hide keyboard.
        if (!(view instanceof EditText)) {
            view.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    hideSoftKeyboard(DRQuestionnaire1Activity.this);
                    return false;
                }
            });
        }

        //If a layout container, iterate over children and seed recursion.
        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                View innerView = ((ViewGroup) view).getChildAt(i);
                setupUI(innerView);
            }
        }
    }

    public static void hideSoftKeyboard(Activity activity) {
        try {
            InputMethodManager inputMethodManager =
                    (InputMethodManager) activity.getSystemService(
                            Activity.INPUT_METHOD_SERVICE);
            if (inputMethodManager != null) {
                inputMethodManager.hideSoftInputFromWindow(
                        Objects.requireNonNull(activity.getCurrentFocus()).getWindowToken(), 0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected void onClickAway() {
        //hide soft keyboard
        try {
            InputMethodManager inputMethodManager = (InputMethodManager)
                    getSystemService(Context.INPUT_METHOD_SERVICE);
            if (inputMethodManager != null) {
                inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
