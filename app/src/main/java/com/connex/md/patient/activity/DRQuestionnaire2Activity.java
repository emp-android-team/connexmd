package com.connex.md.patient.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.connex.md.R;
import com.connex.md.custom_views.RadioGridGroup;
import com.connex.md.ws.MyConstants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DRQuestionnaire2Activity extends AppCompatActivity implements CompoundButton.OnCheckedChangeListener {

    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    TextView tv1,tv2, tv3;
    Button btnNext;
    RadioGridGroup rgAlcoholIntake, rgDrugIntake;
    RadioGroup rgSmokingHabits, rgMedication;
    EditText etCigarette, etMedication;
    // Dermatological History
    CheckBox cb1, cb2, cb3, cb4, cb5, cb6, cb7, cb8, cb9, cb10, cb11, cb12, cb13, cb14, cb15, cb16;
    // Allergies
    CheckBox cb17, cb18, cb19, cb20, cb21, cb22;

    CheckBox[] checkBoxesDermatological, checkBoxesAllergies;

    private LinearLayout root;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dr_questionnaire2);

        ButterKnife.bind(this);

        setupToolbar();

        tv1 = findViewById(R.id.tv1);
        tv2 = findViewById(R.id.tv2);
        tv3 = findViewById(R.id.tv3);
        tv2.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.zoom_in_out));

        btnNext = findViewById(R.id.btnNext);
        etCigarette = findViewById(R.id.etCigarette);
        etMedication = findViewById(R.id.etMedication);
        rgSmokingHabits = findViewById(R.id.rgSmokingHabits);
        rgAlcoholIntake = findViewById(R.id.rgAlcoholIntake);
        rgDrugIntake = findViewById(R.id.rgDrugIntake);
        rgMedication = findViewById(R.id.rgMedication);

        cb1 = findViewById(R.id.cb1);
        cb2 = findViewById(R.id.cb2);
        cb3 = findViewById(R.id.cb3);
        cb4 = findViewById(R.id.cb4);
        cb5 = findViewById(R.id.cb5);
        cb6 = findViewById(R.id.cb6);
        cb7 = findViewById(R.id.cb7);
        cb8 = findViewById(R.id.cb8);
        cb9 = findViewById(R.id.cb9);
        cb10 = findViewById(R.id.cb10);
        cb11 = findViewById(R.id.cb11);
        cb12 = findViewById(R.id.cb12);
        cb13 = findViewById(R.id.cb13);
        cb14 = findViewById(R.id.cb14);
        cb15 = findViewById(R.id.cb15);
        cb16 = findViewById(R.id.cb16);
        cb17 = findViewById(R.id.cb17);
        cb18 = findViewById(R.id.cb18);
        cb19 = findViewById(R.id.cb19);
        cb20 = findViewById(R.id.cb20);
        cb21 = findViewById(R.id.cb21);
        cb22 = findViewById(R.id.cb22);

        cb1.setOnCheckedChangeListener(this);
        cb2.setOnCheckedChangeListener(this);
        cb3.setOnCheckedChangeListener(this);
        cb4.setOnCheckedChangeListener(this);
        cb5.setOnCheckedChangeListener(this);
        cb6.setOnCheckedChangeListener(this);
        cb7.setOnCheckedChangeListener(this);
        cb8.setOnCheckedChangeListener(this);
        cb9.setOnCheckedChangeListener(this);
        cb10.setOnCheckedChangeListener(this);
        cb11.setOnCheckedChangeListener(this);
        cb12.setOnCheckedChangeListener(this);
        cb13.setOnCheckedChangeListener(this);
        cb14.setOnCheckedChangeListener(this);
        cb15.setOnCheckedChangeListener(this);
        cb16.setOnCheckedChangeListener(this);
        cb17.setOnCheckedChangeListener(this);
        cb18.setOnCheckedChangeListener(this);
        cb19.setOnCheckedChangeListener(this);
        cb20.setOnCheckedChangeListener(this);
        cb21.setOnCheckedChangeListener(this);
        cb22.setOnCheckedChangeListener(this);

        root = findViewById(R.id.root);

        setupUI(root);

        checkBoxesDermatological = new CheckBox[]{cb1, cb2, cb3, cb4, cb5, cb6, cb7, cb8, cb9, cb10, cb11, cb12, cb13, cb14, cb15, cb16};
        checkBoxesAllergies = new CheckBox[]{cb17, cb18, cb19, cb20, cb21, cb22};

        rgMedication.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int checkedId) {
                if (checkedId == R.id.rbYes) {
                    etMedication.setVisibility(View.VISIBLE);
                } else {
                    etMedication.setVisibility(View.GONE);
                }
            }
        });

        rgSmokingHabits.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int checkedId) {
                if (checkedId == R.id.rbNoneCigarette){
                    etCigarette.setVisibility(View.GONE);
                } else {
                    etCigarette.setVisibility(View.VISIBLE);
                }
            }
        });

        tv1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(DRQuestionnaire2Activity.this, DRQuestionnaire1Activity.class));
                finish();
            }
        });

        tv3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnNext.performClick();
            }
        });

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // get selected radio button from radioGroup
                int selectedIdSmoking = rgSmokingHabits.getCheckedRadioButtonId();
                // find the radio button by returned id
                RadioButton radioButtonSmoking = findViewById(selectedIdSmoking);

                String smokingHabits = radioButtonSmoking.getText().toString().trim();

                int selectedIdAlcohol = rgAlcoholIntake.getCheckedRadioButtonId();
                RadioButton radioButtonAlcohol = findViewById(selectedIdAlcohol);

                String alcoholIntake = radioButtonAlcohol.getText().toString().trim();

                int selectedIdDrug = rgDrugIntake.getCheckedRadioButtonId();
                RadioButton radioButtonDrug = findViewById(selectedIdDrug);

                String drugIntake = radioButtonDrug.getText().toString().trim();

                int selectedIdMedication = rgMedication.getCheckedRadioButtonId();
                RadioButton radioButtonMedication = findViewById(selectedIdMedication);

                String medication = radioButtonMedication.getText().toString().trim();
                System.out.println("medication:::"+medication);

                String medicationDescription = medication;

                String dermatologicalHistory = "";
                for (int i = 0; i < checkBoxesDermatological.length; i++) {
                    if (checkBoxesDermatological[i].isChecked()) {
                        dermatologicalHistory += checkBoxesDermatological[i].getText().toString().trim() + "--";
                    }
                }

                if (!TextUtils.isEmpty(dermatologicalHistory)) {
                    dermatologicalHistory = dermatologicalHistory.replaceAll("--$", "");
                }

                System.out.println("dermatologicalHistory:::" + dermatologicalHistory);

                String allergies = "";
                for (int i = 0; i < checkBoxesAllergies.length; i++) {
                    if (checkBoxesAllergies[i].isChecked()) {
                        allergies += checkBoxesAllergies[i].getText().toString().trim() + "--";
                    }
                }

                if (!TextUtils.isEmpty(allergies)) {
                    allergies = allergies.replaceAll("--$", "");
                }

                System.out.println("allergies:::" + allergies);

                if (smokingHabits.equalsIgnoreCase("Cigarettes per day")) {
                    System.out.println("smoking");
                    if (etCigarette.getText().toString().isEmpty()) {
                        etCigarette.setError("Enter number of cigarettes");
                        etCigarette.requestFocus();
                    } else {
                        smokingHabits = etCigarette.getText().toString().trim() + " " + smokingHabits;

                        if (medication.equalsIgnoreCase("Yes")) {
                            System.out.println("medication");
                            medicationDescription = etMedication.getText().toString().trim();
                            if (TextUtils.isEmpty(medicationDescription)) {
                                etMedication.setError("Enter medication details");
                                etMedication.requestFocus();
                            } else {
                                medicationDescription = "Yes," + medicationDescription;

                                sendDataForAPI(smokingHabits, alcoholIntake, drugIntake, dermatologicalHistory,allergies, medicationDescription);

                                startActivity(new Intent(DRQuestionnaire2Activity.this, DRQuestionnaire3Activity.class));
                                finish();
                            }
                        } else {

                            medicationDescription = "No";

                            sendDataForAPI(smokingHabits, alcoholIntake, drugIntake, dermatologicalHistory,allergies, medicationDescription);

                            startActivity(new Intent(DRQuestionnaire2Activity.this, DRQuestionnaire3Activity.class));
                            finish();
                        }
                    }
                } else if (medication.equalsIgnoreCase("Yes")) {
                    System.out.println("medication");
                    medicationDescription = etMedication.getText().toString().trim();
                    if (TextUtils.isEmpty(medicationDescription)) {
                        etMedication.setError("Enter medication details");
                        etMedication.requestFocus();
                    } else {
                        medicationDescription = "Yes," + medicationDescription;

                        sendDataForAPI(smokingHabits, alcoholIntake, drugIntake, dermatologicalHistory,allergies, medicationDescription);

                        startActivity(new Intent(DRQuestionnaire2Activity.this, DRQuestionnaire3Activity.class));
                        finish();
                    }
                } else {
                    System.out.println("other");

                    sendDataForAPI(smokingHabits, alcoholIntake, drugIntake, dermatologicalHistory,allergies, medicationDescription);

                    startActivity(new Intent(DRQuestionnaire2Activity.this, DRQuestionnaire3Activity.class));
                    finish();
                }
            }
        });
    }


    private void setupToolbar() {
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle("Medical History");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    public void setupUI(View view) {

        // Set up touch listener for non-text box views to hide keyboard.
        if (!(view instanceof EditText)) {
            view.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    hideSoftKeyboard(DRQuestionnaire2Activity.this);
                    return false;
                }
            });
        }

        //If a layout container, iterate over children and seed recursion.
        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                View innerView = ((ViewGroup) view).getChildAt(i);
                setupUI(innerView);
            }
        }
    }

    public static void hideSoftKeyboard(Activity activity) {
        try {
            InputMethodManager inputMethodManager =
                    (InputMethodManager) activity.getSystemService(
                            Activity.INPUT_METHOD_SERVICE);
            if (inputMethodManager != null) {
                inputMethodManager.hideSoftInputFromWindow(
                        Objects.requireNonNull(activity.getCurrentFocus()).getWindowToken(), 0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void sendDataForAPI(String smokingHabits, String alcoholIntake, String drugIntake, String dermatologicalHistory, String allergies, String medicationDescription) {
        
        try {
            JSONArray Stage_2 = new JSONArray();
            
            JSONObject objectMedicalHistory = new JSONObject();
            objectMedicalHistory.put("StageHeader", "Medical History");
            objectMedicalHistory.put("QuestionHeader", "Social Habits");
            objectMedicalHistory.put("QuestionHeaderOrder", "1");

            JSONArray arrayMedicalHistory = new JSONArray();
            // smoking habits
            JSONObject objSmoking = new JSONObject();
            objSmoking.put("QuestionType", "2");
            objSmoking.put("QuestionOrder", "1");
            objSmoking.put("Question", "Smoking Habits");
            objSmoking.put("Answer", smokingHabits);
            // add object to array
            arrayMedicalHistory.put(objSmoking);

            // alcohol intake
            JSONObject objAlcohol = new JSONObject();
            objAlcohol.put("QuestionType", "2");
            objAlcohol.put("QuestionOrder", "2");
            objAlcohol.put("Question", "Alcohol Intake");
            objAlcohol.put("Answer", alcoholIntake);
            // add object to array
            arrayMedicalHistory.put(objAlcohol);

            // drug intake
            JSONObject objDrug = new JSONObject();
            objDrug.put("QuestionType", "2");
            objDrug.put("QuestionOrder", "3");
            objDrug.put("Question", "Drug Intake");
            objDrug.put("Answer", drugIntake);
            // add object to array
            arrayMedicalHistory.put(objDrug);

            // add QA to object
            objectMedicalHistory.put("QA",arrayMedicalHistory);

            // add object to Stage_2
            Stage_2.put(objectMedicalHistory);


            JSONObject objectPersonalHistory = new JSONObject();
            objectPersonalHistory.put("StageHeader", "Medical History");
            objectPersonalHistory.put("QuestionHeader", "Personal Dermatological History");
            objectPersonalHistory.put("QuestionHeaderOrder", "2");

            // Personal Dermatological History
            JSONObject objPersonalHistory = new JSONObject();
            objPersonalHistory.put("QuestionType", "3");
            objPersonalHistory.put("QuestionOrder", "1");
            objPersonalHistory.put("Question", "Personal Dermatological History");
            objPersonalHistory.put("Answer", dermatologicalHistory);

            // add object to array
            objectPersonalHistory.put("QA",objPersonalHistory);

            // add object to Stage_2
            Stage_2.put(objectPersonalHistory);


            JSONObject objectAllergies = new JSONObject();
            objectAllergies.put("StageHeader", "Medical History");
            objectAllergies.put("QuestionHeader", "Allergies");
            objectAllergies.put("QuestionHeaderOrder", "3");

            // Allergies
            JSONObject objAllergies = new JSONObject();
            objAllergies.put("QuestionType", "3");
            objAllergies.put("QuestionOrder", "1");
            objAllergies.put("Question", "Are you allergic to any of the following medications?Are you allergic to any of the following medications?");
            objAllergies.put("Answer", allergies);

            // add object to array
            objectAllergies.put("QA",objAllergies);

            // add object to Stage_2
            Stage_2.put(objectAllergies);


            JSONObject objectMedication = new JSONObject();
            objectMedication.put("StageHeader", "Medical History");
            objectMedication.put("QuestionHeader", "Current Medications");
            objectMedication.put("QuestionHeaderOrder", "4");

            // Current Medications
            JSONObject objCurrentMedication = new JSONObject();
            objCurrentMedication.put("QuestionType", "4");
            objCurrentMedication.put("QuestionOrder", "1");
            objCurrentMedication.put("Question", "Are you currently taking any medications?");
            objCurrentMedication.put("Answer", medicationDescription);

            // add object to array
            objectMedication.put("QA",objCurrentMedication);

            // add object to Stage_2
            Stage_2.put(objectMedication);

            MyConstants.questionnaire.put("Stage_2", Stage_2);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        System.out.println("json::::"+MyConstants.questionnaire);

    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
        switch (compoundButton.getId()) {
            case R.id.cb1:
                if (b) {
                    cb1.setChecked(true);
                    cb2.setChecked(false);
                    cb3.setChecked(false);
                    cb4.setChecked(false);
                    cb5.setChecked(false);
                    cb6.setChecked(false);
                    cb7.setChecked(false);
                    cb8.setChecked(false);
                    cb9.setChecked(false);
                    cb10.setChecked(false);
                    cb11.setChecked(false);
                    cb12.setChecked(false);
                    cb13.setChecked(false);
                    cb14.setChecked(false);
                    cb15.setChecked(false);
                    cb16.setChecked(false);

                    /*cb2.setEnabled(false);
                    cb3.setEnabled(false);
                    cb4.setEnabled(false);
                    cb5.setEnabled(false);
                    cb6.setEnabled(false);
                    cb7.setEnabled(false);
                    cb8.setEnabled(false);
                    cb9.setEnabled(false);
                    cb10.setEnabled(false);
                    cb11.setEnabled(false);
                    cb12.setEnabled(false);
                    cb13.setEnabled(false);
                    cb14.setEnabled(false);
                    cb15.setEnabled(false);
                    cb16.setEnabled(false);*/
                } else {
                    cb2.setEnabled(true);
                    cb3.setEnabled(true);
                    cb4.setEnabled(true);
                    cb5.setEnabled(true);
                    cb6.setEnabled(true);
                    cb7.setEnabled(true);
                    cb8.setEnabled(true);
                    cb9.setEnabled(true);
                    cb10.setEnabled(true);
                    cb11.setEnabled(true);
                    cb12.setEnabled(true);
                    cb13.setEnabled(true);
                    cb14.setEnabled(true);
                    cb15.setEnabled(true);
                    cb16.setEnabled(true);
                }
                break;

            case R.id.cb2:
            case R.id.cb3:
            case R.id.cb4:
            case R.id.cb5:
            case R.id.cb6:
            case R.id.cb7:
            case R.id.cb8:
            case R.id.cb9:
            case R.id.cb10:
            case R.id.cb11:
            case R.id.cb12:
            case R.id.cb13:
            case R.id.cb14:
            case R.id.cb15:
            case R.id.cb16:
            case R.id.cb17:
            case R.id.cb18:
            case R.id.cb19:
            case R.id.cb20:
            case R.id.cb21:
            case R.id.cb22:
                if (b)
                    cb1.setChecked(false);
                break;
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
