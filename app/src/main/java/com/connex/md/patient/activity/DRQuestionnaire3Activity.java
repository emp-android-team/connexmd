package com.connex.md.patient.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.ScrollView;
import android.widget.TextView;

import com.connex.md.R;
import com.connex.md.custom_views.MultiSelectionSpinner;
import com.connex.md.custom_views.RadioGridGroup;
import com.connex.md.interfaces.AsyncTaskListner;
import com.connex.md.model.DoctorProfile;
import com.connex.md.others.App;
import com.connex.md.others.Internet;
import com.connex.md.patient.adapter.DRQuestionnaire3Adapter;
import com.connex.md.utils.VerticalSpacingDecoration;
import com.connex.md.ws.CallRequest;
import com.connex.md.ws.Constant;
import com.connex.md.ws.MyConstants;
import com.connex.md.ws.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DRQuestionnaire3Activity extends AppCompatActivity implements MultiSelectionSpinner.OnMultipleItemsSelectedListener, AsyncTaskListner {

    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    private static int RESULT_LOAD_IMG = 1;
    TextView tv1, tv2, tv3;
    Button btnSubmit;
    EditText etReasonConsultation, etHowItStart, etReasonForWorstCondition, etPriorMedication, etOtherDetails;
    RecyclerView rvImagesAreaAffected, rvImagesShare;
    MultiSelectionSpinner spinnerSymptoms;
    RadioGridGroup rgDays;
    String[] symptomsArray = {"Itching Sensation", "Stinging Sensation", "Burning Sensation", "Painful Sensation", "None"};
    List<String> selectedSymptoms = new ArrayList<>();
    List<String> imagePathAreaAffected = new ArrayList<>();
    List<String> imagePathShare = new ArrayList<>();
    LinearLayoutManager linearLayoutManager, linearLayoutManager1;
    DRQuestionnaire3Adapter drQuestionnaire3Adapter, drQuestionnaire3Adapter1;
    String filePathFirst = "";
    String type;
    SharedPreferences sharedpreferences;
    DoctorProfile doctorProfile;
    ScrollView scrollView;
    LinearLayout llSpinnerSymptoms;
    TextView tvSymptoms, tvHowItStart, tvReasonForWorstCondition, tvPriorMedication, tvOtherDetails;
    private LinearLayout root;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dr_questionnaire3);

        ButterKnife.bind(this);

        setupToolbar();

        sharedpreferences = getSharedPreferences(MyConstants.PREF, MODE_PRIVATE);

        tv1 = findViewById(R.id.tv1);
        tv2 = findViewById(R.id.tv2);
        tv3 = findViewById(R.id.tv3);
        tv3.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.zoom_in_out));

        doctorProfile = MyConstants.doctorProfile;

        tvHowItStart = findViewById(R.id.tvHowItStart);
        tvReasonForWorstCondition = findViewById(R.id.tvReasonForWorstCondition);
        tvPriorMedication = findViewById(R.id.tvPriorMedication);
        tvOtherDetails = findViewById(R.id.tvOtherDetails);
        btnSubmit = findViewById(R.id.btnSubmit);
        etReasonConsultation = findViewById(R.id.etReasonConsultation);
        etHowItStart = findViewById(R.id.etHowItStart);
        etReasonForWorstCondition = findViewById(R.id.etReasonForWorstCondition);
        etPriorMedication = findViewById(R.id.etPriorMedication);
        etOtherDetails = findViewById(R.id.etOtherDetails);
        rvImagesAreaAffected = findViewById(R.id.rvImagesAreaAffected);
        rvImagesShare = findViewById(R.id.rvImagesShare);
        spinnerSymptoms = findViewById(R.id.spinnerSymptoms);
        rgDays = findViewById(R.id.rgDays);
        scrollView = findViewById(R.id.scroll);
        llSpinnerSymptoms = findViewById(R.id.llSpinnerSymptoms);
        tvSymptoms = findViewById(R.id.tvSymptoms);
        root = findViewById(R.id.root);

        setupUI(root);

        spinnerSymptoms.setItems(symptomsArray);
        spinnerSymptoms.setListener(DRQuestionnaire3Activity.this);

        llSpinnerSymptoms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                spinnerSymptoms.performClick();
            }
        });

        String howItStart = "How did it start / Where did it start? <font color='#a7a7a7'>(Optional)</font>";
        tvHowItStart.setText(Html.fromHtml(howItStart));

        String worstCondition = "What makes your condition worst / better? <font color='#a7a7a7'>(Optional)</font>";
        tvReasonForWorstCondition.setText(Html.fromHtml(worstCondition));

        String priorMedication = "Indicate prior medication applied to affected area and length of usage <font color='#a7a7a7'>(Optional)</font>";
        tvPriorMedication.setText(Html.fromHtml(priorMedication));

        String otherDetails = "Enter any other additional information <font color='#a7a7a7'>(Optional)</font>";
        tvOtherDetails.setText(Html.fromHtml(otherDetails));

        imagePathAreaAffected.add("by default");
        linearLayoutManager = new LinearLayoutManager(DRQuestionnaire3Activity.this, LinearLayoutManager.HORIZONTAL, false);
        rvImagesAreaAffected.setLayoutManager(linearLayoutManager);

        drQuestionnaire3Adapter = new DRQuestionnaire3Adapter(DRQuestionnaire3Activity.this, imagePathAreaAffected, "AreaAffected");
        rvImagesAreaAffected.setHasFixedSize(true);
        rvImagesAreaAffected.addItemDecoration(new VerticalSpacingDecoration(20));
        rvImagesAreaAffected.setItemViewCacheSize(20);
        rvImagesAreaAffected.setDrawingCacheEnabled(true);
        rvImagesAreaAffected.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        rvImagesAreaAffected.setAdapter(drQuestionnaire3Adapter);

        imagePathShare.add("by default");
        linearLayoutManager1 = new LinearLayoutManager(DRQuestionnaire3Activity.this, LinearLayoutManager.HORIZONTAL, false);
        rvImagesShare.setLayoutManager(linearLayoutManager1);

        drQuestionnaire3Adapter1 = new DRQuestionnaire3Adapter(DRQuestionnaire3Activity.this, imagePathShare, "Share");
        rvImagesShare.setHasFixedSize(true);
        rvImagesShare.addItemDecoration(new VerticalSpacingDecoration(20));
        rvImagesShare.setItemViewCacheSize(20);
        rvImagesShare.setDrawingCacheEnabled(true);
        rvImagesShare.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        rvImagesShare.setAdapter(drQuestionnaire3Adapter1);

        tv1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(DRQuestionnaire3Activity.this, DRQuestionnaire1Activity.class));
                finish();
            }
        });

        tv2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(DRQuestionnaire3Activity.this, DRQuestionnaire2Activity.class));
                finish();
            }
        });

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String reasonConsultation = etReasonConsultation.getText().toString().trim();
                String howItStart = etHowItStart.getText().toString().trim();
                String reasonForWorstCondition = etReasonForWorstCondition.getText().toString().trim();
                String priorMedication = etPriorMedication.getText().toString().trim();
                String otherDetails = etOtherDetails.getText().toString().trim();

                int selectedIdDays = rgDays.getCheckedRadioButtonId();
                RadioButton radioButtonDays = findViewById(selectedIdDays);

                String days = radioButtonDays.getText().toString().trim();


                String selectedSymptomsStr = "";

                if (selectedSymptoms.size() > 0) {
                    for (int i = 0; i < selectedSymptoms.size(); i++) {
                        selectedSymptomsStr += selectedSymptoms.get(i) + "--";
                    }
                }

                if (!TextUtils.isEmpty(selectedSymptomsStr)) {
                    selectedSymptomsStr = selectedSymptomsStr.replaceAll("--$", "");
                }

                System.out.println("selectedSymptomsStr:::" + selectedSymptomsStr);

                String selectedImagesAreaAffected = "";
                if (imagePathAreaAffected.size() > 1) {
                    for (int i = 0; i < imagePathAreaAffected.size() - 1; i++) {
                        selectedImagesAreaAffected += "Image" + (i + 1) + ",";
                    }
                }

                if (!TextUtils.isEmpty(selectedImagesAreaAffected)) {
                    selectedImagesAreaAffected = selectedImagesAreaAffected.replaceAll(",$", "");
                }

                int imagesAffectedAreaCount = imagePathAreaAffected.size();

                String selectedImagesShare = "";
                if (imagePathShare.size() > 1) {
                    for (int i = imagesAffectedAreaCount; i < imagePathShare.size() - 1 + imagesAffectedAreaCount; i++) {
                        selectedImagesShare += "Image" + i + ",";
                    }
                }

                if (!TextUtils.isEmpty(selectedImagesShare)) {
                    selectedImagesShare = selectedImagesShare.replaceAll(",$", "");
                }

                System.out.println("selectedImagesAreaAffected:::" + selectedImagesAreaAffected);
                System.out.println("selectedImagesShare:::" + selectedImagesShare);

                sendDataForAPI(reasonConsultation, selectedImagesAreaAffected, selectedSymptomsStr, days, howItStart, reasonForWorstCondition,
                        priorMedication, otherDetails, selectedImagesShare);
            }
        });

    }

    private void setupToolbar() {
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle("Clinical History");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    public void setupUI(View view) {

        // Set up touch listener for non-text box views to hide keyboard.
        if (!(view instanceof EditText)) {
            view.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    hideSoftKeyboard(DRQuestionnaire3Activity.this);
                    return false;
                }
            });
        }

        //If a layout container, iterate over children and seed recursion.
        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                View innerView = ((ViewGroup) view).getChildAt(i);
                setupUI(innerView);
            }
        }
    }

    public static void hideSoftKeyboard(Activity activity) {
        try {
            InputMethodManager inputMethodManager =
                    (InputMethodManager) activity.getSystemService(
                            Activity.INPUT_METHOD_SERVICE);
            if (inputMethodManager != null) {
                inputMethodManager.hideSoftInputFromWindow(
                        Objects.requireNonNull(activity.getCurrentFocus()).getWindowToken(), 0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected void onClickAway() {
        //hide soft keyboard
        try {
            InputMethodManager inputMethodManager = (InputMethodManager)
                    getSystemService(Context.INPUT_METHOD_SERVICE);
            if (inputMethodManager != null) {
                inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void sendDataForAPI(String reasonConsultation, String selectedImagesAreaAffected, String selectedSymptomsStr, String days,
                                String howItStart, String reasonForWorstCondition, String priorMedication,
                                String otherDetails, String selectedImagesShare) {
        try {
            JSONArray Stage_3 = new JSONArray();

            JSONObject objectClinicalHistory = new JSONObject();
            objectClinicalHistory.put("StageHeader", "Clinical History");
            objectClinicalHistory.put("QuestionHeader", "Dermatological Questionnaire");
            objectClinicalHistory.put("QuestionHeaderOrder", "1");

            JSONArray arrayClinicalHistory = new JSONArray();

            // Reason for consultation
            JSONObject objConsultationReason = new JSONObject();
            objConsultationReason.put("QuestionType", "1");
            objConsultationReason.put("QuestionOrder", "1");
            objConsultationReason.put("Question", "Reason for consultation");
            objConsultationReason.put("Answer", reasonConsultation);
            // add object to array
            arrayClinicalHistory.put(objConsultationReason);

            // Pictures of all areas affected
            JSONObject objImagesAreaAffected = new JSONObject();
            objImagesAreaAffected.put("QuestionType", "6");
            objImagesAreaAffected.put("QuestionOrder", "2");
            objImagesAreaAffected.put("Question", "Pictures of all areas affected");
            objImagesAreaAffected.put("Answer", selectedImagesAreaAffected);
            // add object to array
            arrayClinicalHistory.put(objImagesAreaAffected);

            // Symptoms
            JSONObject objSymptoms = new JSONObject();
            objSymptoms.put("QuestionType", "5");
            objSymptoms.put("QuestionOrder", "3");
            objSymptoms.put("Question", "Symptoms");
            objSymptoms.put("Answer", selectedSymptomsStr);
            // add object to array
            arrayClinicalHistory.put(objSymptoms);

            // For how long did you have this condition?
            JSONObject objCondition = new JSONObject();
            objCondition.put("QuestionType", "2");
            objCondition.put("QuestionOrder", "4");
            objCondition.put("Question", "For how long did you have this condition?");
            objCondition.put("Answer", days);
            // add object to array
            arrayClinicalHistory.put(objCondition);

            // How did it start / Where did it start?
            JSONObject objHowDidItStart = new JSONObject();
            objHowDidItStart.put("QuestionType", "1");
            objHowDidItStart.put("QuestionOrder", "5");
            objHowDidItStart.put("Question", "How did it start / Where did it start?");
            objHowDidItStart.put("Answer", howItStart);
            // add object to array
            arrayClinicalHistory.put(objHowDidItStart);

            // What makes your condition worst / better?
            JSONObject objConditionBetter = new JSONObject();
            objConditionBetter.put("QuestionType", "1");
            objConditionBetter.put("QuestionOrder", "6");
            objConditionBetter.put("Question", "What makes your condition worst / better?");
            objConditionBetter.put("Answer", reasonForWorstCondition);
            // add object to array
            arrayClinicalHistory.put(objConditionBetter);

            // Indicate prior medication applied to affected area and length of usage
            JSONObject objPriorMedication = new JSONObject();
            objPriorMedication.put("QuestionType", "1");
            objPriorMedication.put("QuestionOrder", "7");
            objPriorMedication.put("Question", "Indicate prior medication applied to affected area and length of usage");
            objPriorMedication.put("Answer", priorMedication);
            // add object to array
            arrayClinicalHistory.put(objPriorMedication);

            // Enter any other additional information
            JSONObject objOtherInfo = new JSONObject();
            objOtherInfo.put("QuestionType", "1");
            objOtherInfo.put("QuestionOrder", "8");
            objOtherInfo.put("Question", "Enter any other additional information");
            objOtherInfo.put("Answer", otherDetails);
            // add object to array
            arrayClinicalHistory.put(objOtherInfo);

            // Share an Image
            JSONObject objImagesShare = new JSONObject();
            objImagesShare.put("QuestionType", "6");
            objImagesShare.put("QuestionOrder", "9");
            objImagesShare.put("Question", "Share an Image");
            objImagesShare.put("Answer", selectedImagesShare);
            // add object to array
            arrayClinicalHistory.put(objImagesShare);

            // add QA to object
            objectClinicalHistory.put("QA", arrayClinicalHistory);

            // add object to Stage_2
            Stage_3.put(objectClinicalHistory);

            MyConstants.questionnaire.put("Stage_3", Stage_3);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        System.out.println("json::::" + MyConstants.questionnaire);

        submitQuestionnaire();
    }

    private void submitQuestionnaire() {
        if (!Internet.isAvailable(DRQuestionnaire3Activity.this)) {
            Internet.showAlertDialog(DRQuestionnaire3Activity.this, "Error!", "No Internet Connection", false);
        } else {

            Map<String, String> map = new HashMap<String, String>();
            if (MyConstants.isGuest.equalsIgnoreCase("0")) {
                map.put("url", MyConstants.BASE_URL + "submitQuestionnaire");
                map.put("UserId", App.user.getUserID());
            } else {
                map.put("url", MyConstants.GUEST_BASE_URL + "submitQuestionnaire");
                map.put("GuestId", App.user.getUserID());
            }

            map.put("ApiToken", MyConstants.API_TOKEN);
            map.put("SocialType", App.user.getLoginType());
            map.put("SocialId", sharedpreferences.getString(MyConstants.SOCIAL_ID, "0"));
            map.put("Version", MyConstants.WS_VERSION);
            map.put("DoctorId", doctorProfile.getDoctorId());
            map.put("DoctorCharge", doctorProfile.getCosultCharge());
            map.put("ConsultTime", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US).format(new Date()));
            map.put("JSON", MyConstants.questionnaire.toString());

            int imagesAffectedAreaCount = imagePathAreaAffected.size();
            if (imagePathShare.size() > 1) {
                int j = 1;
                for (int i = imagesAffectedAreaCount; i < imagePathShare.size() - 1 + imagesAffectedAreaCount; i++) {
                    map.put("Image" + i, imagePathShare.get(j));
                    j++;
                }
            }

            if (imagePathAreaAffected.size() > 1) {
                for (int i = 1; i < imagePathAreaAffected.size(); i++) {
                    map.put("Image" + i, imagePathAreaAffected.get(i));
                }

                new CallRequest(DRQuestionnaire3Activity.this).submitQuestionnaire(map);
            } else {
                Utils.showAlert("Please attach at least one picture for affected body part", DRQuestionnaire3Activity.this);
            }

            System.out.println("Map:::" + map);

        }
    }

    @Override
    public void selectedIndices(List<Integer> indices, List<String> strings) {

    }

    @Override
    public void selectedStrings(List<String> strings) {
        System.out.println("strings:::" + strings);

        if (strings.size() > 0) {
            String symptoms = "";
            for (int i = 0; i < strings.size(); i++) {
                symptoms += strings.get(i) + ",";
            }

            if (!TextUtils.isEmpty(symptoms)) {
                symptoms = symptoms.replaceAll(",$", "");
            }

            tvSymptoms.setText(symptoms);
        } else {
            tvSymptoms.setText("Select Symptoms");
        }

        selectedSymptoms.clear();
        selectedSymptoms = strings;

        System.out.println("selectedSymptoms:::" + selectedSymptoms);
    }

    public void loadImageFromGallery(String type) {
        this.type = type;
        // Create intent to Open Image applications like Gallery, Google Photos
        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        // Start the Intent
        startActivityForResult(galleryIntent, RESULT_LOAD_IMG);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        try {
            // When an Image is picked
            if (requestCode == RESULT_LOAD_IMG && resultCode == RESULT_OK
                    && null != data) {
                // Get the Image from data

                Uri selectedImage = data.getData();
                String[] filePathColumn = {MediaStore.Images.Media.DATA};

                // Get the cursor
                Cursor cursor = getContentResolver().query(selectedImage,
                        filePathColumn, null, null, null);
                // Move to first row
                int columnIndex = 0;
                if (cursor != null) {
                    cursor.moveToFirst();
                    columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    filePathFirst = cursor.getString(columnIndex);
                    System.out.println(filePathFirst);
                    //imagePath.add(filePathDp);
                    cursor.close();
                    //drQuestionnaire3Adapter.notifyDataSetChanged();

                    if (type.equalsIgnoreCase("AreaAffected")) {
                        // Set The Bitmap Data To ImageView
                        imagePathAreaAffected.add(filePathFirst);
                        drQuestionnaire3Adapter.notifyDataSetChanged();
                    } else {
                        // Set The Bitmap Data To ImageView
                        imagePathShare.add(filePathFirst);
                        drQuestionnaire3Adapter1.notifyDataSetChanged();
                    }


                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        Utils.removeSimpleSpinProgressDialog();
        try {
            if (result != null && !result.isEmpty()) {
                Log.i("TAG", "TAG Result : " + result);
                switch (request) {
                    case submitQuestionnaire:
                        try {
                            JSONObject mainObj = new JSONObject(result);
                            if (mainObj.getString("error_code").equalsIgnoreCase("0")) {
                                Utils.showToast(mainObj.getString("error_string"), DRQuestionnaire3Activity.this);

                                JSONObject resultObj = mainObj.getJSONObject("result");
                                MyConstants.consultId = resultObj.getString("consult_id");
                                MyConstants.Wallet_Balance = resultObj.getString("WalletBalance");

                                MyConstants.isBooked = false;
                                Intent intent = new Intent(DRQuestionnaire3Activity.this, ConfirmPaymentActivity.class);
                                startActivity(intent);
                                finish();

                            } else {
                                Utils.showToast(mainObj.getString("error_string"), DRQuestionnaire3Activity.this);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onProgressUpdate(String uniqueMessageId, int progres) {

    }

    @Override
    public void onProgressComplete(String uniqueMessageId, String result, Constant.REQUESTS request) {

    }
}
