package com.connex.md.patient.activity;

import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.connex.md.R;
import com.connex.md.interfaces.AsyncTaskListner;
import com.connex.md.others.App;
import com.connex.md.others.Internet;
import com.connex.md.others.PicassoTrustAll;
import com.connex.md.ws.CallRequest;
import com.connex.md.ws.Constant;
import com.connex.md.ws.MyConstants;
import com.connex.md.ws.Utils;
import com.jsibbold.zoomage.ZoomageView;
import com.squareup.picasso.Callback;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DoctorNotesActivity extends AppCompatActivity implements AsyncTaskListner{

    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    Button btnPay, btnComingSoon;
    //ImageView ivRefer;
    RelativeLayout rv_preview;
    SharedPreferences sharedpreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_doctor_notes);

        ButterKnife.bind(this);
        setupToolbar();

        btnPay = findViewById(R.id.btnPay);
        btnComingSoon = findViewById(R.id.btnComingSoon);
        //ivRefer = findViewById(R.id.ivRefer);
        rv_preview = findViewById(R.id.rv_preview);

        sharedpreferences = getSharedPreferences(MyConstants.PREF, MODE_PRIVATE);

        // Pay $15
        btnPay.setText("Pay $" + MyConstants.headerPrice);

        btnPay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(DoctorNotesActivity.this, DoctorNotesFormActivity.class);
                startActivity(intent);
            }
        });

        btnComingSoon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                onBackPressed();
                if (!Internet.isAvailable(DoctorNotesActivity.this)) {
                    Internet.showAlertDialog(DoctorNotesActivity.this, "Error!", "No Internet Connection", false);
                } else {
                    Map<String, String> map = new HashMap<>();
                    map.put("url", MyConstants.BASE_URL + "votingFeature");
                    map.put("ApiToken", MyConstants.API_TOKEN);
                    map.put("Version", MyConstants.WS_VERSION);
                    map.put("UserId", App.user.getUserID());
                    map.put("SocialId", sharedpreferences.getString(MyConstants.SOCIAL_ID, "0"));
                    map.put("SocialType", App.user.getLoginType());

                    new CallRequest(DoctorNotesActivity.this).voteNow(map);
                }
            }
        });

        /*ivRefer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showBiggerImage();
            }
        });*/

        /*rv_preview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showBiggerImage();
            }
        });*/
    }


    private void setupToolbar() {
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle("Medical Note");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    public void showBiggerImage() {

        final Dialog nagDialog = new Dialog(DoctorNotesActivity.this, android.R.style.Theme_Translucent_NoTitleBar_Fullscreen);
        nagDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        nagDialog.setCancelable(true);

        LayoutInflater inflater = (DoctorNotesActivity.this).getLayoutInflater();
        View view = inflater.inflate(R.layout.layout_bigger_image, null);
        nagDialog.setContentView(view);

        final ProgressBar pBar = view.findViewById(R.id.pBar);

        Button btnClose = view.findViewById(R.id.btnIvClose);
        ZoomageView ivPreview = view.findViewById(R.id.iv_preview_image);

        try {
            PicassoTrustAll.getInstance(DoctorNotesActivity.this)
                    .load(R.drawable.doctor_note)
                    .into(ivPreview, new Callback() {
                        @Override
                        public void onSuccess() {
                            pBar.setVisibility(View.GONE);
                        }

                        @Override
                        public void onError() {
                            pBar.setVisibility(View.GONE);
                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }

        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                nagDialog.dismiss();
            }
        });
        nagDialog.show();
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        Utils.removeSimpleSpinProgressDialog();
        try {
            if (result != null && !result.isEmpty()) {
                Log.i("TAG", "TAG Result : " + result);
                //      {"document":{"response":{"status":1,"message":"Success."}}}
                switch (request) {
                    case voteNow:
                        try {
                            JSONObject mainObj = new JSONObject(result);
                            if (mainObj.getString("error_code").equalsIgnoreCase("0")) {
                                //Utils.showToast(mainObj.getString("error_string"), MentionQueryActivity.this);

                                Utils.showToast(mainObj.getString("error_string"), DoctorNotesActivity.this);
                                onBackPressed();
                            } else {
                                Utils.showToast(mainObj.getString("error_string"), DoctorNotesActivity.this);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onProgressUpdate(String uniqueMessageId, int progres) {

    }

    @Override
    public void onProgressComplete(String uniqueMessageId, String result, Constant.REQUESTS request) {

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
