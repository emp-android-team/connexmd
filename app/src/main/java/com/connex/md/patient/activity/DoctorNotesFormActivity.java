package com.connex.md.patient.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.connex.md.R;
import com.connex.md.activity.TermsConditionActivity;
import com.connex.md.interfaces.AsyncTaskListner;
import com.connex.md.others.App;
import com.connex.md.ws.CallRequest;
import com.connex.md.ws.Constant;
import com.connex.md.ws.MyConstants;
import com.connex.md.ws.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class DoctorNotesFormActivity extends AppCompatActivity implements AsyncTaskListner {

    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    EditText etFullName, etPhoneNumber, etLastName, etAge, etEmail, etMessage;
    SharedPreferences sharedpreferences;
    private CheckBox cbTerms;
    private TextView tvTerms;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_doctor_notes_form);

        ButterKnife.bind(this);

        setupToolbar();

        sharedpreferences = getSharedPreferences(MyConstants.PREF, MODE_PRIVATE);

        etFullName = findViewById(R.id.etFullName);
        //etLastName = findViewById(R.id.etLastName);
        etPhoneNumber = findViewById(R.id.etPhoneNumber);
        etAge = findViewById(R.id.etAge);
        etEmail = findViewById(R.id.etEmail);
        etMessage = findViewById(R.id.etMessage);
        cbTerms = findViewById(R.id.cbTerms);
        tvTerms = findViewById(R.id.tvTerms);

        tvTerms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(DoctorNotesFormActivity.this, TermsConditionActivity.class);
                startActivity(intent);
            }
        });

        etPhoneNumber.setFocusable(false);
        etPhoneNumber.setCursorVisible(false);
        etPhoneNumber.setInputType(InputType.TYPE_NULL);
        etPhoneNumber.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                etPhoneNumber.requestFocus();

                Intent intent = new Intent(DoctorNotesFormActivity.this, PhoneAuthActivity.class);
                intent.putExtra("profileUpdate", "1");
                startActivityForResult(intent, 11);
            }
        });

        if (sharedpreferences.getBoolean(MyConstants.IS_LOGGED_IN, false)) {
            if (App.user.getUser_Type().equalsIgnoreCase(MyConstants.USER_PT) && MyConstants.isGuest.equalsIgnoreCase("0")) {
                etEmail.setText(App.user.getUserEmail());
                etEmail.setEnabled(false);
                etEmail.setCursorVisible(false);
                etEmail.setFocusable(false);
            }
        }
    }


    private void setupToolbar() {
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle("Details");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }


    @OnClick(R.id.btnSubmit)
    void onSubmitClick() {
        if (!cbTerms.isChecked()){
            Utils.showAlert("Please check terms and condition before you submit your details", DoctorNotesFormActivity.this);
            return;
        }

        String fullName = etFullName.getText().toString().trim();
        String phoneNumber = etPhoneNumber.getText().toString().trim();
        String age = etAge.getText().toString().trim();
        String email = etEmail.getText().toString().trim();
        String message = etMessage.getText().toString().trim();

        if (TextUtils.isEmpty(fullName)) {
            etFullName.requestFocus();
            etFullName.setError("First name can't be empty");
        } else if (TextUtils.isEmpty(age)) {
            etAge.requestFocus();
            etAge.setError("Age can't be empty");
        } else if (TextUtils.isEmpty(email)) {
            etEmail.requestFocus();
            etEmail.setError("Email can't be empty");
        } else if (TextUtils.isEmpty(message)) {
            etMessage.requestFocus();
            etMessage.setError("Message can't be empty");
        } else if (message.length() < 100) {
            etMessage.requestFocus();
            etMessage.setError("Required minimum 100 characters");
        } else {
            System.out.println("header price::" + MyConstants.headerPrice);
            if (TextUtils.isEmpty(phoneNumber) || phoneNumber == null) {
                phoneNumber = "";
            }
                    /*Intent intent = new Intent(DoctorNotesFormActivity.this, PaymentActivity.class);
                    intent.putExtra("isOfferUsed", false);
                    intent.putExtra("finalAmount", Integer.parseInt(MyConstants.headerPrice));
                    intent.putExtra("walletCharge", "0");
                    intent.putExtra("isHeader", true);
                    intent.putExtra("fullName", fullName);
                    intent.putExtra("phoneNumber", phoneNumber);
                    intent.putExtra("age", age);
                    intent.putExtra("email", email);
                    intent.putExtra("message", message);
                    startActivity(intent);*/

            Map<String, String> map = new HashMap<String, String>();
            map.put("url", MyConstants.BASE_URL + "submitNoteForm");
            map.put("Version", MyConstants.WS_VERSION);
            map.put("DeviceType", MyConstants.DEVICE_TYPE);
            map.put("DeviceToken", MyConstants.DEVICE_ID);
            map.put("Name", fullName);
            map.put("Phone", phoneNumber);
            map.put("Age", age);
            map.put("Email", email);
            map.put("Brief", message);

            new CallRequest(DoctorNotesFormActivity.this).createBooking(map);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        try {
            if (requestCode == 11) {
                if (data != null) {
                    String phone_number = data.getStringExtra("phone_number");
                    System.out.println("phone_number" + phone_number);
                    etPhoneNumber.setText(phone_number);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        Utils.removeSimpleSpinProgressDialog();
        try {
            if (result != null && !result.isEmpty()) {
                Log.i("TAG", "TAG Result : " + result);
                //      {"document":{"response":{"status":1,"message":"Success."}}}
                switch (request) {
                    case createBooking:
                        try {
                            JSONObject mainObj = new JSONObject(result);
                            if (mainObj.getString("error_code").equalsIgnoreCase("0")) {
                                Utils.showToast(mainObj.getString("error_string"), DoctorNotesFormActivity.this);

                                JSONObject resultObj = mainObj.getJSONObject("result");
                                MyConstants.consultId = resultObj.getString("DoctorNoteId");

                                if (sharedpreferences.getBoolean(MyConstants.IS_LOGGED_IN, false)) {

                                    MyConstants.isBooked = false;
                                    Intent intent = new Intent(DoctorNotesFormActivity.this, PaymentActivity.class);
                                    intent.putExtra("isOfferUsed", false);
                                    intent.putExtra("finalAmount", Integer.parseInt(MyConstants.headerPrice));
                                    intent.putExtra("walletCharge", "0");
                                    intent.putExtra("isHeader", true);
                                    startActivity(intent);

                                } else {

                                    JSONObject User = resultObj.getJSONObject("User");
                                    setLoginData(User);
                                }

                            } else {
                                Utils.showToast(mainObj.getString("error_string"), DoctorNotesFormActivity.this);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onProgressUpdate(String uniqueMessageId, int progres) {

    }

    @Override
    public void onProgressComplete(String uniqueMessageId, String result, Constant.REQUESTS request) {

    }

    private void setLoginData(JSONObject obj) {
        try {
            JSONArray userArray = obj.getJSONArray("user");
            JSONObject user_Object = userArray.getJSONObject(0);
            String user_id = user_Object.getString("id");
            String email = user_Object.getString("email");
            String ApiToken = user_Object.getString("ApiToken");
            String Latitude = user_Object.getString("Latitude");
            String Longitude = user_Object.getString("Longitude");
            String ReferCode = user_Object.getString("ReferCode");

            JSONArray user_detailArray = obj.getJSONArray("user_detail");
            JSONObject user_detail_object = user_detailArray.getJSONObject(0);
            String FirstName = user_detail_object.getString("FirstName");
            String LastName = user_detail_object.getString("LastName");
            String Gender = user_detail_object.getString("Gender");
            String CountryId = user_detail_object.getString("CountryId");
            String StateId = user_detail_object.getString("StateId");
            String CountryCode = user_detail_object.getString("CountryCode");
            String Country = user_detail_object.getString("Country");
            String State = user_detail_object.getString("State");
            String Phone = user_detail_object.getString("Phone");
            String BirthDate = user_detail_object.getString("BirthDate");
            String Age = user_detail_object.getString("Age");
            String Address = user_detail_object.getString("Address");
            String Timezone = user_detail_object.getString("Timezone");
            String ProfilePic = user_detail_object.getString("ProfilePic");
            //String ProfilePicSocial = user_detail_object.getString("ProfilePicSocial");
            String CoverPic = user_detail_object.getString("CoverPic");
            String CurrentDiscount = user_detail_object.getString("CurrentDiscount");
            String WalletBalance = user_detail_object.getString("WalletBalance");


            JSONArray user_device = obj.getJSONArray("user_device");

            String name = FirstName + " " + LastName;

            SharedPreferences sharedpreferences = getSharedPreferences(MyConstants.PREF, Context.MODE_PRIVATE);

            SharedPreferences.Editor editor = sharedpreferences.edit();

            editor.putString(MyConstants.USER_TYPE, MyConstants.USER_PT);
            editor.putString(MyConstants.USER_EMAIL, email);
            editor.putString(MyConstants.USER_ID, user_id);
            //if (!TextUtils.isEmpty(ProfilePic)) {
            editor.putString(MyConstants.PROFILE_PIC, ProfilePic);
            //} else {
            //    editor.putString(MyConstants.PROFILE_PIC, ProfilePicSocial);
            //}
            editor.putString(MyConstants.PT_ZONE, Timezone);
            editor.putString(MyConstants.PT_NAME, name);
            editor.putString(MyConstants.PT_FIRST_NAME, FirstName);
            editor.putString(MyConstants.PT_LAST_NAME, LastName);
            editor.putString(MyConstants.GENDER, Gender);
            editor.putString(MyConstants.DATE_OF_BIRTH, BirthDate);
            editor.putString(MyConstants.MOBILE, Phone);
            editor.putString(MyConstants.COUNTRY, Country);
            editor.putString(MyConstants.ADDRESS, Address);
            editor.putString(MyConstants.AGE, Age);
            editor.putString(MyConstants.CURRENT_DISCOUNT, CurrentDiscount);
            editor.putString(MyConstants.WALLET_BALANCE, WalletBalance);
            editor.putString(MyConstants.REFER_CODE, ReferCode);
            editor.putString(MyConstants.TOKEN, ApiToken);
            editor.putString(MyConstants.SINCH_ID, "pppp" + user_id);
            editor.putBoolean(MyConstants.FIRST_LOGIN, true);
            editor.putBoolean(MyConstants.IS_LOGGED_IN, true);
            editor.putBoolean(MyConstants.IS_GUEST, false);
            editor.putString(MyConstants.LOGIN_TYPE, "0");
            editor.putString(MyConstants.SOCIAL_ID, "0");
            editor.putString(MyConstants.PT_PASSWORD, "");

            editor.commit();

            App.user.setUserID(user_id);
            App.user.setName(name);
            App.user.setFirstName(FirstName);
            App.user.setLastName(LastName);
            App.user.setUserEmail(email);
            App.user.setPhone(Phone);
            App.user.setUser_Type(MyConstants.USER_PT);
            App.user.setProfileUrl(sharedpreferences.getString(MyConstants.PROFILE_PIC, ""));
            App.user.setSinch_id(sharedpreferences.getString(MyConstants.SINCH_ID, ""));
            App.user.setLoginType("0");
            MyConstants.API_TOKEN = ApiToken;
            MyConstants.isGuest = "0";
            System.out.println("token::" + MyConstants.API_TOKEN);

            Log.i(" SINCH USER ID :", App.user.getSinch_id());
            //getSinchServiceInterface().startClient(App.user.getSinch_id());

            Intent intent = new Intent(DoctorNotesFormActivity.this, PaymentActivity.class);
            intent.putExtra("isOfferUsed", false);
            intent.putExtra("finalAmount", Integer.parseInt(MyConstants.headerPrice));
            intent.putExtra("walletCharge", "0");
            intent.putExtra("isHeader", true);
            startActivity(intent);

            System.out.println("data saved successfully");

        } catch (JSONException e) {
            e.printStackTrace();
            Utils.showToast("Something went wrong", DoctorNotesFormActivity.this);
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
