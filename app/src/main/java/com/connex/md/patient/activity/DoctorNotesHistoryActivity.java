package com.connex.md.patient.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.connex.md.R;
import com.connex.md.interfaces.AsyncTaskListner;
import com.connex.md.model.BookingHistory;
import com.connex.md.others.App;
import com.connex.md.others.Internet;
import com.connex.md.patient.adapter.DoctorNotesHistoryAdapter;
import com.connex.md.utils.DividerItemDecoration;
import com.connex.md.ws.CallRequest;
import com.connex.md.ws.Constant;
import com.connex.md.ws.MyConstants;
import com.connex.md.ws.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DoctorNotesHistoryActivity extends AppCompatActivity implements AsyncTaskListner {

    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    RecyclerView rvBookingHistory;
    ImageView ivNoBookings;
    DoctorNotesHistoryAdapter bookingHistoryAdapter;
    List<BookingHistory> bookingHistoryList = new ArrayList<>();
    SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
    SimpleDateFormat format = new SimpleDateFormat("dd MMM yyyy", Locale.US);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_doctor_notes_history);

        ButterKnife.bind(this);
        setupToolbar();

        rvBookingHistory = findViewById(R.id.rvBookingHistory);
        ivNoBookings = findViewById(R.id.iv_no_bookings);

        getDoctorNotesHistory();
    }


    private void setupToolbar() {
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle("Doctor Notes");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }


    private void getDoctorNotesHistory() {
        if (!Internet.isAvailable(DoctorNotesHistoryActivity.this)) {
            Internet.showAlertDialog(DoctorNotesHistoryActivity.this, "Error!", "No Internet Connection", false);

            return;
        }

        String urlStr = "userNotesList";

        Map<String, String> map = new HashMap<String, String>();
        map.put("url", MyConstants.BASE_URL + urlStr);
        map.put("UserId", App.user.getUserID());
        map.put("ApiToken", MyConstants.API_TOKEN);
        map.put("Version", MyConstants.WS_VERSION);

        new CallRequest(DoctorNotesHistoryActivity.this).doctorNotesHistory(map);
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        Utils.removeSimpleSpinProgressDialog();
        try {
            if (result != null && !result.isEmpty()) {
                Log.i("TAG", "TAG Result : " + result);
                //      {"document":{"response":{"status":1,"message":"Success."}}}
                switch (request) {
                    case doctorNoteHistory:
                        Utils.removeSimpleSpinProgressDialog();
                        try {
                            JSONObject mainObj = new JSONObject(result);
                            if (mainObj.getString("error_code").equalsIgnoreCase("0")) {
                                bookingHistoryList.clear();
                                //MyConstants.Wallet_Balance = mainObj.getString("WalletBalance");
                                if (mainObj.getJSONArray("result") != null && mainObj.getJSONArray("result").length() > 0) {
                                    JSONArray jsonArray = mainObj.getJSONArray("result");
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject jsonObject = jsonArray.getJSONObject(i);



                                        String ConsultTime = jsonObject.getString("created_at");

                                        Date date = null;
                                        String strDate = null;
                                        try {
                                            date = inputFormat.parse(ConsultTime);
                                            strDate = format.format(date);
                                        } catch (ParseException e) {
                                            e.printStackTrace();
                                        }
                                        //String date = format.format(Date.parse(ConsultTime));

                                        String dateFull[] = strDate.split(" ", 2);

                                        BookingHistory bookingHistory = new BookingHistory();
                                        //bookingHistory.setDoctorId(jsonObject.getString("DoctorId"));
                                        bookingHistory.setFirstName(jsonObject.getString("FirstName"));
                                        bookingHistory.setLastName(jsonObject.getString("LastName"));
                                        //bookingHistory.setProfilePic(DoctorDetail.getString("ProfilePic"));
                                        //bookingHistory.setDoctorCharge(jsonObject.getString("DoctorCharge"));
                                        bookingHistory.setStatus(jsonObject.getString("PaymentType"));
                                        bookingHistory.setConsultId(jsonObject.getString("DoctorNoteId"));
                                        bookingHistory.setFinalAmount(jsonObject.getString("Payment"));
                                        //bookingHistory.setResponseTime(DoctorDetail.getString("ResponseTime"));
                                        //bookingHistory.setSpeciality(DoctorDetail.getString("Speciality"));
                                        bookingHistory.setConsultTime(strDate);
                                        bookingHistory.setDate(dateFull[0]);
                                        bookingHistory.setMonth(dateFull[1]);

                                        bookingHistoryList.add(bookingHistory);
                                    }
                                    LinearLayoutManager layoutManager = new LinearLayoutManager(DoctorNotesHistoryActivity.this);
                                    layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
                                    rvBookingHistory.setLayoutManager(layoutManager);

                                    LayoutAnimationController controller = AnimationUtils.loadLayoutAnimation(rvBookingHistory.getContext(), R.anim.layout_animation_fall_down);
                                    rvBookingHistory.setLayoutAnimation(controller);
                                    rvBookingHistory.scheduleLayoutAnimation();

                                    bookingHistoryAdapter = new DoctorNotesHistoryAdapter(DoctorNotesHistoryActivity.this, bookingHistoryList);
                                    rvBookingHistory.setHasFixedSize(true);
                                    //rvBookingHistory.addItemDecoration(new VerticalSpacingDecoration(20));
                                    rvBookingHistory.addItemDecoration(new DividerItemDecoration(DoctorNotesHistoryActivity.this, R.drawable.divider));
                                    rvBookingHistory.setItemViewCacheSize(100);
                                    rvBookingHistory.setDrawingCacheEnabled(true);
                                    rvBookingHistory.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);

                                    rvBookingHistory.setAdapter(bookingHistoryAdapter);

                                    if (bookingHistoryList.size() > 0) {
                                        ivNoBookings.setVisibility(View.GONE);
                                        rvBookingHistory.setVisibility(View.VISIBLE);
                                    } else {
                                        ivNoBookings.setVisibility(View.VISIBLE);
                                        rvBookingHistory.setVisibility(View.GONE);
                                    }

                                } else {
                                    Utils.showToast(mainObj.getString("error_string"), DoctorNotesHistoryActivity.this);
                                }
                            } else if (mainObj.getString("error_code").equalsIgnoreCase("7")) {
                                if (bookingHistoryList.size() > 0) {
                                    ivNoBookings.setVisibility(View.GONE);
                                    rvBookingHistory.setVisibility(View.VISIBLE);
                                } else {
                                    ivNoBookings.setVisibility(View.VISIBLE);
                                    rvBookingHistory.setVisibility(View.GONE);
                                }
                            } else {
                                Utils.showToast(mainObj.getString("error_string"), DoctorNotesHistoryActivity.this);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;
                }
            }
        } catch (Exception e) {
            Utils.removeSimpleSpinProgressDialog();
            e.printStackTrace();
            Utils.showToast("Please try again later", DoctorNotesHistoryActivity.this);
        }
    }

    @Override
    public void onProgressUpdate(String uniqueMessageId, int progres) {

    }

    @Override
    public void onProgressComplete(String uniqueMessageId, String result, Constant.REQUESTS request) {

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
