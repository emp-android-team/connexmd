package com.connex.md.patient.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.TextView;

import com.alimuzaffar.lib.pin.PinEntryEditText;
import com.connex.md.R;
import com.connex.md.audio_video_calling.SinchService;
import com.connex.md.firebase_chat.activity.NurseFireChatActivity;
import com.connex.md.interfaces.AsyncTaskListner;
import com.connex.md.others.App;
import com.connex.md.ws.CallRequest;
import com.connex.md.ws.Constant;
import com.connex.md.ws.MyConstants;
import com.connex.md.ws.Utils;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.FirebaseTooManyRequestsException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.sinch.android.rtc.PushTokenRegistrationCallback;
import com.sinch.android.rtc.SinchError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import static com.connex.md.audio_video_calling.BaseActivity.getSinchServiceInterface;

public class GuestVerifyOTPActivity extends AppCompatActivity implements AsyncTaskListner, SinchService.StartFailedListener, PushTokenRegistrationCallback {

    private static final String TAG = "PhoneAuthActivity";
    public DatabaseReference fireDB;
    PinEntryEditText pinView;
    Button btnSendAgain, btnVerifyOTP;
    String phone, number, firstName, lastName;
    String mVerificationId;
    SharedPreferences sharedpreferences;
    private PhoneAuthProvider.ForceResendingToken mResendToken;
    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks;
    private FirebaseAuth mAuth;
    private String profileUpdate = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_guest_verify_otp);

        pinView = findViewById(R.id.pinview);
        btnSendAgain = findViewById(R.id.btnSendAgain);
        btnVerifyOTP = findViewById(R.id.btnResetPassword);

        sharedpreferences = getSharedPreferences(MyConstants.PREF, Context.MODE_PRIVATE);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            phone = bundle.getString("phone");
            number = bundle.getString("number");
            firstName = bundle.getString("first_name");
            lastName = bundle.getString("last_name");
            mVerificationId = bundle.getString("verification_id");
            mResendToken = (PhoneAuthProvider.ForceResendingToken) bundle.get("resend_token");
            if (bundle.containsKey("profileUpdate")) {
                profileUpdate = bundle.getString("profileUpdate");
            }
        }

        mAuth = FirebaseAuth.getInstance();
        mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
            @Override
            public void onVerificationCompleted(PhoneAuthCredential credential) {
                Log.d(TAG, "onVerificationCompleted:" + credential);
                //signInWithPhoneAuthCredential(credential, true);
            }

            @Override
            public void onVerificationFailed(FirebaseException e) {
                Log.w(TAG, "onVerificationFailed", e);
                if (e instanceof FirebaseAuthInvalidCredentialsException) {
                    //mPhoneNumberField.setError("Invalid phone number.");
                } else if (e instanceof FirebaseTooManyRequestsException) {
                    Snackbar.make(findViewById(android.R.id.content), "Quota exceeded.",
                            Snackbar.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onCodeSent(String verificationId,
                                   PhoneAuthProvider.ForceResendingToken token) {
                Log.d(TAG, "onCodeSent:" + verificationId);
                Utils.showToast("Verification code sent successfully", GuestVerifyOTPActivity.this);
                mVerificationId = verificationId;
                mResendToken = token;
            }
        };

        pinView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    Utils.hideKeyboard(GuestVerifyOTPActivity.this, pinView);
                    return true;
                }
                return false;
            }
        });

        btnSendAgain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pinView.setText("");
                resendVerificationCode(phone, mResendToken);
            }
        });

        btnVerifyOTP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String code = pinView.getText().toString();
                if (TextUtils.isEmpty(code)) {
                    pinView.setError("Cannot be empty.");
                    return;
                } else if (pinView.getText().toString().length() != 6) {
                    pinView.setError("Enter 6 digit password");
                    pinView.requestFocus();
                    return;
                }

                verifyPhoneNumberWithCode(mVerificationId, code);
            }
        });
    }

    private void signInWithPhoneAuthCredential(PhoneAuthCredential credential, final boolean isVerified) {
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            Log.d(TAG, "signInWithCredential:success");
                            FirebaseUser user = task.getResult().getUser();
                            Utils.removeSimpleSpinProgressDialog();
                            if (isVerified) {
                                guestLogin();
                            } else {
                                Intent intent = new Intent();
                                intent.putExtra("phone_number", phone);
                                setResult(11, intent);
                                finish();
                            }
                            /*startActivity(new Intent(PhoneAuthActivity.this, MentionQueryActivity.class));
                            finish();*/
                        } else {
                            Utils.removeSimpleSpinProgressDialog();
                            Log.w(TAG, "signInWithCredential:failure", task.getException());
                            if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                                pinView.requestFocus();
                                pinView.setError("Invalid code.");
                            }
                        }
                    }
                });
    }


    private void verifyPhoneNumberWithCode(String verificationId, String code) {
        Utils.showSimpleSpinProgressDialog(GuestVerifyOTPActivity.this, "Please Wait");
        PhoneAuthCredential credential = PhoneAuthProvider.getCredential(verificationId, code);
        if (profileUpdate.equalsIgnoreCase("1")) {
            signInWithPhoneAuthCredential(credential, false);
        } else {
            signInWithPhoneAuthCredential(credential, true);
        }
    }

    private void resendVerificationCode(String phoneNumber,
                                        PhoneAuthProvider.ForceResendingToken token) {
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                phoneNumber,        // Phone number to verify
                60,                 // Timeout duration
                TimeUnit.SECONDS,   // Unit of timeout
                this,               // Activity (for callback binding)
                mCallbacks,         // OnVerificationStateChangedCallbacks
                token);             // ForceResendingToken from callbacks
    }

    private void guestLogin() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", MyConstants.GUEST_BASE_URL + "login");
        map.put("Phone", phone);
        map.put("Version", MyConstants.WS_VERSION);
        map.put("DeviceType", MyConstants.DEVICE_TYPE);
        map.put("DeviceToken", MyConstants.DEVICE_ID);
        map.put("FirstName", firstName);
        map.put("LastName", lastName);

        new CallRequest(GuestVerifyOTPActivity.this).guestLogin(map);
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {

        try {
            if (result != null && !result.isEmpty()) {
                Log.i("TAG", "TAG Result : " + result);
                //      {"document":{"response":{"status":1,"message":"Success."}}}
                switch (request) {
                    case guestLogin:
                        Utils.removeSimpleSpinProgressDialog();
                        try {
                            JSONObject mainObj = new JSONObject(result);
                            if (mainObj.getString("error_code").equalsIgnoreCase("0")) {
                                //Utils.showToast(mainObj.getString("error_string"), GuestVerifyOTPActivity.this);

                                JSONObject resultObj = mainObj.getJSONObject("result");

                                JSONArray guest = resultObj.getJSONArray("guest");

                                JSONObject guestObj = guest.getJSONObject(0);

                                setUserData(guestObj);


                            } else {
                                Utils.showToast(mainObj.getString("error_string"), GuestVerifyOTPActivity.this);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;

                    case nurseChatDetails:
                        try {
                            JSONObject mainObj = new JSONObject(result);
                            if (mainObj.getString("error_code").equalsIgnoreCase("0")) {
                                //Utils.showToast(mainObj.getString("error_string"), GuestVerifyOTPActivity.this);

                                isChildAvailable(mainObj.getJSONArray("result").getJSONObject(0));

                            } else {
                                Utils.showToast(mainObj.getString("error_string"), GuestVerifyOTPActivity.this);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void isChildAvailable(JSONObject resultObj) {

        try {
            final String id = resultObj.getString("id");
            final String UniqueId = resultObj.getString("UniqueId");
            final String UserId = resultObj.getString("UserId");
            final String NurseId = resultObj.getString("NurseId");
            final String Status = resultObj.getString("Status");
            final String IsGuest = resultObj.getString("IsGuest");
            final String FirstName = resultObj.getString("FirstName");
            final String LastName = resultObj.getString("LastName");
            final String ProfilePic = resultObj.getString("ProfilePic");

            fireDB = FirebaseDatabase.getInstance().getReference().child(MyConstants.NURSE_CHANNEL);

            fireDB.orderByChild(UniqueId).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    Log.i("Data snapshot Nursechat", dataSnapshot.toString());
                    Utils.removeSimpleSpinProgressDialog();
                    Intent intent = new Intent(GuestVerifyOTPActivity.this, NurseFireChatActivity.class);


                    if (dataSnapshot.child(UniqueId).getValue() == null) {
                        intent.putExtra(MyConstants.IS_PAYMENT_DONE, true);
                    } else {
                        intent.putExtra(MyConstants.IS_PAYMENT_DONE, false);
                    }

                    intent.putExtra(MyConstants.PT_ID, UserId);
                    intent.putExtra(MyConstants.DR_ID, NurseId);
                    intent.putExtra(MyConstants.UNIQUE_CHAT_ID, UniqueId);
                    intent.putExtra("status", Status);
                    intent.putExtra(MyConstants.DR_NAME, FirstName + " " + LastName);
                    intent.putExtra(MyConstants.RECEIVER_IMAGE_URL, ProfilePic);
                    startActivity(intent);
                    finish();
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onProgressUpdate(String uniqueMessageId, int progres) {

    }

    @Override
    public void onProgressComplete(String uniqueMessageId, String result, Constant.REQUESTS request) {

    }

    private void setUserData(JSONObject guestObj) {
        try {

            String guest_id = guestObj.getString("id");
            String name = guestObj.getString("FirstName") + " " + guestObj.getString("LastName");

            SharedPreferences sharedpreferences = getSharedPreferences(MyConstants.PREF, Context.MODE_PRIVATE);

            SharedPreferences.Editor editor = sharedpreferences.edit();

            editor.putString(MyConstants.USER_TYPE, MyConstants.USER_PT);

            editor.putString(MyConstants.USER_EMAIL, "");
            editor.putString(MyConstants.USER_ID, guest_id);
            //if (!TextUtils.isEmpty(ProfilePic)) {
            editor.putString(MyConstants.PROFILE_PIC, "");
            //} else {
            //    editor.putString(MyConstants.PROFILE_PIC, ProfilePicSocial);
            //}
            editor.putString(MyConstants.PT_ZONE, "");
            editor.putString(MyConstants.PT_NAME, name);
            editor.putString(MyConstants.PT_FIRST_NAME, guestObj.getString("FirstName"));
            editor.putString(MyConstants.PT_LAST_NAME, guestObj.getString("LastName"));
            editor.putString(MyConstants.GENDER, "");
            editor.putString(MyConstants.DATE_OF_BIRTH, "");
            editor.putString(MyConstants.MOBILE, guestObj.getString("Phone"));
            editor.putString(MyConstants.COUNTRY, "");
            editor.putString(MyConstants.ADDRESS, "");
            editor.putString(MyConstants.AGE, "");
            editor.putString(MyConstants.CURRENT_DISCOUNT, "");
            editor.putString(MyConstants.WALLET_BALANCE, "");
            editor.putString(MyConstants.REFER_CODE, "");
            editor.putString(MyConstants.TOKEN, guestObj.getString("ApiToken"));
            editor.putString(MyConstants.SINCH_ID, "gggg" + guest_id);
            editor.putString(MyConstants.STRIPE_ID, guestObj.getString("StripeId"));
            //editor.putBoolean(MyConstants.FIRST_LOGIN, true);
            editor.putBoolean(MyConstants.IS_LOGGED_IN, true);
            editor.putBoolean(MyConstants.IS_GUEST, true);
            editor.putString(MyConstants.LOGIN_TYPE, "0");
            editor.putString(MyConstants.SOCIAL_ID, "0");

            editor.commit();


            App.user.setUserID(guestObj.getString("id"));
            App.user.setName(name);
            App.user.setFirstName(guestObj.getString("FirstName"));
            App.user.setLastName(guestObj.getString("LastName"));
            App.user.setUserEmail("");
            App.user.setPhone(guestObj.getString("Phone"));
            App.user.setUser_Type(MyConstants.USER_PT);
            App.user.setProfileUrl("");
            App.user.setSinch_id("g" + guest_id);
            App.user.setLoginType("0");
            MyConstants.API_TOKEN = guestObj.getString("ApiToken");
            MyConstants.isGuest = "1";
            System.out.println("token::" + MyConstants.API_TOKEN);
            System.out.println("sinch id==" + App.user.getSinch_id());

            try {
                getSinchServiceInterface().startClient(App.user.getSinch_id());
                getSinchServiceInterface().registerPushToken(this);
            } catch (Exception e) {
                e.printStackTrace();
            }

            // reload all data
            MyConstants.specialitiesList.clear();
            MyConstants.isLoadAgain = true;
            MyConstants.bookingHistoryList.clear();
            MyConstants.messageHistoryList.clear();
            MyConstants.userChatList.clear();
            MyConstants.doctorChatList.clear();
            MyConstants.isBookingHistoryLoad = true;
            MyConstants.isMessageHistoryLoad = true;
            MyConstants.userProfile = null;
            MyConstants.isProfileLoad = true;
            MyConstants.doctorProfilePersonal = null;
            MyConstants.isDoctorProfileLoad = true;

            /*PTDashboardActivity dashboardActivity = new PTDashboardActivity();
            dashboardActivity.setUserName();*/

            if (MyConstants.CHAT_AS_GUEST) {

                //getNurseChatDetails();

                Intent intent = new Intent(GuestVerifyOTPActivity.this, PTDashboardActivity.class);
                startActivity(intent);
                ActivityCompat.finishAffinity(GuestVerifyOTPActivity.this);

               /* Intent intent = new Intent(GuestVerifyOTPActivity.this, NurseFireChatActivity.class);
                startActivity(intent);
                finish();*/
            } else {
                if (MyConstants.doctorProfile.getDoctorId().equalsIgnoreCase(MyConstants.doctorIdForQuestionnaire)) {
                    Intent intent = new Intent(GuestVerifyOTPActivity.this, DRQuestionnaire1Activity.class);
                    startActivity(intent);
                    finish();
                } else {
                    Intent intent = new Intent(GuestVerifyOTPActivity.this, ConfirmEConsultBookingActivity.class);
                    startActivity(intent);
                    finish();
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void getNurseChatDetails() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", MyConstants.GUEST_BASE_URL + "nurseChat");
        map.put("ApiToken", MyConstants.API_TOKEN);
        map.put("Version", MyConstants.WS_VERSION);
        map.put("GuestId", App.user.getUserID());
        map.put("SocialType", App.user.getLoginType());
        map.put("SocialId", sharedpreferences.getString(MyConstants.SOCIAL_ID, ""));

        new CallRequest(GuestVerifyOTPActivity.this).getNurseChatDetails(map);
    }

    @Override
    public void onStartFailed(SinchError error) {

    }

    @Override
    public void onStarted() {

    }

    @Override
    public void tokenRegistered() {

    }

    @Override
    public void tokenRegistrationFailed(SinchError sinchError) {

    }
}
