package com.connex.md.patient.activity;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;

import com.connex.md.R;
import com.connex.md.interfaces.AsyncTaskListner;
import com.connex.md.model.InpersonAppointmentsData;
import com.connex.md.model.TimeSlotsData;
import com.connex.md.others.App;
import com.connex.md.others.Internet;
import com.connex.md.patient.adapter.InPersonAppointmentAdapter;
import com.connex.md.ws.CallRequest;
import com.connex.md.ws.Constant;
import com.connex.md.ws.MyConstants;
import com.connex.md.ws.Utils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

public class InPersonAppointmentsActivity extends AppCompatActivity implements AsyncTaskListner {

    @BindView(R.id.rec_inperson_visit)
    RecyclerView mInpersonVisitRec;

    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    private SharedPreferences mSharedPreferences;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_appointments_inperson_visit);

        ButterKnife.bind(this);
        setupToolbar();

        mSharedPreferences = getSharedPreferences(MyConstants.PREF, Context.MODE_PRIVATE);

        getInPersonAppointments();
    }


    private void setupToolbar() {
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle("My in-Person Appointments");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }


    private void setInPersonAppointmentAdapter(List<InpersonAppointmentsData> dataList) {
        InPersonAppointmentAdapter adapter = new InPersonAppointmentAdapter(this, dataList);
        mInpersonVisitRec.setLayoutManager(new LinearLayoutManager(this));
        mInpersonVisitRec.setAdapter(adapter);
    }


    private void getInPersonAppointments() {
        if (!Internet.isAvailable(this)) {
            Internet.showAlertDialog(this, "Error!", "No Internet Connection", false);
            return;
        }

        Map<String, String> map = new HashMap<String, String>();
        map.put("url", MyConstants.BASE_URL + "inPersonVisitHistory");
        map.put("ApiToken", MyConstants.API_TOKEN);
        map.put("UserId", App.user.getUserID());
        map.put("Version", MyConstants.WS_VERSION);

        new CallRequest(InPersonAppointmentsActivity.this).inPersonVisitHistory(map);
    }



    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        Utils.removeSimpleSpinProgressDialog();

        try {
            if (result != null && !result.isEmpty()) {
                JSONObject jsonObject = new JSONObject(result);
                Gson gson = new Gson();

                Log.i("TAG", "TAG Result : " + result);

                switch (request) {
                    case inPersonVisitHistory:
                        List<InpersonAppointmentsData> inpersonAppointmentsData = (List<InpersonAppointmentsData>) gson.fromJson(jsonObject.get("result").toString(), new TypeToken<List<InpersonAppointmentsData>>(){}.getType());

                        setInPersonAppointmentAdapter(inpersonAppointmentsData);
                        break;
                }
            }
        }catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onProgressUpdate(String uniqueMessageId, int progres) {

    }

    @Override
    public void onProgressComplete(String uniqueMessageId, String result, Constant.REQUESTS request) {

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
