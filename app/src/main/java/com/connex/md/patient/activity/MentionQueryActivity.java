package com.connex.md.patient.activity;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.connex.md.R;
import com.connex.md.custom_views.CenteredToolbar;
import com.connex.md.custom_views.CircleImageView;
import com.connex.md.interfaces.AsyncTaskListner;
import com.connex.md.model.DoctorProfile;
import com.connex.md.others.App;
import com.connex.md.others.Internet;
import com.connex.md.patient.adapter.MentionQueryAdapter;
import com.connex.md.utils.VerticalSpacingDecoration;
import com.connex.md.ws.CallRequest;
import com.connex.md.ws.Constant;
import com.connex.md.ws.MyConstants;
import com.connex.md.ws.Utils;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MentionQueryActivity extends AppCompatActivity implements AsyncTaskListner {

    @BindView(R.id.toolbar)
    CenteredToolbar mToolbar;

    @BindView(R.id.img_profile)
    CircleImageView mProfileImg;

    TextView tvDoctorName, tvSpeciality;
    EditText etProblem;
    Button btnBookAppointment;
    RecyclerView rvImages;
    List<String> imagePath = new ArrayList<>();
    LinearLayoutManager linearLayoutManager;
    MentionQueryAdapter mentionQueryAdapter;
    private static int RESULT_LOAD_IMG = 1;
    DoctorProfile doctorProfile;
    SharedPreferences sharedpreferences;
    String filePathDp = "";
    String filePathFirst = "";
    private static int RESULT_CROP_DP = 3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mention_query);

        ButterKnife.bind(this);

        setupToolbar();

        tvDoctorName = findViewById(R.id.tvDoctorName);
        tvSpeciality = findViewById(R.id.tvSpeciality);
        etProblem = findViewById(R.id.etProblem);
        btnBookAppointment = findViewById(R.id.btnBookAppointment);
        rvImages = findViewById(R.id.rvImages);

        sharedpreferences = getSharedPreferences(MyConstants.PREF, MODE_PRIVATE);

        Bundle b = getIntent().getExtras();
        if (b != null) {
            doctorProfile = (DoctorProfile) b.getSerializable("doctor");
        }

        doctorProfile = MyConstants.doctorProfile;


        tvDoctorName.setText(doctorProfile.getFirstName() + " " + doctorProfile.getLastName());
        tvSpeciality.setText(doctorProfile.getSpeciality());

        Picasso.with(MentionQueryActivity.this)
                .load(doctorProfile.getProfilePic())
                .placeholder(R.drawable.avatar)
                .error(R.drawable.avatar)
                .into(mProfileImg);

        btnBookAppointment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createConsultAPI();
            }
        });

        imagePath.add("by default");
        linearLayoutManager = new LinearLayoutManager(MentionQueryActivity.this, LinearLayoutManager.HORIZONTAL, false);
        rvImages.setLayoutManager(linearLayoutManager);

        mentionQueryAdapter = new MentionQueryAdapter(MentionQueryActivity.this, imagePath);
        rvImages.setHasFixedSize(true);
        rvImages.addItemDecoration(new VerticalSpacingDecoration(20));
        rvImages.setItemViewCacheSize(20);
        rvImages.setDrawingCacheEnabled(true);
        rvImages.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        rvImages.setAdapter(mentionQueryAdapter);
    }


    private void setupToolbar() {
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle("Book");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }

    private void createConsultAPI() {
        if (!Internet.isAvailable(MentionQueryActivity.this)) {
            Internet.showAlertDialog(MentionQueryActivity.this, "Error!", "No Internet Connection", false);
        } else {
            String problemDescription = etProblem.getText().toString().trim();

            Map<String, String> map = new HashMap<String, String>();
            if (MyConstants.isGuest.equalsIgnoreCase("1")) {
                map.put("url", MyConstants.GUEST_BASE_URL + "guestConsultBooking");
                map.put("GuestId", App.user.getUserID());
            } else {
                map.put("url", MyConstants.BASE_URL + "consultBooking");
                map.put("UserId", App.user.getUserID());
                map.put("SocialType", App.user.getLoginType());
            }
            map.put("ApiToken", MyConstants.API_TOKEN);
            map.put("Version", MyConstants.WS_VERSION);
            map.put("SocialId", sharedpreferences.getString(MyConstants.SOCIAL_ID, "0"));
            map.put("DoctorId", doctorProfile.getDoctorId());
            map.put("DoctorCharge", doctorProfile.getCosultCharge());
            map.put("Description", problemDescription);
            map.put("ConsultTime", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US).format(new Date()));

            System.out.println("image list size:::" + imagePath.size());
            if (imagePath.size() > 1) {
                for (int i = 1; i < imagePath.size(); i++) {
                    map.put("Image" + i, imagePath.get(i));
                }
            }

            new CallRequest(MentionQueryActivity.this).createBooking(map);
        }
    }

    public void loadImageFromGallery() {
        // Create intent to Open Image applications like Gallery, Google Photos
        Intent galleryIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        // Start the Intent
        startActivityForResult(galleryIntent, RESULT_LOAD_IMG);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        try {
            // When an Image is picked
            if (requestCode == RESULT_LOAD_IMG && resultCode == RESULT_OK && null != data) {
                // Get the Image from data

                Uri selectedImage = data.getData();
                String[] filePathColumn = {MediaStore.Images.Media.DATA};

                // Get the cursor
                Cursor cursor = getContentResolver().query(selectedImage,
                        filePathColumn, null, null, null);
                // Move to first row
                int columnIndex = 0;
                if (cursor != null) {
                    cursor.moveToFirst();
                    columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    filePathFirst = cursor.getString(columnIndex);
                    System.out.println(filePathFirst);
                    imagePath.add(filePathFirst);
                    cursor.close();

                    mentionQueryAdapter.notifyDataSetChanged();
                    //drQuestionnaire3Adapter.notifyDataSetChanged();
                }

                //doCropDP(filePathFirst);

            }

            if (requestCode == RESULT_CROP_DP) {
                if (resultCode == Activity.RESULT_OK) {
                    if (!TextUtils.isEmpty(filePathDp)) {

                        Bitmap selectedBitmap = decodeSampledBitmapFromPath(filePathDp,100,100);
                        System.out.println("selectedBitmap:::::" + selectedBitmap);

                        for (int i = 0; i < imagePath.size(); i++) {
                            System.out.println("image list::"+ imagePath.get(i));
                        }

                        // Set The Bitmap Data To ImageView
                        imagePath.add(filePathDp);
                        //ivUser.setImageBitmap(selectedBitmap);
                        //ivPageDp.setScaleType(ImageView.ScaleType.FIT_XY);
                        mentionQueryAdapter.notifyDataSetChanged();
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static Bitmap decodeSampledBitmapFromPath(String path, int reqWidth,
                                                     int reqHeight) {

        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, options);

        options.inSampleSize = calculateInSampleSize(options, reqWidth,
                reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        Bitmap bmp = BitmapFactory.decodeFile(path, options);
        return bmp;
    }

    public static int calculateInSampleSize(BitmapFactory.Options options,int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            if (width > height) {
                inSampleSize = Math.round((float) height / (float) reqHeight);
            } else {
                inSampleSize = Math.round((float) width / (float) reqWidth);
            }
        }
        return inSampleSize;
    }

    private void doCropDP(String picPath) {
        try {

            Intent cropIntent = new Intent("com.android.camera.action.CROP");

            File f = new File(picPath);
            Uri contentUri = Uri.fromFile(f);

            cropIntent.setDataAndType(contentUri, "image/*");

            cropIntent.putExtra("crop", "true");
            // indicate aspect of desired crop
            cropIntent.putExtra("aspectX", 1);
            cropIntent.putExtra("aspectY", 1);
            // indicate output X and Y
            cropIntent.putExtra("outputX", 256);
            cropIntent.putExtra("outputY", 256);
            // retrieve data on return
            cropIntent.putExtra("return-data", false);

            File sdCardDirectory = new File(Environment.getExternalStorageDirectory().getPath() + "/ConnexMd/temp");

            if (!sdCardDirectory.exists()) {
                sdCardDirectory.mkdirs();
            }

            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.US)
                    .format(new Date());

            String nw = "Image_" + timeStamp + ".jpeg";

            File image = new File(sdCardDirectory, nw);

            //uploadPathDp = sdCardDirectory + "/" + nw;
            filePathDp = new File(sdCardDirectory, nw).getAbsolutePath();
            System.out.println("UploadPath:::" + filePathDp);

            try {
                image.createNewFile();
            } catch (IOException ex) {
                Log.e("io", ex.getMessage());
            }

            Uri uri = Uri.fromFile(image);

            cropIntent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
            // start the activity - we handle returning in onActivityResult
            startActivityForResult(cropIntent, RESULT_CROP_DP);
        } catch (ActivityNotFoundException anfe) {
            String errorMessage = "your device doesn't support the crop action!";
            Toast toast = Toast.makeText(MentionQueryActivity.this, errorMessage, Toast.LENGTH_SHORT);
            toast.show();
        }
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        Utils.removeSimpleSpinProgressDialog();
        try {
            if (result != null && !result.isEmpty()) {
                Log.i("TAG", "TAG Result : " + result);
                //      {"document":{"response":{"status":1,"message":"Success."}}}
                switch (request) {
                    case createBooking:
                        try {
                            JSONObject mainObj = new JSONObject(result);
                            if (mainObj.getString("error_code").equalsIgnoreCase("0")) {
                                //Utils.showToast(mainObj.getString("error_string"), MentionQueryActivity.this);

                                JSONObject resultObj = mainObj.getJSONObject("result");
                                MyConstants.consultId = resultObj.getString("consult_id");
                                MyConstants.Wallet_Balance = resultObj.getString("WalletBalance");
                                MyConstants.isFree = resultObj.getString("IsFree");

                                MyConstants.isBooked = false;

                                // reload all data
                                MyConstants.bookingHistoryList.clear();
                                MyConstants.messageHistoryList.clear();
                                MyConstants.userChatList.clear();
                                MyConstants.doctorChatList.clear();
                                MyConstants.isBookingHistoryLoad = true;
                                MyConstants.isMessageHistoryLoad = true;

                                Intent intent = new Intent(MentionQueryActivity.this, ConfirmPaymentActivity.class);
                                startActivity(intent);

                            } else {
                                Utils.showToast(mainObj.getString("error_string"), MentionQueryActivity.this);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onProgressUpdate(String uniqueMessageId, int progres) {

    }

    @Override
    public void onProgressComplete(String uniqueMessageId, String result, Constant.REQUESTS request) {

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
