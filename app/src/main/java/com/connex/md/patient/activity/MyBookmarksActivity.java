package com.connex.md.patient.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.ImageView;

import com.connex.md.R;
import com.connex.md.interfaces.AsyncTaskListner;
import com.connex.md.model.SearchDoctor;
import com.connex.md.model.Specialities;
import com.connex.md.others.Internet;
import com.connex.md.patient.adapter.MyBookmarksAdapter;
import com.connex.md.utils.VerticalSpacingDecoration;
import com.connex.md.ws.CallRequest;
import com.connex.md.ws.Constant;
import com.connex.md.ws.JsonParserUniversal;
import com.connex.md.ws.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MyBookmarksActivity extends AppCompatActivity implements AsyncTaskListner {

    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    public Specialities specialities;
    public ArrayList<Specialities> specialitiesArray = new ArrayList<>();
    public JsonParserUniversal jParser;
    RecyclerView rvMyBookmarks;
    ImageView ivNoBookings;
    private StaggeredGridLayoutManager staggeredGridLayoutManager;
    private MyBookmarksAdapter myBookmarksAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_bookmarks);

        ButterKnife.bind(this);

        setupToolbar();

        jParser = new JsonParserUniversal();

        rvMyBookmarks = findViewById(R.id.rvMyBookmarks);
        ivNoBookings = findViewById(R.id.iv_no_bookings);

        rvMyBookmarks.setHasFixedSize(true);
        rvMyBookmarks.addItemDecoration(new VerticalSpacingDecoration(30));
        rvMyBookmarks.setItemViewCacheSize(20);
        rvMyBookmarks.setDrawingCacheEnabled(true);
        rvMyBookmarks.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);

        getMyBookmarks();
    }


    private void setupToolbar() {
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle("My Bookmarks");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }


    private void getMyBookmarks() {
        if (!Internet.isAvailable(MyBookmarksActivity.this)) {
            Internet.showAlertDialog(MyBookmarksActivity.this, "Error!", "No Internet Connection", false);

            return;
        }

        new CallRequest(MyBookmarksActivity.this).getMyBookmarks();
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        try {
            if (result != null && !result.isEmpty()) {
                Log.i("TAG", "TAG Result : " + result);
                //      {"document":{"response":{"status":1,"message":"Success."}}}
                switch (request) {
                    case myBookmarks:
                        Utils.removeSimpleSpinProgressDialog();
                        try {
                            JSONObject mainObj = new JSONObject(result);
                            if (mainObj.getString("error_code").equalsIgnoreCase("0")) {

                                specialitiesArray.clear();
                                if (mainObj.getJSONArray("result") != null && mainObj.getJSONArray("result").length() > 0) {
                                    JSONArray jBranchArray = mainObj.getJSONArray("result");


                                    for (int i = 0; i < jBranchArray.length() ; i++) {

                                        JSONObject jSpecial;
                                        jSpecial = jBranchArray.getJSONObject(i);

                                        specialities = (Specialities) jParser.parseJson(jSpecial, new Specialities());

                                        ArrayList<SearchDoctor> doctorList = new ArrayList<>();
                                        JSONArray array = jSpecial.getJSONArray("Doctors");
                                        for (int j = 0; j < array.length(); j++) {

                                            JSONObject detail_obj = array.getJSONObject(j);

                                            SearchDoctor searchDoctor = new SearchDoctor();
                                            searchDoctor.setFirstName(detail_obj.getString("FirstName"));
                                            searchDoctor.setLastName(detail_obj.getString("LastName"));
                                            searchDoctor.setDoctorId(detail_obj.getString("DoctorId"));
                                            searchDoctor.setCosultCharge(detail_obj.getString("CosultCharge"));
                                            searchDoctor.setCountry(detail_obj.getString("Country"));
                                            searchDoctor.setProfilePic(detail_obj.getString("ProfilePic"));
                                            searchDoctor.setLocalCharge(detail_obj.getString("LocalCharge"));
                                            searchDoctor.setIsFree(detail_obj.getString("IsFree"));

                                            doctorList.add(searchDoctor);
                                        }

                                        specialities.setSearchDoctorList(doctorList);
                                        specialitiesArray.add(specialities);

                                    }


                                    LayoutAnimationController controller = AnimationUtils.loadLayoutAnimation(rvMyBookmarks.getContext(), R.anim.layout_animation_from_right);
                                    rvMyBookmarks.setLayoutAnimation(controller);
                                    rvMyBookmarks.scheduleLayoutAnimation();

                                    staggeredGridLayoutManager = new StaggeredGridLayoutManager(2, 1);
                                    rvMyBookmarks.setLayoutManager(staggeredGridLayoutManager);

                                    myBookmarksAdapter = new MyBookmarksAdapter(MyBookmarksActivity.this, specialitiesArray);
                                    rvMyBookmarks.setAdapter(myBookmarksAdapter);


                                    //openChatDialog();

                                } else {
                                    Utils.showToast(mainObj.getString("error_string"), MyBookmarksActivity.this);
                                }
                            } else {
                                Utils.showToast(mainObj.getString("error_string"), MyBookmarksActivity.this);
                            }
                            if (specialitiesArray.size() > 0) {
                                rvMyBookmarks.setVisibility(View.VISIBLE);
                                ivNoBookings.setVisibility(View.GONE);
                            } else {
                                rvMyBookmarks.setVisibility(View.GONE);
                                ivNoBookings.setVisibility(View.VISIBLE);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onProgressUpdate(String uniqueMessageId, int progres) {

    }

    @Override
    public void onProgressComplete(String uniqueMessageId, String result, Constant.REQUESTS request) {

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
