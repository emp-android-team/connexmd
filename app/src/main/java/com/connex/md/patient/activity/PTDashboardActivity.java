package com.connex.md.patient.activity;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.app.LocalActivityManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTabHost;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;

import com.connex.md.doctor.activity.InPersonVisitsActivity;
import com.crashlytics.android.Crashlytics;
import com.connex.md.R;
import com.connex.md.activity.AboutUsActivity;
import com.connex.md.activity.FaqActivity;
import com.connex.md.activity.HelpActivity;
import com.connex.md.activity.HowItWorksActivity;
import com.connex.md.activity.SettingsActivity;
import com.connex.md.animation.MyBounceInterpolator;
import com.connex.md.audio_video_calling.BaseActivity;
import com.connex.md.audio_video_calling.SinchService;
import com.connex.md.custom_views.RobottoTextView;
import com.connex.md.doctor.fragment.DRMessagesFragment;
import com.connex.md.doctor.fragment.DRProfileFragment;
import com.connex.md.firebase_chat.activity.NurseFireChatActivity;
import com.connex.md.others.App;
import com.connex.md.others.PicassoTrustAll;
import com.connex.md.others.User;
import com.connex.md.patient.fragment.BookingHistoryFragment;
import com.connex.md.patient.fragment.HealthFragment;
import com.connex.md.patient.fragment.HomeFragment;
import com.connex.md.patient.fragment.MessagesFragment;
import com.connex.md.patient.fragment.PTProfileNewFragment;
import com.connex.md.patient.fragment.ReferAndEarnFragment;
import com.connex.md.ws.Constant;
import com.connex.md.ws.MyConstants;
import com.connex.md.ws.Utils;
import com.facebook.appevents.AppEventsConstants;
import com.facebook.appevents.AppEventsLogger;
import com.github.siyamed.shapeimageview.CircularImageView;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.sinch.android.rtc.PushTokenRegistrationCallback;
import com.sinch.android.rtc.SinchError;
import com.squareup.picasso.Callback;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.fabric.sdk.android.Fabric;

public class PTDashboardActivity extends BaseActivity implements View.OnClickListener, SinchService.StartFailedListener, PushTokenRegistrationCallback {


    public static final String TAG = "Dashboard Activity";
    private static final int REQUEST_CODE_PERMISSION = 2;
    public static PTDashboardActivity staticThis;
    public static Dialog dialog, dialogGuest;
    public static ImageView ivCancel;
    public static TextView tvUserName, tvNurseName, tvClinic;
    public static Button btnStartChat;
    public static CircularImageView ivNurseDp;
    public static boolean active = false;
    public static String firstName, lastName, profilePic, work, id;
    public static DatabaseReference fireDB;
    public static SharedPreferences preferences;
    public static RobottoTextView tvBadgeCount;
    private static FragmentTabHost mTabHost;
    public AppEventsLogger fbAppLogger;
    public App app;
    public PTDashboardActivity context;
    public LinearLayout ll_services, ll_how_it_works, ll_wallet, ll_refer_and_earn, ll_about_us, ll_faqs, ll_help, ll_sign_out, ll_payment, ll_feedback, ll_sign_in, ll_doctor_notes, ll_my_bookmarks, mInpersonAppointments;
    public ImageView ivSettings;
    public ImageView ivUser;
    public TextView tvName, tvEmail;
    public TextView tvCount;
    public Button btnMyProfile, btnCompleteMyProfile;
    public LocalActivityManager mLocalActivityManager;
    private String firstTimeChat = "0", isNotification = "0";
    private SharedPreferences sharedpreferences;
    private SharedPreferences.Editor editor;
    private View line, line2;
    private View focusView;
    private String mPermission = Manifest.permission.ACCESS_FINE_LOCATION;
    public boolean mPushTokenIsRegistered = false;


    public static void openNurseDialog(JSONObject resultObj) {

        try {

            final String id = resultObj.getString("id");
            final String UniqueId = resultObj.getString("UniqueId");
            final String UserId = resultObj.getString("UserId");
            final String NurseId = resultObj.getString("NurseId");
            final String Status = resultObj.getString("Status");
            final String IsGuest = resultObj.getString("IsGuest");
            final String FirstName = resultObj.getString("FirstName");
            final String LastName = resultObj.getString("LastName");
            final String ProfilePic = resultObj.getString("ProfilePic");
            final String NurseWork = resultObj.getString("NurseWork");

            createDialog();
            initDialogComponents();


            String nameHTML = "";
            if (preferences.getBoolean(MyConstants.IS_LOGGED_IN, false)) {
                nameHTML = "Hello <font color='#39c3f6'>" + App.user.getFirstName() + " " + App.user.getLastName() + "," + "</font>";
            } else {
                nameHTML = "Hello <font color='#39c3f6'>" + "there," + "</font>";
            }
            tvUserName.setText(Html.fromHtml(nameHTML));
            tvNurseName.setText(FirstName + " " + LastName);
            if (TextUtils.isEmpty(NurseWork)) {
                tvClinic.setText(NurseWork);
                tvClinic.setVisibility(View.GONE);
            } else {
                tvClinic.setText(NurseWork);
                tvClinic.setVisibility(View.VISIBLE);
            }

            try {
                PicassoTrustAll.getInstance(staticThis)
                        .load(ProfilePic)
                        .placeholder(R.drawable.avatar)
                        .error(R.drawable.avatar)
                        .into(ivNurseDp, new Callback() {
                            @Override
                            public void onSuccess() {
                            }

                            @Override
                            public void onError() {
                            }
                        });
            } catch (Exception e) {
                e.printStackTrace();
            }

            btnStartChat.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();

                    Utils.showProgressDialog(staticThis);

                    fireDB = FirebaseDatabase.getInstance().getReference().child(MyConstants.NURSE_CHANNEL);

                    fireDB.orderByChild(UniqueId).addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            Log.i("Data snapshot Nursechat", dataSnapshot.toString());
                            Utils.removeSimpleSpinProgressDialog();
                            Intent intent = new Intent(staticThis, NurseFireChatActivity.class);


                            if (dataSnapshot.child(UniqueId).getValue() == null) {
                                intent.putExtra(MyConstants.IS_PAYMENT_DONE, true);
                            } else {
                                intent.putExtra(MyConstants.IS_PAYMENT_DONE, false);
                            }

                            intent.putExtra(MyConstants.PT_ID, UserId);
                            intent.putExtra(MyConstants.DR_ID, NurseId);
                            intent.putExtra("status", Status);
                            intent.putExtra(MyConstants.UNIQUE_CHAT_ID, UniqueId);
                            intent.putExtra(MyConstants.DR_NAME, FirstName + " " + LastName);
                            intent.putExtra(MyConstants.RECEIVER_IMAGE_URL, ProfilePic);
                            staticThis.startActivity(intent);

                            Utils.removeSimpleSpinProgressDialog();
                            //Key does not exist
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });
                }
            });

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public static void openChatDialog(JSONObject data) {

        try {
            id = data.getString("id");
            firstName = data.getString("FirstName");
            lastName = data.getString("LastName");
            work = data.getString("NurseWork");
            profilePic = data.getString("ProfilePic");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        createDialog();
        initDialogComponents();


        String nameHTML = "";
        if (preferences.getBoolean(MyConstants.IS_LOGGED_IN, false)) {
            nameHTML = "Hello <font color='#39c3f6'>" + App.user.getFirstName() + " " + App.user.getLastName() + "," + "</font>";
        } else {
            nameHTML = "Hello <font color='#39c3f6'>" + "there," + "</font>";
        }
        tvUserName.setText(Html.fromHtml(nameHTML));
        tvNurseName.setText(firstName + " " + lastName);
        tvClinic.setText(work);

        try {
            PicassoTrustAll.getInstance(staticThis)
                    .load(profilePic)
                    .placeholder(R.drawable.avatar)
                    .error(R.drawable.avatar)
                    .into(ivNurseDp, new Callback() {
                        @Override
                        public void onSuccess() {
                        }

                        @Override
                        public void onError() {
                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }

        btnStartChat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                Fragment fragment = staticThis.getSupportFragmentManager().findFragmentById(android.R.id.tabcontent);
                if (fragment instanceof HomeFragment) {
                    HomeFragment yourFragment = (HomeFragment) fragment;
                    yourFragment.onClickChat();
                }
            }
        });
    }

    private static void createDialog() {
        dialog = new Dialog(staticThis, R.style.ChatDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        // Include dialog.xml file
        dialog.setContentView(R.layout.dialog_chat_popup);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.BOTTOM;
        window.setAttributes(lp);
        dialog.show();
    }

    private static void initDialogComponents() {
        //ivCancel = dialog.findViewById(R.id.ivCancel);
        tvUserName = dialog.findViewById(R.id.tvUserName);
        tvNurseName = dialog.findViewById(R.id.tvNurseName);
        tvClinic = dialog.findViewById(R.id.tvClinic);
        btnStartChat = dialog.findViewById(R.id.btnStartChat);
        ivNurseDp = dialog.findViewById(R.id.ivNurseDp);
        //ivNurseDp.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
    }

    public static void setBadgeCount() {

        String badge = preferences.getString(MyConstants.BADGE_COUNT, "0");

        System.out.println("tab count:::" + mTabHost.getTabWidget().getChildCount());
        for (int i = 0; i < mTabHost.getTabWidget().getChildCount(); i++) {
            View v = mTabHost.getTabWidget().getChildAt(i); // unselected
            RobottoTextView tv = v.findViewById(R.id.tvBadgeCount);
            if (i == 1) {
                if (badge.equalsIgnoreCase("0")) {
                    tv.setVisibility(View.GONE);
                } else {
                    tv.setVisibility(View.VISIBLE);
                    tv.setText(badge);
                }
            } else {
                tv.setVisibility(View.GONE);
            }
        }


    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            Fabric.with(this, new Crashlytics());
        } catch (Exception e) {
            e.printStackTrace();
        }
        setContentView(R.layout.activity_pt_dashboard);
        /*mLocalActivityManager = new LocalActivityManager(this, false);
        mLocalActivityManager.dispatchCreate(savedInstanceState);*/

        /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkAllPermissions();
        }*/

        fbAppLogger = AppEventsLogger.newLogger(this);
        fbAppLogger.logEvent(AppEventsConstants.EVENT_NAME_VIEWED_CONTENT, 1);

        Utils.logUser();
        staticThis = this;
        context = this;

        app = App.getInstance();

        sharedpreferences = getSharedPreferences(MyConstants.PREF, Context.MODE_PRIVATE);
        preferences = sharedpreferences;
        if (sharedpreferences.getBoolean(MyConstants.IS_LOGGED_IN, false)) {
            App.user.setUserID(sharedpreferences.getString(MyConstants.USER_ID, ""));
            App.user.setName(sharedpreferences.getString(MyConstants.PT_NAME, ""));
            App.user.setFirstName(sharedpreferences.getString(MyConstants.PT_FIRST_NAME, ""));
            App.user.setLastName(sharedpreferences.getString(MyConstants.PT_LAST_NAME, ""));
            App.user.setUserEmail(sharedpreferences.getString(MyConstants.USER_EMAIL, ""));
            App.user.setUser_Type(sharedpreferences.getString(MyConstants.USER_TYPE, ""));
            App.user.setSinch_id(sharedpreferences.getString(MyConstants.SINCH_ID, ""));
            App.user.setLoginType(sharedpreferences.getString(MyConstants.LOGIN_TYPE, "0"));
            App.user.setProfileUrl(sharedpreferences.getString(MyConstants.PROFILE_PIC, ""));
            MyConstants.API_TOKEN = sharedpreferences.getString(MyConstants.TOKEN, "");

            if (sharedpreferences.getBoolean(MyConstants.IS_GUEST, false)) {
                MyConstants.isGuest = "1";
            } else {
                MyConstants.isGuest = "0";
            }

            System.out.println("----- " + App.user.getSinch_id());
            //getSinchServiceInterface().startClient(App.user.getSinch_id());
            //getSinchServiceInterface().registerPushToken(this);

            //getSinchServiceInterface().getManagedPush().registerPushToken(this);
        }

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            if (bundle.containsKey("firstTimeChat")) {
                firstTimeChat = bundle.getString("firstTimeChat");
            } else {
                firstTimeChat = "0";
            }

            if (bundle.containsKey("isNotification")) {
                isNotification = bundle.getString("isNotification");
            } else {
                isNotification = "0";
            }

        } else {
            firstTimeChat = "0";
        }

        System.out.println("isNotification:::" + isNotification);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);

        NavigationView navigationView = findViewById(R.id.nav_view);
        View headerLayout = navigationView.getHeaderView(0);
        focusView = headerLayout.findViewById(R.id.focusview);

        final DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                try {
                    focusView.requestFocus();

                    InputMethodManager inputMethodManager = (InputMethodManager)
                            getSystemService(Context.INPUT_METHOD_SERVICE);
                    if (inputMethodManager != null) {
                        inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);

                try {
                    InputMethodManager inputMethodManager = (InputMethodManager)
                            getSystemService(Context.INPUT_METHOD_SERVICE);
                    if (inputMethodManager != null) {
                        inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        };
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        manageTabs();

        try {

        } catch (Exception e) {
            e.printStackTrace();
        }

        System.out.println("firstTimeChat:::" + firstTimeChat);
        if (firstTimeChat.equalsIgnoreCase("1") || !isNotification.equalsIgnoreCase("0")) {
            mTabHost.setCurrentTab(1);
            firstTimeChat = "0";
            isNotification = "0";
        } else {
            mTabHost.setCurrentTab(2);
        }

        ivUser = headerLayout.findViewById(R.id.ivUser);
        tvName = headerLayout.findViewById(R.id.tvName);
        tvEmail = headerLayout.findViewById(R.id.tvEmail);
        btnMyProfile = headerLayout.findViewById(R.id.btnMyProfile);
        btnCompleteMyProfile = headerLayout.findViewById(R.id.btnCompleteProfile);
        TextView tvVersion = headerLayout.findViewById(R.id.tvVersion);
        try {
            tvVersion.setText("Version " + getPackageManager().getPackageInfo(this.getPackageName(), 0).versionName);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        tvName.setText(App.user.getName());
        tvEmail.setText(App.user.getUserEmail());
        System.out.println("profile:::" + App.user.getProfileUrl());
        if (App.user.getProfileUrl() != null) {
            try {
                if (!TextUtils.isEmpty(App.user.getProfileUrl())) {
                    PicassoTrustAll.getInstance(PTDashboardActivity.this)
                            .load(App.user.getProfileUrl())
                            .placeholder(R.drawable.avatar)
                            .error(R.drawable.avatar)
                            .into(ivUser, new Callback() {
                                @Override
                                public void onSuccess() {
                                }

                                @Override
                                public void onError() {
                                }
                            });
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        btnMyProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (TextUtils.isEmpty(App.user.getUserID())) {
                    Intent intent = new Intent(PTDashboardActivity.this, PTLoginActivity.class);
                    intent.putExtra("is_logged_in", 0);
                    startActivity(intent);
                } else {
                    drawer.closeDrawer(GravityCompat.START);
                    if (MyConstants.isGuest.equalsIgnoreCase("0")) {
                        mTabHost.setCurrentTab(4);
                    } else {
                        Utils.showAlert("Its seems you are guest user, Please login to access profile.", PTDashboardActivity.this);
                    }
                }
            }
        });

        btnCompleteMyProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawer.closeDrawer(GravityCompat.START);
                Intent intent = new Intent(PTDashboardActivity.this, PTSignUpActivity.class);
                intent.putExtra("completeProfile", "1");
                startActivity(intent);
            }
        });


        mInpersonAppointments = headerLayout.findViewById(R.id.ll_inperson_appointments);
        ll_services = headerLayout.findViewById(R.id.ll_services);
        ll_how_it_works = headerLayout.findViewById(R.id.ll_how_it_works);
        ll_wallet = headerLayout.findViewById(R.id.ll_wallet);
        ll_refer_and_earn = headerLayout.findViewById(R.id.ll_refer_and_earn);
        ll_about_us = headerLayout.findViewById(R.id.ll_about_us);
        ll_faqs = headerLayout.findViewById(R.id.ll_faqs);
        ll_help = headerLayout.findViewById(R.id.ll_help);
        ll_sign_out = headerLayout.findViewById(R.id.ll_sign_out);
        ll_feedback = headerLayout.findViewById(R.id.ll_feedback);
        ll_payment = headerLayout.findViewById(R.id.ll_payment);
        ivSettings = headerLayout.findViewById(R.id.ivSettings);
        ll_doctor_notes = headerLayout.findViewById(R.id.ll_doctor_notes);
        ll_my_bookmarks = headerLayout.findViewById(R.id.ll_my_bookmarks);
        ll_sign_in = headerLayout.findViewById(R.id.ll_sign_in);
        line = headerLayout.findViewById(R.id.line);
        line2 = headerLayout.findViewById(R.id.line2);
        tvCount = headerLayout.findViewById(R.id.count);

        focusView.requestFocus();

        mInpersonAppointments.setOnClickListener(this);
        ll_services.setOnClickListener(this);
        ll_how_it_works.setOnClickListener(this);
        ll_refer_and_earn.setOnClickListener(this);
        ll_wallet.setOnClickListener(this);
        ll_about_us.setOnClickListener(this);
        ll_faqs.setOnClickListener(this);
        ll_help.setOnClickListener(this);
        ll_sign_out.setOnClickListener(this);
        ll_feedback.setOnClickListener(this);
        ll_payment.setOnClickListener(this);
        ivSettings.setOnClickListener(this);
        ll_sign_in.setOnClickListener(this);
        ll_doctor_notes.setOnClickListener(this);
        ll_my_bookmarks.setOnClickListener(this);

        if (TextUtils.isEmpty(App.user.getUserID())) {
            ll_sign_out.setVisibility(View.GONE);
            ll_sign_in.setVisibility(View.VISIBLE);
        } else {
            ll_sign_out.setVisibility(View.VISIBLE);
            ll_sign_in.setVisibility(View.GONE);
        }

        if (sharedpreferences.getBoolean(MyConstants.IS_LOGGED_IN, false)) {
            if (App.user.getUser_Type().equalsIgnoreCase(MyConstants.USER_PT) && MyConstants.isGuest.equalsIgnoreCase("0") && !TextUtils.isEmpty(MyConstants.headerPrice)) {
                ll_refer_and_earn.setVisibility(View.VISIBLE);
                ll_wallet.setVisibility(View.VISIBLE);
                line.setVisibility(View.VISIBLE);
                //ll_doctor_notes.setVisibility(View.VISIBLE);
                ll_doctor_notes.setVisibility(View.GONE);
                ll_my_bookmarks.setVisibility(View.VISIBLE);
                line2.setVisibility(View.VISIBLE);

                btnCompleteMyProfile.setVisibility(View.GONE);
                btnMyProfile.setVisibility(View.VISIBLE);

            } else if (App.user.getUser_Type().equalsIgnoreCase(MyConstants.USER_PT) && MyConstants.isGuest.equalsIgnoreCase("1")) {
                ll_refer_and_earn.setVisibility(View.GONE);
                ll_wallet.setVisibility(View.VISIBLE);
                line.setVisibility(View.VISIBLE);
                ll_doctor_notes.setVisibility(View.GONE);
                ll_my_bookmarks.setVisibility(View.VISIBLE);
                line2.setVisibility(View.GONE);

                btnCompleteMyProfile.setVisibility(View.VISIBLE);
                btnMyProfile.setVisibility(View.GONE);
            } else {
                ll_refer_and_earn.setVisibility(View.GONE);
                ll_wallet.setVisibility(View.GONE);
                line.setVisibility(View.GONE);
                ll_doctor_notes.setVisibility(View.GONE);
                ll_my_bookmarks.setVisibility(View.GONE);
                line2.setVisibility(View.GONE);

                btnCompleteMyProfile.setVisibility(View.GONE);
                btnMyProfile.setVisibility(View.VISIBLE);
            }
        } else {
            ll_refer_and_earn.setVisibility(View.GONE);
            ll_wallet.setVisibility(View.GONE);
            line.setVisibility(View.GONE);
            ll_doctor_notes.setVisibility(View.GONE);
            line2.setVisibility(View.GONE);
            ll_my_bookmarks.setVisibility(View.GONE);

            btnCompleteMyProfile.setVisibility(View.GONE);
            btnMyProfile.setVisibility(View.VISIBLE);
        }

        if (sharedpreferences.getBoolean(MyConstants.IS_LOGGED_IN, false)) {
            // && App.user.getLoginType().equalsIgnoreCase("0")
            if (App.user.getUser_Type().equalsIgnoreCase(MyConstants.USER_PT) && MyConstants.isGuest.equalsIgnoreCase("0")) {
                ivSettings.setVisibility(View.VISIBLE);
            } else if (App.user.getUser_Type().equalsIgnoreCase(MyConstants.USER_DR)) {
                ivSettings.setVisibility(View.VISIBLE);
            } else {
                ivSettings.setVisibility(View.INVISIBLE);
            }
        } else {
            ivSettings.setVisibility(View.INVISIBLE);
        }
    }

    private void manageTabs() {

        try{
            mTabHost = findViewById(R.id.tabhost);
            //mTabHost.setVisibility(View.GONE);
            mTabHost.setup(context, getSupportFragmentManager(), android.R.id.tabcontent);
            mTabHost.getTabWidget().setDividerDrawable(null);
            mTabHost.getTabWidget().setStripEnabled(false);

            mTabHost.addTab(mTabHost.newTabSpec(Constant.TABS.NOTIFICATIONS.toString())
                    .setIndicator(getTabIndicator(mTabHost.getContext(),
                            R.drawable.tab_notifications_selector, "Bookings")), BookingHistoryFragment.class, null);

            if (sharedpreferences.getBoolean(MyConstants.IS_LOGGED_IN, false)) {
                if (App.user.getUser_Type().equalsIgnoreCase(MyConstants.USER_PT)) {
                    mTabHost.addTab(mTabHost.newTabSpec(Constant.TABS.MESSAGES.toString())
                            .setIndicator(getTabIndicator(mTabHost.getContext(),
                                    R.drawable.tab_message_selector, "Messages")), MessagesFragment.class, null);
                } else {
                    mTabHost.addTab(mTabHost.newTabSpec(Constant.TABS.MESSAGES.toString())
                            .setIndicator(getTabIndicator(mTabHost.getContext(),
                                    R.drawable.tab_message_selector, "Messages")), DRMessagesFragment.class, null);
                }
            } else {
                mTabHost.addTab(mTabHost.newTabSpec(Constant.TABS.MESSAGES.toString())
                        .setIndicator(getTabIndicator(mTabHost.getContext(),
                                R.drawable.tab_message_selector, "Messages")), MessagesFragment.class, null);
            }


            mTabHost.addTab(mTabHost.newTabSpec("Home")
                    .setIndicator(getTabIndicator(mTabHost.getContext(),
                            R.drawable.tab_home_selector, "Home")), HomeFragment.class, null);


            mTabHost.addTab(mTabHost.newTabSpec(Constant.TABS.HEALTH.toString())
                    .setIndicator(getTabIndicator(mTabHost.getContext(),
                            R.drawable.tab_health_selector, "Health")), HealthFragment.class, null);


            System.out.println("user type::" + App.user.getUser_Type());

            if (sharedpreferences.getBoolean(MyConstants.IS_LOGGED_IN, false)) {
                if (App.user.getUser_Type().equalsIgnoreCase(MyConstants.USER_PT)) {
                    System.out.println("patient");
                    mTabHost.addTab(mTabHost.newTabSpec(Constant.TABS.PROFILE.toString())
                            .setIndicator(getTabIndicator(mTabHost.getContext(),
                                    R.drawable.tab_profile_selector, "Profile")), PTProfileNewFragment.class, null);
                } else {
                    System.out.println("doctor login");
                    mTabHost.addTab(mTabHost.newTabSpec(Constant.TABS.PROFILE.toString())
                            .setIndicator(getTabIndicator(mTabHost.getContext(),
                                    R.drawable.tab_profile_selector, "Profile")), DRProfileFragment.class, null);
                }
            } else {
                mTabHost.addTab(mTabHost.newTabSpec(Constant.TABS.PROFILE.toString())
                        .setIndicator(getTabIndicator(mTabHost.getContext(),
                                R.drawable.tab_profile_selector, "Profile")), PTProfileNewFragment.class, null);
            }


            mTabHost.getTabWidget().getChildAt(2).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {
                        setAnimationOnView(view);
                        MyConstants.homeFragment = true;
                        mTabHost.setCurrentTab(2);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });


            mTabHost.getTabWidget().getChildAt(0).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    setAnimationOnView(view);
                    MyConstants.homeFragment = false;
                    if (TextUtils.isEmpty(App.user.getUserID())) {
                        Intent intent = new Intent(PTDashboardActivity.this, PTLoginActivity.class);
                        intent.putExtra("is_logged_in", 0);
                        startActivity(intent);
                    } else {
                        mTabHost.setCurrentTab(0);
                    }
                }
            });

            mTabHost.getTabWidget().getChildAt(1).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    setAnimationOnView(view);
                    MyConstants.homeFragment = false;
                    if (TextUtils.isEmpty(App.user.getUserID())) {
                        Intent intent = new Intent(PTDashboardActivity.this, PTLoginActivity.class);
                        intent.putExtra("is_logged_in", 0);
                        startActivity(intent);
                    } else {
                        mTabHost.setCurrentTab(1);
                    }
                }
            });

            mTabHost.getTabWidget().getChildAt(3).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    setAnimationOnView(view);
                    mTabHost.setCurrentTab(3);
                }
            });

            mTabHost.getTabWidget().getChildAt(4).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    setAnimationOnView(view);
                    MyConstants.homeFragment = false;
                    if (TextUtils.isEmpty(App.user.getUserID())) {
                        Intent intent = new Intent(PTDashboardActivity.this, PTLoginActivity.class);
                        intent.putExtra("is_logged_in", 0);
                        startActivity(intent);
                    } else {
                        if (MyConstants.isGuest.equalsIgnoreCase("1")) {
                            btnCompleteMyProfile.performClick();
                            //Utils.showAlert("Its seems you are guest user, Please login to access profile.", PTDashboardActivity.this);
                        } else {
                            mTabHost.setCurrentTab(4);
                        }
                    }
                }
            });

            //tabHost = mTabHost;

            mTabHost.setOnTabChangedListener(new TabHost.OnTabChangeListener() {
                @Override
                public void onTabChanged(String tabId) {

                    for (int i = 0; i < mTabHost.getTabWidget().getChildCount(); i++) {
                        View v = mTabHost.getTabWidget().getChildAt(i); // unselected
                        RobottoTextView tv = v.findViewById(R.id.tvTabName);
                        tv.setTextColor(Color.parseColor("#868686"));
                    }

                    View v = mTabHost.getTabWidget().getChildAt(mTabHost.getCurrentTab());
                    RobottoTextView tv = v.findViewById(R.id.tvTabName);
                    tv.setTextColor(Color.parseColor("#39c3f6"));

                    FragmentManager manager = getSupportFragmentManager();
                    System.out.println("::::::::::count:::::::" + manager.getBackStackEntryCount());
                    if (manager.getBackStackEntryCount() > 0) {
                        manager.popBackStack(null,
                                FragmentManager.POP_BACK_STACK_INCLUSIVE);
                    }


                }
            });
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void setAnimationOnView(View view) {
        final Animation myAnim = AnimationUtils.loadAnimation(PTDashboardActivity.this, R.anim.bounce);

        // Use bounce interpolator with amplitude 0.2 and frequency 20
        MyBounceInterpolator interpolator = new MyBounceInterpolator(0.2, 20);
        myAnim.setInterpolator(interpolator);

        view.startAnimation(myAnim);
    }

    public void setDoctorNotesCount(String count) {
        if (count.equalsIgnoreCase("0")) {
            tvCount.setText(count);
            tvCount.setVisibility(View.GONE);
        } else {
            tvCount.setText(count);
            tvCount.setVisibility(View.VISIBLE);
        }
    }

    public void setVisibilityDoctorNotes(boolean flag) {
        if (flag) {
            ll_doctor_notes.setVisibility(View.VISIBLE);
            line2.setVisibility(View.VISIBLE);
        } else {
            ll_doctor_notes.setVisibility(View.GONE);
            line2.setVisibility(View.GONE);
        }
    }

    public void openCloseDrawer() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            drawer.openDrawer(GravityCompat.START);
        }
    }

    @Override
    protected void onPause() {
        //mLocalActivityManager.dispatchResume();
        super.onPause();
        active = false;
    }

    @Override
    protected void onStop() {
        //mLocalActivityManager.dispatchStop();
        super.onStop();
    }

    /*@Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        //mLocalActivityManager.saveInstanceState();

    }*/

    @Override
    protected void onResume() {
        //mLocalActivityManager.dispatchResume();
        super.onResume();
        active = true;
        if (sharedpreferences.getBoolean(MyConstants.IS_LOGGED_IN, false)) {
            App.user.setUserID(sharedpreferences.getString(MyConstants.USER_ID, ""));
            App.user.setName(sharedpreferences.getString(MyConstants.PT_NAME, ""));
            App.user.setFirstName(sharedpreferences.getString(MyConstants.PT_FIRST_NAME, ""));
            App.user.setLastName(sharedpreferences.getString(MyConstants.PT_LAST_NAME, ""));
            App.user.setUserEmail(sharedpreferences.getString(MyConstants.USER_EMAIL, ""));
            App.user.setUser_Type(sharedpreferences.getString(MyConstants.USER_TYPE, ""));
            App.user.setSinch_id(sharedpreferences.getString(MyConstants.SINCH_ID, ""));
            App.user.setLoginType(sharedpreferences.getString(MyConstants.LOGIN_TYPE, "0"));
            App.user.setProfileUrl(sharedpreferences.getString(MyConstants.PROFILE_PIC, ""));
            MyConstants.API_TOKEN = sharedpreferences.getString(MyConstants.TOKEN, "");

            if (sharedpreferences.getBoolean(MyConstants.IS_GUEST, false)) {
                MyConstants.isGuest = "1";
            } else {
                MyConstants.isGuest = "0";
            }
        }

        setUserName();

        if (App.user.getProfileUrl() != null) {
            try {
                if (!TextUtils.isEmpty(App.user.getProfileUrl())) {
                    PicassoTrustAll.getInstance(PTDashboardActivity.this)
                            .load(App.user.getProfileUrl())
                            .placeholder(R.drawable.avatar)
                            .error(R.drawable.avatar)
                            .into(ivUser, new Callback() {
                                @Override
                                public void onSuccess() {
                                }

                                @Override
                                public void onError() {
                                }
                            });
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (TextUtils.isEmpty(App.user.getUserID())) {
            ll_sign_out.setVisibility(View.GONE);
            ll_sign_in.setVisibility(View.VISIBLE);
        } else {
            ll_sign_out.setVisibility(View.VISIBLE);
            ll_sign_in.setVisibility(View.GONE);
        }

        if (sharedpreferences.getBoolean(MyConstants.IS_LOGGED_IN, false)) {
            // && App.user.getLoginType().equalsIgnoreCase("0")
            if (App.user.getUser_Type().equalsIgnoreCase(MyConstants.USER_PT) && MyConstants.isGuest.equalsIgnoreCase("0")) {
                ivSettings.setVisibility(View.VISIBLE);
            } else if (App.user.getUser_Type().equalsIgnoreCase(MyConstants.USER_DR)) {
                ivSettings.setVisibility(View.VISIBLE);
            } else {
                ivSettings.setVisibility(View.INVISIBLE);
            }
        } else {
            ivSettings.setVisibility(View.INVISIBLE);
        }

        if (sharedpreferences.getBoolean(MyConstants.IS_LOGGED_IN, false)) {
            if (App.user.getUser_Type().equalsIgnoreCase(MyConstants.USER_PT) && MyConstants.isGuest.equalsIgnoreCase("0")) {
                ll_refer_and_earn.setVisibility(View.VISIBLE);
                ll_my_bookmarks.setVisibility(View.VISIBLE);
                ll_wallet.setVisibility(View.VISIBLE);
                line.setVisibility(View.VISIBLE);
            } else if (App.user.getUser_Type().equalsIgnoreCase(MyConstants.USER_PT) && MyConstants.isGuest.equalsIgnoreCase("1")) {
                ll_refer_and_earn.setVisibility(View.GONE);
                ll_my_bookmarks.setVisibility(View.VISIBLE);
                ll_wallet.setVisibility(View.VISIBLE);
                line.setVisibility(View.VISIBLE);
            } else {
                ll_refer_and_earn.setVisibility(View.GONE);
                ll_my_bookmarks.setVisibility(View.GONE);
                ll_wallet.setVisibility(View.GONE);
                line.setVisibility(View.GONE);
            }
        } else {
            ll_refer_and_earn.setVisibility(View.GONE);
            ll_my_bookmarks.setVisibility(View.GONE);
            ll_wallet.setVisibility(View.GONE);
            line.setVisibility(View.GONE);
        }
    }

    public void setUserName() {

        tvName.setText(App.user.getFirstName() + " " + App.user.getLastName());
        tvEmail.setText(App.user.getUserEmail());

        if (TextUtils.isEmpty(tvName.getText().toString().trim())) {
            tvName.setVisibility(View.GONE);
        } else {
            tvName.setVisibility(View.VISIBLE);
        }

        if (TextUtils.isEmpty(tvEmail.getText().toString().trim())) {
            tvEmail.setVisibility(View.GONE);
        } else {
            tvEmail.setVisibility(View.VISIBLE);
        }
    }

    public void setProfilePicture(Activity activity) {

        if (App.user.getProfileUrl() != null) {
            try {
                if (!TextUtils.isEmpty(App.user.getProfileUrl())) {
                    PicassoTrustAll.getInstance(PTDashboardActivity.this)
                            .load(App.user.getProfileUrl())
                            .placeholder(R.drawable.avatar)
                            .error(R.drawable.avatar)
                            .into(ivUser, new Callback() {
                                @Override
                                public void onSuccess() {
                                }

                                @Override
                                public void onError() {
                                }
                            });
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public boolean isTablet() {
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);

        float yInches = metrics.heightPixels / metrics.ydpi;
        float xInches = metrics.widthPixels / metrics.xdpi;
        double diagonalInches = Math.sqrt(xInches * xInches + yInches * yInches);
        // 6.5inch device or bigger
// smaller device
        return diagonalInches >= 6.5;
    }


    private View getTabIndicator(Context context, int icon, String tabName) {
        View view = LayoutInflater.from(context).inflate(R.layout.tab_layout, null);

        LinearLayout internalLinear = view.findViewById(R.id.internalLinear);
        LinearLayout upperTab = view.findViewById(R.id.upperTab);
        ImageView v = view.findViewById(R.id.tab);
        RobottoTextView tv = view.findViewById(R.id.tvTabName);
        //tvBadgeCount = view.findViewById(R.id.tvBadgeCount);
        tv.setText(tabName);
        v.setBackgroundResource(icon);
        v.setScaleType(ImageView.ScaleType.CENTER_INSIDE);

        if (tabName.equalsIgnoreCase("Messages")) {
            //tvBadgeCount.setVisibility(View.VISIBLE);
            setBadgeCount();
        } else {
            //tvBadgeCount.setVisibility(View.GONE);
        }

        if (tabName.equalsIgnoreCase("HOME")) {
            tv.setVisibility(View.GONE);
            //tvBadgeCount.setVisibility(View.GONE);
            if (!isTablet()) {
                Log.i("TAG ", "TAG in MOBILE");
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                LinearLayout.LayoutParams layoutParamsImage = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                upperTab.setPadding(0, 0, 0, 0);
                layoutParamsImage.setMargins((int) getResources().getDimension(R.dimen.margin_8), 0, (int) getResources().getDimension(R.dimen.margin_8), 0);
                v.setLayoutParams(layoutParamsImage);

                v.setScaleType(ImageView.ScaleType.FIT_CENTER);
                internalLinear.setLayoutParams(layoutParams);
                v.invalidate();
                internalLinear.invalidate();
            } else {

                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                LinearLayout.LayoutParams layoutParamsImage = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                upperTab.setPadding(0, 0, 0, 0);
                layoutParamsImage.setMargins((int) getResources().getDimension(R.dimen.margin_8), 0, (int) getResources().getDimension(R.dimen.margin_8), 0);
                v.setLayoutParams(layoutParamsImage);

                v.setScaleType(ImageView.ScaleType.FIT_CENTER);
                internalLinear.setLayoutParams(layoutParams);
                v.invalidate();
                internalLinear.invalidate();
            }

        } else {
            tv.setVisibility(View.VISIBLE);
        }

        return view;
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {

            int tab_position = mTabHost.getCurrentTab();
            System.out.println("tab position:::" + tab_position);
            System.out.println("list size::" + MyConstants.specialitiesList.size());

            if (tab_position != 2) {
                mTabHost.setCurrentTab(2);
            } else {
                super.onBackPressed();
            }
            //}

        }
    }

    @Override
    public void onClick(View view) {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (view == ll_services) {

        } else if (view == mInpersonAppointments) {
            if (App.user.getUser_Type().equalsIgnoreCase(MyConstants.USER_PT)) {
                startActivity(new Intent(PTDashboardActivity.this, InPersonAppointmentsActivity.class));

            } else {
                startActivity(new Intent(PTDashboardActivity.this, InPersonVisitsActivity.class));
            }

        } else if (view == ll_how_it_works) {
            drawer.closeDrawer(GravityCompat.START);
            Intent i6 = new Intent(PTDashboardActivity.this, HowItWorksActivity.class);
            i6.putExtra("x", "Doctor List");
            startActivity(i6);
        } else if (view == ll_refer_and_earn) {
            drawer.closeDrawer(GravityCompat.START);
            ReferAndEarnFragment referAndEarnFragment = new ReferAndEarnFragment();
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.frame, referAndEarnFragment);
            fragmentTransaction.commit();
            fragmentTransaction.addToBackStack(null);
        } else if (view == ll_wallet) {
            drawer.closeDrawer(GravityCompat.START);
            Intent i = new Intent(PTDashboardActivity.this, WalletActivity.class);
            startActivity(i);
        } else if (view == ll_about_us) {
            drawer.closeDrawer(GravityCompat.START);
            Intent i = new Intent(PTDashboardActivity.this, AboutUsActivity.class);
            startActivity(i);
        } else if (view == ll_faqs) {
            drawer.closeDrawer(GravityCompat.START);
            Intent i = new Intent(PTDashboardActivity.this, FaqActivity.class);
            startActivity(i);
        } else if (view == ll_help) {
            drawer.closeDrawer(GravityCompat.START);
            Intent i6 = new Intent(PTDashboardActivity.this, HelpActivity.class);
            i6.putExtra("x", "Doctor List");
            startActivity(i6);
        } else if (view == ll_sign_out) {
            setLogOutAlert();
        } else if (view == ll_feedback) {
            final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
            try {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
            } catch (android.content.ActivityNotFoundException anfe) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
            }
        } else if (view == ll_payment) {

        } else if (view == ivSettings) {
            if (sharedpreferences.getBoolean(MyConstants.IS_LOGGED_IN, false)) {
                if (MyConstants.isGuest.equalsIgnoreCase("0")) {
                    Intent intent = new Intent(PTDashboardActivity.this, SettingsActivity.class);
                    startActivity(intent);
                }
            }
        } else if (view == ll_sign_in) {
            Intent intent = new Intent(PTDashboardActivity.this, PTLoginActivity.class);
            intent.putExtra("is_logged_in", 0);
            startActivity(intent);
        } else if (view == ll_doctor_notes) {
            Intent intent = new Intent(PTDashboardActivity.this, DoctorNotesHistoryActivity.class);
            startActivity(intent);
        } else if (view == ll_my_bookmarks) {
            Intent intent = new Intent(PTDashboardActivity.this, MyBookmarksActivity.class);
            startActivity(intent);
        }

        drawer.closeDrawer(GravityCompat.START);
    }

    @Override
    public void onStartFailed(SinchError error) {

    }

    @Override
    public void onStarted() {

    }

    public void setLogOutAlert() {

        new AlertDialog.Builder(this)
                .setTitle("Alert!")
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setMessage("Would you like to sign out?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        editor = sharedpreferences.edit();
                        editor.remove(MyConstants.ADDRESS);
                        editor.remove(MyConstants.REFER_CODE);
                        editor.remove(MyConstants.TOKEN);
                        editor.remove(MyConstants.MOBILE);
                        editor.remove(MyConstants.SINCH_ID);
                        editor.remove(MyConstants.PT_LAST_NAME);
                        editor.remove(MyConstants.PT_NAME);
                        editor.remove(MyConstants.GENDER);
                        editor.remove(MyConstants.DATE_OF_BIRTH);
                        editor.remove(MyConstants.AGE);
                        editor.remove(MyConstants.PROFILE_PIC);
                        editor.remove(MyConstants.COUNTRY);
                        editor.remove(MyConstants.LOGIN_TYPE);
                        //editor.remove(MyConstants.USER_TYPE);
                        editor.remove(MyConstants.WALLET_BALANCE);
                        editor.remove(MyConstants.PT_ZONE);
                        editor.remove(MyConstants.CURRENT_DISCOUNT);
                        editor.remove(MyConstants.USER_ID);
                        editor.remove(MyConstants.PT_FIRST_NAME);

                        App.user = new User();

                        editor.putBoolean(MyConstants.IS_LOGGED_IN, false);
                        editor.commit();

                        tvName.setText("");
                        tvEmail.setText("");
                        tvName.setVisibility(View.GONE);
                        tvEmail.setVisibility(View.GONE);
                        btnCompleteMyProfile.setVisibility(View.GONE);
                        btnMyProfile.setVisibility(View.VISIBLE);
                        ivUser.setImageResource(R.drawable.avatar);

                        Utils.showToast("Signed out successfully", PTDashboardActivity.this);
                        dialog.dismiss();

                        ll_sign_out.setVisibility(View.GONE);
                        ll_sign_in.setVisibility(View.VISIBLE);
                        ll_refer_and_earn.setVisibility(View.GONE);
                        ll_wallet.setVisibility(View.GONE);
                        ll_doctor_notes.setVisibility(View.GONE);
                        ll_my_bookmarks.setVisibility(View.GONE);
                        line.setVisibility(View.GONE);
                        ivSettings.setVisibility(View.INVISIBLE);

                        int tab_position = mTabHost.getCurrentTab();
                        System.out.println("tab position:::" + tab_position);

                        // reload all data
                        MyConstants.bookingHistoryList.clear();
                        MyConstants.messageHistoryList.clear();
                        MyConstants.userChatList.clear();
                        MyConstants.doctorChatList.clear();
                        MyConstants.isBookingHistoryLoad = true;
                        MyConstants.isMessageHistoryLoad = true;
                        MyConstants.userProfile = null;
                        MyConstants.isProfileLoad = true;
                        MyConstants.doctorProfilePersonal = null;
                        MyConstants.isDoctorProfileLoad = true;

                        if (tab_position != 2) {
                            mTabHost.setCurrentTab(2);
                        }


                        Intent intent = new Intent(PTDashboardActivity.this, PTLoginActivity.class);
                        startActivity(intent);
                        ActivityCompat.finishAffinity(PTDashboardActivity.this);
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .show();
    }

    @Override
    public void onDestroy() {
        if (BaseActivity.getSinchServiceInterface() != null) {
            //BaseActivity.getSinchServiceInterface().stopClient();
        }
        super.onDestroy();
    }


    @Override
    protected void onServiceConnected() {
        getSinchServiceInterface().setStartListener(this);

        try {
            if (!TextUtils.isEmpty(App.user.getUserID())) {
                if (MyConstants.isGuest.equalsIgnoreCase("0")) {
                    App.user.setSinch_id(sharedpreferences.getString(MyConstants.SINCH_ID, ""));
                }

                if (!getSinchServiceInterface().isStarted()) {
                    getSinchServiceInterface().startClient(App.user.getSinch_id());
                    getSinchServiceInterface().registerPushToken(this);
                }
                getSinchServiceInterface().setIncomingLisnter(this);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void tokenRegistered() {

    }

    @Override
    public void tokenRegistrationFailed(SinchError sinchError) {

    }

    /*@Override
    public void tokenRegistered() {
        mPushTokenIsRegistered = true;

    }

    @Override
    public void tokenRegistrationFailed(SinchError sinchError) {
        mPushTokenIsRegistered = false;
        Toast.makeText(this, "Connection failed to Video server - incoming calls can't be received!", Toast.LENGTH_LONG).show();
    }*/
}
