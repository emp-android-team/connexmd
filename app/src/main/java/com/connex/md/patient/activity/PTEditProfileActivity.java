package com.connex.md.patient.activity;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;

import com.connex.md.R;
import com.connex.md.interfaces.AsyncTaskListner;
import com.connex.md.model.Symptoms;
import com.connex.md.model.UserProfile;
import com.connex.md.patient.fragment.AllergiesFragment;
import com.connex.md.patient.fragment.BasicFragment;
import com.connex.md.patient.fragment.MedicalReportsFragment;
import com.connex.md.patient.fragment.MedicinesNewFragment;
import com.connex.md.patient.fragment.SymptomsFragment;
import com.connex.md.ws.CallRequest;
import com.connex.md.ws.Constant;
import com.connex.md.ws.JsonParserUniversal;
import com.connex.md.ws.MyConstants;
import com.connex.md.ws.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PTEditProfileActivity extends AppCompatActivity implements AsyncTaskListner{

    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    private TabLayout tabLayout;
    private ViewPager viewPager;
    UserProfile userProfile;
    public JsonParserUniversal jParser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dr_edit_profile);

        ButterKnife.bind(this);

        setupToolbar();

        jParser = new JsonParserUniversal();

        viewPager = findViewById(R.id.viewpager);
        tabLayout = findViewById(R.id.tabs);

        setupViewPager(viewPager);
        tabLayout.setupWithViewPager(viewPager);

        getSymptoms();
    }


    private void setupToolbar() {
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle("Edit Profile");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }


    private void getSymptoms() {
        new CallRequest(this).getSymptoms();
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new BasicFragment(), "Basic");
        adapter.addFragment(new AllergiesFragment(), "Allergies");
        adapter.addFragment(new MedicinesNewFragment(), "Medication");
        adapter.addFragment(new MedicalReportsFragment(), "Medical Reports");
        adapter.addFragment(new SymptomsFragment(), "Symptoms");

        viewPager.setAdapter(adapter);
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        Log.i("", "RESULT : " + result);
        Utils.removeSimpleSpinProgressDialog();
        if (result != null && !result.isEmpty()) {
            try {

                switch (request) {
                    case getSymptoms:
                        JSONObject jObj = new JSONObject(result);

                        if (jObj.getString("error_code").equalsIgnoreCase("0")) {
                            JSONArray resultArray = jObj.getJSONArray("result");

                            if (resultArray != null && resultArray.length() > 0) {
                                int length = resultArray.length();
                                MyConstants.symptomsArray = new ArrayList<ArrayList<Symptoms>>();
                                for (int i = 0; i < resultArray.length(); i++) {
                                    if (resultArray.getJSONArray(i).length() > 0) {
                                        Symptoms sBean;
                                        ArrayList<Symptoms> sArray = new ArrayList<Symptoms>();
                                        for (int j = 0; j < resultArray.getJSONArray(i).length(); j++) {
                                            sBean = (Symptoms) jParser.parseJson(resultArray.getJSONArray(i).getJSONObject(j), new Symptoms());
                                            sArray.add(sBean);

                                        }
                                        MyConstants.symptomsArray.add(sArray);
                                    }
                                }
                            }


                        } else {
                            Utils.showToast("Please try again later", this);
                        }
                        break;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            Utils.showToast("Please try again later", this);
        }
    }

    @Override
    public void onProgressUpdate(String uniqueMessageId, int progres) {

    }

    @Override
    public void onProgressComplete(String uniqueMessageId, String result, Constant.REQUESTS request) {

    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public int getItemPosition(Object object) {
            // POSITION_NONE makes it possible to reload the PagerAdapter
            return POSITION_NONE;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        MyConstants.isBackPressed = true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
