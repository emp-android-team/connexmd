package com.connex.md.patient.activity;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.KeyguardManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Build;
import android.os.Bundle;
import android.security.keystore.KeyGenParameterSpec;
import android.security.keystore.KeyPermanentlyInvalidatedException;
import android.security.keystore.KeyProperties;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.connex.md.R;
import com.connex.md.activity.ResetPasswordActivity;
import com.connex.md.activity.TermsConditionActivity;
import com.connex.md.audio_video_calling.BaseActivity;
import com.connex.md.audio_video_calling.SinchService;
import com.connex.md.doctor.activity.DRLoginActivity;
import com.connex.md.interfaces.AsyncTaskListner;
import com.connex.md.others.App;
import com.connex.md.others.Internet;
import com.connex.md.utils.FingerprintHandler;
import com.connex.md.ws.CallRequest;
import com.connex.md.ws.Constant;
import com.connex.md.ws.MyConstants;
import com.connex.md.ws.Utils;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginBehavior;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.plus.People;
import com.google.android.gms.plus.Plus;
import com.google.android.gms.plus.model.people.Person;
import com.google.android.gms.tasks.Task;
import com.onesignal.OneSignal;
import com.sinch.android.rtc.PushTokenRegistrationCallback;
import com.sinch.android.rtc.SinchError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;

import io.fabric.sdk.android.Fabric;

public class PTLoginActivity extends BaseActivity implements GoogleApiClient.OnConnectionFailedListener, AsyncTaskListner,
        SinchService.StartFailedListener, View.OnClickListener, PushTokenRegistrationCallback {

    // for Google login
    private static final String TAG = PTLoginActivity.class.getSimpleName();
    private static final int SIGN_IN = 1;
    // Variable used for storing the key in the Android Keystore container
    private static final String KEY_NAME = "ConnexMd";

    private static CallbackManager callbackManager;
    private App app;
    private LinearLayout btnFB, btnGoogle;
    private RelativeLayout root;
    private EditText etEmail, etPassword;
    private Button btnLogin;
    private PTLoginActivity instance;
    private TextView tvForgotPassword, tvCreateAccount, tvClickHereDoctors, tvTerms;
    private CheckBox cbRememberMe;
    private GoogleApiClient mGoogleApiClient;

    private KeyStore keyStore;
    private Cipher cipher;

    public String email = "", first_name = "", last_name = "", gender = "", name = "", fb_picture = "", fb_id = "", birthday = "", social_id, login_type;
    private int is_logged_in;
    private boolean isRememberMe = false;

    private SharedPreferences sharedpreferences;


    public static void hideSoftKeyboard(Activity activity) {
        try {
            InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
            if (inputMethodManager != null) {
                inputMethodManager.hideSoftInputFromWindow(Objects.requireNonNull(activity.getCurrentFocus()).getWindowToken(), 0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(getApplication());
        callbackManager = CallbackManager.Factory.create();

        Fabric.with(this, new Crashlytics());

        setContentView(R.layout.activity_pt_login);

        instance = this;
        app = App.getInstance();
        App.user.setUser_Type(MyConstants.USER_PT);
        Bundle bundle = getIntent().getExtras();

        if (bundle != null) {
            is_logged_in = bundle.getInt("is_logged_in");
        }

        sharedpreferences = getSharedPreferences(MyConstants.PREF, MODE_PRIVATE);

        if (sharedpreferences.getBoolean(MyConstants.IS_LOGGED_IN, false)) {

            /*App.User*/
            Intent intent = new Intent(PTLoginActivity.this, PTDashboardActivity.class);
            startActivity(intent);
            ActivityCompat.finishAffinity(PTLoginActivity.this);
        }

        if (sharedpreferences.getString(MyConstants.USER_TYPE, "").equalsIgnoreCase(MyConstants.USER_PT)) {
            if (!TextUtils.isEmpty(sharedpreferences.getString(MyConstants.PT_PASSWORD, ""))) {
                if (sharedpreferences.getBoolean(MyConstants.FIRST_LOGIN, false)) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        System.out.println("fingerprint");
                        fingerprintLogin();
                    }
                }
            }
        }

        OneSignal.idsAvailable(new OneSignal.IdsAvailableHandler() {
            @Override
            public void idsAvailable(String userId, String registrationId) {
                if (userId != null)
                    MyConstants.DEVICE_ID = userId;
            }
        });

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .addApi(Plus.API)
                .build();

        etEmail = findViewById(R.id.etEmail);
        etPassword = findViewById(R.id.etPassword);
        btnLogin = findViewById(R.id.btnLogin);
        btnFB = findViewById(R.id.btnFB);
        btnGoogle = findViewById(R.id.btnGoogle);
        tvForgotPassword = findViewById(R.id.tvForgotPassword);
        tvCreateAccount = findViewById(R.id.tvCreateAccount);
        tvClickHereDoctors = findViewById(R.id.tvClickHere);
        tvTerms = findViewById(R.id.tvTerms);
        cbRememberMe = findViewById(R.id.cbRememberMe);
        root = findViewById(R.id.root);

        btnLogin.setOnClickListener(this);
        btnFB.setOnClickListener(this);
        btnGoogle.setOnClickListener(this);

        setupUI(root);

        if (sharedpreferences.getString(MyConstants.USER_TYPE, "").equalsIgnoreCase(MyConstants.USER_PT)) {
            if (!TextUtils.isEmpty(sharedpreferences.getString(MyConstants.PT_PASSWORD, ""))) {
                etEmail.setText(sharedpreferences.getString(MyConstants.USER_EMAIL, ""));
                etPassword.setText(sharedpreferences.getString(MyConstants.PT_PASSWORD, ""));
                cbRememberMe.setChecked(true);
            }
        }

        tvForgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(PTLoginActivity.this, ResetPasswordActivity.class);
                intent.putExtra("user_type", "patient");
                startActivity(intent);
            }
        });

        tvClickHereDoctors.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(PTLoginActivity.this, DRLoginActivity.class);
                startActivity(intent);
            }
        });

        tvCreateAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(PTLoginActivity.this, PTSignUpActivity.class);
                startActivity(intent);
            }
        });

        tvTerms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(PTLoginActivity.this, TermsConditionActivity.class);
                startActivity(intent);
            }
        });

        /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(new String[]{Manifest.permission.RECORD_AUDIO, Manifest.permission.CAMERA, Manifest.permission.ACCESS_NETWORK_STATE, Manifest.permission.READ_PHONE_STATE}, 100);
        }*/

        /*btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                doLogin();
            }
        });*/

        /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            fingerprintLogin();
        }*/


        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        System.out.println("on Success");
                        setFbResultData(loginResult);
                    }

                    @Override
                    public void onCancel() {
                        System.out.println("on Cancel");
                        /*com.facebook.Profile profile = com.facebook.Profile.getCurrentProfile();
                        if (profile != null) {
                            // user has logged in
                            System.out.println("profile::" + profile);
                            LoginManager.getInstance().logOut();
                            //finish();
                            finish();
                        }*/
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        System.out.println("on Error");
                        System.out.println("FB error:::" + exception.getMessage());
                    }
                });
    }

    public void setupUI(View view) {
        if (!(view instanceof EditText)) {
            view.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    hideSoftKeyboard(PTLoginActivity.this);
                    return false;
                }
            });
        }

        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                View innerView = ((ViewGroup) view).getChildAt(i);
                setupUI(innerView);
            }
        }
    }

    @Override
    protected void onServiceConnected() {
        getSinchServiceInterface().setStartListener(this);
        Log.i("TAG", "Service Connected:");
        btnLogin.setOnClickListener(this);
        btnFB.setOnClickListener(this);
        btnGoogle.setOnClickListener(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }

    private void doLogin() {
        String email = etEmail.getText().toString().trim();
        String password = etPassword.getText().toString().trim();
        isRememberMe = cbRememberMe.isChecked();

        if (TextUtils.isEmpty(email)) {
            etEmail.requestFocus();
            etEmail.setError("Email can't be empty");

        } else if (TextUtils.isEmpty(password)) {
            etPassword.requestFocus();
            etPassword.setError("Password can't be empty");

        } else if (!Utils.isValidEmail(email)) {
            etEmail.requestFocus();
            etEmail.setError("Email is not valid");

        } else if (password.length() < 6) {
            etPassword.requestFocus();
            etPassword.setError("Password must be at least 6 characters");

        } else {
            if (!Internet.isAvailable(PTLoginActivity.this)) {
                Internet.showAlertDialog(PTLoginActivity.this, "Error!", "No Internet Connection", false);

            } else {
                social_id = "0";
                login_type = "0";
                new CallRequest(PTLoginActivity.this).patientLogin(email, password);
            }
        }
    }

    public void fingerprintLoginCall() {
        String email = sharedpreferences.getString(MyConstants.USER_EMAIL, "");
        String password = sharedpreferences.getString(MyConstants.PT_PASSWORD, "");
        isRememberMe = cbRememberMe.isChecked();

        social_id = "0";
        login_type = "0";

        new CallRequest(PTLoginActivity.this).patientLogin(email, password);
    }


    private void fingerprintLogin() {
        KeyguardManager keyguardManager = (KeyguardManager) getSystemService(KEYGUARD_SERVICE);
        FingerprintManager fingerprintManager = null;

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            fingerprintManager = (FingerprintManager) getSystemService(FINGERPRINT_SERVICE);
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (fingerprintManager != null) {
                if (!fingerprintManager.isHardwareDetected()) {
                    Toast.makeText(instance, "Your Device does not have a Fingerprint Sensor", Toast.LENGTH_SHORT).show();

                } else {
                    if (ActivityCompat.checkSelfPermission(instance, Manifest.permission.USE_FINGERPRINT) != PackageManager.PERMISSION_GRANTED) {
                        Toast.makeText(instance, "Fingerprint authentication permission not enabled", Toast.LENGTH_SHORT).show();

                    } else {
                        if (!fingerprintManager.hasEnrolledFingerprints()) {
                            Toast.makeText(instance, "Register at least one fingerprint in Settings", Toast.LENGTH_SHORT).show();

                        } else {
                            if (!keyguardManager.isKeyguardSecure()) {
                                Toast.makeText(instance, "Lock screen security not enabled in Settings", Toast.LENGTH_SHORT).show();

                            } else {
                                generateKey();
                                if (cipherInit()) {
                                    FingerprintManager.CryptoObject cryptoObject = new FingerprintManager.CryptoObject(cipher);
                                    FingerprintHandler helper = new FingerprintHandler(PTLoginActivity.this, MyConstants.USER_PT);
                                    helper.startAuth(fingerprintManager, cryptoObject);
                                }
                            }
                        }
                    }
                }
            }
        }
    }


    @TargetApi(Build.VERSION_CODES.M)
    protected void generateKey() {
        try {
            keyStore = KeyStore.getInstance("AndroidKeyStore");
        } catch (Exception e) {
            e.printStackTrace();
        }

        KeyGenerator keyGenerator;
        try {
            keyGenerator = KeyGenerator.getInstance(KeyProperties.KEY_ALGORITHM_AES, "AndroidKeyStore");

        } catch (NoSuchAlgorithmException | NoSuchProviderException e) {
            throw new RuntimeException("Failed to get KeyGenerator instance", e);
        }

        try {
            keyStore.load(null);
            keyGenerator.init(new
                    KeyGenParameterSpec.Builder(KEY_NAME,KeyProperties.PURPOSE_ENCRYPT | KeyProperties.PURPOSE_DECRYPT)
                    .setBlockModes(KeyProperties.BLOCK_MODE_CBC)
                    .setUserAuthenticationRequired(true)
                    .setEncryptionPaddings(KeyProperties.ENCRYPTION_PADDING_PKCS7)
                    .build());
            keyGenerator.generateKey();

        } catch (NoSuchAlgorithmException |
                InvalidAlgorithmParameterException
                | CertificateException | IOException e) {
            throw new RuntimeException(e);
        }
    }


    @TargetApi(Build.VERSION_CODES.M)
    public boolean cipherInit() {
        try {
            cipher = Cipher.getInstance(KeyProperties.KEY_ALGORITHM_AES + "/" + KeyProperties.BLOCK_MODE_CBC + "/" + KeyProperties.ENCRYPTION_PADDING_PKCS7);

        } catch (NoSuchAlgorithmException | NoSuchPaddingException e) {
            throw new RuntimeException("Failed to get Cipher", e);
        }

        try {
            keyStore.load(null);
            SecretKey key = (SecretKey) keyStore.getKey(KEY_NAME, null);
            cipher.init(Cipher.ENCRYPT_MODE, key);
            return true;

        } catch (KeyPermanentlyInvalidatedException e) {
            return false;

        } catch (KeyStoreException | CertificateException | UnrecoverableKeyException | IOException | NoSuchAlgorithmException | InvalidKeyException e) {
            throw new RuntimeException("Failed to init Cipher", e);
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        callbackManager.onActivityResult(requestCode, resultCode, data);

        if (requestCode == SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult1(task);

        } else {
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void handleSignInResult1(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);
            handleGoogleSignInResult(account);

        } catch (ApiException e) {
            handleGoogleSignInResult(null);
        }
    }


    public void handleGoogleSignInResult( GoogleSignInAccount account) {
        if (account != null) {
            String email = account.getEmail();
            first_name = account.getDisplayName();
            last_name = account.getFamilyName();
            social_id = account.getId();
            String profilePictureUrl = account.getPhotoUrl() == null ? "" : account.getPhotoUrl().toString();

            System.out.println("Photo URL Sagar --> " + profilePictureUrl);

            gender = "Male";

            Map<String, String> map = new HashMap<String, String>();
            map.put("url", MyConstants.BASE_URL + "register_social");
            map.put("email", email);
            map.put("SocialId", social_id);
            map.put("FirstName", first_name);
            map.put("LastName", last_name);
            map.put("Phone", "");
            map.put("ProfilePic", profilePictureUrl);
            map.put("CoverPic", "");
            map.put("BirthDate", "");
            map.put("Gender", "");
            map.put("SocialType", "2");
            map.put("DeviceType", MyConstants.DEVICE_TYPE);
            map.put("DeviceToken", MyConstants.DEVICE_ID);
            map.put("Version", MyConstants.WS_VERSION);

            login_type = "2";

            new CallRequest(instance).signupPatientFb(map);

        }else {
            Utils.showAlert("We found out that you restricted the access to your Google details. To log-in with Google on ConnexMD we’ll need you to change your settings. Thank you!", PTLoginActivity.this);
        }
    }



    private void handleSignInResult(GoogleSignInResult result) {
        try {
            Log.d(TAG, "handleSignInResult:" + result.isSuccess());
            if (result.isSuccess()) {
                // Signed in successfully, show authenticated UI.
                GoogleSignInAccount acct = result.getSignInAccount();

                if (acct != null) {
                    Log.e(TAG, "display name: " + acct.getDisplayName());

                    login_type = "2";

                    String personName = acct.getDisplayName();
                    String personPhotoUrl = String.valueOf(acct.getPhotoUrl());
                    if (!TextUtils.isEmpty(acct.getEmail())) {
                        email = acct.getEmail();

                        String id = acct.getId();

                        Log.e(TAG, "Name: " + personName + ", email: " + email
                                + ", Image: " + personPhotoUrl);

                        first_name = acct.getGivenName();
                        last_name = acct.getFamilyName();
                        social_id = id;



                        Plus.PeopleApi.load(mGoogleApiClient, acct.getId()).setResultCallback(new ResultCallback<People.LoadPeopleResult>() {
                            @Override
                            public void onResult(@NonNull People.LoadPeopleResult loadPeopleResult) {
                                Person person = loadPeopleResult.getPersonBuffer().get(0);

                                Log.d("GivenName ", person.getName().getGivenName());
                                Log.d("FamilyName ", person.getName().getFamilyName());
                                Log.d("DisplayName ", person.getDisplayName());
                                Log.d("gender ", String.valueOf(person.getGender())); //0 = male 1 = female
                                if (person.getGender() == 0) {
                                    gender = "Male";
                                } else {
                                    gender = "Female";
                                }
                                System.out.println("gender::" + gender);

                                if (person.hasBirthday()) {
                                    birthday = person.getBirthday();
                                    System.out.println("birthday::" + person.getBirthday());
                                }

                            }
                        });

                        System.out.println("first_name::" + acct.getGivenName());
                        System.out.println("last_name::" + acct.getFamilyName());

                        System.out.println("id:::" + id);

                        if (personPhotoUrl == null || "null".equals(personPhotoUrl)) {
                            personPhotoUrl = "";
                        }

                        System.out.println("profile pic:::::::**" + personPhotoUrl);

                        if (first_name == null || last_name == null) {
                            Utils.showAlert("We are unable to fetch information from Google. Please try again later", PTLoginActivity.this);
                            return;
                        }

                        Map<String, String> map = new HashMap<String, String>();
                        map.put("url", MyConstants.BASE_URL + "register_social");
                        map.put("email", email);
                        map.put("SocialId", id);
                        map.put("FirstName", first_name);
                        map.put("LastName", last_name);
                        map.put("Phone", "");
                        map.put("ProfilePic", personPhotoUrl);
                        map.put("CoverPic", "");
                        map.put("BirthDate", "");
                        map.put("Gender", "");
                        // 2 for Google
                        map.put("SocialType", "2");
                        map.put("DeviceType", MyConstants.DEVICE_TYPE);
                        map.put("DeviceToken", MyConstants.DEVICE_ID);
                        map.put("Version", MyConstants.WS_VERSION);

                        new CallRequest(instance).signupPatientFb(map);

                    } else {
                        Utils.showAlert("We found out that you restricted the access to your Google details. To log-in with Google on ConnexMD we’ll need you to change your settings. Thank you!", PTLoginActivity.this);
                    }
                }

                if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
                    Auth.GoogleSignInApi.signOut(mGoogleApiClient);
                }

            } else {
                // Signed out, show unauthenticated UI.

            }
        } catch (Exception e){
            e.printStackTrace();
        }
    }


    private void setFbResultData(LoginResult loginResult) {
        GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        Log.v("HomeActivity", response.toString());

                        try {
                            fb_id = object.getString("id");
                            social_id = fb_id;

                            if (object.has("email")) {
                                email = object.getString("email");

                                first_name = object.getString("first_name");
                                last_name = object.getString("last_name");

                                if (object.has("birthday")) {
                                    birthday = object.getString("birthday");
                                }

                                if (object.has("gender")) {
                                    gender = object.getString("gender");
                                }
                                fb_picture = object.getJSONObject("picture").getJSONObject("data").getString("url");
                                //fb_picture = fb_picture.replaceAll("&", "********");

                                if (email == null || TextUtils.isEmpty(email)) {
                                    email = "";
                                }

                                login_type = "1";

                                System.out.println("fb_id:::" + fb_id);
                                System.out.println("email:::" + email);

                                Map<String, String> map = new HashMap<String, String>();
                                map.put("url", MyConstants.BASE_URL + "register_social");
                                map.put("email", email);
                                map.put("SocialId", fb_id);
                                map.put("FirstName", first_name);
                                map.put("LastName", last_name);
                                map.put("Phone", "");
                                map.put("ProfilePic", fb_picture);
                                map.put("CoverPic", "");
                                map.put("BirthDate", birthday);
                                map.put("Gender", gender);
                                // 1 for FB
                                map.put("SocialType", "1");
                                map.put("DeviceType", MyConstants.DEVICE_TYPE);
                                map.put("DeviceToken", MyConstants.DEVICE_ID);
                                map.put("Version", MyConstants.WS_VERSION);

                                new CallRequest(instance).signupPatientFb(map);

                            } else {
                                Utils.showAlert("We found out that you restricted the access to your Facebook details. To log-in with Facebook on ConnexMD we’ll need you to change your settings. Thank you!", PTLoginActivity.this);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "timezone,locale,id,first_name,last_name,name,gender,email,birthday,picture.type(large),location,hometown");

        request.setParameters(parameters);
        request.executeAsync();
    }


    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }


    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        try {
            Utils.hideProgressDialog();
            if (result != null && !result.isEmpty()) {
                Log.i("TAG", "TAG Result : " + result);

                switch (request) {
                    case singup_patient_fb:
                        try {
                            JSONObject object = new JSONObject(result);
                            String error_code = object.getString("error_code");

                            if (error_code.equalsIgnoreCase("0")) {
                                JSONObject resultObj = object.getJSONObject("result");
                                setLoginData(resultObj);

                            } else if (error_code.equalsIgnoreCase("5")) {
                                String error_string = object.getString("error_string");
                                Toast.makeText(instance, "Something went wrong, Please try again.", Toast.LENGTH_SHORT).show();
                                OneSignal.idsAvailable(new OneSignal.IdsAvailableHandler() {

                                    @Override
                                    public void idsAvailable(String userId, String registrationId) {
                                        if (userId != null)
                                            MyConstants.DEVICE_ID = userId;

                                        Log.d("debug", "User:" + userId);
                                        Log.d("debug", "registrationId:" + registrationId);
                                    }
                                });
                            } else {
                                String error_string = object.getString("error_string");
                                Toast.makeText(instance, error_string, Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;

                    case patientLogin:
                        try {
                            JSONObject object = new JSONObject(result);
                            String error_code = object.getString("error_code");

                            if (error_code.equalsIgnoreCase("0")) {
                                JSONObject resultObj = object.getJSONObject("result");
                                setLoginData(resultObj);

                            } else if (error_code.equalsIgnoreCase("5")) {
                                JSONObject resultObj = object.getJSONObject("result");
                                JSONArray arrayUser = resultObj.getJSONArray("user");
                                JSONObject obj = arrayUser.getJSONObject(0);

                                String id = obj.getString("id");

                                String error_string = object.getString("error_string");
                                Toast.makeText(instance, error_string, Toast.LENGTH_SHORT).show();

                            } else {
                                String error_string = object.getString("error_string");
                                Toast.makeText(instance, error_string, Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;
                }
            } else {
                Utils.showToast("Please try again later", this);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void setLoginData(JSONObject obj) {
        try {
            JSONArray userArray = obj.getJSONArray("user");
            JSONObject user_Object = userArray.getJSONObject(0);
            String user_id = user_Object.getString("id");
            String email = user_Object.getString("email");
            String ApiToken = user_Object.getString("ApiToken");
            String Latitude = user_Object.getString("Latitude");
            String Longitude = user_Object.getString("Longitude");
            String ReferCode = user_Object.getString("ReferCode");
            String StripeId = user_Object.getString("StripeId");

            JSONArray user_detailArray = obj.getJSONArray("user_detail");
            JSONObject user_detail_object = user_detailArray.getJSONObject(0);
            String FirstName = user_detail_object.getString("FirstName");
            String LastName = user_detail_object.getString("LastName");
            String Gender = user_detail_object.getString("Gender");
            String CountryId = user_detail_object.getString("CountryId");
            String StateId = user_detail_object.getString("StateId");
            String CountryCode = user_detail_object.getString("CountryCode");
            String Country = user_detail_object.getString("Country");
            String State = user_detail_object.getString("State");
            String Phone = user_detail_object.getString("Phone");
            String BirthDate = user_detail_object.getString("BirthDate");
            String Age = user_detail_object.getString("Age");
            String Address = user_detail_object.getString("Address");
            String Timezone = user_detail_object.getString("Timezone");
            String ProfilePic = user_detail_object.getString("ProfilePic");
            //String ProfilePicSocial = user_detail_object.getString("ProfilePicSocial");
            String CoverPic = user_detail_object.getString("CoverPic");
            String CurrentDiscount = user_detail_object.getString("CurrentDiscount");
            String WalletBalance = user_detail_object.getString("WalletBalance");

            name = FirstName + " " + LastName;

            SharedPreferences sharedpreferences = getSharedPreferences(MyConstants.PREF, Context.MODE_PRIVATE);

            SharedPreferences.Editor editor = sharedpreferences.edit();

            editor.putString(MyConstants.USER_TYPE, MyConstants.USER_PT);
            editor.putString(MyConstants.USER_EMAIL, email);
            editor.putString(MyConstants.USER_ID, user_id);
            editor.putString(MyConstants.PROFILE_PIC, ProfilePic);
            editor.putString(MyConstants.PT_ZONE, Timezone);
            editor.putString(MyConstants.PT_NAME, name);
            editor.putString(MyConstants.PT_FIRST_NAME, FirstName);
            editor.putString(MyConstants.PT_LAST_NAME, LastName);
            editor.putString(MyConstants.GENDER, Gender);
            editor.putString(MyConstants.DATE_OF_BIRTH, BirthDate);
            editor.putString(MyConstants.MOBILE, Phone);
            editor.putString(MyConstants.COUNTRY, Country);
            editor.putString(MyConstants.ADDRESS, Address);
            editor.putString(MyConstants.AGE, Age);
            editor.putString(MyConstants.CURRENT_DISCOUNT, CurrentDiscount);
            editor.putString(MyConstants.WALLET_BALANCE, WalletBalance);
            editor.putString(MyConstants.REFER_CODE, ReferCode);
            editor.putString(MyConstants.TOKEN, ApiToken);
            editor.putString(MyConstants.SINCH_ID, "pppp" + user_id);
            editor.putString(MyConstants.STRIPE_ID, StripeId);
            editor.putBoolean(MyConstants.FIRST_LOGIN, true);
            editor.putBoolean(MyConstants.IS_LOGGED_IN, true);
            editor.putBoolean(MyConstants.IS_GUEST, false);
            editor.putString(MyConstants.LOGIN_TYPE, login_type);
            editor.putString(MyConstants.SOCIAL_ID, social_id);

            if (isRememberMe) {
                editor.putString(MyConstants.PT_PASSWORD, etPassword.getText().toString());
            } else {
                editor.putString(MyConstants.PT_PASSWORD, "");
            }

            editor.commit();

            App.user.setUserID(user_id);
            App.user.setName(name);
            App.user.setFirstName(FirstName);
            App.user.setLastName(LastName);
            App.user.setUserEmail(email);
            App.user.setPhone(Phone);
            App.user.setUser_Type(MyConstants.USER_PT);
            App.user.setProfileUrl(sharedpreferences.getString(MyConstants.PROFILE_PIC, ""));
            App.user.setSinch_id(sharedpreferences.getString(MyConstants.SINCH_ID, ""));
            App.user.setLoginType(login_type);
            MyConstants.API_TOKEN = ApiToken;
            MyConstants.isGuest = "0";
            System.out.println("token::" + MyConstants.API_TOKEN);

            Log.i(" SINCH USER ID :", App.user.getSinch_id());
            getSinchServiceInterface().startClient(App.user.getSinch_id());
            getSinchServiceInterface().registerPushToken(this);

            System.out.println("data saved successfully");

            // reload all data
            MyConstants.specialitiesList.clear();
            MyConstants.isLoadAgain = true;
            MyConstants.bookingHistoryList.clear();
            MyConstants.messageHistoryList.clear();
            MyConstants.userChatList.clear();
            MyConstants.doctorChatList.clear();
            MyConstants.isBookingHistoryLoad = true;
            MyConstants.isMessageHistoryLoad = true;
            MyConstants.userProfile = null;
            MyConstants.isProfileLoad = true;
            MyConstants.doctorProfilePersonal = null;
            MyConstants.isDoctorProfileLoad = true;

            if (is_logged_in == 0) {
                Intent intent = new Intent(PTLoginActivity.this, PTDashboardActivity.class);
                startActivity(intent);
                ActivityCompat.finishAffinity(PTLoginActivity.this);

            } else if (is_logged_in == 1) {
                if (MyConstants.doctorProfile.getDoctorId().equalsIgnoreCase(MyConstants.doctorIdForQuestionnaire)) {
                    Intent intent = new Intent(PTLoginActivity.this, DRQuestionnaire1Activity.class);
                    startActivity(intent);
                    finish();
                } else {
                    Intent intent = new Intent(PTLoginActivity.this, ConfirmEConsultBookingActivity.class);
                    startActivity(intent);
                    finish();
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
            Utils.showToast("Something went wrong", PTLoginActivity.this);
        }
    }

    @Override
    public void onProgressUpdate(String uniqueMessageId, int progres) {

    }

    @Override
    public void onProgressComplete(String uniqueMessageId, String result, Constant.REQUESTS request) {

    }

    @Override
    public void onStartFailed(SinchError error) {

    }

    @Override
    public void onStarted() {

    }


    @Override
    public void onClick(View view) {
        if (view == btnLogin) {
            doLogin();

        } else if (view == btnFB) {
            LoginManager.getInstance().logOut();
            LoginManager.getInstance().setLoginBehavior(LoginBehavior.NATIVE_WITH_FALLBACK);
            LoginManager.getInstance().logInWithReadPermissions(PTLoginActivity.this, Arrays.asList("public_profile", "email", "user_birthday", "user_location", "user_hometown"));

        } else if (view == btnGoogle) {
            Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
            startActivityForResult(signInIntent, SIGN_IN);
        }
    }

    @Override
    public void tokenRegistered() {

    }

    @Override
    public void tokenRegistrationFailed(SinchError sinchError) {

    }
}