package com.connex.md.patient.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.connex.md.R;
import com.connex.md.activity.TermsConditionActivity;
import com.connex.md.interfaces.AsyncTaskListner;
import com.connex.md.others.App;
import com.connex.md.others.Internet;
import com.connex.md.ws.CallRequest;
import com.connex.md.ws.Constant;
import com.connex.md.ws.MyConstants;
import com.connex.md.ws.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.connex.md.audio_video_calling.BaseActivity.getSinchServiceInterface;

public class PTSignUpActivity extends AppCompatActivity implements AsyncTaskListner {

    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    Button btnCreateAccount;
    EditText etEmail, etFirstName, etLastName, etPassword, etConfirmPassword, etPhoneNumber;
    TextView tvLogin, tvTerms;

    String social_id = "0";
    String completeProfile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ptsign_up);

        ButterKnife.bind(this);

        setupToolbar();

        btnCreateAccount = findViewById(R.id.btnCreateAccount);
        etEmail = findViewById(R.id.etEmail);
        etFirstName = findViewById(R.id.etFirstName);
        etLastName = findViewById(R.id.etLastName);
        etPassword = findViewById(R.id.etPassword);
        etConfirmPassword = findViewById(R.id.etConfirmPassword);
        etPhoneNumber = findViewById(R.id.etPhoneNumber);
        tvLogin = findViewById(R.id.tvLogin);
        tvTerms = findViewById(R.id.tvTerms);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            completeProfile = bundle.getString("completeProfile");

            etFirstName.setText(App.user.getFirstName());
            etLastName.setText(App.user.getLastName());
            etEmail.requestFocus();
        }

        etFirstName.requestFocus();
        btnCreateAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createAccount();
            }
        });
        tvLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(PTSignUpActivity.this, PTLoginActivity.class);
                startActivity(intent);
                finish();
            }
        });

        tvTerms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(PTSignUpActivity.this, TermsConditionActivity.class);
                startActivity(intent);
            }
        });

        etEmail.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_NEXT) {
                    etPhoneNumber.requestFocus();

                    Intent intent = new Intent(PTSignUpActivity.this, PhoneAuthActivity.class);
                    intent.putExtra("profileUpdate", "1");
                    startActivityForResult(intent, 11);

                    return true;
                }
                return false;
            }
        });


        etPhoneNumber.setInputType(InputType.TYPE_NULL);
        //etPhoneNumber.setText("+919876543210");
        etPhoneNumber.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                etPhoneNumber.requestFocus();

                Intent intent = new Intent(PTSignUpActivity.this, PhoneAuthActivity.class);
                intent.putExtra("profileUpdate", "1");
                startActivityForResult(intent, 11);
            }
        });
    }


    private void setupToolbar() {
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle("Sign Up");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        try {
            if (requestCode == 11) {
                if (data != null) {
                    String phone_number = data.getStringExtra("phone_number");
                    System.out.println("phone_number" + phone_number);
                    etPhoneNumber.setText(phone_number);
                    etPhoneNumber.setError(null);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void createAccount() {
        String email = etEmail.getText().toString().trim();
        String first_name = etFirstName.getText().toString().trim();
        String last_name = etLastName.getText().toString().trim();
        String password = etPassword.getText().toString().trim();
        String confirm_password = etConfirmPassword.getText().toString().trim();
        String phone_number = etPhoneNumber.getText().toString().trim();

        if (TextUtils.isEmpty(first_name)) {
            etFirstName.requestFocus();
            etFirstName.setError("First name can't be empty");
        } else if (!first_name.matches("^[A-Za-z]+$")) {
            etFirstName.requestFocus();
            etFirstName.setError("First name contains only characters");
        } else if (TextUtils.isEmpty(last_name)) {
            etLastName.requestFocus();
            etLastName.setError("Last name can't be empty");
        } else if (!last_name.matches("^[A-Za-z]+$")) {
            etLastName.requestFocus();
            etLastName.setError("Last name contains only characters");
        } else if (TextUtils.isEmpty(email)) {
            etEmail.requestFocus();
            etEmail.setError("Email can't be empty");
        } else if (!Utils.isValidEmail(email)) {
            etEmail.requestFocus();
            etEmail.setError("Email is not valid");
        } else if (TextUtils.isEmpty(phone_number)) {
            etPhoneNumber.requestFocus();
            etPhoneNumber.setError("Phone number can't be empty");
        } else if (TextUtils.isEmpty(password)) {
            etPassword.requestFocus();
            etPassword.setError("Password can't be empty");
        } else if (password.length() < 6) {
            etPassword.requestFocus();
            etPassword.setError("Password requires minimum 6 characters");
        } else if (TextUtils.isEmpty(confirm_password)) {
            etConfirmPassword.requestFocus();
            etConfirmPassword.setError("Confirm password can't be empty");
        } else if (!password.equalsIgnoreCase(confirm_password)) {
            etConfirmPassword.requestFocus();
            etConfirmPassword.setError("Password does not match the confirm password");
        } else {
            if (!Internet.isAvailable(PTSignUpActivity.this)) {
                Internet.showAlertDialog(PTSignUpActivity.this, "Error!", "No Internet Connection", false);
            } else {

                Map<String, String> map = new HashMap<String, String>();
                map.put("url", MyConstants.BASE_URL + "register");
                map.put("email", email);
                map.put("FirstName", first_name);
                map.put("LastName", last_name);
                map.put("Password", password);
                map.put("Phone", phone_number);
                map.put("DeviceType", MyConstants.DEVICE_TYPE);
                map.put("DeviceToken", MyConstants.DEVICE_ID);
                map.put("Version", MyConstants.WS_VERSION);

                new CallRequest(PTSignUpActivity.this).registerPatient(map);
            }
        }
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        Utils.removeSimpleSpinProgressDialog();
        try {
            if (result != null && !result.isEmpty()) {
                Log.i("TAG", "TAG Result : " + result);
                //      {"document":{"response":{"status":1,"message":"Success."}}}
                switch (request) {
                    case register:
                        try {
                            JSONObject mainObj = new JSONObject(result);
                            if (mainObj.getString("error_code").equalsIgnoreCase("0")) {
                                //Utils.showToast(mainObj.getString("error_string"), PTSignUpActivity.this);

                                JSONObject resultObj = mainObj.getJSONObject("result");

                               /* Intent intent = new Intent(PTSignUpActivity.this, VerifyAccountOTPActivity.class);
                                intent.putExtra("user_data", resultObj.toString());
                                startActivity(intent);
                                finish();*/
                                setUserData(resultObj);


                            } else {
                                Utils.showToast(mainObj.getString("error_string"), PTSignUpActivity.this);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setUserData(JSONObject obj) {
        try {
            JSONArray userArray = obj.getJSONArray("user");
            JSONObject user_Object = userArray.getJSONObject(0);
            String user_id = user_Object.getString("id");
            String email = user_Object.getString("email");
            String ApiToken = user_Object.getString("ApiToken");
            String Latitude = user_Object.getString("Latitude");
            String Longitude = user_Object.getString("Longitude");
            String ReferCode = user_Object.getString("ReferCode");
            String StripeId = user_Object.getString("StripeId");

            JSONArray user_detailArray = obj.getJSONArray("user_detail");
            JSONObject user_detail_object = user_detailArray.getJSONObject(0);
            String FirstName = user_detail_object.getString("FirstName");
            String LastName = user_detail_object.getString("LastName");
            String Gender = user_detail_object.getString("Gender");
            String CountryId = user_detail_object.getString("CountryId");
            String StateId = user_detail_object.getString("StateId");
            String CountryCode = user_detail_object.getString("CountryCode");
            String Country = user_detail_object.getString("Country");
            String State = user_detail_object.getString("State");
            String Phone = user_detail_object.getString("Phone");
            String BirthDate = user_detail_object.getString("BirthDate");
            String Age = user_detail_object.getString("Age");
            String Address = user_detail_object.getString("Address");
            String Timezone = user_detail_object.getString("Timezone");
            String ProfilePic = user_detail_object.getString("ProfilePic");
            //String ProfilePicSocial = user_detail_object.getString("ProfilePicSocial");
            String CoverPic = user_detail_object.getString("CoverPic");
            String CurrentDiscount = user_detail_object.getString("CurrentDiscount");
            String WalletBalance = user_detail_object.getString("WalletBalance");

            JSONArray user_device = obj.getJSONArray("user_device");

            String name = FirstName + " " + LastName;

            SharedPreferences sharedpreferences = getSharedPreferences(MyConstants.PREF, Context.MODE_PRIVATE);

            SharedPreferences.Editor editor = sharedpreferences.edit();

            editor.putString(MyConstants.USER_TYPE, MyConstants.USER_PT);
            editor.putString(MyConstants.USER_EMAIL, email);
            editor.putString(MyConstants.USER_ID, user_id);
            editor.putString(MyConstants.PROFILE_PIC, ProfilePic);
            editor.putString(MyConstants.PT_ZONE, Timezone);
            editor.putString(MyConstants.PT_NAME, name);
            editor.putString(MyConstants.PT_FIRST_NAME, FirstName);
            editor.putString(MyConstants.PT_LAST_NAME, LastName);
            editor.putString(MyConstants.GENDER, Gender);
            editor.putString(MyConstants.DATE_OF_BIRTH, BirthDate);
            editor.putString(MyConstants.MOBILE, Phone);
            editor.putString(MyConstants.COUNTRY, Country);
            editor.putString(MyConstants.ADDRESS, Address);
            editor.putString(MyConstants.AGE, Age);
            editor.putString(MyConstants.CURRENT_DISCOUNT, CurrentDiscount);
            editor.putString(MyConstants.WALLET_BALANCE, WalletBalance);
            editor.putString(MyConstants.REFER_CODE, ReferCode);
            editor.putString(MyConstants.TOKEN, ApiToken);
            editor.putString(MyConstants.SINCH_ID, "pppp" + user_id);
            editor.putString(MyConstants.STRIPE_ID, StripeId);
            editor.putBoolean(MyConstants.FIRST_LOGIN, true);
            editor.putBoolean(MyConstants.IS_LOGGED_IN, true);
            editor.putString(MyConstants.LOGIN_TYPE, "0");
            editor.putString(MyConstants.SOCIAL_ID, social_id);
            editor.putBoolean(MyConstants.IS_GUEST, false);

            editor.commit();

            App.user.setUserID(user_id);
            App.user.setName(name);
            App.user.setFirstName(FirstName);
            App.user.setLastName(LastName);
            App.user.setUserEmail(email);
            App.user.setPhone(Phone);
            App.user.setUser_Type(MyConstants.USER_PT);
            App.user.setProfileUrl(sharedpreferences.getString(MyConstants.PROFILE_PIC, ""));
            App.user.setSinch_id(sharedpreferences.getString(MyConstants.SINCH_ID, ""));
            App.user.setLoginType("0");
            MyConstants.API_TOKEN = ApiToken;
            MyConstants.isGuest = "0";
            System.out.println("token::" + MyConstants.API_TOKEN);
            getSinchServiceInterface().startClient(App.user.getSinch_id());

            System.out.println("data saved successfully");

            // reload all data
            MyConstants.specialitiesList.clear();
            MyConstants.isLoadAgain = true;
            MyConstants.bookingHistoryList.clear();
            MyConstants.messageHistoryList.clear();
            MyConstants.userChatList.clear();
            MyConstants.doctorChatList.clear();
            MyConstants.isBookingHistoryLoad = true;
            MyConstants.isMessageHistoryLoad = true;
            MyConstants.userProfile = null;
            MyConstants.isProfileLoad = true;
            MyConstants.doctorProfilePersonal = null;
            MyConstants.isDoctorProfileLoad = true;

            Intent intent = new Intent(PTSignUpActivity.this, SymptomsSelectionActivity.class);
            startActivity(intent);
            finish();

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onProgressUpdate(String uniqueMessageId, int progres) {

    }

    @Override
    public void onProgressComplete(String uniqueMessageId, String result, Constant.REQUESTS request) {

    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
