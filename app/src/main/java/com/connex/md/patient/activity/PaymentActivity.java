package com.connex.md.patient.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SwitchCompat;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.Html;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.connex.md.R;
import com.connex.md.firebase_chat.activity.FireChatActivity;
import com.connex.md.interfaces.AsyncTaskListner;
import com.connex.md.model.DoctorProfile;
import com.connex.md.others.App;
import com.connex.md.others.Internet;
import com.connex.md.patient.fragment.CardPaymentFragment;
import com.connex.md.ws.CallRequest;
import com.connex.md.ws.Constant;
import com.connex.md.ws.MyConstants;
import com.connex.md.ws.Utils;
import com.paypal.android.sdk.payments.PayPalAuthorization;
import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalFuturePaymentActivity;
import com.paypal.android.sdk.payments.PayPalItem;
import com.paypal.android.sdk.payments.PayPalPayment;

import com.paypal.android.sdk.payments.PayPalPaymentDetails;
import com.paypal.android.sdk.payments.PayPalProfileSharingActivity;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentConfirmation;
import com.stripe.android.Stripe;
import com.stripe.android.TokenCallback;
import com.stripe.android.model.Card;
import com.stripe.android.model.Token;
import com.stripe.exception.APIConnectionException;
import com.stripe.exception.APIException;
import com.stripe.exception.AuthenticationException;
import com.stripe.exception.CardException;
import com.stripe.exception.InvalidRequestException;
import com.stripe.model.Charge;
import com.stripe.model.Customer;
import com.stripe.model.ExternalAccount;

import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.connex.md.ws.MyConstants.STRIPE_CONFIG_ENVIRONMENT;
import static com.connex.md.ws.MyConstants.STRIPE_PUBLISH_KEY_LIVE;
import static com.connex.md.ws.MyConstants.STRIPE_PUBLISH_KEY_TEST;

public class PaymentActivity extends AppCompatActivity implements AsyncTaskListner {

    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    // for paypal
    private static final int REQUEST_CODE_PAYMENT = 1;
    private static final int REQUEST_CODE_FUTURE_PAYMENT = 2;
    private static final int REQUEST_CODE_PROFILE_SHARING = 3;
    private static final String CONFIG_CLIENT_ID = "AXkBxqsh-aV0PW0wvYBszG92RjzQGOGkYnERWMGZvGsAmbW-xrGilsPVkTSDD_RWZf6tyrikOX6l0W03";
    private static final String sandBox = "AXkBxqsh-aV0PW0wvYBszG92RjzQGOGkYnERWMGZvGsAmbW-xrGilsPVkTSDD_RWZf6tyrikOX6l0W03";
    private static final String live = "AXkBxqsh-aV0PW0wvYBszG92RjzQGOGkYnERWMGZvGsAmbW-xrGilsPVkTSDD_RWZf6tyrikOX6l0W03";
    //private static final String live = "AXORmeUOWK9QW0hw68V6iO7kZMKYpasXqZiG6PaiwMrDJTs9oSc13g5uyRdoNi7mDl77OAtI4FGbhZwx";
    private static String CONFIG_ENVIRONMENT;
    private static PayPalConfiguration config;
    public EditText etCardNumber, etCardHolderName, etMonth, etYear, etCVV;
    public SwitchCompat switchSave;
    public String TAG = "Connex MD Payment";
    //private TabLayout tabLayout;
    //private WrapContentViewPager viewPager;
    Button btnConfirmPay;
    TextView tvPaymentWith;
    String paymentWith;
    int total;
    String mRxFullName, mRxEmail, mRxInsuranceNo, mRxPrescriptionImage;
    SharedPreferences sharedpreferences;
    ImageView ivStripe, ivPaypal, ivAndroidPay;
    int tabIndex = 0;
    //private CardPaymentFragment cardPaymentFragment;
    String paymentType, paymentFor = "";
    // for stripe
    Card card;
    String TOKEN_ID, user_email = "Guest", customerId, charge_id, status;
    boolean saveCardForPayment, isFromInPersonVisit = false;
    String last4, exp_month, exp_year, name;
    String PaymentID;
    boolean isOfferUsed, isHeader = false;
    String OfferId = "", OfferType = "", DiscountAmount = "0", walletCharge = "", fullName = "", phoneNumber = "", age = "", email = "", message = "",
            mConsultTime, mTimeSlotId, mReasonForConsult, mPhoneNumberToReach, mDelivery;
    String stripe_id;

    private DoctorProfile mDoctorProfileData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);

        ButterKnife.bind(this);

        setupToolbar();

        if (MyConstants.STRIPE_CONFIG_ENVIRONMENT.equalsIgnoreCase("0")) {
            CONFIG_ENVIRONMENT = PayPalConfiguration.ENVIRONMENT_SANDBOX;
            config = new PayPalConfiguration()
                    .environment(CONFIG_ENVIRONMENT)
                    .clientId(sandBox)
                    .merchantName("Connex.MD")
                    .merchantPrivacyPolicyUri(Uri.parse("https://www.example.com/privacy"))
                    .merchantUserAgreementUri(Uri.parse("https://www.example.com/legal"));
        } else {
            CONFIG_ENVIRONMENT = PayPalConfiguration.ENVIRONMENT_PRODUCTION;
            config = new PayPalConfiguration()
                    .environment(CONFIG_ENVIRONMENT)
                    .clientId(live)
                    .merchantName("Connex.MD")
                    .merchantPrivacyPolicyUri(Uri.parse("https://www.example.com/privacy"))
                    .merchantUserAgreementUri(Uri.parse("https://www.example.com/legal"));
        }

        user_email = App.user.getUserEmail();

        sharedpreferences = getSharedPreferences(MyConstants.PREF, MODE_PRIVATE);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            if (bundle.containsKey("rxDelivery")) {
                total = bundle.getInt("totalAmount");
                mRxFullName = bundle.getString("fullName");
                mRxEmail = bundle.getString("email");
                mRxInsuranceNo = bundle.getString("privateInsurance");
                mRxPrescriptionImage = bundle.getString("prescriptionImage");
                mDelivery = bundle.getString("Delivery");

            }else if (bundle.containsKey("inPersonVisit")) {
                isFromInPersonVisit = true;
                total = 18;
                mDoctorProfileData = (DoctorProfile)getIntent().getExtras().get("DOCTOR_PROFILE");
                mConsultTime = getIntent().getStringExtra("ConsultTime");
                mTimeSlotId = getIntent().getStringExtra("TIME_SLOT_ID");
                mReasonForConsult = getIntent().getStringExtra("ReasonForConsult");
                mPhoneNumberToReach = getIntent().getStringExtra("PhoneNumberToReach");

            }else {
                total = bundle.getInt("finalAmount");
                walletCharge = bundle.getString("walletCharge");
                isOfferUsed = bundle.getBoolean("isOfferUsed");
                if (isOfferUsed) {
                    OfferId = String.valueOf(bundle.getInt("OfferId"));
                    OfferType = bundle.getString("OfferType");
                    DiscountAmount = bundle.getString("DiscountAmount");
                }
                if (bundle.containsKey("isHeader")) {
                    isHeader = bundle.getBoolean("isHeader");

                    /*if (isHeader) {
                        fullName = bundle.getString("fullName");
                        phoneNumber = bundle.getString("phoneNumber");
                        age = bundle.getString("age");
                        email = bundle.getString("email");
                        message = bundle.getString("message");
                    }*/
                }
            }
        }

        stripe_id = sharedpreferences.getString(MyConstants.STRIPE_ID, "");
        if (!TextUtils.isEmpty(stripe_id)) {
            Utils.showProgressDialog(PaymentActivity.this);
            new RetrieveCustomer().execute();
        }

        btnConfirmPay = findViewById(R.id.btnConfirmPay);
        tvPaymentWith = findViewById(R.id.tvPaymentWith);
        //viewPager = findViewById(R.id.viewpager);

        etCardHolderName = findViewById(R.id.etCardHolderName);
        etCardNumber = findViewById(R.id.etCardNumber);
        etMonth = findViewById(R.id.etMonth);
        etYear = findViewById(R.id.etYear);
        etCVV = findViewById(R.id.etCVV);
        switchSave = findViewById(R.id.switchSave);
        ivStripe = findViewById(R.id.ivStripe);
        ivPaypal = findViewById(R.id.ivPaypal);
        ivAndroidPay = findViewById(R.id.ivAndroidPay);

        etCardHolderName.getBackground().mutate().setColorFilter(getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.SRC_ATOP);
        etCardNumber.getBackground().mutate().setColorFilter(getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.SRC_ATOP);
        etMonth.getBackground().mutate().setColorFilter(getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.SRC_ATOP);
        etYear.getBackground().mutate().setColorFilter(getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.SRC_ATOP);
        etCVV.getBackground().mutate().setColorFilter(getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.SRC_ATOP);

        ivStripe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tabIndex = 0;
                // paymentWith = "<font color='#39c3f6'>stripe.com</font>";
                paymentWith = "stripe.com";
                tvPaymentWith.setText(Html.fromHtml(paymentWith));
                ivStripe.setImageResource(R.drawable.payment_button_bg);
                ivPaypal.setImageResource(R.drawable.payment_button_paypal);
                ivAndroidPay.setImageResource(R.drawable.payment_button_android);

                /*tvPaymentWith.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://stripe.com/"));
                        startActivity(browserIntent);
                    }
                });*/
            }
        });

        ivPaypal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tabIndex = 1;
                ivStripe.setImageResource(R.drawable.payment_button);
                ivPaypal.setImageResource(R.drawable.payment_button_paypal_bg);
                ivAndroidPay.setImageResource(R.drawable.payment_button_android);
                // paymentWith = "<font color='#39c3f6'>paypal.com</font>";
                paymentWith = "paypal.com";
                tvPaymentWith.setText(Html.fromHtml(paymentWith));

                /*tvPaymentWith.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.paypal.com/in/home"));
                        startActivity(browserIntent);
                    }
                });*/
                if (getIntent().getExtras().containsKey("rxDelivery")) {
                    doRxPaymentWithPaypal();

                }else if (isFromInPersonVisit) {
                    doInPersonPaymentWithPaypal();

                }else {
                    doPayPalPayment();
                }
            }
        });

        ivAndroidPay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tabIndex = 2;
                ivStripe.setImageResource(R.drawable.payment_button);
                ivPaypal.setImageResource(R.drawable.payment_button_paypal);
                ivAndroidPay.setImageResource(R.drawable.payment_button_android_bg);
                paymentWith = "<font color='#39c3f6'>android.com/pay/</font>";
                tvPaymentWith.setText(Html.fromHtml(paymentWith));

                tvPaymentWith.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.android.com/pay/"));
                        startActivity(browserIntent);
                    }
                });
            }
        });

        etCardNumber.addTextChangedListener(new FourDigitCardFormatWatcher());
        etMonth.addTextChangedListener(new MonthValidateWatcher());
        etYear.addTextChangedListener(new YearValidateWatcher());
        //setupViewPager(viewPager);

        /*tabLayout = (TabLayout) findViewById(R.id.tabs);
        //tabLayout.setupWithViewPager(viewPager);

        tabLayout.addTab(tabLayout.newTab().setText("Card Payment"));
        tabLayout.addTab(tabLayout.newTab().setText("PayPal"));
        tabLayout.addTab(tabLayout.newTab().setText("Android Pay"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);*/

        //ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager(), tabLayout.getTabCount());

        //Adding adapter to pager
        //viewPager.setAdapter(adapter);

        //btnConfirmPay.setText("Confirm Pay $" + total);
        btnConfirmPay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //System.out.println("card payment:::"+cardPaymentFragment);

                String cardNumber = etCardNumber.getText().toString();
                String cardHolderName = etCardHolderName.getText().toString().trim();
                String month = etMonth.getText().toString();
                String year = etYear.getText().toString();
                String cvv = etCVV.getText().toString();
                boolean saveCard = switchSave.isChecked();

                if (TextUtils.isEmpty(cardNumber)) {
                    etCardNumber.requestFocus();
                    etCardNumber.setError("Enter valid card number");
                } else if (TextUtils.isEmpty(cardHolderName)) {
                    etCardHolderName.requestFocus();
                    etCardHolderName.setError("Enter card holder name");
                } else if (TextUtils.isEmpty(month) || !month.matches("(?:0[1-9]|1[0-2])")) {
                    etMonth.requestFocus();
                    etMonth.setError("Enter valid month");
                } else if (TextUtils.isEmpty(year)) {
                    etYear.requestFocus();
                    etYear.setError("Enter valid year");
                } else if (TextUtils.isEmpty(cvv)) {
                    etCVV.requestFocus();
                    etCVV.setError("Enter valid CVV");
                } else {
                    if (!TextUtils.isEmpty(stripe_id)
                            && cardNumber.substring(cardNumber.length() - 4).equalsIgnoreCase(last4)
                            && month.equalsIgnoreCase(exp_month)
                            && year.equalsIgnoreCase(exp_year)) {
                        System.out.println("saved card:::::::::");
                        customerId = stripe_id;
                        saveCardForPayment = true;

                        new CreateChargeAndDoPayment().execute();

                    } else {
                        System.out.println("new card:::::::::");
                        if (tabIndex == 0) {
                            doStripePayment(cardNumber, cardHolderName, month, year, cvv, saveCard);
                        } else if (tabIndex == 1) {
                            MyConstants.paymentType = "2";
                        } else {
                            MyConstants.paymentType = "3";
                        }
                    }
                }
            }
        });

        //int tabIndex = tabLayout.getSelectedTabPosition();

        if (tabIndex == 0) {
            paymentWith = "<font color='#39c3f6'>stripe.com</font>";
        } else if (tabIndex == 1) {
            paymentWith = "<font color='#39c3f6'>paypal.com</font>";
        } else {
            paymentWith = "<font color='#39c3f6'>android.com/pay/</font>";
        }
        tvPaymentWith.setText(Html.fromHtml(paymentWith));

        /*tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                int position = tab.getPosition();

                if (position == 0) {
                    paymentWith = "Your payment is secured with <font color='#39c3f6'>stripe.com</font>";
                } else if (position == 1) {
                    paymentWith = "Your payment is secured with <font color='#39c3f6'>paypal.com</font>";
                    doPayPalPayment();
                } else {
                    paymentWith = "Your payment is secured with <font color='#39c3f6'>android.com/pay/</font>";
                }
                tvPaymentWith.setText(Html.fromHtml(paymentWith));
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });*/

        /*LinearLayout linearLayout = (LinearLayout) tabLayout.getChildAt(0);
        linearLayout.setShowDividers(LinearLayout.SHOW_DIVIDER_MIDDLE);
        GradientDrawable drawable = new GradientDrawable();
        drawable.setColor(Color.GRAY);
        drawable.setSize(1, 1);
        linearLayout.setDividerPadding(10);
        linearLayout.setDividerDrawable(drawable);*/
    }


    private void setupToolbar() {
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle("Checkout");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }


    @Override
    protected void onResume() {
        super.onResume();
        ivStripe.performClick();
    }

    private void doPayPalPayment() {
        Intent intent = new Intent(PaymentActivity.this, PayPalService.class);
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
        startService(intent);

        PayPalPayment thingToBuy = getStuffToBuy(PayPalPayment.PAYMENT_INTENT_SALE);
        Intent intentPay = new Intent(PaymentActivity.this, com.paypal.android.sdk.payments.PaymentActivity.class);
        intentPay.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
        intentPay.putExtra(com.paypal.android.sdk.payments.PaymentActivity.EXTRA_PAYMENT, thingToBuy);
        startActivityForResult(intentPay, REQUEST_CODE_PAYMENT);
    }


    private void doRxPaymentWithPaypal() {
        Intent intent = new Intent(PaymentActivity.this, PayPalService.class);
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
        startService(intent);

        PayPalPayment thingToBuy = rxPaymentWithPaypal(PayPalPayment.PAYMENT_INTENT_SALE);
        Intent intentPay = new Intent(PaymentActivity.this, com.paypal.android.sdk.payments.PaymentActivity.class);
        intentPay.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
        intentPay.putExtra(com.paypal.android.sdk.payments.PaymentActivity.EXTRA_PAYMENT, thingToBuy);
        startActivityForResult(intentPay, REQUEST_CODE_PAYMENT);
    }


    private void doInPersonPaymentWithPaypal() {
        Intent intent = new Intent(PaymentActivity.this, PayPalService.class);
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
        startService(intent);

        PayPalPayment thingToBuy = inPersonWithPaypal(PayPalPayment.PAYMENT_INTENT_SALE);
        Intent intentPay = new Intent(PaymentActivity.this, com.paypal.android.sdk.payments.PaymentActivity.class);
        intentPay.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
        intentPay.putExtra(com.paypal.android.sdk.payments.PaymentActivity.EXTRA_PAYMENT, thingToBuy);
        startActivityForResult(intentPay, REQUEST_CODE_PAYMENT);
    }


    private PayPalPayment getStuffToBuy(String paymentIntent) {
        PayPalItem[] items;
        if (isHeader) {
            items = new PayPalItem[]{
                            new PayPalItem("Doctor note charge", 1, new BigDecimal(total), "CAD", PaymentID),
                    };
        } else {
            items = new PayPalItem[]{
                            new PayPalItem("DR." + MyConstants.doctorLastName + " Consult charge", 1, new BigDecimal(total), "USD", PaymentID),
                            //new PayPalItem("DR." + MyConstants.doctorLastName + " Consult charge", 1, new BigDecimal("1.99"), "USD", PaymentID),
                    };
        }


        BigDecimal subtotal = PayPalItem.getItemTotal(items);
        BigDecimal shipping = new BigDecimal(0);
        BigDecimal tax = new BigDecimal(0);
        PayPalPaymentDetails paymentDetails = new PayPalPaymentDetails(shipping, subtotal, tax);
        BigDecimal amount = subtotal.add(shipping).add(tax);
        PayPalPayment payment;
        if (isHeader) {
            payment = new PayPalPayment(amount, "CAD", "Doctor note charge", paymentIntent);
            payment.items(items).paymentDetails(paymentDetails);
        } else {
            payment = new PayPalPayment(amount, "USD", "DR." + MyConstants.doctorLastName + " Consult Charge", paymentIntent);
            payment.items(items).paymentDetails(paymentDetails);
        }

        return payment;
    }


    private PayPalPayment inPersonWithPaypal(String paymentIntent) {
        PayPalItem[] items;
        if (isHeader) {
            items = new PayPalItem[]{
                    new PayPalItem("Doctor note charge", 1, new BigDecimal(total), "CAD", PaymentID),
            };
        } else {
            items = new PayPalItem[]{
                    new PayPalItem("DR." + mDoctorProfileData.getLastName() + " Consult charge", 1, new BigDecimal(total), "USD", PaymentID),
                    //new PayPalItem("DR." + MyConstants.doctorLastName + " Consult charge", 1, new BigDecimal("1.99"), "USD", PaymentID),
            };
        }


        BigDecimal subtotal = PayPalItem.getItemTotal(items);
        BigDecimal shipping = new BigDecimal(0);
        BigDecimal tax = new BigDecimal(0);
        PayPalPaymentDetails paymentDetails = new PayPalPaymentDetails(shipping, subtotal, tax);
        BigDecimal amount = subtotal.add(shipping).add(tax);
        PayPalPayment payment;
        if (isHeader) {
            payment = new PayPalPayment(amount, "CAD", "Doctor note charge", paymentIntent);
            payment.items(items).paymentDetails(paymentDetails);
        } else {
            payment = new PayPalPayment(amount, "USD", "DR." + mDoctorProfileData.getLastName() + " Consult Charge", paymentIntent);
            payment.items(items).paymentDetails(paymentDetails);
        }

        return payment;
    }


    private PayPalPayment rxPaymentWithPaypal(String paymentIntent) {
        PayPalItem[] items;
        if (isHeader) {
            items = new PayPalItem[]{
                    new PayPalItem("Doctor note charge", 1, new BigDecimal(total), "CAD", PaymentID),
            };
        } else {
            items = new PayPalItem[]{
                    new PayPalItem("Rx Prescription Delivery Charge", 1, new BigDecimal(total), "USD", PaymentID),
                    //new PayPalItem("DR." + MyConstants.doctorLastName + " Consult charge", 1, new BigDecimal("1.99"), "USD", PaymentID),
            };
        }

        BigDecimal subtotal = PayPalItem.getItemTotal(items);
        BigDecimal shipping = new BigDecimal(0);
        BigDecimal tax = new BigDecimal(0);
        PayPalPaymentDetails paymentDetails = new PayPalPaymentDetails(shipping, subtotal, tax);
        BigDecimal amount = subtotal.add(shipping).add(tax);
        PayPalPayment payment;
        if (isHeader) {
            payment = new PayPalPayment(amount, "CAD", "Doctor note charge", paymentIntent);
            payment.items(items).paymentDetails(paymentDetails);
        } else {
            payment = new PayPalPayment(amount, "USD", "Rx Prescription Delivery Charge", paymentIntent);
            payment.items(items).paymentDetails(paymentDetails);
        }

        return payment;
    }


    @Override
    public void onDestroy() {
        // Stop service when done
        stopService(new Intent(this, PayPalService.class));
        super.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE_PAYMENT) {
            if (resultCode == Activity.RESULT_OK) {
                PaymentConfirmation confirm =
                        data.getParcelableExtra(com.paypal.android.sdk.payments.PaymentActivity.EXTRA_RESULT_CONFIRMATION);
                if (confirm != null) {
                    try {


                       /* {
                            "response": {
                            "state": "approved",
                                    "id": "PAY-18X32451H0459092JKO7KFUI",
                                    "create_time": "2014-07-18T18:46:55Z",
                                    "intent": "sale"
                        },
                            "client": {
                            "platform": "Android",
                                    "paypal_sdk_version": "2.14.2",
                                    "product_name": "PayPal-Android-SDK",
                                    "environment": "mock"
                        },
                            "response_type": "payment"
                        }
                        06-02 05:21:30.830 com.markteq.wms I: {
                            "short_description": "sample item",
                                    "amount": "0.01",
                                    "intent": "sale",
                                    "currency_code": "USD"
                        }*/

                        JSONObject jObj = confirm.toJSONObject();
                        String payId = jObj.getJSONObject("response").getString("id");
                        Log.i(TAG, "Resutl1 : Confirm " + confirm.toJSONObject().toString(4));
                        Log.i(TAG, "Resutl1 : Payment " + confirm.getPayment().toJSONObject().toString(4));


                        //    new CallRequests(this).getPaymentSuccess(payId, PaymentID, SubscriptionID, companyId, "PayPal");

                        if (getIntent().getExtras().containsKey("rxDelivery")) {
                            submitRxDeliveryData("2", payId, "");

                        }else if (isFromInPersonVisit) {
                            bookInPersonConsult("2", payId, "");

                        }else {
                            if (isHeader) {
                                Map<String, String> map = new HashMap<String, String>();
                                map.put("url", MyConstants.BASE_URL + "doctorNotePayment");
                                map.put("Version", MyConstants.WS_VERSION);
                                map.put("Payment", String.valueOf(total));
                                map.put("PaymentType", "2");
                                map.put("TxnId", payId);
                                map.put("ApiToken", MyConstants.API_TOKEN);
                                map.put("UserId", App.user.getUserID());
                                map.put("DoctorNoteId", MyConstants.consultId);

                                new CallRequest(PaymentActivity.this).createBookingPayment(map);

                            } else {
                                Map<String, String> map = new HashMap<String, String>();
                                if (MyConstants.isGuest.equalsIgnoreCase("1")) {
                                    map.put("url", MyConstants.GUEST_BASE_URL + "guestConsultPayment");
                                    map.put("GuestId", App.user.getUserID());
                                } else {
                                    map.put("url", MyConstants.BASE_URL + "consultPayment");
                                    map.put("UserId", App.user.getUserID());
                                }
                                map.put("ApiToken", MyConstants.API_TOKEN);
                                map.put("Version", MyConstants.WS_VERSION);
                                map.put("ConsultId", MyConstants.consultId);
                                map.put("Payment", String.valueOf(total));
                                map.put("DiscountAmount", DiscountAmount);
                                map.put("WalletAmount", walletCharge);
                                map.put("PaymentType", "2");
                                map.put("OfferId", OfferId);
                                map.put("OfferType", OfferType);
                                map.put("TxnId", payId);

                                new CallRequest(PaymentActivity.this).createBookingPayment(map);
                            }
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                Log.i(TAG, "The user canceled.");

            } else if (resultCode == com.paypal.android.sdk.payments.PaymentActivity.RESULT_EXTRAS_INVALID) {
                Log.i(TAG,"An invalid Payment or PayPalConfiguration was submitted. Please see the docs.");
            }

        } else if (requestCode == REQUEST_CODE_FUTURE_PAYMENT) {
            if (resultCode == Activity.RESULT_OK) {
                PayPalAuthorization auth = data.getParcelableExtra(PayPalFuturePaymentActivity.EXTRA_RESULT_AUTHORIZATION);
                if (auth != null) {
                    try {
                        Log.i("FuturePaymentExample", auth.toJSONObject().toString(4));

                        String authorization_code = auth.getAuthorizationCode();
                        Log.i("FuturePaymentExample", authorization_code);

                        sendAuthorizationToServer(auth);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                Log.i("FuturePaymentExample", "The user canceled.");

            } else if (resultCode == PayPalFuturePaymentActivity.RESULT_EXTRAS_INVALID) {
                Log.i("FuturePaymentExample","Probably the attempt to previously start the PayPalService had an invalid PayPalConfiguration. Please see the docs.");
            }

        } else if (requestCode == REQUEST_CODE_PROFILE_SHARING) {
            if (resultCode == Activity.RESULT_OK) {
                PayPalAuthorization auth = data.getParcelableExtra(PayPalProfileSharingActivity.EXTRA_RESULT_AUTHORIZATION);
                if (auth != null) {
                    try {
                        Log.i("ProfileSharingExample", auth.toJSONObject().toString(4));

                        String authorization_code = auth.getAuthorizationCode();
                        Log.i("ProfileSharingExample", authorization_code);

                        sendAuthorizationToServer(auth);
                        //displayResultText("Profile Sharing code received from PayPal");

                    } catch (JSONException e) {


                        Log.e("ProfileSharingExample", "an extremely unlikely failure occurred: ", e);
                    }
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                Log.i("ProfileSharingExample", "The user canceled.");

            } else if (resultCode == PayPalFuturePaymentActivity.RESULT_EXTRAS_INVALID) {
                Log.i("ProfileSharingExample", "Probably the attempt to previously start the PayPalService had an invalid PayPalConfiguration. Please see the docs.");
            }
        }
    }

    public void onFuturePaymentPressed(View pressed) {
        Intent intent = new Intent(PaymentActivity.this, PayPalFuturePaymentActivity.class);

        startActivityForResult(intent, REQUEST_CODE_FUTURE_PAYMENT);
    }

    private void sendAuthorizationToServer(PayPalAuthorization authorization) {

    }

    public void onFuturePaymentPurchasePressed(View pressed) {
        String correlationId = PayPalConfiguration.getApplicationCorrelationId(this);

        Log.i("FuturePaymentExample", "Application Correlation ID: " + correlationId);

        // TODO: Send correlationId and transaction details to your server for
        // processing with
        // PayPal...
        Toast.makeText(getApplicationContext(),"App Correlation ID received from SDK", Toast.LENGTH_LONG).show();
    }


    private void submitRxDeliveryData(String paymentType, String txnId, String offerType) {
        if (!Internet.isAvailable(this)) {
            Internet.showAlertDialog(this, "Error!", "No Internet Connection", false);
            return;
        }

        Map<String, String> map = new HashMap<String, String>();
        map.put("url", MyConstants.BASE_URL + "bookRxDelivery");
        map.put("SocialId", sharedpreferences.getString(MyConstants.SOCIAL_ID, "0"));
        map.put("SocialType", sharedpreferences.getString(MyConstants.LOGIN_TYPE, "0"));
        map.put("Version", MyConstants.WS_VERSION);
        map.put("UserId", App.user.getUserID());
        map.put("ApiToken", MyConstants.API_TOKEN);
        map.put("PrivateInsurance", mRxInsuranceNo);
        map.put("Delivery", mDelivery);
        map.put("FullName", mRxFullName);
        map.put("Email", mRxEmail);
        map.put("PrescriptionImage", mRxPrescriptionImage);
        map.put("Payment", String.valueOf(total));
        map.put("PaymentType", paymentType);
        map.put("TxnId", txnId);
        map.put("OfferType", offerType);

        new CallRequest(PaymentActivity.this).bookRxDelivery(map);
    }


    private void bookInPersonConsult(String paymentType, String txnId, String offerType) {
        if (!Internet.isAvailable(this)) {
            Internet.showAlertDialog(this, "Error!", "No Internet Connection", false);
            return;
        }

        Map<String, String> map = new HashMap<String, String>();
        map.put("url", MyConstants.BASE_URL + "bookInPersonConsult");
        map.put("ApiToken", MyConstants.API_TOKEN);
        map.put("UserId", App.user.getUserID());
        map.put("Version", MyConstants.WS_VERSION);
        map.put("SocialId", sharedpreferences.getString(MyConstants.SOCIAL_ID, "0"));
        map.put("SocialType", sharedpreferences.getString(MyConstants.LOGIN_TYPE, "0"));
        map.put("DoctorId", mDoctorProfileData.getDoctorId());
        map.put("DoctorCharge", mDoctorProfileData.getCosultCharge());
        map.put("ConsultTime", mConsultTime);
        map.put("ConsultTimeSlotId", mTimeSlotId);
        map.put("ReasonForConsult", mReasonForConsult);
        map.put("PhoneNumberToReach", mPhoneNumberToReach);
        map.put("AdminCharge", String.valueOf(total));
        map.put("PaymentType", paymentType);
        map.put("TxnId", txnId);
        map.put("OfferType", offerType);
        map.put("PaymentDashboard", "ANDROID");


        new CallRequest(PaymentActivity.this).bookInPersonConsult(map);
    }


    private void doStripePayment(String cardNumber, String cardHolderName, String month, String year, String cvv, final boolean saveCard) {
        Utils.showProgressDialog(PaymentActivity.this, "Loading");

        paymentType = "1";

        String mYear = year.substring(Math.max(year.length() - 2, 0));

        // validate card
        card = new Card(cardNumber,
                Integer.valueOf(month),
                Integer.valueOf(mYear),
                cvv);

        if (!card.validateNumber()) {
            etCardNumber.requestFocus();
            Toast.makeText(getApplicationContext(), "Enter valid card number", Toast.LENGTH_LONG).show();
            Utils.hideProgressDialog();
        } else if (!card.validateCVC()) {
            etCVV.requestFocus();
            Utils.hideProgressDialog();
            Toast.makeText(getApplicationContext(), "Enter valid CVV number", Toast.LENGTH_LONG).show();
        } else if (!card.validateExpMonth()) {
            etMonth.requestFocus();
            Utils.hideProgressDialog();
            Toast.makeText(getApplicationContext(), "Invalid month", Toast.LENGTH_LONG).show();
        } else if (!card.validateExpYear()) {
            etYear.requestFocus();
            Utils.hideProgressDialog();
            Toast.makeText(getApplicationContext(), "Invalid year", Toast.LENGTH_LONG).show();
        } else {
            System.out.println("keys and status==>" + STRIPE_CONFIG_ENVIRONMENT);
            String PUBLISH_KEY;

            if (STRIPE_CONFIG_ENVIRONMENT.equals("0")) {
                PUBLISH_KEY = STRIPE_PUBLISH_KEY_TEST;
            } else {
                //PUBLISH_KEY = STRIPE_PUBLISH_KEY_LIVE;
                PUBLISH_KEY = STRIPE_PUBLISH_KEY_TEST;
            }

            System.out.println("keys ==>" + PUBLISH_KEY);

            new Stripe().createToken(card, PUBLISH_KEY, new TokenCallback() {
                public void onSuccess(Token token) {
                    //Toast.makeText( getApplicationContext(),"Token created: " + token.getId(),Toast.LENGTH_LONG).show();
                    TOKEN_ID = token.getId();
                    System.out.println("Token:::" + TOKEN_ID);

                    // Create a Customer:
                    if (TextUtils.isEmpty(user_email)) {
                        user_email = "Guest";
                    }
                    System.out.println("email::::" + user_email);
                    if (STRIPE_CONFIG_ENVIRONMENT.equals("0")) {
                        com.stripe.Stripe.apiKey = MyConstants.STRIPE_SECRET_KEY_TEST;
                    } else {
                        //com.stripe.Stripe.apiKey = MyConstants.STRIPE_SECRET_KEY_LIVE;
                        com.stripe.Stripe.apiKey = MyConstants.STRIPE_SECRET_KEY_TEST;
                    }
                    Map<String, Object> customerParams = new HashMap<String, Object>();
                    customerParams.put("email", user_email);
                    customerParams.put("source", TOKEN_ID);

                    saveCardForPayment = saveCard;

                    if (saveCardForPayment) {
                        new CreateCustomer().execute(customerParams);
                    } else {
                        new CreateChargeAndDoPayment().execute();
                    }
                }

                public void onError(Exception error) {
                    Utils.hideProgressDialog();
                    Toast.makeText(getApplicationContext(), "Your card was declined", Toast.LENGTH_LONG).show();

                    Log.d("Stripe", error.getLocalizedMessage());
                }
            });
        }
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        Utils.removeSimpleSpinProgressDialog();
        try {
            if (result != null && !result.isEmpty()) {
                Log.i("TAG", "TAG Result : " + result);

                switch (request) {
                    case createBooking:
                        try {
                            JSONObject mainObj = new JSONObject(result);
                            if (mainObj.getString("error_code").equalsIgnoreCase("0")) {
                                if (isHeader) {
                                    new AlertDialog.Builder(this)
                                            .setTitle("Alert!")
                                            .setIcon(android.R.drawable.ic_dialog_alert)
                                            .setMessage(mainObj.getString("error_string"))
                                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int which) {
                                                    dialog.dismiss();
                                                    MyConstants.specialitiesList.clear();
                                                    MyConstants.isLoadAgain = true;
                                                    Intent intent = new Intent(PaymentActivity.this, PTDashboardActivity.class);
                                                    startActivity(intent);
                                                    ActivityCompat.finishAffinity(PaymentActivity.this);
                                                }
                                            })
                                            .show();
                                } else {

                                    //Utils.showToast(mainObj.getString("error_string"), PaymentActivity.this);

                                    JSONObject jsonObject = mainObj.getJSONArray("result").getJSONObject(0);

                                    String UniqueId = jsonObject.getString("UniqueId");

                                    MyConstants.uniqueChatId = UniqueId;

                                    // reload all data
                                    MyConstants.bookingHistoryList.clear();
                                    MyConstants.messageHistoryList.clear();
                                    MyConstants.userChatList.clear();
                                    MyConstants.doctorChatList.clear();
                                    MyConstants.isBookingHistoryLoad = true;
                                    MyConstants.isMessageHistoryLoad = true;

                                    Intent intent = new Intent(PaymentActivity.this, FireChatActivity.class);
                                    intent.putExtra(MyConstants.IS_PAYMENT_DONE, true);
                                    intent.putExtra(MyConstants.UNIQUE_CHAT_ID, MyConstants.uniqueChatId);
                                    intent.putExtra(MyConstants.Pt_NAME, App.user.getName());
                                    if (App.user.getUser_Type().equalsIgnoreCase(MyConstants.USER_PT) && MyConstants.isBooked) {
                                        intent.putExtra(MyConstants.DR_NAME, MyConstants.DOCTOR_NAME);
                                        intent.putExtra(MyConstants.DR_ID, MyConstants.DOCTOR_ID);
                                        intent.putExtra(MyConstants.RECEIVER_IMAGE_URL, MyConstants.DOCTOR_PROFILEPIC);
                                    } else {
                                        intent.putExtra(MyConstants.DR_NAME, MyConstants.doctorProfile.getFirstName() + " " + MyConstants.doctorProfile.getLastName());
                                        intent.putExtra(MyConstants.DR_ID, MyConstants.doctorProfile.getDoctorId());
                                        intent.putExtra(MyConstants.RECEIVER_IMAGE_URL, MyConstants.doctorProfile.getProfilePic());
                                    }
                                    intent.putExtra(MyConstants.PT_ID, App.user.getUserID());
                                    startActivity(intent);
                                    ActivityCompat.finishAffinity(PaymentActivity.this);
                                }
                            } else {
                                Utils.showToast(mainObj.getString("error_string"), PaymentActivity.this);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;

                    case bookRxDelivery:
                        JSONObject jsonObject = new JSONObject(result);
                        if (jsonObject.getString("error_code").equals("0")) {
                            Toast.makeText(PaymentActivity.this, jsonObject.getString("error_string"), Toast.LENGTH_LONG).show();
                            Intent intent = new Intent(PaymentActivity.this, PTDashboardActivity.class);
                            startActivity(intent);
                            ActivityCompat.finishAffinity(PaymentActivity.this);

                        }else {
                            Utils.showAlert(jsonObject.getString("error_string"), PaymentActivity.this);
                        }
                        break;


                    case bookInPersonConsult:
                        JSONObject jsonObject1 = new JSONObject(result);
                        if (jsonObject1.getString("error_code").equals("0")) {
                            Toast.makeText(PaymentActivity.this, jsonObject1.getString("error_string"), Toast.LENGTH_LONG).show();
                            Intent intent = new Intent(PaymentActivity.this, ThankYouActivity.class);
                            intent.putExtra("DOCTOR_PROFILE", mDoctorProfileData);
                            intent.putExtra("ConsultTime", mConsultTime);
                            startActivity(intent);
                            ActivityCompat.finishAffinity(PaymentActivity.this);

                        }else if (jsonObject1.getString("error_code").equals("")) {
                            Utils.showAlert(jsonObject1.getString("error_string"), PaymentActivity.this);
                        }
                        break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onProgressUpdate(String uniqueMessageId, int progres) {

    }

    @Override
    public void onProgressComplete(String uniqueMessageId, String result, Constant.REQUESTS request) {

    }

    public class FourDigitCardFormatWatcher implements TextWatcher {

        private static final char space = ' ';

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void afterTextChanged(Editable s) {
            // Remove all spacing char
            int pos = 0;
            while (true) {
                if (pos >= s.length()) break;
                if (space == s.charAt(pos) && (((pos + 1) % 5) != 0 || pos + 1 == s.length())) {
                    s.delete(pos, pos + 1);
                } else {
                    pos++;
                }
            }

            // Insert char where needed.
            pos = 4;
            while (true) {
                if (pos >= s.length()) break;
                final char c = s.charAt(pos);
                // Only if its a digit where there should be a space we insert a space
                if ("0123456789".indexOf(c) >= 0) {
                    s.insert(pos, "" + space);
                }
                pos += 5;
            }
        }
    }

    public class MonthValidateWatcher implements TextWatcher {

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void afterTextChanged(Editable s) {
            if (!etMonth.getText().toString().matches("(?:0[1-9]|1[0-2])")) {
                etMonth.setError("Invalid month");
            }
        }
    }

    public class YearValidateWatcher implements TextWatcher {

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void afterTextChanged(Editable s) {
            if (!etYear.getText().toString().matches("^20((1[7-9])|([2-9][0-9]))$")) {
                etYear.setError("Invalid year");
            }
        }
    }

    @SuppressLint("StaticFieldLeak")
    class RetrieveCustomer extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... maps) {

            Customer customer = null;
            try {
                if (STRIPE_CONFIG_ENVIRONMENT.equals("0")) {
                    com.stripe.Stripe.apiKey = MyConstants.STRIPE_SECRET_KEY_TEST;
                } else {
                    //com.stripe.Stripe.apiKey = MyConstants.STRIPE_SECRET_KEY_LIVE;
                    com.stripe.Stripe.apiKey = MyConstants.STRIPE_SECRET_KEY_TEST;
                }
                customer = Customer.retrieve(stripe_id, com.stripe.Stripe.apiKey);

                String defaultSource = customer.getDefaultSource();

                ExternalAccount externalAccount = customer.getSources().retrieve(defaultSource);


                String s = externalAccount.toString();

                s = s.substring(s.indexOf("{"));

                System.out.println("object::" + s);

                JSONObject object = new JSONObject(s);

                last4 = object.getString("last4");
                exp_month = object.getString("exp_month");
                exp_year = object.getString("exp_year");
                name = "";
                if (object.has("name"))
                    name = object.getString("name");
                if (name==null || name.equals("null"))
                    name = "";

            } catch (AuthenticationException e) {
                e.printStackTrace();
            } catch (InvalidRequestException e) {
                e.printStackTrace();
            } catch (APIConnectionException e) {
                e.printStackTrace();
            } catch (CardException e) {
                e.printStackTrace();
            } catch (APIException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            Utils.hideProgressDialog();

            if (last4 != null && !TextUtils.isEmpty(last4)) {
                etCardNumber.setText("xxxx xxxx xxxx " + last4);
                etMonth.setText(exp_month);
                etYear.setText(exp_year);
                etCardHolderName.setText(name);
            }
            //new CreateChargeAndDoPayment().execute();
        }
    }

    @SuppressLint("StaticFieldLeak")
    class CreateCustomer extends AsyncTask<Map<String, Object>, Void, Void> {

        @Override
        protected Void doInBackground(Map<String, Object>[] maps) {
            try {
                if (STRIPE_CONFIG_ENVIRONMENT.equals("0")) {
                    com.stripe.Stripe.apiKey = MyConstants.STRIPE_SECRET_KEY_TEST;
                } else {
                    //com.stripe.Stripe.apiKey = MyConstants.STRIPE_SECRET_KEY_LIVE;
                    com.stripe.Stripe.apiKey = MyConstants.STRIPE_SECRET_KEY_TEST;
                }
                Customer customer = Customer.create(maps[0], com.stripe.Stripe.apiKey);
                customerId = customer.getId();

                SharedPreferences.Editor editor = sharedpreferences.edit();
                editor.putString(MyConstants.STRIPE_ID, customerId);
                editor.commit();

            } catch (AuthenticationException e) {
                e.printStackTrace();
            } catch (InvalidRequestException e) {
                e.printStackTrace();
            } catch (APIConnectionException e) {
                e.printStackTrace();
            } catch (CardException e) {
                e.printStackTrace();
            } catch (APIException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
//            Utils.hideProgressDialog();

            new CreateChargeAndDoPayment().execute();
        }
    }

    @SuppressLint("StaticFieldLeak")
    private class CreateChargeAndDoPayment extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog

        }

        @Override
        protected Void doInBackground(Void... arg0) {
            if (STRIPE_CONFIG_ENVIRONMENT.equals("0")) {
                com.stripe.Stripe.apiKey = MyConstants.STRIPE_SECRET_KEY_TEST;
            } else {
                //com.stripe.Stripe.apiKey = MyConstants.STRIPE_SECRET_KEY_LIVE;
                com.stripe.Stripe.apiKey = MyConstants.STRIPE_SECRET_KEY_TEST;
            }
            Number num = null;
            try {
                num = NumberFormat.getInstance().parse(String.valueOf(total));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            int grand_total1 = 0;
            if (num != null) {
                grand_total1 = num.intValue() * 100;
            }
            Map<String, Object> chargeParams = new HashMap<String, Object>();
            chargeParams.put("amount", grand_total1);
            System.out.println("isHeader:::***" + isHeader);
            if (isHeader) {
                chargeParams.put("description", user_email);
                chargeParams.put("currency", "cad");
            } else {
                chargeParams.put("description", "DR." + MyConstants.doctorLastName + " (#" + MyConstants.consultId + ")");
                chargeParams.put("currency", "usd");
            }
            if (saveCardForPayment) {
                chargeParams.put("customer", customerId);
                System.out.println("customer::::" + customerId);
            } else {
                chargeParams.put("source", TOKEN_ID);
                System.out.println("TOKEN_ID::::" + TOKEN_ID);
            }
            System.out.println("amount:::::::" + grand_total1);
            System.out.println("description:::" + "DR." + MyConstants.doctorLastName + " (#" + MyConstants.consultId + ")");

            try {
                Charge charge = Charge.create(chargeParams, com.stripe.Stripe.apiKey);
                System.out.println("charge:::::" + charge);
                status = charge.getStatus();
                System.out.println("status::::::" + status);
                if (status.equalsIgnoreCase("succeeded")) {
                    charge_id = charge.getId();
                    System.out.println("charge_id::::" + charge_id);
                    System.out.println("payment done");
                    //paymentDone();
                } else {
                    Toast.makeText(PaymentActivity.this, "Payment Failed", Toast.LENGTH_SHORT).show();
                }

            } catch (AuthenticationException e) {
                e.printStackTrace();
            } catch (InvalidRequestException e) {
                e.printStackTrace();
            } catch (APIConnectionException e) {
                e.printStackTrace();
            } catch (CardException e) {
                e.printStackTrace();
                System.out.println("Status is: " + e.getCode());
                System.out.println("Message is: " + e.getMessage());
            } catch (APIException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            // Dismiss the progress dialog
            Utils.hideProgressDialog();

            if (!TextUtils.isEmpty(status)) {
                if (status.equalsIgnoreCase("succeeded")) {
                    //Toast.makeText(getApplicationContext(), "Payment has been successful", Toast.LENGTH_SHORT).show();

                    if (getIntent().getExtras().containsKey("rxDelivery")) {
                        submitRxDeliveryData("1", charge_id, "");

                    }else if (isFromInPersonVisit) {
                        bookInPersonConsult("1", charge_id, "");

                    }else {
                        if (isHeader) {
                            Map<String, String> map = new HashMap<String, String>();
                            map.put("url", MyConstants.BASE_URL + "doctorNotePayment");
                            map.put("Version", MyConstants.WS_VERSION);
                            map.put("Payment", String.valueOf(total));
                            map.put("PaymentType", "1");
                            map.put("TxnId", charge_id);
                            map.put("DoctorNoteId", MyConstants.consultId);
                            map.put("ApiToken", MyConstants.API_TOKEN);
                            map.put("UserId", App.user.getUserID());
                            map.put("DoctorNoteId", MyConstants.consultId);

                            new CallRequest(PaymentActivity.this).createBookingPayment(map);

                        } else {
                            Map<String, String> map = new HashMap<String, String>();
                            if (MyConstants.isGuest.equalsIgnoreCase("1")) {
                                map.put("url", MyConstants.GUEST_BASE_URL + "guestConsultPayment");
                                map.put("GuestId", App.user.getUserID());
                            } else {
                                map.put("url", MyConstants.BASE_URL + "consultPayment");
                                map.put("UserId", App.user.getUserID());
                            }
                            map.put("ApiToken", MyConstants.API_TOKEN);
                            map.put("Version", MyConstants.WS_VERSION);
                            map.put("ConsultId", MyConstants.consultId);
                            map.put("Payment", String.valueOf(total));
                            map.put("DiscountAmount", DiscountAmount);
                            map.put("WalletAmount", walletCharge);
                            map.put("PaymentType", "1");
                            map.put("OfferId", OfferId);
                            map.put("OfferType", OfferType);
                            map.put("TxnId", charge_id);

                            if (!saveCardForPayment) {
                                customerId = "";
                            }

                            map.put("StripeId", customerId);
                            new CallRequest(PaymentActivity.this).createBookingPayment(map);
                        }
                        /*Intent intent = new Intent(PaymentActivity.this, PTDashboardActivity.class);
                        startActivity(intent);
                        ActivityCompat.finishAffinity(PaymentActivity.this);*/
                    }
                }
            } else {
                Toast.makeText(PaymentActivity.this, "Payment Failed", Toast.LENGTH_SHORT).show();
            }
        }
    }

    /*private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager(), tabLayout.getTabCount());
        adapter.addFragment(new CardPaymentFragment(), "Card Payment");
        adapter.addFragment(new CardPaymentFragment(), "Paypal");
        adapter.addFragment(new CardPaymentFragment(), "Android pay");
        viewPager.setAdapter(adapter);
    }*/


    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();
        int tabCount;

        public ViewPagerAdapter(FragmentManager manager, int tabCount) {
            super(manager);
            this.tabCount = tabCount;
        }

        @Override
        public Fragment getItem(int position) {
            //cardPaymentFragment = new CardPaymentFragment();
            switch (position) {
                case 0:
                    CardPaymentFragment cardPaymentFragment = new CardPaymentFragment();
                    return cardPaymentFragment;

                default:
                    return null;
            }
        }

        @Override
        public int getCount() {
            return tabCount;
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        public int getItemPosition(Object object) {
            return POSITION_NONE;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
