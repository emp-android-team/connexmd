package com.connex.md.patient.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.connex.md.R;
import com.connex.md.firebase_chat.activity.NurseFireChatActivity;
import com.connex.md.interfaces.AsyncTaskListner;
import com.connex.md.others.App;
import com.connex.md.others.Internet;
import com.connex.md.ws.CallRequest;
import com.connex.md.ws.Constant;
import com.connex.md.ws.MyConstants;
import com.connex.md.ws.Utils;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.FirebaseTooManyRequestsException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.hbb20.CountryCodePicker;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import static com.connex.md.audio_video_calling.BaseActivity.getSinchServiceInterface;

/**
 * Created by Krupa Kakkad
 *
 */

public class PhoneAuthActivity extends AppCompatActivity implements View.OnClickListener, AsyncTaskListner {

    private EditText mPhoneNumberField;
    //EditText mVerificationField;
    private Button mStartButton, mVerifyButton, mResendButton;
    private CountryCodePicker countryCode;
    private EditText etFirstName, etLastName;
    private FirebaseAuth mAuth;
    private PhoneAuthProvider.ForceResendingToken mResendToken;
    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks;
    public String mVerificationId;
    public DatabaseReference fireDB;
    private SharedPreferences sharedpreferences;
    private String profileUpdate="";

    private static final String TAG = "PhoneAuthActivity";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_phone);

        mPhoneNumberField = findViewById(R.id.field_phone_number);
        //mVerificationField = (EditText) findViewById(R.id.field_verification_code);
        countryCode = findViewById(R.id.countryCode);

        countryCode.setAutoDetectedCountry(true);

        sharedpreferences = getSharedPreferences(MyConstants.PREF, Context.MODE_PRIVATE);

        mStartButton = findViewById(R.id.button_start_verification);
        //mVerifyButton = (Button) findViewById(R.id.button_verify_phone);
        //mResendButton = (Button) findViewById(R.id.button_resend);

        etFirstName = findViewById(R.id.etFirstName);
        etLastName = findViewById(R.id.etLastName);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null){
            if (bundle.containsKey("profileUpdate")){
                profileUpdate = bundle.getString("profileUpdate");
            }
        }

        mStartButton.setOnClickListener(this);
        /*mVerifyButton.setOnClickListener(this);
        mResendButton.setOnClickListener(this);*/

        etFirstName.getBackground().mutate().setColorFilter(getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.SRC_ATOP);
        etLastName.getBackground().mutate().setColorFilter(getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.SRC_ATOP);
        mPhoneNumberField.getBackground().mutate().setColorFilter(getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.SRC_ATOP);


        mAuth = FirebaseAuth.getInstance();
        mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
            @Override
            public void onVerificationCompleted(PhoneAuthCredential credential) {
                Log.d(TAG, "onVerificationCompleted:" + credential);
                if (profileUpdate.equalsIgnoreCase("1")){
                    signInWithPhoneAuthCredential(credential, false);
                } else {
                    signInWithPhoneAuthCredential(credential, true);
                }
            }

            @Override
            public void onVerificationFailed(FirebaseException e) {
                Log.w(TAG, "onVerificationFailed", e);
                Utils.removeSimpleSpinProgressDialog();
                if (e instanceof FirebaseAuthInvalidCredentialsException) {
                    mPhoneNumberField.setError("Invalid phone number.");
                } else if (e instanceof FirebaseTooManyRequestsException) {
                    Snackbar.make(findViewById(android.R.id.content), "Quota exceeded.",
                            Snackbar.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onCodeSent(String verificationId, PhoneAuthProvider.ForceResendingToken token) {
                Log.d(TAG, "onCodeSent:" + verificationId);
                mVerificationId = verificationId;
                mResendToken = token;
                Utils.removeSimpleSpinProgressDialog();

                Intent intent = new Intent(PhoneAuthActivity.this, GuestVerifyOTPActivity.class);
                intent.putExtra("phone", countryCode.getSelectedCountryCodeWithPlus() + mPhoneNumberField.getText().toString());
                intent.putExtra("number", mPhoneNumberField.getText().toString());
                intent.putExtra("first_name", etFirstName.getText().toString());
                intent.putExtra("last_name", etLastName.getText().toString());
                intent.putExtra("verification_id", mVerificationId);
                intent.putExtra("resend_token", mResendToken);
                if (profileUpdate.equalsIgnoreCase("1")){
                    intent.putExtra("profileUpdate",profileUpdate);
                    startActivityForResult(intent, 11);
                } else {
                    startActivity(intent);
                    finish();
                }
            }
        };
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        setResult(11, data);
        finish();
    }

    private void signInWithPhoneAuthCredential(PhoneAuthCredential credential, final boolean isVerified) {
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            Log.d(TAG, "signInWithCredential:success");
                            FirebaseUser user = task.getResult().getUser();
                            Utils.removeSimpleSpinProgressDialog();
                            if (isVerified) {
                                guestLogin();
                            } else {
                                Intent intent = new Intent();
                                intent.putExtra("phone_number", countryCode.getSelectedCountryCodeWithPlus() + mPhoneNumberField.getText().toString());
                                setResult(11, intent);
                                finish();
                            }
                            /*startActivity(new Intent(PhoneAuthActivity.this, MentionQueryActivity.class));
                            finish();*/
                        } else {
                            Utils.removeSimpleSpinProgressDialog();
                            Log.w(TAG, "signInWithCredential:failure", task.getException());
                            /*if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                                mVerificationField.setError("Invalid code.");
                            }*/
                        }
                    }
                });
    }

    private void guestLogin() {
        if (!Internet.isAvailable(PhoneAuthActivity.this)) {
            Internet.showAlertDialog(PhoneAuthActivity.this, "Error!", "No Internet Connection", false);
        } else {
            Map<String, String> map = new HashMap<String, String>();
            map.put("url", MyConstants.GUEST_BASE_URL + "login");
            map.put("Phone", countryCode.getSelectedCountryCodeWithPlus() + mPhoneNumberField.getText().toString());
            map.put("Version", MyConstants.WS_VERSION);
            map.put("DeviceType", MyConstants.DEVICE_TYPE);
            map.put("DeviceToken", MyConstants.DEVICE_ID);
            map.put("FirstName", etFirstName.getText().toString().trim());
            map.put("LastName", etLastName.getText().toString().trim());

            new CallRequest(PhoneAuthActivity.this).guestLogin(map);
        }
    }


    private void startPhoneNumberVerification(String phoneNumber) {
        Utils.showSimpleSpinProgressDialog(PhoneAuthActivity.this, "Please Wait");
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                phoneNumber,        // Phone number to verify
                60,                 // Timeout duration
                TimeUnit.SECONDS,   // Unit of timeout
                this,               // Activity (for callback binding)
                mCallbacks);        // OnVerificationStateChangedCallbacks
    }

    private void verifyPhoneNumberWithCode(String verificationId, String code) {
        PhoneAuthCredential credential = PhoneAuthProvider.getCredential(verificationId, code);
        signInWithPhoneAuthCredential(credential, true);
    }

    private void resendVerificationCode(String phoneNumber,
                                        PhoneAuthProvider.ForceResendingToken token) {
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                phoneNumber,        // Phone number to verify
                60,                 // Timeout duration
                TimeUnit.SECONDS,   // Unit of timeout
                this,               // Activity (for callback binding)
                mCallbacks,         // OnVerificationStateChangedCallbacks
                token);             // ForceResendingToken from callbacks
    }

    private boolean validatePhoneNumber() {
        String phoneNumber = mPhoneNumberField.getText().toString();
        if (TextUtils.isEmpty(phoneNumber)) {
            mPhoneNumberField.requestFocus();
            mPhoneNumberField.setError("Phone number should not empty");
            return false;
        }
        return true;
    }

    @Override
    public void onStart() {
        super.onStart();
        FirebaseUser currentUser = mAuth.getCurrentUser();
        /*if (currentUser != null) {
            startActivity(new Intent(PhoneAuthActivity.this, MentionQueryActivity.class));
            finish();
        }*/
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.button_start_verification:
                /*if (TextUtils.isEmpty(etFirstName.getText().toString().trim())) {
                    etFirstName.requestFocus();
                    etFirstName.setError("First name can't be empty");
                    return;
                } else if (TextUtils.isEmpty(etLastName.getText().toString().trim())) {
                    etLastName.requestFocus();
                    etLastName.setError("Last name can't be empty");
                    return;
                } else*/
                    if (!validatePhoneNumber()) {
                    return;
                } else if (mPhoneNumberField.getText().toString().trim().length() != 10){
                    mPhoneNumberField.requestFocus();
                    mPhoneNumberField.setError("Invalid phone number");
                    return;
                }
                System.out.println("code::" + countryCode.getSelectedCountryCodeWithPlus());
                startPhoneNumberVerification(countryCode.getSelectedCountryCodeWithPlus() + mPhoneNumberField.getText().toString());
                break;
            /*case R.id.button_verify_phone:
                String code = mVerificationField.getText().toString();
                if (TextUtils.isEmpty(code)) {
                    mVerificationField.setError("Cannot be empty.");
                    return;
                }

                verifyPhoneNumberWithCode(mVerificationId, code);
                break;
            case R.id.button_resend:
                resendVerificationCode(countryCode.getSelectedCountryCodeWithPlus() + mPhoneNumberField.getText().toString(), mResendToken);
                break;*/
        }

    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        Utils.removeSimpleSpinProgressDialog();
        try {
            if (result != null && !result.isEmpty()) {
                Log.i("TAG", "TAG Result : " + result);
                //      {"document":{"response":{"status":1,"message":"Success."}}}
                switch (request) {
                    case guestLogin:
                        try {
                            JSONObject mainObj = new JSONObject(result);
                            if (mainObj.getString("error_code").equalsIgnoreCase("0")) {
                                //Utils.showToast(mainObj.getString("error_string"), PhoneAuthActivity.this);

                                JSONObject resultObj = mainObj.getJSONObject("result");

                                JSONArray guest = resultObj.getJSONArray("guest");

                                JSONObject guestObj = guest.getJSONObject(0);

                                setUserData(guestObj);


                            } else {
                                Utils.showToast(mainObj.getString("error_string"), PhoneAuthActivity.this);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;

                    case nurseChatDetails:
                        try {
                            JSONObject mainObj = new JSONObject(result);
                            if (mainObj.getString("error_code").equalsIgnoreCase("0")) {
                                //Utils.showToast(mainObj.getString("error_string"), GuestVerifyOTPActivity.this);

                                isChildAvailable(mainObj.getJSONArray("result").getJSONObject(0));
                            } else {
                                Utils.showToast(mainObj.getString("error_string"), PhoneAuthActivity.this);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void isChildAvailable(JSONObject resultObj) {

        try {
            final String id = resultObj.getString("id");
            final String UniqueId = resultObj.getString("UniqueId");
            final String UserId = resultObj.getString("UserId");
            final String NurseId = resultObj.getString("NurseId");
            final String Status = resultObj.getString("Status");
            final String IsGuest = resultObj.getString("IsGuest");
            final String FirstName = resultObj.getString("FirstName");
            final String LastName = resultObj.getString("LastName");
            final String ProfilePic = resultObj.getString("ProfilePic");

            fireDB = FirebaseDatabase.getInstance().getReference().child(MyConstants.NURSE_CHANNEL);

            fireDB.orderByChild(UniqueId).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    Log.i("Data snapshot Nursechat", dataSnapshot.toString());
                    Utils.removeSimpleSpinProgressDialog();
                    Intent intent = new Intent(PhoneAuthActivity.this, NurseFireChatActivity.class);


                    if(dataSnapshot.child(UniqueId).getValue()  == null) {
                        intent.putExtra(MyConstants.IS_PAYMENT_DONE, true);
                    } else {
                        intent.putExtra(MyConstants.IS_PAYMENT_DONE, false);
                    }

                    intent.putExtra(MyConstants.PT_ID, UserId);
                    intent.putExtra(MyConstants.DR_ID, NurseId);
                    intent.putExtra(MyConstants.UNIQUE_CHAT_ID, UniqueId);
                    intent.putExtra("status", Status);
                    intent.putExtra(MyConstants.DR_NAME, FirstName + " " + LastName);
                    intent.putExtra(MyConstants.RECEIVER_IMAGE_URL, ProfilePic);
                    startActivity(intent);
                    finish();
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setUserData(JSONObject guestObj) {
        try {
            String guest_id = guestObj.getString("id");
            String name = guestObj.getString("FirstName") + " " + guestObj.getString("LastName");

            SharedPreferences sharedpreferences = getSharedPreferences(MyConstants.PREF, Context.MODE_PRIVATE);

            SharedPreferences.Editor editor = sharedpreferences.edit();

            editor.putString(MyConstants.USER_TYPE, MyConstants.USER_PT);

            editor.putString(MyConstants.USER_EMAIL, "");
            editor.putString(MyConstants.USER_ID, guest_id);
            //if (!TextUtils.isEmpty(ProfilePic)) {
            editor.putString(MyConstants.PROFILE_PIC, "");
            //} else {
            //    editor.putString(MyConstants.PROFILE_PIC, ProfilePicSocial);
            //}
            editor.putString(MyConstants.PT_ZONE, "");
            editor.putString(MyConstants.PT_NAME, name);
            editor.putString(MyConstants.PT_FIRST_NAME, guestObj.getString("FirstName"));
            editor.putString(MyConstants.PT_LAST_NAME, guestObj.getString("LastName"));
            editor.putString(MyConstants.GENDER, "");
            editor.putString(MyConstants.DATE_OF_BIRTH, "");
            editor.putString(MyConstants.MOBILE, guestObj.getString("Phone"));
            editor.putString(MyConstants.COUNTRY, "");
            editor.putString(MyConstants.ADDRESS, "");
            editor.putString(MyConstants.AGE, "");
            editor.putString(MyConstants.CURRENT_DISCOUNT, "");
            editor.putString(MyConstants.WALLET_BALANCE, "");
            editor.putString(MyConstants.REFER_CODE, "");
            editor.putString(MyConstants.TOKEN, guestObj.getString("ApiToken"));
            editor.putString(MyConstants.SINCH_ID, "gggg" + guest_id);
            editor.putString(MyConstants.STRIPE_ID, guestObj.getString("StripeId"));
            //editor.putBoolean(MyConstants.FIRST_LOGIN, true);
            editor.putBoolean(MyConstants.IS_LOGGED_IN, true);
            editor.putBoolean(MyConstants.IS_GUEST, true);
            editor.putString(MyConstants.LOGIN_TYPE, "0");
            editor.putString(MyConstants.SOCIAL_ID, "0");

            editor.commit();

            App.user.setUserID(guestObj.getString("id"));
            App.user.setName(name);
            App.user.setFirstName(guestObj.getString("FirstName"));
            App.user.setLastName(guestObj.getString("LastName"));
            App.user.setUserEmail("");
            App.user.setPhone(guestObj.getString("Phone"));
            App.user.setUser_Type(MyConstants.USER_PT);
            App.user.setProfileUrl("");
            App.user.setSinch_id("g" + guest_id);
            App.user.setLoginType("0");
            MyConstants.API_TOKEN = guestObj.getString("ApiToken");
            MyConstants.isGuest = "1";
            System.out.println("token::" + MyConstants.API_TOKEN);
            System.out.println("sinch id==" + App.user.getSinch_id());

            /*PTDashboardActivity dashboardActivity = new PTDashboardActivity();
            dashboardActivity.setUserName();*/

            try {
                getSinchServiceInterface().startClient(App.user.getSinch_id());
            } catch (Exception e) {
                e.printStackTrace();
            }

            // reload all data
            MyConstants.specialitiesList.clear();
            MyConstants.isLoadAgain = true;
            MyConstants.bookingHistoryList.clear();
            MyConstants.messageHistoryList.clear();
            MyConstants.userChatList.clear();
            MyConstants.doctorChatList.clear();
            MyConstants.isBookingHistoryLoad = true;
            MyConstants.isMessageHistoryLoad = true;
            MyConstants.userProfile = null;
            MyConstants.isProfileLoad = true;
            MyConstants.doctorProfilePersonal = null;
            MyConstants.isDoctorProfileLoad = true;

            if (MyConstants.CHAT_AS_GUEST) {

                //getNurseChatDetails();

                Intent intent = new Intent(PhoneAuthActivity.this, PTDashboardActivity.class);
                startActivity(intent);
                ActivityCompat.finishAffinity(PhoneAuthActivity.this);

               /* Intent intent = new Intent(GuestVerifyOTPActivity.this, NurseFireChatActivity.class);
                startActivity(intent);
                finish();*/
            } else {
                if (MyConstants.doctorProfile.getDoctorId().equalsIgnoreCase(MyConstants.doctorIdForQuestionnaire)) {
                    Intent intent = new Intent(PhoneAuthActivity.this, DRQuestionnaire1Activity.class);
                    startActivity(intent);
                    finish();

                } else {
                    Intent intent = new Intent(PhoneAuthActivity.this, ConfirmEConsultBookingActivity.class);
                    startActivity(intent);
                    finish();
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void getNurseChatDetails() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", MyConstants.GUEST_BASE_URL + "nurseChat");
        map.put("ApiToken", MyConstants.API_TOKEN);
        map.put("Version", MyConstants.WS_VERSION);
        map.put("GuestId", App.user.getUserID());
        map.put("SocialType", App.user.getLoginType());
        map.put("SocialId", sharedpreferences.getString(MyConstants.SOCIAL_ID, ""));

        new CallRequest(PhoneAuthActivity.this).getNurseChatDetails(map);
    }

    @Override
    public void onProgressUpdate(String uniqueMessageId, int progres) {

    }

    @Override
    public void onProgressComplete(String uniqueMessageId, String result, Constant.REQUESTS request) {

    }
}
