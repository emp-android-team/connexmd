package com.connex.md.patient.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.Toast;

import com.connex.md.R;
import com.connex.md.custom_views.RobottoEditTextView;
import com.connex.md.custom_views.RobottoTextView;
import com.connex.md.custom_views.RobottoTextViewBold;
import com.connex.md.interfaces.AsyncTaskListner;
import com.connex.md.others.App;
import com.connex.md.others.Internet;
import com.connex.md.ws.CallRequest;
import com.connex.md.ws.Constant;
import com.connex.md.ws.MyConstants;
import com.connex.md.ws.Utils;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;

public class RXDeliveryActivity extends AppCompatActivity implements AsyncTaskListner, CompoundButton.OnCheckedChangeListener {

    private static int RESULT_LOAD_IMG = 1;
    private static int RESULT_CROP_DP = 3;

    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    @BindView(R.id.edt_name)
    RobottoEditTextView mNameEdt;

    @BindView(R.id.edt_email)
    RobottoEditTextView mEmailEdt;

    @BindView(R.id.txt_file_name)
    RobottoTextView mFileNameTxt;

    @BindView(R.id.img_preview)
    ImageView mPreviewImg;

    @BindView(R.id.rdo_insurance_no)
    RadioButton mInsuranceNoRdo;

    @BindView(R.id.rdo_insurance_yes)
    RadioButton mInsuranceYesRdo;

    @BindView(R.id.rdo_pharmacy_delivery)
    RadioButton mPharmacyDeliveryRdo;

    @BindView(R.id.rdo_pharmacy_pickup)
    RadioButton mPharmacyPickupRdo;

    @BindView(R.id.txt_address)
    RobottoTextViewBold mAddressTxt;

    private String filePathDp = "", filePathFirst = "", mInsurance = "Yes", mDelivery = "No";

    private SharedPreferences mSharedPreferences;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pediatrician);

        ButterKnife.bind(this);
        mSharedPreferences = getSharedPreferences(MyConstants.PREF, Context.MODE_PRIVATE);

        setListener();
        setData();
        setupToolbar();
    }



    private void setupToolbar() {
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle("Rx Prescription Delivery");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }


    private void setData() {
        mNameEdt.setText(mSharedPreferences.getString(MyConstants.PT_FIRST_NAME, "") + " " + mSharedPreferences.getString(MyConstants.PT_LAST_NAME, ""));
        mEmailEdt.setText(mSharedPreferences.getString(MyConstants.USER_EMAIL, ""));
    }


    private void setListener() {
        mInsuranceNoRdo.setOnCheckedChangeListener(this);
        mInsuranceYesRdo.setOnCheckedChangeListener(this);
        mPharmacyDeliveryRdo.setOnCheckedChangeListener(this);
        mPharmacyPickupRdo.setOnCheckedChangeListener(this);
    }


    private boolean isValid() {
        if (mNameEdt.getText().toString().trim().equals("")) {
            mNameEdt.setError("Please enter your name");
            mNameEdt.requestFocus();
            return false;

        }
        return true;
    }


    public void loadImageFromGallery() {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(galleryIntent, RESULT_LOAD_IMG);
    }


    public static Bitmap decodeSampledBitmapFromPath(String path, int reqWidth, int reqHeight) {
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, options);

        options.inSampleSize = calculateInSampleSize(options, reqWidth,reqHeight);

        options.inJustDecodeBounds = false;
        Bitmap bmp = BitmapFactory.decodeFile(path, options);
        return bmp;
    }


    public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            if (width > height) {
                inSampleSize = Math.round((float) height / (float) reqHeight);
            } else {
                inSampleSize = Math.round((float) width / (float) reqWidth);
            }
        }
        return inSampleSize;
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        try {
            if (requestCode == RESULT_LOAD_IMG && resultCode == RESULT_OK&& null != data) {
                Uri selectedImage = data.getData();
                String[] filePathColumn = {MediaStore.Images.Media.DATA};

                Cursor cursor = getContentResolver().query(selectedImage,filePathColumn, null, null, null);
                int columnIndex = 0;

                if (cursor != null) {
                    cursor.moveToFirst();
                    columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    filePathFirst = cursor.getString(columnIndex);
                    System.out.println(filePathFirst);
                    cursor.close();

                    File imgFile = new  File(filePathFirst);
                    if(imgFile.exists()){
                        mFileNameTxt.setText(imgFile.getName());
                        //Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
                        //mPreviewImg.setImageBitmap(myBitmap);
                    }
                }
            }

            if (requestCode == RESULT_CROP_DP) {
                if (resultCode == Activity.RESULT_OK) {
                    if (!TextUtils.isEmpty(filePathDp)) {
                        Bitmap selectedBitmap = decodeSampledBitmapFromPath(filePathDp,100,100);
                        System.out.println("selectedBitmap:::::" + selectedBitmap);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @OnClick(R.id.btn_submit)
    void onSubmitClick() {
        if (!isValid())
            return;

        startActivity(new Intent(RXDeliveryActivity.this, PaymentActivity.class)
                .putExtra("rxDelivery", "yes")
                .putExtra("totalAmount", 5)
                .putExtra("fullName", mNameEdt.getText().toString().trim())
                .putExtra("email", mEmailEdt.getText().toString().trim())
                .putExtra("privateInsurance", mInsurance)
                .putExtra("Delivery", mDelivery)
                .putExtra("prescriptionImage", filePathFirst));
    }


    @OnClick({R.id.img_attach_file, R.id.txt_file_name})
    void onAttachFileClick() {
        loadImageFromGallery();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }



    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        Utils.removeSimpleSpinProgressDialog();

        try {
            if (result != null && !result.isEmpty()) {
                JSONObject jsonObject = new JSONObject(result);
                Gson gson = new Gson();

                Log.i("TAG", "TAG Result : " + result);

                switch (request) {
                    case bookRxDelivery:
                        if (jsonObject.getString("error_code").equals("0")) {
                            Toast.makeText(RXDeliveryActivity.this, jsonObject.getString("error_string"), Toast.LENGTH_LONG).show();
                            finish();

                        }else {
                            Utils.showAlert(jsonObject.getString("error_string"), RXDeliveryActivity.this);
                        }
                        break;
                }
            }
        }catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onProgressUpdate(String uniqueMessageId, int progres) {

    }

    @Override
    public void onProgressComplete(String uniqueMessageId, String result, Constant.REQUESTS request) {

    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
        switch (compoundButton.getId()) {
            case R.id.rdo_insurance_no:
                if (b)
                    mInsurance = "No";
                break;

            case R.id.rdo_insurance_yes:
                if (b)
                    mInsurance = "Yes";
                break;

            case R.id.rdo_pharmacy_delivery:
                if (b) {
                    mDelivery = "Yes";
                    mAddressTxt.setText(getString(R.string.txt_verify_address));
                }
                break;

            case R.id.rdo_pharmacy_pickup:
                if (b) {
                    mDelivery = "No";
                    mAddressTxt.setText(getString(R.string.txt_pharmacy_address));
                }
                break;
        }
    }
}
