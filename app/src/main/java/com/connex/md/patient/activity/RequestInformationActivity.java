package com.connex.md.patient.activity;

import android.annotation.SuppressLint;
import android.app.DownloadManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.MenuItem;
import android.widget.RatingBar;
import android.widget.Toast;

import com.connex.md.R;
import com.connex.md.custom_views.CircleImageView;
import com.connex.md.custom_views.RobottoEditTextView;
import com.connex.md.custom_views.RobottoTextView;
import com.connex.md.custom_views.RobottoTextViewBold;
import com.connex.md.interfaces.AsyncTaskListner;
import com.connex.md.model.DoctorProfile;
import com.connex.md.model.TimeSlotsData;
import com.connex.md.others.App;
import com.connex.md.others.Internet;
import com.connex.md.ws.CallRequest;
import com.connex.md.ws.Constant;
import com.connex.md.ws.MyConstants;
import com.connex.md.ws.Utils;
import com.crashlytics.android.Crashlytics;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.fabric.sdk.android.Fabric;

public class RequestInformationActivity extends AppCompatActivity implements AsyncTaskListner {

    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    @BindView(R.id.collapsing_toolbar)
    CollapsingToolbarLayout mToolbarLayout;

    @BindView(R.id.edt_name)
    RobottoEditTextView mNameEdt;

    @BindView(R.id.edt_email)
    RobottoEditTextView mEmailEdt;

    @BindView(R.id.edt_phone)
    RobottoEditTextView mPhoneEdt;

    @BindView(R.id.edt_comments)
    RobottoEditTextView mCommentsEdt;

    @BindView(R.id.txt_fee)
    RobottoTextViewBold mFeeTxt;

    @BindView(R.id.txt_location)
    RobottoTextViewBold mLocationTxt;

    @BindView(R.id.txt_doctor_name)
    RobottoTextViewBold mDoctorNameTxt;

    @BindView(R.id.txt_speciality)
    RobottoTextView mSpecialityTxt;

    @BindView(R.id.img_profile)
    CircleImageView mProfileImg;

    @BindView(R.id.ratingbar)
    RatingBar mRatingBar;

    @BindView(R.id.txt_total_ratings)
    RobottoTextView mTotalRatingTxt;

    private SharedPreferences mSharedPreferences;
    private DoctorProfile mDoctorProfileData;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_request_information);

        Fabric.with(this, new Crashlytics());
        ButterKnife.bind(this);

        mSharedPreferences = getSharedPreferences(MyConstants.PREF, Context.MODE_PRIVATE);

        setupToolbar();

        if (getIntent().getExtras() != null) {
            mDoctorProfileData = (DoctorProfile) getIntent().getExtras().get("DOCTOR_PROFILE");
            setDoctorData();
        }
    }


    private void setupToolbar() {
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle("Request Information");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }


    private boolean isValid() {
        if (mNameEdt.getText().toString().trim().equals("")) {
            mNameEdt.setError("Please enter name");
            mNameEdt.requestFocus();
            return false;

        }else if (mEmailEdt.getText().toString().trim().equals("")) {
            mNameEdt.setError("Please enter Email");
            mNameEdt.requestFocus();
            return false;

        }else if (mPhoneEdt.getText().toString().trim().equals("")) {
            mNameEdt.setError("Please enter Phone");
            mNameEdt.requestFocus();
            return false;
        }
        return true;
    }


    @SuppressLint("SetTextI18n")
    private void setDoctorData() {
        mDoctorNameTxt.setText(mDoctorProfileData.getFirstName() + " " + mDoctorProfileData.getLastName());
        mSpecialityTxt.setText(mDoctorProfileData.getSpeciality());
        mFeeTxt.setText("$" + mDoctorProfileData.getCosultCharge() + " (" + Html.fromHtml(mDoctorProfileData.getLocalCharge()) + ")");
        mLocationTxt.setText(mDoctorProfileData.getCountry());

        mRatingBar.setRating(Float.valueOf(mDoctorProfileData.getRating()));
        mTotalRatingTxt.setText("(" + mDoctorProfileData.getTotalRating() + ") Ratings");

        mNameEdt.setText(mSharedPreferences.getString(MyConstants.PT_FIRST_NAME, "") + " " + mSharedPreferences.getString(MyConstants.PT_LAST_NAME, ""));
        mEmailEdt.setText(mSharedPreferences.getString(MyConstants.USER_EMAIL, ""));

        Picasso.with(RequestInformationActivity.this)
                .load(mDoctorProfileData.getProfilePic())
                .placeholder(R.drawable.avatar)
                .error(R.drawable.avatar)
                .into(mProfileImg);
    }


    private void requestInformation() {
        if (!Internet.isAvailable(this)) {
            Internet.showAlertDialog(this, "Error!", "No Internet Connection", false);
            return;
        }

        Map<String, String> map = new HashMap<String, String>();
        map.put("url", MyConstants.BASE_URL + "requestForInformation");
        map.put("SocialId", mSharedPreferences.getString(MyConstants.SOCIAL_ID, "0"));
        map.put("SocialType", mSharedPreferences.getString(MyConstants.LOGIN_TYPE, "0"));
        map.put("Version", MyConstants.WS_VERSION);
        map.put("UserId", App.user.getUserID());
        map.put("ApiToken", MyConstants.API_TOKEN);
        map.put("DoctorId", "");
        map.put("FullName", mNameEdt.getText().toString().trim());
        map.put("Email", mEmailEdt.getText().toString().trim());
        map.put("PhoneNumberToReach", mPhoneEdt.getText().toString().trim());
        map.put("AdditionalInfo", mCommentsEdt.getText().toString().trim());

        new CallRequest(RequestInformationActivity.this).requestForInformation(map);
    }


    @OnClick(R.id.btn_book_appointment)
    void onBookAppointmentClick() {
        if (!isValid())
            return;

        requestInformation();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        Utils.removeSimpleSpinProgressDialog();

        try {
            if (result != null && !result.isEmpty()) {
                JSONObject jsonObject = new JSONObject(result);
                Gson gson = new Gson();

                Log.i("TAG", "TAG Result : " + result);

                switch (request) {
                    case requestForInformation:
                        if (jsonObject.getString("error_code").equals("")) {
                            Toast.makeText(RequestInformationActivity.this, jsonObject.getString("error_string"), Toast.LENGTH_LONG).show();
                            Intent intent = new Intent(RequestInformationActivity.this, PTDashboardActivity.class);
                            startActivity(intent);
                            ActivityCompat.finishAffinity(RequestInformationActivity.this);

                        }else if (jsonObject.getString("error_code").equals("")) {
                            Utils.showAlert(jsonObject.getString("error_string"), RequestInformationActivity.this);
                        }
                        break;
                }
            }
        }catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onProgressUpdate(String uniqueMessageId, int progres) {

    }

    @Override
    public void onProgressComplete(String uniqueMessageId, String result, Constant.REQUESTS request) {

    }
}
