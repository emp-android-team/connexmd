package com.connex.md.patient.activity;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.connex.md.custom_views.CenteredToolbar;
import com.crashlytics.android.Crashlytics;
import com.connex.md.custom_views.RobottoTextViewBold;
import com.connex.md.others.PicassoTrustAll;
import com.connex.md.R;
import com.connex.md.model.SearchDoctor;
import com.connex.md.model.Specialities;
import com.connex.md.patient.fragment.EConsultationFragment;
import com.connex.md.patient.fragment.InPersonVisitFragment;
import com.connex.md.ws.MyConstants;
import com.squareup.picasso.Callback;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.fabric.sdk.android.Fabric;

public class SearchDoctorActivity extends AppCompatActivity {

    @BindView(R.id.tab_layout)
    TabLayout mTabLayout;

    @BindView(R.id.view_pager)
    ViewPager mViewPager;

    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    RecyclerView rvSearchDoctor;
    ImageView ivBanner;
    TextView tvCategory;
    public Specialities specialities;
    public Dialog dialog;
    TextView  tvNoDoctors;

    private RobottoTextViewBold tabOne, tabTwo;

    private ArrayList<SearchDoctor> mEConsultDrArr = new ArrayList<>();
    private ArrayList<SearchDoctor> mInPersonVisitArr = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_doctor);

        Fabric.with(this, new Crashlytics());
        ButterKnife.bind(this);

        rvSearchDoctor = findViewById(R.id.rvSearchDoctor);
        ivBanner = findViewById(R.id.ivBanner);
        tvCategory = findViewById(R.id.tvCategory);
        tvNoDoctors = findViewById(R.id.tvNoDoctors);

        if (getIntent().getExtras() != null) {
            specialities = (Specialities) getIntent().getExtras().getSerializable("speciality");
            setData();
        }

        try {
            PicassoTrustAll.getInstance(SearchDoctorActivity.this)
                    .load(specialities.getImage())
                    .error(R.drawable.no_img)
                    .into(ivBanner, new Callback() {
                        @Override
                        public void onSuccess() {
                        }

                        @Override
                        public void onError() {
                        }
                    });
        } catch (Exception e){
            e.printStackTrace();
        }

        tvCategory.setText(specialities.getSpeciality());

        setupToolbar();
        setupViewPager();
        setupTabIcons();
    }


    private void setupToolbar() {
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle(specialities.getSpeciality());
        //getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }


    /**
     * Add data to array depends on doctor type..
     */
    private void setData() {
        for (int i = 0; i < specialities.getSearchDoctorList().size(); i++) {
            ArrayList<String> drTypes = new ArrayList<>(Arrays.asList(specialities.getSearchDoctorList().get(i).getDotorType().split(",")));

            if (drTypes.contains("1"))
                mEConsultDrArr.add(specialities.getSearchDoctorList().get(i));

            if (drTypes.contains("0") || drTypes.contains("2"))
                mInPersonVisitArr.add(specialities.getSearchDoctorList().get(i));

            /*if (drType[0].equals("1"))
                mEConsultDrArr.add(specialities.getSearchDoctorList().get(i));
            else if (drType[0].equals("2"))
                mInPersonVisitArr.add(specialities.getSearchDoctorList().get(i));

            if (drType.length > 1){
                if (drType[1].equals("1"))
                    mEConsultDrArr.add(specialities.getSearchDoctorList().get(i));
                else if (drType[1].equals("2"))
                    mInPersonVisitArr.add(specialities.getSearchDoctorList().get(i));
            }*/
        }
    }


    private void setupTabIcons() {
        tabOne = (RobottoTextViewBold) LayoutInflater.from(this).inflate(R.layout.layout_doctor_custom_tab, null);
        tabOne.setText("eConsultation");
        tabOne.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_e_consultation, 0, 0, 0);
        mTabLayout.getTabAt(0).setCustomView(tabOne);

        tabTwo = (RobottoTextViewBold) LayoutInflater.from(this).inflate(R.layout.layout_doctor_custom_tab, null);
        tabTwo.setText("In-Person Visit");
        tabTwo.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_location_pin_unselect, 0, 0, 0);
        tabTwo.setTextColor(Color.parseColor("#990C6C7A"));

        mTabLayout.getTabAt(1).setCustomView(tabTwo);
    }


    private void setupViewPager() {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());

        EConsultationFragment eConsultationFragment = new EConsultationFragment();
        Bundle eConsultationBundle = new Bundle();
        eConsultationBundle.putSerializable("doctors", mEConsultDrArr);
        eConsultationFragment.setArguments(eConsultationBundle);
        adapter.addFrag(eConsultationFragment, "eConsultation");

        InPersonVisitFragment inPersonVisitFragment = new InPersonVisitFragment();
        Bundle inPersonBundle = new Bundle();
        inPersonBundle.putSerializable("doctors", mInPersonVisitArr);
        inPersonVisitFragment.setArguments(inPersonBundle);
        adapter.addFrag(inPersonVisitFragment, "In-Person Visit");
        mViewPager.setAdapter(adapter);
        mTabLayout.setupWithViewPager(mViewPager);

        mTabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if (tab.getPosition() == 0) {
                    tabOne.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_e_consultation, 0, 0, 0);
                    tabOne.setTextColor(Color.parseColor("#0C6C7A"));

                    tabTwo.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_location_pin_unselect, 0, 0, 0);
                    tabTwo.setTextColor(Color.parseColor("#990C6C7A"));

                }else if (tab.getPosition() == 1) {
                    tabOne.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_e_consultation_unselect, 0, 0, 0);
                    tabOne.setTextColor(Color.parseColor("#990C6C7A"));

                    tabTwo.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_location_pin, 0, 0, 0);
                    tabTwo.setTextColor(Color.parseColor("#0C6C7A"));
                }

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }


    @OnClick(R.id.ivBack)
    void onBackClick() {
        onBackPressed();
    }


    @Override
    public void onBackPressed() {
        MyConstants.isBackPressed = true;

        try {
            InputMethodManager inputMethodManager = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            if (inputMethodManager != null) {
                inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
            }
        } catch (Exception e){
            e.printStackTrace();
        }
        super.onBackPressed();
    }


    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }

        return super.onOptionsItemSelected(item);
    }
}
