package com.connex.md.patient.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;

import com.connex.md.R;

import com.connex.md.custom_views.RobottoTextView;
import com.connex.md.interfaces.AsyncTaskListner;
import com.connex.md.model.Symptoms;
import com.connex.md.others.App;
import com.connex.md.patient.adapter.SymptomsAdapter;
import com.connex.md.ws.CallRequest;
import com.connex.md.ws.Constant;
import com.connex.md.ws.JsonParserUniversal;
import com.connex.md.ws.MyConstants;
import com.connex.md.ws.Utils;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import io.fabric.sdk.android.Fabric;

public class SymptomsSelectionActivity extends Activity implements AsyncTaskListner {

    public RecyclerView mRecyclerView1, mRecyclerView2, mRecyclerView3, mRecyclerView4, mRecyclerView5;
    public RecyclerView.LayoutManager mLayoutManager1, mLayoutManager2, mLayoutManager3, mLayoutManager4, mLayoutManager5;
    public JsonParserUniversal jParser;
    public SymptomsAdapter sAdapter1, sAdapter2, sAdapter3, sAdapter4, sAdapter5;
    public ArrayList<ArrayList<Symptoms>> sympomsArray;
    public RobottoTextView tvGo, tvSkip;
    public String symptomsIDS = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_main);
        Utils.logUser();

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        jParser = new JsonParserUniversal();

        new CallRequest(this).getSymptoms();

        mRecyclerView1 = findViewById(R.id.recycler_view1);
        mRecyclerView2 = findViewById(R.id.recycler_view2);
        mRecyclerView3 = findViewById(R.id.recycler_view3);
        mRecyclerView4 = findViewById(R.id.recycler_view4);
        mRecyclerView5 = findViewById(R.id.recycler_view5);
        tvGo = findViewById(R.id.tvGo);
        tvSkip = findViewById(R.id.tvSkip);
        mRecyclerView1.setVisibility(View.GONE);
        mRecyclerView2.setVisibility(View.GONE);
        mRecyclerView3.setVisibility(View.GONE);
        mRecyclerView4.setVisibility(View.GONE);
        mRecyclerView5.setVisibility(View.GONE);

        mRecyclerView1.setHasFixedSize(true);

        // The number of Columns
        mLayoutManager1 = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        mLayoutManager2 = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        mLayoutManager3 = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        mLayoutManager4 = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        mLayoutManager5 = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);

        mRecyclerView1.setLayoutManager(mLayoutManager1);
        mRecyclerView1.setHasFixedSize(true);
        mRecyclerView2.setLayoutManager(mLayoutManager2);
        mRecyclerView2.setHasFixedSize(true);
        mRecyclerView3.setLayoutManager(mLayoutManager3);
        mRecyclerView3.setHasFixedSize(true);
        mRecyclerView4.setLayoutManager(mLayoutManager4);
        mRecyclerView4.setHasFixedSize(true);
        mRecyclerView5.setLayoutManager(mLayoutManager5);
        mRecyclerView5.setHasFixedSize(true);

        RecyclerView.OnScrollListener scrollListener = new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                mRecyclerView1.removeOnScrollListener(this);
                mRecyclerView1.scrollBy(dx, 0);
                mRecyclerView1.addOnScrollListener(this);

                mRecyclerView2.removeOnScrollListener(this);
                mRecyclerView2.scrollBy(dx, 0);
                mRecyclerView2.addOnScrollListener(this);

                mRecyclerView3.removeOnScrollListener(this);
                mRecyclerView3.scrollBy(dx, 0);
                mRecyclerView3.addOnScrollListener(this);

                mRecyclerView4.removeOnScrollListener(this);
                mRecyclerView4.scrollBy(dx, 0);
                mRecyclerView4.addOnScrollListener(this);

                mRecyclerView5.removeOnScrollListener(this);
                mRecyclerView5.scrollBy(dx, 0);
                mRecyclerView5.addOnScrollListener(this);

               /* if (mRecyclerView1 == mRecyclerView2) {
                    mRecyclerView1.removeOnScrollListener(this);
                    mRecyclerView1.scrollBy(dx, 0);
                    mRecyclerView1.addOnScrollListener(this);
                } else if (recyclerView == recycler_view2) {
                    mRecyclerView2.removeOnScrollListener(this);
                    mRecyclerView2.scrollBy(dx, 0);
                    mRecyclerView2.addOnScrollListener(this);
                }*/
            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }
        };

        mRecyclerView1.addOnScrollListener(scrollListener);

        tvGo.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                symptomsIDS = "";

                if (isValidated()) {
                    Map<String, String> map = new HashMap<String, String>();
                    map.put("url", MyConstants.BASE_URL + "userSymptoms");
                    map.put("Version", MyConstants.WS_VERSION);
                    map.put("ApiToken", MyConstants.API_TOKEN);
                    map.put("UserId", App.user.getUserID());
                    map.put("Symptoms", symptomsIDS);
                    map.put("IsDelete", "0");
                    map.put("DeleteId", "0");
                    new CallRequest(SymptomsSelectionActivity.this).setSymptoms(map);
                } else {
                    Utils.showToast("Please select Symptoms!",SymptomsSelectionActivity.this);
                }
            }
        });

        tvSkip.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(SymptomsSelectionActivity.this, PTDashboardActivity.class);
                startActivity(intent);
                ActivityCompat.finishAffinity(SymptomsSelectionActivity.this);
            }
        });

    }

    public boolean isValidated() {
        ArrayList<String> IDS = new ArrayList<>();

        for (ArrayList<Symptoms> a : sympomsArray) {
            for (Symptoms s : a) {
                if (s.selected) {
                    IDS.add(String.valueOf(s.getId()));
                }
            }
        }
        if (IDS.size() > 0) {
            symptomsIDS = android.text.TextUtils.join(",", IDS);
            return true;
        } else {
            return false;
        }

    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        Log.i("", "RESULT : " + result);
        Utils.removeSimpleSpinProgressDialog();
        if (result != null && !result.isEmpty()) {
            try {

                switch (request) {
                    case getSymptoms:
                        JSONObject jObj = new JSONObject(result);

                        if (jObj.getString("error_code").equalsIgnoreCase("0")) {
                            JSONArray resultArray = jObj.getJSONArray("result");

                            if (resultArray != null && resultArray.length() > 0) {
                                int length = resultArray.length();
                                sympomsArray = new ArrayList<ArrayList<Symptoms>>();
                                for (int i = 0; i < resultArray.length(); i++) {
                                    if (resultArray.getJSONArray(i).length() > 0) {
                                        Symptoms sBean;
                                        ArrayList<Symptoms> sArray = new ArrayList<Symptoms>();
                                        for (int j = 0; j < resultArray.getJSONArray(i).length(); j++) {
                                            sBean = (Symptoms) jParser.parseJson(resultArray.getJSONArray(i).getJSONObject(j),
                                                    new Symptoms());
                                            sArray.add(sBean);

                                        }
                                        sympomsArray.add(sArray);
                                    }
                                }


                                if (length > 0) {
                                    sAdapter1 = new SymptomsAdapter(this, sympomsArray.get(0));
                                    mRecyclerView1.setVisibility(View.VISIBLE);
                                    mRecyclerView1.setAdapter(sAdapter1);
                                }

                                if (length > 1) {
                                    sAdapter2 = new SymptomsAdapter(this, sympomsArray.get(1));
                                    mRecyclerView2.setVisibility(View.VISIBLE);
                                    mRecyclerView2.setAdapter(sAdapter2);
                                }

                                if (length > 2) {
                                    sAdapter3 = new SymptomsAdapter(this, sympomsArray.get(2));
                                    mRecyclerView3.setVisibility(View.VISIBLE);
                                    mRecyclerView3.setAdapter(sAdapter3);
                                }

                                if (length > 3) {
                                    sAdapter4 = new SymptomsAdapter(this, sympomsArray.get(3));
                                    mRecyclerView4.setVisibility(View.VISIBLE);
                                    mRecyclerView4.setAdapter(sAdapter4);
                                }
                                if (length > 4) {
                                    sAdapter5 = new SymptomsAdapter(this, sympomsArray.get(4));
                                    mRecyclerView5.setVisibility(View.VISIBLE);
                                    mRecyclerView5.setAdapter(sAdapter5);
                                }

                            }


                        } else {
                            Utils.showToast("Please try again later", this);
                        }
                        break;

                    case setSymptoms:

                        jObj = new JSONObject(result);
                        if (jObj.getString("error_code").equalsIgnoreCase("0")) {

                            Toast.makeText(this, "We have successfully saved your most likely symptoms", Toast.LENGTH_LONG).show();
                            Intent intent = new Intent(this, PTDashboardActivity.class);
                            this.startActivity(intent);
                            ActivityCompat.finishAffinity(SymptomsSelectionActivity.this);
                        }

                        break;


                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            Utils.showToast("Please try again later", this);
        }
    }

    @Override
    public void onProgressUpdate(String uniqueMessageId, int progres) {

    }

    @Override
    public void onProgressComplete(String uniqueMessageId, String result, Constant.REQUESTS request) {

    }

    @Override
    public void onBackPressed() {
        Utils.showToast("Please select symptoms", SymptomsSelectionActivity.this);
    }


}

	


	