package com.connex.md.patient.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;

import com.connex.md.R;
import com.connex.md.custom_views.CircleImageView;
import com.connex.md.custom_views.RobottoTextView;
import com.connex.md.custom_views.RobottoTextViewBold;
import com.connex.md.model.DoctorProfile;
import com.connex.md.others.PicassoTrustAll;
import com.connex.md.ws.MyConstants;
import com.squareup.picasso.Callback;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;

public class ThankYouActivity extends AppCompatActivity {

    @BindView(R.id.img_profile)
    CircleImageView mProfileImg;

    @BindView(R.id.txt_consult_time)
    RobottoTextViewBold mConsultTimeTxt;

    @BindView(R.id.txt_doctor_name)
    RobottoTextViewBold mDrNameTxt;

    private DoctorProfile doctorProfile;
    private String mConsultTime;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_thank_you);
        ButterKnife.bind(this);

        if (getIntent().getExtras() != null) {
            doctorProfile = (DoctorProfile) getIntent().getExtras().get("DOCTOR_PROFILE");
            mConsultTime = getIntent().getStringExtra("ConsultTime");
            setData();
        }

    }


    @SuppressLint("SetTextI18n")
    private void setData() {
        mDrNameTxt.setText(doctorProfile.getFirstName() + " " + doctorProfile.getLastName());
        mConsultTimeTxt.setText(mConsultTime);
        PicassoTrustAll.getInstance(ThankYouActivity.this)
                .load(doctorProfile.getProfilePic())
                .placeholder(R.drawable.avatar)
                .error(R.drawable.avatar)
                .into(mProfileImg, new Callback() {
                    @Override
                    public void onSuccess() {
                    }

                    @Override
                    public void onError() {
                    }
                });
    }

    @OnClick(R.id.btn_back_to_dashboard)
    void onBackToDashboardClick() {
        MyConstants.specialitiesList.clear();
        Intent intent = new Intent(ThankYouActivity.this, PTDashboardActivity.class);
        startActivity(intent);
        ActivityCompat.finishAffinity(ThankYouActivity.this);
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        MyConstants.specialitiesList.clear();
        Intent intent = new Intent(ThankYouActivity.this, PTDashboardActivity.class);
        startActivity(intent);
        ActivityCompat.finishAffinity(ThankYouActivity.this);
    }
}
