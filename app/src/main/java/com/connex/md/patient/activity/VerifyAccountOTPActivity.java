package com.connex.md.patient.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.alimuzaffar.lib.pin.PinEntryEditText;
import com.connex.md.R;
import com.connex.md.interfaces.AsyncTaskListner;
import com.connex.md.others.App;
import com.connex.md.others.Internet;
import com.connex.md.ws.CallRequest;
import com.connex.md.ws.Constant;
import com.connex.md.ws.MyConstants;
import com.connex.md.ws.Utils;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.sinch.android.rtc.PushTokenRegistrationCallback;
import com.sinch.android.rtc.SinchError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import static com.connex.md.audio_video_calling.BaseActivity.getSinchServiceInterface;

public class VerifyAccountOTPActivity extends AppCompatActivity implements AsyncTaskListner, PushTokenRegistrationCallback {

    PinEntryEditText pinView;
    Button btnVerifyAccount;
    String OTP, user_id, user_type, email, phone;
    JSONObject userObj;
    TextView tvVerifiedLink;
    String social_id = "0";
    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verify_account_otp);

        pinView = findViewById(R.id.pinview);
        tvVerifiedLink = findViewById(R.id.tvVerifiedLink);
        btnVerifyAccount = findViewById(R.id.btnResetPassword);

        mAuth = FirebaseAuth.getInstance();

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            try {
                userObj = new JSONObject(bundle.getString("user_data"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        btnVerifyAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    if (pinView.getText().toString().length() != 6){
                        pinView.setError("Enter 6 digit password");
                        pinView.requestFocus();
                        return;
                    }

                    callVerifyAccountOTPApi();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

        tvVerifiedLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    callVerifyAccountUsingLink();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

        /*btnSendAgain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callResetPasswordAPI();
            }
        });*/
    }

    private void callVerifyAccountUsingLink() throws JSONException {

        if (!Internet.isAvailable(this)) {
            Internet.showAlertDialog(this, "Error!", "No Internet Connection", false);

            return;
        }

        Map<String, String> map = new HashMap<String, String>();
        map.put("url", MyConstants.BASE_URL + "checkUserVerify");
        map.put("UserId", userObj.getJSONArray("user").getJSONObject(0).getString("id"));
        map.put("Version", MyConstants.WS_VERSION);

        new CallRequest(VerifyAccountOTPActivity.this).isUserVerified(map);
    }


    private void callVerifyAccountOTPApi() throws JSONException {

        if (!Internet.isAvailable(this)) {
            Internet.showAlertDialog(this, "Error!", "No Internet Connection", false);

            return;
        }

        OTP = pinView.getText().toString();
        System.out.println("otp::" + OTP);

        if (TextUtils.isEmpty(OTP)) {
            Utils.showToast("Please enter valid OTP", VerifyAccountOTPActivity.this);
        } else {

            Map<String, String> map = new HashMap<String, String>();
            map.put("url", MyConstants.BASE_URL + "verifyUser");
            map.put("ApiToken", userObj.getJSONArray("user").getJSONObject(0).getString("ApiToken"));
            map.put("UserId", userObj.getJSONArray("user").getJSONObject(0).getString("id"));
            map.put("Version", MyConstants.WS_VERSION);
            map.put("Token", OTP);

            new CallRequest(VerifyAccountOTPActivity.this).verifyAccount(map);
        }
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        Utils.removeSimpleSpinProgressDialog();
        try {
            if (result != null && !result.isEmpty()) {
                Log.i("TAG", "TAG Result : " + result);
                //      {"document":{"response":{"status":1,"message":"Success."}}}
                switch (request) {
                    case verifyAccount:
                        try {
                            JSONObject mainObj = new JSONObject(result);
                            if (mainObj.getString("error_code").equalsIgnoreCase("0")) {

                                setUserData(userObj);

                                //signUpWithGoogle();

                                //Utils.showToast(mainObj.getString("error_string"), ResetPasswordActivity.this);

                            } else {
                                Utils.showToast(mainObj.getString("error_string"), VerifyAccountOTPActivity.this);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;

                    case isVerified:
                        try {
                            JSONObject mainObj = new JSONObject(result);
                            if (mainObj.getString("error_code").equalsIgnoreCase("0")) {

                                JSONObject object = mainObj.getJSONArray("result").getJSONObject(0);

                                int IsVerified = object.getInt("IsVerified");

                                if (IsVerified == 1){
                                    setUserData(userObj);
                                } else {
                                    Utils.showToast("Please verify your account", VerifyAccountOTPActivity.this);
                                }

                                //Utils.showToast(mainObj.getString("error_string"), ResetPasswordActivity.this);

                            } else {
                                Utils.showToast(mainObj.getString("error_string"), VerifyAccountOTPActivity.this);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void signUpWithGoogle() {

        FirebaseAuth.getInstance().signOut();

        Utils.showSimpleSpinProgressDialog(VerifyAccountOTPActivity.this, "Authenticating...");

        try {
            String email = userObj.getJSONArray("user").getJSONObject(0).getString("email");

            byte[] data = new byte[0];
            try {
                data = email.getBytes("UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            String password = Base64.encodeToString(data, Base64.DEFAULT);

            mAuth.createUserWithEmailAndPassword(email, password)
                    .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            Log.d("Firebase Auth:", "createUser:onComplete:" + task.isSuccessful());
                            Utils.hideProgressDialog();

                            if (task.isSuccessful()) {

                                setUserData(userObj);

                                //onAuthSuccess(task.getResult().getUser());
                            } else {
                                /*Toast.makeText(VerifyAccountOTPActivity.this, "Sign Up Failed",
                                        Toast.LENGTH_SHORT).show();*/

                                Snackbar snackbar = Snackbar
                                        .make(findViewById(R.id.root), "Authentication Failed", Snackbar.LENGTH_LONG)
                                        .setAction("Try Again", new View.OnClickListener() {
                                            @Override
                                            public void onClick(View view) {
                                                signUpWithGoogle();
                                            }
                                        });

                                snackbar.show();
                            }
                        }
                    });

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onProgressUpdate(String uniqueMessageId, int progres) {

    }

    @Override
    public void onProgressComplete(String uniqueMessageId, String result, Constant.REQUESTS request) {

    }

    @Override
    public void onBackPressed() {
        Utils.showToast("Please verify your account", VerifyAccountOTPActivity.this);
    }

    private void setUserData(JSONObject obj) {
        try {
            JSONArray userArray = obj.getJSONArray("user");
            JSONObject user_Object = userArray.getJSONObject(0);
            String user_id = user_Object.getString("id");
            String email = user_Object.getString("email");
            String ApiToken = user_Object.getString("ApiToken");
            String Latitude = user_Object.getString("Latitude");
            String Longitude = user_Object.getString("Longitude");
            String ReferCode = user_Object.getString("ReferCode");
            String StripeId = user_Object.getString("StripeId");

            JSONArray user_detailArray = obj.getJSONArray("user_detail");
            JSONObject user_detail_object = user_detailArray.getJSONObject(0);
            String FirstName = user_detail_object.getString("FirstName");
            String LastName = user_detail_object.getString("LastName");
            String Gender = user_detail_object.getString("Gender");
            String CountryId = user_detail_object.getString("CountryId");
            String StateId = user_detail_object.getString("StateId");
            String CountryCode = user_detail_object.getString("CountryCode");
            String Country = user_detail_object.getString("Country");
            String State = user_detail_object.getString("State");
            String Phone = user_detail_object.getString("Phone");
            String BirthDate = user_detail_object.getString("BirthDate");
            String Age = user_detail_object.getString("Age");
            String Address = user_detail_object.getString("Address");
            String Timezone = user_detail_object.getString("Timezone");
            String ProfilePic = user_detail_object.getString("ProfilePic");
            //String ProfilePicSocial = user_detail_object.getString("ProfilePicSocial");
            String CoverPic = user_detail_object.getString("CoverPic");
            String CurrentDiscount = user_detail_object.getString("CurrentDiscount");
            String WalletBalance = user_detail_object.getString("WalletBalance");

            JSONArray user_device = obj.getJSONArray("user_device");

            String name = FirstName + " " + LastName;

            SharedPreferences sharedpreferences = getSharedPreferences(MyConstants.PREF, Context.MODE_PRIVATE);

            SharedPreferences.Editor editor = sharedpreferences.edit();

            editor.putString(MyConstants.USER_TYPE, MyConstants.USER_PT);
            editor.putString(MyConstants.USER_EMAIL, email);
            editor.putString(MyConstants.USER_ID, user_id);
            editor.putString(MyConstants.PROFILE_PIC, ProfilePic);
            editor.putString(MyConstants.PT_ZONE, Timezone);
            editor.putString(MyConstants.PT_NAME, name);
            editor.putString(MyConstants.PT_FIRST_NAME, FirstName);
            editor.putString(MyConstants.PT_LAST_NAME, LastName);
            editor.putString(MyConstants.GENDER, Gender);
            editor.putString(MyConstants.DATE_OF_BIRTH, BirthDate);
            editor.putString(MyConstants.MOBILE, Phone);
            editor.putString(MyConstants.COUNTRY, Country);
            editor.putString(MyConstants.ADDRESS, Address);
            editor.putString(MyConstants.AGE, Age);
            editor.putString(MyConstants.CURRENT_DISCOUNT, CurrentDiscount);
            editor.putString(MyConstants.WALLET_BALANCE, WalletBalance);
            editor.putString(MyConstants.REFER_CODE, ReferCode);
            editor.putString(MyConstants.TOKEN, ApiToken);
            editor.putString(MyConstants.SINCH_ID, "pppp" + user_id);
            editor.putString(MyConstants.STRIPE_ID, StripeId);
            editor.putBoolean(MyConstants.FIRST_LOGIN, true);
            editor.putBoolean(MyConstants.IS_LOGGED_IN, true);
            editor.putString(MyConstants.LOGIN_TYPE, "0");
            editor.putString(MyConstants.SOCIAL_ID, social_id);
            editor.putBoolean(MyConstants.IS_GUEST, false);

            editor.commit();

            App.user.setUserID(user_id);
            App.user.setName(name);
            App.user.setFirstName(FirstName);
            App.user.setLastName(LastName);
            App.user.setUserEmail(email);
            App.user.setPhone(Phone);
            App.user.setUser_Type(MyConstants.USER_PT);
            App.user.setProfileUrl(sharedpreferences.getString(MyConstants.PROFILE_PIC, ""));
            App.user.setSinch_id(sharedpreferences.getString(MyConstants.SINCH_ID, ""));
            App.user.setLoginType("0");
            MyConstants.API_TOKEN = ApiToken;
            MyConstants.isGuest = "0";

            System.out.println("token::" + MyConstants.API_TOKEN);
            getSinchServiceInterface().startClient(App.user.getSinch_id());
            getSinchServiceInterface().registerPushToken(this);

            System.out.println("data saved successfully");

            // reload all data
            MyConstants.specialitiesList.clear();
            MyConstants.isLoadAgain = true;
            MyConstants.bookingHistoryList.clear();
            MyConstants.messageHistoryList.clear();
            MyConstants.userChatList.clear();
            MyConstants.doctorChatList.clear();
            MyConstants.isBookingHistoryLoad = true;
            MyConstants.isMessageHistoryLoad = true;
            MyConstants.userProfile = null;
            MyConstants.isProfileLoad = true;
            MyConstants.doctorProfilePersonal = null;
            MyConstants.isDoctorProfileLoad = true;

            Intent intent = new Intent(VerifyAccountOTPActivity.this, SymptomsSelectionActivity.class);
            startActivity(intent);
            finish();

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void tokenRegistered() {

    }

    @Override
    public void tokenRegistrationFailed(SinchError sinchError) {

    }
}
