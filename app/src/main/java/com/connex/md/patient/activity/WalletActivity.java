package com.connex.md.patient.activity;

import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.connex.md.R;
import com.connex.md.custom_views.CenteredToolbar;
import com.connex.md.interfaces.AsyncTaskListner;
import com.connex.md.model.WalletHistory;
import com.connex.md.others.App;
import com.connex.md.others.Internet;
import com.connex.md.patient.fragment.WalletPaidFragment;
import com.connex.md.patient.fragment.WalletReceivedFragment;
import com.connex.md.ws.CallRequest;
import com.connex.md.ws.Constant;
import com.connex.md.ws.MyConstants;
import com.connex.md.ws.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

public class WalletActivity extends AppCompatActivity implements AsyncTaskListner{

    @BindView(R.id.toolbar)
    CenteredToolbar mToolbar;

    private TabLayout tabLayout;
    private ViewPager viewPager;
    private TextView tvBalance;
    private Fragment f;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wallet);

        ButterKnife.bind(this);

        setupToolbar();

        viewPager = findViewById(R.id.viewpager);
        tabLayout = findViewById(R.id.tabs);
        tvBalance = findViewById(R.id.tvBalance);

        setupViewPager(viewPager);
        tabLayout.setupWithViewPager(viewPager);

        getWalletHistory();
    }

    private void setupToolbar() {
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle("Credits");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void getWalletHistory() {
        if (!Internet.isAvailable(WalletActivity.this)) {
            Internet.showAlertDialog(WalletActivity.this, "Error!", "No Internet Connection", false);
            return;
        }

        Map<String, String> map = new HashMap<String, String>();
        if (App.user.getUser_Type().equalsIgnoreCase(MyConstants.USER_PT) && MyConstants.isGuest.equalsIgnoreCase("0")) {
            map.put("url", MyConstants.BASE_URL + "getWallet");
            map.put("UserId", App.user.getUserID());
            map.put("IsGuest", "0");
        } else {
            map.put("url", MyConstants.GUEST_BASE_URL + "getWallet");
            map.put("GuestId", App.user.getUserID());
            map.put("IsGuest", "1");
        }
        map.put("ApiToken", MyConstants.API_TOKEN);
        map.put("Version", MyConstants.WS_VERSION);


        new CallRequest(WalletActivity.this).getWalletHistory(map);
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());

        adapter.addFragment(new WalletPaidFragment(), "Paid");
        adapter.addFragment(new WalletReceivedFragment(), "Received");

        viewPager.setAdapter(adapter);
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        Utils.removeSimpleSpinProgressDialog();
        try {
            if (result != null && !result.isEmpty()) {
                Log.i("TAG", "TAG Result : " + result);

                switch (request) {
                    case walletHistory:
                        try {
                            JSONObject mainObj = new JSONObject(result);
                            if (mainObj.getString("error_code").equalsIgnoreCase("0")) {
                                MyConstants.walletReceivedList.clear();
                                MyConstants.walletPaidList.clear();
                                if (mainObj.getJSONArray("result") != null && mainObj.getJSONArray("result").length() > 0){

                                    JSONArray array = mainObj.getJSONArray("result");
                                    JSONObject object = array.getJSONObject(0);

                                    String WalletBalance = object.getString("WalletBalance");
                                    MyConstants.walletBalance = WalletBalance;

                                    JSONArray paidArray = object.getJSONArray("Paid");
                                    JSONArray receivedArray = object.getJSONArray("Received");

                                    for (int i = 0; i < paidArray.length(); i++) {
                                        JSONObject obj = paidArray.getJSONObject(i);

                                        WalletHistory walletHistory = new WalletHistory();
                                        walletHistory.setTitle(obj.getString("Title"));
                                        walletHistory.setDescription(obj.getString("Description"));
                                        walletHistory.setAmount(obj.getString("Amount"));
                                        walletHistory.setDate(obj.getString("Date"));
                                        walletHistory.setTxnId(obj.getString("TxnId"));
                                        walletHistory.setDpReferenceId(obj.getString("DpReferenceId"));

                                        MyConstants.walletPaidList.add(walletHistory);
                                    }

                                    for (int i = 0; i < receivedArray.length(); i++) {
                                        JSONObject obj = receivedArray.getJSONObject(i);

                                        WalletHistory walletHistory = new WalletHistory();
                                        walletHistory.setTitle(obj.getString("Title"));
                                        walletHistory.setDescription(obj.getString("Description"));
                                        walletHistory.setAmount(obj.getString("Amount"));
                                        walletHistory.setDate(obj.getString("Date"));
                                        walletHistory.setTxnId(obj.getString("TxnId"));
                                        walletHistory.setDpReferenceId(obj.getString("DpReferenceId"));

                                        MyConstants.walletReceivedList.add(walletHistory);
                                    }

                                    System.out.println("list size:::***"+MyConstants.walletReceivedList.size());

                                    tvBalance.setText("$ "+MyConstants.walletBalance);

                                    ViewPagerAdapter adapter = (ViewPagerAdapter) viewPager.getAdapter();
                                    if (adapter != null) {
                                        f = adapter.getItem(0);
                                    }

                                    if (f instanceof WalletPaidFragment){
                                        if (((WalletPaidFragment) f).walletAdapter != null) {
                                            ((WalletPaidFragment) f).refreshList();
                                        }
                                    }

                                    tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
                                        @Override
                                        public void onTabSelected(TabLayout.Tab tab) {

                                            ViewPagerAdapter adapter = (ViewPagerAdapter) viewPager.getAdapter();
                                            if (adapter != null) {
                                                f = adapter.getItem(tab.getPosition());
                                            }

                                            if (f instanceof WalletPaidFragment){
                                                if (((WalletPaidFragment) f).walletAdapter != null) {
                                                    ((WalletPaidFragment) f).refreshList();
                                                }
                                            } else if (f instanceof WalletReceivedFragment){
                                                if (((WalletReceivedFragment) f).walletAdapter != null) {
                                                    ((WalletReceivedFragment) f).refreshList();
                                                }
                                            }
                                        }

                                        @Override
                                        public void onTabUnselected(TabLayout.Tab tab) {

                                        }

                                        @Override
                                        public void onTabReselected(TabLayout.Tab tab) {

                                        }
                                    });

                                }  else {
                                    Utils.showToast(mainObj.getString("error_string"), WalletActivity.this);
                                }
                            } else {
                                Utils.showToast(mainObj.getString("error_string"), WalletActivity.this);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;

                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onProgressUpdate(String uniqueMessageId, int progres) {

    }

    @Override
    public void onProgressComplete(String uniqueMessageId, String result, Constant.REQUESTS request) {

    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
           /* mFragmentList.add(new UserChatFragment());
            mFragmentList.add(new DoctorChatFragment());

            mFragmentTitleList.add("User Chats");
            mFragmentTitleList.add("Doctor Chats");*/
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public int getItemPosition(Object object) {
            // POSITION_NONE makes it possible to reload the PagerAdapter
            return POSITION_NONE;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
