package com.connex.md.patient.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.connex.md.R;
import com.connex.md.model.TimeSlotsData;
import com.connex.md.patient.activity.BookInPersonActivity;
import com.firebase.client.core.operation.AckUserWrite;

import java.text.SimpleDateFormat;
import java.util.List;

/**
 * Created by abc on 11/21/2017.
 */

public class AvailableSlotsAdapter extends RecyclerView.Adapter<AvailableSlotsAdapter.MyViewHolder> {

    private Context mContext;
    private LayoutInflater inflater;

    private List<TimeSlotsData> mDatas;
    private static int lastCheckedPos = -1;


    public AvailableSlotsAdapter(Context mContext, List<TimeSlotsData> data) {
        this.mContext = mContext;
        this.mDatas = data;
        setHasStableIds(true);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public AvailableSlotsAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_available_time_slots, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final AvailableSlotsAdapter.MyViewHolder holder, int position) {
        TimeSlotsData slotsData = mDatas.get(position);

        try {
            holder.timeSloatTxt.setText(new SimpleDateFormat("HH:mm").format(new SimpleDateFormat("HH:mm:ss").parse(slotsData.getStartTime())));

            if (lastCheckedPos != -1) {
                if (position == lastCheckedPos) {
                    holder.timeSloatTxt.setBackgroundResource(R.drawable.bg_time_slot_selected);
                    holder.timeSloatTxt.setTextColor(Color.WHITE);

                }else {
                    holder.timeSloatTxt.setBackgroundResource(R.drawable.bg_time_slot_normal);
                    holder.timeSloatTxt.setTextColor(Color.parseColor("#06031D"));
                }
            }

        }catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return mDatas.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView timeSloatTxt;

        public MyViewHolder(View itemView) {
            super(itemView);
            timeSloatTxt = itemView.findViewById(R.id.txt_slot_timing);
            timeSloatTxt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    timeSloatTxt.setSelected(true);

                    lastCheckedPos = getAdapterPosition();
                    ((BookInPersonActivity)mContext).mSelectedTimeStr = mDatas.get(getAdapterPosition()).getStartTime();
                    ((BookInPersonActivity)mContext).mSelectedEndTime = mDatas.get(getAdapterPosition()).getEndTime();
                    ((BookInPersonActivity)mContext).mTimeSlotId = mDatas.get(getAdapterPosition()).getId();

                    notifyDataSetChanged();
                }
            });
        }
    }
}
