package com.connex.md.patient.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.connex.md.R;
import com.connex.md.model.BookingHistory;
import com.connex.md.others.App;
import com.connex.md.patient.activity.ConfirmPaymentActivity;
import com.connex.md.ws.MyConstants;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by abc on 11/21/2017.
 */

public class BookingHistoryAdapter extends RecyclerView.Adapter<BookingHistoryAdapter.MyViewHolder> {

    Context mContext;
    LayoutInflater inflater;
    List<BookingHistory> bookingHistoryList;
    private List<BookingHistory> searchDoctorFilterList = new ArrayList<BookingHistory>();

    public BookingHistoryAdapter(Context mContext, List<BookingHistory> bookingHistoryList) {
        this.mContext = mContext;
        this.bookingHistoryList = bookingHistoryList;
        this.searchDoctorFilterList.addAll(bookingHistoryList);
        setHasStableIds(true);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public BookingHistoryAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_booking_history, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final BookingHistoryAdapter.MyViewHolder holder, int position) {
        final BookingHistory bookingHistory = bookingHistoryList.get(position);
        holder.tvDoctorName.setText(bookingHistory.getFirstName() + " " + bookingHistory.getLastName());

        if (bookingHistory.getIsFree().equalsIgnoreCase("1")){
            holder.tvFees.setText("Free");
            holder.btnPayNow.setText("Book Now");

        } else {
            holder.tvFees.setText("$"+bookingHistory.getFinalAmount());
        }

        if (bookingHistory.getStatus().equals("1")) {
            if (bookingHistory.getType().equals("1")) {
                holder.typeTxt.setText(Html.fromHtml("<b>Type: </b>eConsult"));

            }else {
                holder.typeTxt.setText(Html.fromHtml("<b>Type: </b>InPerson"));
            }
        }

        try {
            String time = new SimpleDateFormat("hh:mm a").format(new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").parse(bookingHistory.getConsultTimeOrignal()));
            holder.timeTxt.setText(time);

        } catch (ParseException e) {
            e.printStackTrace();
        }

        holder.tvDate.setText(bookingHistory.getDate());
        holder.tvMonth.setText(bookingHistory.getMonth());

        if (App.user.getUser_Type().equalsIgnoreCase(MyConstants.USER_PT)) {
            holder.tvCategory.setVisibility(View.VISIBLE);
            holder.tvCategory.setText(bookingHistory.getSpeciality());

            if (!bookingHistory.getType().equals("1")) {
                holder.locationTxt.setVisibility(View.VISIBLE);
                holder.locationTxt.setText(Html.fromHtml("<b>Location: </b>" + bookingHistory.getAddress()));
            }

            if (bookingHistory.getStatus().equalsIgnoreCase("1")){
                holder.tvPaid.setText("Paid");
                holder.btnPayNow.setVisibility(View.GONE);
                holder.tvPaid.setVisibility(View.VISIBLE);

            } else {
                holder.tvPaid.setText("Unpaid");
                holder.btnPayNow.setVisibility(View.VISIBLE);
                holder.tvPaid.setVisibility(View.GONE);
            }
        } else {
            holder.tvCategory.setVisibility(View.GONE);
            holder.tvPaid.setVisibility(View.VISIBLE);
            holder.btnPayNow.setVisibility(View.GONE);
            if (bookingHistory.getStatus().equalsIgnoreCase("1")) {
                holder.tvPaid.setText("Paid");
            } else {
                holder.tvPaid.setText("Unpaid");
            }
        }

        holder.btnPayNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MyConstants.consultId = bookingHistory.getConsultId();
                MyConstants.isBooked = true;
                MyConstants.isFree = bookingHistory.getIsFree();
                MyConstants.DOCTOR_NAME = bookingHistory.getFirstName()+ " "+ bookingHistory.getLastName();
                MyConstants.DOCTOR_ID = bookingHistory.getDoctorId();
                MyConstants.DOCTOR_PROFILEPIC = bookingHistory.getProfilePic();
                MyConstants.doctorLastName = bookingHistory.getLastName();

                Intent intent = new Intent(mContext, ConfirmPaymentActivity.class);
                intent.putExtra("isBooked", true);
                intent.putExtra("DoctorName", bookingHistory.getFirstName()+ " "+ bookingHistory.getLastName());
                intent.putExtra("Speciality", bookingHistory.getSpeciality());
                intent.putExtra("ResponseTime", bookingHistory.getResponseTime());
                intent.putExtra("ProfilePic", bookingHistory.getProfilePic());
                intent.putExtra("ConsultCharge", bookingHistory.getFinalAmount());
                mContext.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return bookingHistoryList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView tvDoctorName, tvFees, tvDate, tvMonth, tvCategory, tvPaid, locationTxt, timeTxt, typeTxt;
        Button btnPayNow;

        public MyViewHolder(View itemView) {
            super(itemView);
            tvDoctorName = itemView.findViewById(R.id.tvDoctorName);
            tvCategory = itemView.findViewById(R.id.tvCategory);
            tvFees = itemView.findViewById(R.id.tvFees);
            tvDate = itemView.findViewById(R.id.tvDate);
            tvMonth = itemView.findViewById(R.id.tvMonth);
            tvPaid = itemView.findViewById(R.id.tvPaid);
            btnPayNow = itemView.findViewById(R.id.btnPayNow);
            locationTxt = itemView.findViewById(R.id.txt_location);
            timeTxt = itemView.findViewById(R.id.txt_time);
            typeTxt = itemView.findViewById(R.id.txt_type);
        }
    }
}
