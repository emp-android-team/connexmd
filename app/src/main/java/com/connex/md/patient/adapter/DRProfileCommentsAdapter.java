package com.connex.md.patient.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.connex.md.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by abc on 11/14/2017.
 */

public class DRProfileCommentsAdapter extends BaseAdapter {

    private Activity context;
    List<HashMap<String,String>> detailList = new ArrayList<>();

    public DRProfileCommentsAdapter(Activity context, List<HashMap<String,String>> detailList){
        this.context = context;
        this.detailList = detailList;
    }

    public void addItem(HashMap<String,String> Item) {
        detailList.add(Item);
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return detailList.size();
    }

    @Override
    public Object getItem(int i) {
        return detailList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {
        final ViewHolder holder;
        LayoutInflater vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null) {
            convertView = vi.inflate(R.layout.list_item_dr_profile_comments, null);
            holder = createViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.tvName.setText(detailList.get(position).get("name"));
        holder.tvDate.setText(detailList.get(position).get("date"));
        holder.tvComment.setText(detailList.get(position).get("comment"));

        return convertView;
    }

    private ViewHolder createViewHolder(View convertView) {

        ViewHolder holder = new ViewHolder();
        holder.tvName = convertView.findViewById(R.id.tvName);
        holder.tvDate = convertView.findViewById(R.id.tvDate);
        holder.tvComment = convertView.findViewById(R.id.tvComment);

        return holder;
    }

    private static class ViewHolder {
        public TextView tvName, tvDate, tvComment;
    }
}
