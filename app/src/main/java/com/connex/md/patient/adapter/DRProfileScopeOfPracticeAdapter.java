package com.connex.md.patient.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.connex.md.R;

import java.util.HashMap;
import java.util.List;

/**
 * Created by abc on 11/14/2017.
 */

public class DRProfileScopeOfPracticeAdapter extends BaseAdapter {

    private Context context;
    List<HashMap<String,String>> scope_of_practice;

    public DRProfileScopeOfPracticeAdapter(Context context, List<HashMap<String,String>> scope_of_practice){
        this.context = context;
        this.scope_of_practice = scope_of_practice;
    }

    @Override
    public int getCount() {
        return scope_of_practice.size();
    }

    @Override
    public Object getItem(int i) {
        return scope_of_practice.get(i).get("Speciality");
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {
        ViewHolder holder;
        LayoutInflater vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null) {
            convertView = vi.inflate(R.layout.layout_dr_profile_scope_of_practice, null);
            holder = createViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        System.out.println("speciality in adapter::"+ scope_of_practice.get(i).get("Speciality"));
        holder.tvSpeciality.setText(scope_of_practice.get(i).get("Speciality"));
        System.out.println("textview ::"+holder.tvSpeciality.getText().toString());

        return convertView;
    }

    private ViewHolder createViewHolder(View convertView) {

        ViewHolder holder = new ViewHolder();
        holder.tvSpeciality = convertView.findViewById(R.id.tvSpeciality);

        return holder;
    }

    private static class ViewHolder {
        public TextView tvSpeciality;
    }
}
