package com.connex.md.patient.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.connex.md.R;
import com.connex.md.model.BookingHistory;
import com.connex.md.patient.activity.PaymentActivity;
import com.connex.md.ws.MyConstants;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by abc on 11/21/2017.
 */

public class DoctorNotesHistoryAdapter extends RecyclerView.Adapter<DoctorNotesHistoryAdapter.MyViewHolder> {

    Context mContext;
    LayoutInflater inflater;
    List<BookingHistory> bookingHistoryList;
    private List<BookingHistory> searchDoctorFilterList = new ArrayList<BookingHistory>();

    public DoctorNotesHistoryAdapter(Context mContext, List<BookingHistory> bookingHistoryList) {
        this.mContext = mContext;
        this.bookingHistoryList = bookingHistoryList;
        this.searchDoctorFilterList.addAll(bookingHistoryList);
        setHasStableIds(true);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public DoctorNotesHistoryAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_booking_history, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final DoctorNotesHistoryAdapter.MyViewHolder holder, int position) {
        final BookingHistory bookingHistory = bookingHistoryList.get(position);
        holder.tvDoctorName.setText(bookingHistory.getFirstName() + " " + bookingHistory.getLastName());
        holder.tvFees.setText("$15");
        holder.tvDate.setText(bookingHistory.getDate());
        holder.tvMonth.setText(bookingHistory.getMonth());
        holder.tvCategory.setVisibility(View.GONE);
        //holder.tvCategory.setText(bookingHistory.getSpeciality());
        if (!bookingHistory.getStatus().equalsIgnoreCase("0")) {
            holder.tvPaid.setText("Paid");
            holder.btnPayNow.setVisibility(View.GONE);
            holder.tvPaid.setVisibility(View.VISIBLE);
        } else {
            holder.tvPaid.setText("Unpaid");
            holder.btnPayNow.setVisibility(View.VISIBLE);
            holder.tvPaid.setVisibility(View.GONE);
        }

        holder.btnPayNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                MyConstants.consultId = bookingHistory.getConsultId();

                Intent intent = new Intent(mContext, PaymentActivity.class);
                intent.putExtra("isOfferUsed", false);
                intent.putExtra("finalAmount", Integer.parseInt(MyConstants.headerPrice));
                intent.putExtra("walletCharge", "0");
                intent.putExtra("isHeader", true);
                mContext.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return bookingHistoryList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView tvDoctorName, tvFees, tvDate, tvMonth, tvCategory, tvPaid;
        Button btnPayNow;

        public MyViewHolder(View itemView) {
            super(itemView);
            tvDoctorName = itemView.findViewById(R.id.tvDoctorName);
            tvCategory = itemView.findViewById(R.id.tvCategory);
            tvFees = itemView.findViewById(R.id.tvFees);
            tvDate = itemView.findViewById(R.id.tvDate);
            tvMonth = itemView.findViewById(R.id.tvMonth);
            tvPaid = itemView.findViewById(R.id.tvPaid);
            btnPayNow = itemView.findViewById(R.id.btnPayNow);
        }
    }
}
