package com.connex.md.patient.adapter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.connex.md.R;
import com.connex.md.custom_views.RobottoTextView;
import com.connex.md.interfaces.AsyncTaskListner;
import com.connex.md.model.DoctorProfile;
import com.connex.md.model.SearchDoctor;
import com.connex.md.model.TimeSlotsData;
import com.connex.md.others.App;
import com.connex.md.others.Internet;
import com.connex.md.others.PicassoTrustAll;
import com.connex.md.patient.activity.BookInPersonActivity;
import com.connex.md.patient.activity.DRProfileActivity;
import com.connex.md.patient.activity.DRQuestionnaire1Activity;
import com.connex.md.patient.activity.SearchDoctorActivity;
import com.connex.md.ws.CallRequest;
import com.connex.md.ws.Constant;
import com.connex.md.ws.MyConstants;
import com.connex.md.ws.Utils;
import com.github.siyamed.shapeimageview.CircularImageView;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.squareup.picasso.Callback;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by abc on 11/21/2017.
 */

public class EConsultationAdapter extends RecyclerView.Adapter<EConsultationAdapter.MyViewHolder> {

    Fragment mContext;
    LayoutInflater inflater;
    ArrayList<SearchDoctor> searchDoctorList;
    private List<SearchDoctor> searchDoctorFilterList = new ArrayList<SearchDoctor>();

    private SharedPreferences mSharedPreferences;

    public EConsultationAdapter(Fragment mContext, ArrayList<SearchDoctor> searchDoctorList){
        this.mContext = mContext;
        mSharedPreferences = mContext.getActivity().getSharedPreferences(MyConstants.PREF, MODE_PRIVATE);
        this.searchDoctorList = searchDoctorList;
        this.searchDoctorFilterList.addAll(searchDoctorList);
        setHasStableIds(true);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public EConsultationAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_econsultation, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final EConsultationAdapter.MyViewHolder holder, final int position) {
        SearchDoctor searchDoctor = searchDoctorList.get(position);

        if (position%2 != 0)
            holder.mRootLayout.setBackgroundColor(Color.parseColor("#F2FCFE"));
        else
            holder.mRootLayout.setBackgroundColor(Color.WHITE);

        if (mSharedPreferences.getBoolean(MyConstants.IS_LOGGED_IN, false)) {
            if (App.user.getUser_Type().equalsIgnoreCase(MyConstants.USER_DR) && MyConstants.isGuest.equalsIgnoreCase("0"))
                holder.bookVisitBtn.setVisibility(View.GONE);
        }

        holder.tvDoctorName.setText(searchDoctor.getFirstName() + " " + searchDoctor.getLastName());
        holder.tvLocation.setText(searchDoctor.getCountry());
        holder.mPriceTxt.setText("Fee: $" + searchDoctor.getCosultCharge() + " (" + Html.fromHtml(searchDoctor.getLocalCharge()) + ")");

        try {
            PicassoTrustAll.getInstance(mContext.getActivity())
                    .load(searchDoctor.getProfilePic())
                    .placeholder(R.drawable.avatar)
                    .error(R.drawable.avatar)
                    .into(holder.ivDoctorDp, new Callback() {
                        @Override
                        public void onSuccess() {
                        }

                        @Override
                        public void onError() {
                        }
                    });
        } catch (Exception e){
            e.printStackTrace();
        }

        holder.itemView.setClickable(true);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //holder.itemView.setClickable(false);
                //enableClick(holder.itemView);

                Bundle b = new Bundle();
                b.putSerializable("search_doctor", getItem(position));

                MyConstants.isSuggestDoctor = false;

                Intent intent = new Intent(mContext.getActivity(), DRProfileActivity.class);
                intent.putExtras(b);
                mContext.startActivity(intent);
            }
        });

        holder.bookVisitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mSharedPreferences.getBoolean(MyConstants.IS_LOGGED_IN, false)) {
                    if (App.user.getUser_Type().equalsIgnoreCase(MyConstants.USER_PT) && MyConstants.isGuest.equalsIgnoreCase("0"))
                        getDRProfileData(searchDoctor.getDoctorId());
                }
            }
        });
    }

    private void enableClick(final View view) {
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                view.setClickable(true);
                handler.removeCallbacks(this);
            }
        }, 4000);
    }

    @Override
    public int getItemCount() {
        return searchDoctorList.size();
    }

    public SearchDoctor getItem(int position) {
        return searchDoctorList.get(position);
    }

    public void filters(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        searchDoctorList.clear();
        if (charText.length() == 0) {
            searchDoctorList.addAll(searchDoctorFilterList);
        } else {
            for (SearchDoctor bean : searchDoctorFilterList) {
                String Contanint = bean.getFirstName();
                if (bean.getFirstName().toLowerCase(Locale.getDefault())
                        .contains(charText)) {
                    searchDoctorList.add(bean);
                }
            }
        }
        notifyDataSetChanged();

    }

    public class MyViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.ll_root)
        LinearLayout mRootLayout;

        @BindView(R.id.btn_book_visit)
        Button bookVisitBtn;

        @BindView(R.id.txt_price)
        RobottoTextView mPriceTxt;

        TextView tvDoctorName, tvFee, tvLocation;
        CircularImageView ivDoctorDp;

        public MyViewHolder(View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);

            tvDoctorName = itemView.findViewById(R.id.tvDoctorName);
            tvFee = itemView.findViewById(R.id.tvFee);
            tvLocation = itemView.findViewById(R.id.tvLocation);
            ivDoctorDp = itemView.findViewById(R.id.ivDoctorDp);
        }
    }

    private void getDRProfileData(String mDoctorId) {
        if (!Internet.isAvailable(mContext.getActivity())) {
            Internet.showAlertDialog(mContext.getActivity(), "Error!", "No Internet Connection", false);
            return;
        }

        Map<String, String> map = new HashMap<String, String>();
        map.put("url", MyConstants.BASE_URL + "doctorProfile");
        map.put("ApiToken", MyConstants.API_TOKEN);
        map.put("Version", MyConstants.WS_VERSION);
        map.put("DoctorId", mDoctorId);
        map.put("UserId", App.user.getUserID());
        map.put("IsGuest", "0");

        new CallRequest(mContext).getDoctorProfile(map);
    }
}
