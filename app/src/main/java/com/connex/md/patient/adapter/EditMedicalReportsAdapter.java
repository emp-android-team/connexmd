package com.connex.md.patient.adapter;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.connex.md.R;
import com.connex.md.interfaces.DoctorListener;
import com.connex.md.others.PicassoTrustAll;
import com.squareup.picasso.Callback;

import java.util.HashMap;
import java.util.List;

/**
 * Created by abc on 11/28/2017.
 */

public class EditMedicalReportsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context mContext;
    private List<HashMap<String, String>> imageList;
    private DoctorListener listener;

    public EditMedicalReportsAdapter(Fragment mContext, List<HashMap<String, String>> imageList) {
        this.mContext = mContext.getActivity();
        this.imageList = imageList;
        this.listener = (DoctorListener) mContext;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //Inflate the layout, initialize the View Holder
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_edit_medical_report, parent, false);

        return new ViewHolderImages(itemView);
    }

    @Override
    public int getItemCount() {
        //returns the number of elements the RecyclerView will display
        return imageList.size();
    }


    public class ViewHolderImages extends RecyclerView.ViewHolder {

        ImageView ivImage, ivCancel;

        ViewHolderImages(View v) {
            super(v);
            ivImage = v.findViewById(R.id.ivImage);
            ivCancel = v.findViewById(R.id.ivCancel);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        ViewHolderImages holderImages = (ViewHolderImages) holder;
        bindImagesHolder(holderImages, position);


    }

    private void bindImagesHolder(final ViewHolderImages holder, final int position) {

        try {
            PicassoTrustAll.getInstance(mContext)
                    .load(imageList.get(position).get("Description"))
                    .placeholder(R.drawable.no_img)
                    .error(R.drawable.no_img)
                    .into(holder.ivImage, new Callback() {
                        @Override
                        public void onSuccess() {
                        }

                        @Override
                        public void onError() {
                            holder.ivImage.setImageResource(R.drawable.no_img);
                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }

        holder.ivCancel.setTag(position);
        holder.ivCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final int position = (int) view.getTag();
                new AlertDialog.Builder(mContext)
                        .setTitle("Alert!")
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setMessage("Would you like to delete Medical Report?")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                listener.editDetails(position, "", "", "");
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })
                        .show();

            }
        });

        holder.ivImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showBiggerImage(imageList.get(position).get("Description"));
            }
        });
    }

    public void showBiggerImage(String url) {

        final Dialog nagDialog = new Dialog(mContext, android.R.style.Theme_Translucent_NoTitleBar_Fullscreen);
        nagDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        nagDialog.setCancelable(false);

        LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
        View view = inflater.inflate(R.layout.layout_bigger_image, null);
        nagDialog.setContentView(view);

        final ProgressBar pBar = view.findViewById(R.id.pBar);

        Button btnClose = view.findViewById(R.id.btnIvClose);
        ImageView ivPreview = view.findViewById(R.id.iv_preview_image);

        try {
            PicassoTrustAll.getInstance(mContext)
                    .load(url)
                    .into(ivPreview, new Callback() {
                        @Override
                        public void onSuccess() {
                            pBar.setVisibility(View.GONE);
                        }

                        @Override
                        public void onError() {
                            pBar.setVisibility(View.GONE);
                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }
        /*aq.id(ivPreview).progress(pBar)
                .image(url, true, true, 0, 0, null, 0, 1.0f);*/

        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {

                nagDialog.dismiss();
            }
        });
        nagDialog.show();
    }

}
