package com.connex.md.patient.adapter;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.connex.md.R;
import com.connex.md.interfaces.DoctorListener;

import java.util.HashMap;
import java.util.List;

/**
 * Created by abc on 11/21/2017.
 */

public class EditMedicinesAdapter extends RecyclerView.Adapter<EditMedicinesAdapter.MyViewHolder> {

    private Context mContext;
    private List<HashMap<String, String>> educationList;
    private Dialog dialog;
    private ImageView ivClose;
    private Button btnSave;
    private EditText etUniversityName, etFrom, etTo;
    private String universityName, startYear, endYear, type;
    private TextView tvUniversityName;
    private DoctorListener doctorListener;
    private LinearLayout llFromTo;

    public EditMedicinesAdapter(Fragment mContext, List<HashMap<String, String>> educationList, String type) {
        this.mContext = mContext.getActivity();
        this.educationList = educationList;
        this.type = type;
        doctorListener = (DoctorListener) mContext;
    }

    @Override
    public EditMedicinesAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_edit_medicines, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final EditMedicinesAdapter.MyViewHolder holder, int position) {

        holder.tvEducation.setText(educationList.get(position).get("Description"));

        holder.ivEdit.setTag(position);
        holder.ivEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int position = (int) view.getTag();

                openEditDialog(position);
            }
        });

    }

    private void openEditDialog(final int position) {
        createDialog();
        initDialogComponents();

        etUniversityName.setText(educationList.get(position).get("Description"));

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                universityName = etUniversityName.getText().toString().trim();

                if (TextUtils.isEmpty(universityName)) {
                    etUniversityName.requestFocus();
                    etUniversityName.setError("Medicine name is empty");
                    return;
                }

                dialog.dismiss();

                doctorListener.editDetails(position, universityName, "", "");
            }
        });

        ivClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
    }

    private void createDialog() {
        dialog = new Dialog(mContext, R.style.CustomDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        // Include dialog.xml file
        dialog.setContentView(R.layout.dialog_edit_doctor);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
        dialog.show();
    }

    private void initDialogComponents() {
        btnSave = dialog.findViewById(R.id.btnSave);
        ivClose = dialog.findViewById(R.id.ivClose);
        etUniversityName = dialog.findViewById(R.id.etUniversityName);
        etFrom = dialog.findViewById(R.id.etFrom);
        etTo = dialog.findViewById(R.id.etTo);
        llFromTo = dialog.findViewById(R.id.ll_from_to);
        tvUniversityName = dialog.findViewById(R.id.tvUniversityName);

        llFromTo.setVisibility(View.GONE);
        tvUniversityName.setText("Add regularly taken medication here");
        etUniversityName.setHint("Medicine name");
        btnSave.setText("Add");
    }

    @Override
    public int getItemCount() {
        return educationList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView tvEducation;
        ImageView ivEdit;

        public MyViewHolder(View itemView) {
            super(itemView);

            tvEducation = itemView.findViewById(R.id.tvEducation);
            ivEdit = itemView.findViewById(R.id.ivEdit);
        }
    }
}
