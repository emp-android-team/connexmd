package com.connex.md.patient.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.ToggleButton;
import android.widget.ViewFlipper;

import com.connex.md.R;
import com.connex.md.animation.AnimationFactory;
import com.connex.md.custom_views.RoundRectCornerImageView;
import com.connex.md.interfaces.HomeListener;
import com.connex.md.interfaces.SpecialitiesListener;
import com.connex.md.model.Specialities;
import com.connex.md.others.App;
import com.connex.md.others.PicassoTrustAll;
import com.connex.md.patient.activity.DoctorNotesActivity;
import com.connex.md.patient.activity.SearchDoctorActivity;
import com.connex.md.patient.fragment.HomeFragment;
import com.connex.md.ws.MyConstants;
import com.squareup.picasso.Callback;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;


public class HomeAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public Context mContext;
    public LayoutInflater inflater;
    public int type = 0;
    private SpecialitiesListener spelistner;
    public ViewHolderSpecialities HOLDER;
    private HomeListener homeListener;
    public HomeFragment fragment;
    private RecyclerView recylerViewThis;
    private Specialities specialities;
    public ArrayList<Specialities> spaArray;
    public ArrayList<Specialities> searchArray;
    private ItemFilter mFilter = new ItemFilter();
    private boolean isFlipped = false, isFilter = false;

    private int parentHeight = 0, viewHeight = 0;


    public HomeAdapter(Fragment context, ArrayList<Specialities> spaArray, HomeFragment fragment) {
        mContext = context.getActivity();
        inflater = LayoutInflater.from(mContext);
        this.spaArray = spaArray;
        this.searchArray = new ArrayList<>();
        this.searchArray.addAll(this.spaArray);
        spelistner = (SpecialitiesListener) context;
        homeListener = (HomeListener) context;
        this.fragment = fragment;

        setHasStableIds(true);
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder = null;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        parentHeight = parent.getHeight();

        switch (viewType) {
            case 0:
                View v1 = inflater.inflate(R.layout.list_item_home_chat, parent, false);
                viewHolder = new ViewHolderChat(v1);
                break;

            case 1:
                View v2 = inflater.inflate(R.layout.list_item_home, parent, false);
                viewHolder = new ViewHolderSpecialities(v2);
                break;

            case 2:
                View v3 = inflater.inflate(R.layout.list_item_home_clinic, parent, false);
                viewHolder = new ViewHolderClinic(v3);
                break;

            case 3:
                View v5 = inflater.inflate(R.layout.list_item_home_see_more, parent, false);
                viewHolder = new ViewHolderSeeMore(v5);
                break;

            case 4:
                View v4 = inflater.inflate(R.layout.layout_home_header, parent, false);
                viewHolder = new ViewHolderHeader(v4);
                break;
        }
        return viewHolder;
    }

    @Override
    public int getItemCount() {
        if (!isFilter && spaArray != null && spaArray.get(HomeFragment.showSeeMorePosition * 2).isShowSeeMore())
            return ((HomeFragment.showSeeMorePosition * 2) + 1);
        else
            return (null != spaArray ? spaArray.size() : 0);
    }

    @Override
    public int getItemViewType(int position) {
        if (getItem(position).isRandom) {
            return 0;

        } else if (getItem(position).isHeader) {
            return 4;

        } else if (getItem(position).getIs_partner().equals("Y")) {
            return 2;

        } else if (getItem(position).isShowSeeMore()){
            return 3;

        }else
            return 1;
    }


    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public Specialities getItem(int position) {
        return spaArray.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void filter(final String text) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                spaArray.clear();

                if (TextUtils.isEmpty(text)) {
                    spaArray.addAll(searchArray);
                    isFilter = false;

                } else {
                    for (Specialities item : searchArray) {
                        if (item.getSpeciality().toLowerCase(Locale.getDefault()).contains(text) || item.getSymptoms().toLowerCase(Locale.getDefault()).contains(text)) {
                            if (!item.isRandom) {
                                spaArray.add(item);
                            }
                        }
                    }
                    isFilter = true;
                }

                ((Activity) mContext).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        notifyDataSetChanged();
                    }
                });

            }
        }).start();

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        switch (holder.getItemViewType()) {
            case 0:
                ViewHolderChat chatHolder = (ViewHolderChat) holder;
                bindChatHolder(chatHolder, position);
                break;

            case 1:
                ViewHolderSpecialities spaHolder = (ViewHolderSpecialities) holder;
                bindSpecialityHolder(spaHolder, position);
                break;

            case 2:
                ViewHolderClinic holderClinic = (ViewHolderClinic) holder;
                bindClinicHolder(holderClinic, position);
                break;

            case 4:
                ViewHolderHeader holderHeader = (ViewHolderHeader) holder;
                StaggeredGridLayoutManager.LayoutParams layoutParams = (StaggeredGridLayoutManager.LayoutParams) holderHeader.itemView.getLayoutParams();
                layoutParams.setFullSpan(true);
                bindHeaderHolder(holderHeader, position);
                break;
        }
    }


    private void bindHeaderHolder(ViewHolderHeader holderHeader, int position) {
        holderHeader.btnDoctorNote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, DoctorNotesActivity.class);
                mContext.startActivity(intent);
            }
        });
    }

    private void bindClinicHolder(final ViewHolderClinic holderClinic, final int position) {
        specialities = getItem(position);

        holderClinic.tvClinicName.setText(specialities.getClinic_name());
        holderClinic.tvDoctorName.setText(specialities.getDoctor_name());
        holderClinic.tvLocation.setText(specialities.getLocation());

        try {
            PicassoTrustAll.getInstance(mContext)
                    .load(specialities.getImage())
                    .error(R.drawable.no_img)
                    .into(holderClinic.ivClinicBG, new Callback() {
                        @Override
                        public void onSuccess() {
                            holderClinic.pBar.setVisibility(View.GONE);
                        }

                        @Override
                        public void onError() {
                            holderClinic.pBar.setVisibility(View.GONE);
                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }

        holderClinic.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Bundle b = new Bundle();
                b.putSerializable("speciality", getItem(position));

                if (!App.isActivityRunning(mContext, SearchDoctorActivity.class)) {
                    Intent intent = new Intent(mContext, SearchDoctorActivity.class);
                    intent.putExtras(b);
                    mContext.startActivity(intent);
                }
            }
        });
    }

    public void setRecyclerView(RecyclerView r) {
        recylerViewThis = r;
    }


    private void bindChatHolder(ViewHolderChat holder, final int pos) {
        specialities = getItem(pos);

        holder.img_chat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                spelistner.onClickChat();
            }
        });
    }


    private void bindSpecialityHolder(final ViewHolderSpecialities holder, final int pos) {
        specialities = getItem(pos);

        holder.tv_specialti.setText(specialities.getSpeciality());
        holder.tv_specialty.setText(specialities.getSpeciality());

        try {
            PicassoTrustAll.getInstance(mContext)
                    .load(specialities.getImage())
                    .error(R.drawable.no_img)
                    .into(holder.img_speciality, new Callback() {
                        @Override
                        public void onSuccess() {
                            holder.pBar.setVisibility(View.GONE);
                        }

                        @Override
                        public void onError() {
                            holder.pBar.setVisibility(View.GONE);
                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (specialities.getIsSaved() == 0) {
            holder.img_favrite.setChecked(false);
        } else {
            holder.img_favrite.setChecked(true);
        }

        if (TextUtils.isEmpty(App.user.getUserID())) {
            holder.img_favrite.setEnabled(false);
            holder.img_favrite.setVisibility(View.GONE);
        } else if (MyConstants.isGuest.equalsIgnoreCase("0") && App.user.getUser_Type().equalsIgnoreCase(MyConstants.USER_PT)) {
            holder.img_favrite.setEnabled(true);
        } else {
            holder.img_favrite.setEnabled(false);
            holder.img_favrite.setVisibility(View.GONE);
        }

        holder.img_favrite.setTag(pos);
        holder.img_favrite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    int position = (int) view.getTag();
                    Specialities specialities = getItem(position);
                    homeListener.saveRemoveCategory(specialities, pos);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });

        if (!TextUtils.isEmpty(specialities.getSymptoms())) {
            List<String> symptomsList = Arrays.asList(specialities.getSymptoms().split("\\s*,\\s*"));


            String symptoms = "";
            if (symptomsList.size() > 0) {
                for (int i = 0; i < symptomsList.size(); i++) {
                    symptoms += "&#62; " + symptomsList.get(i) + "<br/>";
                }
            }

            holder.tvSymptoms.setText(Html.fromHtml(symptoms));
            holder.tvSymptoms.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) { // Disallow the touch request for parent scroll on touch of childview
                    v.getParent().requestDisallowInterceptTouchEvent(true);
                    return false;
                }
            });

        } else {
            holder.tvSymptoms.setText("");
        }


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle b = new Bundle();
                b.putSerializable("speciality", getItem(pos));

                Intent intent = new Intent(mContext, SearchDoctorActivity.class);
                intent.putExtras(b);
                mContext.startActivity(intent);
            }
        });

        holder.btnConfused.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                spelistner.onClickChat();
            }
        });

        holder.rel_info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isFlipped)
                    foldAllFlipper();

                isFlipped = true;
                flipViewFlipper(holder.viewFlipper);
            }
        });

        holder.ivInfoBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isFlipped = false;
                flipViewFlipper(holder.viewFlipper);
            }
        });
    }


    private void foldAllFlipper() {
        for (int i = 0; i < getItemCount(); i++) {
            try {
                ViewHolderSpecialities v = (ViewHolderSpecialities) recylerViewThis.findViewHolderForAdapterPosition(i);

                if (v != null) {
                    if (v.viewFlipper.getDisplayedChild() != 0) {
                        AnimationFactory.flipTransition(v.viewFlipper, AnimationFactory.FlipDirection.RIGHT_LEFT);
                        v.viewFlipper.setDisplayedChild(0);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void flipViewFlipper(ViewFlipper flipper) {

        if (flipper.getDisplayedChild() == 0) {
            AnimationFactory.flipTransition(flipper, AnimationFactory.FlipDirection.LEFT_RIGHT);
            flipper.setDisplayedChild(1);
        } else {
            AnimationFactory.flipTransition(flipper, AnimationFactory.FlipDirection.RIGHT_LEFT);
            flipper.setDisplayedChild(0);
        }

    }

    public class ViewHolderSpecialities extends RecyclerView.ViewHolder {
        public TextView tv_specialti, tv_specialty;
        public RoundRectCornerImageView img_speciality;
        public ProgressBar pBar;
        public ViewFlipper viewFlipper;
        public ImageView img_info;
        public ToggleButton img_favrite;
        public RelativeLayout rel_favrite, rel_info;
        public TextView tvSymptoms;
        public ImageView ivInfoBack;
        public ScrollView childScroll;
        public Button btnConfused;

        public ViewHolderSpecialities(View v) {
            super(v);
            tv_specialti = v.findViewById(R.id.tv_specialti);
            img_speciality = v.findViewById(R.id.img_speciality);
            pBar = v.findViewById(R.id.progressBar);
            img_favrite = v.findViewById(R.id.toggle);
            img_info = v.findViewById(R.id.img_info);
            viewFlipper = v.findViewById(R.id.my_view_flipper);
            rel_favrite = v.findViewById(R.id.rel_favrite);
            rel_info = v.findViewById(R.id.rel_info);
            tvSymptoms = v.findViewById(R.id.tvSymptoms);
            ivInfoBack = v.findViewById(R.id.img_info_back);
            tv_specialty = v.findViewById(R.id.tv_specialty);
            childScroll = v.findViewById(R.id.childScroll);
            btnConfused = v.findViewById(R.id.btnConfused);
        }
    }

    public class ViewHolderChat extends RecyclerView.ViewHolder {
        public ImageView img_chat;

        public ViewHolderChat(View v) {
            super(v);
            img_chat = v.findViewById(R.id.img_chat);

        }
    }

    public class ViewHolderSeeMore extends RecyclerView.ViewHolder {
        Button moreSpecialistBtn;

        public ViewHolderSeeMore(View v) {
            super(v);
            moreSpecialistBtn = v.findViewById(R.id.btn_more_specialist);

            moreSpecialistBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    spaArray.get(getAdapterPosition()).setShowSeeMore(false);
                    spaArray.remove(getAdapterPosition());
                    notifyDataSetChanged();
                }
            });
        }
    }

    public class ViewHolderHeader extends RecyclerView.ViewHolder {
        Button btnDoctorNote;

        ViewHolderHeader(View v) {
            super(v);
            btnDoctorNote = v.findViewById(R.id.btnDoctorNote);
        }
    }

    public class ViewHolderClinic extends RecyclerView.ViewHolder {
        RelativeLayout ll_bg;
        ImageView ivClinicBG;
        public ProgressBar pBar;
        TextView tvClinicName, tvDoctorName, tvLocation;

        ViewHolderClinic(View v) {
            super(v);
            ll_bg = v.findViewById(R.id.ll_bg);
            tvClinicName = v.findViewById(R.id.tvClinicName);
            tvDoctorName = v.findViewById(R.id.tvDoctorName);
            tvLocation = v.findViewById(R.id.tvLocation);
            pBar = v.findViewById(R.id.progressBar);
            ivClinicBG = v.findViewById(R.id.ivClinicBG);
        }
    }

    private class ItemFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            String filterString = constraint.toString().toLowerCase();

            FilterResults results = new FilterResults();

            if (filterString.length() == 0) {
                spaArray.addAll(searchArray);
            } else {
                for (Specialities bean : searchArray) {

                    if (bean.getSpeciality().toLowerCase(Locale.getDefault()).contains(filterString)) {
                        spaArray.add(bean);
                    }
                }
            }

            results.values = spaArray;
            results.count = spaArray.size();

            return results;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            spaArray = (ArrayList<Specialities>) results.values;
            notifyDataSetChanged();
        }

    }
}
