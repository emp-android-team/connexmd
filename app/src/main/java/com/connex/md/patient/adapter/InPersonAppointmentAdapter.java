package com.connex.md.patient.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.connex.md.R;
import com.connex.md.custom_views.RobottoTextView;
import com.connex.md.custom_views.RobottoTextViewBold;
import com.connex.md.model.InpersonAppointmentsData;
import com.connex.md.model.TimeSlotsData;
import com.connex.md.patient.activity.BookInPersonActivity;

import java.text.SimpleDateFormat;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by abc on 11/21/2017.
 */

public class InPersonAppointmentAdapter extends RecyclerView.Adapter<InPersonAppointmentAdapter.MyViewHolder> {

    private Context mContext;
    private LayoutInflater inflater;

    private List<InpersonAppointmentsData> mDatas;


    public InPersonAppointmentAdapter(Context mContext, List<InpersonAppointmentsData> data) {
        this.mContext = mContext;
        this.mDatas = data;
        setHasStableIds(true);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public InPersonAppointmentAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_inperson_appointments, parent, false);

        return new MyViewHolder(itemView);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(final InPersonAppointmentAdapter.MyViewHolder holder, int position) {
        InpersonAppointmentsData appointmentsData = mDatas.get(position);

        try {
            holder.doctorNameTxt.setText(appointmentsData.getDoctorFirstName() + " " + appointmentsData.getDoctorLastName());
            holder.specialityTxt.setText(appointmentsData.getSpeciality());
            holder.priceTxt.setText("$" + appointmentsData.getConsultCharge());
            holder.mLocationTxt.setText(Html.fromHtml("<b>Location: </b>" + appointmentsData.getDoctorAddress()));

            String date = new SimpleDateFormat("dd").format(new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").parse(appointmentsData.getConsultTime()));
            String time = new SimpleDateFormat("HH:mm a").format(new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").parse(appointmentsData.getConsultTime()));
            String monthYear = new SimpleDateFormat("MMM yyyy").format(new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").parse(appointmentsData.getConsultTime()));

            holder.dateTxt.setText(date);
            holder.monthYearTxt.setText(monthYear);
            holder.mTimeTxt.setText(time);

        }catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return mDatas.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_doctor_name)
        RobottoTextViewBold doctorNameTxt;

        @BindView(R.id.txt_speciality)
        RobottoTextView specialityTxt;

        @BindView(R.id.txt_price)
        RobottoTextViewBold priceTxt;

        @BindView(R.id.txt_date)
        RobottoTextView dateTxt;

        @BindView(R.id.txt_month_year)
        RobottoTextView monthYearTxt;

        @BindView(R.id.txt_location)
        RobottoTextView mLocationTxt;

        @BindView(R.id.txt_time)
        RobottoTextView mTimeTxt;

        public MyViewHolder(View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);
        }
    }
}
