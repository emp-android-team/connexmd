package com.connex.md.patient.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.connex.md.R;

import java.util.HashMap;
import java.util.List;

/**
 * Created by abc on 11/28/2017.
 */

public class MedicinesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Context mContext;
    LayoutInflater inflater;
    private List<HashMap<String,String>> medicineList;

    public MedicinesAdapter(Context mContext, List<HashMap<String,String>> medicineList) {
        this.mContext = mContext;
        this.medicineList = medicineList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //Inflate the layout, initialize the View Holder
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_medicines, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public int getItemCount() {
        //returns the number of elements the RecyclerView will display
        return medicineList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView tvMedicine;

        ViewHolder(View v) {
            super(v);
            tvMedicine = v.findViewById(R.id.tvMedicine);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        ViewHolder viewHolder = (ViewHolder) holder;

        viewHolder.tvMedicine.setText(medicineList.get(position).get("Description"));

    }

}
