package com.connex.md.patient.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.connex.md.R;
import com.connex.md.firebase_chat.activity.FireChatActivity;
import com.connex.md.model.MessageHistory;
import com.connex.md.others.App;
import com.connex.md.others.PicassoTrustAll;
import com.connex.md.ws.MyConstants;
import com.github.siyamed.shapeimageview.CircularImageView;
import com.squareup.picasso.Callback;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by abc on 11/21/2017.
 */

public class MessagesAdapter extends RecyclerView.Adapter<MessagesAdapter.MyViewHolder> {

    Context mContext;
    List<MessageHistory> messagesList;
    private List<MessageHistory> searchDoctorFilterList = new ArrayList<MessageHistory>();

    public MessagesAdapter(Context mContext, List<MessageHistory> messagesList) {
        this.mContext = mContext;
        this.messagesList = messagesList;
        this.searchDoctorFilterList.addAll(messagesList);

        setHasStableIds(true);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public MessagesAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_messages, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MessagesAdapter.MyViewHolder holder, int position) {
        final MessageHistory messageHistory = messagesList.get(position);
        holder.tvDoctorName.setText(messageHistory.getFirstName() + " " + messageHistory.getLastName());
        holder.tvLastMessage.setText(messageHistory.getLastMessage());
        if (TextUtils.isEmpty(messageHistory.getCounter()) || messageHistory.getCounter().equalsIgnoreCase("0")) {
            holder.tvCount.setVisibility(View.GONE);
            System.out.println("counter 0");
        } else {
            System.out.println("counter > 0");
            holder.tvCount.setVisibility(View.VISIBLE);
            holder.tvCount.setText(messageHistory.getCounter());
        }
        holder.tvTime.setText(messageHistory.getTime());


        try {
            if (!messageHistory.getProfilePic().isEmpty()) {
                PicassoTrustAll.getInstance(mContext)
                        .load(messageHistory.getProfilePic())
                        .placeholder(R.drawable.avatar)
                        .error(R.drawable.avatar)
                        .into(holder.ivDoctor, new Callback() {
                            @Override
                            public void onSuccess() {
                            }

                            @Override
                            public void onError() {
                                holder.ivDoctor.setImageResource(R.drawable.avatar);
                            }
                        });
            } else {
                holder.ivDoctor.setImageResource(R.drawable.avatar);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, FireChatActivity.class);
                intent.putExtra(MyConstants.IS_PAYMENT_DONE, false);
                intent.putExtra(MyConstants.UNIQUE_CHAT_ID, messageHistory.getBookingId());
                if (App.user.getUser_Type().equalsIgnoreCase(MyConstants.USER_PT)) {
                    intent.putExtra(MyConstants.Pt_NAME, App.user.getName());
                    intent.putExtra(MyConstants.DR_NAME, messageHistory.getFirstName() + " " + messageHistory.getLastName());
                } else {
                    intent.putExtra(MyConstants.Pt_NAME, messageHistory.getFirstName() + " " + messageHistory.getLastName());
                    intent.putExtra(MyConstants.DR_NAME, App.user.getName());
                    intent.putExtra("IS_GUEST", messageHistory.getIsGuest());
                }
                intent.putExtra(MyConstants.PT_ID, messageHistory.getUserId());
                intent.putExtra(MyConstants.DR_ID, messageHistory.getDoctorId());
                intent.putExtra(MyConstants.RECEIVER_IMAGE_URL, messageHistory.getProfilePic());
                mContext.startActivity(intent);
            }
        });
    }

    public void filters(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        messagesList.clear();
        if (charText.length() == 0) {
            messagesList.addAll(searchDoctorFilterList);
        } else {
            for (MessageHistory bean : searchDoctorFilterList) {
                String Contanint = bean.getFirstName();
                if (bean.getFirstName().toLowerCase(Locale.getDefault())
                        .contains(charText) || bean.getLastName().toLowerCase(Locale.getDefault()).contains(charText)) {
                    messagesList.add(bean);
                }
            }
        }
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return messagesList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        CircularImageView ivDoctor;
        TextView tvDoctorName, tvLastMessage, tvTime, tvCount;

        public MyViewHolder(View itemView) {
            super(itemView);
            ivDoctor = itemView.findViewById(R.id.ivDoctor);
            tvDoctorName = itemView.findViewById(R.id.tvDoctorName);
            tvLastMessage = itemView.findViewById(R.id.tvLastMessage);
            tvTime = itemView.findViewById(R.id.tvTime);
            tvCount = itemView.findViewById(R.id.tvCount);

        }
    }
}
