package com.connex.md.patient.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.ToggleButton;
import android.widget.ViewFlipper;

import com.connex.md.R;
import com.connex.md.animation.AnimationFactory;
import com.connex.md.custom_views.RoundRectCornerImageView;
import com.connex.md.model.Specialities;
import com.connex.md.others.PicassoTrustAll;
import com.connex.md.patient.activity.SearchDoctorActivity;
import com.squareup.picasso.Callback;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class MyBookmarksAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public Context mContext;
    public LayoutInflater inflater;
    public int type = 0;
    public ViewHolderSpecialities HOLDER;
    private Specialities specialities;
    private ArrayList<Specialities> spaArray;

    public MyBookmarksAdapter(Context context, ArrayList<Specialities> spaArray) {

        mContext = context;
        inflater = LayoutInflater.from(mContext);
        this.spaArray = spaArray;


        setHasStableIds(true);
    }

   /* public void notify(ArrayList<Specialities> list){
        this.spaArray= list;
        notifyDataSetChanged();
    }*/


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //Inflate the layout, initialize the View Holder
        RecyclerView.ViewHolder viewHolder = null;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        switch (viewType) {
            case 0:
                View v2 = inflater.inflate(R.layout.list_item_home, parent, false);
                viewHolder = new ViewHolderSpecialities(v2);
                break;
        }
        return viewHolder;
    }


    @Override
    public int getItemCount() {
        //returns the number of elements the RecyclerView will display
        return (null != spaArray ? spaArray.size() : 0);
    }

    @Override
    public int getItemViewType(int position) {
        return 0;

    }


    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public Specialities getItem(int position) {
        return spaArray.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        switch (holder.getItemViewType()) {
            case 0:
                ViewHolderSpecialities spaHolder = (ViewHolderSpecialities) holder;
                bindSpaHolder(spaHolder, position);
                break;

        }

    }

    public void bindSpaHolder(final ViewHolderSpecialities holder, final int pos) {
        specialities = getItem(pos);

        holder.tv_specialti.setText(specialities.getSpeciality());
        holder.tv_specialty.setText(specialities.getSpeciality());

        try {
            PicassoTrustAll.getInstance(mContext)
                    .load(specialities.getImage())
                    .error(R.drawable.no_img)
                    .into(holder.img_speciality, new Callback() {
                        @Override
                        public void onSuccess() {
                            holder.pBar.setVisibility(View.GONE);
                        }

                        @Override
                        public void onError() {
                            holder.pBar.setVisibility(View.GONE);
                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (specialities.getIsSaved() == 0) {
            holder.img_favrite.setChecked(false);
        } else {
            holder.img_favrite.setChecked(true);
        }

        holder.img_favrite.setEnabled(false);


        /*holder.img_favrite.setTag(pos);
        holder.img_favrite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                int position = (int) view.getTag();

                Specialities specialities = getItem(position);

                homeListener.saveRemoveCategory(specialities, pos);
                //saveRemoveCategory(String.valueOf(specialities.getId()));
            }
        });*/

        if (!TextUtils.isEmpty(specialities.getSymptoms())) {
            List<String> symptomsList = Arrays.asList(specialities.getSymptoms().split("\\s*,\\s*"));


            String symptoms = "";
            if (symptomsList.size() > 0) {
                for (int i = 0; i < symptomsList.size(); i++) {
                    symptoms += "&#62; " + symptomsList.get(i) + "<br/>";
                }
            }
            //System.out.println("symptoms::::"+symptoms);
            //String symptoms = "&#62; Rash<br/>&#62; Fungus<br/>&#62; Eczema<br/>&#62; Rash<br/>&#62; Fungus<br/>&#62; Eczema";
            holder.tvSymptoms.setText(Html.fromHtml(symptoms));
            holder.tvSymptoms.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) { // Disallow the touch request for parent scroll on touch of childview
                    v.getParent().requestDisallowInterceptTouchEvent(true);
                    return false;
                }
            });
            //holder.tvSymptoms.setMovementMethod(new ScrollingMovementMethod());
        } else {
            holder.tvSymptoms.setText("");
        }


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //fragment.clearSearchSymptoms();

                Bundle b = new Bundle();
                b.putSerializable("speciality", getItem(pos));

                Intent intent = new Intent(mContext, SearchDoctorActivity.class);
                intent.putExtras(b);
                mContext.startActivity(intent);

                /*SearchDoctorFragment searchDoctorFragment =  new SearchDoctorFragment();
                searchDoctorFragment.setArguments(b);
                ((PTDashboardActivity)mContext).replaceFragment(searchDoctorFragment);*/

            }
        });

        holder.btnConfused.setVisibility(View.GONE);

        holder.rel_info.setOnClickListener(new View.OnClickListener()

        {
            @Override
            public void onClick(View view) {
                //spelistner.OnclickInfo(pos);
                flipViewFlipper(holder.viewFlipper);
            }
        });

        holder.ivInfoBack.setOnClickListener(new View.OnClickListener()

        {
            @Override
            public void onClick(View view) {
                flipViewFlipper(holder.viewFlipper);
            }
        });
    }

    private void flipViewFlipper(ViewFlipper flipper) {

        if (flipper.getDisplayedChild() == 0) {
            AnimationFactory.flipTransition(flipper, AnimationFactory.FlipDirection.LEFT_RIGHT);
            flipper.setDisplayedChild(1);
        } else {
            AnimationFactory.flipTransition(flipper, AnimationFactory.FlipDirection.RIGHT_LEFT);
            flipper.setDisplayedChild(0);
        }

    }

    public class ViewHolderSpecialities extends RecyclerView.ViewHolder {
        public TextView tv_specialti, tv_specialty;
        public RoundRectCornerImageView img_speciality;
        public ProgressBar pBar;
        public ViewFlipper viewFlipper;
        public ImageView img_info;
        public ToggleButton img_favrite;
        public RelativeLayout rel_favrite, rel_info;
        public TextView tvSymptoms;
        ImageView ivInfoBack;
        int pos;
        ScrollView childScroll;
        Button btnConfused;

        public ViewHolderSpecialities(View v) {
            super(v);
            tv_specialti = v.findViewById(R.id.tv_specialti);
            img_speciality = v.findViewById(R.id.img_speciality);
            pBar = v.findViewById(R.id.progressBar);
            img_favrite = v.findViewById(R.id.toggle);
            img_info = v.findViewById(R.id.img_info);
            viewFlipper = v.findViewById(R.id.my_view_flipper);
            rel_favrite = v.findViewById(R.id.rel_favrite);
            rel_info = v.findViewById(R.id.rel_info);
            tvSymptoms = v.findViewById(R.id.tvSymptoms);
            ivInfoBack = v.findViewById(R.id.img_info_back);
            tv_specialty = v.findViewById(R.id.tv_specialty);
            childScroll = v.findViewById(R.id.childScroll);
            btnConfused = v.findViewById(R.id.btnConfused);
        }
    }
}
