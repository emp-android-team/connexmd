package com.connex.md.patient.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.connex.md.R;
import com.connex.md.model.Symptoms;

import java.util.ArrayList;

/**
 * Created by admin on 4/26/2017.
 */
public class SymptomsAdapter extends RecyclerView.Adapter<SymptomsAdapter.ViewHolder> {


    public ArrayList<Symptoms> alName;
    public static Context context;


    public SymptomsAdapter(Context context, ArrayList<Symptoms> alName) {
        super();
        SymptomsAdapter.context = context;
        this.alName = alName;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.custom_symptoms_row, viewGroup, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, final int i) {
        viewHolder.totalItem.setText(alName.get(i).getSymptoms());


        viewHolder.totalItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (alName.get(i).selected) {
                    viewHolder.totalItem.setBackground(context.getResources().getDrawable(R.drawable.rounded));
                    viewHolder.totalItem.setTextColor(context.getResources().getColor(R.color.colorPrimary));
                //    viewHolder.totalItem.setTypeface(Typeface.NORMAL);
                    alName.get(i).selected = false;
                } else {
                    viewHolder.totalItem.setBackground(context.getResources().getDrawable(R.drawable.rounde_item));
                    viewHolder.totalItem.setTextColor(context.getResources().getColor(R.color.white));
                   // viewHolder.totalItem.setTypeface(Typeface.DEFAULT_BOLD);
                    alName.get(i).selected = true;
                }
            }
        });

    }


    @Override
    public int getItemCount() {
        return alName.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnLongClickListener {

        public TextView totalItem;

        public ViewHolder(View itemView) {
            super(itemView);
            totalItem = itemView.findViewById(R.id.totalItem);
            itemView.setOnLongClickListener(this);
        }


        @Override
        public boolean onLongClick(View v) {
            return false;
        }
    }

}

