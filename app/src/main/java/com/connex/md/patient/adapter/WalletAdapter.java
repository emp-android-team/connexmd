package com.connex.md.patient.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.connex.md.R;
import com.connex.md.model.WalletHistory;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Created by abc on 11/21/2017.
 */

public class WalletAdapter extends RecyclerView.Adapter<WalletAdapter.MyViewHolder> {

    Context mContext;
    List<WalletHistory> walletHistoryList;

    public WalletAdapter(Context mContext, List<WalletHistory> walletHistoryList) {
        this.mContext = mContext;
        this.walletHistoryList = walletHistoryList;

        setHasStableIds(true);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public WalletAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_wallet, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final WalletAdapter.MyViewHolder holder, int position) {
        final WalletHistory walletHistory = walletHistoryList.get(position);

        holder.tvTitle.setText(walletHistory.getTitle());
        holder.tvDescription.setText(walletHistory.getDescription());
        holder.tvAmount.setText("$ "+walletHistory.getAmount());
        holder.tvDate.setText(getFormattedDate(walletHistory.getDate()));

    }

    @Override
    public int getItemCount() {
        return walletHistoryList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {


        TextView tvTitle, tvDescription, tvDate, tvAmount;

        public MyViewHolder(View itemView) {
            super(itemView);
            tvTitle = itemView.findViewById(R.id.tvTitle);
            tvDescription = itemView.findViewById(R.id.tvDescription);
            tvDate = itemView.findViewById(R.id.tvDate);
            tvAmount = itemView.findViewById(R.id.tvAmount);

        }

    }

    public String getFormattedDate(String date) {
        try {
            Log.i("DATE", "IN Android FORMAT");
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);

            SimpleDateFormat newFormat = new SimpleDateFormat("MMM dd, yyyy", Locale.US);
            format.setTimeZone(TimeZone.getTimeZone("UTC"));
            Date currentDate;

            currentDate = format.parse(date);
            newFormat.setTimeZone(TimeZone.getDefault());
            return newFormat.format(currentDate);
        } catch (ParseException e) {
            e.printStackTrace();
            return "";
        }
    }
}
