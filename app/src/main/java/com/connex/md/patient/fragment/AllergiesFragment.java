package com.connex.md.patient.fragment;


import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.connex.md.R;
import com.connex.md.interfaces.AsyncTaskListner;
import com.connex.md.others.App;
import com.connex.md.others.Internet;
import com.connex.md.ws.CallRequest;
import com.connex.md.ws.Constant;
import com.connex.md.ws.MyConstants;
import com.connex.md.ws.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import co.lujun.androidtagview.TagContainerLayout;
import co.lujun.androidtagview.TagView;

/**
 * A simple {@link Fragment} subclass.
 */
public class AllergiesFragment extends Fragment implements AsyncTaskListner {


    Button btnAdd;
    EditText etAllergies;
    TagContainerLayout tags;
    List<String> allergies = new ArrayList<>();
    String allergy;
    int pos;

    public AllergiesFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_allergies, container, false);

        btnAdd = view.findViewById(R.id.btnAdd);
        etAllergies = view.findViewById(R.id.etAllergies);
        tags = view.findViewById(R.id.tags);



        allergies.clear();
        if (MyConstants.editPatientProfile.getAllergy().size() > 0) {

            for (int i = 0; i < MyConstants.editPatientProfile.getAllergy().size(); i++) {
                String text = MyConstants.editPatientProfile.getAllergy().get(i).get("Description");
                allergies.add(text);
            }
            tags.setTags(allergies);
        }

        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                allergy = etAllergies.getText().toString().trim();

                if (TextUtils.isEmpty(etAllergies.getText().toString().trim())) {
                    return;
                }

                addAllergy(allergy);
            }
        });

        tags.setOnTagClickListener(new TagView.OnTagClickListener() {
            @Override
            public void onTagClick(int position, String text) {

            }

            @Override
            public void onTagLongClick(int position, String text) {

            }

            @Override
            public void onTagCrossClick(int position) {
                pos = position;
                new AlertDialog.Builder(getActivity())
                        .setTitle("Alert!")
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setMessage("Would you like to delete Allergy?")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                deleteAllergy(pos);
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })
                        .show();
            }
        });

        return view;
    }

    private void deleteAllergy(int position) {
        if (!Internet.isAvailable(getActivity())) {
            Internet.showAlertDialog(getActivity(), "Error!", "No Internet Connection", false);

            return;
        }

        String urlStr = "userMedicalAllergy";

        Map<String, String> map = new HashMap<String, String>();
        map.put("url", MyConstants.BASE_URL + urlStr);
        map.put("ApiToken", MyConstants.API_TOKEN);
        map.put("Version", MyConstants.WS_VERSION);
        map.put("UserId", App.user.getUserID());
        map.put("Description", MyConstants.editPatientProfile.getAllergy().get(position).get("Description"));
        map.put("IsDelete", "1");
        map.put("DeleteId", MyConstants.editPatientProfile.getAllergy().get(position).get("id"));

        new CallRequest(AllergiesFragment.this).deleteAllergy(map);
    }

    private void addAllergy(String allergy) {
        if (!Internet.isAvailable(getActivity())) {
            Internet.showAlertDialog(getActivity(), "Error!", "No Internet Connection", false);

            return;
        }

        String urlStr = "userMedicalAllergy";

        Map<String, String> map = new HashMap<String, String>();
        map.put("url", MyConstants.BASE_URL + urlStr);
        map.put("ApiToken", MyConstants.API_TOKEN);
        map.put("Version", MyConstants.WS_VERSION);
        map.put("UserId", App.user.getUserID());
        map.put("Description", allergy);
        map.put("IsDelete", "0");
        map.put("DeleteId", "0");

        new CallRequest(AllergiesFragment.this).addAllergy(map);
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        Utils.removeSimpleSpinProgressDialog();
        try {
            if (result != null && !result.isEmpty()) {
                Log.i("TAG", "TAG Result : " + result);
                switch (request) {
                    case addAllergy:
                        Utils.removeSimpleSpinProgressDialog();

                        try {
                            JSONObject mainObj = new JSONObject(result);
                            if (mainObj.getString("error_code").equalsIgnoreCase("0")) {
                                if (mainObj.getJSONArray("result") != null && mainObj.getJSONArray("result").length() > 0) {
                                    String id = mainObj.getJSONArray("result").getJSONObject(0).getString("id");

                                    HashMap<String, String> map = new HashMap<>();
                                    map.put("id", id);
                                    map.put("Description", allergy);

                                    MyConstants.editPatientProfile.getAllergy().add(map);
                                    allergies.clear();
                                    for (int i = 0; i < MyConstants.editPatientProfile.getAllergy().size(); i++) {
                                        String text = MyConstants.editPatientProfile.getAllergy().get(i).get("Description");
                                        allergies.add(text);
                                    }
                                    tags.setTags(allergies);
                                    etAllergies.setText("");
                                }

                            } else {
                                Utils.showToast(mainObj.getString("error_string"), getActivity());
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;

                    case deleteAllergy:
                        Utils.removeSimpleSpinProgressDialog();

                        try {
                            JSONObject mainObj = new JSONObject(result);
                            if (mainObj.getString("error_code").equalsIgnoreCase("0")) {

                                MyConstants.editPatientProfile.getAllergy().remove(pos);
                                allergies.clear();
                                for (int i = 0; i < MyConstants.editPatientProfile.getAllergy().size(); i++) {
                                    String text = MyConstants.editPatientProfile.getAllergy().get(i).get("Description");
                                    allergies.add(text);
                                }
                                tags.setTags(allergies);
                                etAllergies.setText("");

                            } else {
                                Utils.showToast(mainObj.getString("error_string"), getActivity());
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;
                }
            }
        } catch (Exception e) {
            Utils.removeSimpleSpinProgressDialog();
            e.printStackTrace();
            Utils.showToast("Please try again later", getActivity());
        }
    }

    @Override
    public void onProgressUpdate(String uniqueMessageId, int progres) {

    }

    @Override
    public void onProgressComplete(String uniqueMessageId, String result, Constant.REQUESTS
            request) {

    }
}
