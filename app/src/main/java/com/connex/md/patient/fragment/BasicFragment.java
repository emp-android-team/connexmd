package com.connex.md.patient.fragment;


import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.connex.md.R;
import com.connex.md.interfaces.AsyncTaskListner;
import com.connex.md.others.App;
import com.connex.md.others.Internet;
import com.connex.md.others.PicassoTrustAll;
import com.connex.md.patient.activity.PhoneAuthActivity;
import com.connex.md.utils.ZeroPaddingArrayAdapter;
import com.connex.md.ws.CallRequest;
import com.connex.md.ws.Constant;
import com.connex.md.ws.MyConstants;
import com.connex.md.ws.Utils;
import com.squareup.picasso.Callback;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import static android.app.Activity.RESULT_OK;
import static com.connex.md.ws.Utils.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE;

/**
 * A simple {@link Fragment} subclass.
 */
public class BasicFragment extends Fragment implements AsyncTaskListner {

    private static int RESULT_LOAD_IMG_DP = 1;
    private static int RESULT_CROP_DP = 3;
    public LinearLayout llUploadPic;
    public ImageView ivPatient, ivEditTop;
    public EditText etEmail, etPhoneNumber, etBirthDate, etHeight, etWeight;
    public Button btnSavePatient;
    Spinner spinnerGender, spinnerSmoker, spinnerUnitWeight, spinnerUnitHeight;
    String filePath = "";
    String filePathFirst = "";
    String email = "", phone = "", birthday = "", height = "", weight = "", gender = "", smoker = "", unitWeight = "", unitHeight = "";
    String[] genderArray = {"-", "Male", "Female", "Other"};
    String[] smokerArray = {"-", "Yes", "No"};
    String[] weightUnitArray = {"kg", "lbs"};
    String[] heightUnitArray = {"cm", "ft"};
    DatePickerDialog DialogDOB;
    String dateofbirth;
    boolean withImage = false;
    SharedPreferences sharedpreferences;
    private SimpleDateFormat dateFormatter;

    public BasicFragment() {
        // Required empty public constructor
    }

    public static Bitmap decodeSampledBitmapFromPath(String path, int reqWidth, int reqHeight) {

        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, options);

        options.inSampleSize = calculateInSampleSize(options, reqWidth,
                reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        Bitmap bmp = BitmapFactory.decodeFile(path, options);
        return bmp;
    }

    public static int calculateInSampleSize(BitmapFactory.Options options,
                                            int reqWidth, int reqHeight) {

        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            if (width > height) {
                inSampleSize = Math.round((float) height / (float) reqHeight);
            } else {
                inSampleSize = Math.round((float) width / (float) reqWidth);
            }
        }
        return inSampleSize;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_basic, container, false);

        llUploadPic = view.findViewById(R.id.ll_upload_pic);
        ivPatient = view.findViewById(R.id.ivPatient);
        ivEditTop = view.findViewById(R.id.ivEditTop);
        etEmail = view.findViewById(R.id.etEmail);
        etPhoneNumber = view.findViewById(R.id.etPhoneNumber);
        etBirthDate = view.findViewById(R.id.etBirthDate);
        etHeight = view.findViewById(R.id.etHeight);
        etWeight = view.findViewById(R.id.etWeight);
        btnSavePatient = view.findViewById(R.id.btnSavePatient);
        spinnerGender = view.findViewById(R.id.spinnerGender);
        spinnerSmoker = view.findViewById(R.id.spinnerSmoker);
        spinnerUnitWeight = view.findViewById(R.id.spinnerUnitWeight);
        spinnerUnitHeight = view.findViewById(R.id.spinnerUnitHeight);

        sharedpreferences = getActivity().getSharedPreferences(MyConstants.PREF, Context.MODE_PRIVATE);

        ArrayAdapter<String> genderAdapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_spinner_item, genderArray);
        genderAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerGender.setAdapter(genderAdapter);

        ArrayAdapter<String> smokerAdapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_spinner_item, smokerArray);
        smokerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerSmoker.setAdapter(smokerAdapter);

        ZeroPaddingArrayAdapter<String> unitWeightAdapter = new ZeroPaddingArrayAdapter<String>(getActivity(),
                android.R.layout.simple_spinner_item, weightUnitArray);
        unitWeightAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerUnitWeight.setAdapter(unitWeightAdapter);

        ZeroPaddingArrayAdapter<String> unitHeightAdapter = new ZeroPaddingArrayAdapter<String>(getActivity(),
                android.R.layout.simple_spinner_item, heightUnitArray);
        unitHeightAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerUnitHeight.setAdapter(unitHeightAdapter);

        System.out.println("profile::" + MyConstants.editPatientProfile);
        etEmail.setText(MyConstants.editPatientProfile.getEmail());
        etPhoneNumber.setText(MyConstants.editPatientProfile.getPhone());
        if (!MyConstants.editPatientProfile.getBirthDate().equalsIgnoreCase("0000-00-00")) {
            etBirthDate.setText(MyConstants.editPatientProfile.getBirthDate());
        }
        etHeight.setText(MyConstants.editPatientProfile.getHeight());
        etWeight.setText(MyConstants.editPatientProfile.getWeight());

        if (MyConstants.editPatientProfile.getGender().equalsIgnoreCase("Other")) {
            spinnerGender.setSelection(3);
        } else if (MyConstants.editPatientProfile.getGender().equalsIgnoreCase("Female")) {
            spinnerGender.setSelection(2);
        } else if (MyConstants.editPatientProfile.getGender().equalsIgnoreCase("Male")) {
            spinnerGender.setSelection(1);
        } else {
            spinnerGender.setSelection(0);
        }

        if (MyConstants.editPatientProfile.getSmoke().equalsIgnoreCase("No")) {
            spinnerSmoker.setSelection(2);
        } else if (MyConstants.editPatientProfile.getSmoke().equalsIgnoreCase("Yes")) {
            spinnerSmoker.setSelection(1);
        } else {
            spinnerSmoker.setSelection(0);
        }

        if (MyConstants.editPatientProfile.getUnit().equalsIgnoreCase("kg")) {
            spinnerUnitWeight.setSelection(0);
        } else {
            spinnerUnitWeight.setSelection(1);
        }

        if (MyConstants.editPatientProfile.getUnitHeight().equalsIgnoreCase("cm")) {
            spinnerUnitHeight.setSelection(0);
        } else {
            spinnerUnitHeight.setSelection(1);
        }

        if (!TextUtils.isEmpty(MyConstants.editPatientProfile.getProfilePic())) {
            try {
                PicassoTrustAll.getInstance(getActivity())
                        .load(MyConstants.editPatientProfile.getProfilePic())
                        .placeholder(R.drawable.avatar)
                        .error(R.drawable.avatar)
                        .into(ivPatient, new Callback() {
                            @Override
                            public void onSuccess() {
                            }

                            @Override
                            public void onError() {
                            }
                        });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        dateFormatter = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        Calendar newCalendar = Calendar.getInstance();
        DialogDOB = new DatePickerDialog(getActivity(), R.style.DialogTheme, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                etBirthDate.setText(dateFormatter.format(newDate.getTime()));
                dateofbirth = dateFormatter.format(newDate.getTime());
                System.out.println("date of birth" + dateofbirth);

                if (!dateofbirth.equalsIgnoreCase(MyConstants.editPatientProfile.getBirthDate())) {
                    btnSavePatient.setVisibility(View.VISIBLE);
                } else {
                    btnSavePatient.setVisibility(View.GONE);
                }
            }

        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));

        DialogDOB.getDatePicker().setMaxDate(System.currentTimeMillis());
        etBirthDate.setInputType(InputType.TYPE_NULL);
        etBirthDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                etBirthDate.requestFocus();
                Utils.hideKeyboard(getActivity(), view);
                DialogDOB.show();
            }
        });

        etPhoneNumber.setInputType(InputType.TYPE_NULL);
        etPhoneNumber.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                etPhoneNumber.requestFocus();

                Intent intent = new Intent(getActivity(), PhoneAuthActivity.class);
                intent.putExtra("profileUpdate", "1");
                startActivityForResult(intent, 11);
            }
        });

        etHeight.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().equalsIgnoreCase(MyConstants.editPatientProfile.getHeight())) {
                    btnSavePatient.setVisibility(View.GONE);
                } else {
                    btnSavePatient.setVisibility(View.VISIBLE);
                }
            }
        });

        etWeight.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().equalsIgnoreCase(MyConstants.editPatientProfile.getWeight())) {
                    btnSavePatient.setVisibility(View.GONE);
                } else {
                    btnSavePatient.setVisibility(View.VISIBLE);
                }
            }
        });

        spinnerGender.postDelayed(new Runnable() {
            @Override
            public void run() {
                spinnerGender.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                        if (i != 0) {
                            gender = genderArray[i];
                            //if (!TextUtils.isEmpty(MyConstants.editPatientProfile.getGender())) {
                            if (gender.equalsIgnoreCase(MyConstants.editPatientProfile.getGender())) {
                                btnSavePatient.setVisibility(View.GONE);
                            } else {
                                btnSavePatient.setVisibility(View.VISIBLE);
                            }
                        } else {
                            gender = "";
                        }
                        // }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {
                        gender = "";
                        btnSavePatient.setVisibility(View.GONE);
                    }
                });
            }
        }, 3000);

        spinnerSmoker.postDelayed(new Runnable() {
            @Override
            public void run() {
                spinnerSmoker.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                        if (i != 0) {
                            smoker = smokerArray[i];
                            //if (!TextUtils.isEmpty(MyConstants.editPatientProfile.getSmoke())) {
                            if (smoker.equalsIgnoreCase(MyConstants.editPatientProfile.getSmoke())) {
                                btnSavePatient.setVisibility(View.GONE);
                            } else {
                                btnSavePatient.setVisibility(View.VISIBLE);
                            }
                        } else {
                            smoker = "";
                        }
                        //}
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {
                        smoker = "";
                        btnSavePatient.setVisibility(View.GONE);
                    }
                });
            }
        }, 3000);

        //unitWeight = weightUnitArray[0];

        /*spinnerUnitWeight.postDelayed(new Runnable() {
            @Override
            public void run() {*/
        spinnerUnitWeight.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                unitWeight = weightUnitArray[i];
                if (!TextUtils.isEmpty(MyConstants.editPatientProfile.getUnit())) {
                    if (unitWeight.equalsIgnoreCase(MyConstants.editPatientProfile.getUnit())) {
                        btnSavePatient.setVisibility(View.GONE);
                    } else {
                        btnSavePatient.setVisibility(View.VISIBLE);
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                unitWeight = weightUnitArray[0];
                btnSavePatient.setVisibility(View.GONE);
            }
        });
            /*}
        }, 3000);*/

        //unitHeight = heightUnitArray[0];

        /*spinnerUnitHeight.postDelayed(new Runnable() {
            @Override
            public void run() {*/
        spinnerUnitHeight.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                unitHeight = heightUnitArray[i];
                if (!TextUtils.isEmpty(MyConstants.editPatientProfile.getUnitHeight())) {
                    if (unitHeight.equalsIgnoreCase(MyConstants.editPatientProfile.getUnitHeight())) {
                        btnSavePatient.setVisibility(View.GONE);
                    } else {
                        btnSavePatient.setVisibility(View.VISIBLE);
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                unitHeight = heightUnitArray[0];
                btnSavePatient.setVisibility(View.GONE);
            }
        });
           /* }
        }, 3000);*/

        etEmail.setFocusable(false);
        etEmail.setEnabled(false);
        etEmail.setCursorVisible(false);

        etPhoneNumber.setFocusable(false);
        etPhoneNumber.setEnabled(false);
        etPhoneNumber.setCursorVisible(false);

        if (TextUtils.isEmpty(etPhoneNumber.getText().toString())) {
            //etPhoneNumber.setFocusableInTouchMode(true);
            etPhoneNumber.setEnabled(true);
            //etPhoneNumber.setCursorVisible(true);
        }

        /*etBirthDate.setFocusable(false);
        etBirthDate.setEnabled(false);
        etBirthDate.setCursorVisible(false);

        etHeight.setFocusable(false);
        etHeight.setEnabled(false);
        etHeight.setCursorVisible(false);

        tvUnitHeight.setEnabled(false);

        etWeight.setFocusable(false);
        etWeight.setEnabled(false);
        etWeight.setCursorVisible(false);

        spinnerGender.setEnabled(false);
        spinnerGender.setClickable(false);
        spinnerSmoker.setEnabled(false);
        spinnerSmoker.setClickable(false);
        spinnerUnitWeight.setEnabled(false);
        spinnerUnitWeight.setClickable(false);*/

        /*ivEditTop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (TextUtils.isEmpty(etPhoneNumber.getText().toString())) {
                    //etPhoneNumber.setFocusableInTouchMode(true);
                    etPhoneNumber.setEnabled(true);
                    //etPhoneNumber.setCursorVisible(true);
                }

                etBirthDate.setEnabled(true);

                etHeight.setFocusableInTouchMode(true);
                etHeight.setEnabled(true);
                etHeight.setCursorVisible(true);

                tvUnitHeight.setEnabled(true);

                etWeight.setFocusableInTouchMode(true);
                etWeight.setEnabled(true);
                etWeight.setCursorVisible(true);

                etHeight.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                        if (charSequence.length() > 0) {
                            tvUnitHeight.setEnabled(true);
                        } else {
                            tvUnitHeight.setEnabled(false);
                        }
                    }

                    @Override
                    public void afterTextChanged(Editable editable) {

                    }
                });

                spinnerGender.setEnabled(true);
                spinnerGender.setClickable(true);
                spinnerSmoker.setEnabled(true);
                spinnerSmoker.setClickable(true);
                spinnerUnitWeight.setEnabled(true);
                spinnerUnitWeight.setClickable(true);
            }
        });*/

        llUploadPic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Utils.checkPermission(getActivity())) {
                    loadImageFromGalleryForDp();
                }
            }
        });

        btnSavePatient.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                email = etEmail.getText().toString().trim();
                phone = etPhoneNumber.getText().toString().trim();
                birthday = etBirthDate.getText().toString().trim();
                height = etHeight.getText().toString().trim();
                weight = etWeight.getText().toString().trim();

                saveDetails();
            }
        });

        return view;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            loadImageFromGalleryForDp();
        }
    }

    private void saveDetails() {

        if (!Internet.isAvailable(getActivity())) {
            Internet.showAlertDialog(getActivity(), "Error!", "No Internet Connection", false);

            return;
        }

        String urlStr = "userProfileUpdate";

        Map<String, String> map = new HashMap<String, String>();
        map.put("url", MyConstants.BASE_URL + urlStr);
        map.put("ApiToken", MyConstants.API_TOKEN);
        map.put("Version", MyConstants.WS_VERSION);
        map.put("UserId", App.user.getUserID());
        map.put("FirstName", App.user.getFirstName());
        map.put("LastName", App.user.getLastName());
        map.put("SocialId", sharedpreferences.getString(MyConstants.SOCIAL_ID, "0"));
        map.put("Phone", phone);
        map.put("BirthDate", birthday);
        map.put("Gender", gender);
        map.put("Smoke", smoker);
        map.put("Height", height);
        map.put("Weight", weight);
        map.put("Unit", unitWeight);
        map.put("HeightUnit", unitHeight);
        map.put("SocialType", App.user.getLoginType());
        if (!TextUtils.isEmpty(filePath)) {
            withImage = true;
            map.put("ProfilePic", filePath);

        }

        new CallRequest(BasicFragment.this).updatePatientProfile(map, withImage);
    }

    public void loadImageFromGalleryForDp() {
        // Create intent to Open Image applications like Gallery, Google Photos
        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        // Start the Intent
        startActivityForResult(galleryIntent, RESULT_LOAD_IMG_DP);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        try {
            // When an Image is picked
            if (requestCode == RESULT_LOAD_IMG_DP && resultCode == RESULT_OK
                    && null != data) {
                // Get the Image from data

                Uri selectedImage = data.getData();
                String[] filePathColumn = {MediaStore.Images.Media.DATA};

                // Get the cursor
                Cursor cursor = getActivity().getContentResolver().query(selectedImage,
                        filePathColumn, null, null, null);
                // Move to first row
                int columnIndex = 0;
                if (cursor != null) {
                    cursor.moveToFirst();
                    columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    filePathFirst = cursor.getString(columnIndex);
                    System.out.println(filePathFirst);
                    cursor.close();


                    /*ivUser.setImageBitmap(BitmapFactory
                            .decodeFile(filePath));*/
                }

                doCropDP(filePathFirst);

            }

            if (requestCode == RESULT_CROP_DP) {
                if (resultCode == Activity.RESULT_OK) {
                    if (!TextUtils.isEmpty(filePath)) {

                        System.out.println("path***" + filePath);
                        Bitmap selectedBitmap = decodeSampledBitmapFromPath(filePath, 100, 100);
                        System.out.println("selectedBitmap:::::" + selectedBitmap);

                        // Set The Bitmap Data To ImageView
                        ivPatient.setImageBitmap(selectedBitmap);
                        //ivPageDp.setScaleType(ImageView.ScaleType.FIT_XY);
                        btnSavePatient.setVisibility(View.VISIBLE);
                    }
                }
            }

            if (requestCode == 11) {
                if (data != null) {
                    String phone_number = data.getStringExtra("phone_number");
                    System.out.println("phone_number" + phone_number);
                    etPhoneNumber.setText(phone_number);

                    btnSavePatient.setVisibility(View.VISIBLE);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void doCropDP(String picPath) {
        try {

            Intent cropIntent = new Intent("com.android.camera.action.CROP");

            File f = new File(picPath);
            Uri contentUri = Uri.fromFile(f);

            cropIntent.setDataAndType(contentUri, "image/*");

            cropIntent.putExtra("crop", "true");
            // indicate aspect of desired crop
            cropIntent.putExtra("aspectX", 1);
            cropIntent.putExtra("aspectY", 1);
            // indicate output X and Y
            cropIntent.putExtra("outputX", 256);
            cropIntent.putExtra("outputY", 256);
            // retrieve data on return
            cropIntent.putExtra("return-data", false);

            File sdCardDirectory = new File(Environment.getExternalStorageDirectory().getPath() + "/ConnexMd/profile_pictures");

            if (!sdCardDirectory.exists()) {
                sdCardDirectory.mkdirs();
            }

            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss")
                    .format(new Date());

            String nw = "Profile_" + timeStamp + ".jpeg";

            File image = new File(sdCardDirectory, nw);

            //uploadPathDp = sdCardDirectory + "/" + nw;
            filePath = new File(sdCardDirectory, nw).getAbsolutePath();
            System.out.println("UploadPath:::" + filePath);

            try {
                image.createNewFile();
            } catch (IOException ex) {
                Log.e("io", ex.getMessage());
            }

            Uri uri;
            /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                uri = FileProvider.getUriForFile(getActivity(),
                        BuildConfig.APPLICATION_ID + ".provider",
                        image);
            } else {*/
            uri = Uri.fromFile(image);
            //}

            cropIntent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
            // start the activity - we handle returning in onActivityResult
            startActivityForResult(cropIntent, RESULT_CROP_DP);
        } catch (ActivityNotFoundException anfe) {
            String errorMessage = "Your device doesn't support the crop action!";
            Toast toast = Toast.makeText(getActivity(), errorMessage, Toast.LENGTH_SHORT);
            toast.show();
        }
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        Utils.removeSimpleSpinProgressDialog();
        try {
            if (result != null && !result.isEmpty()) {
                Log.i("TAG", "TAG Result : " + result);
                switch (request) {
                    case updatePatientProfile:
                        Utils.removeSimpleSpinProgressDialog();

                        try {
                            JSONObject mainObj = new JSONObject(result);
                            if (mainObj.getString("error_code").equalsIgnoreCase("0")) {

                                if (mainObj.getJSONArray("result") != null && mainObj.getJSONArray("result").length() > 0) {
                                    JSONArray jsonArray = mainObj.getJSONArray("result");
                                    JSONObject jsonObject = jsonArray.getJSONObject(0);
                                    JSONArray user_detail = jsonObject.getJSONArray("user_detail");
                                    JSONObject userDetailObj = user_detail.getJSONObject(0);

                                    MyConstants.editPatientProfile.setEmail(email);
                                    MyConstants.editPatientProfile.setPhone(phone);
                                    MyConstants.editPatientProfile.setBirthDate(birthday);
                                    MyConstants.editPatientProfile.setHeight(height);
                                    MyConstants.editPatientProfile.setWeight(weight);
                                    MyConstants.editPatientProfile.setGender(gender);
                                    MyConstants.editPatientProfile.setSmoke(smoker);
                                    MyConstants.editPatientProfile.setUnit(unitWeight);
                                    MyConstants.editPatientProfile.setUnitHeight(unitHeight);
                                    MyConstants.editPatientProfile.setProfilePic(userDetailObj.getString("ProfilePic"));

                                    SharedPreferences.Editor editor = sharedpreferences.edit();
                                    editor.putString(MyConstants.PROFILE_PIC, MyConstants.editPatientProfile.getProfilePic());
                                    editor.commit();
                                    App.user.setProfileUrl(MyConstants.editPatientProfile.getProfilePic());

                                    etEmail.setFocusable(false);
                                    etEmail.setEnabled(false);
                                    etEmail.setCursorVisible(false);

                                    etPhoneNumber.setFocusable(false);
                                    etPhoneNumber.setEnabled(false);
                                    etPhoneNumber.setCursorVisible(false);

                                    if (TextUtils.isEmpty(etPhoneNumber.getText().toString())) {
                                        //etPhoneNumber.setFocusableInTouchMode(true);
                                        etPhoneNumber.setEnabled(true);
                                        //etPhoneNumber.setCursorVisible(true);
                                    }

                                    btnSavePatient.setVisibility(View.GONE);

                                    /*etBirthDate.setFocusable(false);
                                    etBirthDate.setEnabled(false);
                                    etBirthDate.setCursorVisible(false);

                                    etHeight.setFocusable(false);
                                    etHeight.setEnabled(false);
                                    etHeight.setCursorVisible(false);
                                    tvUnitHeight.setEnabled(false);

                                    etWeight.setFocusable(false);
                                    etWeight.setEnabled(false);
                                    etWeight.setCursorVisible(false);

                                    spinnerGender.setEnabled(false);
                                    spinnerGender.setClickable(false);
                                    spinnerSmoker.setEnabled(false);
                                    spinnerSmoker.setClickable(false);
                                    spinnerUnitWeight.setEnabled(false);
                                    spinnerUnitWeight.setClickable(false);*/

                                    Utils.showToast("Profile saved successfully", getActivity());
                                }
                            } else {
                                Utils.showToast(mainObj.getString("error_string"), getActivity());
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;
                }
            }
        } catch (Exception e) {
            Utils.removeSimpleSpinProgressDialog();
            e.printStackTrace();
            Utils.showToast("Please try again later", getActivity());
        }
    }

    @Override
    public void onProgressUpdate(String uniqueMessageId, int progres) {

    }

    @Override
    public void onProgressComplete(String uniqueMessageId, String result, Constant.REQUESTS request) {

    }
}
