package com.connex.md.patient.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.connex.md.others.App;
import com.connex.md.patient.adapter.BookingHistoryAdapter;
import com.connex.md.R;
import com.connex.md.interfaces.AsyncTaskListner;
import com.connex.md.model.BookingHistory;
import com.connex.md.others.Internet;
import com.connex.md.utils.DividerItemDecoration;
import com.connex.md.ws.CallRequest;
import com.connex.md.ws.Constant;
import com.connex.md.ws.MyConstants;
import com.connex.md.ws.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class BookingHistoryFragment extends Fragment implements AsyncTaskListner {

    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    private RecyclerView rvBookingHistory;
    private ImageView ivNoBookings;
    private BookingHistoryAdapter bookingHistoryAdapter;
    private SwipeRefreshLayout swipe_container;

    private LinearLayout llConfirmed, llUnconfirmed;
    private TextView tvConfirmed, tvUnconfirmed;
    private View lineConfirmed, lineUnconfirmed;

    private SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
    private SimpleDateFormat format = new SimpleDateFormat("dd MMM yyyy", Locale.US);

    private List<BookingHistory> bookingHistoryList = new ArrayList<>();

    public BookingHistoryFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_booking_history, container, false);

        ButterKnife.bind(this, view);

        setupToolbar();

        rvBookingHistory = view.findViewById(R.id.rvBookingHistory);
        ivNoBookings = view.findViewById(R.id.iv_no_bookings);
        llConfirmed = view.findViewById(R.id.llConfirmed);
        llUnconfirmed = view.findViewById(R.id.llUnconfirmed);
        tvConfirmed = view.findViewById(R.id.tvConfirmed);
        tvUnconfirmed = view.findViewById(R.id.tvUnconfirmed);
        lineConfirmed = view.findViewById(R.id.lineConfirmed);
        lineUnconfirmed = view.findViewById(R.id.lineUnconfirmed);
        swipe_container = view.findViewById(R.id.swipe_container);

        setBookingHistoryData();

        swipe_container.setColorScheme(android.R.color.holo_blue_dark,
                android.R.color.holo_blue_light,
                android.R.color.holo_green_dark,
                android.R.color.holo_green_light);

        swipe_container.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (App.user.getUser_Type().equalsIgnoreCase(MyConstants.USER_PT)) {
                    getBookingHistory();
                } else {
                    getDoctorBookingHistory();
                }
            }
        });


        if (App.user.getUser_Type().equalsIgnoreCase(MyConstants.USER_PT) && MyConstants.isBookingHistoryLoad) {
            getBookingHistory();
            MyConstants.isBookingHistoryLoad = false;

        } else if (App.user.getUser_Type().equalsIgnoreCase(MyConstants.USER_DR) && MyConstants.isBookingHistoryLoad) {
            getDoctorBookingHistory();
            MyConstants.isBookingHistoryLoad = false;
        }

        setHasOptionsMenu(true);
        return view;
    }

    private void setupToolbar() {
        ((AppCompatActivity) getActivity()).setSupportActionBar(mToolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Health");
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void getDoctorBookingHistory() {
        if (!Internet.isAvailable(getActivity())) {
            Internet.showAlertDialog(getActivity(), "Error!", "No Internet Connection", false);

            return;
        }

        String urlStr = "getAllBookings";

        Map<String, String> map = new HashMap<String, String>();
        map.put("url", MyConstants.DOCTOR_BASE_URL + urlStr);
        map.put("ApiToken", MyConstants.API_TOKEN);
        map.put("Version", MyConstants.WS_VERSION);
        map.put("DoctorId", App.user.getUserID());
        map.put("UserId", App.user.getUserID());

        new CallRequest(BookingHistoryFragment.this).bookingHistoryDoctor(map);
    }

    private void getBookingHistory() {
        if (!Internet.isAvailable(getActivity())) {
            Internet.showAlertDialog(getActivity(), "Error!", "No Internet Connection", false);

            return;
        }

        //String urlStr = "bookingHistory";
        String urlStr = "getAllBookings";

        Map<String, String> map = new HashMap<String, String>();
        if (MyConstants.isGuest.equalsIgnoreCase("1")) {
            map.put("url", MyConstants.GUEST_BASE_URL + "guestBookingHistory");
            map.put("GuestId", App.user.getUserID());

        } else {
            map.put("url", MyConstants.BASE_URL + urlStr);
            map.put("UserId", App.user.getUserID());
        }
        map.put("ApiToken", MyConstants.API_TOKEN);
        map.put("Version", MyConstants.WS_VERSION);

        new CallRequest(BookingHistoryFragment.this).bookingHistory(map);
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        Utils.removeSimpleSpinProgressDialog();
        try {
            if (result != null && !result.isEmpty()) {
                Log.i("TAG", "TAG Result : " + result);

                switch (request) {
                    case getAllBookings:
                        Utils.removeSimpleSpinProgressDialog();
                        try {
                            JSONObject mainObj = new JSONObject(result);
                            if (mainObj.getString("error_code").equalsIgnoreCase("0")) {
                                bookingHistoryList.clear();

                                MyConstants.bookingHistoryList.clear();
                                MyConstants.Wallet_Balance = mainObj.getString("WalletBalance");

                                if (mainObj.getJSONArray("result") != null && mainObj.getJSONArray("result").length() > 0) {
                                    JSONArray jsonArray = mainObj.getJSONArray("result");

                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                                        JSONObject DoctorDetail = jsonObject.getJSONObject("DoctorDetail");
                                        JSONObject BookingDetails = jsonObject.getJSONObject("BookingDetails");

                                        String ConsultTime = BookingDetails.getString("ConsultTime");

                                        Date date = null;
                                        String strDate = null;
                                        try {
                                            date = inputFormat.parse(ConsultTime);
                                            strDate = format.format(date);
                                        } catch (ParseException e) {
                                            e.printStackTrace();
                                        }
                                        //String date = format.format(Date.parse(ConsultTime));

                                        String dateFull[] = strDate.split(" ", 2);

                                        BookingHistory bookingHistory = new BookingHistory();

                                        if (BookingDetails.getString("Type").equals("1")) {
                                            bookingHistory.setDoctorId(String.valueOf(DoctorDetail.getInt("DoctorId")));
                                            bookingHistory.setDoctorCharge(String.valueOf(BookingDetails.getInt("DoctorCharge")));
                                            bookingHistory.setStatus(BookingDetails.getString("Status"));
                                            bookingHistory.setIsFree(BookingDetails.getString("IsFree"));
                                            bookingHistory.setFinalAmount(BookingDetails.getString("FinalAmount"));
                                            bookingHistory.setResponseTime(DoctorDetail.getString("ResponseTime"));
                                            bookingHistory.setConsultCharge(String.valueOf(DoctorDetail.getInt("CosultCharge")));

                                        } else {
                                            bookingHistory.setConsultCharge(String.valueOf(DoctorDetail.getInt("ConsultCharge")));
                                            bookingHistory.setDoctorCharge(String.valueOf(DoctorDetail.getInt("ConsultCharge")));
                                            bookingHistory.setAddress(DoctorDetail.getString("DoctorAddress"));
                                            bookingHistory.setStatus("1");
                                            bookingHistory.setDoctorId("");
                                            bookingHistory.setDoctorCharge("");
                                            bookingHistory.setIsFree("");
                                            bookingHistory.setFinalAmount(String.valueOf(DoctorDetail.getInt("ConsultCharge")));
                                            bookingHistory.setResponseTime("");
                                            bookingHistory.setConsultCharge("");
                                        }

                                        bookingHistory.setConsultTimeOrignal(BookingDetails.getString("ConsultTime"));
                                        bookingHistory.setProfilePic(DoctorDetail.getString("ProfilePic"));
                                        bookingHistory.setProfilePic(DoctorDetail.getString("ProfilePic"));
                                        bookingHistory.setFirstName(DoctorDetail.getString("FirstName"));
                                        bookingHistory.setLastName(DoctorDetail.getString("LastName"));
                                        bookingHistory.setConsultId(BookingDetails.getString("ConsultId"));
                                        bookingHistory.setType(BookingDetails.getString("Type"));
                                        bookingHistory.setSpeciality(DoctorDetail.getString("Speciality"));
                                        bookingHistory.setConsultTime(strDate);
                                        bookingHistory.setDate(dateFull[0]);
                                        bookingHistory.setMonth(dateFull[1]);

                                        bookingHistoryList.add(bookingHistory);
                                    }

                                    MyConstants.bookingHistoryList = bookingHistoryList;

                                    setBookingHistoryData();

                                    swipe_container.setRefreshing(false);
                                    if (MyConstants.bookingHistoryList.size() > 0) {
                                        ivNoBookings.setVisibility(View.GONE);
                                        rvBookingHistory.setVisibility(View.VISIBLE);

                                    } else {
                                        ivNoBookings.setVisibility(View.VISIBLE);
                                        rvBookingHistory.setVisibility(View.GONE);
                                    }

                                } else {
                                    Utils.showToast(mainObj.getString("error_string"), getActivity());
                                }
                            } else if (mainObj.getString("error_code").equalsIgnoreCase("5")) {
                                if (bookingHistoryList.size() > 0) {
                                    ivNoBookings.setVisibility(View.GONE);
                                    rvBookingHistory.setVisibility(View.VISIBLE);

                                } else {
                                    ivNoBookings.setVisibility(View.VISIBLE);
                                    rvBookingHistory.setVisibility(View.GONE);
                                }
                            } else {
                                Utils.showToast(mainObj.getString("error_string"), getActivity());
                            }
                            swipe_container.setRefreshing(false);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;

                    case bookingHistoryDoctor:
                        Utils.removeSimpleSpinProgressDialog();
                        try {
                            JSONObject mainObj = new JSONObject(result);
                            if (mainObj.getString("error_code").equalsIgnoreCase("0")) {
                                bookingHistoryList.clear();
                                MyConstants.bookingHistoryList.clear();

                                if (mainObj.getJSONArray("result") != null && mainObj.getJSONArray("result").length() > 0) {
                                    JSONArray jsonArray = mainObj.getJSONArray("result");

                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject jsonObject = jsonArray.getJSONObject(i);

                                        JSONObject UserDetail = jsonObject.getJSONObject("UserDetail");
                                        JSONObject BookingDetails = jsonObject.getJSONObject("BookingDetails");

                                        String ConsultTime = BookingDetails.getString("ConsultTime");

                                        Date date = null;
                                        String strDate = null;
                                        try {
                                            date = inputFormat.parse(ConsultTime);
                                            strDate = format.format(date);
                                        } catch (ParseException e) {
                                            e.printStackTrace();
                                        }

                                        String dateFull[] = strDate.split(" ", 2);

                                        BookingHistory bookingHistory = new BookingHistory();

                                        if (BookingDetails.getString("Type").equals("1")) {
                                            bookingHistory.setDoctorCharge(BookingDetails.getString("DoctorCharge"));
                                            bookingHistory.setFinalAmount(BookingDetails.getString("FinalAmount"));
                                            bookingHistory.setIsFree(BookingDetails.getString("IsFree"));
                                            bookingHistory.setStatus(BookingDetails.getString("Status"));

                                        }else {
                                            bookingHistory.setDoctorCharge("");
                                            bookingHistory.setFinalAmount("");
                                            bookingHistory.setIsFree("");
                                            bookingHistory.setStatus("1");
                                            bookingHistory.setFinalAmount(BookingDetails.getString("ConsultCharge"));
                                        }

                                        bookingHistory.setConsultTimeOrignal(BookingDetails.getString("ConsultTime"));
                                        bookingHistory.setDoctorId(UserDetail.getString("UserId"));
                                        bookingHistory.setFirstName(UserDetail.getString("FirstName"));
                                        bookingHistory.setLastName(UserDetail.getString("LastName"));
                                        bookingHistory.setProfilePic(UserDetail.getString("ProfilePic"));
                                        bookingHistory.setType(BookingDetails.getString("Type"));
                                        /*bookingHistory.setSpeciality(DoctorDetail.getString("Speciality"));*/
                                        bookingHistory.setConsultTime(strDate);
                                        bookingHistory.setDate(dateFull[0]);
                                        bookingHistory.setMonth(dateFull[1]);

                                        bookingHistoryList.add(bookingHistory);
                                    }

                                    MyConstants.bookingHistoryList = bookingHistoryList;

                                    setBookingHistoryData();

                                    swipe_container.setRefreshing(false);

                                    /*LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
                                    layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
                                    rvBookingHistory.setLayoutManager(layoutManager);

                                   *//* LayoutAnimationController controller = AnimationUtils.loadLayoutAnimation(rvBookingHistory.getContext(), R.anim.layout_animation_fall_down);
                                    rvBookingHistory.setLayoutAnimation(controller);
                                    rvBookingHistory.scheduleLayoutAnimation();*//*

                                    //bookingHistoryAdapter = new BookingHistoryAdapter(getActivity(), bookingHistoryList);
                                    rvBookingHistory.setHasFixedSize(true);
                                    //rvBookingHistory.addItemDecoration(new VerticalSpacingDecoration(20));
                                    rvBookingHistory.addItemDecoration(new DividerItemDecoration(getActivity(), R.drawable.divider));
                                    rvBookingHistory.setItemViewCacheSize(20);
                                    rvBookingHistory.setDrawingCacheEnabled(true);
                                    rvBookingHistory.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);

                                    //rvBookingHistory.setAdapter(bookingHistoryAdapter);

                                    llConfirmed.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            tvConfirmed.setTextColor(getResources().getColor(R.color.theme_color));
                                            lineConfirmed.setBackgroundColor(getResources().getColor(R.color.theme_color));
                                            tvUnconfirmed.setTextColor(getResources().getColor(R.color.tab_other_color));
                                            lineUnconfirmed.setBackgroundColor(getResources().getColor(R.color.white));

                                            List<BookingHistory> confirmedList = new ArrayList<>();
                                            for (int i = 0; i < MyConstants.bookingHistoryList.size(); i++) {
                                                if (MyConstants.bookingHistoryList.get(i).getStatus().equalsIgnoreCase("1")){
                                                    confirmedList.add(MyConstants.bookingHistoryList.get(i));
                                                }
                                            }

                                            LayoutAnimationController controller = AnimationUtils.loadLayoutAnimation(rvBookingHistory.getContext(), R.anim.layout_animation_fall_down);
                                            rvBookingHistory.setLayoutAnimation(controller);
                                            rvBookingHistory.scheduleLayoutAnimation();

                                            bookingHistoryAdapter = new BookingHistoryAdapter(getActivity(), confirmedList);
                                            rvBookingHistory.setAdapter(bookingHistoryAdapter);
                                            bookingHistoryAdapter.notifyDataSetChanged();
                                        }
                                    });

                                    llUnconfirmed.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            tvConfirmed.setTextColor(getResources().getColor(R.color.tab_other_color));
                                            lineConfirmed.setBackgroundColor(getResources().getColor(R.color.white));
                                            tvUnconfirmed.setTextColor(getResources().getColor(R.color.theme_color));
                                            lineUnconfirmed.setBackgroundColor(getResources().getColor(R.color.theme_color));

                                            List<BookingHistory> unconfirmedList = new ArrayList<>();
                                            for (int i = 0; i < MyConstants.bookingHistoryList.size(); i++) {
                                                if (MyConstants.bookingHistoryList.get(i).getStatus().equalsIgnoreCase("0")){
                                                    unconfirmedList.add(MyConstants.bookingHistoryList.get(i));
                                                }
                                            }

                                            LayoutAnimationController controller = AnimationUtils.loadLayoutAnimation(rvBookingHistory.getContext(), R.anim.layout_animation_fall_down);
                                            rvBookingHistory.setLayoutAnimation(controller);
                                            rvBookingHistory.scheduleLayoutAnimation();

                                            bookingHistoryAdapter = new BookingHistoryAdapter(getActivity(), unconfirmedList);
                                            rvBookingHistory.setAdapter(bookingHistoryAdapter);
                                            bookingHistoryAdapter.notifyDataSetChanged();
                                        }
                                    });

                                    llConfirmed.performClick();*/

                                    if (MyConstants.bookingHistoryList.size() > 0) {
                                        ivNoBookings.setVisibility(View.GONE);
                                        rvBookingHistory.setVisibility(View.VISIBLE);
                                    } else {
                                        ivNoBookings.setVisibility(View.VISIBLE);
                                        rvBookingHistory.setVisibility(View.GONE);
                                    }
                                } else {
                                    Utils.showToast(mainObj.getString("error_string"), getActivity());
                                }
                            } else if (mainObj.getString("error_code").equalsIgnoreCase("5")) {
                                if (bookingHistoryList.size() > 0) {
                                    ivNoBookings.setVisibility(View.GONE);
                                    rvBookingHistory.setVisibility(View.VISIBLE);
                                } else {
                                    ivNoBookings.setVisibility(View.VISIBLE);
                                    rvBookingHistory.setVisibility(View.GONE);
                                }
                            } else {
                                Utils.showToast(mainObj.getString("error_string"), getActivity());
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;
                }
            }
        } catch (Exception e) {
            Utils.removeSimpleSpinProgressDialog();
            e.printStackTrace();
            Utils.showToast("Please try again later", getActivity());
        }
    }

    private void setBookingHistoryData() {
        if (MyConstants.bookingHistoryList.size() > 0) {
            ivNoBookings.setVisibility(View.GONE);
            rvBookingHistory.setVisibility(View.VISIBLE);
        } else {
            ivNoBookings.setVisibility(View.VISIBLE);
            rvBookingHistory.setVisibility(View.GONE);
        }

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rvBookingHistory.setLayoutManager(layoutManager);

        //bookingHistoryAdapter = new BookingHistoryAdapter(getActivity(), bookingHistoryList);
        rvBookingHistory.setHasFixedSize(true);
        //rvBookingHistory.addItemDecoration(new VerticalSpacingDecoration(20));
        rvBookingHistory.addItemDecoration(new DividerItemDecoration(getActivity(), R.drawable.divider));
        rvBookingHistory.setItemViewCacheSize(100);
        rvBookingHistory.setDrawingCacheEnabled(true);
        rvBookingHistory.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);


        llConfirmed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tvConfirmed.setTextColor(getResources().getColor(R.color.theme_color));
                lineConfirmed.setBackgroundColor(getResources().getColor(R.color.theme_color));
                tvUnconfirmed.setTextColor(getResources().getColor(R.color.tab_other_color));
                lineUnconfirmed.setBackgroundColor(getResources().getColor(R.color.white));

                List<BookingHistory> confirmedList = new ArrayList<>();
                for (int i = 0; i < MyConstants.bookingHistoryList.size(); i++) {
                    if (MyConstants.bookingHistoryList.get(i).getType().equals("2") ||
                            (MyConstants.bookingHistoryList.get(i).getType().equals("1") &&
                                    MyConstants.bookingHistoryList.get(i).getStatus().equalsIgnoreCase("1"))) {
                        confirmedList.add(MyConstants.bookingHistoryList.get(i));
                    }
                }

                Collections.sort(confirmedList, new Comparator<BookingHistory>() {
                    public int compare(BookingHistory o1, BookingHistory o2) {
                        if (o1.getConsultTimeOrignal() == null || o2.getConsultTimeOrignal() == null)
                            return 0;
                        return o2.getConsultTimeOrignal().compareTo(o1.getConsultTimeOrignal());
                    }
                });


                LayoutAnimationController controller = AnimationUtils.loadLayoutAnimation(rvBookingHistory.getContext(), R.anim.layout_animation_fall_down);
                rvBookingHistory.setLayoutAnimation(controller);
                rvBookingHistory.scheduleLayoutAnimation();

                bookingHistoryAdapter = new BookingHistoryAdapter(getActivity(), confirmedList);
                rvBookingHistory.setAdapter(bookingHistoryAdapter);
                bookingHistoryAdapter.notifyDataSetChanged();
            }
        });


        llUnconfirmed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tvConfirmed.setTextColor(getResources().getColor(R.color.tab_other_color));
                lineConfirmed.setBackgroundColor(getResources().getColor(R.color.white));
                tvUnconfirmed.setTextColor(getResources().getColor(R.color.theme_color));
                lineUnconfirmed.setBackgroundColor(getResources().getColor(R.color.theme_color));

                List<BookingHistory> unconfirmedList = new ArrayList<>();
                for (int i = 0; i < MyConstants.bookingHistoryList.size(); i++) {
                    if (MyConstants.bookingHistoryList.get(i).getType().equals("1") && MyConstants.bookingHistoryList.get(i).getStatus().equalsIgnoreCase("0")) {
                        unconfirmedList.add(MyConstants.bookingHistoryList.get(i));
                    }
                }

                Collections.sort(unconfirmedList, new Comparator<BookingHistory>() {
                    public int compare(BookingHistory o1, BookingHistory o2) {
                        if (o1.getConsultTimeOrignal() == null || o2.getConsultTimeOrignal() == null)
                            return 0;
                        return o2.getConsultTimeOrignal().compareTo(o1.getConsultTimeOrignal());
                    }
                });

                LayoutAnimationController controller = AnimationUtils.loadLayoutAnimation(rvBookingHistory.getContext(), R.anim.layout_animation_fall_down);
                rvBookingHistory.setLayoutAnimation(controller);
                rvBookingHistory.scheduleLayoutAnimation();

                bookingHistoryAdapter = new BookingHistoryAdapter(getActivity(), unconfirmedList);
                rvBookingHistory.setAdapter(bookingHistoryAdapter);
                bookingHistoryAdapter.notifyDataSetChanged();

            }
        });

        llConfirmed.performClick();
    }

    @Override
    public void onProgressUpdate(String uniqueMessageId, int progres) {

    }

    @Override
    public void onProgressComplete(String uniqueMessageId, String result, Constant.REQUESTS request) {

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                getActivity().onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
