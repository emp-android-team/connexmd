package com.connex.md.patient.fragment;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.SwitchCompat;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.connex.md.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class CardPaymentFragment extends Fragment {

    public EditText etCardNumber, etCardHolderName, etMonth, etYear, etCVV;
    public SwitchCompat switchSave;
    private static CardPaymentFragment instance;

    public CardPaymentFragment() {
        // Required empty public constructor
    }

    public static synchronized CardPaymentFragment getInstance()
    {
        return instance;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_card_payment, container, false);
        instance=this;
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        etCardHolderName = view.findViewById(R.id.etCardHolderName);
        etCardNumber = view.findViewById(R.id.etCardNumber);
        etMonth = view.findViewById(R.id.etMonth);
        etYear = view.findViewById(R.id.etYear);
        etCVV = view.findViewById(R.id.etCVV);
        switchSave = view.findViewById(R.id.switchSave);

        etCardNumber.addTextChangedListener(new FourDigitCardFormatWatcher());
    }

    public boolean validateCreditCardDetails(){
        String cardNumber = etCardNumber.getText().toString();
        String cardHolderName = etCardHolderName.getText().toString().trim();
        String month = etMonth.getText().toString();
        String year = etYear.getText().toString();
        String cvv = etCVV.getText().toString();

        if (TextUtils.isEmpty(cardNumber)) {
            etCardNumber.requestFocus();
            etCardNumber.setError("Enter valid card number");
            return false;
        } else if (TextUtils.isEmpty(cardHolderName)) {
            etCardHolderName.requestFocus();
            etCardHolderName.setError("Enter card holder name");
            return false;
        } else if (TextUtils.isEmpty(month) || !month.matches("(?:0[1-9]|1[0-2])")) {
            etMonth.requestFocus();
            etMonth.setError("Enter valid month");
            return false;
        } else if (TextUtils.isEmpty(year)) {
            etYear.requestFocus();
            etYear.setError("Enter valid year");
            return false;
        } else if (TextUtils.isEmpty(cvv)) {
            etCVV.requestFocus();
            etCVV.setError("Enter valid CVV");
            return false;
        } else {
            return true;
        }
    }

    public boolean isSaveCard(){
        return switchSave.isChecked();
    }

    public String getCardNumber(){
        return etCardNumber.getText().toString();
    }

    public String getCardHolderName(){
        return etCardHolderName.getText().toString();
    }

    public String getMonth(){
        return etMonth.getText().toString();
    }

    public String getYear(){
        return etYear.getText().toString();
    }

    public String getCVV(){
        return etCVV.getText().toString();
    }

    public class FourDigitCardFormatWatcher implements TextWatcher {

        private static final char space = ' ';

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void afterTextChanged(Editable s) {
            if (s.length() > 0 && (s.length() % 5) == 0) {
                final char c = s.charAt(s.length() - 1);
                if (space == c) {
                    s.delete(s.length() - 1, s.length());
                }
            }
            if (s.length() > 0 && (s.length() % 5) == 0) {
                char c = s.charAt(s.length() - 1);
                if (Character.isDigit(c) && TextUtils.split(s.toString(), String.valueOf(space)).length <= 3) {
                    s.insert(s.length() - 1, String.valueOf(space));
                }
            }
        }
    }




}
