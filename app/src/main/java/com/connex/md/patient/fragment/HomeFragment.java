package com.connex.md.patient.fragment;


import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearSnapHelper;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SnapHelper;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.connex.md.custom_views.SpanningGridLayoutManager;
import com.connex.md.patient.activity.RXDeliveryActivity;
import com.crashlytics.android.Crashlytics;
import com.connex.md.R;
import com.connex.md.interfaces.AsyncTaskListner;
import com.connex.md.interfaces.HomeListener;
import com.connex.md.interfaces.SpecialitiesListener;
import com.connex.md.model.SearchDoctor;
import com.connex.md.model.Specialities;
import com.connex.md.others.App;
import com.connex.md.others.Internet;
import com.connex.md.patient.activity.PTDashboardActivity;
import com.connex.md.patient.activity.PTLoginActivity;
import com.connex.md.patient.activity.PhoneAuthActivity;
import com.connex.md.patient.adapter.HomeAdapter;
import com.connex.md.utils.VerticalSpacingDecoration;
import com.connex.md.ws.CallRequest;
import com.connex.md.ws.Constant;
import com.connex.md.ws.JsonParserUniversal;
import com.connex.md.ws.MyConstants;
import com.connex.md.ws.Utils;
import com.google.firebase.database.DatabaseReference;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.fabric.sdk.android.Fabric;

import static android.content.Context.MODE_PRIVATE;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment implements AsyncTaskListner, SpecialitiesListener, HomeListener {

    @BindView(R.id.btn_prescription_delivery)
    Button mPrescriptionDeliveryBtn;

    public static Context staticInstance;
    public static HomeFragment staticFragment;
    public Context instance;
    public RecyclerView rvHome;
    public EditText etSearch;
    public JsonParserUniversal jParser;
    public String nurseFirebaseChannel = "";
    public DatabaseReference fireDB, fireDbMessages, fireDbTyping, fireDbOnlineOffline, fireChannel;
    public HomeAdapter homeAdapter;
    public Specialities specialities;
    public ArrayList<Specialities> specialitiesArray = new ArrayList<>();
    //public ImageView ivSearch;
    // for nurse chat dialog
    public Dialog dialogGuest;
    public ImageView ivDrawerIcon;
    public TextView tvCount;
    private StaggeredGridLayoutManager staggeredGridLayoutManager;
    private SharedPreferences sharedpreferences;
    private SharedPreferences.Editor editor;
    private Button btnLoginNow, btnBookAsGuest;
    private int pos;
    public static int showSeeMorePosition = -1;
    private SwipeRefreshLayout swipe_container;

    public HomeFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        instance = getActivity();
        staticInstance = getActivity();
        staticFragment = this;
        jParser = new JsonParserUniversal();
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);

        Fabric.with(getActivity(), new Crashlytics());
        ButterKnife.bind(this, view);

        rvHome = view.findViewById(R.id.rvHome);
        //ivSearch = view.findViewById(R.id.imageView2);
        etSearch = view.findViewById(R.id.edit_search);
        ivDrawerIcon = view.findViewById(R.id.img_left_arrow_search);
        tvCount = view.findViewById(R.id.count);
        swipe_container = view.findViewById(R.id.swipe_container);

        rvHome.addItemDecoration(new VerticalSpacingDecoration(30));
        rvHome.setItemViewCacheSize(20);
        rvHome.setDrawingCacheEnabled(true);
        rvHome.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);

        ivDrawerIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (getActivity() != null) {
                    ((PTDashboardActivity) getActivity()).openCloseDrawer();
                }
            }
        });

        swipe_container.setColorScheme(android.R.color.holo_blue_dark,
                android.R.color.holo_blue_light,
                android.R.color.holo_green_dark,
                android.R.color.holo_green_light);

        setSpecialitiesData();
        clearSearchSymptoms();

        sharedpreferences = getActivity().getSharedPreferences(MyConstants.PREF, MODE_PRIVATE);
        if (App.user.getUser_Type().equalsIgnoreCase(MyConstants.USER_PT) && MyConstants.isGuest.equalsIgnoreCase("0"))
            mPrescriptionDeliveryBtn.setVisibility(View.VISIBLE);
        else
            mPrescriptionDeliveryBtn.setVisibility(View.GONE);

        swipe_container.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                if (!Internet.isAvailable(getActivity())) {
                    Internet.showAlertDialog(getActivity(), "Error!", "No Internet Connection", false);
                    swipe_container.setRefreshing(false);

                } else {
                    MyConstants.specialitiesList.clear();
                    MyConstants.isLoadAgain = true;

                    if (sharedpreferences.getBoolean(MyConstants.IS_LOGGED_IN, false)) {
                        if (App.user.getUser_Type().equalsIgnoreCase(MyConstants.USER_PT) && MyConstants.isGuest.equalsIgnoreCase("0")) {
                            //tvCount.setVisibility(View.VISIBLE);
                            ((PTDashboardActivity) getActivity()).setVisibilityDoctorNotes(false);
                            getDoctorNotesCount();

                        } else {
                            ((PTDashboardActivity) getActivity()).setVisibilityDoctorNotes(false);
                            //tvCount.setVisibility(View.GONE);
                        }
                    } else {
                        //tvCount.setVisibility(View.GONE);
                        ((PTDashboardActivity) getActivity()).setVisibilityDoctorNotes(false);
                    }

                    if (!Internet.isAvailable(getActivity())) {
                        Internet.showAlertDialog(getActivity(), "Error!", "No Internet Connection", false);
                        swipe_container.setRefreshing(false);

                    } else {
                        if (sharedpreferences.getBoolean("isUpdated", false)) {
                            if (sharedpreferences.getBoolean(MyConstants.IS_LOGGED_IN, false)) {
                                new CallRequest(HomeFragment.this).setDeviceToken();

                            } else {
                                if (MyConstants.isLoadAgain) {
                                    new CallRequest(HomeFragment.this).getSpecialities();
                                    MyConstants.isLoadAgain = false;
                                }
                            }
                        } else {
                            if (MyConstants.isLoadAgain) {
                                new CallRequest(HomeFragment.this).getSpecialities();
                                MyConstants.isLoadAgain = false;
                            }
                        }
                    }
                }
            }
        });

        /*if (sharedpreferences.getBoolean(MyConstants.IS_LOGGED_IN, false)) {
            if (App.user.getUser_Type().equalsIgnoreCase(MyConstants.USER_PT) && MyConstants.isGuest.equalsIgnoreCase("0")) {
                //tvCount.setVisibility(View.VISIBLE);
                ((PTDashboardActivity) getActivity()).setVisibilityDoctorNotes(true);
                getDoctorNotesCount();
            } else {
                ((PTDashboardActivity) getActivity()).setVisibilityDoctorNotes(false);
                //tvCount.setVisibility(View.GONE);
            }
        } else {
            //tvCount.setVisibility(View.GONE);
            ((PTDashboardActivity) getActivity()).setVisibilityDoctorNotes(false);
        }*/

        if (!Internet.isAvailable(getActivity())) {
            Internet.showAlertDialog(getActivity(), "Error!", "No Internet Connection", false);

        } else {
            if (sharedpreferences.getBoolean("isUpdated", false)) {
                if (sharedpreferences.getBoolean(MyConstants.IS_LOGGED_IN, false)) {
                    new CallRequest(HomeFragment.this).setDeviceToken();
                } else {
                    if (MyConstants.isLoadAgain) {
                        new CallRequest(HomeFragment.this).getSpecialities();
                        MyConstants.isLoadAgain = false;
                    }
                }
            } else {
                if (MyConstants.isLoadAgain) {
                    new CallRequest(HomeFragment.this).getSpecialities();
                    MyConstants.isLoadAgain = false;
                }
            }
        }

        etSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                etSearch.setCursorVisible(true);
            }
        });

        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                try {
                    if (homeAdapter != null) {
                        homeAdapter.filter(charSequence.toString());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        return view;
    }

    private void getDoctorNotesCount() {
        if (!Internet.isAvailable(getActivity())) {
            Internet.showAlertDialog(getActivity(), "Error!", "No Internet Connection", false);

        } else {
            Map<String, String> map = new HashMap<String, String>();
            map.put("url", MyConstants.BASE_URL + "getUnpaidCount");
            map.put("Version", MyConstants.WS_VERSION);
            map.put("UserId", App.user.getUserID());
            map.put("ApiToken", MyConstants.API_TOKEN);
            map.put("show", "");

            new CallRequest(HomeFragment.this).getDoctorNotesCount(map);
        }
    }

    @Override
    public void OnclickInfo(int lastIndex) {

    }

    public void setSpecialitiesData() {
        if (MyConstants.specialitiesList != null && MyConstants.specialitiesList.size() > 0) {

            LayoutAnimationController controller = AnimationUtils.loadLayoutAnimation(rvHome.getContext(), R.anim.layout_animation_from_right);
            rvHome.setLayoutAnimation(controller);
            rvHome.scheduleLayoutAnimation();

            staggeredGridLayoutManager = new StaggeredGridLayoutManager(2, 1);
            rvHome.setLayoutManager(staggeredGridLayoutManager);

            homeAdapter = new HomeAdapter(HomeFragment.this, MyConstants.specialitiesList, HomeFragment.this);

            rvHome.setAdapter(homeAdapter);

            //homeAdapter.setRecyclerView(rvHome);
            //rvHome.getRecycledViewPool().clear();
            homeAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onClickChat() {
        if (TextUtils.isEmpty(App.user.getUserID())) {
            openGuestDialog();
        } else {
            if (App.user.getUser_Type().equalsIgnoreCase(MyConstants.USER_PT)) {
                getNurseChatDetails();

            } else {
                Utils.showAlert("Talk to our care team is available for users only, Thanks!", getActivity());
            }
        }
    }

    private void getNurseChatDetails() {
        if (!Internet.isAvailable(getActivity())) {
            Internet.showAlertDialog(getActivity(), "Error!", "No Internet Connection", false);
            return;
        }

        Map<String, String> map = new HashMap<String, String>();

        System.out.println("is guest::" + MyConstants.isGuest);
        if (MyConstants.isGuest.equalsIgnoreCase("1")) {
            map.put("url", MyConstants.GUEST_BASE_URL + "nurseChat");
            map.put("GuestId", App.user.getUserID());
        } else {
            map.put("url", MyConstants.BASE_URL + "nurseChat");
            map.put("UserId", App.user.getUserID());
        }
        map.put("ApiToken", MyConstants.API_TOKEN);
        map.put("Version", MyConstants.WS_VERSION);
        map.put("SocialType", App.user.getLoginType());
        map.put("SocialId", sharedpreferences.getString(MyConstants.SOCIAL_ID, ""));

        new CallRequest(HomeFragment.this).getNurseChatDetailsFragment(map);
    }

    private void openGuestDialog() {
        createGuestDialog();
        initGuestDialogComponents();

        btnLoginNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogGuest.dismiss();
                Intent intent = new Intent(getActivity(), PTLoginActivity.class);
                intent.putExtra("is_logged_in", 0);
                startActivity(intent);
            }
        });

        btnBookAsGuest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogGuest.dismiss();
                MyConstants.CHAT_AS_GUEST = true;
                Intent intent = new Intent(getActivity(), PhoneAuthActivity.class);
                startActivity(intent);
            }
        });
    }

    private void createGuestDialog() {
        dialogGuest = new Dialog(getActivity(), R.style.CustomDialog);
        dialogGuest.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogGuest.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialogGuest.setContentView(R.layout.dialog_guest_popup);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialogGuest.getWindow();
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
        dialogGuest.show();
    }

    private void initGuestDialogComponents() {
        btnLoginNow = dialogGuest.findViewById(R.id.btnLoginNow);
        btnBookAsGuest = dialogGuest.findViewById(R.id.btnBookAsGuest);

        btnBookAsGuest.setText("Chat as Guest");
    }


    @OnClick(R.id.btn_prescription_delivery)
    void onPrescriptionDeliveryClick() {
        startActivity(new Intent(getActivity(), RXDeliveryActivity.class));
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        try {
            if (result != null && !result.isEmpty()) {
                Log.i("TAG", "TAG Result : " + result);

                switch (request) {
                    case getSpecialities:
                        Utils.removeSimpleSpinProgressDialog();
                        try {
                            JSONObject mainObj = new JSONObject(result);
                            if (mainObj.getString("error_code").equalsIgnoreCase("0")) {

                                editor = sharedpreferences.edit();
                                editor.putBoolean("isUpdated", false);
                                editor.apply();

                                String price = "";

                                if (sharedpreferences.getBoolean(MyConstants.IS_LOGGED_IN, false)) {
                                    if (App.user.getUser_Type().equalsIgnoreCase(MyConstants.USER_PT) && MyConstants.isGuest.equalsIgnoreCase("0"))
                                        price = mainObj.getString("error_string");

                                    else
                                        price = "";

                                } else {
                                    price = mainObj.getString("error_string");
                                }

                                specialitiesArray.clear();
                                MyConstants.specialitiesList.clear();

                                if (mainObj.getJSONArray("result") != null && mainObj.getJSONArray("result").length() > 0) {
                                    JSONArray jBranchArray = mainObj.getJSONArray("result");

                                    //System.out.println("recyvler view height---- " + rvHome.getHeight());
                                    //System.out.println("view height---- " + getResources().getDimension(R.dimen._100sdp));
                                    //System.out.println("View Position -- " + (Math.round((rvHome.getHeight()/getResources().getDimension(R.dimen._100sdp)) * 100) / 100));
                                    showSeeMorePosition = (Math.round((rvHome.getHeight()/getResources().getDimension(R.dimen._100sdp)) * 100) / 100);

                                    if (TextUtils.isEmpty(price)) {
                                        for (int i = 0; i < jBranchArray.length(); i++) {
                                            if (i == 1) {
                                                Specialities spe = new Specialities();
                                                spe.isRandom = true;
                                                specialitiesArray.add(spe);

                                            } else if ((showSeeMorePosition * 2) == i) {
                                                Specialities spe = new Specialities();
                                                spe.setShowSeeMore(true);
                                                spe.setSeeMorePosition(i);
                                                specialitiesArray.add(spe);

                                            }else {
                                                JSONObject jSpecial;

                                                if (specialitiesArray.size() > 2)
                                                    jSpecial = jBranchArray.getJSONObject(i - 2);

                                                else if (specialitiesArray.size() > 1)
                                                    jSpecial = jBranchArray.getJSONObject(i - 1);

                                                else
                                                    jSpecial = jBranchArray.getJSONObject(i);

                                                specialities = (Specialities) jParser.parseJson(jSpecial, new Specialities());
                                                System.out.println("--- Specility " + specialities.getSpeciality() + "--- " + i);

                                                ArrayList<SearchDoctor> doctorList = new ArrayList<>();
                                                JSONArray array = jSpecial.getJSONArray("Doctors");

                                                for (int j = 0; j < array.length(); j++) {
                                                    JSONObject detail_obj = array.getJSONObject(j);
                                                    SearchDoctor searchDoctor = new SearchDoctor();
                                                    searchDoctor.setFirstName(detail_obj.getString("FirstName"));
                                                    searchDoctor.setLastName(detail_obj.getString("LastName"));
                                                    searchDoctor.setDoctorId(detail_obj.getString("DoctorId"));
                                                    searchDoctor.setDotorType(detail_obj.getString("DotorType"));
                                                    searchDoctor.setAvailableFrom(detail_obj.getString("AvailableFrom"));
                                                    searchDoctor.setCosultCharge(detail_obj.getString("CosultCharge"));
                                                    searchDoctor.setCountry(detail_obj.getString("Country"));
                                                    searchDoctor.setProfilePic(detail_obj.getString("ProfilePic"));
                                                    searchDoctor.setLocalCharge(detail_obj.getString("LocalCharge"));
                                                    searchDoctor.setIsFree(detail_obj.getString("IsFree"));

                                                    doctorList.add(searchDoctor);
                                                }
                                                specialities.setSearchDoctorList(doctorList);
                                                specialitiesArray.add(specialities);
                                            }
                                        }
                                    } else {
                                        MyConstants.headerPrice = price;

                                        for (int i = 0; i < jBranchArray.length() + 2; i++) {
                                            if (i == 2) {
                                                Specialities spe = new Specialities();
                                                spe.isRandom = true;
                                                specialitiesArray.add(spe);

                                            } else if (i == 0) {
                                                Specialities spe = new Specialities();
                                                spe.isHeader = true;
                                                specialitiesArray.add(spe);

                                            } else if ((showSeeMorePosition * 2) == i) {
                                                Specialities spe = new Specialities();
                                                spe.setShowSeeMore(true);
                                                spe.setSeeMorePosition(i);
                                                specialitiesArray.add(spe);

                                            } else {
                                                JSONObject jSpecial;

                                                if (specialitiesArray.size() > 3)
                                                    jSpecial = jBranchArray.getJSONObject(i - 3);

                                                else if (specialitiesArray.size() > 2)
                                                    jSpecial = jBranchArray.getJSONObject(i - 2);

                                                else
                                                    jSpecial = jBranchArray.getJSONObject(i - 1);

                                                specialities = (Specialities) jParser.parseJson(jSpecial, new Specialities());

                                                ArrayList<SearchDoctor> doctorList = new ArrayList<>();
                                                JSONArray array = jSpecial.getJSONArray("Doctors");

                                                if (array.length() > 0) {
                                                    for (int j = 0; j < array.length(); j++) {
                                                        JSONObject detail_obj = array.getJSONObject(j);

                                                        SearchDoctor searchDoctor = new SearchDoctor();
                                                        searchDoctor.setFirstName(detail_obj.getString("FirstName"));
                                                        searchDoctor.setLastName(detail_obj.getString("LastName"));
                                                        searchDoctor.setDoctorId(detail_obj.getString("DoctorId"));
                                                        searchDoctor.setCosultCharge(detail_obj.getString("CosultCharge"));
                                                        searchDoctor.setCountry(detail_obj.getString("Country"));
                                                        searchDoctor.setProfilePic(detail_obj.getString("ProfilePic"));
                                                        searchDoctor.setLocalCharge(detail_obj.getString("LocalCharge"));
                                                        searchDoctor.setIsFree(detail_obj.getString("IsFree"));

                                                        doctorList.add(searchDoctor);
                                                    }
                                                }
                                                specialities.setSearchDoctorList(doctorList);
                                                specialitiesArray.add(specialities);
                                            }
                                        }
                                    }

                                    MyConstants.specialitiesList = specialitiesArray;
                                    setSpecialitiesData();
                                    swipe_container.setRefreshing(false);

                                    etSearch.setText("");
                                    if (App.user.getProfileUrl() != null) {
                                        if (getActivity() != null)
                                            ((PTDashboardActivity) getActivity()).setProfilePicture(getActivity());
                                    }
                                } else {
                                    Utils.showToast(mainObj.getString("error_string"), getActivity());
                                }
                            } else {
                                Utils.showToast(mainObj.getString("error_string"), getActivity());
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;

                    case nurseChatDetails:
                        Utils.removeSimpleSpinProgressDialog();
                        try {
                            JSONObject mainObj = new JSONObject(result);
                            if (mainObj.getString("error_code").equalsIgnoreCase("0")) {
                                isChildAvailable(mainObj.getJSONArray("result").getJSONObject(0));

                            } else {
                                Utils.showToast(mainObj.getString("error_string"), getActivity());
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;

                    case saveCategory:
                        Utils.removeSimpleSpinProgressDialog();
                        try {
                            JSONObject mainObj = new JSONObject(result);
                            if (mainObj.getString("error_code").equalsIgnoreCase("0")) {
                                int saved = mainObj.getInt("error_string");

                                try {
                                    MyConstants.specialitiesList.get(pos).setIsSaved(saved);
                                    homeAdapter.notifyDataSetChanged();

                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;

                    case setDeviceToken:
                        Utils.removeSimpleSpinProgressDialog();
                        try {
                            JSONObject mainObj = new JSONObject(result);
                            if (mainObj.getString("error_code").equalsIgnoreCase("0")) {
                                editor = sharedpreferences.edit();
                                editor.putBoolean("isUpdated", false);
                                editor.commit();

                            } else {
                                Utils.showToast(mainObj.getString("error_string"), getActivity());
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        new CallRequest(HomeFragment.this).getSpecialities();
                        break;

                    case doctorNoteCount:
                        try {
                            JSONObject mainObj = new JSONObject(result);
                            if (mainObj.getString("error_code").equalsIgnoreCase("0")) {
                                JSONObject object = mainObj.getJSONObject("result");
                                if (object != null) {

                                    String UnPaidCount = object.getString("UnPaidCount");
                                    tvCount.setText(UnPaidCount);

                                    if (!UnPaidCount.equalsIgnoreCase("0")) {
                                        tvCount.setVisibility(View.VISIBLE);

                                        if (getActivity() != null) {
                                            ((PTDashboardActivity) getActivity()).setDoctorNotesCount(UnPaidCount);
                                        }
                                    } else {
                                        if (getActivity() != null) {
                                            ((PTDashboardActivity) getActivity()).setDoctorNotesCount(UnPaidCount);
                                            ((PTDashboardActivity) getActivity()).setVisibilityDoctorNotes(false);
                                        }
                                        tvCount.setVisibility(View.GONE);
                                    }
                                }
                                swipe_container.setRefreshing(false);

                            } else {
                                Utils.showToast(mainObj.getString("error_string"), getActivity());
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void isChildAvailable(JSONObject resultObj) {
        try {
            final String id = resultObj.getString("id");
            final String UniqueId = resultObj.getString("UniqueId");
            final String UserId = resultObj.getString("UserId");
            final String NurseId = resultObj.getString("NurseId");
            final String Status = resultObj.getString("Status");
            final String IsGuest = resultObj.getString("IsGuest");
            final String FirstName = resultObj.getString("FirstName");
            final String LastName = resultObj.getString("LastName");
            final String ProfilePic = resultObj.getString("ProfilePic");
            final String NurseWork = resultObj.getString("NurseWork");

            PTDashboardActivity.openNurseDialog(resultObj);

            /*fireDB = FirebaseDatabase.getInstance().getReference().child(MyConstants.NURSE_CHANNEL);

            fireDB.orderByChild(UniqueId).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    Log.i("Data snapshot Nursechat", dataSnapshot.toString());
                    Utils.removeSimpleSpinProgressDialog();
                    Intent intent = new Intent(getActivity(), NurseFireChatActivity.class);


                    if (dataSnapshot.child(UniqueId).getValue() == null) {
                        intent.putExtra(MyConstants.IS_PAYMENT_DONE, true);
                    } else {
                        intent.putExtra(MyConstants.IS_PAYMENT_DONE, false);
                    }

                    intent.putExtra(MyConstants.PT_ID, UserId);
                    intent.putExtra(MyConstants.DR_ID, NurseId);
                    intent.putExtra("status", Status);
                    intent.putExtra(MyConstants.UNIQUE_CHAT_ID, UniqueId);
                    intent.putExtra(MyConstants.DR_NAME, FirstName + " " + LastName);
                    intent.putExtra(MyConstants.RECEIVER_IMAGE_URL, ProfilePic);
                    startActivity(intent);
                    //Key does not exist
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });*/
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onProgressUpdate(String uniqueMessageId, int progres) {

    }

    @Override
    public void onProgressComplete(String uniqueMessageId, String result, Constant.REQUESTS request) {

    }

    @Override
    public void saveRemoveCategory(Specialities specialities, int position) {
        if (!Internet.isAvailable(getActivity())) {
            Internet.showAlertDialog(getActivity(), "Error!", "No Internet Connection", false);

            return;
        }

        pos = position;

        System.out.println("pos::" + pos);
        System.out.println("speciality id:::" + specialities.getId());

        Map<String, String> map = new HashMap<String, String>();
        map.put("url", MyConstants.BASE_URL + "saveSpeciality");
        map.put("ApiToken", MyConstants.API_TOKEN);
        map.put("Version", MyConstants.WS_VERSION);
        map.put("SpecialityId", String.valueOf(specialities.getId()));
        map.put("UserId", App.user.getUserID());
        map.put("show", "");

        new CallRequest(HomeFragment.this).saveRemoveCategory(map);
    }

    public void clearSearchSymptoms() {
        etSearch.setText("");
    }

    @Override
    public void onResume() {
        super.onResume();
        if (MyConstants.isBackPressed) {
            clearSearchSymptoms();
            MyConstants.isBackPressed = false;
        }

        if (MyConstants.specialitiesList == null || MyConstants.specialitiesList.size() == 0) {
            new CallRequest(HomeFragment.this).getSpecialities();
        }
    }
}
