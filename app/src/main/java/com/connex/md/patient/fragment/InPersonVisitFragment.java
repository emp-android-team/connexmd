package com.connex.md.patient.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.connex.md.R;
import com.connex.md.custom_views.RobottoTextView;
import com.connex.md.interfaces.AdapterClickListner;
import com.connex.md.interfaces.AsyncTaskListner;
import com.connex.md.model.DoctorProfile;
import com.connex.md.model.SearchDoctor;
import com.connex.md.others.App;
import com.connex.md.others.Internet;
import com.connex.md.patient.activity.BookInPersonActivity;
import com.connex.md.patient.adapter.InPersonVisitAdapter;
import com.connex.md.ws.CallRequest;
import com.connex.md.ws.Constant;
import com.connex.md.ws.MyConstants;
import com.connex.md.ws.Utils;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

public class InPersonVisitFragment extends Fragment implements AdapterClickListner, AsyncTaskListner {

    @BindView(R.id.rec_doctors)
    RecyclerView mDoctorsRec;

    @BindView(R.id.txt_no_doctors)
    RobottoTextView mNoDoctorTxt;

    private ArrayList<SearchDoctor> mDoctorArr;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_in_person_visit, container, false);

        ButterKnife.bind(this, view);

        if (getArguments() != null) {
            mDoctorArr = (ArrayList<SearchDoctor>) getArguments().get("doctors");
            setDoctorAdapter();
        }

        return view;
    }


    /**
     * Set doctor adapter...
     */
    private void setDoctorAdapter() {
        if (mDoctorArr.size() > 0) {
            mNoDoctorTxt.setVisibility(View.GONE);
            mDoctorsRec.setVisibility(View.VISIBLE);
        } else {
            mNoDoctorTxt.setVisibility(View.VISIBLE);
            mDoctorsRec.setVisibility(View.GONE);
        }

        InPersonVisitAdapter searchDoctorAdapter = new InPersonVisitAdapter(InPersonVisitFragment.this, mDoctorArr);
        mDoctorsRec.setLayoutManager(new LinearLayoutManager(getActivity()));
        mDoctorsRec.setHasFixedSize(true);
        //rvSearchDoctor.addItemDecoration(new VerticalSpacingDecoration(20));
        mDoctorsRec.setItemViewCacheSize(20);
        mDoctorsRec.setDrawingCacheEnabled(true);
        mDoctorsRec.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);

        mDoctorsRec.setAdapter(searchDoctorAdapter);
    }


    private void getDRProfileData(String mDoctorId) {
        if (!Internet.isAvailable(getActivity())) {
            Internet.showAlertDialog(getActivity(), "Error!", "No Internet Connection", false);
            return;
        }

        Map<String, String> map = new HashMap<String, String>();
        map.put("url", MyConstants.BASE_URL + "doctorProfile");
        map.put("ApiToken", MyConstants.API_TOKEN);
        map.put("Version", MyConstants.WS_VERSION);
        map.put("DoctorId", mDoctorId);
        map.put("UserId", App.user.getUserID());
        map.put("IsGuest", "0");

        new CallRequest(this).getDoctorProfile(map);
    }


    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        Utils.removeSimpleSpinProgressDialog();

        try {
            if (result != null && !result.isEmpty()) {
                JSONObject jsonObject = new JSONObject(result);
                Gson gson = new Gson();

                Log.i("TAG", "TAG Result : " + result);

                switch (request) {
                    case getDoctorProfile:
                        List<HashMap<String, String>> professionalRotationList, scopeOfPracticeList, membershipList, educationList, hospitalAffiliationList, publicationsList, feedbackList;
                        professionalRotationList = new ArrayList<>();
                        scopeOfPracticeList = new ArrayList<>();
                        membershipList = new ArrayList<>();
                        educationList = new ArrayList<>();
                        hospitalAffiliationList = new ArrayList<>();
                        publicationsList = new ArrayList<>();
                        feedbackList = new ArrayList<>();

                        try {
                            JSONObject mainObj = new JSONObject(result);
                            if (mainObj.getString("error_code").equalsIgnoreCase("0")) {
                                if (mainObj.getJSONArray("result") != null && mainObj.getJSONArray("result").length() > 0) {
                                    JSONArray jsonArray = mainObj.getJSONArray("result");
                                    JSONObject object = jsonArray.getJSONObject(0);

                                    String speciality = "";

                                    JSONObject doctor = object.getJSONObject("doctor");

                                    // speciality
                                    JSONArray Speciality = doctor.getJSONArray("Speciality");
                                    for (int i = 0; i < Speciality.length(); i++) {
                                        JSONObject specialityObj = Speciality.getJSONObject(i);

                                        if (i == Speciality.length() - 1) {
                                            speciality += specialityObj.getString("Speciality");
                                        } else {
                                            speciality += specialityObj.getString("Speciality") + " and ";
                                        }
                                    }
                                    System.out.println("speciality:::" + speciality);

                                    // rating
                                    JSONObject Rating = doctor.getJSONObject("Rating");

                                    // scope of practice
                                    for (int i = 0; i < Speciality.length(); i++) {
                                        JSONObject specialityObj = Speciality.getJSONObject(i);

                                        HashMap<String, String> map = new HashMap<>();
                                        map.put("Speciality", specialityObj.getString("Speciality"));

                                        scopeOfPracticeList.add(map);
                                    }

                                    // education
                                    JSONArray Education = doctor.getJSONArray("Education");
                                    for (int i = 0; i < Education.length(); i++) {
                                        JSONObject educationObj = Education.getJSONObject(i);

                                        HashMap<String, String> map = new HashMap<>();
                                        map.put("Description", educationObj.getString("Description"));
                                        map.put("StartYear", educationObj.getString("StartYear"));
                                        map.put("EndYear", educationObj.getString("EndYear"));

                                        educationList.add(map);
                                    }

                                    // professional rotation
                                    JSONArray Professional_Rotation = doctor.getJSONArray("Experience");
                                    for (int i = 0; i < Professional_Rotation.length(); i++) {
                                        JSONObject professionalObj = Professional_Rotation.getJSONObject(i);

                                        HashMap<String, String> map = new HashMap<>();
                                        map.put("Description", professionalObj.getString("Description"));
                                        map.put("StartYear", professionalObj.getString("StartYear"));
                                        map.put("EndYear", professionalObj.getString("EndYear"));

                                        professionalRotationList.add(map);
                                    }

                                    // membership
                                    JSONArray Membership = doctor.getJSONArray("Membership");
                                    for (int i = 0; i < Membership.length(); i++) {
                                        JSONObject membershipObj = Membership.getJSONObject(i);

                                        HashMap<String, String> map = new HashMap<>();
                                        map.put("Description", membershipObj.getString("Description"));
                                        map.put("StartYear", membershipObj.getString("StartYear"));
                                        map.put("EndYear", membershipObj.getString("EndYear"));

                                        membershipList.add(map);
                                    }

                                    // hospital affiliation
                                    JSONArray Affiliation = doctor.getJSONArray("Affiliation");
                                    for (int i = 0; i < Affiliation.length(); i++) {
                                        JSONObject affiliationObj = Affiliation.getJSONObject(i);

                                        HashMap<String, String> map = new HashMap<>();
                                        map.put("Description", affiliationObj.getString("Description"));
                                        map.put("StartYear", affiliationObj.getString("StartYear"));
                                        map.put("EndYear", affiliationObj.getString("EndYear"));

                                        hospitalAffiliationList.add(map);
                                    }

                                    // publications
                                    JSONArray Publication = doctor.getJSONArray("Publication");
                                    for (int i = 0; i < Publication.length(); i++) {
                                        JSONObject publicationObj = Publication.getJSONObject(i);

                                        HashMap<String, String> map = new HashMap<>();
                                        map.put("Description", publicationObj.getString("Description"));
                                        map.put("StartYear", publicationObj.getString("StartYear"));
                                        map.put("EndYear", publicationObj.getString("EndYear"));

                                        publicationsList.add(map);
                                    }

                                    // feedback
                                    JSONArray Feedback = doctor.getJSONArray("Feedback");
                                    for (int i = 0; i < Feedback.length(); i++) {
                                        JSONObject feedbackObj = Feedback.getJSONObject(i);
                                        JSONObject userDetailObj = feedbackObj.getJSONObject("UserDetail");

                                        HashMap<String, String> map = new HashMap<>();
                                        map.put("name", userDetailObj.getString("FirstName") + " " + userDetailObj.getString("LastName"));
                                        //map.put("date", getFormatedTime(userDetailObj.getString("updated_at")));
                                        map.put("comment", feedbackObj.getString("Feedback"));

                                        feedbackList.add(map);
                                    }

                                    DoctorProfile mDoctorProfileData = new DoctorProfile();
                                    mDoctorProfileData.setDoctorId(doctor.getString("DoctorId"));
                                    mDoctorProfileData.setFirstName(doctor.getString("FirstName"));
                                    mDoctorProfileData.setLastName(doctor.getString("LastName"));
                                    mDoctorProfileData.setLocalCharge(doctor.getString("LocalCharge"));
                                    mDoctorProfileData.setSpeciality(doctor.getString("DoctorSpeciality"));
                                    mDoctorProfileData.setAbout_doctor(doctor.getString("Description"));
                                    mDoctorProfileData.setCosultCharge(doctor.getString("CosultCharge"));
                                    mDoctorProfileData.setCountry(doctor.getString("Country"));
                                    mDoctorProfileData.setResponseTime(doctor.getString("ResponseTime"));
                                    mDoctorProfileData.setProfilePic(doctor.getString("ProfilePic"));
                                    mDoctorProfileData.setIsSaved(doctor.getString("IsSaved"));
                                    mDoctorProfileData.setIsFree(doctor.getString("IsFree"));
                                    mDoctorProfileData.setDoctorType(doctor.getString("DoctorType"));
                                    //mDoctorProfileData.setSpeciality(speciality);
                                    mDoctorProfileData.setRating(Rating.getString("Rating"));
                                    mDoctorProfileData.setTotalRating(Rating.getString("TotalRating"));
                                    mDoctorProfileData.setScope_of_practice(scopeOfPracticeList);
                                    mDoctorProfileData.setEducation(educationList);
                                    mDoctorProfileData.setProfessional_rotation(professionalRotationList);
                                    mDoctorProfileData.setMembership(membershipList);
                                    mDoctorProfileData.setHospital_affiliation(hospitalAffiliationList);
                                    mDoctorProfileData.setPublications(publicationsList);
                                    mDoctorProfileData.setFeedback(feedbackList);

                                    MyConstants.doctorProfile = mDoctorProfileData;
                                    MyConstants.doctorLastName = mDoctorProfileData.getLastName();

                                    Intent i = new Intent(getActivity(), BookInPersonActivity.class);
                                    i.putExtra("DOCTOR_PROFILE", mDoctorProfileData);
                                    startActivity(i);

                                } else {
                                    Utils.showToast(mainObj.getString("error_string"), getActivity());
                                }
                            } else {
                                Utils.showToast(mainObj.getString("error_string"), getActivity());
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;
                }
            }
        }catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onProgressUpdate(String uniqueMessageId, int progres) {

    }

    @Override
    public void onProgressComplete(String uniqueMessageId, String result, Constant.REQUESTS request) {

    }


    @Override
    public void onRowClick(int pos, String message) {
        getDRProfileData(message);
    }

    @Override
    public void onRowClick(int pos) {

    }
}
