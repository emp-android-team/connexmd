package com.connex.md.patient.fragment;


import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.connex.md.R;
import com.connex.md.interfaces.AsyncTaskListner;
import com.connex.md.interfaces.DoctorListener;
import com.connex.md.others.App;
import com.connex.md.others.Internet;
import com.connex.md.patient.adapter.EditMedicalReportsAdapter;
import com.connex.md.utils.VerticalSpacingDecoration;
import com.connex.md.ws.CallRequest;
import com.connex.md.ws.Constant;
import com.connex.md.ws.MyConstants;
import com.connex.md.ws.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import static android.app.Activity.RESULT_OK;
import static com.connex.md.ws.Utils.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE;

/**
 * A simple {@link Fragment} subclass.
 */
public class MedicalReportsFragment extends Fragment implements AsyncTaskListner, DoctorListener {

    LinearLayout llUploadMedicalReport;
    RecyclerView rvMedicalReports;
    EditMedicalReportsAdapter editMedicalReportsAdapter;
    private static int RESULT_LOAD_IMG_DP = 1;
    private static int RESULT_CROP_DP = 3;
    String filePath = "";
    String filePathFirst = "";
    List<String> imagePath = new ArrayList<>();
    int pos;

    public MedicalReportsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_medical_reports, container, false);

        llUploadMedicalReport = view.findViewById(R.id.ll_upload_medical_report);
        rvMedicalReports = view.findViewById(R.id.rvMedicalReports);

        GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(), 3);
        gridLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rvMedicalReports.setLayoutManager(gridLayoutManager);

        rvMedicalReports.setHasFixedSize(true);
        rvMedicalReports.addItemDecoration(new VerticalSpacingDecoration(20));
        rvMedicalReports.setItemViewCacheSize(20);
        rvMedicalReports.setDrawingCacheEnabled(true);
        rvMedicalReports.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);

        editMedicalReportsAdapter = new EditMedicalReportsAdapter(MedicalReportsFragment.this, MyConstants.editPatientProfile.getReports());
        rvMedicalReports.setAdapter(editMedicalReportsAdapter);

        llUploadMedicalReport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Utils.checkPermission(getActivity())) {
                    loadImageFromGalleryForDp();
                }
            }
        });

        return view;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            loadImageFromGalleryForDp();
        }
    }

    public void loadImageFromGalleryForDp() {
        // Create intent to Open Image applications like Gallery, Google Photos
        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        // Start the Intent
        startActivityForResult(galleryIntent, RESULT_LOAD_IMG_DP);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        try {
            // When an Image is picked
            if (requestCode == RESULT_LOAD_IMG_DP && resultCode == RESULT_OK
                    && null != data) {
                // Get the Image from data

                Uri selectedImage = data.getData();
                String[] filePathColumn = {MediaStore.Images.Media.DATA};

                // Get the cursor
                Cursor cursor = getActivity().getContentResolver().query(selectedImage,
                        filePathColumn, null, null, null);
                // Move to first row
                int columnIndex = 0;
                if (cursor != null) {
                    cursor.moveToFirst();
                    columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    filePathFirst = cursor.getString(columnIndex);
                    System.out.println(filePathFirst);
                    cursor.close();

                    addReport(filePathFirst);
                }

                //doCropDP(filePathFirst);

            }

            if (requestCode == RESULT_CROP_DP) {
                if (resultCode == Activity.RESULT_OK) {
                    if (!TextUtils.isEmpty(filePath)) {

                        System.out.println("path***" + filePath);
                        Bitmap selectedBitmap = decodeSampledBitmapFromPath(filePath, 100, 100);
                        System.out.println("selectedBitmap:::::" + selectedBitmap);

                        // Set The Bitmap Data To ImageView
                        //ivPatient.setImageBitmap(selectedBitmap);
                        //ivPageDp.setScaleType(ImageView.ScaleType.FIT_XY);

                        addReport(filePath);

                       /* imagePath.add(filePath);
                        editMedicalReportsAdapter.notifyDataSetChanged();*/
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void addReport(String filePath) {

        if (!Internet.isAvailable(getActivity())) {
            Internet.showAlertDialog(getActivity(), "Error!", "No Internet Connection", false);

            return;
        }

        String urlStr = "userMedicalReport";

        Map<String, String> map = new HashMap<String, String>();
        map.put("url", MyConstants.BASE_URL + urlStr);
        map.put("ApiToken", MyConstants.API_TOKEN);
        map.put("Version", MyConstants.WS_VERSION);
        map.put("UserId", App.user.getUserID());
        map.put("Image", filePath);
        map.put("IsDelete", "0");
        map.put("DeleteId", "0");

        new CallRequest(MedicalReportsFragment.this).addReport(map);

    }

    public static Bitmap decodeSampledBitmapFromPath(String path, int reqWidth,
                                                     int reqHeight) {

        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, options);

        options.inSampleSize = calculateInSampleSize(options, reqWidth,
                reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        Bitmap bmp = BitmapFactory.decodeFile(path, options);
        return bmp;
    }

    public static int calculateInSampleSize(BitmapFactory.Options options,
                                            int reqWidth, int reqHeight) {

        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            if (width > height) {
                inSampleSize = Math.round((float) height / (float) reqHeight);
            } else {
                inSampleSize = Math.round((float) width / (float) reqWidth);
            }
        }
        return inSampleSize;
    }

    private void doCropDP(String picPath) {
        try {

            Intent cropIntent = new Intent("com.android.camera.action.CROP");

            File f = new File(picPath);
            Uri contentUri = Uri.fromFile(f);

            cropIntent.setDataAndType(contentUri, "image/*");

            cropIntent.putExtra("crop", "true");
            // indicate aspect of desired crop
            cropIntent.putExtra("aspectX", 1);
            cropIntent.putExtra("aspectY", 1);
            // indicate output X and Y
            cropIntent.putExtra("outputX", 256);
            cropIntent.putExtra("outputY", 256);
            // retrieve data on return
            cropIntent.putExtra("return-data", false);

            File sdCardDirectory = new File(Environment.getExternalStorageDirectory().getPath() + "/ConnexMd/reports");

            if (!sdCardDirectory.exists()) {
                sdCardDirectory.mkdirs();
            }

            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.US)
                    .format(new Date());

            String nw = "Report_" + timeStamp + ".jpeg";

            File image = new File(sdCardDirectory, nw);

            //uploadPathDp = sdCardDirectory + "/" + nw;
            filePath = new File(sdCardDirectory, nw).getAbsolutePath();
            System.out.println("UploadPath:::" + filePath);

            try {
                image.createNewFile();
            } catch (IOException ex) {
                Log.e("io", ex.getMessage());
            }

            Uri uri;
            /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                uri = FileProvider.getUriForFile(getActivity(),
                        BuildConfig.APPLICATION_ID + ".provider",
                        image);
            } else {*/
            uri = Uri.fromFile(image);
            //}

            cropIntent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
            // start the activity - we handle returning in onActivityResult
            startActivityForResult(cropIntent, RESULT_CROP_DP);
        } catch (ActivityNotFoundException anfe) {
            String errorMessage = "Your device doesn't support the crop action!";
            Toast toast = Toast.makeText(getActivity(), errorMessage, Toast.LENGTH_SHORT);
            toast.show();
        }
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        Utils.removeSimpleSpinProgressDialog();
        try {
            if (result != null && !result.isEmpty()) {
                Log.i("TAG", "TAG Result : " + result);
                switch (request) {
                    case addReport:
                        Utils.removeSimpleSpinProgressDialog();

                        try {
                            JSONObject mainObj = new JSONObject(result);
                            if (mainObj.getString("error_code").equalsIgnoreCase("0")) {
                                if (mainObj.getJSONArray("result") != null && mainObj.getJSONArray("result").length() > 0) {

                                    String id = mainObj.getJSONArray("result").getJSONObject(0).getString("id");
                                    String Report = mainObj.getJSONArray("result").getJSONObject(0).getString("Report");

                                    HashMap<String, String> map = new HashMap<>();
                                    map.put("id", id);
                                    map.put("Description", Report);

                                    MyConstants.editPatientProfile.getReports().add(map);
                                    editMedicalReportsAdapter.notifyDataSetChanged();
                                }

                            } else {
                                Utils.showToast(mainObj.getString("error_string"), getActivity());
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;

                    case deleteReport:
                        Utils.removeSimpleSpinProgressDialog();

                        try {
                            JSONObject mainObj = new JSONObject(result);
                            if (mainObj.getString("error_code").equalsIgnoreCase("0")) {

                                MyConstants.editPatientProfile.getReports().remove(pos);
                                editMedicalReportsAdapter.notifyDataSetChanged();

                            } else {
                                Utils.showToast(mainObj.getString("error_string"), getActivity());
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;
                }
            }
        } catch (Exception e) {
            Utils.removeSimpleSpinProgressDialog();
            e.printStackTrace();
            Utils.showToast("Please try again later", getActivity());
        }
    }

    @Override
    public void onProgressUpdate(String uniqueMessageId, int progres) {

    }

    @Override
    public void onProgressComplete(String uniqueMessageId, String result, Constant.REQUESTS request) {

    }

    @Override
    public void editDetails(int position, String imageName, String startYear, String endYear) {

        if (!Internet.isAvailable(getActivity())) {
            Internet.showAlertDialog(getActivity(), "Error!", "No Internet Connection", false);

            return;
        }

        pos = position;

        String urlStr = "userMedicalReport";

        Map<String, String> map = new HashMap<String, String>();
        map.put("url", MyConstants.BASE_URL + urlStr);
        map.put("ApiToken", MyConstants.API_TOKEN);
        map.put("Version", MyConstants.WS_VERSION);
        map.put("UserId", App.user.getUserID());
        map.put("Image", "0");
        map.put("IsDelete", "1");
        map.put("DeleteId", MyConstants.editPatientProfile.getReports().get(position).get("id"));

        new CallRequest(MedicalReportsFragment.this).deleteReport(map);
    }
}
