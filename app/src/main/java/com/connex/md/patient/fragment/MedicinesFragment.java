package com.connex.md.patient.fragment;


import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.connex.md.R;
import com.connex.md.interfaces.AsyncTaskListner;
import com.connex.md.interfaces.DoctorListener;
import com.connex.md.others.App;
import com.connex.md.others.Internet;
import com.connex.md.patient.adapter.EditMedicinesAdapter;
import com.connex.md.ws.CallRequest;
import com.connex.md.ws.Constant;
import com.connex.md.ws.MyConstants;
import com.connex.md.ws.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 */
public class MedicinesFragment extends Fragment implements AsyncTaskListner, DoctorListener {

    public Button btnAddMedicine;
    public RecyclerView rvMedicines;
    public EditMedicinesAdapter editMedicinesAdapter;
    private Dialog dialog;
    private ImageView ivClose;
    private Button btnSave;
    private EditText etMedicineName, etFrom, etTo;
    public String medicineName, startYear, endYear;
    private int position;
    private String editMedicineName, editStartYear, editEndYear;
    private TextView tvUniversityName;
    private LinearLayout llFromTo;

    public MedicinesFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_education, container, false);

        rvMedicines = view.findViewById(R.id.rvEducation);
        btnAddMedicine = view.findViewById(R.id.btnAddEducation);

        btnAddMedicine.setText("Add");

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        layoutManager.setAutoMeasureEnabled(true);
        rvMedicines.setLayoutManager(layoutManager);
        rvMedicines.setNestedScrollingEnabled(false);


        editMedicinesAdapter = new EditMedicinesAdapter(MedicinesFragment.this, MyConstants.editPatientProfile.getMedicines(), "Education");
        rvMedicines.setItemViewCacheSize(20);
        rvMedicines.setDrawingCacheEnabled(true);
        rvMedicines.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        rvMedicines.setHasFixedSize(false);

        rvMedicines.setAdapter(editMedicinesAdapter);

        btnAddMedicine.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openAddDialog();
            }
        });

        return view;
    }

    private void openAddDialog() {
        createDialog();
        initDialogComponents();

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                medicineName = etMedicineName.getText().toString().trim();

                if (TextUtils.isEmpty(medicineName)) {
                    etMedicineName.requestFocus();
                    etMedicineName.setError("Medicine name is empty");
                    return;
                }

                dialog.dismiss();

                saveDetails();
            }
        });

        ivClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
    }

    private void saveDetails() {

        if (!Internet.isAvailable(getActivity())) {
            Internet.showAlertDialog(getActivity(), "Error!", "No Internet Connection", false);

            return;
        }

        Map<String, String> map = new HashMap<String, String>();
        map.put("url", MyConstants.BASE_URL + "userMedicine");
        map.put("ApiToken", MyConstants.API_TOKEN);
        map.put("Version", MyConstants.WS_VERSION);
        map.put("UserId", App.user.getUserID());
        map.put("IsEdit", "0");
        map.put("EditId", "0");
        map.put("MedicineName", medicineName);

        new CallRequest(this).addMedicine(map);
    }

    private void createDialog() {
        dialog = new Dialog(getActivity(), R.style.CustomDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        // Include dialog.xml file
        dialog.setContentView(R.layout.dialog_edit_doctor);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
        dialog.show();
    }

    private void initDialogComponents() {
        btnSave = dialog.findViewById(R.id.btnSave);
        ivClose = dialog.findViewById(R.id.ivClose);
        etMedicineName = dialog.findViewById(R.id.etUniversityName);
        etFrom = dialog.findViewById(R.id.etFrom);
        etTo = dialog.findViewById(R.id.etTo);
        tvUniversityName = dialog.findViewById(R.id.tvUniversityName);
        llFromTo = dialog.findViewById(R.id.ll_from_to);

        tvUniversityName.setText("Add regularly taken medication here");
        etMedicineName.setHint("Medicine name");
        btnSave.setText("Add");
        llFromTo.setVisibility(View.GONE);
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        Utils.removeSimpleSpinProgressDialog();
        try {
            if (result != null && !result.isEmpty()) {
                Log.i("TAG", "TAG Result : " + result);
                //      {"document":{"response":{"status":1,"message":"Success."}}}
                switch (request) {
                    case addMedicine:
                        Utils.removeSimpleSpinProgressDialog();

                        try {
                            JSONObject mainObj = new JSONObject(result);
                            if (mainObj.getString("error_code").equalsIgnoreCase("0")) {

                                JSONObject resultObj = mainObj.getJSONArray("result").getJSONObject(0);

                                HashMap<String, String> map = new HashMap<>();
                                map.put("id", resultObj.getString("id"));
                                map.put("Description", medicineName);

                                MyConstants.editPatientProfile.getMedicines().add(map);

                                editMedicinesAdapter.notifyDataSetChanged();

                                Utils.showToast("Profile saved successfully", getActivity());
                            } else {
                                Utils.showToast(mainObj.getString("error_string"), getActivity());
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;

                    case updateMedicine:
                        Utils.removeSimpleSpinProgressDialog();

                        try {
                            JSONObject mainObj = new JSONObject(result);
                            if (mainObj.getString("error_code").equalsIgnoreCase("0")) {

                                MyConstants.editPatientProfile.getMedicines().get(position).put("Description", editMedicineName);

                                editMedicinesAdapter.notifyDataSetChanged();

                                Utils.showToast("Profile saved successfully", getActivity());
                            } else {
                                Utils.showToast(mainObj.getString("error_string"), getActivity());
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;
                }
            }
        } catch (Exception e) {
            Utils.removeSimpleSpinProgressDialog();
            e.printStackTrace();
            Utils.showToast("Please try again later", getActivity());
        }
    }

    @Override
    public void onProgressUpdate(String uniqueMessageId, int progres) {

    }

    @Override
    public void onProgressComplete(String uniqueMessageId, String result, Constant.REQUESTS request) {

    }

    @Override
    public void editDetails(int position, String medicineName, String startYear, String endYear) {

        if (!Internet.isAvailable(getActivity())) {
            Internet.showAlertDialog(getActivity(), "Error!", "No Internet Connection", false);

            return;
        }

        this.position = position;
        editMedicineName = medicineName;

        Map<String, String> map = new HashMap<String, String>();
        map.put("url", MyConstants.BASE_URL + "userMedicine");
        map.put("ApiToken", MyConstants.API_TOKEN);
        map.put("Version", MyConstants.WS_VERSION);
        map.put("UserId", App.user.getUserID());
        map.put("IsEdit", "1");
        map.put("MedicineName", editMedicineName);
        map.put("EditId", MyConstants.editPatientProfile.getMedicines().get(position).get("id"));

        new CallRequest(this).updateMedicine(map);
    }
}
