package com.connex.md.patient.fragment;


import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.connex.md.R;
import com.connex.md.interfaces.AsyncTaskListner;
import com.connex.md.others.App;
import com.connex.md.others.Internet;
import com.connex.md.ws.CallRequest;
import com.connex.md.ws.Constant;
import com.connex.md.ws.MyConstants;
import com.connex.md.ws.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import co.lujun.androidtagview.TagContainerLayout;
import co.lujun.androidtagview.TagView;

/**
 * A simple {@link Fragment} subclass.
 */
public class MedicinesNewFragment extends Fragment implements AsyncTaskListner {


    Button btnAdd;
    EditText etMedicines;
    TagContainerLayout tags;
    List<String> medicines = new ArrayList<>();
    String medicine;
    int pos;

    public MedicinesNewFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_allergies, container, false);

        btnAdd = view.findViewById(R.id.btnAdd);
        etMedicines = view.findViewById(R.id.etAllergies);
        tags = view.findViewById(R.id.tags);
        
        etMedicines.setHint("Add regularly taken medication here");

        medicines.clear();
        if (MyConstants.editPatientProfile.getMedicines().size() > 0) {

            for (int i = 0; i < MyConstants.editPatientProfile.getMedicines().size(); i++) {
                medicines.add(MyConstants.editPatientProfile.getMedicines().get(i).get("Description"));
            }
            tags.setTags(medicines);

        }

        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                medicine = etMedicines.getText().toString().trim();

                if (TextUtils.isEmpty(etMedicines.getText().toString().trim())) {
                    return;
                }

                addMedicine(medicine);
            }
        });

        tags.setOnTagClickListener(new TagView.OnTagClickListener() {
            @Override
            public void onTagClick(int position, String text) {

            }

            @Override
            public void onTagLongClick(int position, String text) {

            }

            @Override
            public void onTagCrossClick(int position) {
                pos = position;
                new AlertDialog.Builder(getActivity())
                        .setTitle("Alert!")
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setMessage("Would you like to delete Medicine?")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                deleteMedicine(pos);
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })
                        .show();
            }
        });

        return view;
    }

    private void deleteMedicine(int position) {
        if (!Internet.isAvailable(getActivity())) {
            Internet.showAlertDialog(getActivity(), "Error!", "No Internet Connection", false);

            return;
        }

        String urlStr = "userMedicine";

        Map<String, String> map = new HashMap<String, String>();
        map.put("url", MyConstants.BASE_URL + urlStr);
        map.put("ApiToken", MyConstants.API_TOKEN);
        map.put("Version", MyConstants.WS_VERSION);
        map.put("UserId", App.user.getUserID());
        map.put("MedicineName", MyConstants.editPatientProfile.getMedicines().get(position).get("Description"));
        map.put("IsEdit", "2");
        map.put("EditId", MyConstants.editPatientProfile.getMedicines().get(position).get("id"));

        new CallRequest(MedicinesNewFragment.this).updateMedicine(map);
    }

    private void addMedicine(String medicine) {
        if (!Internet.isAvailable(getActivity())) {
            Internet.showAlertDialog(getActivity(), "Error!", "No Internet Connection", false);

            return;
        }

        String urlStr = "userMedicine";

        Map<String, String> map = new HashMap<String, String>();
        map.put("url", MyConstants.BASE_URL + urlStr);
        map.put("ApiToken", MyConstants.API_TOKEN);
        map.put("Version", MyConstants.WS_VERSION);
        map.put("UserId", App.user.getUserID());
        map.put("MedicineName", medicine);
        map.put("IsEdit", "0");
        map.put("EditId", "0");

        new CallRequest(MedicinesNewFragment.this).addMedicine(map);
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        Utils.removeSimpleSpinProgressDialog();
        try {
            if (result != null && !result.isEmpty()) {
                Log.i("TAG", "TAG Result : " + result);
                switch (request) {
                    case addMedicine:
                        Utils.removeSimpleSpinProgressDialog();

                        try {
                            JSONObject mainObj = new JSONObject(result);
                            if (mainObj.getString("error_code").equalsIgnoreCase("0")) {
                                if (mainObj.getJSONArray("result") != null && mainObj.getJSONArray("result").length() > 0) {
                                    String id = mainObj.getJSONArray("result").getJSONObject(0).getString("id");

                                    HashMap<String, String> map = new HashMap<>();
                                    map.put("id", id);
                                    map.put("Description", medicine);

                                    MyConstants.editPatientProfile.getMedicines().add(map);
                                    medicines.clear();
                                    for (int i = 0; i < MyConstants.editPatientProfile.getMedicines().size(); i++) {
                                        medicines.add(MyConstants.editPatientProfile.getMedicines().get(i).get("Description"));
                                    }
                                    tags.setTags(medicines);
                                    etMedicines.setText("");
                                }

                            } else {
                                Utils.showToast(mainObj.getString("error_string"), getActivity());
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;

                    case updateMedicine:
                        Utils.removeSimpleSpinProgressDialog();

                        try {
                            JSONObject mainObj = new JSONObject(result);
                            if (mainObj.getString("error_code").equalsIgnoreCase("0")) {

                                MyConstants.editPatientProfile.getMedicines().remove(pos);
                                medicines.clear();
                                for (int i = 0; i < MyConstants.editPatientProfile.getMedicines().size(); i++) {
                                    medicines.add(MyConstants.editPatientProfile.getMedicines().get(i).get("Description"));
                                }
                                tags.setTags(medicines);
                                etMedicines.setText("");

                            } else {
                                Utils.showToast(mainObj.getString("error_string"), getActivity());
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;
                }
            }
        } catch (Exception e) {
            Utils.removeSimpleSpinProgressDialog();
            e.printStackTrace();
            Utils.showToast("Please try again later", getActivity());
        }
    }

    @Override
    public void onProgressUpdate(String uniqueMessageId, int progres) {

    }

    @Override
    public void onProgressComplete(String uniqueMessageId, String result, Constant.REQUESTS
            request) {

    }
}
