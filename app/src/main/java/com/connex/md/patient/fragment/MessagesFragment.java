package com.connex.md.patient.fragment;


import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.connex.md.R;
import com.connex.md.custom_views.CenteredToolbar;
import com.connex.md.firebase_chat.model.FireMessage;
import com.connex.md.firebase_chat.others.ChatConstants;
import com.connex.md.interfaces.AsyncTaskListner;
import com.connex.md.model.MessageHistory;
import com.connex.md.others.App;
import com.connex.md.others.Internet;
import com.connex.md.patient.activity.PTDashboardActivity;
import com.connex.md.patient.adapter.MessagesAdapter;
import com.connex.md.utils.DividerItemDecoration;
import com.connex.md.ws.CallRequest;
import com.connex.md.ws.Constant;
import com.connex.md.ws.MyConstants;
import com.connex.md.ws.Utils;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.onesignal.OneSignal;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

/**
 * A simple {@link Fragment} subclass.
 */
public class MessagesFragment extends Fragment implements AsyncTaskListner {

    public RecyclerView rvMessages;
    public ImageView ivBack1;
    public MessagesAdapter messagesAdapter;
    public List<MessageHistory> messageHistoryList = new ArrayList<>();
    public DatabaseReference fireDB, fireChannel;
    public int totalCounter = 0;
    public ArrayList<FireMessage> chatArray = new ArrayList<>();
    ImageView ivNoMessages, ivSearch;
    String[] lastMessage = new String[1];
    String[] lastDate = new String[1];
    String[] counter = new String[1];
    MenuItem item;
    SearchView searchView;
    private SwipeRefreshLayout swipe_container;
    private Menu menu;

    public MessagesFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_messages, container, false);

        CenteredToolbar toolbar = view.findViewById(R.id.toolbar);
        if (getActivity() != null) {
            ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
            ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Messages");
            ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);

            /*if (((AppCompatActivity) getActivity()).getSupportActionBar() != null) {
                ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayShowTitleEnabled(false);
            }*/
        }

        OneSignal.clearOneSignalNotifications();

        SharedPreferences.Editor editor = PTDashboardActivity.preferences.edit();
        editor.putString(MyConstants.BADGE_COUNT, "0");
        editor.commit();
        PTDashboardActivity.setBadgeCount();

        rvMessages = view.findViewById(R.id.rvMessages);
        //ivBack1 = toolbar.findViewById(R.id.ivBack1);
        //ivSearch = view.findViewById(R.id.ivSearch);
        ivNoMessages = view.findViewById(R.id.iv_no_messages);
        swipe_container = view.findViewById(R.id.swipe_container);

        fireDB = FirebaseDatabase.getInstance().getReference().child(MyConstants.CHANNEL);

        swipe_container.setColorScheme(android.R.color.holo_blue_dark,
                android.R.color.holo_blue_light,
                android.R.color.holo_green_dark,
                android.R.color.holo_green_light);

        swipe_container.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (App.user.getUser_Type().equalsIgnoreCase(MyConstants.USER_PT)) {
                    getMessageHistory();
                }
            }
        });

        setData();

        if (App.user.getUser_Type().equalsIgnoreCase(MyConstants.USER_PT) && MyConstants.isMessageHistoryLoad) {
            getMessageHistory();
            MyConstants.isMessageHistoryLoad = false;
        }
        /*else {
            getDoctorMessageHistory();
        }*/

        setHasOptionsMenu(true);

        return view;
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        item = menu.findItem(R.id.search);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        //super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.main_messages, menu);
        this.menu = menu; // init the variable
        searchView = (SearchView) MenuItemCompat.getActionView(menu.findItem(R.id.search));
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
//                if (messageHistoryList.size()>0) {
                if (TextUtils.isEmpty(newText)) {
                    messagesAdapter.filters("");
                    //rvMessages.clearTextFilter();
                } else {
                    messagesAdapter.filters(newText);
                }
                //}
                return true;
            }
        });
        super.onCreateOptionsMenu(menu, inflater);
    }

    private void getDoctorMessageHistory() {

        if (!Internet.isAvailable(getActivity())) {
            Internet.showAlertDialog(getActivity(), "Error!", "No Internet Connection", false);
            return;
        }

        Map<String, String> map = new HashMap<String, String>();
        map.put("url", MyConstants.DOCTOR_BASE_URL + "messageHistory");
        map.put("ApiToken", MyConstants.API_TOKEN);
        map.put("Version", MyConstants.WS_VERSION);
        map.put("DoctorId", App.user.getUserID());

        new CallRequest(MessagesFragment.this).getChatHistory(map);
    }

    private void getMessageHistory() {
        if (!Internet.isAvailable(getActivity())) {
            Internet.showAlertDialog(getActivity(), "Error!", "No Internet Connection", false);
            return;
        }

        String urlStr = "messageHistory";

        Map<String, String> map = new HashMap<String, String>();
        if (MyConstants.isGuest.equalsIgnoreCase("1")) {
            map.put("url", MyConstants.GUEST_BASE_URL + "messageHistory");
            map.put("GuestId", App.user.getUserID());
        } else {
            map.put("url", MyConstants.BASE_URL + urlStr);
            map.put("UserId", App.user.getUserID());
        }
        map.put("ApiToken", MyConstants.API_TOKEN);
        map.put("Version", MyConstants.WS_VERSION);

        new CallRequest(MessagesFragment.this).getChatHistory(map);
    }


    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        Utils.removeSimpleSpinProgressDialog();
        try {
            if (result != null && !result.isEmpty()) {
                Log.i("TAG", "TAG Result : " + result);
                //      {"document":{"response":{"status":1,"message":"Success."}}}
                switch (request) {
                    case chatHistory:
                        Utils.removeSimpleSpinProgressDialog();
                        try {
                            JSONObject mainObj = new JSONObject(result);
                            if (mainObj.getString("error_code").equalsIgnoreCase("0")) {
                                messageHistoryList.clear();
                                MyConstants.messageHistoryList.clear();
                                if (mainObj.getJSONArray("result") != null && mainObj.getJSONArray("result").length() > 0) {
                                    JSONArray jsonArray = mainObj.getJSONArray("result");

                                    String keys = "";
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject jsonObject = jsonArray.getJSONObject(i);

                                        final MessageHistory messageHistory = new MessageHistory();
                                        messageHistory.setDoctorId(jsonObject.getString("DoctorId"));
                                        messageHistory.setBookingId(jsonObject.getString("UniqueId"));
                                        messageHistory.setUserId(jsonObject.getString("UserId"));
                                        messageHistory.setConsultTime(jsonObject.getString("ConsultTime"));
                                        messageHistory.setFirstName(jsonObject.getString("FirstName"));
                                        messageHistory.setLastName(jsonObject.getString("LastName"));
                                        if (App.user.getUser_Type().equalsIgnoreCase(MyConstants.USER_DR)) {
                                            messageHistory.setIsGuest(jsonObject.getString("IsGuest"));
                                        }
                                        messageHistory.setProfilePic(jsonObject.getString("ProfilePic"));

                                        String receiverID;

                                        if (App.user.getUser_Type().equalsIgnoreCase(MyConstants.USER_PT)) {
                                            receiverID = jsonObject.getString("DoctorId");
                                        } else {
                                            receiverID = jsonObject.getString("UserId");
                                        }

                                        System.out.println("last message**" + lastMessage[0]);
                                        messageHistory.setTime(lastDate[0]);
                                        messageHistory.setLastMessage(lastMessage[0]);
                                        messageHistory.setCounter(counter[0]);

                                        messageHistoryList.add(messageHistory);

                                    }

                                    MyConstants.messageHistoryList = messageHistoryList;

                                    setData();

                                    swipe_container.setRefreshing(false);

                                    if (MyConstants.messageHistoryList.size() > 0) {
                                        ivNoMessages.setVisibility(View.GONE);
                                        rvMessages.setVisibility(View.VISIBLE);
                                    } else {
                                        ivNoMessages.setVisibility(View.VISIBLE);
                                        rvMessages.setVisibility(View.GONE);
                                    }

                                } else {
                                    Utils.showToast(mainObj.getString("error_string"), getActivity());
                                }
                            } else if (mainObj.getString("error_code").equalsIgnoreCase("5")) {
                                messageHistoryList.clear();
                                System.out.println("size:::" + messageHistoryList.size());
                                if (messageHistoryList.size() > 0) {
                                    ivNoMessages.setVisibility(View.GONE);
                                    rvMessages.setVisibility(View.VISIBLE);
                                    item.setVisible(true);
                                } else {
                                    ivNoMessages.setVisibility(View.VISIBLE);
                                    rvMessages.setVisibility(View.GONE);
                                    item.setVisible(false);
                                }
                            } else {
                                Utils.showToast(mainObj.getString("error_string"), getActivity());
                            }

                            swipe_container.setRefreshing(false);
                        } catch (JSONException e) {
                            e.printStackTrace();
                            swipe_container.setRefreshing(false);
                        }
                        break;
                }
            }
        } catch (Exception e) {
            Utils.removeSimpleSpinProgressDialog();
            e.printStackTrace();
            Utils.showToast("Please try again later", getActivity());
        }
    }

    private void setData() {

        if (MyConstants.messageHistoryList.size() > 0) {
            ivNoMessages.setVisibility(View.GONE);
            rvMessages.setVisibility(View.VISIBLE);
        } else {
            ivNoMessages.setVisibility(View.VISIBLE);
            rvMessages.setVisibility(View.GONE);
        }

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rvMessages.setLayoutManager(layoutManager);

        LayoutAnimationController controller = AnimationUtils.loadLayoutAnimation(rvMessages.getContext(), R.anim.layout_animation_fall_down);
        rvMessages.setLayoutAnimation(controller);
        rvMessages.scheduleLayoutAnimation();

        getUpdatedDB();

    }

    @Override
    public void onProgressUpdate(String uniqueMessageId, int progres) {

    }

    @Override
    public void onProgressComplete(String uniqueMessageId, String result, Constant.REQUESTS request) {

    }

    @Override
    public void onPause() {
        super.onPause();
        if (searchView != null) {
            if (!searchView.isIconified()) {
                searchView.setIconified(true);
                searchView.clearFocus();

                if (menu != null) {
                    (menu.findItem(R.id.search)).collapseActionView();
                }
            }
        }
    }

    public void getUpdatedDB() {

        try {

            for (int i = 0; i < MyConstants.messageHistoryList.size(); i++) {
                new ValueEventsListner().setListner(i);
            }

            messagesAdapter = new MessagesAdapter(getActivity(), MyConstants.messageHistoryList);
            rvMessages.setHasFixedSize(true);
            //rvMessages.addItemDecoration(new VerticalSpacingDecoration(20));
            rvMessages.addItemDecoration(new DividerItemDecoration(getActivity(), R.drawable.divider));
            rvMessages.setItemViewCacheSize(20);
            rvMessages.setDrawingCacheEnabled(true);
            rvMessages.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);

            rvMessages.setAdapter(messagesAdapter);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public class ValueEventsListner {
        public DatabaseReference dbRef;

        public void setListner(final int pos) {

            try {

                dbRef = fireDB.child(MyConstants.messageHistoryList.get(pos).BookingId);
                dbRef.child("last_message").addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        try {
                            if (dataSnapshot != null) {
                                System.out.println("datasnapshot::" + dataSnapshot.toString());
                                String last_message = dataSnapshot.getValue(String.class);
                                System.out.println("last message" + last_message);
                                //Log.i("Last Messages : Log : ", last_message);
                                MyConstants.messageHistoryList.get(pos).lastMessage = last_message;
                                messagesAdapter.notifyDataSetChanged();
                            }
                        } catch (Exception e){
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

                dbRef.child("last_message").addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        try {
                            if (dataSnapshot != null) {
                                String last_message = dataSnapshot.getValue(String.class);
                                System.out.println("last message" + last_message);
                                //Log.i("Last Messages : Log : ", last_message);
                                MyConstants.messageHistoryList.get(pos).lastMessage = last_message;
                                messagesAdapter.notifyDataSetChanged();
                            }
                        } catch (Exception e){
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

                dbRef.child("last_date").addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        try {
                            String time = "";
                            Date inputDate;
                            if (dataSnapshot != null) {
                                String last_date = dataSnapshot.getValue(String.class);
                                System.out.println("last date::" + last_date);
                                //Log.i("Last date : Log : ", last_date);

                                // "2017-10-23T15:48:04.GMT"

                                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm a", Locale.US);
                                SimpleDateFormat df = new SimpleDateFormat("yyyy/MM/dd h:mm a", Locale.getDefault());

                                if (last_date != null) {
                                    try {
                                        format.setTimeZone(TimeZone.getTimeZone("UTC"));
                                        inputDate = format.parse(last_date);
                                        df.setTimeZone(TimeZone.getDefault());
                                        time = df.format(inputDate);
                                        System.out.println("time::" + time);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                            MyConstants.messageHistoryList.get(pos).time = time;
                            messagesAdapter.notifyDataSetChanged();
                        } catch (Exception e){
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

                dbRef.child("last_date").addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        try {
                            String time = "";
                            Date inputDate;
                            if (dataSnapshot != null) {
                                String last_date = dataSnapshot.getValue(String.class);
                                System.out.println("last date::" + last_date);
                                //Log.i("Last date : Log : ", last_date);

                                // "2017-10-23T15:48:04.GMT"

                                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm a", Locale.US);
                                SimpleDateFormat df = new SimpleDateFormat("yyyy/MM/dd h:mm a", Locale.getDefault());
                                if (last_date != null) {
                                    try {
                                        format.setTimeZone(TimeZone.getTimeZone("UTC"));
                                        inputDate = format.parse(last_date);
                                        df.setTimeZone(TimeZone.getDefault());
                                        time = df.format(inputDate);
                                        System.out.println("time::" + time);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                            MyConstants.messageHistoryList.get(pos).time = time;
                            messagesAdapter.notifyDataSetChanged();
                        } catch (Exception e){
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

                String receiverID = "";
                if (App.user.getUser_Type().equalsIgnoreCase(MyConstants.USER_PT)) {
                    receiverID = MyConstants.messageHistoryList.get(pos).getDoctorId();
                } else {
                    receiverID = MyConstants.messageHistoryList.get(pos).getUserId();
                }

                dbRef.child(ChatConstants.FIRE_UNREAD_COUNTER).child(receiverID)
                        .addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                if (dataSnapshot != null) {

                                    try {
                                        String count = String.valueOf(dataSnapshot.getValue(Integer.class));

                                        System.out.println("count::" + count);

                                        if (count.equalsIgnoreCase("null") || count == null || TextUtils.isEmpty(count)) {
                                            MyConstants.messageHistoryList.get(pos).counter = "0";
                                        } else {
                                            MyConstants.messageHistoryList.get(pos).counter = count;
                                        }

                                        messagesAdapter.notifyDataSetChanged();
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {

                            }
                        });

                dbRef.child(ChatConstants.FIRE_UNREAD_COUNTER).child(receiverID).
                        addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                if (dataSnapshot != null) {

                                    try {
                                        String count = String.valueOf(dataSnapshot.getValue(Integer.class));
                                        System.out.println("count:: in change : " + count);

                                        if (count.equalsIgnoreCase("null") || count == null || TextUtils.isEmpty(count)) {
                                            MyConstants.messageHistoryList.get(pos).counter = "0";
                                        } else {
                                            MyConstants.messageHistoryList.get(pos).counter = count;
                                        }

                                        messagesAdapter.notifyDataSetChanged();
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {

                            }
                        });


            } catch (Exception e){
                e.printStackTrace();
            }
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                getActivity().onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
