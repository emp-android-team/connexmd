package com.connex.md.patient.fragment;


import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.connex.md.interfaces.AsyncTaskListner;
import com.connex.md.model.UserProfile;
import com.connex.md.others.Internet;
import com.connex.md.others.PicassoTrustAll;
import com.connex.md.R;
import com.connex.md.others.App;
import com.connex.md.ws.CallRequest;
import com.connex.md.ws.Constant;
import com.connex.md.ws.MyConstants;
import com.connex.md.ws.Utils;
import com.squareup.picasso.Callback;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import static android.app.Activity.RESULT_OK;
import static com.connex.md.ws.Utils.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE;

/**
 * A simple {@link Fragment} subclass.
 */
public class PTProfileFragment extends Fragment implements AsyncTaskListner {

    LinearLayout ivBack;
    ImageView ivUser;
    TextView tvName, tvAppointments, tvSavedDoctors, tvFeedbackGiven;
    //ImageView ivEditUp;
    //ImageView ivEditDown;
    EditText etEmail, etPhone;
    //EditText etGender;
    EditText etDOB, etSmoker, etAllergies, etMedications;
    UserProfile userProfile;
    public AQuery aqNewIMG;
    TextView tvSave, tvEdit;
    String filePath = "";
    String filePathFirst = "";
    boolean withImage = false;
    private static int RESULT_LOAD_IMG_DP = 1;
    private static int RESULT_CROP_DP = 3;
    DatePickerDialog DialogDOB;
    private SimpleDateFormat dateFormatter;
    String dateofbirth;
    SharedPreferences sharedpreferences;
    Spinner spinner;
    String gender;
    final String[] items = new String[]{"Male", "Female"};

    public PTProfileFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_pt_profile, container, false);

        ivBack = view.findViewById(R.id.ivBack);
        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();
            }
        });

        tvSave = view.findViewById(R.id.tvSave);
        tvEdit = view.findViewById(R.id.tvEdit);
        ivUser = view.findViewById(R.id.ivUser);
        tvName = view.findViewById(R.id.tvName);
        tvAppointments = view.findViewById(R.id.tvAppointments);
        tvSavedDoctors = view.findViewById(R.id.tvSavedDoctors);
        tvFeedbackGiven = view.findViewById(R.id.tvFeedbackGiven);
        //ivEditUp = view.findViewById(R.id.ivEditUp);
        //ivEditDown = view.findViewById(R.id.ivEditDown);
        etEmail = view.findViewById(R.id.etEmail);
        etPhone = view.findViewById(R.id.etPhone);
        //etGender = view.findViewById(R.id.etGender);
        etDOB = view.findViewById(R.id.etDOB);
        etSmoker = view.findViewById(R.id.etSmoker);
        etAllergies = view.findViewById(R.id.etAllergies);
        etMedications = view.findViewById(R.id.etMedications);
        spinner = view.findViewById(R.id.spinner);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, items);
        spinner.setAdapter(adapter);

        etEmail.setFocusable(false);
        etPhone.setFocusable(false);

        //etGender.setFocusable(false);
        etDOB.setFocusable(false);
        etSmoker.setFocusable(false);
        etAllergies.setFocusable(false);
        etMedications.setFocusable(false);

        etEmail.setEnabled(false);
        etPhone.setEnabled(false);

        //etGender.setEnabled(false);
        spinner.setEnabled(false);
        etDOB.setEnabled(false);
        etSmoker.setEnabled(false);
        etAllergies.setEnabled(false);
        etMedications.setEnabled(false);
        ivUser.setEnabled(false);

        spinner.setClickable(false);

        etEmail.setCursorVisible(false);
        etPhone.setCursorVisible(false);

        //etGender.setCursorVisible(false);
        etDOB.setCursorVisible(false);
        etSmoker.setCursorVisible(false);
        etAllergies.setCursorVisible(false);
        etMedications.setCursorVisible(false);

        sharedpreferences = getActivity().getSharedPreferences(MyConstants.PREF, Context.MODE_PRIVATE);

        getProfileData();

        /*ivEditUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                etEmail.setFocusableInTouchMode(true);
                etPhone.setFocusableInTouchMode(true);
            }
        });*/

        dateFormatter = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        Calendar newCalendar = Calendar.getInstance();
        DialogDOB = new DatePickerDialog(getActivity(), R.style.DialogTheme, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                etDOB.setText(dateFormatter.format(newDate.getTime()));
                dateofbirth = dateFormatter.format(newDate.getTime());
                System.out.println("date of birth" + dateofbirth);
            }

        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));

        DialogDOB.getDatePicker().setMaxDate(System.currentTimeMillis());
        etDOB.setInputType(InputType.TYPE_NULL);
        etDOB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                etDOB.requestFocus();
                DialogDOB.show();
            }
        });

        tvSave.setVisibility(View.GONE);

        tvEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //etGender.setFocusableInTouchMode(true);
                //etDOB.setFocusableInTouchMode(true);

                tvEdit.setVisibility(View.GONE);
                tvSave.setVisibility(View.VISIBLE);

                if (TextUtils.isEmpty(etPhone.getText().toString().trim())) {
                    etPhone.setFocusableInTouchMode(true);
                    etPhone.setEnabled(true);
                    etPhone.setCursorVisible(true);
                }

                etSmoker.setFocusableInTouchMode(true);
                etAllergies.setFocusableInTouchMode(true);
                etMedications.setFocusableInTouchMode(true);

                //etGender.setEnabled(true);
                spinner.setEnabled(true);
                spinner.setClickable(true);
                etDOB.setEnabled(true);
                etSmoker.setEnabled(true);
                etAllergies.setEnabled(true);
                etMedications.setEnabled(true);
                ivUser.setEnabled(true);

                //etGender.setCursorVisible(true);
                etDOB.setCursorVisible(true);
                etSmoker.setCursorVisible(true);
                etAllergies.setCursorVisible(true);
                etMedications.setCursorVisible(true);


            }
        });

        /*tvEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), PTEditProfileActivity.class);
                startActivity(intent);
            }
        });*/

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                gender = items[i];
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                gender = "";
            }
        });

        ivUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(Utils.checkPermission(getActivity())) {
                    loadImageFromGalleryForDp();
                }
            }
        });

        tvSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                String email = etEmail.getText().toString().trim();
                String phone = etPhone.getText().toString().trim();

                //gender = etGender.getText().toString().trim();
                String dob = etDOB.getText().toString().trim();
                String smoker = etSmoker.getText().toString().trim();
                String allergies = etAllergies.getText().toString().trim();
                String medications = etMedications.getText().toString().trim();

                if (dob.equalsIgnoreCase("0000-00-00")) {
                    dob = "";
                }

                /*if (TextUtils.isEmpty(email)) {
                    etEmail.setError("Email is empty");
                    etEmail.requestFocus();
                } else if (TextUtils.isEmpty(phone)) {
                    etPhone.setError("Phone number is empty");
                    etPhone.requestFocus();
                } else if (!Utils.isValidEmail(email)) {
                    etEmail.setError("Email is not valid");
                    etEmail.requestFocus();
                } else if (phone.length() != 10) {
                    etPhone.setError("Phone number is not valid");
                    etPhone.requestFocus();
                } else {
                    updateProfile(gender, dob, smoker, allergies, medications);
                }*/
                updateProfile(phone, gender, dob, smoker, allergies, medications);
            }
        });

        return view;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            loadImageFromGalleryForDp();
        }
    }

    public void loadImageFromGalleryForDp() {
        // Create intent to Open Image applications like Gallery, Google Photos
        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        // Start the Intent
        startActivityForResult(galleryIntent, RESULT_LOAD_IMG_DP);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        try {
            // When an Image is picked
            if (requestCode == RESULT_LOAD_IMG_DP && resultCode == RESULT_OK
                    && null != data) {
                // Get the Image from data

                Uri selectedImage = data.getData();
                String[] filePathColumn = {MediaStore.Images.Media.DATA};

                // Get the cursor
                Cursor cursor = getActivity().getContentResolver().query(selectedImage,
                        filePathColumn, null, null, null);
                // Move to first row
                int columnIndex = 0;
                if (cursor != null) {
                    cursor.moveToFirst();
                    columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    filePathFirst = cursor.getString(columnIndex);
                    System.out.println(filePathFirst);
                    cursor.close();


                    /*ivUser.setImageBitmap(BitmapFactory
                            .decodeFile(filePath));*/
                }

                doCropDP(filePathFirst);

            }

            if (requestCode == RESULT_CROP_DP) {
                if (resultCode == Activity.RESULT_OK) {
                    if (!TextUtils.isEmpty(filePath)) {

                        System.out.println("path***" + filePath);
                        Bitmap selectedBitmap = decodeSampledBitmapFromPath(filePath, 100, 100);
                        System.out.println("selectedBitmap:::::" + selectedBitmap);

                        // Set The Bitmap Data To ImageView
                        ivUser.setImageBitmap(selectedBitmap);
                        //ivPageDp.setScaleType(ImageView.ScaleType.FIT_XY);

                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static Bitmap decodeSampledBitmapFromPath(String path, int reqWidth,
                                                     int reqHeight) {

        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, options);

        options.inSampleSize = calculateInSampleSize(options, reqWidth,
                reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        Bitmap bmp = BitmapFactory.decodeFile(path, options);
        return bmp;
    }

    public static int calculateInSampleSize(BitmapFactory.Options options,
                                            int reqWidth, int reqHeight) {

        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            if (width > height) {
                inSampleSize = Math.round((float) height / (float) reqHeight);
            } else {
                inSampleSize = Math.round((float) width / (float) reqWidth);
            }
        }
        return inSampleSize;
    }

    private void doCropDP(String picPath) {
        try {

            Intent cropIntent = new Intent("com.android.camera.action.CROP");

            File f = new File(picPath);
            Uri contentUri = Uri.fromFile(f);

            cropIntent.setDataAndType(contentUri, "image/*");

            cropIntent.putExtra("crop", "true");
            // indicate aspect of desired crop
            cropIntent.putExtra("aspectX", 1);
            cropIntent.putExtra("aspectY", 1);
            // indicate output X and Y
            cropIntent.putExtra("outputX", 256);
            cropIntent.putExtra("outputY", 256);
            // retrieve data on return
            cropIntent.putExtra("return-data", false);

            File sdCardDirectory = new File(Environment.getExternalStorageDirectory().getPath() + "/ConnexMd/profile_pictures");

            if (!sdCardDirectory.exists()) {
                sdCardDirectory.mkdirs();
            }

            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.US)
                    .format(new Date());

            String nw = "Profile_" + timeStamp + ".jpeg";

            File image = new File(sdCardDirectory, nw);

            //uploadPathDp = sdCardDirectory + "/" + nw;
            filePath = new File(sdCardDirectory, nw).getAbsolutePath();
            System.out.println("UploadPath:::" + filePath);

            try {
                image.createNewFile();
            } catch (IOException ex) {
                Log.e("io", ex.getMessage());
            }

            Uri uri;
            /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                uri = FileProvider.getUriForFile(getActivity(),
                        BuildConfig.APPLICATION_ID + ".provider",
                        image);
            } else {*/
            uri = Uri.fromFile(image);
            //}

            cropIntent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
            // start the activity - we handle returning in onActivityResult
            startActivityForResult(cropIntent, RESULT_CROP_DP);
        } catch (ActivityNotFoundException anfe) {
            String errorMessage = "your device doesn't support the crop action!";
            Toast toast = Toast.makeText(getActivity(), errorMessage, Toast.LENGTH_SHORT);
            toast.show();
        }
    }

    private void updateProfile(String phone, String gender, String dob, String smoker, String allergies, String medications) {
        if (!Internet.isAvailable(getActivity())) {
            Internet.showAlertDialog(getActivity(), "Error!", "No Internet Connection", false);

            return;
        }

        String urlStr = "userProfileUpdate";

        Map<String, String> map = new HashMap<String, String>();
        map.put("url", MyConstants.BASE_URL + urlStr);
        map.put("ApiToken", MyConstants.API_TOKEN);
        map.put("Version", MyConstants.WS_VERSION);
        map.put("UserId", App.user.getUserID());
        map.put("FirstName", App.user.getFirstName());
        map.put("LastName", App.user.getLastName());
        map.put("SocialId", sharedpreferences.getString(MyConstants.SOCIAL_ID, "0"));
        map.put("Phone", phone);
        map.put("BirthDate", dob);
        map.put("Gender", gender);
        map.put("Smoke", smoker);
        map.put("Allergies", allergies);
        map.put("Medications", medications);
        map.put("SocialType", App.user.getLoginType());
        if (!TextUtils.isEmpty(filePath)) {
            withImage = true;
            map.put("ProfilePic", filePath);

        }

        new CallRequest(PTProfileFragment.this).updatePatientProfile(map, withImage);
    }

    private void getProfileData() {

        Map<String, String> map = new HashMap<String, String>();
        map.put("url", MyConstants.BASE_URL + "userProfile");
        map.put("ApiToken", MyConstants.API_TOKEN);
        map.put("UserId", App.user.getUserID());
        map.put("Version", MyConstants.WS_VERSION);
        map.put("SocialId", sharedpreferences.getString(MyConstants.SOCIAL_ID, "0"));
        map.put("SocialType", App.user.getLoginType());

        new CallRequest(PTProfileFragment.this).getProfilData(map);
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        Utils.removeSimpleSpinProgressDialog();
        try {
            if (result != null && !result.isEmpty()) {
                Log.i("TAG", "TAG Result : " + result);
                //      {"document":{"response":{"status":1,"message":"Success."}}}
                switch (request) {
                    case getProfile:
                        Utils.removeSimpleSpinProgressDialog();
                        try {
                            JSONObject mainObj = new JSONObject(result);
                            if (mainObj.getString("error_code").equalsIgnoreCase("0")) {

                                if (mainObj.getJSONArray("result") != null && mainObj.getJSONArray("result").length() > 0) {
                                    JSONArray jsonArray = mainObj.getJSONArray("result");
                                    JSONObject jsonObject = jsonArray.getJSONObject(0);
                                    JSONArray user = jsonObject.getJSONArray("user");
                                    JSONObject userObj = user.getJSONObject(0);
                                    JSONArray user_detail = jsonObject.getJSONArray("user_detail");
                                    JSONObject userDetailObj = user_detail.getJSONObject(0);

                                    userProfile = new UserProfile();
                                    userProfile.setEmail(userObj.getString("email"));
                                    userProfile.setPhone(userObj.getString("phone"));
                                    userProfile.setGender(userDetailObj.getString("Gender"));
                                    userProfile.setBirthDate(userDetailObj.getString("BirthDate"));
                                    userProfile.setSmoke(userDetailObj.getString("Smoke"));
                                    userProfile.setBookingCount(userDetailObj.getString("BookingCount"));
                                    userProfile.setSavedCount(userDetailObj.getString("SavedCount"));
                                    userProfile.setAllergies(userDetailObj.getString("Allergies"));
                                    userProfile.setMedications(userDetailObj.getString("Medications"));
                                    userProfile.setProfilePic(userDetailObj.getString("ProfilePic"));
                                    userProfile.setFirstName(userDetailObj.getString("FirstName"));
                                    userProfile.setLastName(userDetailObj.getString("LastName"));

                                    tvName.setText(userProfile.getFirstName() + " " + userProfile.getLastName());
                                    etEmail.setText(userProfile.getEmail());
                                    etPhone.setText(userProfile.getPhone());
                                    if (userProfile.getGender().equalsIgnoreCase("male")) {
                                        spinner.setSelection(0);
                                    } else if (userProfile.getGender().equalsIgnoreCase("female")) {
                                        spinner.setSelection(1);
                                    }
                                    tvSavedDoctors.setText("(" + userProfile.getSavedCount() + ")");
                                    tvAppointments.setText("(" + userProfile.getBookingCount() + ")");
                                    etDOB.setText(userProfile.getBirthDate());
                                    etSmoker.setText(userProfile.getSmoke());
                                    etAllergies.setText(userProfile.getAllergies());
                                    etMedications.setText(userProfile.getMedications());

                                    MyConstants.editPatientProfile = userProfile;
                                    System.out.println("constant profile:::"+MyConstants.editPatientProfile);

                                    System.out.println("profile pic::" + userProfile.getProfilePic());

                                    try {
                                        PicassoTrustAll.getInstance(getActivity())
                                                .load(userProfile.getProfilePic())
                                                .error(R.drawable.avatar)
                                                .into(ivUser, new Callback() {
                                                    @Override
                                                    public void onSuccess() {
                                                    }

                                                    @Override
                                                    public void onError() {
                                                        ivUser.setImageDrawable(getResources().getDrawable(R.drawable.avatar));
                                                    }
                                                });
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }

                                } else {
                                    Utils.showToast(mainObj.getString("error_string"), getActivity());
                                }
                            } else {
                                Utils.showToast(mainObj.getString("error_string"), getActivity());
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;

                    case updatePatientProfile:
                        Utils.removeSimpleSpinProgressDialog();
                        try {
                            JSONObject mainObj = new JSONObject(result);
                            if (mainObj.getString("error_code").equalsIgnoreCase("0")) {

                                if (mainObj.getJSONArray("result") != null && mainObj.getJSONArray("result").length() > 0) {
                                    JSONObject jsonObject = mainObj.getJSONArray("result").getJSONObject(0);

                                    JSONArray user = jsonObject.getJSONArray("user");
                                    JSONObject userObj = user.getJSONObject(0);
                                    JSONArray user_detail = jsonObject.getJSONArray("user_detail");
                                    JSONObject userDetailObj = user_detail.getJSONObject(0);

                                    userProfile = new UserProfile();
                                    userProfile.setEmail(userObj.getString("email"));
                                    userProfile.setPhone(userObj.getString("phone"));
                                    userProfile.setGender(userDetailObj.getString("Gender"));
                                    userProfile.setBirthDate(userDetailObj.getString("BirthDate"));
                                    userProfile.setSmoke(userDetailObj.getString("Smoke"));
                                    userProfile.setAllergies(userDetailObj.getString("Allergies"));
                                    userProfile.setMedications(userDetailObj.getString("Medications"));
                                    userProfile.setProfilePic(userDetailObj.getString("ProfilePic"));
                                    userProfile.setFirstName(userDetailObj.getString("FirstName"));
                                    userProfile.setLastName(userDetailObj.getString("LastName"));

                                    tvName.setText(userProfile.getFirstName() + " " + userProfile.getLastName());
                                    etEmail.setText(userProfile.getEmail());
                                    etPhone.setText(userProfile.getPhone());
                                    if (userProfile.getGender().equalsIgnoreCase("male")) {
                                        spinner.setSelection(0);
                                    } else if (userProfile.getGender().equalsIgnoreCase("female")) {
                                        spinner.setSelection(1);
                                    }
                                    etDOB.setText(userProfile.getBirthDate());
                                    etSmoker.setText(userProfile.getSmoke());
                                    etAllergies.setText(userProfile.getAllergies());
                                    etMedications.setText(userProfile.getMedications());

                                    System.out.println("profile pic::" + userProfile.getProfilePic());

                                    try {
                                        PicassoTrustAll.getInstance(getActivity())
                                                .load(userProfile.getProfilePic())
                                                .error(R.drawable.avatar)
                                                .into(ivUser, new Callback() {
                                                    @Override
                                                    public void onSuccess() {
                                                    }

                                                    @Override
                                                    public void onError() {
                                                    }
                                                });
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }

                                    SharedPreferences sharedpreferences = getActivity().getSharedPreferences(MyConstants.PREF, Context.MODE_PRIVATE);

                                    SharedPreferences.Editor editor = sharedpreferences.edit();
                                    editor.putString(MyConstants.PROFILE_PIC, userProfile.getProfilePic());
                                    editor.commit();
                                    App.user.setProfileUrl(userProfile.getProfilePic());

                                    etEmail.setFocusable(false);
                                    etPhone.setFocusable(false);

                                    etDOB.setFocusable(false);
                                    etSmoker.setFocusable(false);
                                    etAllergies.setFocusable(false);
                                    etMedications.setFocusable(false);

                                    etEmail.setEnabled(false);
                                    etPhone.setEnabled(false);

                                    spinner.setEnabled(false);
                                    etDOB.setEnabled(false);
                                    etSmoker.setEnabled(false);
                                    etAllergies.setEnabled(false);
                                    etMedications.setEnabled(false);
                                    ivUser.setEnabled(false);

                                    spinner.setClickable(false);

                                    etEmail.setCursorVisible(false);
                                    etPhone.setCursorVisible(false);

                                    etDOB.setCursorVisible(false);
                                    etSmoker.setCursorVisible(false);
                                    etAllergies.setCursorVisible(false);
                                    etMedications.setCursorVisible(false);

                                    tvEdit.setVisibility(View.VISIBLE);
                                    tvSave.setVisibility(View.GONE);

                                    Utils.showToast("Profile saved successfully", getActivity());
                                } else {
                                    Utils.showToast(mainObj.getString("error_string"), getActivity());
                                }
                            } else {
                                Utils.showToast(mainObj.getString("error_string"), getActivity());
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;

                }
            }
        } catch (Exception e) {
            Utils.removeSimpleSpinProgressDialog();
            e.printStackTrace();
            Utils.showToast("Please try again later", getActivity());
        }
    }

    @Override
    public void onProgressUpdate(String uniqueMessageId, int progres) {

    }

    @Override
    public void onProgressComplete(String uniqueMessageId, String result, Constant.REQUESTS request) {

    }
}
