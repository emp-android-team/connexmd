package com.connex.md.patient.fragment;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.connex.md.R;
import com.connex.md.interfaces.AsyncTaskListner;
import com.connex.md.model.UserProfile;
import com.connex.md.others.App;
import com.connex.md.others.Internet;
import com.connex.md.others.PicassoTrustAll;
import com.connex.md.patient.activity.PTEditProfileActivity;
import com.connex.md.patient.adapter.MedicalReportsAdapter;
import com.connex.md.patient.adapter.MedicinesAdapter;
import com.connex.md.utils.VerticalSpacingDecoration;
import com.connex.md.ws.CallRequest;
import com.connex.md.ws.Constant;
import com.connex.md.ws.MyConstants;
import com.connex.md.ws.Utils;
import com.squareup.picasso.Callback;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import co.lujun.androidtagview.TagContainerLayout;

/**
 * A simple {@link Fragment} subclass.
 */
public class PTProfileNewFragment extends Fragment implements AsyncTaskListner {

    private LinearLayout llBack, llEdit;
    private RecyclerView rvMedicalReports, rvMedicines;
    private ImageView ivUser;
    private TextView tvUserName, tvEmail, tvPhone, tvGender, tvDOB, tvSmoker, tvHeight, tvWeight;
    private TagContainerLayout tagsAllergies, tagsSymptoms;
    private MedicinesAdapter medicinesAdapter;
    private MedicalReportsAdapter medicalReportsAdapter;
    private SharedPreferences sharedpreferences;
    private UserProfile userProfile;
    private TextView tvAllergyLabel, tvMedicineLabel, tvSymptomLabel, tvMedicalReportLabel;
    private View lineAllergy, lineMedicine, lineSymptom, lineReport;
    private SwipeRefreshLayout swipe_container;

    public PTProfileNewFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_pt_profile_new, container, false);

        llBack = view.findViewById(R.id.ivBack);
        llEdit = view.findViewById(R.id.tvEdit);
        rvMedicalReports = view.findViewById(R.id.rvMedicalReports);
        rvMedicines = view.findViewById(R.id.rvMedicines);
        ivUser = view.findViewById(R.id.ivUser);
        tvUserName = view.findViewById(R.id.tvUserName);
        tvEmail = view.findViewById(R.id.tvEmail);
        tvPhone = view.findViewById(R.id.tvPhone);
        tvGender = view.findViewById(R.id.tvGender);
        tvDOB = view.findViewById(R.id.tvDOB);
        tvSmoker = view.findViewById(R.id.tvSmoker);
        tvHeight = view.findViewById(R.id.tvHeight);
        tvWeight = view.findViewById(R.id.tvWeight);
        tagsAllergies = view.findViewById(R.id.tagsAllergies);
        tagsSymptoms = view.findViewById(R.id.tagsSymptoms);
        tvAllergyLabel = view.findViewById(R.id.tvAllergyLabel);
        tvMedicineLabel = view.findViewById(R.id.tvMedicineLabel);
        tvSymptomLabel = view.findViewById(R.id.tvSymptomLabel);
        tvMedicalReportLabel = view.findViewById(R.id.tvMedicalReportLabel);
        lineAllergy = view.findViewById(R.id.lineAllergy);
        lineMedicine = view.findViewById(R.id.lineMedicine);
        lineSymptom = view.findViewById(R.id.lineSymptom);
        lineReport = view.findViewById(R.id.lineReport);
        swipe_container = view.findViewById(R.id.swipe_container);

        sharedpreferences = getActivity().getSharedPreferences(MyConstants.PREF, Context.MODE_PRIVATE);

        llBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();
            }
        });

        llEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (MyConstants.editPatientProfile != null) {
                    startActivity(new Intent(getActivity(), PTEditProfileActivity.class));
                }
            }
        });


        GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(), 3);
        gridLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rvMedicalReports.setLayoutManager(gridLayoutManager);

        GridLayoutManager gridLayoutManager1 = new GridLayoutManager(getActivity(), 3);
        gridLayoutManager1.setOrientation(LinearLayoutManager.VERTICAL);
        rvMedicines.setLayoutManager(gridLayoutManager1);

        rvMedicalReports.setHasFixedSize(true);
        rvMedicalReports.addItemDecoration(new VerticalSpacingDecoration(30));
        rvMedicalReports.setItemViewCacheSize(20);
        rvMedicalReports.setDrawingCacheEnabled(true);
        rvMedicalReports.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);

        rvMedicines.setHasFixedSize(true);
        rvMedicines.setItemViewCacheSize(20);
        rvMedicines.setDrawingCacheEnabled(true);
        rvMedicines.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);

        swipe_container.setColorScheme(android.R.color.holo_blue_dark,
                android.R.color.holo_blue_light,
                android.R.color.holo_green_dark,
                android.R.color.holo_green_light);

        swipe_container.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getProfileData();
            }
        });

        System.out.println("profile:::" + MyConstants.userProfile);
        if (MyConstants.userProfile != null) {
            setUserData();
        }

        if (MyConstants.isProfileLoad) {
            getProfileData();
            MyConstants.isProfileLoad = false;
        }

        return view;
    }

    private void getProfileData() {

        if (!Internet.isAvailable(getActivity())) {
            Internet.showAlertDialog(getActivity(), "Error!", "No Internet Connection", false);
            return;
        }

        Map<String, String> map = new HashMap<String, String>();
        map.put("url", MyConstants.BASE_URL + "userProfile");
        map.put("ApiToken", MyConstants.API_TOKEN);
        map.put("UserId", App.user.getUserID());
        map.put("Version", MyConstants.WS_VERSION);
        map.put("SocialId", sharedpreferences.getString(MyConstants.SOCIAL_ID, "0"));
        map.put("SocialType", App.user.getLoginType());

        new CallRequest(PTProfileNewFragment.this).getProfilData(map);
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        Utils.removeSimpleSpinProgressDialog();
        try {
            if (result != null && !result.isEmpty()) {
                Log.i("TAG", "TAG Result : " + result);
                //      {"document":{"response":{"status":1,"message":"Success."}}}
                switch (request) {
                    case getProfile:
                        Utils.removeSimpleSpinProgressDialog();

                        List<HashMap<String, String>> allergyList, reportList, medicineList, symptomList;

                        allergyList = new ArrayList<>();
                        reportList = new ArrayList<>();
                        medicineList = new ArrayList<>();
                        symptomList = new ArrayList<>();

                        try {
                            JSONObject mainObj = new JSONObject(result);
                            if (mainObj.getString("error_code").equalsIgnoreCase("0")) {

                                if (mainObj.getJSONArray("result") != null && mainObj.getJSONArray("result").length() > 0) {
                                    JSONArray jsonArray = mainObj.getJSONArray("result");
                                    JSONObject jsonObject = jsonArray.getJSONObject(0);
                                    JSONArray user = jsonObject.getJSONArray("user");
                                    JSONObject userObj = user.getJSONObject(0);
                                    JSONArray user_detail = jsonObject.getJSONArray("user_detail");
                                    JSONObject userDetailObj = user_detail.getJSONObject(0);

                                    JSONArray allergyArray = jsonObject.getJSONArray("Allergy");
                                    JSONArray reportArray = jsonObject.getJSONArray("Reports");
                                    JSONArray medicineArray = jsonObject.getJSONArray("Medicine");
                                    JSONArray symptomsArray = jsonObject.getJSONArray("Symptoms");

                                    for (int i = 0; i < allergyArray.length(); i++) {
                                        JSONObject allergyObj = allergyArray.getJSONObject(i);

                                        HashMap<String, String> map = new HashMap<>();
                                        map.put("id", allergyObj.getString("id"));
                                        map.put("Description", allergyObj.getString("Description"));

                                        allergyList.add(map);
                                    }

                                    for (int i = 0; i < reportArray.length(); i++) {
                                        JSONObject reportObj = reportArray.getJSONObject(i);

                                        HashMap<String, String> map = new HashMap<>();
                                        map.put("id", reportObj.getString("id"));
                                        map.put("Description", reportObj.getString("Description"));

                                        reportList.add(map);
                                    }

                                    for (int i = 0; i < medicineArray.length(); i++) {
                                        JSONObject medicineObj = medicineArray.getJSONObject(i);

                                        HashMap<String, String> map = new HashMap<>();
                                        map.put("id", medicineObj.getString("id"));
                                        map.put("Description", medicineObj.getString("Description"));

                                        medicineList.add(map);
                                    }

                                    for (int i = 0; i < symptomsArray.length(); i++) {
                                        JSONObject symptomObj = symptomsArray.getJSONObject(i);

                                        HashMap<String, String> map = new HashMap<>();

                                        map.put("id", symptomObj.getString("id"));
                                        map.put("Description", symptomObj.getString("Symptoms"));

                                        symptomList.add(map);
                                    }


                                    userProfile = new UserProfile();
                                    userProfile.setEmail(userObj.getString("email"));
                                    userProfile.setPhone(userObj.getString("phone"));
                                    userProfile.setGender(userDetailObj.getString("Gender"));
                                    userProfile.setBirthDate(userDetailObj.getString("BirthDate"));
                                    userProfile.setSmoke(userDetailObj.getString("Smoke"));
                                    userProfile.setBookingCount(userDetailObj.getString("BookingCount"));
                                    userProfile.setSavedCount(userDetailObj.getString("SavedCount"));
                                    userProfile.setAllergies(userDetailObj.getString("Allergies"));
                                    userProfile.setMedications(userDetailObj.getString("Medications"));
                                    userProfile.setProfilePic(userDetailObj.getString("ProfilePic"));
                                    userProfile.setFirstName(userDetailObj.getString("FirstName"));
                                    userProfile.setLastName(userDetailObj.getString("LastName"));
                                    userProfile.setHeight(userDetailObj.getString("Height"));
                                    userProfile.setWeight(userDetailObj.getString("Weight"));
                                    userProfile.setUnit(userDetailObj.getString("Unit"));
                                    userProfile.setUnitHeight(userDetailObj.getString("HeightUnit"));
                                    userProfile.setAllergy(allergyList);
                                    userProfile.setMedicines(medicineList);
                                    userProfile.setReports(reportList);
                                    userProfile.setSymptoms(symptomList);

                                    MyConstants.userProfile = new UserProfile();
                                    MyConstants.userProfile = userProfile;

                                    setUserData();

                                    swipe_container.setRefreshing(false);

                                } else {
                                    Utils.showToast(mainObj.getString("error_string"), getActivity());
                                }
                            } else {
                                Utils.showToast(mainObj.getString("error_string"), getActivity());
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;
                }
            }
        } catch (Exception e) {
            Utils.removeSimpleSpinProgressDialog();
            e.printStackTrace();
            Utils.showToast("Please try again later", getActivity());
        }
    }

    @Override
    public void onResume() {
        if (MyConstants.isBackPressed) {
            getProfileData();
            MyConstants.isBackPressed = false;
        }
        super.onResume();
    }

    private void setUserData() {

        tvUserName.setText(MyConstants.userProfile.getFirstName() + " " + MyConstants.userProfile.getLastName());

        if (!TextUtils.isEmpty(MyConstants.userProfile.getEmail().trim())) {
            tvEmail.setText(MyConstants.userProfile.getEmail());
        } else {
            tvEmail.setText("N/A");
        }
        if (!TextUtils.isEmpty(MyConstants.userProfile.getPhone().trim())) {
            tvPhone.setText(MyConstants.userProfile.getPhone());
        } else {
            tvPhone.setText("N/A");
        }
        if (!TextUtils.isEmpty(MyConstants.userProfile.getGender().trim())) {
            tvGender.setText(MyConstants.userProfile.getGender());
        } else {
            tvGender.setText("N/A");
        }
        if (!TextUtils.isEmpty(MyConstants.userProfile.getBirthDate().trim()) && !MyConstants.userProfile.getBirthDate().equalsIgnoreCase("0000-00-00")) {
            tvDOB.setText(MyConstants.userProfile.getBirthDate());
        } else {
            tvDOB.setText("N/A");
        }
        if (!TextUtils.isEmpty(MyConstants.userProfile.getSmoke().trim())) {
            tvSmoker.setText(MyConstants.userProfile.getSmoke());
        } else {
            tvSmoker.setText("N/A");
        }
        if (!TextUtils.isEmpty(MyConstants.userProfile.getHeight().trim())) {
            tvHeight.setText(MyConstants.userProfile.getHeight() + " " + MyConstants.userProfile.getUnitHeight());
        } else {
            tvHeight.setText("N/A");
        }
        if (!TextUtils.isEmpty(MyConstants.userProfile.getWeight().trim())) {
            tvWeight.setText(MyConstants.userProfile.getWeight() + " " + MyConstants.userProfile.getUnit());
        } else {
            tvWeight.setText("N/A");
        }


        MyConstants.editPatientProfile = MyConstants.userProfile;
        System.out.println("constant profile:::" + MyConstants.editPatientProfile);

        System.out.println("profile pic::" + MyConstants.userProfile.getProfilePic());

        if (!TextUtils.isEmpty(MyConstants.userProfile.getProfilePic())) {
            try {
                PicassoTrustAll.getInstance(getActivity())
                        .load(MyConstants.userProfile.getProfilePic())
                        .placeholder(R.drawable.avatar)
                        .error(R.drawable.avatar)
                        .into(ivUser, new Callback() {
                            @Override
                            public void onSuccess() {
                            }

                            @Override
                            public void onError() {
                                ivUser.setImageDrawable(getResources().getDrawable(R.drawable.avatar));
                            }
                        });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (MyConstants.userProfile.getAllergy().size() > 0) {
            List<String> allergies = new ArrayList<>();
            for (int i = 0; i < MyConstants.userProfile.getAllergy().size(); i++) {
                allergies.add(MyConstants.userProfile.getAllergy().get(i).get("Description"));
            }
            tagsAllergies.setTags(allergies);

            tvAllergyLabel.setVisibility(View.VISIBLE);
            lineAllergy.setVisibility(View.VISIBLE);
            tagsAllergies.setVisibility(View.VISIBLE);
        } else {
            tvAllergyLabel.setVisibility(View.GONE);
            lineAllergy.setVisibility(View.GONE);
            tagsAllergies.setVisibility(View.GONE);
        }

        if (MyConstants.userProfile.getSymptoms().size() > 0) {
            List<String> symptoms = new ArrayList<>();
            for (int i = 0; i < MyConstants.userProfile.getSymptoms().size(); i++) {
                symptoms.add(MyConstants.userProfile.getSymptoms().get(i).get("Description"));
            }
            tagsSymptoms.setTags(symptoms);

            tvSymptomLabel.setVisibility(View.VISIBLE);
            lineSymptom.setVisibility(View.VISIBLE);
            tagsSymptoms.setVisibility(View.VISIBLE);
        } else {
            tvSymptomLabel.setVisibility(View.GONE);
            lineSymptom.setVisibility(View.GONE);
            tagsSymptoms.setVisibility(View.GONE);
        }

        if (MyConstants.userProfile.getMedicines().size() > 0) {
            medicinesAdapter = new MedicinesAdapter(getActivity(), MyConstants.userProfile.getMedicines());
            rvMedicines.setAdapter(medicinesAdapter);

            tvMedicineLabel.setVisibility(View.VISIBLE);
            lineMedicine.setVisibility(View.VISIBLE);
            rvMedicines.setVisibility(View.VISIBLE);
        } else {
            tvMedicineLabel.setVisibility(View.GONE);
            lineMedicine.setVisibility(View.GONE);
            rvMedicines.setVisibility(View.GONE);
        }

        if (MyConstants.userProfile.getReports().size() > 0) {
            medicalReportsAdapter = new MedicalReportsAdapter(getActivity(), MyConstants.userProfile.getReports());
            rvMedicalReports.setAdapter(medicalReportsAdapter);

            tvMedicalReportLabel.setVisibility(View.VISIBLE);
            lineReport.setVisibility(View.VISIBLE);
            rvMedicalReports.setVisibility(View.VISIBLE);
        } else {
            tvMedicalReportLabel.setVisibility(View.GONE);
            lineReport.setVisibility(View.GONE);
            rvMedicalReports.setVisibility(View.GONE);
        }

    }

    @Override
    public void onProgressUpdate(String uniqueMessageId, int progres) {

    }

    @Override
    public void onProgressComplete(String uniqueMessageId, String result, Constant.REQUESTS request) {

    }
}
