package com.connex.md.patient.fragment;


import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.connex.md.R;
import com.connex.md.interfaces.AsyncTaskListner;
import com.connex.md.others.App;
import com.connex.md.others.Internet;
import com.connex.md.ws.CallRequest;
import com.connex.md.ws.Constant;
import com.connex.md.ws.MyConstants;
import com.connex.md.ws.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static android.content.Context.MODE_PRIVATE;

/**
 * A simple {@link Fragment} subclass.
 */
public class ReferAndEarnFragment extends Fragment implements AsyncTaskListner {

    Button btnReferFriend;
    TextView tvFirstConsult, tvReferCode, tvSavedBucks, tvCopy;
    LinearLayout ivBack;
    LinearLayout llRateUs;
    SharedPreferences sharedPreferences;
    ImageView ivSMS, ivCopy, ivEmail, ivFB, ivInsta;
    String referLink, link;

    public ReferAndEarnFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_refer_and_earn, container, false);

        ivBack = view.findViewById(R.id.ivBack);
        tvFirstConsult = view.findViewById(R.id.tvFirstConsult);
        tvReferCode = view.findViewById(R.id.tvReferCode);
        tvCopy = view.findViewById(R.id.tvCopy);
        btnReferFriend = view.findViewById(R.id.btnReferFriend);
        llRateUs = view.findViewById(R.id.llRateUs);
        tvSavedBucks = view.findViewById(R.id.tvSavedBucks);
        ivSMS = view.findViewById(R.id.ivSMS);
        ivCopy = view.findViewById(R.id.ivCopy);
        ivEmail = view.findViewById(R.id.ivEmail);
        ivFB = view.findViewById(R.id.ivFB);
        ivInsta = view.findViewById(R.id.ivInsta);

        sharedPreferences = getActivity().getSharedPreferences(MyConstants.PREF, MODE_PRIVATE);

        final String referCode = sharedPreferences.getString(MyConstants.REFER_CODE, "");

        tvReferCode.setText(referCode);

        llRateUs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String appPackageName = getActivity().getPackageName(); // getPackageName() from Context or Activity object
                try {

                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                } catch (android.content.ActivityNotFoundException anfe) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                }
            }
        });

        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();
            }
        });

        tvCopy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ClipboardManager clipboard = (ClipboardManager) getActivity().getSystemService(Context.CLIPBOARD_SERVICE);
                ClipData clip = ClipData.newPlainText("Refer Code", referCode);
                if (clipboard != null) {
                    clipboard.setPrimaryClip(clip);
                    Utils.showToast("The code was copied to clipboard", getActivity());
                }
            }
        });

        ivCopy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (referLink != null) {
                    ClipboardManager clipboard = (ClipboardManager) getActivity().getSystemService(Context.CLIPBOARD_SERVICE);
                    ClipData clip = ClipData.newPlainText("Refer Link", referLink);
                    if (clipboard != null) {
                        clipboard.setPrimaryClip(clip);
                        Utils.showToast("The link was copied to clipboard", getActivity());
                    }
                }
            }
        });

        ivSMS.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Intent intent = new Intent(Intent.ACTION_SEND);
                //intent.setData(Uri.parse("smsto:")); // This ensures only SMS apps respond
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("sms:"));

                intent.putExtra("sms_body", referLink);
                if (intent.resolveActivity(getActivity().getPackageManager()) != null) {
                    startActivity(intent);
                } else {
                    Utils.showToast("Your system does not support this", getActivity());
                }
            }
        });

        ivEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                        "mailto", "", null));
                emailIntent.putExtra(Intent.EXTRA_SUBJECT, "");
                emailIntent.putExtra(Intent.EXTRA_TEXT, referLink);
                startActivity(Intent.createChooser(emailIntent, "Send Email"));
            }
        });

        ivFB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                shareAppLinkViaFacebook(link);
            }
        });

        ivInsta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                shareAppLinkViaTwitter(link);
            }
        });

        getReferAPIData();

        return view;
    }

    private void shareAppLinkViaTwitter(String referLink) {
        try {
            Intent shareIntent = new Intent();
            shareIntent.setAction(Intent.ACTION_SEND);
            shareIntent.setType("text/plain");
            //shareIntent.putExtra(Intent.EXTRA_STREAM,Uri.parse(referLink));
            shareIntent.setPackage("com.twitter.android");

            shareIntent.putExtra(Intent.EXTRA_TEXT, referLink);
            //if (shareIntent.resolveActivity(getActivity().getPackageManager()) != null) {
                getActivity().startActivity(shareIntent);
            /*} else {
                //Toast.makeText(getActivity(), "You have not installed Twitter", Toast.LENGTH_SHORT).show();
            }*/
        } catch (Exception e) {
            e.printStackTrace();

            Intent shareIntent;
            String tweetUrl = "https://twitter.com/intent/tweet?text=" + referLink;
            Uri uri = Uri.parse(tweetUrl);
            shareIntent = new Intent(Intent.ACTION_VIEW, uri);

            if (shareIntent.resolveActivity(getActivity().getPackageManager()) != null)
                getActivity().startActivity(shareIntent);
            else {
                Toast.makeText(getActivity(), "You have not installed any Browser", Toast.LENGTH_SHORT).show();

            }
        }
    }

    private void shareAppLinkViaFacebook(String urlToShare) {
        try {
            Intent intent1 = new Intent();
            intent1.setPackage("com.facebook.katana");
            //intent1.setClassName("com.facebook.katana", "com.facebook.katana.activity.composer.ImplicitShareIntentHandler");
            intent1.setAction("android.intent.action.SEND");
            intent1.setType("text/plain");
            intent1.putExtra("android.intent.extra.TEXT", urlToShare);
            startActivity(intent1);
        } catch (Exception e) {
            // If we failed (not native FB app installed), try share through SEND
            /*String sharerUrl = "https://www.facebook.com/sharer/sharer.php?u=" + urlToShare;
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(sharerUrl));
            startActivity(intent);*/

            e.printStackTrace();
            Intent mIntentFacebookBrowser;
            String mStringURL = "https://www.facebook.com/sharer/sharer.php?u=" + urlToShare;
            mIntentFacebookBrowser = new Intent(Intent.ACTION_VIEW, Uri.parse(mStringURL));

            if (mIntentFacebookBrowser.resolveActivity(getActivity().getPackageManager()) != null)
                getActivity().startActivity(mIntentFacebookBrowser);
            else {
                Toast.makeText(getActivity(), "You have not installed any Browser", Toast.LENGTH_SHORT).show();

            }
        }
    }

    private void getReferAPIData() {

        if (!Internet.isAvailable(getActivity())) {
            Internet.showAlertDialog(getActivity(), "Error!", "No Internet Connection", false);
            return;
        }

        Map<String, String> map = new HashMap<String, String>();
        map.put("url", MyConstants.BASE_URL + "discountData");
        map.put("ApiToken", MyConstants.API_TOKEN);
        map.put("Version", MyConstants.WS_VERSION);
        map.put("UserId", App.user.getUserID());

        new CallRequest(ReferAndEarnFragment.this).getReferData(map);
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        Utils.removeSimpleSpinProgressDialog();
        try {
            if (result != null && !result.isEmpty()) {
                Log.i("TAG", "TAG Result : " + result);
                //      {"document":{"response":{"status":1,"message":"Success."}}}
                switch (request) {
                    case referData:
                        try {
                            JSONObject mainObj = new JSONObject(result);
                            if (mainObj.getString("error_code").equalsIgnoreCase("0")) {
                                //Utils.showToast(mainObj.getString("error_string"), GuestVerifyOTPActivity.this);

                                JSONObject resultObj = mainObj.getJSONObject("result");

                                String SavedAmount = resultObj.getString("SavedAmount");
                                final String ReferLink = resultObj.getString("ReferLink");
                                String ReferDiscount = resultObj.getString("ReferDiscount");
                                String SignUpDiscount = resultObj.getString("SignUpDiscount");
                                link = resultObj.getString("Link");
                                referLink = ReferLink;

                                String firstConsultHTML = "Save <font color='#39c3f6'>" + ReferDiscount + "%</font> on your First Consult and <font color='#39c3f6'>"
                                        + SignUpDiscount + "%</font> for each friend referral..!";
                                tvFirstConsult.setText(Html.fromHtml(firstConsultHTML));

                                String referHTML = "and another <font color='#39c3f6'>" + SignUpDiscount + "%</font> for every user that  you refer to us.";
                                //tvRefer.setText(Html.fromHtml(referHTML));

                                tvSavedBucks.setText(SavedAmount);

                                btnReferFriend.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        String shareBody = ReferLink;
                                        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                                        sharingIntent.setType("text/plain");

                                        sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "ConnexMD Referral System");
                                        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
                                        startActivity(Intent.createChooser(sharingIntent, "Share via"));

                                    }
                                });

                            } else {
                                Utils.showToast(mainObj.getString("error_string"), getActivity());
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onProgressUpdate(String uniqueMessageId, int progres) {

    }

    @Override
    public void onProgressComplete(String uniqueMessageId, String result, Constant.REQUESTS request) {

    }
}
