package com.connex.md.patient.fragment;


import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.connex.md.R;
import com.connex.md.interfaces.AsyncTaskListner;
import com.connex.md.model.SearchDoctor;
import com.connex.md.model.Specialities;
import com.connex.md.others.App;
import com.connex.md.others.Internet;
import com.connex.md.others.PicassoTrustAll;
import com.connex.md.patient.adapter.InPersonVisitAdapter;
import com.connex.md.ws.CallRequest;
import com.connex.md.ws.Constant;
import com.connex.md.ws.MyConstants;
import com.connex.md.ws.Utils;
import com.squareup.picasso.Callback;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 */
public class SearchDoctorFragment extends Fragment implements AsyncTaskListner{

    RecyclerView rvSearchDoctor;
    ImageView ivBanner;
    LinearLayout ivBack;
    ToggleButton tbFavourite;
    TextView tvCategory;
    InPersonVisitAdapter searchDoctorAdapter;
    public Specialities specialities;
    ArrayList<SearchDoctor> doctorList = new ArrayList<>();
    private SharedPreferences sharedpreferences;
    public Dialog dialog;
    Button btnSend;
    EditText etMessage;
    TextView tvCategoryName;

    public SearchDoctorFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_search_doctor, container, false);

        sharedpreferences = getActivity().getSharedPreferences(MyConstants.PREF, Context.MODE_PRIVATE);

        rvSearchDoctor = view.findViewById(R.id.rvSearchDoctor);
        ivBack = view.findViewById(R.id.ivBack);
        ivBanner = view.findViewById(R.id.ivBanner);
        tbFavourite = view.findViewById(R.id.toggle);
        tvCategory = view.findViewById(R.id.tvCategory);

        Bundle b = getArguments();
        if (b != null) {
            specialities = (Specialities) b.getSerializable("speciality");
        }

        try {
            PicassoTrustAll.getInstance(getActivity())
                    .load(specialities.getImage())
                    .error(R.drawable.no_img)
                    .into(ivBanner, new Callback() {
                        @Override
                        public void onSuccess() {
                        }

                        @Override
                        public void onError() {
                        }
                    });
        } catch (Exception e){
            e.printStackTrace();
        }


        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();
            }
        });

        tvCategory.setText(specialities.getSpeciality());


        tbFavourite.setEnabled(false);
        if (specialities.getIsSaved() == 0){
            tbFavourite.setChecked(false);
            tbFavourite.setVisibility(View.INVISIBLE);
        } else {
            tbFavourite.setChecked(true);
            tbFavourite.setVisibility(View.VISIBLE);
        }

        getDoctorListAPI();

        return view;
    }

    private void openNotificationDialog() {
        createDialog();
        initDialogComponents();

        tvCategoryName.setText(specialities.getSpeciality());

        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String message = etMessage.getText().toString().trim();

                if (TextUtils.isEmpty(message)){
                    etMessage.setError("Description is empty");
                    etMessage.requestFocus();
                    return;
                }

                dialog.dismiss();
                sendNotificationToDoctors(message);
            }
        });
    }

    private void createDialog() {
        dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        // Include dialog.xml file
        dialog.setContentView(R.layout.dialog_notification_doctor);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }

    private void initDialogComponents() {
        etMessage = dialog.findViewById(R.id.etMessage);
        tvCategoryName = dialog.findViewById(R.id.tvCategoryName);
        btnSend = dialog.findViewById(R.id.btnSend);
    }

    private void sendNotificationToDoctors(String message) {
        if (!Internet.isAvailable(getActivity())) {
            Internet.showAlertDialog(getActivity(), "Error!", "No Internet Connection", false);

            return;
        }
        String urlStr = "sendDoctorNotif";

        Map<String, String> map = new HashMap<String, String>();
        map.put("url", MyConstants.DOCTOR_BASE_URL + urlStr);
        map.put("ApiToken", MyConstants.API_TOKEN);
        map.put("Version", MyConstants.WS_VERSION);
        map.put("SpecialityId", String.valueOf(specialities.getId()));
        map.put("DoctorId", App.user.getUserID());
        map.put("Message", message);

        new CallRequest(getActivity()).sendOfflineNotificationSpecialityDoctor(map);
    }

    /*@Override
    public void onBackPressed() {
        MyConstants.isBackPressed = true;

        try {
            InputMethodManager inputMethodManager = (InputMethodManager)
                    getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            if (inputMethodManager != null) {
                inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
            }
        } catch (Exception e){
            e.printStackTrace();
        }
        super.onBackPressed();
    }*/

    public void getDoctorListAPI() {

        if (!Internet.isAvailable(getActivity())) {
            Internet.showAlertDialog(getActivity(), "Error!", "No Internet Connection", false);

            return;
        }
        String urlStr = "specialityWiseDoctor";

        Map<String, String> map = new HashMap<String, String>();
        map.put("url", MyConstants.BASE_URL + urlStr);
        map.put("ApiToken", MyConstants.API_TOKEN);
        map.put("Version", MyConstants.WS_VERSION);
        map.put("SpecialityId", String.valueOf(specialities.getId()));
        Log.i("speciality_id", MyConstants.BASE_URL + urlStr + "speciality_id:->" + specialities.getId());
        if (sharedpreferences.getBoolean(MyConstants.IS_LOGGED_IN, false)) {
            if (App.user.getUser_Type().equalsIgnoreCase(MyConstants.USER_DR)) {
                map.put("DoctorId", App.user.getUserID());
            } else {
                map.put("UserId", App.user.getUserID());
                map.put("IsGuest", MyConstants.isGuest);
            }
        }

        new CallRequest(SearchDoctorFragment.this).doctorListFragment(map);

    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        Utils.removeSimpleSpinProgressDialog();
        try {
            if (result != null && !result.isEmpty()) {
                Log.i("TAG", "TAG Result : " + result);
                //      {"document":{"response":{"status":1,"message":"Success."}}}
                switch (request) {
                    case doctorList:
                        Utils.removeSimpleSpinProgressDialog();
                        try {
                            JSONObject mainObj = new JSONObject(result);
                            if (mainObj.getString("error_code").equalsIgnoreCase("0")) {
                                doctorList.clear();
                                if (mainObj.getJSONArray("result") != null && mainObj.getJSONArray("result").length() > 0) {
                                    JSONArray jsonArray = mainObj.getJSONArray("result");
                                    JSONObject jsonObject = jsonArray.getJSONObject(0);
                                    JSONArray Doctors = jsonObject.getJSONArray("Doctors");
                                    for (int i = 0; i < Doctors.length(); i++) {
                                        JSONObject object = Doctors.getJSONObject(i);
                                        JSONArray doctor_detail = object.getJSONArray("doctor_detail");
                                        JSONObject detail_obj = doctor_detail.getJSONObject(0);

                                        System.out.println("name::" + detail_obj.getString("FirstName"));

                                        SearchDoctor searchDoctor = new SearchDoctor();
                                        searchDoctor.setFirstName(detail_obj.getString("FirstName"));
                                        searchDoctor.setLastName(detail_obj.getString("LastName"));
                                        searchDoctor.setDoctorId(detail_obj.getString("DoctorId"));
                                        searchDoctor.setCosultCharge(detail_obj.getString("CosultCharge"));
                                        searchDoctor.setCountry(detail_obj.getString("Country"));
                                        searchDoctor.setProfilePic(detail_obj.getString("ProfilePic"));
                                        searchDoctor.setLocalCharge(detail_obj.getString("LocalCharge"));
                                        searchDoctor.setIsFree(detail_obj.getString("IsFree"));

                                        if (specialities!= null && specialities.getSpeciality().equalsIgnoreCase("Nutrition")){
                                            searchDoctor.setIsNutrition("1");
                                        } else {
                                            searchDoctor.setIsNutrition("0");
                                        }

                                        doctorList.add(searchDoctor);
                                    }
                                    LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
                                    layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
                                    rvSearchDoctor.setLayoutManager(layoutManager);

                                    LayoutAnimationController controller = AnimationUtils.loadLayoutAnimation(rvSearchDoctor.getContext(), R.anim.layout_animation_fall_down);
                                    rvSearchDoctor.setLayoutAnimation(controller);
                                    rvSearchDoctor.scheduleLayoutAnimation();

                                    searchDoctorAdapter = new InPersonVisitAdapter(SearchDoctorFragment.this, doctorList);
                                    rvSearchDoctor.setHasFixedSize(true);
                                    //rvSearchDoctor.addItemDecoration(new VerticalSpacingDecoration(20));
                                    rvSearchDoctor.setItemViewCacheSize(20);
                                    rvSearchDoctor.setDrawingCacheEnabled(true);
                                    rvSearchDoctor.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);

                                    rvSearchDoctor.setAdapter(searchDoctorAdapter);
                                } else {
                                    Utils.showToast(mainObj.getString("error_string"), getActivity());
                                }
                            } else {
                                Utils.showToast(mainObj.getString("error_string"), getActivity());
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;

                    case offlineNotification:
                        Utils.removeSimpleSpinProgressDialog();
                        try {
                            JSONObject mainObj = new JSONObject(result);
                            if (mainObj.getString("error_code").equalsIgnoreCase("0")) {
                                Utils.showToast("Message sent successfully", getActivity());
                            } else {
                                Utils.showToast(mainObj.getString("error_string"), getActivity());
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;
                }
            }
        } catch (Exception e) {
            Utils.removeSimpleSpinProgressDialog();
            e.printStackTrace();
            Utils.showToast("Please try again later", getActivity());
        }
    }

    @Override
    public void onProgressUpdate(String uniqueMessageId, int progres) {

    }

    @Override
    public void onProgressComplete(String uniqueMessageId, String result, Constant.REQUESTS request) {

    }

}
