package com.connex.md.patient.fragment;


import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.connex.md.R;
import com.connex.md.custom_views.MultiSelectionSpinner;
import com.connex.md.interfaces.AsyncTaskListner;
import com.connex.md.others.App;
import com.connex.md.others.Internet;
import com.connex.md.ws.CallRequest;
import com.connex.md.ws.Constant;
import com.connex.md.ws.MyConstants;
import com.connex.md.ws.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import co.lujun.androidtagview.TagContainerLayout;
import co.lujun.androidtagview.TagView;

/**
 * A simple {@link Fragment} subclass.
 */
public class SymptomsFragment extends Fragment implements MultiSelectionSpinner.OnMultipleItemsSelectedListener, AsyncTaskListner {


    Button btnSave, btnAdd;
    Button btnSymptoms;
    TagContainerLayout tags;
    MultiSelectionSpinner spinner;
    List<Integer> symptomsIdAPIList = new ArrayList<>();
    List<String> symptomsAPIList = new ArrayList<>();
    List<String> symptomsList = new ArrayList<>();
    List<Integer> symptomsIdList = new ArrayList<>();
    List<Integer> selectedSymptomsList = new ArrayList<>();
    int pos;

    public SymptomsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_symptoms, container, false);

        //btnAdd = view.findViewById(R.id.btnAdd);
        //btnSave = view.findViewById(R.id.btnSavePatient);
        btnSymptoms = view.findViewById(R.id.btnSymptoms);
        tags = view.findViewById(R.id.tags);
        spinner = view.findViewById(R.id.spinner);

        selectedSymptomsList.clear();
        symptomsIdList.clear();
        symptomsList.clear();
        for (int i = 0; i < MyConstants.symptomsArray.size(); i++) {
            for (int j = 0; j < MyConstants.symptomsArray.get(i).size(); j++) {
                symptomsIdList.add(MyConstants.symptomsArray.get(i).get(j).getId());
                symptomsList.add(MyConstants.symptomsArray.get(i).get(j).getSymptoms());
            }
        }

        spinner.setItems(symptomsList);
        spinner.setListener(SymptomsFragment.this);

        symptomsIdAPIList.clear();
        symptomsAPIList.clear();
        for (int i = 0; i < MyConstants.editPatientProfile.getSymptoms().size(); i++) {
            symptomsIdAPIList.add(Integer.parseInt(MyConstants.editPatientProfile.getSymptoms().get(i).get("id")));
            symptomsAPIList.add(MyConstants.editPatientProfile.getSymptoms().get(i).get("Description"));
        }

        tags.setTags(symptomsAPIList);

        btnSymptoms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //btnSymptoms.requestFocus();
                spinner.performClick();
            }
        });

        tags.setOnTagClickListener(new TagView.OnTagClickListener() {
            @Override
            public void onTagClick(int position, String text) {

            }

            @Override
            public void onTagLongClick(int position, String text) {

            }

            @Override
            public void onTagCrossClick(int position) {
                pos = position;
                new AlertDialog.Builder(getActivity())
                        .setTitle("Alert!")
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setMessage("Would you like to delete Symptom?")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                deleteSymptoms(pos);
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })
                        .show();
            }
        });

        /*btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });*/

        return view;
    }

    private void deleteSymptoms(int pos) {
        if (!Internet.isAvailable(getActivity())) {
            Internet.showAlertDialog(getActivity(), "Error!", "No Internet Connection", false);

            return;
        }

        String urlStr = "userSymptoms";

        Map<String, String> map = new HashMap<String, String>();
        map.put("url", MyConstants.BASE_URL + urlStr);
        map.put("ApiToken", MyConstants.API_TOKEN);
        map.put("Version", MyConstants.WS_VERSION);
        map.put("UserId", App.user.getUserID());
        map.put("Symptoms", "0");
        map.put("IsDelete", "1");
        map.put("DeleteId", String.valueOf(symptomsIdAPIList.get(pos)));

        new CallRequest(SymptomsFragment.this).deleteSymptoms(map);
    }

    private void addSymptoms() {
        String selectedId = "";
        Set<Integer> hs = new HashSet<>();
        hs.addAll(selectedSymptomsList);
        selectedSymptomsList.clear();
        selectedSymptomsList.addAll(hs);

        if (selectedSymptomsList.size()>0) {
            selectedId = TextUtils.join(",", selectedSymptomsList);
            System.out.println("id::::" + selectedId);


            if (!Internet.isAvailable(getActivity())) {
                Internet.showAlertDialog(getActivity(), "Error!", "No Internet Connection", false);

                return;
            }

            String urlStr = "userSymptoms";

            Map<String, String> map = new HashMap<String, String>();
            map.put("url", MyConstants.BASE_URL + urlStr);
            map.put("ApiToken", MyConstants.API_TOKEN);
            map.put("Version", MyConstants.WS_VERSION);
            map.put("UserId", App.user.getUserID());
            map.put("Symptoms", selectedId);
            map.put("IsDelete", "0");
            map.put("DeleteId", "0");

            new CallRequest(SymptomsFragment.this).setSymptomsFragment(map);
        } else {
            Utils.showToast("Please select Symptoms!",getActivity());
        }
    }

    @Override
    public void selectedIndices(List<Integer> indices, List<String> strings) {

        List<Integer> addedSymptomsId = new ArrayList<>();
        addedSymptomsId.addAll(symptomsIdAPIList);

        List<String> addedSymptoms = new ArrayList<>();
        addedSymptoms.addAll(symptomsAPIList);

        List<Integer> tempId = new ArrayList<>();

        for (int i = 0; i < indices.size(); i++) {
            tempId.add(symptomsIdList.get(indices.get(i)));
        }

        tempId.removeAll(addedSymptomsId);
        strings.removeAll(addedSymptoms);

        System.out.println("index all::::"+symptomsIdAPIList);
        System.out.println("index:::" + tempId);
        System.out.println("symptoms***" + strings);

        if (tempId.size() > 0) {
            for (int i = 0; i < tempId.size(); i++) {
                symptomsAPIList.add(strings.get(i));
                symptomsIdAPIList.add(tempId.get(i));
                selectedSymptomsList.add(tempId.get(i));
            }
        }

        tags.setTags(symptomsAPIList);

        System.out.println("symptoms:::::" + symptomsAPIList);
        System.out.println("selected symptoms:::::" + selectedSymptomsList);

        addSymptoms();
    }

    @Override
    public void selectedStrings(List<String> strings) {

    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        Log.i("Response", "RESULT : " + result);
        Utils.removeSimpleSpinProgressDialog();
        if (result != null && !result.isEmpty()) {
            try {
                switch (request) {
                    case setSymptoms:

                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getString("error_code").equalsIgnoreCase("0")) {
                            selectedSymptomsList.clear();
                            MyConstants.editPatientProfile.getSymptoms().clear();

                            List<HashMap<String,String>> symptomList = new ArrayList<>();
                            for (int i = 0; i < symptomsAPIList.size(); i++) {
                                HashMap<String,String> map = new HashMap<>();
                                map.put("id", String.valueOf(symptomsIdAPIList.get(i)));
                                map.put("Description", symptomsAPIList.get(i));

                               symptomList.add(map);
                            }

                            MyConstants.editPatientProfile.setSymptoms(symptomList);
                            //Utils.showToast("Profile saved successfully", getActivity());
                        }

                        break;

                    case deleteSymptoms:
                        Utils.removeSimpleSpinProgressDialog();

                        try {
                            JSONObject mainObj = new JSONObject(result);
                            if (mainObj.getString("error_code").equalsIgnoreCase("0")) {

                                MyConstants.editPatientProfile.getSymptoms().remove(pos);
                                symptomsIdAPIList.clear();
                                symptomsAPIList.clear();
                                for (int i = 0; i < MyConstants.editPatientProfile.getSymptoms().size(); i++) {
                                    symptomsIdAPIList.add(Integer.parseInt(MyConstants.editPatientProfile.getSymptoms().get(i).get("id")));
                                    symptomsAPIList.add(MyConstants.editPatientProfile.getSymptoms().get(i).get("Description"));
                                }

                                tags.setTags(symptomsAPIList);

                            } else {
                                Utils.showToast(mainObj.getString("error_string"), getActivity());
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            Utils.showToast("Please try again later", getActivity());
        }
    }

    @Override
    public void onProgressUpdate(String uniqueMessageId, int progres) {

    }

    @Override
    public void onProgressComplete(String uniqueMessageId, String result, Constant.REQUESTS request) {

    }
}
