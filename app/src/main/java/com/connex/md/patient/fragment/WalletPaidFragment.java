package com.connex.md.patient.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.ImageView;

import com.connex.md.R;
import com.connex.md.patient.adapter.WalletAdapter;
import com.connex.md.utils.DividerItemDecoration;
import com.connex.md.ws.MyConstants;

/**
 * A simple {@link Fragment} subclass.
 */
public class WalletPaidFragment extends Fragment {

    public RecyclerView rvWalletPaid;
    private ImageView ivNoMessages;
    public WalletAdapter walletAdapter;

    public WalletPaidFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_wallet_paid, container, false);

        rvWalletPaid = view.findViewById(R.id.rvWalletPaid);
        ivNoMessages = view.findViewById(R.id.iv_no_messages);

        refreshList();

        return view;
    }

    public void refreshList(){
        if (MyConstants.walletPaidList.size() > 0) {
            rvWalletPaid.setVisibility(View.VISIBLE);
            ivNoMessages.setVisibility(View.GONE);
        } else {
            ivNoMessages.setVisibility(View.VISIBLE);
            rvWalletPaid.setVisibility(View.GONE);
        }

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rvWalletPaid.setLayoutManager(layoutManager);

        LayoutAnimationController controller = AnimationUtils.loadLayoutAnimation(rvWalletPaid.getContext(), R.anim.layout_animation_fall_down);
        rvWalletPaid.setLayoutAnimation(controller);
        rvWalletPaid.scheduleLayoutAnimation();

        walletAdapter = new WalletAdapter(getActivity(), MyConstants.walletPaidList);
        rvWalletPaid.setHasFixedSize(true);
        //rvPendingPatients.addItemDecoration(new VerticalSpacingDecoration(20));
        rvWalletPaid.addItemDecoration(new DividerItemDecoration(getActivity(), R.drawable.divider));
        rvWalletPaid.setItemViewCacheSize(20);
        rvWalletPaid.setDrawingCacheEnabled(true);
        rvWalletPaid.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);

        rvWalletPaid.setAdapter(walletAdapter);

        walletAdapter.notifyDataSetChanged();
    }

}
