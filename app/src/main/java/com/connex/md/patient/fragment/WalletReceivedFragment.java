package com.connex.md.patient.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.ImageView;

import com.connex.md.R;
import com.connex.md.patient.adapter.WalletAdapter;
import com.connex.md.utils.DividerItemDecoration;
import com.connex.md.ws.MyConstants;

/**
 * A simple {@link Fragment} subclass.
 */
public class WalletReceivedFragment extends Fragment {

    public RecyclerView rvWalletReceived;
    private ImageView ivNoMessages;
    public WalletAdapter walletAdapter;

    public WalletReceivedFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_wallet_received, container, false);

        rvWalletReceived = view.findViewById(R.id.rvWalletReceived);
        ivNoMessages = view.findViewById(R.id.iv_no_messages);

        refreshList();

        return view;
    }

    public void refreshList(){

        if (MyConstants.walletReceivedList.size() > 0) {
            rvWalletReceived.setVisibility(View.VISIBLE);
            ivNoMessages.setVisibility(View.GONE);
        } else {
            ivNoMessages.setVisibility(View.VISIBLE);
            rvWalletReceived.setVisibility(View.GONE);
        }

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rvWalletReceived.setLayoutManager(layoutManager);

        LayoutAnimationController controller = AnimationUtils.loadLayoutAnimation(rvWalletReceived.getContext(), R.anim.layout_animation_fall_down);
        rvWalletReceived.setLayoutAnimation(controller);
        rvWalletReceived.scheduleLayoutAnimation();

        System.out.println("list size:::"+MyConstants.walletReceivedList.size());

        walletAdapter = new WalletAdapter(getActivity(), MyConstants.walletReceivedList);
        rvWalletReceived.setHasFixedSize(true);
        //rvPendingPatients.addItemDecoration(new VerticalSpacingDecoration(20));
        rvWalletReceived.addItemDecoration(new DividerItemDecoration(getActivity(), R.drawable.divider));
        rvWalletReceived.setItemViewCacheSize(20);
        rvWalletReceived.setDrawingCacheEnabled(true);
        rvWalletReceived.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);

        rvWalletReceived.setAdapter(walletAdapter);

        walletAdapter.notifyDataSetChanged();
    }

}
