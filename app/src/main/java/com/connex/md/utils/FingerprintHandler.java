package com.connex.md.utils;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Build;
import android.os.CancellationSignal;
import android.support.v4.app.ActivityCompat;

import com.connex.md.doctor.activity.DRLoginActivity;
import com.connex.md.patient.activity.PTLoginActivity;
import com.connex.md.ws.MyConstants;

/**
 * Created by abc on 11/3/2017.
 */

@TargetApi(Build.VERSION_CODES.M)
public class FingerprintHandler extends FingerprintManager.AuthenticationCallback {

    public Activity context;
    String loginType;

    // Constructor
    public FingerprintHandler(Activity mContext, String loginType) {
        context = mContext;
        this.loginType = loginType;
    }

    public void startAuth(FingerprintManager manager, FingerprintManager.CryptoObject cryptoObject) {
        CancellationSignal cancellationSignal = new CancellationSignal();
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.USE_FINGERPRINT) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        manager.authenticate(cryptoObject, cancellationSignal, 0, this, null);
    }


    @Override
    public void onAuthenticationError(int errMsgId, CharSequence errString) {
        this.update("Fingerprint Authentication error\n" + errString, false);
    }


    @Override
    public void onAuthenticationHelp(int helpMsgId, CharSequence helpString) {
        this.update("Fingerprint Authentication help\n" + helpString, false);
    }


    @Override
    public void onAuthenticationFailed() {
        this.update("Fingerprint Authentication failed.", false);
    }


    @Override
    public void onAuthenticationSucceeded(FingerprintManager.AuthenticationResult result) {
        this.update("Fingerprint Authentication succeeded.", true);
    }


    private void update(String e, Boolean success){
       /* TextView textView = (TextView) ((Activity)context).findViewById(R.id.errorText);
        textView.setText(e);*/
        //Toast.makeText(context, e, Toast.LENGTH_SHORT).show();
        if(success){
            //Toast.makeText(context, "success", Toast.LENGTH_SHORT).show();

            if (loginType.equalsIgnoreCase(MyConstants.USER_PT)) {
                ((PTLoginActivity) context).fingerprintLoginCall();
            } else {
                ((DRLoginActivity) context).fingerprintLoginCall();
            }
        }
    }
}
