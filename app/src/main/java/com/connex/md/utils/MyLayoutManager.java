package com.connex.md.utils;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

public class MyLayoutManager extends LinearLayoutManager {

    private static final float MILLISECONDS_PER_INCH = 50f;
    private Context mContext;

    public MyLayoutManager(Context context) {
        super(context, LinearLayoutManager.VERTICAL, false);
        mContext = context;
    }

    @Override
    public void smoothScrollToPosition(RecyclerView recyclerView,
                                       final RecyclerView.State state, final int position) {

        int fcvip = findFirstCompletelyVisibleItemPosition();
        int lcvip = findLastCompletelyVisibleItemPosition();

        if (position < fcvip || lcvip < position) {
            // scrolling to invisible position
            try {
                float fcviY = findViewByPosition(fcvip).getY();
                float lcviY = findViewByPosition(lcvip).getY();

                recyclerView.setOnScrollListener(new RecyclerView.OnScrollListener() {

                    int currentState = RecyclerView.SCROLL_STATE_IDLE;

                    @Override
                    public void onScrollStateChanged(RecyclerView recyclerView, int newState) {

                        if (currentState == RecyclerView.SCROLL_STATE_SETTLING
                                && newState == RecyclerView.SCROLL_STATE_IDLE) {

                            // recursive scrolling
                            smoothScrollToPosition(recyclerView, state, position);
                        }

                        currentState = newState;
                    }

                    @Override
                    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {

                        int fcvip = findFirstCompletelyVisibleItemPosition();
                        int lcvip = findLastCompletelyVisibleItemPosition();

                        if ((dy < 0 && fcvip == position) || (dy > 0 && lcvip == position)) {
                            // stop scrolling
                            recyclerView.setOnScrollListener(null);
                        }
                    }
                });

                if (position < fcvip) {
                    // scroll up

                    recyclerView.smoothScrollBy(0, (int) (fcviY - lcviY));
                } else {
                    // scroll down

                    recyclerView.smoothScrollBy(0, (int) (lcviY - fcviY));
                }
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
        } else {
            // scrolling to visible position

            float fromY = findViewByPosition(fcvip).getY();
            float targetY = findViewByPosition(position).getY();

            recyclerView.smoothScrollBy(0, (int) (targetY - fromY));
        }

        /*LinearSmoothScroller smoothScroller =
                new LinearSmoothScroller(mContext) {

                    //This controls the direction in which smoothScroll looks
                    //for your view
                    @Override
                    public PointF computeScrollVectorForPosition
                    (int targetPosition) {
                        return MyLayoutManager.this
                                .computeScrollVectorForPosition(targetPosition);
                    }

                    //This returns the milliseconds it takes to
                    //scroll one pixel.
                    @Override
                    protected float calculateSpeedPerPixel
                    (DisplayMetrics displayMetrics) {
                        return MILLISECONDS_PER_INCH/displayMetrics.densityDpi;
                    }
                };

        smoothScroller.setTargetPosition(position);
        startSmoothScroll(smoothScroller);*/
    }
}