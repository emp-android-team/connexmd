package com.connex.md.ws;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.text.TextUtils;


import com.connex.md.others.App;

import java.util.HashMap;
import java.util.Map;

public class CallRequest {

    public App app;
    public Context ct;
    public Fragment ft;
    public static String version = "1";

    public CallRequest(Fragment ft) {
        app = App.getInstance();
        this.ft = ft;
    }

    public CallRequest(Context ct) {
        this.ct = ct;
        app = App.getInstance();
    }

    public CallRequest() {
    }

    public void updatePatientProfile(Map<String, String> map, boolean withImage) {
        Utils.printMap(map);
        if (!withImage) {
            new AsyncHttpRequest(ft, Constant.REQUESTS.updatePatientProfile, Constant.POST_TYPE.POST, map);
        } else {
            new AsyncHttpRequest(ft, Constant.REQUESTS.updatePatientProfile, Constant.POST_TYPE.POST_WITH_IMAGE, map);
        }
    }

    public void addNote(Map<String, String> map) {
        new AsyncHttpRequest(ct, Constant.REQUESTS.addNote, Constant.POST_TYPE.POST_WITH_IMAGE, map);
    }

    public void displayNotes(Map<String, String> map) {
        new AsyncHttpRequest(ct, Constant.REQUESTS.displayNote, Constant.POST_TYPE.POST, map);
    }

    public void addAllergy(Map<String, String> map) {
        new AsyncHttpRequest(ft, Constant.REQUESTS.addAllergy, Constant.POST_TYPE.POST, map);
    }

    public void deleteAllergy(Map<String, String> map) {
        new AsyncHttpRequest(ft, Constant.REQUESTS.deleteAllergy, Constant.POST_TYPE.POST, map);
    }

    public void addMedicine(Map<String, String> map) {
        new AsyncHttpRequest(ft, Constant.REQUESTS.addMedicine, Constant.POST_TYPE.POST, map);
    }

    public void updateMedicine(Map<String, String> map) {
        new AsyncHttpRequest(ft, Constant.REQUESTS.updateMedicine, Constant.POST_TYPE.POST, map);
    }

    public void addReport(Map<String, String> map) {
        new AsyncHttpRequest(ft, Constant.REQUESTS.addReport, Constant.POST_TYPE.POST_WITH_IMAGE, map);
    }

    public void deleteReport(Map<String, String> map) {
        new AsyncHttpRequest(ft, Constant.REQUESTS.deleteReport, Constant.POST_TYPE.POST, map);
    }

    public void getSuggestDoctors(Map<String, String> map) {
        new AsyncHttpRequest(ct, Constant.REQUESTS.suggestDoctors, Constant.POST_TYPE.POST, map);
    }

    public void voteNow(Map<String, String> map) {
        new AsyncHttpRequest(ct, Constant.REQUESTS.voteNow, Constant.POST_TYPE.POST, map);
    }

    public void sendOfflineNotification(String message, String sender_id, String reciever_id, String user_type, String social_id, String social_type, String is_guest, boolean isShow) {

        Map<String, String> map = new HashMap<String, String>();
        map.put("url", MyConstants.NOTIFICATION_URL + "offlineNotification");
        map.put("Message", message);
        map.put("SenderId", sender_id);
        map.put("ReceiverId", reciever_id);
        map.put("SocialId", social_id);
        map.put("SocialType", social_type);
        map.put("SenderType", user_type);
        if (App.user.getUser_Type().equalsIgnoreCase(MyConstants.USER_DR)) {
            map.put("IsGuest", is_guest);
        }
        //map.put("ApiToken", MyConstants.API_TOKEN);
        map.put("Version", MyConstants.WS_VERSION);
        if (isShow) {
            map.put("Type", "1");
        }
        map.put("show", "");
        new AsyncHttpRequest(ct, Constant.REQUESTS.offlineNotification, Constant.POST_TYPE.POST, map);
    }

    public void sendOfflineNotificationDoctor(String message, String sender_id, String reciever_id) {

        Map<String, String> map = new HashMap<String, String>();
        map.put("url", MyConstants.NOTIFICATION_URL + "doctorOfflineNotification");
        map.put("Message", message);
        map.put("SenderId", sender_id);
        map.put("ReceiverId", reciever_id);
        map.put("Version", MyConstants.WS_VERSION);
        map.put("show", "");
        new AsyncHttpRequest(ct, Constant.REQUESTS.offlineNotification, Constant.POST_TYPE.POST, map);
    }

    public void sendOfflineNotificationNurse(String message, String sender_id, String reciever_id, String user_type, String social_id, String social_type) {
        Map<String, String> map = new HashMap<String, String>();

        map.put("url", MyConstants.NOTIFICATION_URL + "nurseOfflineNotification");
        map.put("Message", message);
        map.put("SenderId", sender_id);
        map.put("ReceiverId", reciever_id);
        map.put("SocialId", social_id);
        map.put("SocialType", social_type);
        map.put("SenderType", user_type);
        //map.put("ApiToken", MyConstants.API_TOKEN);
        map.put("Version", MyConstants.WS_VERSION);

        map.put("show", "");
        new AsyncHttpRequest(ct, Constant.REQUESTS.offlineNotification, Constant.POST_TYPE.POST, map);
    }

    public void sendOfflineNotificationSpecialityDoctor(Map<String, String> map) {
        new AsyncHttpRequest(ct, Constant.REQUESTS.offlineNotification, Constant.POST_TYPE.POST, map);
    }

    public void sendOfflineNurseNotification(String message, String sender_id, String reciever_id, String user_type) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", MyConstants.BASE_URL + "offline_nurse_notification.php?");
        map.put("message", message);
        map.put("sender_id", sender_id.replace(" ", ""));
        map.put("reciever_id", reciever_id.replace(" ", ""));
        map.put("user_type", user_type);

        map.put("show", "");
        new AsyncHttpRequest(ct, Constant.REQUESTS.offlineNotification, Constant.POST_TYPE.POST, map);
    }

    public void createBookingPayment(Map<String, String> map) {
        new AsyncHttpRequest(ct, Constant.REQUESTS.createBooking, Constant.POST_TYPE.POST, map);
    }

    public void changePassword(Map<String, String> map) {
        new AsyncHttpRequest(ct, Constant.REQUESTS.changePassword, Constant.POST_TYPE.POST, map);
    }

    public void guestLogin(Map<String, String> map) {
        new AsyncHttpRequest(ct, Constant.REQUESTS.guestLogin, Constant.POST_TYPE.POST, map);
    }

    public void patientLogin(String email, String password) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", MyConstants.BASE_URL + "login");
        map.put("email", email);
        map.put("password", password);
        map.put("Version", MyConstants.WS_VERSION);
        map.put("DeviceType", MyConstants.DEVICE_TYPE);
        map.put("DeviceToken", MyConstants.DEVICE_ID);
        new AsyncHttpRequest(ct, Constant.REQUESTS.patientLogin, Constant.POST_TYPE.POST, map);

    }

    public void doctorLogin(String email, String password) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", MyConstants.DOCTOR_BASE_URL + "login");
        map.put("email", email);
        map.put("password", password);
        map.put("DeviceType", MyConstants.DEVICE_TYPE);
        map.put("DeviceToken", MyConstants.DEVICE_ID);
        map.put("Version", MyConstants.WS_VERSION);
        new AsyncHttpRequest(ct, Constant.REQUESTS.doctorLogin, Constant.POST_TYPE.POST, map);

    }

    public void getChatHistory(Map<String, String> map) {
        new AsyncHttpRequest(ft, Constant.REQUESTS.chatHistory, Constant.POST_TYPE.POST, map);
    }

    public void changeNurse(Map<String, String> map) {
        new AsyncHttpRequest(ct, Constant.REQUESTS.changeNurse, Constant.POST_TYPE.POST, map);
    }

    public void getWalletHistory(Map<String, String> map) {
        new AsyncHttpRequest(ct, Constant.REQUESTS.walletHistory, Constant.POST_TYPE.POST, map);
    }

    public void checkCancelStatus(String bookingID, String type, String user_id) {

        Map<String, String> map = new HashMap<String, String>();
        if (type.equalsIgnoreCase(MyConstants.USER_PT)) {
            if (MyConstants.isGuest.equalsIgnoreCase("1")) {
                map.put("url", MyConstants.GUEST_BASE_URL + "checkConsult");
                map.put("GuestId", user_id);
            } else {
                map.put("url", MyConstants.BASE_URL + "checkConsult");
                map.put("UserId", user_id);
            }
        } else {
            map.put("url", MyConstants.DOCTOR_BASE_URL + "checkConsult");
            map.put("DoctorId", user_id);
        }
        map.put("ApiToken", MyConstants.API_TOKEN);
        map.put("Version", MyConstants.WS_VERSION);
        map.put("UniqueId", bookingID);

        new AsyncHttpRequest(ct, Constant.REQUESTS.checkCancelStatus, Constant.POST_TYPE.POST, map);

    }

    public void getDoctorProfile(Map<String, String> map) {
        if (ft != null) {
            new AsyncHttpRequest(ft, Constant.REQUESTS.getDoctorProfile, Constant.POST_TYPE.POST, map);
        } else {
            new AsyncHttpRequest(ct, Constant.REQUESTS.getDoctorProfile, Constant.POST_TYPE.POST, map);
        }

    }

    public void giveRating(Map<String, String> map) {
        new AsyncHttpRequest(ct, Constant.REQUESTS.rate_doctor, Constant.POST_TYPE.POST, map);
    }

    public void getDRProfile(Map<String, String> map) {
        new AsyncHttpRequest(ft, Constant.REQUESTS.getDoctorProfile, Constant.POST_TYPE.POST, map);
    }

    public void saveDoctorDetails(Map<String, String> map) {
        new AsyncHttpRequest(ft, Constant.REQUESTS.saveDoctorDetails, Constant.POST_TYPE.POST_WITH_IMAGE, map);
    }

    public void editDoctorDetails(Map<String, String> map) {
        new AsyncHttpRequest(ft, Constant.REQUESTS.editDoctorDetails, Constant.POST_TYPE.POST_WITH_IMAGE, map);
    }

    public void saveRemoveDoctor(Map<String, String> map) {
        new AsyncHttpRequest(ct, Constant.REQUESTS.saveDoctor, Constant.POST_TYPE.POST, map);
    }

    public void saveRemoveCategory(Map<String, String> map) {
        new AsyncHttpRequest(ft, Constant.REQUESTS.saveCategory, Constant.POST_TYPE.POST, map);
    }

    public void getProfilData(Map<String, String> map) {
        new AsyncHttpRequest(ft, Constant.REQUESTS.getProfile, Constant.POST_TYPE.POST, map);
    }

    public void changeDoctor(Map<String, String> map) {
        new AsyncHttpRequest(ct, Constant.REQUESTS.changeDoctor, Constant.POST_TYPE.POST, map);
    }

    public void getIpToLocation() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", "http://ip-api.com/json");
        map.put("ip", "");

        new AsyncHttpRequest(ct, Constant.REQUESTS.getIP_LOCATION, Constant.POST_TYPE.GET, map);
    }

    public void registerPatient(Map<String, String> map) {
        new AsyncHttpRequest(ct, Constant.REQUESTS.register, Constant.POST_TYPE.POST, map);
    }

    public void submitQuestionnaire(Map<String, String> map) {
        new AsyncHttpRequest(ct, Constant.REQUESTS.submitQuestionnaire, Constant.POST_TYPE.POST_WITH_IMAGE, map);
    }

    public void verifyAccount(Map<String, String> map) {
        new AsyncHttpRequest(ct, Constant.REQUESTS.verifyAccount, Constant.POST_TYPE.POST, map);
    }

    public void isUserVerified(Map<String, String> map) {
        new AsyncHttpRequest(ct, Constant.REQUESTS.isVerified, Constant.POST_TYPE.POST, map);
    }

    public void sendEmail(Map<String, String> map) {
        new AsyncHttpRequest(ct, Constant.REQUESTS.sendEmail, Constant.POST_TYPE.POST, map);
    }

    public void resetPassword(Map<String, String> map) {
        new AsyncHttpRequest(ct, Constant.REQUESTS.resetPassword, Constant.POST_TYPE.POST, map);
    }

    public void sendResetPassword(Map<String, String> map) {
        new AsyncHttpRequest(ct, Constant.REQUESTS.sendResetPassword, Constant.POST_TYPE.POST, map);
    }

    public void signupPatientFb(Map<String, String> map) {
        new AsyncHttpRequest(ct, Constant.REQUESTS.singup_patient_fb, Constant.POST_TYPE.POST, map);
    }

    public void bookingHistory(Map<String, String> map) {
        new AsyncHttpRequest(ft, Constant.REQUESTS.getAllBookings, Constant.POST_TYPE.POST, map);
    }

    public void doctorNotesHistory(Map<String, String> map) {
        new AsyncHttpRequest(ct, Constant.REQUESTS.doctorNoteHistory, Constant.POST_TYPE.POST, map);
    }

    public void getMyBookmarks() {

        Map<String, String> map = new HashMap<String, String>();
        map.put("url", MyConstants.BASE_URL + "speciality");
        map.put("Version", version);
        map.put("IsGuest", MyConstants.isGuest);
        map.put("ListBookmark", "1");

//        if (sharedpreferences.getBoolean(MyConstants.IS_LOGGED_IN, false)) {
        if (!TextUtils.isEmpty(App.user.getUserID())) {
            if (MyConstants.isGuest.equalsIgnoreCase("0") && App.user.getUser_Type().equalsIgnoreCase(MyConstants.USER_PT)) {
                map.put("UserId", App.user.getUserID());
                map.put("Latitude", MyConstants.Latitude);
                map.put("Longitude", MyConstants.Longitude);
                /*map.put("Latitude", "46.890804");
                map.put("Longitude","-71.1498727");*/
            } else if (MyConstants.isGuest.equalsIgnoreCase("0") && App.user.getUser_Type().equalsIgnoreCase(MyConstants.USER_DR)) {
                map.put("DoctorId", App.user.getUserID());
                map.put("Latitude", "");
                map.put("Longitude", "");
            } else {
                map.put("UserId", "0");
                map.put("Latitude", "");
                map.put("Longitude", "");
            }
        } else {
            map.put("UserId", "0");
            map.put("Latitude", "");
            map.put("Longitude", "");
        }

        new AsyncHttpRequest(ct, Constant.REQUESTS.myBookmarks, Constant.POST_TYPE.POST, map);
    }

    public void bookingHistoryDoctor(Map<String, String> map) {
        new AsyncHttpRequest(ft, Constant.REQUESTS.bookingHistoryDoctor, Constant.POST_TYPE.POST, map);
    }

    public void doctorList(Map<String, String> map) {
        new AsyncHttpRequest(ct, Constant.REQUESTS.doctorList, Constant.POST_TYPE.POST, map);
    }

    public void doctorListFragment(Map<String, String> map) {
        new AsyncHttpRequest(ft, Constant.REQUESTS.doctorList, Constant.POST_TYPE.POST, map);
    }

    public void getSymptoms() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", MyConstants.BASE_URL + "symptoms");
        map.put("Version", MyConstants.WS_VERSION);
        new AsyncHttpRequest(ct, Constant.REQUESTS.getSymptoms, Constant.POST_TYPE.POST, map);
    }

    public void setSymptoms(Map<String, String> map) {
        new AsyncHttpRequest(ct, Constant.REQUESTS.setSymptoms, Constant.POST_TYPE.POST, map);
    }

    public void setSymptomsFragment(Map<String, String> map) {
        new AsyncHttpRequest(ft, Constant.REQUESTS.setSymptoms, Constant.POST_TYPE.POST, map);
    }

    public void deleteSymptoms(Map<String, String> map) {
        new AsyncHttpRequest(ft, Constant.REQUESTS.deleteSymptoms, Constant.POST_TYPE.POST, map);
    }

    public void createBooking(Map<String, String> map) {
        new AsyncHttpRequest(ct, Constant.REQUESTS.createBooking, Constant.POST_TYPE.POST_WITH_IMAGE, map);
    }

    public void getDoctorNotesCount(Map<String, String> map) {
        new AsyncHttpRequest(ft, Constant.REQUESTS.doctorNoteCount, Constant.POST_TYPE.POST, map);
    }

    public void setDeviceToken() {
        Map<String, String> map = new HashMap<String, String>();
        if (App.user.getUser_Type().equalsIgnoreCase(MyConstants.USER_PT) && MyConstants.isGuest.equalsIgnoreCase("0")) {
            map.put("url", MyConstants.BASE_URL + "updateDeviceToken");
            map.put("UserId", App.user.getUserID());
        } else if (App.user.getUser_Type().equalsIgnoreCase(MyConstants.USER_PT) && MyConstants.isGuest.equalsIgnoreCase("1")) {
            map.put("url", MyConstants.GUEST_BASE_URL + "updateDeviceToken");
            map.put("GuestId", App.user.getUserID());
        } else if (App.user.getUser_Type().equalsIgnoreCase(MyConstants.USER_DR) && MyConstants.isGuest.equalsIgnoreCase("0")) {
            map.put("url", MyConstants.DOCTOR_BASE_URL + "updateDeviceToken");
            map.put("DoctorId", App.user.getUserID());
        }
        map.put("Version", version);
        map.put("ApiToken", MyConstants.API_TOKEN);
        map.put("DeviceToken", MyConstants.DEVICE_ID);
        map.put("DeviceType", MyConstants.DEVICE_TYPE);

        new AsyncHttpRequest(ft, Constant.REQUESTS.setDeviceToken, Constant.POST_TYPE.POST, map);
    }

    public void getSpecialities() {
        Map<String, String> map = new HashMap<>();
        map.put("url", MyConstants.BASE_URL + "speciality");
        map.put("Version", version);
        map.put("IsGuest", MyConstants.isGuest);

//        if (sharedpreferences.getBoolean(MyConstants.IS_LOGGED_IN, false)) {
        if (!TextUtils.isEmpty(App.user.getUserID())) {
            if (MyConstants.isGuest.equalsIgnoreCase("0") && App.user.getUser_Type().equalsIgnoreCase(MyConstants.USER_PT)) {
                map.put("UserId", App.user.getUserID());
                map.put("Latitude", MyConstants.Latitude);
                map.put("Longitude", MyConstants.Longitude);
                /*map.put("Latitude", "46.890804");
                map.put("Longitude","-71.1498727");*/
            } else if (MyConstants.isGuest.equalsIgnoreCase("0") && App.user.getUser_Type().equalsIgnoreCase(MyConstants.USER_DR)) {
                map.put("DoctorId", App.user.getUserID());
                map.put("Latitude", MyConstants.Latitude);
                map.put("Longitude", MyConstants.Longitude);
            } else {
                map.put("UserId", "0");
                map.put("Latitude", MyConstants.Latitude);
                map.put("Longitude", MyConstants.Longitude);
            }
        } else {
            map.put("UserId", "0");
            map.put("Latitude", MyConstants.Latitude);
            map.put("Longitude", MyConstants.Longitude);
        }
//        }
        new AsyncHttpRequest(ft, Constant.REQUESTS.getSpecialities, Constant.POST_TYPE.POST, map);
    }

    public void getReferredDiscount(String UserID) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", MyConstants.BASE_URL + "getReferredDiscount");
        map.put("ApiToken", MyConstants.API_TOKEN);
        map.put("UserId", UserID);
        map.put("Version", version);
        new AsyncHttpRequest(ct, Constant.REQUESTS.getReferredDiscount, Constant.POST_TYPE.POST, map);
    }

    public void endConsult(String senderId, String uniqueChatId) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", MyConstants.DOCTOR_BASE_URL + "endConsult");
        map.put("ApiToken", MyConstants.API_TOKEN);
        map.put("DoctorId", senderId);
        map.put("UniqueId", uniqueChatId);
        map.put("Version", version);
        new AsyncHttpRequest(ct, Constant.REQUESTS.cancelChat, Constant.POST_TYPE.POST, map);
    }

    public void getNurseChatDetailsFragment(Map<String, String> map) {
        new AsyncHttpRequest(ft, Constant.REQUESTS.nurseChatDetails, Constant.POST_TYPE.POST, map);
    }

    public void getNurseChatDetails(Map<String, String> map) {
        new AsyncHttpRequest(ct, Constant.REQUESTS.nurseChatDetails, Constant.POST_TYPE.POST, map);
    }

    public void getDoctorChatDetails(Map<String, String> map) {
        new AsyncHttpRequest(ct, Constant.REQUESTS.doctorChatDetails, Constant.POST_TYPE.POST, map);
    }

    public void getReferData(Map<String, String> map) {
        new AsyncHttpRequest(ft, Constant.REQUESTS.referData, Constant.POST_TYPE.POST, map);
    }

    public void getMonthlySlots(Map<String, String> map) {
        new AsyncHttpRequest(ct, Constant.REQUESTS.getMonthlySlots, Constant.POST_TYPE.POST, map);
    }

    public void getDailySlots(Map<String, String> map) {
        new AsyncHttpRequest(ct, Constant.REQUESTS.getDailySlots, Constant.POST_TYPE.POST, map);
    }

    public void bookRxDelivery(Map<String, String> map) {
        new AsyncHttpRequest(ct, Constant.REQUESTS.bookRxDelivery, Constant.POST_TYPE.POST_WITH_IMAGE, map);
    }

    public void bookInPersonConsult(Map<String, String> map) {
        new AsyncHttpRequest(ct, Constant.REQUESTS.bookInPersonConsult, Constant.POST_TYPE.POST, map);
    }

    public void requestForInformation(Map<String, String> map) {
        new AsyncHttpRequest(ct, Constant.REQUESTS.requestForInformation, Constant.POST_TYPE.POST, map);
    }

    public void inPersonVisitHistory(Map<String, String> map) {
        new AsyncHttpRequest(ct, Constant.REQUESTS.inPersonVisitHistory, Constant.POST_TYPE.POST, map);
    }
}