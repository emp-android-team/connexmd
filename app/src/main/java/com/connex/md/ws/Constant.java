package com.connex.md.ws;

public class Constant {



    public enum POST_TYPE {
        GET, POST, POST_WITH_IMAGE
    }


    public enum TABS {
        DR_LIST, HISTORY, PROFILE, MESSAGES, HEALTH, NOTIFICATIONS

    }

    public enum REQUESTS {
        offlineNotification, singup_patient_fb, getProfile, cancelBookingFromUser, doctorLogin, bookingHistoryDoctor, register,
        getCountry, getZone, getDoctorProfile, updateDevice, getIP_LOCATION, resetPasswordOTP, verifyAccount, isVerified, submitQuestionnaire,
        updateBadge, get_refferalCode, getDiscounts, updateRefer, patientLogin, guestLogin, saveDoctor, referData, saveCategory,
        chatHistory, cancelChat, setOnOffStatus, getOnOffStatus, updateDoctorProfile, rate_doctor, nurseChatDetails, saveDoctorDetails,
        updatePatientProfile, checkCancelStatus, createBooking, GET_MORNING_SLOT, GET_EVENING_SLOT, getReferredDiscount, editDoctorDetails,
        uploadChatImage, uploadChatVideo, uploadChatAudio, checkTimeOver, acceptReject, getSymptoms, sendResetPassword, walletHistory, doctorNoteCount, doctorNoteHistory,
        resetPassword, bookingHistory, getPatientProfile, getPatientProfileActivity, doctorList, historyDoctorBooking, doctorChatDetails,
        getDoctorProfileFragment, doctorListActivity, getSpecialities, changePassword, setSymptoms, changeNurse, suggestDoctors, changeDoctor, myBookmarks,
        addAllergy, deleteAllergy, addMedicine, updateMedicine, addReport, deleteReport, deleteSymptoms, addNote, displayNote, setDeviceToken, voteNow, sendEmail,
        getMonthlySlots, getDailySlots, bookRxDelivery, bookInPersonConsult, requestForInformation, inPersonVisitHistory, getAllBookings
    }

    /**
     * This is for PUSH Notification
     **/

}