package com.connex.md.ws;

import com.connex.md.model.BookingHistory;
import com.connex.md.model.DoctorProfile;
import com.connex.md.model.MessageHistory;
import com.connex.md.model.Specialities;
import com.connex.md.model.Symptoms;
import com.connex.md.model.UserProfile;
import com.connex.md.model.WalletHistory;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class MyConstants {

    // test channels
    /*public static final String CHANNEL = "test_channels";
    public static final String NURSE_CHANNEL = "nurse_test_channels";
    public static final String DOCTOR_CHANNEL = "doctor_test_channels";

    //test URLs
    public static final String MAIN_URL = "https://test-api.doctorpocket.ca/";
    //public static final String MAIN_URL = "http://test-api.venuapp.club/";
    public static final String BASE_URL = MAIN_URL + "api/v1/";
    public static final String DOCTOR_BASE_URL = MAIN_URL + "api/v1/doctor/";
    public static final String GUEST_BASE_URL = MAIN_URL + "api/v1/guest/";
    public static final String NURSE_BASE_URL = MAIN_URL + "api/v1/nurse/";
    public static final String NOTIFICATION_URL = MAIN_URL + "api/v1/notif/";*/

    // live channels
    public static final String CHANNEL = "live_channels";
    public static final String NURSE_CHANNEL = "nurse_live_channels";
    public static final String DOCTOR_CHANNEL = "doctor_live_channels";

    // live URLs
    public static final String MAIN_URL = "https://connex-api.doctorpocket.ca/";
    public static final String BASE_URL = MAIN_URL + "api/v2/";
    public static final String DOCTOR_BASE_URL = MAIN_URL + "api/v2/doctor/";
    public static final String GUEST_BASE_URL = MAIN_URL + "api/v2/guest/";
    public static final String NOTIFICATION_URL = MAIN_URL + "api/v2/notif/";

    // sinch live credentials
    public static final String APP_KEY = "aece00ac-0dd3-4bcb-935b-1cec9548f6f7";
    public static final String APP_SECRET = "+sWJd0rZsEGl0H4yoMzPpQ==";
    public static final String ENVIRONMENT = "clientapi.sinch.com";

    // sinch test credentials
    //private static final String ENVIRONMENT = "sandbox.sinch.com";

    public static String doctorIdForQuestionnaire = "6";
    //public static String doctorIdForQuestionnaire = "9";


    // for  Stripe
    //0--->Test key
    //1--->Production key
    public static String STRIPE_CONFIG_ENVIRONMENT = "1";
    public static String STRIPE_PUBLISH_KEY_LIVE = "";
    public static String STRIPE_PUBLISH_KEY_TEST = "pk_test_cnfziT3fFHz6bOVyoEdi6Deo00CnPSCEpa";
    public static String STRIPE_SECRET_KEY_LIVE = "";
    public static String STRIPE_SECRET_KEY_TEST = "sk_test_mQ41zqUXxRGLO6WKA2sr5fqy00uzSV4gmk";
    public static String STRIPE_MERCHANT_NAME = "ConnexMD Payment";

    //ws version
    public static final String WS_VERSION = "2";
    // device type = 1 for android
    public static final String DEVICE_TYPE = "1";
    public static final int REQUEST_STRIPE_PAYMENT = 2000;
    public static final String CONSULT_CHARGE = "consult_charge";
    public static final String IS_GUEST = "is_guest";
    public static final String PT_PASSWORD = "pt_password";
    public static final String DR_PASSWORD = "dr_password";
    public static final String LOGIN_TYPE = "login_type";
    public static final String FIRST_LOGIN = "first_login";
    public static final String IS_FIRST = "first";
    public static final String IS_INTRO_SEEN = "first";
    public static final String USER_TYPE = "user_type";
    public static final String USER_ID = "user_id";
    public static final String PREF = "Doctor_Pocket";
    public static final String USER_PT = "user_PT";
    public static final String USER_DR = "user_DR";
    public static final String USER_EMAIL = "user_email";
    public static final String PT_ZONE = "patient_zone";
    public static final String PT_NAME = "NAME";
    public static final String PT_FIRST_NAME = "first_name";
    public static final String PT_LAST_NAME = "last_name";
    public static final String PROFILE_PIC = "profile_pic";
    public static final String FREE_CONSULT = "free_consult";
    public static final String SINCH_ID = "sinch_id";
    public static final String IS_LOGGED_IN = "is_logged_in";
    public static final String SOCIAL_ID = "social_id";
    public static final String COUNTER = "counter";
    public static final String GENDER = "gender";
    public static final String FACEBOOK_ID = "facebookid";
    public static final String MOBILE = "mobile";
    public static final String ZONE = "zone";
    public static final String DATE_OF_BIRTH = "DOB";
    public static final String STATE = "state";
    public static final String COUNTRY = "country";
    public static final String ADDRESS = "address";
    public static final String AGE = "age";
    public static final String CURRENT_DISCOUNT = "current_discount";
    public static final String WALLET_BALANCE = "wallet_balance";
    public static final String REFER_CODE = "refer_code";
    public static final String TOKEN = "token";
    public static final String UNIQUE_CHAT_ID = "unique_chat_id";
    public static final String DR_ID = "dr_id";
    public static final String PT_ID = "pt_id";
    public static final String DR_NAME = "dr_name";
    public static final String Pt_NAME = "pt_name";
    public static final String RECEIVER_IMAGE_URL = "receiver_image_url";
    public static final String IS_PAYMENT_DONE = "is_payment_done";
    public static final String STRIPE_ID = "stripe_id";
    public static final String BADGE_COUNT = "badgeCount";
    public static boolean CHAT_AS_GUEST = false;
    public static String DRAWING_PATH = "";
    // device id
    public static String DEVICE_ID = "";
    public static String API_TOKEN = "";
    public static boolean homeFragment = false;

    public static JSONObject questionnaire = new JSONObject();
    public static String headerPrice = "";
    public static String Latitude;
    public static String Longitude;
    public static int TAB_POSITION;
    public static String doctorLastName;
    public static DoctorProfile doctorProfile;
    public static DoctorProfile editDoctorProfile;
    public static UserProfile editPatientProfile;
    public static String uniqueChatId = "";
    public static String consultId = "";
    public static String isFree = "";
    public static String paymentType = "";
    public static String DOCTOR_NAME = "";
    public static String DOCTOR_ID = "";
    public static String DOCTOR_PROFILEPIC = "";
    public static String Wallet_Balance = "0";
    public static String isGuest = "0";
    public static boolean isBooked = false;
    public static boolean isBackPressed = false;
    public static boolean isLoadAgain = false;
    public static boolean isBookingHistoryLoad = false;
    public static boolean isMessageHistoryLoad = false;
    public static boolean isProfileLoad = false;
    public static boolean isDoctorProfileLoad = false;
    public static boolean isSuggestDoctor = false;
    public static ArrayList<ArrayList<Symptoms>> symptomsArray;
    public static ArrayList<Specialities> specialitiesList = new ArrayList<>();
    public static List<BookingHistory> bookingHistoryList = new ArrayList<>();
    public static List<MessageHistory> messageHistoryList = new ArrayList<>();
    public static List<MessageHistory> userChatList = new ArrayList<>();
    public static List<MessageHistory> doctorChatList = new ArrayList<>();
    public static UserProfile userProfile;
    public static DoctorProfile doctorProfilePersonal;
    public static String walletBalance;
    public static List<WalletHistory> walletPaidList = new ArrayList<>();
    public static List<WalletHistory> walletReceivedList = new ArrayList<>();


    public static String MEDIA_RECORD_ERROR = "-38";

    public static final String CALL_ID = "CALL_ID";

    /*Audio Video Constants*/

    public static final int VOICE_CALL_REQUEST_CODE = 888;
    public static final int VIDEO_CALL_REQUEST_CODE = 999;
}
