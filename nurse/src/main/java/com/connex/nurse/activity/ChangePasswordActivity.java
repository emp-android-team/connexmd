package com.connex.nurse.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.connex.nurse.R;
import com.connex.nurse.interfaces.AsyncTaskListner;
import com.connex.nurse.others.App;
import com.connex.nurse.ws.CallRequest;
import com.connex.nurse.ws.Constant;
import com.connex.nurse.ws.MyConstants;
import com.connex.nurse.ws.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ChangePasswordActivity extends AppCompatActivity implements AsyncTaskListner {

    EditText etOldPassword, etNewPassword, etConfirmPassword;
    Button btnChangePassword, btnCancel;
    SharedPreferences sharedpreferences;
    SharedPreferences.Editor editor;
    String newPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);

        sharedpreferences = getSharedPreferences(MyConstants.PREF, MODE_PRIVATE);

        etOldPassword = findViewById(R.id.etOldPassword);
        etNewPassword = findViewById(R.id.etNewPassword);
        etConfirmPassword = findViewById(R.id.etConfirmPassword);
        btnChangePassword = findViewById(R.id.btnChangePassword);
        btnCancel = findViewById(R.id.btnCancel);

        btnChangePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callChangePasswordAPI();
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

    }

    private void callChangePasswordAPI() {

        String currentPassword = etOldPassword.getText().toString().trim();
        newPassword = etNewPassword.getText().toString().trim();
        String confirmPassword = etConfirmPassword.getText().toString().trim();

        if (TextUtils.isEmpty(currentPassword)) {
            etOldPassword.requestFocus();
            etOldPassword.setError("Enter old password");
            return;
        } else if (TextUtils.isEmpty(newPassword)) {
            etNewPassword.requestFocus();
            etNewPassword.setError("Enter new password");
            return;
        } else if (TextUtils.isEmpty(confirmPassword)) {
            etConfirmPassword.requestFocus();
            etConfirmPassword.setError("Enter confirm password");
            return;
        }

        Map<String, String> map = new HashMap<String, String>();

        map.put("url", MyConstants.NURSE_BASE_URL + "changePassword");
        map.put("NurseId", App.user.getUserID());
        map.put("ApiToken", MyConstants.API_TOKEN);
        map.put("Version", MyConstants.WS_VERSION);
        map.put("CurrentPassword", currentPassword);
        map.put("password", newPassword);
        map.put("ConfirmPassword", confirmPassword);

        new CallRequest(ChangePasswordActivity.this).changePassword(map);
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        Utils.removeSimpleSpinProgressDialog();
        try {
            if (result != null && !result.isEmpty()) {
                Log.i("TAG", "TAG Result : " + result);
                //      {"document":{"response":{"status":1,"message":"Success."}}}
                switch (request) {
                    case changePassword:
                        try {
                            JSONObject mainObj = new JSONObject(result);
                            if (mainObj.getString("error_code").equalsIgnoreCase("0")) {
                                Utils.showToast(mainObj.getString("error_string"), ChangePasswordActivity.this);
                                etOldPassword.setText("");
                                etNewPassword.setText("");
                                etConfirmPassword.setText("");
                                //Utils.showToast("Password changed successfully", ChangePasswordActivity.this);

                                if (!TextUtils.isEmpty(sharedpreferences.getString(MyConstants.NURSE_PASSWORD, ""))) {
                                    editor = sharedpreferences.edit();
                                    editor.putString(MyConstants.NURSE_PASSWORD, newPassword);
                                    editor.commit();
                                }

                                Intent intent = new Intent(ChangePasswordActivity.this, NurseDashboardActivity.class);
                                MyConstants.isBackPressed = true;
                                startActivity(intent);
                                ActivityCompat.finishAffinity(ChangePasswordActivity.this);

                            } else if (mainObj.getString("error_code").equalsIgnoreCase("1") ||
                                    mainObj.getString("error_code").equalsIgnoreCase("2")) {
                                editor = sharedpreferences.edit();
                                editor.clear();
                                editor.commit();

                                Utils.showToast(mainObj.getString("error_string"), ChangePasswordActivity.this);

                                startActivity(new Intent(ChangePasswordActivity.this, LoginActivity.class));
                                ActivityCompat.finishAffinity(ChangePasswordActivity.this);
                            } else {
                                Utils.showToast(mainObj.getString("error_string"), ChangePasswordActivity.this);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onProgressUpdate(String uniqueMessageId, int progres) {

    }

    @Override
    public void onProgressComplete(String uniqueMessageId, String result, Constant.REQUESTS request) {

    }
}
