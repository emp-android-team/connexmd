package com.connex.nurse.activity;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.KeyguardManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Build;
import android.security.keystore.KeyGenParameterSpec;
import android.security.keystore.KeyPermanentlyInvalidatedException;
import android.security.keystore.KeyProperties;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


import com.connex.nurse.R;
import com.connex.nurse.interfaces.AsyncTaskListner;
import com.connex.nurse.others.App;
import com.connex.nurse.others.Internet;
import com.connex.nurse.utils.FingerprintHandler;
import com.connex.nurse.ws.CallRequest;
import com.connex.nurse.ws.Constant;
import com.connex.nurse.ws.MyConstants;
import com.connex.nurse.ws.Utils;
import com.onesignal.OneSignal;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;


public class LoginActivity extends AppCompatActivity implements AsyncTaskListner{

    EditText etEmail, etPassword;
    Button btnLogin;
    TextView tvForgotPassword, tvTerms;
    CheckBox cbRememberMe;
    LoginActivity instance;
    String name;
    SharedPreferences sharedpreferences;
    SharedPreferences.Editor editor;
    public static Activity mActivity;
    private boolean isRememberMe = false;
    private KeyStore keyStore;
    // Variable used for storing the key in the Android Keystore container
    private static final String KEY_NAME = "DoctorPocket";
    private Cipher cipher;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        instance = this;
        this.mActivity = this;

        etEmail = findViewById(R.id.etEmail);
        etPassword = findViewById(R.id.etPassword);
        btnLogin = findViewById(R.id.btnLogin);
        tvForgotPassword = findViewById(R.id.tvForgotPassword);
        tvTerms = findViewById(R.id.tvTerms);
        cbRememberMe = findViewById(R.id.cbRememberMe);

        sharedpreferences = getSharedPreferences(MyConstants.PREF, MODE_PRIVATE);

        OneSignal.idsAvailable(new OneSignal.IdsAvailableHandler() {
            @Override
            public void idsAvailable(String userId, String registrationId) {
                if (userId != null)
                    MyConstants.DEVICE_ID = userId;
                Log.d("debug", "User:" + userId);
            /*    if (registrationId != null)
                    MyConstants.DEVICE_ID = registrationId;*/
                Log.d("debug", "registrationId:" + registrationId);
            }
        });

        System.out.println("Device Id:::" + MyConstants.DEVICE_ID);

        if (sharedpreferences.getBoolean(MyConstants.IS_LOGGED_IN,false)){
            Intent intent = new Intent(LoginActivity.this, NurseDashboardActivity.class);
            startActivity(intent);
            finish();
        }

        if (sharedpreferences.getString(MyConstants.USER_TYPE, "").equalsIgnoreCase(MyConstants.USER_NURSE)) {
            if (!TextUtils.isEmpty(sharedpreferences.getString(MyConstants.NURSE_PASSWORD, ""))) {
                if (sharedpreferences.getBoolean(MyConstants.FIRST_LOGIN, false)) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        System.out.println("fingerprint");
                        fingerprintLogin();
                    }
                }
            }
        }

        if (sharedpreferences.getString(MyConstants.USER_TYPE,"").equalsIgnoreCase(MyConstants.USER_NURSE)) {
            if (!TextUtils.isEmpty(sharedpreferences.getString(MyConstants.NURSE_PASSWORD, ""))) {
                etEmail.setText(sharedpreferences.getString(MyConstants.USER_EMAIL, ""));
                etPassword.setText(sharedpreferences.getString(MyConstants.NURSE_PASSWORD, ""));
                cbRememberMe.setChecked(true);
            }
        }

        tvForgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LoginActivity.this, ResetPasswordActivity.class);
                startActivity(intent);
            }
        });

        tvTerms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LoginActivity.this, TermsConditionActivity.class);
                startActivity(intent);
            }
        });

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                doLogin();
            }
        });
    }

    private void fingerprintLogin() {
        // Initializing both Android Keyguard Manager and Fingerprint Manager
        KeyguardManager keyguardManager = (KeyguardManager) getSystemService(KEYGUARD_SERVICE);
        FingerprintManager fingerprintManager = (FingerprintManager) getSystemService(FINGERPRINT_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (fingerprintManager != null) {
                if (!fingerprintManager.isHardwareDetected()) {
                    /**
                     * An error message will be displayed if the device does not contain the fingerprint hardware.
                     * However if you plan to implement a default authentication method,
                     * you can redirect the user to a default authentication activity from here.
                     * Example:
                     * Intent intent = new Intent(this, DefaultAuthenticationActivity.class);
                     * startActivity(intent);
                     */
                    System.out.println("Your Device does not have a Fingerprint Sensor");
                    Toast.makeText(instance, "Your Device does not have a Fingerprint Sensor", Toast.LENGTH_SHORT).show();
                    //textView.setText("Your Device does not have a Fingerprint Sensor");
                } else {
                    // Checks whether fingerprint permission is set on manifest
                    if (ActivityCompat.checkSelfPermission(instance, Manifest.permission.USE_FINGERPRINT) != PackageManager.PERMISSION_GRANTED) {
                        System.out.println("Fingerprint authentication permission not enabled");
                        Toast.makeText(instance, "Fingerprint authentication permission not enabled", Toast.LENGTH_SHORT).show();
                        //textView.setText("Fingerprint authentication permission not enabled");
                    } else {
                        // Check whether at least one fingerprint is registered
                        if (!fingerprintManager.hasEnrolledFingerprints()) {
                            System.out.println("Register at least one fingerprint in Settings");
                            Toast.makeText(instance, "Register at least one fingerprint in Settings", Toast.LENGTH_SHORT).show();
                            //textView.setText("Register at least one fingerprint in Settings");
                        } else {
                            // Checks whether lock screen security is enabled or not
                            if (!keyguardManager.isKeyguardSecure()) {
                                System.out.println("Lock screen security not enabled in Settings");
                                Toast.makeText(instance, "Lock screen security not enabled in Settings", Toast.LENGTH_SHORT).show();
                                //textView.setText("Lock screen security not enabled in Settings");
                            } else {
                                generateKey();

                                if (cipherInit()) {
                                    FingerprintManager.CryptoObject cryptoObject = new FingerprintManager.CryptoObject(cipher);
                                    FingerprintHandler helper = new FingerprintHandler(LoginActivity.this);
                                    helper.startAuth(fingerprintManager, cryptoObject);
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    protected void generateKey() {
        try {
            keyStore = KeyStore.getInstance("AndroidKeyStore");
        } catch (Exception e) {
            e.printStackTrace();
        }

        KeyGenerator keyGenerator;
        try {
            keyGenerator = KeyGenerator.getInstance(KeyProperties.KEY_ALGORITHM_AES, "AndroidKeyStore");
        } catch (NoSuchAlgorithmException | NoSuchProviderException e) {
            throw new RuntimeException("Failed to get KeyGenerator instance", e);
        }

        try {
            keyStore.load(null);
            keyGenerator.init(new
                    KeyGenParameterSpec.Builder(KEY_NAME,
                    KeyProperties.PURPOSE_ENCRYPT |
                            KeyProperties.PURPOSE_DECRYPT)
                    .setBlockModes(KeyProperties.BLOCK_MODE_CBC)
                    .setUserAuthenticationRequired(true)
                    .setEncryptionPaddings(
                            KeyProperties.ENCRYPTION_PADDING_PKCS7)
                    .build());
            keyGenerator.generateKey();
        } catch (NoSuchAlgorithmException |
                InvalidAlgorithmParameterException
                | CertificateException | IOException e) {
            throw new RuntimeException(e);
        }
    }


    @TargetApi(Build.VERSION_CODES.M)
    public boolean cipherInit() {
        try {
            cipher = Cipher.getInstance(KeyProperties.KEY_ALGORITHM_AES + "/" + KeyProperties.BLOCK_MODE_CBC + "/" + KeyProperties.ENCRYPTION_PADDING_PKCS7);
        } catch (NoSuchAlgorithmException | NoSuchPaddingException e) {
            throw new RuntimeException("Failed to get Cipher", e);
        }

        try {
            keyStore.load(null);
            SecretKey key = (SecretKey) keyStore.getKey(KEY_NAME,
                    null);
            cipher.init(Cipher.ENCRYPT_MODE, key);
            return true;
        } catch (KeyPermanentlyInvalidatedException e) {
            return false;
        } catch (KeyStoreException | CertificateException | UnrecoverableKeyException | IOException | NoSuchAlgorithmException | InvalidKeyException e) {
            throw new RuntimeException("Failed to init Cipher", e);
        }
    }

    private void doLogin() {
        String email = etEmail.getText().toString().trim();
        String password = etPassword.getText().toString().trim();
        if (cbRememberMe.isChecked()) {
            isRememberMe = true;
        } else {
            isRememberMe = false;
        }

        if (TextUtils.isEmpty(email)) {
            etEmail.requestFocus();
            etEmail.setError("Email can't be empty");
        } else if (TextUtils.isEmpty(password)) {
            etPassword.requestFocus();
            etPassword.setError("Password can't be empty");
        } else if (!Utils.isValidEmail(email)) {
            etEmail.requestFocus();
            etEmail.setError("Email is not valid");
        } else if (password.length() < 6) {
            etPassword.requestFocus();
            etPassword.setError("Password must be at least 6 characters");
        } else {
            if (!Internet.isAvailable(LoginActivity.this)) {
                Internet.showAlertDialog(LoginActivity.this, "Error!", "No Internet Connection", false);
            } else {
                new CallRequest(LoginActivity.this).nurseLogin(email, password);
            }
        }
    }

    public void fingerprintLoginCall() {
        String email = sharedpreferences.getString(MyConstants.USER_EMAIL, "");
        String password = sharedpreferences.getString(MyConstants.NURSE_PASSWORD, "");
        if (cbRememberMe.isChecked()) {
            isRememberMe = true;
        } else {
            isRememberMe = false;
        }

        new CallRequest(LoginActivity.this).nurseLogin(email, password);
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        try {

            Utils.hideProgressDialog();
            if (result != null && !result.isEmpty()) {
                Log.i("TAG", "TAG Result : " + result);

                switch (request) {
                    case nurseLogin:

                        try {
                            JSONObject object = new JSONObject(result);
                            String error_code = object.getString("error_code");
                            if (error_code.equalsIgnoreCase("0")) {
                                JSONObject resultObj = object.getJSONObject("result");
                                setLoginData(resultObj);
                            } else if (error_code.equalsIgnoreCase("5")) {

                                JSONObject resultObj = object.getJSONObject("result");
                                JSONArray arrayUser = resultObj.getJSONArray("user");
                                JSONObject obj = arrayUser.getJSONObject(0);

                                String id = obj.getString("id");

                                String error_string = object.getString("error_string");
                                Toast.makeText(instance, error_string, Toast.LENGTH_SHORT).show();
                            } else {
                                String error_string = object.getString("error_string");
                                Toast.makeText(instance, error_string, Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;
                }
            } else {
                Utils.showToast("Please try again later", this);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onProgressUpdate(String uniqueMessageId, int progres) {

    }

    @Override
    public void onProgressComplete(String uniqueMessageId, String result, Constant.REQUESTS request) {

    }

    private void setLoginData(JSONObject obj) {

        try {
            JSONArray jsonArray = obj.getJSONArray("nurse");
            JSONObject jsonObject = jsonArray.getJSONObject(0);
            String user_id = jsonObject.getString("id");
            String email = jsonObject.getString("email");
            String ApiToken = jsonObject.getString("ApiToken");
            String FirstName = jsonObject.getString("FirstName");
            String LastName = jsonObject.getString("LastName");
            String Country = jsonObject.getString("Country");
            String State = jsonObject.getString("State");
            String ProfilePic = jsonObject.getString("ProfilePic");

            name = FirstName + " " + LastName;

            SharedPreferences sharedpreferences = getSharedPreferences(MyConstants.PREF, Context.MODE_PRIVATE);

            SharedPreferences.Editor editor = sharedpreferences.edit();

            editor.putString(MyConstants.USER_TYPE, MyConstants.USER_NURSE);
            editor.putString(MyConstants.USER_EMAIL, email);
            editor.putString(MyConstants.USER_ID, user_id);
            editor.putString(MyConstants.PROFILE_PIC, ProfilePic);
            editor.putString(MyConstants.PT_NAME, name);
            editor.putString(MyConstants.PT_FIRST_NAME, FirstName);
            editor.putString(MyConstants.PT_LAST_NAME, LastName);
            editor.putString(MyConstants.TOKEN, ApiToken);
            editor.putString(MyConstants.SINCH_ID, "nnnn" + user_id);
            editor.putBoolean(MyConstants.IS_GUEST, false);
            editor.putBoolean(MyConstants.FIRST_LOGIN, true);
            editor.putBoolean(MyConstants.IS_LOGGED_IN, true);
            editor.putString(MyConstants.LOGIN_TYPE, "0");
            editor.putString(MyConstants.SOCIAL_ID, "0");
            MyConstants.isGuest = "0";
            if (isRememberMe) {
                editor.putString(MyConstants.NURSE_PASSWORD, etPassword.getText().toString());
            } else {
                editor.putString(MyConstants.NURSE_PASSWORD, "");
            }
            editor.commit();

            MyConstants.isFirstTime = true;

            App.user.setUserID(user_id);
            App.user.setName(name);
            App.user.setUserEmail(email);
            App.user.setUser_Type(MyConstants.USER_NURSE);
            App.user.setProfileUrl(sharedpreferences.getString(MyConstants.PROFILE_PIC, ""));
            App.user.setSinch_id(sharedpreferences.getString(MyConstants.SINCH_ID, ""));
            MyConstants.API_TOKEN = ApiToken;
            //getSinchServiceInterface().startClient(App.user.getSinch_id());

            System.out.println("data saved successfully");

            Intent intent = new Intent(LoginActivity.this, NurseDashboardActivity.class);
            startActivity(intent);
            ActivityCompat.finishAffinity(LoginActivity.this);

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
}
