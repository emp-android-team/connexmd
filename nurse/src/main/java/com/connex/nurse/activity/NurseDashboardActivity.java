package com.connex.nurse.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.connex.nurse.R;
import com.connex.nurse.fragments.CompleteChatFragment;
import com.connex.nurse.fragments.MyPatientFragment;
import com.connex.nurse.fragments.PendingPatientFragment;
import com.connex.nurse.interfaces.AsyncTaskListner;
import com.connex.nurse.model.MessageHistory;
import com.connex.nurse.others.App;
import com.connex.nurse.others.Internet;
import com.connex.nurse.ws.CallRequest;
import com.connex.nurse.ws.Constant;
import com.connex.nurse.ws.MyConstants;
import com.connex.nurse.ws.Utils;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class NurseDashboardActivity extends AppCompatActivity implements AsyncTaskListner {

    public TabLayout tabLayout;
    private ViewPager viewPager;
    private SharedPreferences sharedpreferences;
    public App app;
    public LinearLayout ivProfile;
    public EditText etSearch;
    private Fragment f;
    SharedPreferences.Editor editor;
//    String[] tabTitle = {"CALLS", "CHAT", "CONTACTS"};
    public ViewPagerAdapter viewPagerAdapter;
    public TextView tabName0,tabName1, tabName2, tabCount0, tabCount1, tabCount2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nurse_dashboard);

        app = App.getInstance();

        sharedpreferences = getSharedPreferences(MyConstants.PREF, Context.MODE_PRIVATE);
        if (sharedpreferences.getBoolean(MyConstants.IS_LOGGED_IN, false)) {
            App.user.setUserID(sharedpreferences.getString(MyConstants.USER_ID, ""));
            App.user.setName(sharedpreferences.getString(MyConstants.PT_NAME, ""));
            App.user.setFirstName(sharedpreferences.getString(MyConstants.PT_FIRST_NAME, ""));
            App.user.setLastName(sharedpreferences.getString(MyConstants.PT_LAST_NAME, ""));
            App.user.setUserEmail(sharedpreferences.getString(MyConstants.USER_EMAIL, ""));
            App.user.setUser_Type(sharedpreferences.getString(MyConstants.USER_TYPE, ""));
            App.user.setSinch_id(sharedpreferences.getString(MyConstants.SINCH_ID, ""));
            App.user.setLoginType(sharedpreferences.getString(MyConstants.LOGIN_TYPE, "0"));
            App.user.setProfileUrl(sharedpreferences.getString(MyConstants.PROFILE_PIC, ""));
            MyConstants.API_TOKEN = sharedpreferences.getString(MyConstants.TOKEN, "");

        }

        viewPager = findViewById(R.id.viewpager);
        tabLayout = findViewById(R.id.tabs);
        ivProfile = findViewById(R.id.ivProfile);
        etSearch = findViewById(R.id.etSearch);

        //viewPager.setOffscreenPageLimit(3);
        //setupViewPager(viewPager);
        //tabLayout.setupWithViewPager(viewPager);

        viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
        //viewPager.setOffscreenPageLimit(3);
        viewPager.setAdapter(viewPagerAdapter);

        final TabLayout.Tab pendingPatients = tabLayout.newTab();
        final TabLayout.Tab myPatients = tabLayout.newTab();
        final TabLayout.Tab completeChats = tabLayout.newTab();

        View pendingPatientView = getLayoutInflater().inflate(R.layout.badge,null);
        tabName0 = pendingPatientView.findViewById(R.id.tabName);
        tabCount0 = pendingPatientView.findViewById(R.id.count);
        tabName0.setText("Pending Patients");
        tabName0.setTextColor(getResources().getColor(R.color.theme_color));
        setTabBadge(0,"0");

        View myPatientView = getLayoutInflater().inflate(R.layout.badge,null);
        tabName1 = myPatientView.findViewById(R.id.tabName);
        tabCount1 = myPatientView.findViewById(R.id.count);
        tabName1.setText("My Patients");
        tabName1.setTextColor(getResources().getColor(R.color.tab_text_color));
        setTabBadge(1,"0");

        View completeChatView = getLayoutInflater().inflate(R.layout.badge,null);
        tabName2 = completeChatView.findViewById(R.id.tabName);
        tabCount2 = completeChatView.findViewById(R.id.count);
        tabName2.setText("Complete Chats");
        tabName2.setTextColor(getResources().getColor(R.color.tab_text_color));
        setTabBadge(2,"0");

        pendingPatients.setCustomView(pendingPatientView);
        myPatients.setCustomView(myPatientView);
        completeChats.setCustomView(completeChatView);

        tabLayout.addTab(pendingPatients, 0);
        tabLayout.addTab(myPatients, 1);
        tabLayout.addTab(completeChats, 2);

        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));

        etSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                etSearch.setCursorVisible(true);
            }
        });

        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                try {
                    Log.i("TAG", "SEARCH" + charSequence.toString());

                    System.out.println("fragment :::" + f);
                    if (MyConstants.TAB_POSITION == 0) {
                        if (((PendingPatientFragment) f).pendingPatientAdapter != null)
                            (((PendingPatientFragment) f)).pendingPatientAdapter.filters(charSequence.toString());
                    } else if (MyConstants.TAB_POSITION == 1) {
                        if (((MyPatientFragment) f).myPatientAdapter != null)
                            ((MyPatientFragment) f).myPatientAdapter.filters(charSequence.toString());
                    } else if (MyConstants.TAB_POSITION == 2) {
                        if (((CompleteChatFragment) f).completeChatAdapter != null)
                            ((CompleteChatFragment) f).completeChatAdapter.filters(charSequence.toString());
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        ivProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(NurseDashboardActivity.this, NurseProfileActivity.class);
                startActivity(intent);
            }
        });

        if (!Internet.isAvailable(NurseDashboardActivity.this)) {
            Internet.showAlertDialog(NurseDashboardActivity.this, "Error!", "No Internet Connection", false);
        } else {
            if (sharedpreferences.getBoolean("isUpdated", false)){
                if (sharedpreferences.getBoolean(MyConstants.IS_LOGGED_IN, false)) {
                    new CallRequest(NurseDashboardActivity.this).setDeviceToken();
                } else {
                    if (MyConstants.isFirstTime) {
                        sendNotificationToAllUsers();
                    }
                }
            } else {
                if (MyConstants.isFirstTime) {
                    sendNotificationToAllUsers();
                }
            }
        }

        /*if (MyConstants.isFirstTime) {
            sendNotificationToAllUsers();
        }*/

    }

    public void setTabBadge(int index, String value) {
        if (index == 0){
            if (Integer.parseInt(value) > 0) {
                tabCount0.setText(value);
                tabCount0.setVisibility(View.VISIBLE);
            } else {
                tabCount0.setVisibility(View.GONE);
            }
        } else if (index == 1){
            if (Integer.parseInt(value) > 0) {
                tabCount1.setText(value);
                tabCount1.setVisibility(View.VISIBLE);
            } else {
                tabCount1.setVisibility(View.GONE);
            }
        } else if (index == 2){
            if (Integer.parseInt(value) > 0) {
                tabCount2.setText(value);
                tabCount2.setVisibility(View.VISIBLE);
            } else {
                tabCount2.setVisibility(View.GONE);
            }
        }
    }

    private void sendNotificationToAllUsers() {
        if (!Internet.isAvailable(NurseDashboardActivity.this)) {
            Internet.showAlertDialog(NurseDashboardActivity.this, "Error!", "No Internet Connection", false);
            return;
        }

        Map<String, String> map = new HashMap<String, String>();

        map.put("url", MyConstants.NURSE_BASE_URL + "nurseOnline");
        map.put("NurseId", App.user.getUserID());
        map.put("ApiToken", MyConstants.API_TOKEN);
        map.put("Version", MyConstants.WS_VERSION);


        new CallRequest(NurseDashboardActivity.this).sendNotificationToUsers(map);
    }

    public void getMessageHistory() {
        if (!Internet.isAvailable(NurseDashboardActivity.this)) {
            Internet.showAlertDialog(NurseDashboardActivity.this, "Error!", "No Internet Connection", false);
            return;
        }

        Map<String, String> map = new HashMap<String, String>();
        map.put("url", MyConstants.NURSE_BASE_URL + "allStatus");
        map.put("NurseId", App.user.getUserID());
        map.put("ApiToken", MyConstants.API_TOKEN);
        map.put("Version", MyConstants.WS_VERSION);

        new CallRequest(NurseDashboardActivity.this).getChatHistory(map);
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new PendingPatientFragment(), "Pending Patients");
        adapter.addFragment(new MyPatientFragment(), "My Patients");
        adapter.addFragment(new CompleteChatFragment(), "Complete Chat");
        viewPager.setAdapter(adapter);
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        Utils.removeSimpleSpinProgressDialog();
        try {
            if (result != null && !result.isEmpty()) {
                Log.i("TAG", "TAG Result : " + result);
                //      {"document":{"response":{"status":1,"message":"Success."}}}
                switch (request) {

                    case sendNotification:
                        Utils.removeSimpleSpinProgressDialog();
                        try {
                            JSONObject mainObj = new JSONObject(result);
                            if (mainObj.getString("error_code").equalsIgnoreCase("0")) {
                                MyConstants.isFirstTime = false;
                                editor = sharedpreferences.edit();
                                editor.putBoolean("isUpdated", false);
                                editor.commit();
                            } else if (mainObj.getString("error_code").equalsIgnoreCase("1") ||
                                    mainObj.getString("error_code").equalsIgnoreCase("2")) {
                                /*editor = sharedpreferences.edit();
                                editor.clear();
                                editor.commit();

                                startActivity(new Intent(NurseDashboardActivity.this, LoginActivity.class));
                                ActivityCompat.finishAffinity(NurseDashboardActivity.this);*/
                            } else {
                                Utils.showToast(mainObj.getString("error_string"), NurseDashboardActivity.this);
                            }
                            getMessageHistory();
                        } catch (JSONException e) {
                            e.printStackTrace();
                            getMessageHistory();
                        }

                        break;

                    case chatHistory:
                        Utils.removeSimpleSpinProgressDialog();
                        try {
                            JSONObject mainObj = new JSONObject(result);
                            if (mainObj.getString("error_code").equalsIgnoreCase("0")) {
                                MyConstants.pendingPatientList.clear();
                                MyConstants.completeChatList.clear();
                                MyConstants.myPatientList.clear();
                                if (mainObj.getJSONObject("result") != null && mainObj.getJSONObject("result").length() > 0) {
                                    JSONObject jsonObject = mainObj.getJSONObject("result");

                                    JSONArray NotAssigned = jsonObject.getJSONArray("NotAssigned");
                                    JSONArray Assigned = jsonObject.getJSONArray("Assigned");
                                    JSONArray Completed = jsonObject.getJSONArray("Completed");

                                    if (NotAssigned.length() > 0) {
                                        for (int i = 0; i < NotAssigned.length(); i++) {
                                            JSONObject object = NotAssigned.getJSONObject(i);

                                            final MessageHistory messageHistory = new MessageHistory();
                                            messageHistory.setNurseId(object.getString("NurseId"));
                                            messageHistory.setUserId(object.getString("UserId"));
                                            messageHistory.setUniqueId(object.getString("UniqueId"));
                                            messageHistory.setIsGuest(object.getString("IsGuest"));
                                            messageHistory.setSocialId(object.getString("SocialId"));
                                            messageHistory.setSocialType(object.getString("SocialType"));

                                            JSONObject user_detail = object.getJSONObject("user_detail");

                                            messageHistory.setFirstName(user_detail.getString("FirstName"));
                                            messageHistory.setLastName(user_detail.getString("LastName"));
                                            messageHistory.setProfilePic(user_detail.getString("ProfilePic"));

                                            MyConstants.pendingPatientList.add(messageHistory);

                                        }
                                    }

                                    if (Assigned.length() > 0) {
                                        for (int i = 0; i < Assigned.length(); i++) {
                                            JSONObject object = Assigned.getJSONObject(i);

                                            final MessageHistory messageHistory = new MessageHistory();
                                            messageHistory.setNurseId(object.getString("NurseId"));
                                            messageHistory.setUserId(object.getString("UserId"));
                                            messageHistory.setUniqueId(object.getString("UniqueId"));
                                            messageHistory.setIsGuest(object.getString("IsGuest"));
                                            messageHistory.setSocialId(object.getString("SocialId"));
                                            messageHistory.setSocialType(object.getString("SocialType"));

                                            JSONObject user_detail = object.getJSONObject("user_detail");

                                            messageHistory.setFirstName(user_detail.getString("FirstName"));
                                            messageHistory.setLastName(user_detail.getString("LastName"));
                                            messageHistory.setProfilePic(user_detail.getString("ProfilePic"));

                                            MyConstants.myPatientList.add(messageHistory);

                                        }
                                    }

                                    if (Completed.length() > 0) {
                                        for (int i = 0; i < Completed.length(); i++) {
                                            JSONObject object = Completed.getJSONObject(i);

                                            final MessageHistory messageHistory = new MessageHistory();
                                            messageHistory.setNurseId(object.getString("NurseId"));
                                            messageHistory.setUserId(object.getString("UserId"));
                                            messageHistory.setUniqueId(object.getString("UniqueId"));
                                            messageHistory.setIsGuest(object.getString("IsGuest"));
                                            messageHistory.setSocialId(object.getString("SocialId"));
                                            messageHistory.setSocialType(object.getString("SocialType"));

                                            JSONObject user_detail = object.getJSONObject("user_detail");

                                            messageHistory.setFirstName(user_detail.getString("FirstName"));
                                            messageHistory.setLastName(user_detail.getString("LastName"));
                                            messageHistory.setProfilePic(user_detail.getString("ProfilePic"));

                                            MyConstants.completeChatList.add(messageHistory);

                                        }
                                    }

                                    viewPager = findViewById(R.id.viewpager);
                                    ViewPagerAdapter adapter = (ViewPagerAdapter) viewPager.getAdapter();
                                    if (adapter != null) {
                                        adapter.notifyDataSetChanged();
                                    }

                                    tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
                                        @Override
                                        public void onTabSelected(TabLayout.Tab tab) {
                                            viewPager.setCurrentItem(tab.getPosition());

                                            if (tab.getPosition() == 0){
                                                tabName0.setTextColor(getResources().getColor(R.color.theme_color));
                                                tabName1.setTextColor(getResources().getColor(R.color.tab_text_color));
                                                tabName2.setTextColor(getResources().getColor(R.color.tab_text_color));
                                            } else if (tab.getPosition() == 1){
                                                tabName0.setTextColor(getResources().getColor(R.color.tab_text_color));
                                                tabName1.setTextColor(getResources().getColor(R.color.theme_color));
                                                tabName2.setTextColor(getResources().getColor(R.color.tab_text_color));
                                            } else if (tab.getPosition() == 2){
                                                tabName0.setTextColor(getResources().getColor(R.color.tab_text_color));
                                                tabName1.setTextColor(getResources().getColor(R.color.tab_text_color));
                                                tabName2.setTextColor(getResources().getColor(R.color.theme_color));
                                            }

                                            MyConstants.TAB_POSITION = tab.getPosition();
                                            System.out.println("tab pos::::" + MyConstants.TAB_POSITION);
                                            //f = getSupportFragmentManager().getFragments().get(viewPager.getCurrentItem());
                                            ViewPagerAdapter adapter = (ViewPagerAdapter) viewPager.getAdapter();
                                            if (adapter != null) {
                                                f = adapter.getItem(tab.getPosition());
                                                adapter.notifyDataSetChanged();
                                            }

                                            etSearch.setText("");

                                            try {
                                                InputMethodManager inputMethodManager = (InputMethodManager)
                                                        getSystemService(Context.INPUT_METHOD_SERVICE);
                                                if (inputMethodManager != null) {
                                                    inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                                                }
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                        }

                                        @Override
                                        public void onTabUnselected(TabLayout.Tab tab) {

                                        }

                                        @Override
                                        public void onTabReselected(TabLayout.Tab tab) {

                                        }
                                    });

                                } else {
                                    Utils.showToast(mainObj.getString("error_string"), NurseDashboardActivity.this);
                                }
                            } else if (mainObj.getString("error_code").equalsIgnoreCase("1") ||
                                    mainObj.getString("error_code").equalsIgnoreCase("2")) {
                                editor = sharedpreferences.edit();
                                editor.clear();
                                editor.commit();

                                Utils.showToast(mainObj.getString("error_string"), NurseDashboardActivity.this);

                                startActivity(new Intent(NurseDashboardActivity.this, LoginActivity.class));
                                ActivityCompat.finishAffinity(NurseDashboardActivity.this);
                            } else if (mainObj.getString("error_code").equalsIgnoreCase("9")) {
                                Utils.showAlert("No chats found", NurseDashboardActivity.this);

                                viewPager = findViewById(R.id.viewpager);
                                ViewPagerAdapter adapter = (ViewPagerAdapter) viewPager.getAdapter();
                                if (adapter != null) {
                                    adapter.notifyDataSetChanged();
                                }

                                tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
                                    @Override
                                    public void onTabSelected(TabLayout.Tab tab) {
                                        viewPager.setCurrentItem(tab.getPosition());

                                        if (tab.getPosition() == 0){
                                            tabName0.setTextColor(getResources().getColor(R.color.theme_color));
                                            tabName1.setTextColor(getResources().getColor(R.color.tab_text_color));
                                            tabName2.setTextColor(getResources().getColor(R.color.tab_text_color));
                                        } else if (tab.getPosition() == 1){
                                            tabName0.setTextColor(getResources().getColor(R.color.tab_text_color));
                                            tabName1.setTextColor(getResources().getColor(R.color.theme_color));
                                            tabName2.setTextColor(getResources().getColor(R.color.tab_text_color));
                                        } else if (tab.getPosition() == 2){
                                            tabName0.setTextColor(getResources().getColor(R.color.tab_text_color));
                                            tabName1.setTextColor(getResources().getColor(R.color.tab_text_color));
                                            tabName2.setTextColor(getResources().getColor(R.color.theme_color));
                                        }

                                        MyConstants.TAB_POSITION = tab.getPosition();
                                        System.out.println("tab pos::::" + MyConstants.TAB_POSITION);
                                        //f = getSupportFragmentManager().getFragments().get(viewPager.getCurrentItem());
                                        ViewPagerAdapter adapter = (ViewPagerAdapter) viewPager.getAdapter();
                                        if (adapter != null) {
                                            f = adapter.getItem(tab.getPosition());
                                            adapter.notifyDataSetChanged();
                                        }

                                        etSearch.setText("");

                                        try {
                                            InputMethodManager inputMethodManager = (InputMethodManager)
                                                    getSystemService(Context.INPUT_METHOD_SERVICE);
                                            if (inputMethodManager != null) {
                                                inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                                            }
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }

                                    @Override
                                    public void onTabUnselected(TabLayout.Tab tab) {

                                    }

                                    @Override
                                    public void onTabReselected(TabLayout.Tab tab) {

                                    }
                                });
                            } else {
                                Utils.showToast(mainObj.getString("error_string"), NurseDashboardActivity.this);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;

                    case setDeviceToken:
                        Utils.removeSimpleSpinProgressDialog();
                        try {
                            JSONObject mainObj = new JSONObject(result);
                            if (mainObj.getString("error_code").equalsIgnoreCase("0")) {
                                editor = sharedpreferences.edit();
                                editor.putBoolean("isUpdated", false);
                                editor.commit();
                                if (MyConstants.isFirstTime) {
                                    sendNotificationToAllUsers();
                                }
                            } else {
                                Utils.showToast(mainObj.getString("error_string"), NurseDashboardActivity.this);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;
                }
            }
        } catch (Exception e) {
            Utils.removeSimpleSpinProgressDialog();
            e.printStackTrace();
            Utils.showToast("Please try again later", NurseDashboardActivity.this);
        }
    }

    @Override
    public void onProgressUpdate(String uniqueMessageId, int progres) {

    }

    @Override
    public void onProgressComplete(String uniqueMessageId, String result, Constant.REQUESTS request) {

    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
            mFragmentList.add(new PendingPatientFragment());
            mFragmentList.add(new MyPatientFragment());
            mFragmentList.add(new CompleteChatFragment());

            mFragmentTitleList.add("Pending Patients");
            mFragmentTitleList.add("My Patients");
            mFragmentTitleList.add("Complete Chats");
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public int getItemPosition(Object object) {
            // POSITION_NONE makes it possible to reload the PagerAdapter
            return POSITION_NONE;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        viewPager = findViewById(R.id.viewpager);
        ViewPagerAdapter adapter = (ViewPagerAdapter) viewPager.getAdapter();
        if (adapter != null) {
            adapter.notifyDataSetChanged();
        }

        if (MyConstants.isBackPressed) {
            getMessageHistory();
            MyConstants.isBackPressed = false;
        }
    }

}
