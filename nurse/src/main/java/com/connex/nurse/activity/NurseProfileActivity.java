package com.connex.nurse.activity;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.crashlytics.android.Crashlytics;

import com.connex.nurse.R;
import com.connex.nurse.custom_views.CircleImageView;
import com.connex.nurse.custom_views.StaggeredTextGridView;
import com.connex.nurse.interfaces.AsyncTaskListner;
import com.connex.nurse.model.NurseProfile;
import com.connex.nurse.others.App;
import com.connex.nurse.others.Internet;
import com.connex.nurse.others.PicassoTrustAll;
import com.connex.nurse.ws.CallRequest;
import com.connex.nurse.ws.Constant;
import com.connex.nurse.ws.MyConstants;
import com.connex.nurse.ws.Utils;
import com.squareup.picasso.Callback;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.fabric.sdk.android.Fabric;

public class NurseProfileActivity extends AppCompatActivity implements AsyncTaskListner {

    TextView tvAboutDoctor, tvEducation, tvMembership, tvHospitalAffiliation, tvPublications;
    LinearLayout llProfession;
    //GridLayout llScopeOfPractice;
    //ExpandableHeightListView lvComments;
    //DRProfileCommentsAdapter drProfileCommentsAdapter;
    StaggeredTextGridView tvScopeOfPractice;
    //DRProfileScopeOfPracticeAdapter adapter;
    LinearLayout ll_about_doctor;
    ImageView ivArrow;
    LinearLayout ivBack;
    NurseProfile nurseProfile;
    Button btnSignOut;
    CircleImageView ivDRImage;
    TextView tvDRName, tvSpeciality, tvRating, tvTotalRating, tvFee, tvWaitingTime, tvLocation, tvUnderDoctors;
    RatingBar ratingBar;
    ToggleButton toggleFavourite;
    View focusView;
    TextView tvEducationLabel,tvAboutDoctorLabel, tvProfessionLabel, tvMembershipLabel, tvHospitalLabel, tvPublicationLabel, tvComments, tvUnderDoctorsLabel;
    public Dialog dialog;
    Button btnLoginNow, btnBookAsGuest;
    LinearLayout llBookNow;
    View line, line1;
    SharedPreferences sharedpreferences;
    SharedPreferences.Editor editor;
    LinearLayout ll_settings;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nurse_profile);

        Fabric.with(this, new Crashlytics());

        focusView = findViewById(R.id.focusView);
        focusView.requestFocus();

        sharedpreferences = getSharedPreferences(MyConstants.PREF, Context.MODE_PRIVATE);

        tvEducationLabel = findViewById(R.id.tvEducationLabel);
        tvProfessionLabel = findViewById(R.id.tvProfessionLabel);
        btnSignOut = findViewById(R.id.btnSignOut);
        ivDRImage = findViewById(R.id.ivDrImage);
        ratingBar = findViewById(R.id.ratingBar);
        tvDRName = findViewById(R.id.tvDrName);
        tvSpeciality = findViewById(R.id.tvSpeciality);
        tvRating = findViewById(R.id.tvRating);
        tvWaitingTime = findViewById(R.id.tvWaitingTime);
        tvAboutDoctor = findViewById(R.id.tvAboutDoctor);
        tvEducation = findViewById(R.id.tvEducation);
        llProfession = findViewById(R.id.ll_profession);
        ll_about_doctor = findViewById(R.id.ll_about_doctor);
        ivArrow = findViewById(R.id.ivArrow);
        ivBack = findViewById(R.id.ivBack);
        line1 = findViewById(R.id.line1);
        tvUnderDoctors = findViewById(R.id.tvUnderDoctors);
        tvUnderDoctorsLabel = findViewById(R.id.tvUnderDoctorsLabel);
        ll_settings = findViewById(R.id.llSettings);
        tvAboutDoctorLabel = findViewById(R.id.tvAboutDoctorLabel);

        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        /*ll_about_doctor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (tvAboutDoctor.getLineCount() == 1) {
                    tvAboutDoctor.setSingleLine(false);
                    ivArrow.setImageResource(R.drawable.up_aero);

                } else {
                    tvAboutDoctor.setSingleLine(true);
                    ivArrow.setImageResource(R.drawable.down_aero);
                }
            }
        });*/

        ll_settings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(NurseProfileActivity.this, SettingsActivity.class));
            }
        });

        btnSignOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setLogOutAlert();
            }
        });

        getNurseProfileData();

    }

    public void setLogOutAlert() {

        new AlertDialog.Builder(this)
                .setTitle("Alert!")
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setMessage("Would you like to logout?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        editor = sharedpreferences.edit();

                        editor.remove(MyConstants.TOKEN);
                        editor.remove(MyConstants.SINCH_ID);
                        editor.remove(MyConstants.PT_LAST_NAME);
                        editor.remove(MyConstants.PT_NAME);
                        editor.remove(MyConstants.PT_FIRST_NAME);
                        editor.remove(MyConstants.USER_ID);
                        editor.remove(MyConstants.LOGIN_TYPE);
                        editor.remove(MyConstants.PROFILE_PIC);

                        editor.putBoolean(MyConstants.IS_LOGGED_IN, false);
                        editor.commit();

                        dialog.dismiss();

                        Intent intent = new Intent(NurseProfileActivity.this, LoginActivity.class);
                        startActivity(intent);
                        ActivityCompat.finishAffinity(NurseProfileActivity.this);

                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .show();
    }

    private void getNurseProfileData() {

        if (!Internet.isAvailable(this)) {
            Internet.showAlertDialog(this, "Error!", "No Internet Connection", false);

            return;
        }

        Map<String, String> map = new HashMap<String, String>();
        map.put("url", MyConstants.NURSE_BASE_URL + "nurseProfile");
        map.put("ApiToken", MyConstants.API_TOKEN);
        map.put("Version", MyConstants.WS_VERSION);
        map.put("NurseId", App.user.getUserID());

        new CallRequest(this).getNurseProfile(map);
    }


    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        Utils.removeSimpleSpinProgressDialog();
        try {
            if (result != null && !result.isEmpty()) {
                Log.i("TAG", "TAG Result : " + result);
                switch (request) {
                    case nurseProfile:
                        Utils.removeSimpleSpinProgressDialog();

                        List<HashMap<String, String>> professionalRotationList, scopeOfPracticeList, underDoctorList, educationList, hospitalAffiliationList, publicationsList, feedbackList;
                        professionalRotationList = new ArrayList<>();
                        scopeOfPracticeList = new ArrayList<>();
                        underDoctorList = new ArrayList<>();
                        educationList = new ArrayList<>();
                        hospitalAffiliationList = new ArrayList<>();
                        publicationsList = new ArrayList<>();
                        feedbackList = new ArrayList<>();

                        try {
                            JSONObject mainObj = new JSONObject(result);
                            if (mainObj.getString("error_code").equalsIgnoreCase("0")) {
                                if (mainObj.getJSONArray("result") != null && mainObj.getJSONArray("result").length() > 0) {
                                    JSONArray jsonArray = mainObj.getJSONArray("result");
                                    JSONObject object = jsonArray.getJSONObject(0);


                                    JSONObject nurse = object.getJSONObject("Nurse");

                                    // education
                                    JSONArray Education = nurse.getJSONArray("Education");
                                    for (int i = 0; i < Education.length(); i++) {
                                        JSONObject educationObj = Education.getJSONObject(i);

                                        HashMap<String, String> map = new HashMap<>();
                                        map.put("id", educationObj.getString("id"));
                                        map.put("NurseId", educationObj.getString("NurseId"));
                                        map.put("Description", educationObj.getString("Description"));
                                        map.put("StartYear", educationObj.getString("StartYear"));
                                        map.put("EndYear", educationObj.getString("EndYear"));

                                        educationList.add(map);
                                    }

                                    // professional rotation
                                    JSONArray Professional_Rotation = nurse.getJSONArray("Experience");
                                    for (int i = 0; i < Professional_Rotation.length(); i++) {
                                        JSONObject professionalObj = Professional_Rotation.getJSONObject(i);

                                        HashMap<String, String> map = new HashMap<>();
                                        map.put("id", professionalObj.getString("id"));
                                        map.put("NurseId", professionalObj.getString("NurseId"));
                                        map.put("Description", professionalObj.getString("Description"));
                                        map.put("StartYear", professionalObj.getString("StartYear"));
                                        map.put("EndYear", professionalObj.getString("EndYear"));

                                        professionalRotationList.add(map);
                                    }

                                    // hospital affiliation
                                    JSONArray Affiliation = nurse.getJSONArray("Affiliation");
                                    for (int i = 0; i < Affiliation.length(); i++) {
                                        JSONObject affiliationObj = Affiliation.getJSONObject(i);

                                        HashMap<String, String> map = new HashMap<>();
                                        map.put("id", affiliationObj.getString("id"));
                                        map.put("NurseId", affiliationObj.getString("NurseId"));
                                        map.put("Description", affiliationObj.getString("Description"));
                                        map.put("StartYear", affiliationObj.getString("StartYear"));
                                        map.put("EndYear", affiliationObj.getString("EndYear"));

                                        hospitalAffiliationList.add(map);
                                    }

                                    // under doctors
                                    JSONArray UnderDoctors = nurse.getJSONArray("UnderDoctors");
                                    for (int i = 0; i < UnderDoctors.length(); i++) {
                                        JSONObject underDoctorObj = UnderDoctors.getJSONObject(i);

                                        HashMap<String, String> map = new HashMap<>();
                                        map.put("id", underDoctorObj.getString("id"));
                                        map.put("NurseId", underDoctorObj.getString("NurseId"));
                                        map.put("DoctorName", underDoctorObj.getString("DoctorName"));
                                        map.put("DoctorSpeciality", underDoctorObj.getString("DoctorSpeciality"));

                                        underDoctorList.add(map);
                                    }

                                    nurseProfile = new NurseProfile();
                                    nurseProfile.setNurseId(nurse.getString("id"));
                                    nurseProfile.setFirstName(nurse.getString("FirstName"));
                                    nurseProfile.setLastName(nurse.getString("LastName"));
                                    nurseProfile.setAbout_doctor(nurse.getString("Description"));
                                    nurseProfile.setResponseTime(nurse.getString("ResponseTime"));
                                    nurseProfile.setProfilePic(nurse.getString("ProfilePic"));
                                    nurseProfile.setEducation(educationList);
                                    nurseProfile.setProfessional_rotation(professionalRotationList);
                                    nurseProfile.setHospital_affiliation(hospitalAffiliationList);
                                    nurseProfile.setUnder_doctors(underDoctorList);

                                    setProfileData();

                                } else {
                                    Utils.showToast(mainObj.getString("error_string"), NurseProfileActivity.this);
                                }
                            } else if (mainObj.getString("error_code").equalsIgnoreCase("1") ||
                                    mainObj.getString("error_code").equalsIgnoreCase("2")) {

                                editor = sharedpreferences.edit();
                                editor.clear();
                                editor.commit();

                                Utils.showToast(mainObj.getString("error_string"), NurseProfileActivity.this);

                                startActivity(new Intent(NurseProfileActivity.this, LoginActivity.class));
                                ActivityCompat.finishAffinity(NurseProfileActivity.this);
                            } else {
                                Utils.showToast(mainObj.getString("error_string"), NurseProfileActivity.this);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;
                }
            }
        } catch (Exception e) {
            Utils.removeSimpleSpinProgressDialog();
            e.printStackTrace();
            Utils.showToast("Please try again later", this);
        }
    }

    @Override
    public void onProgressUpdate(String uniqueMessageId, int progres) {
    }

    @Override
    public void onProgressComplete(String uniqueMessageId, String result, Constant.REQUESTS request) {
    }

    private void setProfileData() {

        tvDRName.setText(nurseProfile.getFirstName() + " " + nurseProfile.getLastName());
        //tvSpeciality.setText(nurseProfile.getSpeciality());
        tvWaitingTime.setText(nurseProfile.getResponseTime());

        try {
            PicassoTrustAll.getInstance(NurseProfileActivity.this)
                    .load(nurseProfile.getProfilePic())
                    .placeholder(R.drawable.avatar)
                    .error(R.drawable.avatar)
                    .into(ivDRImage, new Callback() {
                        @Override
                        public void onSuccess() {
                        }

                        @Override
                        public void onError() {
                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (!TextUtils.isEmpty(nurseProfile.getAbout_doctor())) {
            tvAboutDoctor.setText(nurseProfile.getAbout_doctor());
            tvAboutDoctorLabel.setVisibility(View.VISIBLE);
            ll_about_doctor.setVisibility(View.VISIBLE);
            if (nurseProfile.getEducation().size() == 0 && nurseProfile.getProfessional_rotation().size() == 0
                    && nurseProfile.getUnder_doctors().size() == 0) {
                tvAboutDoctor.setSingleLine(false);
                ivArrow.setVisibility(View.GONE);
            } else {
                ll_about_doctor.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (tvAboutDoctor.getLineCount() == 1) {
                            tvAboutDoctor.setSingleLine(false);
                            ivArrow.setImageResource(R.drawable.up_aero);

                        } else {
                            tvAboutDoctor.setSingleLine(true);
                            ivArrow.setImageResource(R.drawable.down_aero);
                        }
                    }
                });
            }
        } else {
            tvAboutDoctorLabel.setVisibility(View.GONE);
            ll_about_doctor.setVisibility(View.GONE);
        }

        if (nurseProfile.getEducation().size() > 0) {
            tvEducationLabel.setVisibility(View.VISIBLE);
            tvEducation.setVisibility(View.VISIBLE);
            StringBuilder educationStr = new StringBuilder();
            for (int i = 0; i < nurseProfile.getEducation().size(); i++) {
                if (!TextUtils.isEmpty(nurseProfile.getEducation().get(i).get("StartYear").trim()) && !TextUtils.isEmpty(nurseProfile.getEducation().get(i).get("EndYear").trim())) {
                    educationStr.append("<font color='#39c3f6'>&#62;&nbsp;&nbsp;</font>")
                            .append(nurseProfile.getEducation().get(i).get("Description"))
                            .append(", ")
                            .append(nurseProfile.getEducation().get(i).get("StartYear"))
                            .append(" - ")
                            .append(nurseProfile.getEducation().get(i).get("EndYear"))
                            .append("<br/>");
                } else if (TextUtils.isEmpty(nurseProfile.getEducation().get(i).get("EndYear").trim())) {
                    educationStr.append("<font color='#39c3f6'>&#62;&nbsp;&nbsp;</font>")
                            .append(nurseProfile.getEducation().get(i).get("Description"))
                            .append(", ")
                            .append(nurseProfile.getEducation().get(i).get("StartYear"))
                            .append("<br/>");
                } else if (TextUtils.isEmpty(nurseProfile.getEducation().get(i).get("StartYear").trim()) && TextUtils.isEmpty(nurseProfile.getEducation().get(i).get("EndYear").trim())) {
                    educationStr.append("<font color='#39c3f6'>&#62;&nbsp;&nbsp;</font>")
                            .append(nurseProfile.getEducation().get(i).get("Description"))
                            .append("<br/>");
                }
            }

            System.out.println("string:::" + educationStr);

            tvEducation.setText(Html.fromHtml(educationStr.toString()), TextView.BufferType.SPANNABLE);
        } else {
            tvEducation.setVisibility(View.GONE);
            tvEducationLabel.setVisibility(View.GONE);
        }

        if (nurseProfile.getUnder_doctors().size() > 0) {
            tvUnderDoctorsLabel.setVisibility(View.VISIBLE);

            StringBuilder underDoctorStr = new StringBuilder();
            for (int i = 0; i < nurseProfile.getUnder_doctors().size(); i++) {
                underDoctorStr.append("<font color='#39c3f6'>&#62;&nbsp;&nbsp;</font>")
                        .append(nurseProfile.getUnder_doctors().get(i).get("DoctorName"))
                        .append(" - <font color='#39c3f6'>")
                        .append(nurseProfile.getUnder_doctors().get(i).get("DoctorSpeciality"))
                        .append("</font><br/>");
            }

            System.out.println("string:::" + underDoctorStr);

            tvUnderDoctors.setText(Html.fromHtml(underDoctorStr.toString()), TextView.BufferType.SPANNABLE);
        } else {
            tvUnderDoctorsLabel.setVisibility(View.GONE);
        }

        if (nurseProfile.getProfessional_rotation().size() > 0) {
            tvProfessionLabel.setVisibility(View.VISIBLE);
            for (int i = 0; i < nurseProfile.getProfessional_rotation().size(); i++) {
                View layout = LayoutInflater.from(NurseProfileActivity.this).inflate(R.layout.layout_nurse_profile_profession, llProfession, false);
                TextView tvDescription = layout.findViewById(R.id.tvDescription);
                TextView tvTitle = layout.findViewById(R.id.tvTitle);
                View line = layout.findViewById(R.id.line_view);

                if (TextUtils.isEmpty(nurseProfile.getProfessional_rotation().get(i).get("EndYear"))) {
                    tvTitle.setText(nurseProfile.getProfessional_rotation().get(i).get("StartYear"));
                } else {
                    tvTitle.setText(nurseProfile.getProfessional_rotation().get(i).get("StartYear") + " - " + nurseProfile.getProfessional_rotation().get(i).get("EndYear"));
                }
                tvDescription.setText(nurseProfile.getProfessional_rotation().get(i).get("Description"));

                if (i == nurseProfile.getProfessional_rotation().size() - 1) {
                    line.setVisibility(View.GONE);
                }

                llProfession.addView(layout);
            }
        } else {
            tvProfessionLabel.setVisibility(View.GONE);
        }

        if (nurseProfile.getHospital_affiliation().size() > 0) {
            for (int i = 0; i < nurseProfile.getHospital_affiliation().size(); i++) {
                if (TextUtils.isEmpty(nurseProfile.getHospital_affiliation().get(i).get("EndYear").trim())) {
                    tvSpeciality.setText(nurseProfile.getHospital_affiliation().get(i).get("Description"));
                    break;
                }
            }
        }

    }
}
