package com.connex.nurse.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;


import com.alimuzaffar.lib.pin.PinEntryEditText;

import com.connex.nurse.R;
import com.connex.nurse.interfaces.AsyncTaskListner;
import com.connex.nurse.ws.CallRequest;
import com.connex.nurse.ws.Constant;
import com.connex.nurse.ws.MyConstants;
import com.connex.nurse.ws.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ResetPasswordOTPActivity extends AppCompatActivity implements AsyncTaskListner {

    PinEntryEditText pinView;
    Button btnSendAgain, btnResetPassword;
    String OTP, user_id, user_type, email, phone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password_otp);

        pinView = findViewById(R.id.pinview);
        btnSendAgain = findViewById(R.id.btnSendAgain);
        btnResetPassword = findViewById(R.id.btnResetPassword);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            user_id = bundle.getString("user_id");
            //user_type = bundle.getString("user_type");
            email = bundle.getString("email");
            phone = bundle.getString("phone");
        }

        btnResetPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callResetPasswordOTPApi();
            }
        });

        btnSendAgain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callResetPasswordAPI();
            }
        });
    }

    private void callResetPasswordAPI() {

        System.out.println("phone::" + phone);

        Map<String, String> map = new HashMap<String, String>();
        map.put("url", MyConstants.NURSE_BASE_URL + "sendResetPassword");
        map.put("Version", MyConstants.WS_VERSION);
        map.put("email", email);
        //map.put("Phone", phone);

        new CallRequest(ResetPasswordOTPActivity.this).sendResetPassword(map);

    }

    private void callResetPasswordOTPApi() {

        OTP = pinView.getText().toString();
        System.out.println("otp::" + OTP);

        if (TextUtils.isEmpty(OTP)) {
            Utils.showToast("Please enter valid OTP", ResetPasswordOTPActivity.this);
        } else {

            Map<String, String> map = new HashMap<String, String>();
            map.put("url", MyConstants.NURSE_BASE_URL + "resetPassword");
            map.put("NurseId", user_id);
            map.put("Version", MyConstants.WS_VERSION);
            map.put("OtpToken", OTP);

            new CallRequest(ResetPasswordOTPActivity.this).resetPassword(map);
        }
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        Utils.removeSimpleSpinProgressDialog();
        try {
            if (result != null && !result.isEmpty()) {
                Log.i("TAG", "TAG Result : " + result);
                //      {"document":{"response":{"status":1,"message":"Success."}}}
                switch (request) {
                    case resetPassword:
                        try {
                            JSONObject mainObj = new JSONObject(result);
                            if (mainObj.getString("error_code").equalsIgnoreCase("0")) {


                                new AlertDialog.Builder(ResetPasswordOTPActivity.this)
                                        .setMessage("We sent a temporary password to your email. Please use for next login. Kindly change password after login to ensure Safety.")
                                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                LoginActivity.mActivity.finish();
                                                ResetPasswordActivity.mActivity.finish();
                                                Intent intent = new Intent(ResetPasswordOTPActivity.this, LoginActivity.class);
                                                startActivity(intent);
                                                finish();
                                            }
                                        })
                                        .show();

                                //Utils.showToast(mainObj.getString("error_string"), ResetPasswordActivity.this);

                            } else {
                                Utils.showToast(mainObj.getString("error_string"), ResetPasswordOTPActivity.this);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;

                    case sendResetPassword:
                        try {
                            JSONObject mainObj = new JSONObject(result);
                            if (mainObj.getString("error_code").equalsIgnoreCase("0")) {

                                JSONObject resultObj = mainObj.getJSONArray("result").getJSONObject(0);

                                //String EmailToken = resultObj.getString("EmailToken");
                                user_id = resultObj.getString("id");
                                email = resultObj.getString("email");

                                phone = "";
                                if (resultObj.has("phone")) {
                                    phone = resultObj.getString("phone");
                                }

                                Utils.showToast("OTP sent successfully", ResetPasswordOTPActivity.this);

                            } else {
                                Utils.showToast(mainObj.getString("error_string"), ResetPasswordOTPActivity.this);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onProgressUpdate(String uniqueMessageId, int progres) {

    }

    @Override
    public void onProgressComplete(String uniqueMessageId, String result, Constant.REQUESTS request) {

    }
}
