package com.connex.nurse.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.connex.nurse.R;
import com.connex.nurse.interfaces.DashboardListener;
import com.connex.nurse.model.MessageHistory;
import com.connex.nurse.others.PicassoTrustAll;
import com.github.siyamed.shapeimageview.CircularImageView;
import com.squareup.picasso.Callback;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;

/**
 * Created by abc on 11/21/2017.
 */

public class PendingPatientAdapter extends RecyclerView.Adapter<PendingPatientAdapter.MyViewHolder> {

    Context mContext;
    List<MessageHistory> messagesList;
    private List<MessageHistory> searchDoctorFilterList = new ArrayList<MessageHistory>();
    DashboardListener dashboardListener;

    public PendingPatientAdapter(Fragment mContext, List<MessageHistory> messagesList) {
        this.mContext = mContext.getActivity();
        this.messagesList = messagesList;
        this.searchDoctorFilterList.addAll(messagesList);
        dashboardListener = (DashboardListener) mContext;

        setHasStableIds(true);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public PendingPatientAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_messages, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final PendingPatientAdapter.MyViewHolder holder, int position) {
        final MessageHistory messageHistory = messagesList.get(position);
        holder.tvDoctorName.setText(messageHistory.getFirstName() + " " + messageHistory.getLastName());
        holder.tvLastMessage.setText(messageHistory.getLastMessage());
        if (TextUtils.isEmpty(messageHistory.getCounter()) || messageHistory.getCounter().equalsIgnoreCase("0")) {
            holder.tvCount.setVisibility(View.GONE);
            System.out.println("counter 0");
        } else {
            System.out.println("counter > 0");
            holder.tvCount.setVisibility(View.VISIBLE);
            holder.tvCount.setText(messageHistory.getCounter());
        }
        holder.tvTime.setText(messageHistory.getTime());

        holder.ll_doctor.setVisibility(View.GONE);


        try {

            if (!messageHistory.getProfilePic().isEmpty()) {
                PicassoTrustAll.getInstance(mContext)
                        .load(messageHistory.getProfilePic())
                        .placeholder(R.drawable.avatar)
                        .error(R.drawable.avatar)
                        .into(holder.ivDoctor, new Callback() {
                            @Override
                            public void onSuccess() {
                            }

                            @Override
                            public void onError() {
                                holder.ivDoctor.setImageResource(R.drawable.avatar);
                            }
                        });
            } else {
                holder.ivDoctor.setImageResource(R.drawable.avatar);
            }
        } catch (Exception e) {
            e.printStackTrace();
            holder.ivDoctor.setImageResource(R.drawable.avatar);
        }
        //aq.id(holder.ivDoctor).progress(null).image( messageHistory.getProfilePic(), true, true, 0, R.drawable.avatar, null, 0, 0.0f);


        holder.itemView.setTag(position);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                int position = (int) view.getTag();
                MessageHistory messageHistory = messagesList.get(position);
                dashboardListener.onChatAssigned(messageHistory, position);

            }
        });

    }

    public void filters(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        messagesList.clear();
        if (charText.length() == 0) {
            messagesList.addAll(searchDoctorFilterList);

            sortingListBasedOnTime();
            //notifyDataSetChanged();
        } else {
            for (MessageHistory bean : searchDoctorFilterList) {
                String Contanint = bean.getFirstName();
                if (bean.getFirstName().toLowerCase(Locale.getDefault())
                        .contains(charText) || bean.getLastName().toLowerCase(Locale.getDefault()).contains(charText)) {
                    messagesList.add(bean);
                }
            }
            notifyDataSetChanged();
        }

    }

    public void sortingListBasedOnTime() {
        Collections.sort(messagesList, new Comparator<MessageHistory>() {
            public int compare(MessageHistory o1, MessageHistory o2) {
                if (o1.getTime() == null || o2.getTime() == null)
                    return 0;
                try {
                    return new SimpleDateFormat("yyyy-MM-dd hh:mm a", Locale.US).parse(o2.getTime()).compareTo(new SimpleDateFormat("yyyy-MM-dd hh:mm a", Locale.US).parse(o1.getTime()));
                } catch (ParseException e) {
                    e.printStackTrace();
                    return 0;
                }
            }
        });

        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return messagesList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        CircularImageView ivDoctor;
        TextView tvDoctorName, tvLastMessage, tvTime, tvCount, tvDoctor;
        LinearLayout ll_doctor;

        public MyViewHolder(View itemView) {
            super(itemView);
            ivDoctor = itemView.findViewById(R.id.ivDoctor);
            tvDoctorName = itemView.findViewById(R.id.tvDoctorName);
            tvLastMessage = itemView.findViewById(R.id.tvLastMessage);
            tvTime = itemView.findViewById(R.id.tvTime);
            tvCount = itemView.findViewById(R.id.tvCount);
            tvDoctor = itemView.findViewById(R.id.tvDoctor);
            ll_doctor = itemView.findViewById(R.id.ll_doctor);
        }
    }
}
