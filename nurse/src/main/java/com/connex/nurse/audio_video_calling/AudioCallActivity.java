package com.connex.nurse.audio_video_calling;

import android.content.Context;
import android.media.AudioManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.androidquery.AQuery;


import com.connex.nurse.R;
import com.connex.nurse.others.PicassoTrustAll;
import com.github.siyamed.shapeimageview.CircularImageView;
import com.sinch.android.rtc.AudioController;
import com.sinch.android.rtc.PushPair;
import com.sinch.android.rtc.SinchClient;
import com.sinch.android.rtc.calling.Call;
import com.sinch.android.rtc.calling.CallListener;
import com.squareup.picasso.Callback;

import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

public class AudioCallActivity extends BaseActivity {

    private Call call;
    private TextView callState;
    private SinchClient sinchClient;
    private Button button;
    private String callerId;
    private String recipientId;
    private String mCallId,recvImageUrl,recvName;
    private AudioPlayer mAudioPlayer;
    private Timer mTimer;
    static final String CALL_START_TIME = "callStartTime";
    static final String ADDED_LISTENER = "addedListener";
    private long mCallStart = 0;
    private boolean mAddedListener = false;
    private TextView mCallDuration;
    private TextView mCallerName;
    public CircularImageView img_recv_Profile;
    public AQuery aqList;
    public AQuery aq;
    private AudioCallActivity.UpdateCallDurationTask mDurationTask;
    public ImageView img_specker,img_mic;
    boolean isSpiker=true,isMute = false;

    @Override
    protected void onSaveInstanceState(Bundle savedInstanceState) {

        savedInstanceState.putLong(CALL_START_TIME, mCallStart);
        savedInstanceState.putBoolean(ADDED_LISTENER, mAddedListener);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        mCallStart = savedInstanceState.getLong(CALL_START_TIME);
        mAddedListener = savedInstanceState.getBoolean(ADDED_LISTENER);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.audio_call_activity);
        mAudioPlayer = new AudioPlayer(this);
        aqList = new AQuery(this);
        mCallId = getIntent().getStringExtra(SinchService.CALL_ID);
        recvImageUrl = getIntent().getStringExtra("recvImageUrl");
        recvName = getIntent().getStringExtra("recvName");
        mCallDuration = (TextView) findViewById(R.id.callDuration);

        img_mic = (ImageView)findViewById(R.id.img_mic);
        img_specker = (ImageView)findViewById(R.id.img_specker);
        mCallerName = (TextView) findViewById(R.id.remoteUser);
        img_recv_Profile = (CircularImageView) findViewById(R.id.img_recv_Profile);
        Log.i("TAG", "mCallId ID::-->" + mCallId);
        //sinchClient.getCallClient().addCallClientListener(new SinchCallClientListener());

        button = (Button) findViewById(R.id.button);
        callState = (TextView) findViewById(R.id.callState);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                endCall();

            }
        });
        ImageView endCallButton = (ImageView) findViewById(R.id.hangupButton);

        endCallButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                endCall();
            }
        });

        mCallerName.setText(recvName);
        aqList = new AQuery(this);
        aq = aqList.recycle(this.findViewById(android.R.id.content));

        try {
            PicassoTrustAll.getInstance(AudioCallActivity.this)
                    .load(recvImageUrl)
                    .error(R.drawable.avatar)
                    .into(img_recv_Profile, new Callback() {
                        @Override
                        public void onSuccess() {
                        }

                        @Override
                        public void onError() {
                        }
                    });
        } catch (Exception e){
            e.printStackTrace();
        }
        /*aq.id(img_recv_Profile).progress(null)
                .image(recvImageUrl, true, true, 0, R.drawable.avatar, null, 0, 1.0f);
 */
        if (savedInstanceState == null) {
            mCallStart = System.currentTimeMillis();
        }
        img_mic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switchAudio();
            }
        });
        img_specker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switchspeker();
            }
        });
      /*  Log.i("TAG","CALL ID::-->"+call);
        if (call == null) {
            call = sinchClient.getCallClient().callUser(recipientId);
            call.addCallListener(new SinchCallListener());
            button.setText("Hang Up");
        } else {
            finish();
        }*/
    }

    private class UpdateCallDurationTask extends TimerTask {

        @Override
        public void run() {
            AudioCallActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    updateCallDuration();
                }
            });
        }
    }

    private void updateCallDuration() {
        Log.i("TAG", "Call ::-->" + mCallStart);
        if (mCallStart > 0) {
            mCallDuration.setText(formatTimespan(System.currentTimeMillis() - mCallStart));

            Log.i("TAG", "Call Duration ::-->" + mCallDuration);

        }
    }

    private String formatTimespan(long timespan) {
        long totalSeconds = timespan / 1000;
        long minutes = totalSeconds / 60;
        long seconds = totalSeconds % 60;
        return String.format(Locale.US, "%02d:%02d", minutes, seconds);
    }

    //method to update live duration of the call
    @Override
    public void onStop() {
        super.onStop();
        try {
            mDurationTask.cancel();
            mTimer.cancel();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    //start the timer for the call duration here
    @Override
    public void onStart() {
        super.onStart();

        updateUI();
    }

    private void updateUI() {
        if (getSinchServiceInterface() == null) {
            return; // early
        }

        Call call = getSinchServiceInterface().getCall(mCallId);
        if (call != null) {
            mCallerName.setText(recvName);
            aqList = new AQuery(this);
            aq = aqList.recycle(this.findViewById(android.R.id.content));

            try {
                PicassoTrustAll.getInstance(AudioCallActivity.this)
                        .load(recvImageUrl)
                        .error(R.drawable.avatar)
                        .into(img_recv_Profile, new Callback() {
                            @Override
                            public void onSuccess() {
                            }

                            @Override
                            public void onError() {
                            }
                        });
            } catch (Exception e){
                e.printStackTrace();
            }

            /*aq.id(img_recv_Profile).progress(null)
                    .image(recvImageUrl, true, true, 0, R.drawable.avatar, null, 0, 1.0f);
 */
        }
    }

    @Override
    public void onBackPressed() {
        // UserDetail should exit activity by ending call, not by going back.
    }

    @Override
    public void onServiceConnected() {
        Call call = getSinchServiceInterface().getCall(mCallId);
        if (call != null) {

            call.addCallListener(new SinchCallListener());


        } else {

            Log.e("TAG", "Started with invalid callId, aborting.");
            finish();
        }


    }

    private void endCall() {
        mAudioPlayer.stopProgressTone();
        Call call = getSinchServiceInterface().getCall(mCallId);
        if (call != null) {
            call.hangup();
        }
        finish();
    }

    private class SinchCallListener implements CallListener {
        @Override
        public void onCallEnded(Call endedCall) {
            mAudioPlayer.stopProgressTone();
            call = null;
            endCall();
            finish();
            setVolumeControlStream(AudioManager.USE_DEFAULT_STREAM_TYPE);
        }

        @Override
        public void onCallEstablished(Call establishedCall) {
            mAudioPlayer.stopProgressTone();
            mTimer = new Timer();
            mDurationTask = new UpdateCallDurationTask();
            mTimer.schedule(mDurationTask, 0, 500);
            setVolumeControlStream(AudioManager.STREAM_VOICE_CALL);
            AudioController audioController = getSinchServiceInterface().getAudioController();
            audioController.enableSpeaker();
            mCallStart = System.currentTimeMillis();

            callState.setText("connected");
        }

        @Override
        public void onCallProgressing(Call progressingCall) {
            callState.setText("ringing");

            mAudioPlayer.playProgressTone();
        }

        @Override
        public void onShouldSendPushNotification(Call call, List<PushPair> pushPairs) {
        }
    }
    public void switchAudio() {
        AudioManager audioManager = ((AudioManager) AudioCallActivity.this.getApplicationContext().getSystemService(Context.AUDIO_SERVICE));

        if (isMute) {
            img_mic.setImageResource(R.drawable.mic_off);
            BaseActivity.getSinchServiceInterface().getAudioController().mute();
        } else {
            img_mic.setImageResource(R.drawable.mic_on);
            BaseActivity.getSinchServiceInterface().getAudioController().unmute();
        }

        isMute = !isMute;
    }
    public void switchspeker() {
        AudioManager audioManager = ((AudioManager) AudioCallActivity.this.getApplicationContext().getSystemService(Context.AUDIO_SERVICE));

        if (isSpiker) {
            img_specker.setImageResource(R.drawable.speaker_off);
            BaseActivity.getSinchServiceInterface().getAudioController().disableSpeaker();
        } else {
            img_specker.setImageResource(R.drawable.speaker_on);
            BaseActivity.getSinchServiceInterface().getAudioController().enableSpeaker();
        }

        isSpiker = !isSpiker;
    }
}

