package com.connex.nurse.audio_video_calling;

import android.content.Intent;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CaptureRequest;
import android.media.ImageReader;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.util.Log;
import android.util.Size;
import android.util.SparseIntArray;
import android.view.Surface;
import android.view.TextureView;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.androidquery.AQuery;


import com.connex.nurse.R;
import com.connex.nurse.custom_views.CircleImageView;
import com.connex.nurse.others.PicassoTrustAll;
import com.sinch.android.rtc.PushPair;
import com.sinch.android.rtc.calling.Call;
import com.sinch.android.rtc.calling.CallEndCause;
import com.sinch.android.rtc.video.VideoCallListener;
import com.sinch.android.rtc.video.VideoController;
import com.squareup.picasso.Callback;

import java.io.File;
import java.util.List;

public class IncomingCallScreenActivity extends BaseActivity {

    static final String TAG = IncomingCallScreenActivity.class.getSimpleName();
    private String mCallId, recvImageUrl, recvName;
    private AudioPlayer mAudioPlayer;
    SeekBar simpleSeekBar;
    boolean visible;
    ImageView call_ans, call_end;
    private static final SparseIntArray ORIENTATIONS = new SparseIntArray();

    static {
        ORIENTATIONS.append(Surface.ROTATION_0, 90);
        ORIENTATIONS.append(Surface.ROTATION_90, 0);
        ORIENTATIONS.append(Surface.ROTATION_180, 270);
        ORIENTATIONS.append(Surface.ROTATION_270, 180);
    }

    protected CameraDevice cameraDevice;
    protected CameraCaptureSession cameraCaptureSessions;
    protected CaptureRequest captureRequest;
    protected CaptureRequest.Builder captureRequestBuilder;
    private Size imageDimension;
    private ImageReader imageReader;
    private File file;
    private static final int REQUEST_CAMERA_PERMISSION = 200;
    private boolean mFlashSupported;
    private Handler mBackgroundHandler;
    private HandlerThread mBackgroundThread;
    private String cameraId;
    private TextureView textureView;
    private boolean mVideoViewsAdded = false;
    public CircleImageView img_recv_Profile;
    String TYPE_CALL = "";
    public TextView remoteUser;
    public AQuery aqList;
    public AQuery aq;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.incoming);

        recvImageUrl = getIntent().getStringExtra("profile_path");
        recvName = getIntent().getStringExtra("name");
        Log.i("TAG", "recvName ::->" + recvName);
        Log.i("TAG", "recvImageUrl _path ::->" + recvImageUrl);
        aqList = new AQuery(this);

        TYPE_CALL = getIntent().getStringExtra("TYPE_CALL");
        TextView answer = (TextView) findViewById(R.id.tv_ans);

        answer.setOnClickListener(mClickListener);
        TextView decline = (TextView) findViewById(R.id.tv_decline);
        decline.setOnClickListener(mClickListener);


        mAudioPlayer = new AudioPlayer(this);

        mAudioPlayer.playRingtone();
        mCallId = getIntent().getStringExtra(SinchService.CALL_ID);


        img_recv_Profile = (CircleImageView) findViewById(R.id.img_recv_Profile);
        remoteUser = (TextView) findViewById(R.id.remoteUser);


    }

    private void addVideoViews() {
        if (mVideoViewsAdded || getSinchServiceInterface() == null) {
            return; //early
        }

        final VideoController vc = getSinchServiceInterface().getVideoController();
        if (vc != null) {
            RelativeLayout localView = (RelativeLayout) findViewById(R.id.localVideo);
            localView.addView(vc.getLocalView());

            localView.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    //this toggles the front camera to rear camera and vice versa
                    vc.toggleCaptureDevicePosition();
                }
            });

            LinearLayout view = (LinearLayout) findViewById(R.id.remoteVideo);
            view.addView(vc.getRemoteView());
            mVideoViewsAdded = true;
        }
    }


    @Override
    protected void onServiceConnected() {
        Call call = getSinchServiceInterface().getCall(mCallId);
        if (call != null) {
            call.addCallListener(new SinchCallListener());
            remoteUser = (TextView) findViewById(R.id.remoteUser);
            remoteUser.setText(recvName);
            aqList = new AQuery(this);
            aq = aqList.recycle(this.findViewById(android.R.id.content));

            try {
                PicassoTrustAll.getInstance(IncomingCallScreenActivity.this)
                        .load(recvImageUrl)
                        .error(R.drawable.avatar)
                        .into(img_recv_Profile, new Callback() {
                            @Override
                            public void onSuccess() {
                            }

                            @Override
                            public void onError() {
                            }
                        });
            } catch (Exception e){
                e.printStackTrace();
            }

           /* aq.id(img_recv_Profile).progress(null)
                    .image(recvImageUrl, true, true, 0, R.drawable.avatar, null, 0, 1.0f);
*/

        } else {
            Log.e(TAG, "Started with invalid callId, aborting");
            finish();
        }
    }

    private void answerClicked() {
        mAudioPlayer.stopRingtone();
        Call call = getSinchServiceInterface().getCall(mCallId);

        String recvImageUrl = call.getHeaders().get("profile_pic");
        String recvName = call.getHeaders().get("name");
        if (call != null) {
            call.answer();
            if (TYPE_CALL.equalsIgnoreCase("Audio")) {
                Intent intent = new Intent(this, AudioCallActivity.class);
                intent.putExtra("recvImageUrl", recvImageUrl);
                intent.putExtra("recvName", recvName);
                intent.putExtra(SinchService.CALL_ID, mCallId);
                startActivity(intent);
            } else {
                Intent intent = new Intent(this, CallScreenActivity.class);
                intent.putExtra("recvImageUrl", recvImageUrl);
                intent.putExtra("recvName", recvName);
                intent.putExtra(SinchService.CALL_ID, mCallId);
                startActivity(intent);

            }
        } else {
            finish();
        }
    }

    private void declineClicked() {
        mAudioPlayer.stopRingtone();
        Call call = getSinchServiceInterface().getCall(mCallId);
        if (call != null) {
            call.hangup();
        }
        finish();
    }


    public class SinchCallListener implements VideoCallListener {

        @Override
        public void onCallEnded(Call call) {
            CallEndCause cause = call.getDetails().getEndCause();
            Log.d(TAG, "Call ended, cause: " + cause.toString());
            mAudioPlayer.stopRingtone();
            finish();
        }

        @Override
        public void onCallEstablished(Call call) {
            Log.d(TAG, "Call established");
        }

        @Override
        public void onCallProgressing(Call call) {
            Log.d(TAG, "Call progressing");
        }

        @Override
        public void onShouldSendPushNotification(Call call, List<PushPair> pushPairs) {
            // Send a push through your push provider here, e.g. GCM
        }

        @Override
        public void onVideoTrackAdded(Call call) {
            // Display some kind of icon showing it's a video call
        }

    }

    private OnClickListener mClickListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.tv_ans:
                    answerClicked();
                    break;
                case R.id.tv_decline:
                    declineClicked();
                    break;
            }
        }
    };


}
