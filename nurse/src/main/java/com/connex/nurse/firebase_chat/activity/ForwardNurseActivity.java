package com.connex.nurse.firebase_chat.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;

import com.connex.nurse.R;
import com.connex.nurse.activity.LoginActivity;
import com.connex.nurse.activity.NurseDashboardActivity;
import com.connex.nurse.firebase_chat.adapter.ForwardNurseAdapter;
import com.connex.nurse.firebase_chat.model.ForwardNurse;
import com.connex.nurse.interfaces.AsyncTaskListner;
import com.connex.nurse.interfaces.ForwardNurseListener;
import com.connex.nurse.others.App;
import com.connex.nurse.others.Internet;
import com.connex.nurse.ws.CallRequest;
import com.connex.nurse.ws.Constant;
import com.connex.nurse.ws.MyConstants;
import com.connex.nurse.ws.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ForwardNurseActivity extends AppCompatActivity implements ForwardNurseListener, AsyncTaskListner {

    private RecyclerView rvForwardNurse;
    private LinearLayout ivBack;
    public List<ForwardNurse> nurseList = new ArrayList<>();
    private ForwardNurseAdapter forwardNurseAdapter;
    String uniqueChatId = "";
    SharedPreferences sharedpreferences;
    SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forward_nurse);

        sharedpreferences = getSharedPreferences(MyConstants.PREF, Context.MODE_PRIVATE);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            uniqueChatId = bundle.getString("uniqueChatId");
        }

        rvForwardNurse = findViewById(R.id.rvForwardNurse);
        ivBack = findViewById(R.id.ivBack);

        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        GridLayoutManager gridLayoutManager = new GridLayoutManager(ForwardNurseActivity.this, 2);
        gridLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rvForwardNurse.setLayoutManager(gridLayoutManager);

        rvForwardNurse.setHasFixedSize(true);
        rvForwardNurse.setItemViewCacheSize(20);
        rvForwardNurse.setDrawingCacheEnabled(true);
        rvForwardNurse.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);

        getNurseList();

    }

    private void getNurseList() {
        if (!Internet.isAvailable(ForwardNurseActivity.this)) {
            Internet.showAlertDialog(ForwardNurseActivity.this, "Error!", "No Internet Connection", false);
            return;
        }

        Map<String, String> map = new HashMap<String, String>();
        map.put("url", MyConstants.NURSE_BASE_URL + "nurseList");
        map.put("NurseId", App.user.getUserID());
        map.put("ApiToken", MyConstants.API_TOKEN);
        map.put("Version", MyConstants.WS_VERSION);

        new CallRequest(ForwardNurseActivity.this).getNurseList(map);
    }

    @Override
    public void itemClicked(ForwardNurse forwardNurse) {
        forwardNurse(forwardNurse);
    }

    private void forwardNurse(ForwardNurse forwardNurse) {
        if (!Internet.isAvailable(ForwardNurseActivity.this)) {
            Internet.showAlertDialog(ForwardNurseActivity.this, "Error!", "No Internet Connection", false);
            return;
        }

        Map<String, String> map = new HashMap<String, String>();
        map.put("url", MyConstants.NURSE_BASE_URL + "forwardNurse");
        map.put("NurseId", App.user.getUserID());
        map.put("ApiToken", MyConstants.API_TOKEN);
        map.put("Version", MyConstants.WS_VERSION);
        map.put("UniqueId", uniqueChatId);
        map.put("ForwardId", forwardNurse.getId());

        new CallRequest(ForwardNurseActivity.this).forwardNurse(map);
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        try {
            Utils.hideProgressDialog();
            if (result != null && !result.isEmpty()) {
                Log.i("TAG", "TAG Result : " + result);
                nurseList.clear();
                switch (request) {

                    case getNurseList:
                        Utils.removeSimpleSpinProgressDialog();
                        try {
                            JSONObject mainObj = new JSONObject(result);
                            if (mainObj.getString("error_code").equalsIgnoreCase("0")) {
                                if (mainObj.getJSONArray("result") != null && mainObj.getJSONArray("result").length() > 0) {
                                    JSONArray array = mainObj.getJSONArray("result");
                                    for (int i = 0; i < array.length(); i++) {
                                        JSONObject object = array.getJSONObject(i);

                                        ForwardNurse forwardNurse = new ForwardNurse();
                                        forwardNurse.setId(object.getString("id"));
                                        forwardNurse.setFirstName(object.getString("FirstName"));
                                        forwardNurse.setLastName(object.getString("LastName"));
                                        forwardNurse.setProfilePic(object.getString("ProfilePic"));
                                        forwardNurse.setEmail(object.getString("email"));

                                        nurseList.add(forwardNurse);
                                    }

                                    forwardNurseAdapter = new ForwardNurseAdapter(ForwardNurseActivity.this, nurseList);
                                    rvForwardNurse.setAdapter(forwardNurseAdapter);

                                } else {
                                    Utils.showToast(mainObj.getString("error_string"), ForwardNurseActivity.this);
                                }
                            } else if (mainObj.getString("error_code").equalsIgnoreCase("1") ||
                                    mainObj.getString("error_code").equalsIgnoreCase("2")) {
                                editor = sharedpreferences.edit();
                                editor.clear();
                                editor.commit();

                                startActivity(new Intent(ForwardNurseActivity.this, LoginActivity.class));
                                ActivityCompat.finishAffinity(ForwardNurseActivity.this);
                            } else {
                                Utils.showToast(mainObj.getString("error_string"), ForwardNurseActivity.this);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        break;

                    case forwardNurse:
                        Utils.removeSimpleSpinProgressDialog();
                        try {
                            JSONObject mainObj = new JSONObject(result);
                            if (mainObj.getString("error_code").equalsIgnoreCase("0")) {
                                Intent intent = new Intent(ForwardNurseActivity.this, NurseDashboardActivity.class);
                                startActivity(intent);
                                MyConstants.isBackPressed = true;
                                ActivityCompat.finishAffinity(ForwardNurseActivity.this);
                            } else if (mainObj.getString("error_code").equalsIgnoreCase("1") ||
                                    mainObj.getString("error_code").equalsIgnoreCase("2")) {
                                editor = sharedpreferences.edit();
                                editor.clear();
                                editor.commit();

                                startActivity(new Intent(ForwardNurseActivity.this, LoginActivity.class));
                                ActivityCompat.finishAffinity(ForwardNurseActivity.this);
                            } else {
                                Utils.showToast(mainObj.getString("error_string"), ForwardNurseActivity.this);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        break;

                }
            }
        } catch (
                Exception e)

        {
            e.printStackTrace();
        }

    }

    @Override
    public void onProgressUpdate(String uniqueMessageId, int progres) {

    }

    @Override
    public void onProgressComplete(String uniqueMessageId, String result, Constant.REQUESTS request) {

    }


}
