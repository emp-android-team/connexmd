package com.connex.nurse.firebase_chat.activity;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.crashlytics.android.Crashlytics;


import com.connex.nurse.activity.LoginActivity;
import com.connex.nurse.utils.KeyboardUtils;

import com.connex.nurse.utils.WavAudioRecorder;

import com.connex.nurse.R;
import com.connex.nurse.audio_video_calling.AudioCallActivity;
import com.connex.nurse.audio_video_calling.BaseActivity;
import com.connex.nurse.audio_video_calling.CallScreenActivity;
import com.connex.nurse.audio_video_calling.SinchService;
import com.connex.nurse.custom_views.RobottoTextView;
import com.connex.nurse.firebase_chat.adapter.ChatAdapter;
import com.connex.nurse.firebase_chat.model.FireMessage;
import com.connex.nurse.firebase_chat.others.ChatConstants;
import com.connex.nurse.firebase_chat.service.MyDownloadService;
import com.connex.nurse.firebase_chat.service.MyUploadService;
import com.connex.nurse.interfaces.AsyncTaskListner;
import com.connex.nurse.others.App;
import com.connex.nurse.others.PicassoTrustAll;
import com.connex.nurse.utils.MyLayoutManager;
import com.connex.nurse.ws.CallRequest;
import com.connex.nurse.ws.Constant;
import com.connex.nurse.ws.MyConstants;
import com.connex.nurse.ws.Utils;
import com.github.siyamed.shapeimageview.CircularImageView;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseException;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.sinch.android.rtc.SinchError;
import com.sinch.android.rtc.calling.Call;
import com.squareup.picasso.Callback;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import io.fabric.sdk.android.Fabric;

public class NurseFireChatActivity extends BaseActivity implements SinchService.StartFailedListener, AsyncTaskListner {

    public final String IMG_TAG = "imageTag";
    public final String VIDEO_TAG = "videoTag";
    public final String AUDIO_TAG = "audioTag";

    public final String TAG = "tag";

    private FirebaseAuth mAuth;
    public ImageView imgSend, imgMic, imgCamera;
    public EditText etChat;
    public RobottoTextView tvTimer;

    private WavAudioRecorder mRecorder;
    public ChatAdapter cAdapter;
    public RecyclerView lstChat;
    public RelativeLayout relAudioPanel, relChatPanel;
    public String imgDecodableString;
    public static final int PiCK_IMAGE = 111;
    public static final int PiCK_AUDIO = 222;
    public static final int PICK_VIDEO = 333;
    public static final int TAKE_PICTURE = 444;
    public static final int TAKE_VIDEO = 555;
    public static final int DRAW_PICTURE = 666;
    public static final int DOCTOR_SUGGEST = 777;
    public boolean isStart = false;
    public MediaRecorder myAudioRecorder;
    public Handler mHandler, mStatusHandler;
    public Runnable mAudioRunnable, mStatusRunnable;
    public byte[] bytes;
    public Uri selectedUri;
    //public ImageView imgOffOn;
    public String FileType = "", FileName = "", booking_id = "", pt_id = "", pt_name = "", dr_name = "", dr_id = "", uniqueChatId = "";
    private static final String AUDIO_RECORDER_FOLDER = "dr_pocket/audio/";
    private static final String AUDIO_RECORDER_FILE_EXT_3GP = ".m4a";
    public boolean isLive = true;

    public static String[] thumbColumns = {MediaStore.Video.Thumbnails.DATA};
    public static String[] mediaColumns = {MediaStore.Video.Media._ID};

    public Constant.REQUESTS request;
    public Map<String, String> map;
    public String selectedType = "";
    public static String file;
    public long timer = 1;

    String senderImageUrl, recvImageUrl;
    public ImageView img_audio, img_video, img_end_chat, img_suggest_doctor;
    //public RobottoTextView tvEndChat;
    public App app;
    public RelativeLayout relChat;


    public DatabaseReference fireDB, fireDbMessages, fireDbTyping, fireDbOnlineOffline, fireChaneel, fireUnreadCounter;

    public LinearLayout relCamera;
    public View greenView;
    public View greyView;
    public CircularImageView imgRecvr;
    //public CircularImageView imgRecvr, imgSender;
    public AQuery aqList;
    public AQuery aq;
    public int drId = 0, ptId = 0;

    public RobottoTextView tvOppName, tvTyping;

    public LinearLayout imgBack;
    public Uri mFileUri;
    public FireMessage chatMessage;

    public String senderId = "";
    public String reciverId = "";
    public String str_name = "";
    public String myName = "";
    public String myPic = "";
    public String recvName = "";
    public String recvPic = "";
    public SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm a", Locale.US);
    public RatingBar simpleRatingBar;
    boolean isPaymentDone;
    SharedPreferences sharedpreferences;
    SharedPreferences.Editor editor;
    int unreadMessageCount = 0;
    int position, end;
    private Toolbar toolbar;
    MenuItem itemEndChat, itemSuggestDoctor;
    String IS_GUEST;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
     /*   getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);*/
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_chat_new);
        Utils.logUser();

        df.setTimeZone(TimeZone.getTimeZone("UTC"));

        app = App.getInstance();
        //   mRecorder = WavAudioRecorder.getInstanse();
        sharedpreferences = getSharedPreferences(MyConstants.PREF, Context.MODE_PRIVATE);
        if (getIntent().hasExtra(MyConstants.IS_PAYMENT_DONE)) {
            isPaymentDone = getIntent().getBooleanExtra(MyConstants.IS_PAYMENT_DONE, false);
        }

        if (getIntent().hasExtra("position")) {
            position = getIntent().getIntExtra("position", -1);
        }

        if (getIntent().hasExtra("end")) {
            end = getIntent().getIntExtra("end", -1);
        }

        pt_id = getIntent().getStringExtra(MyConstants.PT_ID);
        dr_id = getIntent().getStringExtra(MyConstants.DR_ID);
        senderId = pt_id;
        reciverId = dr_id;
        uniqueChatId = getIntent().getStringExtra(MyConstants.UNIQUE_CHAT_ID);
        dr_name = getIntent().getStringExtra(MyConstants.DR_NAME).trim();
        recvName = dr_name;
        recvImageUrl = getIntent().getStringExtra(MyConstants.RECEIVER_IMAGE_URL);
        IS_GUEST = getIntent().getStringExtra("IS_GUEST");

        tvOppName = (RobottoTextView) findViewById(R.id.tvOppName);
        tvTyping = findViewById(R.id.tvTyping);
        //img_end_chat = findViewById(R.id.img_end_chat);
        //img_suggest_doctor = findViewById(R.id.img_suggest_doctor);
        etChat = (EditText) findViewById(R.id.etChat);
        lstChat = (RecyclerView) findViewById(R.id.lstChat);
        relChat = (RelativeLayout) findViewById(R.id.relChat);
        imgBack = findViewById(R.id.img_left_arrow);
        imgSend = (ImageView) findViewById(R.id.imgSend);
        imgCamera = (ImageView) findViewById(R.id.imgCamera);
        img_audio = (ImageView) findViewById(R.id.img_audio);
        img_video = (ImageView) findViewById(R.id.img_video);
        imgMic = (ImageView) findViewById(R.id.imgMic);
        tvTimer = (RobottoTextView) findViewById(R.id.tvTimer);
        relAudioPanel = (RelativeLayout) findViewById(R.id.relAudioPanel);
        relChatPanel = (RelativeLayout) findViewById(R.id.relChatPanel);
        relCamera = (LinearLayout) findViewById(R.id.relCameraButtons);
        imgRecvr = (CircularImageView) findViewById(R.id.imgRecvr);
        greenView = (View) findViewById(R.id.greenView);
        greyView = (View) findViewById(R.id.greyView);
        toolbar = findViewById(R.id.toolbar);

        toolbar.inflateMenu(R.menu.menu_nurse);
        Menu menu = toolbar.getMenu();
        itemEndChat = menu.findItem(R.id.endChat);
        itemSuggestDoctor = menu.findItem(R.id.suggestDoctor);

        if (end == 1) {
            relChatPanel.setVisibility(View.GONE);
            img_audio.setVisibility(View.GONE);
            //img_end_chat.setVisibility(View.GONE);
            itemEndChat.setVisible(false);
            itemSuggestDoctor.setVisible(false);
            toolbar.setVisibility(View.GONE);
            //img_suggest_doctor.setVisibility(View.GONE);
            img_video.setVisibility(View.GONE);
        } else {
            relChatPanel.setVisibility(View.VISIBLE);
            // TODO itemSuggestDoctor.setVisible(true);
            itemSuggestDoctor.setVisible(true);
            toolbar.setVisibility(View.VISIBLE);
            //img_suggest_doctor.setVisibility(View.VISIBLE);
            //img_end_chat.setVisibility(View.VISIBLE);
            itemEndChat.setVisible(true);
            img_audio.setVisibility(View.GONE);
            img_video.setVisibility(View.GONE);
        }

        Utils.checkPermission(this);

        System.out.println("doctor name::" + dr_name);

        if (TextUtils.isEmpty(dr_name) || dr_name == null) {
            tvOppName.setText("Care Team");
        } else {
            tvOppName.setText(dr_name);
        }

        mAuth = FirebaseAuth.getInstance();
        signInAnonymously();
        senderImageUrl = App.user.getProfileUrl();

        System.out.println("profile url::" + recvImageUrl);

        try {
            if (recvImageUrl != null && !TextUtils.isEmpty(recvImageUrl)) {

                PicassoTrustAll.getInstance(NurseFireChatActivity.this)
                        .load(recvImageUrl)
                        .error(R.drawable.avatar)
                        .into(imgRecvr, new Callback() {
                            @Override
                            public void onSuccess() {
                            }

                            @Override
                            public void onError() {
                            }
                        });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                if (item.getItemId() == R.id.forwardNurse) {
                    Intent intent = new Intent(NurseFireChatActivity.this, ForwardNurseActivity.class);
                    intent.putExtra("uniqueChatId", uniqueChatId);
                    startActivity(intent);
                } else if (item.getItemId() == R.id.endChat) {
                    new AlertDialog.Builder(NurseFireChatActivity.this)
                            .setTitle("Alert!")
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .setMessage("Do you want to end Consult with " + recvName + "?")
                            .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    changeAssignStatus();
                                    //new CallRequest(NurseFireChatActivity.this).endConsult(senderId, uniqueChatId);
                                    dialog.dismiss();
                                }
                            })
                            .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            })
                            .show();
                } else if (item.getItemId() == R.id.suggestDoctor) {
                    Intent intent = new Intent(NurseFireChatActivity.this, SuggestDoctorActivity.class);
                    startActivityForResult(intent, DOCTOR_SUGGEST);
                }
                return false;
            }
        });


        fireDB = FirebaseDatabase.getInstance().getReference().child(MyConstants.NURSE_CHANNEL);

        fireChaneel = fireDB.child(uniqueChatId);
        fireDbMessages = fireChaneel.child(ChatConstants.FIRE_DB_MESSAGES);
        fireDbTyping = fireChaneel.child(ChatConstants.FIRE_DB_TYPING);
        fireDbOnlineOffline = fireChaneel.child(ChatConstants.FIRE_DB_ONLINE);
        fireUnreadCounter = fireChaneel.child(ChatConstants.FIRE_UNREAD_COUNTER);

        /*img_suggest_doctor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(NurseFireChatActivity.this, SuggestDoctorActivity.class);
                startActivityForResult(intent, DOCTOR_SUGGEST);
            }
        });*/

        /*img_end_chat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new AlertDialog.Builder(NurseFireChatActivity.this)
                        .setTitle("Alert!")
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setMessage("Do you want to end Consult with " + recvName + "?")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                changeAssignStatus();
                                //new CallRequest(NurseFireChatActivity.this).endConsult(senderId, uniqueChatId);
                                dialog.dismiss();
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })
                        .show();
            }
        });*/

        img_audio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String recipientName = str_name;
                HashMap<String, String> headers = new HashMap<>();
                headers.put("name", App.user.getName());
                headers.put("profile_path", App.user.getProfileUrl());
                Log.i("TAG", "MY NAME ::->" + headers.get("name"));

                Log.i("TAG", "MY profile_path ::->" + headers.get("profile_path"));
                try {
                    Call call = BaseActivity.getSinchServiceInterface().callUser(recipientName, headers);

                    String callId = call.getCallId();
                    Intent callScreen = new Intent(NurseFireChatActivity.this, AudioCallActivity.class);
                    callScreen.putExtra("recvImageUrl", recvImageUrl);
                    callScreen.putExtra("recvName", tvOppName.getText().toString());
                    callScreen.putExtra(SinchService.CALL_ID, callId);
                    startActivity(callScreen);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        img_video.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.i("TAG", "NAme" + str_name);
                HashMap<String, String> map = new HashMap<>();
                map.put("name", App.user.getName());
                map.put("profile_path", App.user.getProfileUrl());
                try {
                    Call call = BaseActivity.getSinchServiceInterface().callUserVideo(str_name, map);
                    String callId = call.getCallId();

                    Intent callScreen = new Intent(NurseFireChatActivity.this, CallScreenActivity.class);
                    callScreen.putExtra("recvImageUrl", recvImageUrl);
                    callScreen.putExtra("recvName", tvOppName.getText().toString());
                    callScreen.putExtra(SinchService.CALL_ID, callId);
                    startActivity(callScreen);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });


        setZeroCounter(reciverId);

        cAdapter = new ChatAdapter(this, app.fireChatArray, senderId, reciverId);
        lstChat.setAdapter(cAdapter);

        LayoutAnimationController controller = AnimationUtils.loadLayoutAnimation(lstChat.getContext(), R.anim.layout_animation_fall_down);
        lstChat.setLayoutAnimation(controller);
        lstChat.scheduleLayoutAnimation();

        MyLayoutManager linearLayoutManager = new MyLayoutManager(this);
        lstChat.setLayoutManager(linearLayoutManager);

        createDirectories();
        Utils.showProgressDialog(this);
        setDatabaseListner();

        mHandler = new Handler();

        mAudioRunnable = new Runnable() {
            @Override
            public void run() {

                long minutes = TimeUnit.MILLISECONDS
                        .toMinutes(timer * 1000);
                long seconds = TimeUnit.MILLISECONDS
                        .toSeconds(timer * 1000);

                tvTimer.setText(addZero(minutes) + ":" + addZero(seconds));

                timer++;
                mHandler.postDelayed(this, 1000);

            }
        };

        /*if (isPaymentDone) {
            Log.i("doctor pocket", "Going to check");

            String messageDefault1 = "Automatic Notification: Hi, we hope to make things super easy for both the patient (YOU!) and the Doctor. You can start typing answers or send things like, how long have you been experiencing these symptoms? Do you have pictures that would help explain your situation better? Or would you like to send a short video recording?";

            String messageDefault2 = "Automatic Notification: Don't worry your messages will be saved here, and your doctor will respond within his usual response time!";

            sendDefaultMessage(messageDefault1);
            sendDefaultMessage(messageDefault2);

            Log.i("doctor pocket", "Going to send message");
        }*/

        imgSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i("doctor pocket", "Going to check");

                if (!etChat.getText().toString().trim().isEmpty())
                    sendMessage(etChat.getText().toString().trim());
                Log.i("doctor pocket", "Going to send message");

            }
        });

        imgCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(etChat.getWindowToken(), 0);
                selectImage();
            }
        });


        imgMic.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:

                        startRecording();
                        return true; // if you want to handle the touch event
                    case MotionEvent.ACTION_UP:

                        timer = 0;
                        tvTimer.setText("");
                        mHandler.removeCallbacks(mAudioRunnable);

                        relChatPanel.setVisibility(View.VISIBLE);
                        relAudioPanel.setVisibility(View.GONE);

                        stopRecording();
                        return true; // if you want to handle the touch event
                }
                return false;
            }
        });
        etChat.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (s.toString().trim().length() > 0) {
                    relCamera.setVisibility(View.GONE);
                    imgSend.setVisibility(View.VISIBLE);
                } else {
                    relCamera.setVisibility(View.VISIBLE);
                    imgSend.setVisibility(View.GONE);
                }

                if (s.length() > 0){
                    setTypingIndicator(true);
                } else {
                    setTypingIndicator(false);
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        /*etChat.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                setTypingIndicator(hasFocus);
            }
        });*/

        KeyboardUtils.addKeyboardToggleListener(this, new KeyboardUtils.SoftKeyboardToggleListener() {
            @Override
            public void onToggleSoftKeyboard(boolean isVisible) {
                if (isVisible) {
                    lstChat.scrollToPosition(cAdapter.getItemCount() - 1);
                }

            }
        });

    }

    private void changeAssignStatus() {

        Map<String, String> map = new HashMap<String, String>();
        map.put("url", MyConstants.NURSE_BASE_URL + "changeAssignStatus");
        map.put("NurseId", App.user.getUserID());
        map.put("ApiToken", MyConstants.API_TOKEN);
        map.put("Version", MyConstants.WS_VERSION);
        map.put("UniqueId", uniqueChatId);
        map.put("Status", "2");

        new CallRequest(NurseFireChatActivity.this).changeAssignStatusActivity(map);
    }

    private void getNurseChatDetails() {
        Map<String, String> map = new HashMap<String, String>();

        if (MyConstants.isGuest.equalsIgnoreCase("1")) {
            map.put("url", MyConstants.GUEST_BASE_URL + "nurseChat");
        } else {
            map.put("url", MyConstants.BASE_URL + "nurseChat");
        }
        map.put("ApiToken", MyConstants.API_TOKEN);
        map.put("Version", MyConstants.WS_VERSION);
        map.put("UserId", App.user.getUserID());

        new CallRequest(NurseFireChatActivity.this).getNurseChatDetails(map);
    }


    @Override
    public void onBackPressed() {
        /*if (isPaymentDone) {
            Intent intent = new Intent(NurseFireChatActivity.this, PTDashboardActivity.class);
            startActivity(intent);
            ActivityCompat.finishAffinity(NurseFireChatActivity.this);
        } else {*/
        try {
            InputMethodManager inputMethodManager = (InputMethodManager)
                    getSystemService(Context.INPUT_METHOD_SERVICE);
            if (inputMethodManager != null) {
                inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        super.onBackPressed();
        MyConstants.isBackPressed = true;
        //}
    }

    public void setZeroCounter(String reciverId) {

        Map<String, Object> unreadCounter = new HashMap<>();
        unreadCounter.put(reciverId, 0);
        fireUnreadCounter.updateChildren(unreadCounter);

        /*Map<String, Object> lastValue = new HashMap<>();
        lastValue.put("last_message", "");
        lastValue.put("last_date", "");
        lastValue.put("profile_pic", senderImageUrl);
        lastValue.put("is_asked", "y");
        lastValue.put("ConsultId", uniqueChatId);
        fireChaneel.updateChildren(lastValue);*/
    }

    @Override
    protected void onServiceConnected() {
        img_audio.setEnabled(true);
        img_video.setEnabled(true);

        try {
            if (!TextUtils.isEmpty(App.user.getUserID())) {
                if (MyConstants.isGuest.equalsIgnoreCase("0")) {
                    App.user.setSinch_id(sharedpreferences.getString(MyConstants.SINCH_ID, ""));
                }
                System.out.println("sinch id::" + App.user.getSinch_id());
                getSinchServiceInterface().startClient(App.user.getSinch_id());
               // getSinchServiceInterface().registerPushToken(this);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroy() {
        /*if (getSinchServiceInterface() != null) {
            getSinchServiceInterface().stopClient();
        }*/
        super.onDestroy();
    }

    // [START write_fan_out]
    public void sendNewMessage(FireMessage chat) {
        try {
            String key = fireDbMessages.push().getKey();

            Map<String, Object> postValues = chat.toMap();
            Map<String, Object> childUpdates = new HashMap<>();
            childUpdates.put(key, postValues);
            fireDbMessages.updateChildren(childUpdates);
            Log.i(IMG_TAG, "Image is going to send message firebase done");
            Map<String, Object> lastValue = new HashMap<>();
            lastValue.put("last_message", chat.text);
            lastValue.put("last_date", chat.date);
            fireChaneel.updateChildren(lastValue);


            if (!isLive) {
                unreadMessageCount++;
            } else {
                unreadMessageCount = 0;
            }

            Map<String, Object> unreadCounter = new HashMap<>();
            unreadCounter.put(senderId, unreadMessageCount);
            fireUnreadCounter.updateChildren(unreadCounter);

            etChat.setText("");
            cAdapter.notifyDataSetChanged();
            lstChat.scrollToPosition(cAdapter.getItemCount() - 1);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setChannelName(FireMessage chat) {
        try {
            String key = fireDbMessages.push().getKey();

            Map<String, Object> postValues = chat.toMap();

            Map<String, Object> childUpdates = new HashMap<>();
            childUpdates.put(key, postValues);
            fireDbMessages.updateChildren(childUpdates);

            etChat.setText("");
            cAdapter.notifyDataSetChanged();
            ///lstChat.scrollToPosition(cAdapter.getItemCount() - 1);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void setOnlineOffline(boolean isLive) {
        try {
            String key = fireDbOnlineOffline.child(senderId).getKey();


            Map<String, Object> postValues = new HashMap<>();
            if (isLive) {
                postValues.put(senderId, "1");
            } else {
                postValues.put(senderId, "0");
            }
            Map<String, Object> childUpdates = new HashMap<>();
            childUpdates.put(key, postValues);
            fireDbOnlineOffline.updateChildren(postValues);


        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void signInAnonymously() {
        // Sign in anonymously. Authentication is required to read or write from Firebase Storage.
        Utils.showProgressDialog(this, "Accessing Databse");
        mAuth.signInAnonymously()
                .addOnSuccessListener(this, new OnSuccessListener<AuthResult>() {
                    @Override
                    public void onSuccess(AuthResult authResult) {
                        Log.d(TAG, "signInAnonymously:SUCCESS");
                        Utils.hideProgressDialog();

                    }
                })
                .addOnFailureListener(this, new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception exception) {
                        Log.e(TAG, "signInAnonymously:FAILURE", exception);
                        Utils.hideProgressDialog();

                    }
                });
    }

    public void setTypingIndicator(boolean isLive) {
        try {
            String key = fireDbTyping.child(senderId).getKey();
            Map<String, Object> postValues = new HashMap<>();
            if (isLive) {
                postValues.put(senderId, "1");
            } else {
                postValues.put(senderId, "0");
            }

            fireDbTyping.updateChildren(postValues);
            lstChat.scrollToPosition(cAdapter.getItemCount() - 1);

         /*   lstChat.post(new Runnable() {
                @Override
                public void run() {
                    lstChat.scrollToPosition(cAdapter.getItemCount());
                }
            });*/


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setDatabaseListner() {


        fireDbMessages.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(com.google.firebase.database.DataSnapshot dataSnapshot, String s) {
                try {
                    app.fireChatArray.add(dataSnapshot.getValue(FireMessage.class));
                    playTone(true);

                } catch (DatabaseException e) {
                    e.printStackTrace();
                }

                if (cAdapter != null && app.fireChatArray.size() > 0) {
                    cAdapter.notifyDataSetChanged();
                }


                lstChat.scrollToPosition(cAdapter.getItemCount() - 1);
            }

            @Override
            public void onChildChanged(com.google.firebase.database.DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(com.google.firebase.database.DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(com.google.firebase.database.DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        fireDbMessages.addValueEventListener(
                new com.google.firebase.database.ValueEventListener() {
                    @Override
                    public void onDataChange(com.google.firebase.database.DataSnapshot dataSnapshot) {

                        Utils.hideProgressDialog();
                        ;
                        // Get user value
                        Log.i(AUDIO_TAG, "FireMessage: getting data snapstp : " + dataSnapshot.getChildrenCount());

                        app.fireChatArray.clear();


                        for (com.google.firebase.database.DataSnapshot dsp : dataSnapshot.getChildren()) {
                            Log.i(AUDIO_TAG, "FireMessage: getting data snapstp single");
                            try {
                                app.fireChatArray.add(dsp.getValue(FireMessage.class));
                            } catch (DatabaseException e) {
                                e.printStackTrace();
                            }
                        }
                        if (cAdapter != null && app.fireChatArray.size() > 0) {
                            cAdapter.notifyDataSetChanged();
                        }


                        lstChat.scrollToPosition(cAdapter.getItemCount() - 1);

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        Utils.hideProgressDialog();
                        ;
                        Log.w(AUDIO_TAG, "getUser:onCancelled", databaseError.toException());

                    }
                });


        fireDbTyping.addValueEventListener(
                new com.google.firebase.database.ValueEventListener() {
                    @Override
                    public void onDataChange(com.google.firebase.database.DataSnapshot dataSnapshot) {
                        Log.i(AUDIO_TAG, "FireMessage:  dbTyping  getting data snapstp : " + dataSnapshot.getChildrenCount());

                        if (dataSnapshot.hasChild(reciverId)) {
                            String onOff = dataSnapshot.child(reciverId).getValue(String.class);
                            if (onOff.equals("1")) {
                                tvTyping.setVisibility(View.VISIBLE);
                            } else {
                                tvTyping.setVisibility(View.GONE);
                            }
                        }
                        lstChat.scrollToPosition(cAdapter.getItemCount() - 1);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        Log.w(AUDIO_TAG, "getUser:onCancelled", databaseError.toException());

                    }
                });

        fireDbOnlineOffline.addValueEventListener(
                new com.google.firebase.database.ValueEventListener() {
                    @Override
                    public void onDataChange(com.google.firebase.database.DataSnapshot dataSnapshot) {

                        Log.i(AUDIO_TAG, "FireMessage:  dbOnIff  getting data snapstp : " + dataSnapshot.getChildrenCount());

                        if (dataSnapshot.hasChild(reciverId)) {
                            Log.i(AUDIO_TAG, "FireMessage:  dbOnIff  : " + dataSnapshot.child(reciverId).getValue(String.class));
                            String onOff = dataSnapshot.child(reciverId).getValue(String.class);
                            if (onOff.equals("1")) {
                                isLive = true;
                                unreadMessageCount = 0;
                                greenView.setVisibility(View.VISIBLE);
                                greyView.setVisibility(View.GONE);
                            } else {
                                isLive = false;
                                greenView.setVisibility(View.GONE);
                                greyView.setVisibility(View.VISIBLE);
                            }
                        } else {
                            isLive = false;
                            greenView.setVisibility(View.GONE);
                            greyView.setVisibility(View.VISIBLE);
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        Log.w(AUDIO_TAG, "getUser:onCancelled", databaseError.toException());

                    }
                });
        setOnlineOffline(true);

    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        //Checking the request code of our request
        if (requestCode == Utils.RECORD_AUDDIO) {

            //If permission is granted
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                startRecording();
            } else {
                //Displaying another toast if permission is not granted

            }
        } else if (requestCode == Utils.CAMERA_PERMISSION) {

            //If permission is granted
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                if (selectedType.equalsIgnoreCase("Take Photo")) {
                    File f = new File(Environment.getExternalStorageDirectory() + "/dr_pocket/images");
                    if (!f.exists()) {
                        f.mkdirs();
                    }
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    File file = new File(Environment.getExternalStorageDirectory(), "dr_pocket/images/img_" + System.currentTimeMillis() + ".jpg");
                    selectedUri = Uri.fromFile(file);
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, selectedUri);

                    startActivityForResult(intent, TAKE_PICTURE);
                } else {

                    File f = new File(Environment.getExternalStorageDirectory() + "/dr_pocket/video");
                    if (!f.exists()) {
                        f.mkdirs();

                    }
                    Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
                    File file = new File(Environment.getExternalStorageDirectory(), "dr_pocket/video/vid__" + System.currentTimeMillis() + ".mp4");
                    selectedUri = Uri.fromFile(file);
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, selectedUri);

                    startActivityForResult(intent, TAKE_VIDEO);
                }
            } else {
                Utils.showToast("Permission not granted", NurseFireChatActivity.this);

            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        //new CallRequest(NurseFireChatActivity.this).checkCancelStatus(uniqueChatId, App.user.getUser_Type(), senderId);

        setOnlineOffline(true);

    }

    @Override
    protected void onPause() {
        if (mStatusHandler != null && mStatusRunnable != null) {
            mStatusHandler.removeCallbacks(mStatusRunnable);
        }

        setOnlineOffline(false);
        setTypingIndicator(false);

        super.onPause();
    }

    public void startRecording() {
        try {

            boolean result2 = Utils.checkPermission(NurseFireChatActivity.this);

            boolean result = Utils.checkAudioPermission(NurseFireChatActivity.this);
            if (result && result2) {
                isStart = true;
                relChatPanel.setVisibility(View.GONE);
                relAudioPanel.setVisibility(View.VISIBLE);
                timer = 0;
                tvTimer.setText("00:00");
                mHandler.postDelayed(mAudioRunnable, 1000);
                file = getFilename();


                myAudioRecorder = new MediaRecorder();
                myAudioRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
                myAudioRecorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
                myAudioRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);
//                myAudioRecorder.setAudioEncoder(MediaRecorder.getAudioSourceMax());
                myAudioRecorder.setAudioEncodingBitRate(16);
                myAudioRecorder.setAudioSamplingRate(44100);
                file = getFilename();
                myAudioRecorder.setOutputFile(file);

                myAudioRecorder.setOnErrorListener(errorListener);
                myAudioRecorder.setOnInfoListener(infoListener);


                myAudioRecorder.prepare();
                myAudioRecorder.start();

                //Utils.showToast("Recording Started", FireChatActivity.this);
            } else {
                Utils.showToast("Permission not granted", NurseFireChatActivity.this);
            }
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public void stopRecording() {

        try {
            if (null != myAudioRecorder) {
                myAudioRecorder.stop();

                myAudioRecorder.reset();

                isStart = false;
                // m.start();
                imgDecodableString = file;
                Log.i("Tag", "Path :" + file);
                String pure = file.replace("/storage", "");
                int bytesRead;

                File f = new File(file);
                FileInputStream is = new FileInputStream(f);
                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                int size = is.available();
                byte[] buffer = new byte[size];

                while ((bytesRead = is.read(buffer)) != -1) {
                    bos.write(buffer, 0, bytesRead);
                }
                is.close();
                bytes = bos.toByteArray();
                System.out.println(" One Recording values is: " + bytes + " \n");

                FileType = "AudioFiles";

                Calendar c = Calendar.getInstance();
                df.setTimeZone(TimeZone.getTimeZone("UTC"));
                System.out.println("Current time => " + c.getTime());

                String formattedDate = df.format(c.getTime());

                chatBean = new FireMessage();

                chatBean.date = formattedDate;
                chatBean.localPath = imgDecodableString;
                chatBean.isReceived = false;
                chatBean.senderId = senderId;
                chatBean.isSent = true;
                chatBean.chatID = System.currentTimeMillis() + "";
                app.fireChatArray.add(chatBean);
                selectedUri = Uri.fromFile(f);
                if (selectedUri != null) {
                    uploadFromUri(chatBean, selectedUri, ChatConstants.FILE_TYPE_AUDIO);
                } else {
                    Log.w(IMG_TAG, "File URI is null");
                }
                cAdapter.notifyDataSetChanged();
                relAudioPanel.setVisibility(View.GONE);
                relChatPanel.setVisibility(View.VISIBLE);
                ///lstChat.scrollToPosition(cAdapter.getItemCount() - 1);
            }

        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (IllegalThreadStateException e) {
            e.printStackTrace();
        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (RuntimeException ex) {
            //Ignore
        }

    }

    public String addZero(long l) {
        return l > 9 ? l + "" : "0" + l;
    }


    private String getFilename() {
        String filepath = Environment.getExternalStorageDirectory().getPath();
        File file = new File(filepath, AUDIO_RECORDER_FOLDER);

        if (!file.exists()) {
            file.mkdirs();
        }
        FileName = System.currentTimeMillis() + AUDIO_RECORDER_FILE_EXT_3GP;
        return (file.getAbsolutePath() + "/" + System.currentTimeMillis() + AUDIO_RECORDER_FILE_EXT_3GP);
    }

    private MediaRecorder.OnErrorListener errorListener = new MediaRecorder.OnErrorListener() {
        @Override
        public void onError(MediaRecorder mr, int what, int extra) {
            /*Toast.makeText(FireChatActivity.this,
                    "Error: " + what + ", " + extra, Toast.LENGTH_SHORT).show();*/
            Toast.makeText(app, "Hold to record, release to send", Toast.LENGTH_SHORT).show();
        }
    };

    private MediaRecorder.OnInfoListener infoListener = new MediaRecorder.OnInfoListener() {
        @Override
        public void onInfo(MediaRecorder mr, int what, int extra) {
            /*Toast.makeText(NurseFireChatActivity.this,
                    "Warning: " + what + ", " + extra, Toast.LENGTH_SHORT)
                    .show();*/
        }
    };

    private void sendDefaultMessage(String message) {
        try {
            Calendar c = Calendar.getInstance();
            df.setTimeZone(TimeZone.getTimeZone("UTC"));
            System.out.println("Current time => " + c.getTime());
            String formattedDate = df.format(c.getTime());
            chatBean = new FireMessage();
            chatBean.text = message;
            chatBean.date = formattedDate;
            chatBean.isSent = true;
            chatBean.isReceived = false;
            chatBean.senderId = senderId;

            app.fireChatArray.add(chatBean);


            try {
                String key = fireDbMessages.push().getKey();

                Map<String, Object> postValues = chatBean.toMap();
                Map<String, Object> childUpdates = new HashMap<>();
                childUpdates.put(key, postValues);
                fireDbMessages.updateChildren(childUpdates);

                Map<String, Object> lastValue = new HashMap<>();
                lastValue.put("last_message", chatBean.text);
                lastValue.put("last_date", chatBean.date);
                lastValue.put("profile_pic", senderImageUrl);
                lastValue.put("is_asked", "y");
                lastValue.put("ConsultId", uniqueChatId);
                fireChaneel.updateChildren(lastValue);


                if (!isLive) {
                    unreadMessageCount++;
                } else {
                    unreadMessageCount = 0;
                }

                Map<String, Object> unreadCounter = new HashMap<>();
                unreadCounter.put(senderId, unreadMessageCount);
                fireUnreadCounter.updateChildren(unreadCounter);

                etChat.setText("");
                cAdapter.notifyDataSetChanged();
                lstChat.scrollToPosition(cAdapter.getItemCount() - 1);
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (!isLive) {
                sendNotificationMessage(message, false);
            }

            playTone(false);

            /*app.fireChatArray.add(chatBean);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(etChat.getWindowToken(), 0);
                    cAdapter.notifyDataSetChanged();
                     lstChat.smoothScrollToPosition(cAdapter.getItemCount() - 1);

                    etChat.setText("");
                }
            });*/


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void sendMessage(String message) {

        Log.i("doctor pocket", "Going to sendMessage");
        sendTextMessage(message);
    }

    public FireMessage chatBean;


    public void sendTextMessage(String message) {
        Log.i("doctor pocket", "Going to sendTextMessage");
        try {
            Calendar c = Calendar.getInstance();
            df.setTimeZone(TimeZone.getTimeZone("UTC"));
            System.out.println("Current time => " + c.getTime());
            String formattedDate = df.format(c.getTime());
            chatBean = new FireMessage();
            chatBean.text = message;
            chatBean.date = formattedDate;
            chatBean.isSent = true;
            chatBean.isReceived = false;
            chatBean.senderId = senderId;
            app.fireChatArray.add(chatBean);
            sendNewMessage(chatBean);

            if (!isLive) {
                sendNotificationMessage(message, false);
            }

            playTone(false);

            /*app.fireChatArray.add(chatBean);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(etChat.getWindowToken(), 0);
                    cAdapter.notifyDataSetChanged();
                     lstChat.smoothScrollToPosition(cAdapter.getItemCount() - 1);

                    etChat.setText("");
                }
            });*/


        } catch (Exception e) {
            e.printStackTrace();
        }


    }


    public void selectImage() {
        final CharSequence[] items = {"Take Photo", "Capture Video", "Choose Photo from Library", "Choose Video from Library", "Drawing Tool",
                "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(NurseFireChatActivity.this, R.style.MyDialogTheme);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                boolean result = Utils.checkCameraPermission(NurseFireChatActivity.this);
                if (items[item].equals("Take Photo")) {
                    selectedType = "Take Photo";
                    boolean result2 = Utils.checkPermission(NurseFireChatActivity.this);
                    if (result && result2) {

                        File f2 = new File(Environment.getExternalStorageDirectory() + "/dr_pocket/images");
                        if (!f2.exists()) {
                            f2.mkdirs();
                        }

                        /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                            StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
                            StrictMode.setVmPolicy(builder.build());
                            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                            File file = new File(Environment.getExternalStorageDirectory(), "dr_pocket/images/img_" + System.currentTimeMillis() + ".jpg");
                            intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                            selectedUri = FileProvider.getUriForFile(getApplicationContext(), BuildConfig.APPLICATION_ID + ".provider", file);
                            intent.putExtra(MediaStore.EXTRA_OUTPUT, selectedUri);
                            startActivityForResult(intent, TAKE_PICTURE);
                        } else {*/
                        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        File file = new File(Environment.getExternalStorageDirectory(), "dr_pocket/images/img_" + System.currentTimeMillis() + ".jpg");
                        selectedUri = Uri.fromFile(file);
                        intent.putExtra(MediaStore.EXTRA_OUTPUT, selectedUri);
                        startActivityForResult(intent, TAKE_PICTURE);
                        //}
                    }

                } else if (items[item].equals("Capture Video")) {
                    selectedType = "Take Photo";
                    boolean result2 = Utils.checkPermission(NurseFireChatActivity.this);
                    if (result && result2) {

                        File f = new File(Environment.getExternalStorageDirectory() + "/dr_pocket/video");
                        if (!f.exists()) {
                            f.mkdirs();

                        }

                        /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                            StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
                            StrictMode.setVmPolicy(builder.build());
                            Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
                            File file = new File(Environment.getExternalStorageDirectory(), "dr_pocket/video/vid__" + System.currentTimeMillis() + ".mp4");
                            intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                            selectedUri = FileProvider.getUriForFile(getApplicationContext(), BuildConfig.APPLICATION_ID + ".provider", file);
                            intent.putExtra(MediaStore.EXTRA_OUTPUT, selectedUri);
                            startActivityForResult(intent, TAKE_VIDEO);
                        } else {*/
                        Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
                        File file = new File(Environment.getExternalStorageDirectory(), "dr_pocket/video/vid__" + System.currentTimeMillis() + ".mp4");
                        selectedUri = Uri.fromFile(file);
                        intent.putExtra(MediaStore.EXTRA_OUTPUT, selectedUri);
                        startActivityForResult(intent, TAKE_VIDEO);
                        //}
                    }
                } else if (items[item].equals("Choose Video from Library")) {
                    if (result) {
                        try {
                            Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Video.Media.EXTERNAL_CONTENT_URI);
                            intent.setType("video/*");
                            startActivityForResult(Intent.createChooser(intent, "Select Video"), PICK_VIDEO);
                        } catch (Exception e) {
                            Log.d("EXChooseVideo", e.getMessage());
                        }
                    }
                } else if (items[item].equals("Choose Photo from Library")) {

                    if (result) {
                        Intent intent = new Intent(Intent.ACTION_PICK,
                                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                     /*   intent.setType("image*//*");
                        intent.setAction(Intent.ACTION_GET_CONTENT);//*/
                        startActivityForResult(Intent.createChooser(intent, "Select File"), PiCK_IMAGE);

                    }

                } else if (items[item].equals("Drawing Tool")) {
                    Intent intent = new Intent(NurseFireChatActivity.this, DrawingToolActivity.class);
                    startActivityForResult(intent, DRAW_PICTURE);
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == TAKE_VIDEO && resultCode == RESULT_OK) {
            Calendar c = Calendar.getInstance();
            //SimpleDateFormat df = new SimpleDateFormat("hh:mm a");
            df.setTimeZone(TimeZone.getTimeZone("UTC"));
            String formattedDate = df.format(c.getTime());

            chatBean = new FireMessage();
            chatBean.videoURL = "video";

            //   chatBean.message = message;
            chatBean.date = formattedDate;
            chatBean.localPath = getPath(selectedUri);
            chatBean.isReceived = false;
            chatBean.isSent = false;
            chatBean.senderId = senderId;
            chatBean.chatID = System.currentTimeMillis() + "";
            app.fireChatArray.add(chatBean);

            Log.i(IMG_TAG, "Image is going to Select : " + selectedUri);
            if (selectedUri != null) {
                uploadFromUri(chatBean, selectedUri, ChatConstants.FILE_TYPE_VIDEO);
            } else {
                Log.w(IMG_TAG, "File URI is null");
            }
            cAdapter.notifyDataSetChanged();
            ///lstChat.scrollToPosition(cAdapter.getItemCount() - 1);
        }

        if (requestCode == TAKE_PICTURE && resultCode == RESULT_OK) {
            Calendar c = Calendar.getInstance();
            df.setTimeZone(TimeZone.getTimeZone("UTC"));
            System.out.println("Current time => " + c.getTime());
            String formattedDate = df.format(c.getTime());

            chatBean = new FireMessage();
            chatBean.photoURL = "image";
            chatBean.date = formattedDate;
            chatBean.localPath = getPath(selectedUri);
            chatBean.isReceived = false;
            chatBean.isSent = false;
            chatBean.senderId = senderId;
            chatBean.chatID = System.currentTimeMillis() + "";
            app.fireChatArray.add(chatBean);

            Log.i(IMG_TAG, "Image is going to Select : " + selectedUri);
            if (selectedUri != null) {
                uploadFromUri(chatBean, selectedUri, ChatConstants.FILE_TYPE_IMAGE);
            } else {
                Log.w(IMG_TAG, "File URI is null");
            }

            cAdapter.notifyDataSetChanged();
            ///lstChat.scrollToPosition(cAdapter.getItemCount() - 1);

        }

        if (requestCode == DRAW_PICTURE) {
            if (data != null) {
                String filepath = data.getStringExtra("filepath");
                System.out.println("path******" + filepath);

                Calendar c = Calendar.getInstance();
                df.setTimeZone(TimeZone.getTimeZone("UTC"));
                System.out.println("Current time => " + c.getTime());

                String formattedDate = df.format(c.getTime());
                chatBean = new FireMessage();
                chatBean.photoURL = "image";
                chatBean.date = formattedDate;
                chatBean.localPath = filepath;
                chatBean.isReceived = false;
                chatBean.isSent = false;
                chatBean.senderId = senderId;
                chatBean.chatID = System.currentTimeMillis() + "";
                app.fireChatArray.add(chatBean);
                selectedUri = Uri.fromFile(new File(filepath));
                if (selectedUri != null) {
                    uploadFromUri(chatBean, selectedUri, ChatConstants.FILE_TYPE_IMAGE);
                } else {
                    Log.w(IMG_TAG, "File URI is null");
                }

                cAdapter.notifyDataSetChanged();
            }
        }

        if (requestCode == DOCTOR_SUGGEST) {
            if (data != null) {
                String doctorId = data.getStringExtra("doctorId");
                String doctorURL = data.getStringExtra("doctorURL");
                String doctorName = data.getStringExtra("doctorName");
                String rating = data.getStringExtra("rating");
                String responseTime = data.getStringExtra("response_time");
                String speciality = data.getStringExtra("speciality");
                String consult_charge = data.getStringExtra("consult_charge");
                System.out.println("doctor path******" + doctorURL);

                Calendar c = Calendar.getInstance();
                df.setTimeZone(TimeZone.getTimeZone("UTC"));
                System.out.println("Current time => " + c.getTime());

                String formattedDate = df.format(c.getTime());
                chatBean = new FireMessage();
                chatBean.date = formattedDate;
                chatBean.isReceived = false;
                chatBean.isSent = false;
                chatBean.doctorId = doctorId;
                chatBean.doctorName = doctorName;
                chatBean.doctorURL = doctorURL;
                chatBean.consultCharge = consult_charge;
                chatBean.rating = rating;
                chatBean.senderId = senderId;
                chatBean.speciality = speciality;
                chatBean.responseTime = responseTime;
                chatBean.sended = "DoctorSuggest";
                app.fireChatArray.add(chatBean);

                sendSuggestDoctorMessage(chatBean);
                cAdapter.notifyDataSetChanged();
            }
        }

        if ((requestCode == PiCK_IMAGE || requestCode == PICK_VIDEO) && resultCode == RESULT_OK) {

            Calendar c = Calendar.getInstance();
            df.setTimeZone(TimeZone.getTimeZone("UTC"));
            System.out.println("Current time => " + c.getTime());

            selectedUri = data.getData();
            String formattedDate = df.format(c.getTime());
            chatBean = new FireMessage();
            if (requestCode == PICK_VIDEO) {
                chatBean.videoURL = "video";
            } else {
                chatBean.photoURL = "image";
            }
            chatBean.date = formattedDate;
            chatBean.localPath = getRealVideoPathFromUri(selectedUri);
            chatBean.isReceived = false;
            chatBean.isSent = false;
            chatBean.senderId = senderId;
            chatBean.chatID = System.currentTimeMillis() + "";
            app.fireChatArray.add(chatBean);

            if (requestCode == PICK_VIDEO) {
                uploadFromUri(chatBean, selectedUri, ChatConstants.FILE_TYPE_VIDEO);
            } else {
                uploadFromUri(chatBean, selectedUri, ChatConstants.FILE_TYPE_IMAGE);
            }
            cAdapter.notifyDataSetChanged();
            ///lstChat.scrollToPosition(cAdapter.getItemCount() - 1);

        }
    }


    public String getPath(Uri uri) {
        File myFile = new File(uri.getPath());
        myFile.getAbsolutePath();
        return myFile.getAbsolutePath();
    }

    public String getRealImagePath(Uri contentURI) {
        String result;
        Cursor cursor = getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) { // Source is Dropbox or other similar local file path
            result = contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            result = cursor.getString(idx);
            cursor.close();
        }
        return result;
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    public String getRealVideoPathFromUri(Uri contentURI) {
        String[] projection = {MediaStore.Video.Media.DATA};
        Cursor cursor = managedQuery(contentURI, projection, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Video.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);

    }


    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        try {
            if (result != null && !result.isEmpty()) {
                Log.i("TAG", "TAG Result : " + result);
                //      {"document":{"response":{"status":1,"message":"Success."}}}
                switch (request) {
                    case cancelChat:
                        Utils.removeSimpleSpinProgressDialog();
                        try {
                            JSONObject obj = new JSONObject(result);

                            String error_code = obj.getString("error_code");

                            if (error_code.equalsIgnoreCase("0")) {

                                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                                builder.setTitle("Thank you for using Doctor Pocket");
                                builder.setMessage("You've ended your virtual consult.")
                                        .setCancelable(false)
                                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {

                                                /*Intent intent = new Intent(NurseFireChatActivity.this, PTDashboardActivity.class);
                                                startActivity(intent);
                                                ActivityCompat.finishAffinity(NurseFireChatActivity.this);*/
                                            }
                                        });
                                AlertDialog alert = builder.create();
                                alert.show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;

                    case nurseChatDetails:
                        try {
                            JSONObject mainObj = new JSONObject(result);
                            if (mainObj.getString("error_code").equalsIgnoreCase("0")) {
                                //Utils.showToast(mainObj.getString("error_string"), GuestVerifyOTPActivity.this);

                                JSONObject resultObj = mainObj.getJSONArray("result").getJSONObject(0);

                                String id = resultObj.getString("id");
                                String UniqueId = resultObj.getString("UniqueId");
                                String UserId = resultObj.getString("UserId");
                                String NurseId = resultObj.getString("NurseId");
                                String Status = resultObj.getString("Status");
                                String IsGuest = resultObj.getString("IsGuest");

                                uniqueChatId = UniqueId;
                                pt_id = UserId;
                                dr_id = NurseId;
                                senderId = UserId;

                                tvOppName.setText("Care Team");


                            } else {
                                Utils.showToast(mainObj.getString("error_string"), NurseFireChatActivity.this);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;

                    case rate_doctor:
                        Utils.removeSimpleSpinProgressDialog();
                        JSONObject obj = new JSONObject(result);
                        try {

                            if (obj.getJSONObject("document").getJSONObject("response").getString("status").equals("1")) {

                                Utils.showToast(obj.getJSONObject("document").getJSONObject("response").getString("message"), this);
                            } else {
                                Utils.showToast(obj.getJSONObject("document").getJSONObject("response").getString("message"), this);
                            }

                        } catch (JSONException e) {
                            Utils.showToast(obj.getJSONObject("document").getJSONObject("response").getString("message"), this);

                            e.printStackTrace();
                        }
                        break;

                    case checkCancelStatus:
                        Utils.removeSimpleSpinProgressDialog();
                        try {
                            obj = new JSONObject(result);

                            if (obj.getString("error_code").equalsIgnoreCase("0")) {

                                JSONObject object = obj.getJSONArray("result").getJSONObject(0);

                                String Status = object.getString("Status");

                                if (Status.equalsIgnoreCase("1")) {
                                    relChatPanel.setVisibility(View.GONE);
                                    img_audio.setVisibility(View.GONE);
                                    img_end_chat.setVisibility(View.GONE);
                                    img_video.setVisibility(View.GONE);
                                }
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;

                    case changeAssignStatus:
                        Utils.removeSimpleSpinProgressDialog();
                        try {
                            JSONObject mainObj = new JSONObject(result);
                            if (mainObj.getString("error_code").equalsIgnoreCase("0")) {
                                //Utils.showToast(mainObj.getString("error_string"), GuestVerifyOTPActivity.this);

                                MyConstants.completeChatList.add(MyConstants.myPatientList.get(position));
                                MyConstants.myPatientList.remove(position);

                                onBackPressed();

                            } else if (mainObj.getString("error_code").equalsIgnoreCase("1") ||
                                    mainObj.getString("error_code").equalsIgnoreCase("2")) {
                                editor = sharedpreferences.edit();
                                editor.clear();
                                editor.commit();

                                Utils.showToast(mainObj.getString("error_string"), NurseFireChatActivity.this);

                                startActivity(new Intent(NurseFireChatActivity.this, LoginActivity.class));
                                ActivityCompat.finishAffinity(NurseFireChatActivity.this);
                            } else {
                                Utils.showToast(mainObj.getString("error_string"), NurseFireChatActivity.this);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;
                }

            } else {
                //  Utils.showToast("Please try again later", this);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    public void sendNotificationMessage(String message, boolean isShow) {

        String sender_id = "", user_type = "", is_live = "", reciver_id = "";


        sender_id = pt_id;
        user_type = "N";
        reciver_id = dr_id;


        String social_id = sharedpreferences.getString(MyConstants.SOCIAL_ID, "0");
        String social_type = sharedpreferences.getString(MyConstants.LOGIN_TYPE, "0");

        new CallRequest(this).sendOfflineNotificationNurse(message, sender_id, reciver_id, user_type, social_id, social_type, IS_GUEST);


    }

    @Override
    public void onProgressUpdate(String uniqueMessageId, int progres) {

    }

    @Override
    public void onProgressComplete(String uniqueMessageId, String result, Constant.REQUESTS
            request) {


    }

    public void sendSuggestDoctorMessage(FireMessage chatBean) {
        Log.i("Suggest doc", "Suggest doctor is going to send message firebase");
        try {
            sendNewMessage(chatBean);
            if (!isLive) {
                if (App.user.getUser_Type().equalsIgnoreCase(MyConstants.USER_DR)) {
                    sendNotificationMessage("Your patient send an image", false);
                } else {
                    sendNotificationMessage(dr_name + " suggested a doctor", false);
                }
            }

            Map<String, Object> lastValue = new HashMap<>();
            lastValue.put("last_message", "Suggested a doctor");
            lastValue.put("last_date", chatBean.date);
            fireChaneel.updateChildren(lastValue);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void sendImageMessage(FireMessage chatBean) {
        Log.i(IMG_TAG, "Image is going to send message firebase");
        try {
            Calendar c = Calendar.getInstance();
            df.setTimeZone(TimeZone.getTimeZone("UTC"));
            System.out.println("Current time => " + c.getTime());

            String formattedDate = df.format(c.getTime());
            chatBean.date = formattedDate;
            chatBean.senderId = senderId;
            chatBean.isSent = true;

            chatBean.isReceived = false;

            sendNewMessage(chatBean);
            if (!isLive) {
                if (App.user.getUser_Type().equalsIgnoreCase(MyConstants.USER_DR)) {
                    sendNotificationMessage("Your patient send an image", false);
                } else {
                    sendNotificationMessage(dr_name + " send an image", false);
                }
            }


            Map<String, Object> lastValue = new HashMap<>();
            lastValue.put("last_message", "Sent Photo");
            lastValue.put("last_date", chatBean.date);
            fireChaneel.updateChildren(lastValue);
            Utils.removeSimpleSpinProgressDialog();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    public void sendVideoMessage(FireMessage chatBean) {
        Log.i("doctor pocket", "Going to sendTextMessage");


        try {
            Calendar c = Calendar.getInstance();
            df.setTimeZone(TimeZone.getTimeZone("UTC"));
            System.out.println("Current time => " + c.getTime());

            String formattedDate = df.format(c.getTime());
            chatBean.date = formattedDate;
            chatBean.isSent = true;
            chatBean.isReceived = false;
            chatBean.senderId = senderId;

            sendNewMessage(chatBean);
            if (!isLive) {
                if (App.user.getUser_Type().equalsIgnoreCase(MyConstants.USER_DR)) {
                    sendNotificationMessage("Your patient send a Video", false);
                } else {
                    sendNotificationMessage(dr_name + " send a Video", false);
                }
            }
            Map<String, Object> lastValue = new HashMap<>();
            lastValue.put("last_message", "Sent Video");
            lastValue.put("last_date", chatBean.date);
            fireChaneel.updateChildren(lastValue);


            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    cAdapter.notifyDataSetChanged();
                    ///lstChat.scrollToPosition(cAdapter.getItemCount() - 1);

                    etChat.setText("");
                }
            });

            Utils.removeSimpleSpinProgressDialog();
        } catch (Exception e) {
            e.printStackTrace();
        }


    }


    public void sendAudioMessage(FireMessage chatBean) {
        Log.i("doctor pocket", "Going to send Audio MEssage");

        try {

            Calendar c = Calendar.getInstance();
            df.setTimeZone(TimeZone.getTimeZone("UTC"));
            System.out.println("Current time => " + c.getTime());

            String formattedDate = df.format(c.getTime());
            chatBean.date = formattedDate;
            chatBean.isSent = true;
            chatBean.isReceived = false;


            sendNewMessage(chatBean);
            if (!isLive) {
                if (App.user.getUser_Type().equalsIgnoreCase(MyConstants.USER_DR)) {
                    sendNotificationMessage("Your patient send a Voice note", false);
                } else {
                    sendNotificationMessage(dr_name + " send a Voice note", false);
                }
            }


            Map<String, Object> lastValue = new HashMap<>();
            lastValue.put("last_message", "Sent Audio");
            lastValue.put("last_date", chatBean.date);
            fireChaneel.updateChildren(lastValue);

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    cAdapter.notifyDataSetChanged();
                    ///lstChat.scrollToPosition(cAdapter.getItemCount() - 1);

                    etChat.setText("");
                }
            });
            Utils.removeSimpleSpinProgressDialog();

        } catch (Exception e) {
            e.printStackTrace();
        }


    }


    public void showDoctorCancelAlert(String dr_name) {

        AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.myDialog));
        builder.setTitle("Thank you for using Doctor Pocket");
        builder.setMessage(dr_name + " has ended the consult.")
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        NurseFireChatActivity.this.finish();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();

    }

    public void showPatientCanceledAlert() {

        AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.myDialog));
        builder.setTitle("Thank you for using Doctor Pocket");
        builder.setMessage("Your doctor has ended the consult.")
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        NurseFireChatActivity.this.finish();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();

    }

    public BroadcastReceiver mImageBroadCastreciver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d(IMG_TAG, "onReceive:" + intent);
            String type = intent.getStringExtra(ChatConstants.FILE_TYPE);

            switch (intent.getAction()) {
                case MyDownloadService.DOWNLOAD_COMPLETED:
                    // Get number of bytes downloaded
                    String downloadPath = intent.getStringExtra(MyDownloadService.EXTRA_DOWNLOAD_PATH);
                    String savePath = intent.getStringExtra(ChatConstants.SAVE_PATH);

                    for (FireMessage f : app.fireChatArray) {
                        if (f.photoURL.equalsIgnoreCase(downloadPath) || f.videoURL.equalsIgnoreCase(downloadPath) || f.voiceURL.equalsIgnoreCase(downloadPath)) {
                            final File downloadedFile = new File(savePath);
                            if(downloadedFile.exists()){
                                f.localPath = savePath;
                                f.isSent = true;
                            }
                        }
                    }
                    cAdapter.notifyDataSetChanged();

                    break;
                case MyDownloadService.DOWNLOAD_ERROR:
                    downloadPath = intent.getStringExtra(MyDownloadService.EXTRA_DOWNLOAD_PATH);

                    for (FireMessage f : app.fireChatArray) {
                        if (f.photoURL.equalsIgnoreCase(downloadPath) || f.videoURL.equalsIgnoreCase(downloadPath) || f.voiceURL.equalsIgnoreCase(downloadPath)) {
                            f.isError = true;
                        }
                    }

                    cAdapter.notifyDataSetChanged();

                    break;
                case MyUploadService.UPLOAD_COMPLETED:

                    //Utils.removeSimpleSpinProgressDialog();

                    Log.i(IMG_TAG, "Image is uploaded");

                    uniqueChatId = intent.getStringExtra(ChatConstants.UNIQUE_ID);
                    Uri fileUri = intent.getParcelableExtra(MyUploadService.EXTRA_FILE_URI);
                    String donwnloadURL = intent.getStringExtra(MyUploadService.EXTRA_DOWNLOAD_URL);

                    type = intent.getStringExtra(ChatConstants.FILE_TYPE);
                    for (FireMessage f : app.fireChatArray) {
                        if (f.chatID.equalsIgnoreCase(uniqueChatId)) {
                            f.isSent = true;

                            switch (type) {
                                case "audio":
                                    if (donwnloadURL.contains(".mp3")) {
                                        donwnloadURL = donwnloadURL.replace(".mp3", "");
                                    }
                                    f.voiceURL = donwnloadURL.toString();
                                    sendAudioMessage(f);
                                    break;
                                case "video":
                                    f.videoURL = donwnloadURL.toString();
                                    sendVideoMessage(f);
                                    break;
                                case "image":
                                    Log.i(IMG_TAG, "Image is going to send message :" + donwnloadURL.toString());
                                    f.photoURL = donwnloadURL.toString();
                                    sendImageMessage(f);
                                    break;
                            }
                        }
                    }

                    cAdapter.notifyDataSetChanged();
                    break;
                case MyUploadService.UPLOAD_ERROR:

                    Utils.removeSimpleSpinProgressDialog();

                    for (FireMessage f : app.fireChatArray) {
                        if (f.chatID.equalsIgnoreCase(uniqueChatId)) {
                            f.isError = true;
                        }
                    }
                    cAdapter.notifyDataSetChanged();
                    break;
                case MyUploadService.TRANSFERING:
                    int percentage = intent.getIntExtra(ChatConstants.PERCENTAGE, 0);
                    for (FireMessage f : app.fireChatArray) {
                        if (f.chatID.equalsIgnoreCase(uniqueChatId)) {
                            f.percentage = percentage;
                        }
                    }
                    cAdapter.notifyDataSetChanged();
                    break;
            }
        }
    };


    private void uploadFromUri(FireMessage fireMessage, Uri fileUri, String fileType) {

        Utils.showSimpleSpinProgressDialog(NurseFireChatActivity.this, "Uploading");

        Log.i(IMG_TAG, "Image is going to upload");
        Log.d(IMG_TAG, "uploadFromUri:src:" + fileUri.toString());

        // Save the File URI
        mFileUri = fileUri;
        startService(new Intent(this, MyUploadService.class)
                .putExtra(MyUploadService.EXTRA_FILE_URI, fileUri)
                .putExtra(ChatConstants.FILE_TYPE, fileType)
                .putExtra(ChatConstants.UNIQUE_ID, fireMessage.chatID)
                .setAction(MyUploadService.ACTION_UPLOAD));
    }

    @Override
    public void onStart() {
        super.onStart();


        // Register receiver for uploads and downloads
        LocalBroadcastManager manager = LocalBroadcastManager.getInstance(this);
        manager.registerReceiver(mImageBroadCastreciver, MyDownloadService.getIntentFilter());
        manager.registerReceiver(mImageBroadCastreciver, MyUploadService.getIntentFilter());
    }

    @Override
    public void onStop() {
        super.onStop();

        // Unregister download receiver
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mImageBroadCastreciver);
    }

    @Override
    public void onSaveInstanceState(Bundle out) {
       /* out.putParcelable(KEY_FILE_URI, mFileUri);
        out.putParcelable(KEY_DOWNLOAD_URL, mDownloadUrl);*/
    }


    public static String getThumbnailPathForLocalFile(Activity context,
                                                      Uri fileUri) {

        long fileId = getFileId(context, fileUri);

        MediaStore.Video.Thumbnails.getThumbnail(context.getContentResolver(),
                fileId, MediaStore.Video.Thumbnails.MICRO_KIND, null);

        Cursor thumbCursor = null;
        try {

            thumbCursor = context.managedQuery(
                    MediaStore.Video.Thumbnails.EXTERNAL_CONTENT_URI,
                    thumbColumns, MediaStore.Video.Thumbnails.VIDEO_ID + " = "
                            + fileId, null, null);

            if (thumbCursor.moveToFirst()) {
                String thumbPath = thumbCursor.getString(thumbCursor
                        .getColumnIndex(MediaStore.Video.Thumbnails.DATA));

                return thumbPath;
            }

        } finally {
        }

        return null;
    }

    public static long getFileId(Activity context, Uri fileUri) {

        Cursor cursor = context.managedQuery(fileUri, mediaColumns, null, null,
                null);

        if (cursor.moveToFirst()) {
            int columnIndex = cursor
                    .getColumnIndexOrThrow(MediaStore.Video.Media._ID);
            int id = cursor.getInt(columnIndex);

            return id;
        }

        return 0;
    }


    public void createDirectories() {
        try {
            File f = new File(Environment.getExternalStorageDirectory() + "/dr_pocket/images");
            if (!f.exists()) {
                f.mkdirs();
            }

            File f2 = new File(Environment.getExternalStorageDirectory() + "/dr_pocket/video");
            if (!f2.exists()) {
                f2.mkdirs();

            }

            File f3 = new File(Environment.getExternalStorageDirectory() + "/dr_pocket/audio");
            if (!f3.exists()) {
                f3.mkdirs();

            }

            File f4 = new File(Environment.getExternalStorageDirectory() + "/dr_pocket/drawings");
            if (!f4.exists()) {
                f4.mkdirs();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void playTone(boolean isRecieve) {
      /*  MediaPlayer mediaPlayer;

        if (isRecieve) {
            mediaPlayer = MediaPlayer.create(getApplicationContext(), R.raw.recive);
        } else {
            mediaPlayer = MediaPlayer.create(getApplicationContext(), R.raw.send);
        }
        try {
            mediaPlayer.setAudioStreamType(AudioManager.STREAM_NOTIFICATION);
            mediaPlayer.prepare();
            mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {

                @Override
                public void onCompletion(MediaPlayer mp) {
                    mp.release();
                }
            });
            mediaPlayer.start();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (SecurityException e) {
            e.printStackTrace();
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }*/
    }

    @Override
    public void onStartFailed(SinchError error) {

    }

    @Override
    public void onStarted() {

    }
}
