package com.connex.nurse.firebase_chat.adapter;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.AudioManager;
import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.SeekBar;

import com.androidquery.AQuery;


import com.connex.nurse.R;
import com.connex.nurse.custom_views.RobottoTextView;
import com.connex.nurse.firebase_chat.model.FireMessage;
import com.connex.nurse.firebase_chat.others.ChatConstants;
import com.connex.nurse.firebase_chat.service.MyDownloadService;
import com.connex.nurse.firebase_chat.view_holders.ViewHolder;
import com.connex.nurse.firebase_chat.view_holders.ViewHolderAudio;
import com.connex.nurse.firebase_chat.view_holders.ViewHolderSuggestDoctor;
import com.connex.nurse.firebase_chat.view_holders.ViewHolderTEXT;
import com.connex.nurse.firebase_chat.view_holders.ViewHolderTyping;
import com.connex.nurse.firebase_chat.view_holders.ViewHolderVideo;
import com.connex.nurse.others.App;
import com.connex.nurse.others.PicassoTrustAll;
import com.connex.nurse.ws.Utils;
import com.nostra13.universalimageloader.cache.disc.impl.UnlimitedDiskCache;
import com.nostra13.universalimageloader.cache.disc.naming.HashCodeFileNameGenerator;
import com.nostra13.universalimageloader.cache.memory.impl.LruMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.nostra13.universalimageloader.core.decode.BaseImageDecoder;
import com.nostra13.universalimageloader.core.download.BaseImageDownloader;
import com.nostra13.universalimageloader.utils.StorageUtils;
import com.squareup.picasso.Callback;

import java.io.File;
import java.io.FileInputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

public class ChatAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public List<FireMessage> chatArray = Collections.emptyList();
    public Context context;
    public View v;
    public AQuery aqList;
    public String senderId, receiverId;

    public static final int SEND_TEXT = 0;
    public static final int SEND_IMAGE = 1;
    public static final int SEND_VIDEO = 2;
    public static final int SEND_AUDIO = 3;
    public static final int SEND_SUGGEST_DOCTOR = 9;

    public static final int RECV_TEXT = 4;
    public static final int RECV_IMAGE = 5;
    public static final int RECV_VIDEO = 6;
    public static final int RECV_AUDIO = 7;
    public static final int RECV_SUGGEST_DOCTOR = 10;

    public static final int TYPING = 8;
    public Handler seekHandler;
    public AudioManager audioManager;
    DisplayImageOptions options;

    public ChatAdapter(Context context, ArrayList<FireMessage> chatArray, String senderId, String receiverId) {

        Log.i("Fire", "Fire sender id :" + senderId);
        this.chatArray = chatArray;
        this.context = context;
        this.senderId = senderId;
        this.receiverId = receiverId;
        this.aqList = new AQuery(context);
        this.audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);

        options = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .resetViewBeforeLoading(false)  // default
                .delayBeforeLoading(1000)
                .bitmapConfig(Bitmap.Config.ARGB_8888)
                .imageScaleType(ImageScaleType.IN_SAMPLE_POWER_OF_2)
                .build();

        File cacheDir = StorageUtils.getOwnCacheDirectory(context, "dr_pocket/cache");

        // Create global configuration and initialize ImageLoader with this config
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context)
                .memoryCacheExtraOptions(480, 800) // default = device screen dimensions
                .diskCacheExtraOptions(480, 800, null)
                .threadPoolSize(3) // default
                .threadPriority(Thread.NORM_PRIORITY - 2) // default
                .tasksProcessingOrder(QueueProcessingType.FIFO) // default
                .denyCacheImageMultipleSizesInMemory()
                .memoryCache(new LruMemoryCache(2 * 1024 * 1024))
                .memoryCacheSize(2 * 1024 * 1024)
                .memoryCacheSizePercentage(13) // default
                .diskCache(new UnlimitedDiskCache(cacheDir)) // default
                .diskCacheSize(50 * 1024 * 1024)
                .diskCacheFileCount(100)
                .diskCacheFileNameGenerator(new HashCodeFileNameGenerator()) // default
                .imageDownloader(new BaseImageDownloader(context)) // default
                .imageDecoder(new BaseImageDecoder(false)) // default
                .defaultDisplayImageOptions(options) // default
                .writeDebugLogs()
                .build();
        ImageLoader.getInstance().init(config);

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //Inflate the layout, initialize the View Holder
        RecyclerView.ViewHolder viewHolder = null;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        switch (viewType) {
            case RECV_TEXT:
                View v1 = inflater.inflate(R.layout.chat_recv_row, parent, false);
                viewHolder = new ViewHolderTEXT(v1);
                break;
            case RECV_AUDIO:
                View vAudio = inflater.inflate(R.layout.chat_recv_audio_row, parent, false);
                viewHolder = new ViewHolderAudio(vAudio);
                break;
            case RECV_IMAGE:
                View v2 = inflater.inflate(R.layout.chat_image_recv, parent, false);
                viewHolder = new ViewHolder(v2);
                break;
            case RECV_VIDEO:
                View v9 = inflater.inflate(R.layout.chat_video_recv, parent, false);
                viewHolder = new ViewHolderVideo(v9);
                break;
            case SEND_TEXT:
                View v3 = inflater.inflate(R.layout.chat_send_row, parent, false);
                viewHolder = new ViewHolderTEXT(v3);
                break;
            case SEND_AUDIO:
                View vsAudio = inflater.inflate(R.layout.chat_send_audio_row, parent, false);
                viewHolder = new ViewHolderAudio(vsAudio);
                break;
            case SEND_IMAGE:
                View v4 = inflater.inflate(R.layout.chat_image_send, parent, false);
                viewHolder = new ViewHolder(v4);
                break;
            case SEND_VIDEO:
                View v10 = inflater.inflate(R.layout.chat_video_send, parent, false);
                viewHolder = new ViewHolderVideo(v10);
                break;
            case SEND_SUGGEST_DOCTOR:
                View v11 = inflater.inflate(R.layout.chat_doctor_suggest_send, parent, false);
                viewHolder = new ViewHolderSuggestDoctor(v11);
                break;
            case RECV_SUGGEST_DOCTOR:
                View v12 = inflater.inflate(R.layout.chat_doctor_suggest_recv, parent, false);
                viewHolder = new ViewHolderSuggestDoctor(v12);
                break;
            case TYPING:
                View v5 = inflater.inflate(R.layout.chat_recv_typing_row, parent, false);
                viewHolder = new ViewHolderTyping(v5);
                break;
        }
        return viewHolder;
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        switch (holder.getItemViewType()) {

            case RECV_TEXT:
                ViewHolderTEXT textHolder = (ViewHolderTEXT) holder;
                configureTextViewHolder(textHolder, position);
                break;
            case RECV_AUDIO:
                ViewHolderAudio audioHolder = (ViewHolderAudio) holder;
                configureAudioHolder(audioHolder, position);
                break;
            case RECV_IMAGE:
                ViewHolder imgaHolder = (ViewHolder) holder;
                configureImageViewHolder(imgaHolder, position);
                break;
            case RECV_VIDEO:
                ViewHolderVideo videoHolder = (ViewHolderVideo) holder;
                configureVideoHolder(videoHolder, position);
                break;
            case RECV_SUGGEST_DOCTOR:
                ViewHolderSuggestDoctor suggestDoctor = (ViewHolderSuggestDoctor) holder;
                configureSuggestDoctorHolder(suggestDoctor, position);
                break;
            case SEND_TEXT:
                ViewHolderTEXT sTextHolder = (ViewHolderTEXT) holder;
                configureTextViewHolder(sTextHolder, position);
                break;
            case SEND_AUDIO:
                ViewHolderAudio sAudioHolder = (ViewHolderAudio) holder;
                configureAudioHolder(sAudioHolder, position);
                break;
            case SEND_IMAGE:
                ViewHolder sImgaHolder = (ViewHolder) holder;
                configureImageViewHolder(sImgaHolder, position);
                break;
            case SEND_VIDEO:
                ViewHolderVideo sVideoHolder = (ViewHolderVideo) holder;
                configureVideoHolder(sVideoHolder, position);
                break;
            case SEND_SUGGEST_DOCTOR:
                ViewHolderSuggestDoctor sSuggestDoctor = (ViewHolderSuggestDoctor) holder;
                configureSuggestDoctorHolder(sSuggestDoctor, position);
                break;
            case TYPING:
                ViewHolderTyping tVideoHolder = (ViewHolderTyping) holder;
                configureTyping(tVideoHolder, position);
                break;
        }
    }

    private void configureSuggestDoctorHolder(ViewHolderSuggestDoctor holder, int position) {
        FireMessage chat = (FireMessage) chatArray.get(position);

        if (position > 0) {
            if (getHeaderFormattedTime(chatArray.get(position).getDate()).equalsIgnoreCase(getHeaderFormattedTime(chatArray.get(position - 1).getDate()))) {
                holder.tvHeader.setVisibility(View.GONE);
            } else {
                holder.tvHeader.setVisibility(View.VISIBLE);
            }
        } else {
            holder.tvHeader.setVisibility(View.VISIBLE);
        }
        holder.tvHeader.setText(getHeaderFormattedTimeLocal(chat.date));

        holder.tvTime.setText(getFormatedTime(chat.date));
        holder.tvSpeciality.setText(chat.speciality);
        holder.tvDoctorName.setText(chat.getDoctorName());
        holder.tvRating.setText(chat.getRating());
        holder.ratingBar.setRating(Float.parseFloat(chat.getRating()));
        try {
            PicassoTrustAll.getInstance(context)
                    .load(chat.getDoctorURL())
                    .placeholder(R.drawable.avatar)
                    .error(R.drawable.avatar)
                    .into(holder.ivDoctor, new Callback() {
                        @Override
                        public void onSuccess() {
                        }

                        @Override
                        public void onError() {
                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void configureTextViewHolder(ViewHolderTEXT holder, int position) {
        FireMessage chat = (FireMessage) chatArray.get(position);

        if (position > 0) {
            if (getHeaderFormattedTime(chatArray.get(position).getDate()).equalsIgnoreCase(getHeaderFormattedTime(chatArray.get(position - 1).getDate()))) {
                holder.tvHeader.setVisibility(View.GONE);
            } else {
                holder.tvHeader.setVisibility(View.VISIBLE);
            }
        } else {
            holder.tvHeader.setVisibility(View.VISIBLE);
        }
        holder.tvHeader.setText(getHeaderFormattedTimeLocal(chat.date));

        holder.tvTime.setText(getFormatedTime(chat.date));
        holder.tvMessage.setText(chat.text);
    }


    private void configureTyping(final ViewHolderTyping holder, int position) {
        FireMessage chat = (FireMessage) chatArray.get(position);
        holder.red.startAnimation();
        //    Handler handler = new Handler();
     /* //  holder.tvMessage.setText("...");
       for (i = 100; i <= 3500; i = i + 100) {
            handler.postDelayed(new Runnable() {

                @Override
                public void run() {
                    if (i % 300 == 0) {
                        holder.tvMessage.setText(".");
                    } else if (i % 200 == 0) {
                        holder.tvMessage.setText("..");
                    } else if (i % 100 == 0) {
                        holder.tvMessage.setText("...");
                    }
                }
            }, i);

        }*/
    }

    private void configureImageViewHolder(final ViewHolder holder, final int position) {
        FireMessage chat = (FireMessage) chatArray.get(position);

        if (position > 0) {
            if (getHeaderFormattedTime(chatArray.get(position).getDate()).equalsIgnoreCase(getHeaderFormattedTime(chatArray.get(position - 1).getDate()))) {
                holder.tvHeader.setVisibility(View.GONE);
            } else {
                holder.tvHeader.setVisibility(View.VISIBLE);
            }
        } else {
            holder.tvHeader.setVisibility(View.VISIBLE);
        }
        holder.tvHeader.setText(getHeaderFormattedTimeLocal(chat.date));

        holder.tvTime.setText(getFormatedTime(chat.date));
        holder.pBar.setVisibility(View.VISIBLE);
        if (chat.senderId.equalsIgnoreCase(App.user.getUserID())) {
            if (!chat.localPath.isEmpty()) {
                final File f = new File(chat.localPath);
                if (f.exists()) {
                    /*try {
                        PicassoTrustAll.getInstance(context)
                                .load(f)
                                .fit()
                                .into(holder.imgMessage, new Callback() {
                                    @Override
                                    public void onSuccess() {
                                        holder.pBar.setVisibility(View.GONE);
                                    }

                                    @Override
                                    public void onError() {
                                        holder.pBar.setVisibility(View.GONE);
                                    }
                                });
                    } catch (Exception e) {
                        e.printStackTrace();
                    }*/

                    /*String decodedImgUri =  Uri.fromFile(f).toString();
                    String strUri = decodedImgUri.replace("%20", " ");
                    ImageLoader.getInstance().displayImage(strUri, holder.imgMessage, options, new ImageLoadingListener() {
                        @Override
                        public void onLoadingStarted(String imageUri, View view) {

                        }

                        @Override
                        public void onLoadingFailed(String uri, View view, FailReason failReason) {
                            System.out.println("reason::"+failReason);
                        }

                        @Override
                        public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                            holder.pBar.setVisibility(View.GONE);
                        }

                        @Override
                        public void onLoadingCancelled(String imageUri, View view) {

                        }
                    });*/
                    try {
                        Bitmap bitmap = Bitmap.createScaledBitmap(BitmapFactory.decodeFile(f.getPath()), 150, 130, false);
                        holder.imgMessage.setImageBitmap(bitmap);
                        holder.imgMessage.setVisibility(View.VISIBLE);
                        holder.pBar.setVisibility(View.GONE);
                        holder.imgMessage.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                showLocalBiggerImage(f);
                            }
                        });
                    } catch (Exception e){
                        e.printStackTrace();
                    }
                } else {
                    holder.imgMessage.setVisibility(View.GONE);
                    holder.pBar.setVisibility(View.VISIBLE);
                    String savePath = ChatConstants.DEFULT_IMAGE_DOWNLOAD_PATH + getFileName(chat.getPhotoURL());
                    beginDownload(chat.photoURL, savePath, ".jpg");
                    holder.imgMessage.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            showBiggerImage(chatArray.get(position).photoURL);
                        }
                    });
                }
            } else {
                holder.imgMessage.setVisibility(View.GONE);
                holder.pBar.setVisibility(View.VISIBLE);
                String savePath = ChatConstants.DEFULT_IMAGE_DOWNLOAD_PATH + getFileName(chat.getPhotoURL());
                beginDownload(chat.photoURL, savePath, ".jpg");
                holder.imgMessage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        showBiggerImage(chatArray.get(position).photoURL);
                    }
                });
            }
        } else if (!chat.photoURL.isEmpty()) {
            final File imgFile = new File(ChatConstants.DEFULT_IMAGE_DOWNLOAD_PATH + getFileName(chat.getPhotoURL()));
            if (imgFile.exists()) {

                /*String decodedImgUri = Uri.fromFile(imgFile).toString();
                String strUri = decodedImgUri.replace("%20", " ");
                ImageLoader.getInstance().displayImage(strUri, holder.imgMessage, options, new ImageLoadingListener() {
                    @Override
                    public void onLoadingStarted(String imageUri, View view) {

                    }

                    @Override
                    public void onLoadingFailed(String imageUri, View view, FailReason failReason) {

                    }

                    @Override
                    public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                        holder.pBar.setVisibility(View.GONE);
                    }

                    @Override
                    public void onLoadingCancelled(String imageUri, View view) {

                    }
                });*/

                holder.imgMessage.setImageBitmap(BitmapFactory.decodeFile(imgFile.getPath()));
                holder.imgMessage.setVisibility(View.VISIBLE);
                holder.pBar.setVisibility(View.GONE);
                holder.imgMessage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        showLocalBiggerImage(imgFile);
                    }
                });
            } else {
                holder.imgMessage.setVisibility(View.GONE);
                holder.pBar.setVisibility(View.VISIBLE);
                String savePath = ChatConstants.DEFULT_IMAGE_DOWNLOAD_PATH + getFileName(chat.getPhotoURL());
                beginDownload(chat.photoURL, savePath, ".jpg");
                holder.imgMessage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        showBiggerImage(chatArray.get(position).photoURL);
                    }
                });
            }
        }
    }

    private String getRealPathFromURI(Uri contentURI) {
        String result;
        Cursor cursor = context.getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) {
            result = contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            result = cursor.getString(idx);
            cursor.close();
        }
        return result;
    }

    private void configureAudioHolder(final ViewHolderAudio holder, final int position) {
        final FireMessage chat = (FireMessage) chatArray.get(position);

        if (position > 0) {
            if (getHeaderFormattedTime(chatArray.get(position).getDate()).equalsIgnoreCase(getHeaderFormattedTime(chatArray.get(position - 1).getDate()))) {
                holder.tvHeader.setVisibility(View.GONE);
            } else {
                holder.tvHeader.setVisibility(View.VISIBLE);
            }
        } else {
            holder.tvHeader.setVisibility(View.VISIBLE);
        }
        holder.tvHeader.setText(getHeaderFormattedTimeLocal(chat.date));

        holder.tvTime.setText(getFormatedTime(chat.date));
        Log.i("Audio View Holder", "Going to Initialize");

        if (!chat.localPath.isEmpty()) {
            final File f = new File(chat.localPath);
            if (f.exists()) {
                /// play from locally
                holder.imgPlay.setVisibility(View.VISIBLE);
                holder.pBar.setVisibility(View.GONE);

                getDuration(chat, holder.tvDuration, f.getPath());

                playFromLocal(holder.player, f.getPath(), holder.seekBar, false, holder.imgPlay, holder.imgPause, holder.imgMic);

            } else {
                /// download remotley
                holder.imgPlay.setVisibility(View.GONE);
                holder.pBar.setVisibility(View.VISIBLE);
                String savePath = ChatConstants.DEFULT_AUDIO_DOWNLOAD_PATH + getFileName(chat.voiceURL);
                beginDownload(chat.voiceURL, savePath, ".mp3");
            }
        } else if (!chat.voiceURL.isEmpty()) {
            final File imgFile = new File(ChatConstants.DEFULT_AUDIO_DOWNLOAD_PATH + getFileName(chat.voiceURL));
            if (imgFile.exists()) {
                /// play from locally downloaded
                holder.imgPlay.setVisibility(View.VISIBLE);
                holder.pBar.setVisibility(View.GONE);
                getDuration(chat, holder.tvDuration, imgFile.getPath());

                playFromLocal(holder.player, imgFile.getPath(), holder.seekBar, false, holder.imgPlay, holder.imgPause, holder.imgMic);


            } else {
                /// download remotely
                holder.imgPlay.setVisibility(View.GONE);
                holder.pBar.setVisibility(View.VISIBLE);
                String savePath = ChatConstants.DEFULT_AUDIO_DOWNLOAD_PATH + getFileName(chat.voiceURL);
                beginDownload(chat.voiceURL, savePath, ".mp3");
            }
        }


    }


    private void configureVideoHolder(ViewHolderVideo holder, final int position) {
        final FireMessage chat = (FireMessage) chatArray.get(position);

        if (position > 0) {
            if (getHeaderFormattedTime(chatArray.get(position).getDate()).equalsIgnoreCase(getHeaderFormattedTime(chatArray.get(position - 1).getDate()))) {
                holder.tvHeader.setVisibility(View.GONE);
            } else {
                holder.tvHeader.setVisibility(View.VISIBLE);
            }
        } else {
            holder.tvHeader.setVisibility(View.VISIBLE);
        }
        holder.tvHeader.setText(getHeaderFormattedTimeLocal(chat.date));

        holder.tvTime.setText(getFormatedTime(chat.date));
        holder.pBar.setVisibility(View.VISIBLE);

        if (!chat.localPath.isEmpty()) {
            final File f = new File(chat.localPath);
            if (f.exists()) {
                try {
                    holder.imgMessage.setImageBitmap(retriveVideoFrameFromVideo(f.getPath()));
                    holder.imgMessage.setVisibility(View.VISIBLE);
                    holder.pBar.setVisibility(View.GONE);
                    holder.imgDownload.setVisibility(View.VISIBLE);
                    holder.imgMessage.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            Intent intent = new Intent(Intent.ACTION_VIEW);
                            intent.setDataAndType(Uri.fromFile(f), "video/*");
                            context.startActivity(intent);
                            //  context.startActivity(new Intent(context, VideoPlayerController.class).putExtra("url", f.getPath()));

                        }
                    });
                } catch (Throwable throwable) {
                    throwable.printStackTrace();
                }

            } else {
                holder.imgMessage.setVisibility(View.GONE);
                holder.pBar.setVisibility(View.VISIBLE);
                String savePath = ChatConstants.DEFULT_VIDEO_DOWNLOAD_PATH + getFileName(chat.getVideoURL());
                beginDownload(chat.videoURL, savePath, ".mp4");
            }
        } else if (!chat.videoURL.isEmpty()) {
            final File imgFile = new File(ChatConstants.DEFULT_VIDEO_DOWNLOAD_PATH + getFileName(chat.getVideoURL()));
            if (imgFile.exists()) {
                try {
                    holder.imgMessage.setImageBitmap(retriveVideoFrameFromVideo(imgFile.getPath()));
                } catch (Throwable throwable) {
                    throwable.printStackTrace();
                }
                holder.imgMessage.setVisibility(View.VISIBLE);
                holder.pBar.setVisibility(View.GONE);
                holder.imgDownload.setVisibility(View.VISIBLE);
                holder.imgMessage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(Intent.ACTION_VIEW);
                        intent.setDataAndType(Uri.fromFile(imgFile), "video/*");
                        context.startActivity(intent);
                        //     context.startActivity(new Intent(context, VideoPlayerController.class).putExtra("url", imgFile.getPath()));
                    }
                });
            } else {
                holder.imgMessage.setVisibility(View.GONE);
                holder.pBar.setVisibility(View.VISIBLE);
                String savePath = ChatConstants.DEFULT_VIDEO_DOWNLOAD_PATH + getFileName(chat.getVideoURL());
                beginDownload(chat.videoURL, savePath, ".mp4");
            }
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (chatArray.get(position).senderId.equalsIgnoreCase(receiverId)) {

            if (!chatArray.get(position).text.isEmpty()) {
                return RECV_TEXT;
            } else if (!chatArray.get(position).photoURL.isEmpty()) {
                return RECV_IMAGE;
            } else if (!chatArray.get(position).voiceURL.isEmpty()) {
                return RECV_AUDIO;
            } else if (!chatArray.get(position).videoURL.isEmpty()) {
                return RECV_VIDEO;
            } else if (!chatArray.get(position).doctorURL.isEmpty()){
                return RECV_SUGGEST_DOCTOR;
            }

        } else if (chatArray.get(position).isTyping) {
            return TYPING;
        } else {
            if (!chatArray.get(position).text.isEmpty()) {
                return SEND_TEXT;
            } else if (!chatArray.get(position).photoURL.isEmpty()) {
                return SEND_IMAGE;
            } else if (!chatArray.get(position).voiceURL.isEmpty()) {
                return SEND_AUDIO;
            } else if (!chatArray.get(position).videoURL.isEmpty()) {
                return SEND_VIDEO;
            } else if (!chatArray.get(position).doctorURL.isEmpty()){
                return  SEND_SUGGEST_DOCTOR;
            }
        }


        return super.getItemViewType(position);
    }


    @Override
    public int getItemCount() {
        //returns the number of elements the RecyclerView will display
        return chatArray.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    // Insert a new item to the RecyclerView on a predefined position
    public void insert(int position, FireMessage data) {
        chatArray.add(position, data);
        notifyItemInserted(position);
    }

    // Remove a RecyclerView item containing a specified Data object
    public void removeTypingIndicator() {
        int total = chatArray.size();

        if (total > 1) {
            if (chatArray.get(total - 1).isTyping) {
                chatArray.remove(chatArray.get(total - 1));
            }

        }
        notifyDataSetChanged();

    }

    public void addTypingIndicator() {
        int total = chatArray.size();

        if (total > 1) {
            if (!chatArray.get(total - 1).isTyping) {
                FireMessage chat = new FireMessage();
                chat.isTyping = true;
                chat.text = "...";
                chatArray.add(chat);

                notifyDataSetChanged();
            }

        }


    }


    public static Bitmap retriveVideoFrameFromVideo(String videoPath) throws Throwable {

        Bitmap bitmap = null;
        MediaMetadataRetriever mediaMetadataRetriever = null;
        try {
            mediaMetadataRetriever = new MediaMetadataRetriever();
            if (Build.VERSION.SDK_INT >= 14) {
                FileInputStream inputStream = new FileInputStream(new File(videoPath).getAbsolutePath());

                mediaMetadataRetriever.setDataSource(inputStream.getFD());
            } else
                mediaMetadataRetriever.setDataSource(videoPath);
            //   mediaMetadataRetriever.setDataSource(videoPath);
            bitmap = mediaMetadataRetriever.getFrameAtTime();
        } catch (Exception e) {
            e.printStackTrace();
            throw new Throwable(
                    "Exception in retriveVideoFrameFromVideo(String videoPath)"
                            + e.getMessage());

        } finally {
            if (mediaMetadataRetriever != null) {
                mediaMetadataRetriever.release();
            }
        }
        return bitmap;
    }


    public void showLocalBiggerImage(File f) {

        final Dialog nagDialog = new Dialog(context, android.R.style.Theme_Translucent_NoTitleBar_Fullscreen);
        nagDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        nagDialog.setCancelable(true);

        LayoutInflater inflater = ((Activity) context).getLayoutInflater();
        View view = inflater.inflate(R.layout.layout_bigger_image, null);
        nagDialog.setContentView(view);

        ProgressBar pBar = (ProgressBar) view.findViewById(R.id.pBar);

        Button btnClose = (Button) view.findViewById(R.id.btnIvClose);
        AQuery aq = aqList.recycle(view);
        ImageView ivPreview = (ImageView) view.findViewById(R.id.iv_preview_image);
        ivPreview.setImageBitmap(BitmapFactory.decodeFile(f.getPath()));

        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {

                nagDialog.dismiss();
            }
        });
        nagDialog.show();
    }

    public void showBiggerImage(String url) {

        final Dialog nagDialog = new Dialog(context, android.R.style.Theme_Translucent_NoTitleBar_Fullscreen);
        nagDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        nagDialog.setCancelable(true);

        LayoutInflater inflater = ((Activity) context).getLayoutInflater();
        View view = inflater.inflate(R.layout.layout_bigger_image, null);
        nagDialog.setContentView(view);

        final ProgressBar pBar = (ProgressBar) view.findViewById(R.id.pBar);

        Button btnClose = (Button) view.findViewById(R.id.btnIvClose);
        AQuery aq = aqList.recycle(view);
        ImageView ivPreview = (ImageView) view.findViewById(R.id.iv_preview_image);

        try {
            PicassoTrustAll.getInstance(context)
                    .load(url)
                    .into(ivPreview, new Callback() {
                        @Override
                        public void onSuccess() {
                            pBar.setVisibility(View.GONE);
                        }

                        @Override
                        public void onError() {
                            pBar.setVisibility(View.GONE);
                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }
        /*aq.id(ivPreview).progress(pBar)
                .image(url, true, true, 0, 0, null, 0, 1.0f);*/

        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {

                nagDialog.dismiss();
            }
        });
        nagDialog.show();
    }

    public void getDuration(FireMessage frm, RobottoTextView tv, String path) {
        try {
            Log.i("Audio View Holder", "Going to getDuration");
            MediaMetadataRetriever metaRetriever = new MediaMetadataRetriever();


            metaRetriever.setDataSource(path);

            String out = "";
            // get mp3 info

            // convert duration to minute:seconds
            String duration =
                    metaRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
            Log.v("time", duration);
            long dur = Long.parseLong(duration);


            frm.duration = getDurationBreakdown(dur);
            tv.setText(frm.duration + "");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getDurationBreakdown(long millis) {
        if (millis < 0) {
            throw new IllegalArgumentException("Duration must be greater than zero!");
        }


        long minutes = TimeUnit.MILLISECONDS.toMinutes(millis);
        millis -= TimeUnit.MINUTES.toMillis(minutes);
        long seconds = TimeUnit.MILLISECONDS.toSeconds(millis);

        StringBuilder sb = new StringBuilder(64);

        sb.append(minutes);
        sb.append(" : ");
        sb.append(getZero(seconds));


        return sb.toString();
    }

    public String getZero(long n) {

        if (n <= 9) {
            return "0" + n;
        } else {
            return "" + n;
        }
    }


    boolean isMute = false;

    public void playFromLocal(final MediaPlayer player, final String path, final SeekBar seekBar, final boolean isSend,
                              final ImageView imgPlay, final ImageView imgPause, final ImageView mic) {


        try {
            player.setDataSource(path);
            player.prepare();

            final int millSecond = player.getDuration();

            final Runnable updateSeekBar = new Runnable() {
                public void run() {
                    long currentDuration = player.getCurrentPosition();

                    int per = (int) currentDuration * 100 / millSecond;
                    if (per < 99 && player.isPlaying()) {
                        seekBar.setProgress(per);
                        seekHandler.postDelayed(this, 15);
                    } else {
                        imgPlay.setVisibility(View.VISIBLE);
                        imgPause.setVisibility(View.GONE);
                        seekBar.setProgress(0);
                        seekHandler.removeCallbacks(this);

                    }
                }
            };

            player.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    imgPlay.setVisibility(View.VISIBLE);
                    imgPause.setVisibility(View.GONE);
                    imgPlay.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            if (player.getDuration() == 0) {
                                try {
                                    player.setDataSource(path);
                                    player.prepare();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            } else {
                                imgPause.setVisibility(View.VISIBLE);
                                imgPlay.setVisibility(View.GONE);
                                player.start();

                                if (player.isPlaying()) {
                                    seekHandler.postDelayed(updateSeekBar, 15);
                                }
                            }
                        }

                    });


                    imgPause.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (player.isPlaying()) {
                                try {
                                    player.pause();
                                    imgPlay.setVisibility(View.VISIBLE);
                                    imgPause.setVisibility(View.GONE);
                                    seekHandler.removeCallbacks(updateSeekBar);

                                } catch (Exception e) {
                                    e.printStackTrace();
                                    imgPlay.setVisibility(View.VISIBLE);
                                    imgPause.setVisibility(View.GONE);
                                }
                            }

                        }
                    });
                }
            });


            seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                @Override
                public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

                    if (progress > 99) {

                        seekBar.setProgress(0);
                        seekHandler.removeCallbacks(updateSeekBar);
                        imgPlay.setVisibility(View.VISIBLE);
                        imgPause.setVisibility(View.GONE);

                        return;
                    }
                    if (fromUser)
                        if (player.isPlaying()) {
                            if (millSecond != 0) {
                                player.seekTo(progress);
                            } else {
                                try {
                                    int seekto = (millSecond * progress) / 100;
                                    player.seekTo(Integer.valueOf(seekto));
                                } catch (NumberFormatException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {

                }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {

                }
            });

            player.setOnSeekCompleteListener(new MediaPlayer.OnSeekCompleteListener() {
                @Override
                public void onSeekComplete(MediaPlayer mp) {
                    mp.stop();
                    seekBar.setProgress(0);
                    imgPlay.setVisibility(View.VISIBLE);
                    imgPause.setVisibility(View.GONE);
                    seekHandler.removeCallbacks(updateSeekBar);
                }
            });

            mic.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (isMute) {
                        player.setVolume(1, 1);
                        isMute = false;
                        mic.setImageResource(isSend ? R.drawable.chat_send_volume_off : R.drawable.chat_recv_volume_off);
                    } else {
                        isMute = true;
                        player.setVolume(0, 0);
                        mic.setImageResource(isSend ? R.drawable.chat_send_volume_on : R.drawable.chat_recv_volume_on);
                    }
                }
            });


            if (isMute) {
                mic.setImageResource(isSend ? R.drawable.chat_send_volume_on : R.drawable.chat_recv_volume_on);
            } else {
                mic.setImageResource(isSend ? R.drawable.chat_send_volume_off : R.drawable.chat_recv_volume_off);

            }

            seekHandler = new Handler();


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String milliSecondsToTimer(long milliseconds) {
        String finalTimerString = "";
        String secondsString = "";

        // Convert finalAmount duration into time
        int hours = (int) (milliseconds / (1000 * 60 * 60));
        int minutes = (int) (milliseconds % (1000 * 60 * 60)) / (1000 * 60);
        int seconds = (int) ((milliseconds % (1000 * 60 * 60)) % (1000 * 60) / 1000);
        // Add hours if there
        if (hours > 0) {
            finalTimerString = hours + ":";
        }

        // Prepending 0 to seconds if it is one digit
        if (seconds < 10) {
            secondsString = "0" + seconds;
        } else {
            secondsString = "" + seconds;
        }

        finalTimerString = finalTimerString + minutes + ":" + secondsString;

        // return timer string
        return finalTimerString;
    }

    public String getFileName(String path) {

        // gs://isoft-doctorpocket.appspot.com/uzQ6DHEeTwemDETP284V0T57kE32/509279165359
        if (path.contains("gs")) {
            return path.substring(path.lastIndexOf("/") + 1, path.length());
        } else
            return "";

    }

    public void beginDownload(String path, String savePath, String fileType) {
        // Get path
        // Kick off MyDownloadService to download the file

        boolean isGranted = Utils.checkPermission(context);
        if (isGranted) {

            Intent intent = new Intent(context, MyDownloadService.class)
                    .putExtra(MyDownloadService.EXTRA_DOWNLOAD_PATH, path)
                    .putExtra(ChatConstants.SAVE_PATH, savePath)
                    .putExtra(ChatConstants.FILE_TYPE, fileType)

                    .setAction(MyDownloadService.ACTION_DOWNLOAD);
            context.startService(intent);
        }
        // Show loading spinner
        //   showProgressDialog(getString(R.string.progress_downloading));
    }

    public static String getExtension(String fileName) {
        String encoded;
        try {
            encoded = URLEncoder.encode(fileName, "UTF-8").replace("+", "%20");
        } catch (UnsupportedEncodingException e) {
            encoded = fileName;
        }
        return MimeTypeMap.getFileExtensionFromUrl(encoded).toLowerCase();
    }

    public String getFormatedTime(String date) {
        /// iphone  ///  2017-02-23T14:21:16.GMT
        /// android  /// 2017-02-23 07:49
        if (date.contains("GMT")) {
            try {
                Log.i("DATE", "IN IOS FORMAT");
                date = date.replace("GMT", "");
                date = date.replace("T", " ");

                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.", Locale.US);
                format.setTimeZone(TimeZone.getTimeZone("UTC"));
                SimpleDateFormat newFormat = new SimpleDateFormat("h:mm a", Locale.US);
                newFormat.setTimeZone(TimeZone.getDefault());
                Date currentDate = new Date();

                currentDate = format.parse(date);
                return newFormat.format(currentDate);
            } catch (ParseException e) {
                e.printStackTrace();
                return "";
            }
        } else {
            try {
                Log.i("DATE", "IN Android FORMAT");
                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm a", Locale.US);
                format.setTimeZone(TimeZone.getTimeZone("UTC"));
                SimpleDateFormat newFormat = new SimpleDateFormat("h:mm a", Locale.US);
                newFormat.setTimeZone(TimeZone.getDefault());
                Date currentDate = new Date();

                currentDate = format.parse(date);
                return newFormat.format(currentDate);
            } catch (ParseException e) {
                e.printStackTrace();
                return "";
            }
        }

    }

    public String getHeaderFormattedTime(String date) {
        /// iphone  ///  2017-02-23T14:21:16.GMT
        /// android  /// 2017-02-23 07:49
        if (date.contains("GMT")) {
            try {
                Log.i("DATE", "IN IOS FORMAT");
                date = date.replace("GMT", "");
                date = date.replace("T", " ");

                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.", Locale.US);

                SimpleDateFormat newFormat = new SimpleDateFormat("MMM dd, yyyy", Locale.US);
                //newFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
                Date currentDate = new Date();

                currentDate = format.parse(date);
                return newFormat.format(currentDate);
            } catch (ParseException e) {
                e.printStackTrace();
                return "";
            }
        } else {
            try {
                Log.i("DATE", "IN Android FORMAT");
                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm a", Locale.US);

                SimpleDateFormat newFormat = new SimpleDateFormat("MMM dd, yyyy", Locale.US);
                //newFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
                Date currentDate = new Date();

                currentDate = format.parse(date);
                return newFormat.format(currentDate);
            } catch (ParseException e) {
                e.printStackTrace();
                return "";
            }
        }

    }

    public String getHeaderFormattedTimeLocal(String date) {
        /// iphone  ///  2017-02-23T14:21:16.GMT
        /// android  /// 2017-02-23 07:49
        if (date.contains("GMT")) {
            try {
                Log.i("DATE", "IN IOS FORMAT");
                date = date.replace("GMT", "");
                date = date.replace("T", " ");

                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.", Locale.US);
                format.setTimeZone(TimeZone.getTimeZone("UTC"));
                SimpleDateFormat newFormat = new SimpleDateFormat("MMM dd, yyyy", Locale.US);
                newFormat.setTimeZone(TimeZone.getDefault());
                Date currentDate = new Date();

                currentDate = format.parse(date);
                return newFormat.format(currentDate);
            } catch (ParseException e) {
                e.printStackTrace();
                return "";
            }
        } else {
            try {
                Log.i("DATE", "IN Android FORMAT");
                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm a", Locale.US);

                SimpleDateFormat newFormat = new SimpleDateFormat("MMM dd, yyyy", Locale.US);
                newFormat.setTimeZone(TimeZone.getDefault());
                Date currentDate = new Date();

                currentDate = format.parse(date);
                return newFormat.format(currentDate);
            } catch (ParseException e) {
                e.printStackTrace();
                return "";
            }
        }

    }


}
