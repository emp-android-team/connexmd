package com.connex.nurse.firebase_chat.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.connex.nurse.R;
import com.connex.nurse.firebase_chat.model.ForwardNurse;
import com.connex.nurse.interfaces.ForwardNurseListener;
import com.connex.nurse.others.PicassoTrustAll;
import com.squareup.picasso.Callback;

import java.util.List;

/**
 * Created by abc on 11/28/2017.
 */

public class ForwardNurseAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Context mContext;
    LayoutInflater inflater;
    private List<ForwardNurse> nurseList;
    private ForwardNurseListener mItemClickListener;

    public ForwardNurseAdapter(Context mContext, List<ForwardNurse> nurseList) {
        this.mContext = mContext;
        this.nurseList = nurseList;
        mItemClickListener = (ForwardNurseListener) mContext;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //Inflate the layout, initialize the View Holder
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_forward_nurse, parent, false);

        return new ViewHolderImages(itemView);
    }

    @Override
    public int getItemCount() {
        //returns the number of elements the RecyclerView will display
        return nurseList.size();
    }


    public class ViewHolderImages extends RecyclerView.ViewHolder {

        ImageView ivNurse;
        TextView tvNurseName;
        Button btnAssign;

        ViewHolderImages(View v) {
            super(v);
            ivNurse = v.findViewById(R.id.ivNurse);
            tvNurseName = v.findViewById(R.id.tvNurseName);
            btnAssign = v.findViewById(R.id.btnAssign);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        ViewHolderImages holderImages = (ViewHolderImages) holder;
        bindImagesHolder(holderImages, position);


    }

    private void bindImagesHolder(final ViewHolderImages holder, final int position) {
        try {
            PicassoTrustAll.getInstance(mContext)
                    .load(nurseList.get(position).getProfilePic())
                    .placeholder(R.drawable.avatar)
                    .error(R.drawable.avatar)
                    .into(holder.ivNurse, new Callback() {
                        @Override
                        public void onSuccess() {
                        }

                        @Override
                        public void onError() {
                            holder.ivNurse.setImageDrawable(mContext.getResources().getDrawable(R.drawable.avatar));
                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }

        holder.tvNurseName.setText(nurseList.get(position).getFirstName() + " " + nurseList.get(position).getLastName());
        holder.btnAssign.setTag(position);
        holder.btnAssign.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int position = (int) view.getTag();
                mItemClickListener.itemClicked(nurseList.get(position));
            }
        });
    }

}
