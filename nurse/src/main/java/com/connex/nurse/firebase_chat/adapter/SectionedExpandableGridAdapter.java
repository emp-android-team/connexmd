package com.connex.nurse.firebase_chat.adapter;

import android.content.Context;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.connex.nurse.R;
import com.connex.nurse.firebase_chat.interfaces.ItemClickListener;
import com.connex.nurse.firebase_chat.interfaces.SectionStateChangeListener;
import com.connex.nurse.firebase_chat.model.Item;
import com.connex.nurse.firebase_chat.model.Section;
import com.connex.nurse.others.PicassoTrustAll;
import com.squareup.picasso.Callback;

import java.util.ArrayList;

public class SectionedExpandableGridAdapter extends RecyclerView.Adapter<SectionedExpandableGridAdapter.ViewHolder> {

    //data array
    private ArrayList<Object> mDataArrayList;

    //context
    private final Context mContext;

    //listeners
    private final ItemClickListener mItemClickListener;
    private final SectionStateChangeListener mSectionStateChangeListener;

    //view type
    private static final int VIEW_TYPE_SECTION = R.layout.layout_section;
    private static final int VIEW_TYPE_ITEM = R.layout.layout_item;

    public SectionedExpandableGridAdapter(Context context, ArrayList<Object> dataArrayList,
                                          final GridLayoutManager gridLayoutManager, ItemClickListener itemClickListener,
                                          SectionStateChangeListener sectionStateChangeListener) {
        mContext = context;
        mItemClickListener = itemClickListener;
        mSectionStateChangeListener = sectionStateChangeListener;
        mDataArrayList = dataArrayList;

        gridLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                return isSection(position) ? gridLayoutManager.getSpanCount() : 1;
            }
        });
    }

    private boolean isSection(int position) {
        return mDataArrayList.get(position) instanceof Section;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(mContext).inflate(viewType, parent, false), viewType);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        switch (holder.viewType) {
            case VIEW_TYPE_ITEM:
                final Item item = (Item) mDataArrayList.get(position);
                holder.tvDoctorName.setText(item.getDoctorName());
                holder.tvDoctorSpeciality.setText(item.getDoctorSpeciality());
                holder.tvRating.setText(item.getRating());
                holder.ratingBar.setRating(Float.parseFloat(item.getRating()));
                if (item.getIsFeatured().equalsIgnoreCase("0")){
                    holder.ivFeatured.setVisibility(View.GONE);
                } else {
                    holder.ivFeatured.setVisibility(View.VISIBLE);
                }
                holder.btnAppoint.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        mItemClickListener.itemClicked(item);
                    }
                });

                try {
                    PicassoTrustAll.getInstance(mContext)
                            .load(item.getProfilePic())
                            .placeholder(R.drawable.avatar)
                            .error(R.drawable.avatar)
                            .into(holder.ivDoctor, new Callback() {
                                @Override
                                public void onSuccess() {
                                }

                                @Override
                                public void onError() {
                                }
                            });
                } catch (Exception e) {
                    e.printStackTrace();
                }

                break;
            case VIEW_TYPE_SECTION:
                final Section section = (Section) mDataArrayList.get(position);
                holder.sectionTextView.setText(section.getName());
                holder.sectionTextView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mItemClickListener.itemClicked(section);
                    }
                });
                holder.sectionToggleButton.setChecked(section.isExpanded);
                holder.sectionToggleButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        mSectionStateChangeListener.onSectionStateChanged(section, isChecked);
                    }
                });
                break;
        }
    }

    @Override
    public int getItemCount() {
        return mDataArrayList.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (isSection(position))
            return VIEW_TYPE_SECTION;
        else return VIEW_TYPE_ITEM;
    }

    protected static class ViewHolder extends RecyclerView.ViewHolder {

        //common
        View view;
        int viewType;

        //for section
        TextView sectionTextView;
        ToggleButton sectionToggleButton;

        //for item
        TextView tvDoctorName, tvDoctorSpeciality, tvRating;
        RatingBar ratingBar;
        Button btnAppoint;
        ImageView ivFeatured, ivDoctor;


        public ViewHolder(View view, int viewType) {
            super(view);
            this.viewType = viewType;
            this.view = view;
            if (viewType == VIEW_TYPE_ITEM) {
                tvDoctorName =  view.findViewById(R.id.tvDoctorName);
                tvDoctorSpeciality =  view.findViewById(R.id.tvDoctorSpeciality);
                tvRating =  view.findViewById(R.id.tvRating);
                ratingBar =  view.findViewById(R.id.ratingBar);
                btnAppoint =  view.findViewById(R.id.btnAppoint);
                ivFeatured =  view.findViewById(R.id.ivFeatured);
                ivDoctor =  view.findViewById(R.id.ivDoctor);
            } else {
                sectionTextView = view.findViewById(R.id.text_section);
                sectionToggleButton = view.findViewById(R.id.toggle_button_section);
            }
        }
    }
}
