package com.connex.nurse.firebase_chat.interfaces;


import com.connex.nurse.firebase_chat.model.Item;
import com.connex.nurse.firebase_chat.model.Section;

/**
 * Created by lenovo on 2/23/2016.
 */
public interface ItemClickListener {
    void itemClicked(Item item);
    void itemClicked(Section section);
}
