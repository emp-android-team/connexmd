package com.connex.nurse.firebase_chat.interfaces;

public interface KeyboardVisibilityListener {
    void onKeyboardVisibilityChanged(boolean keyboardVisible);
}