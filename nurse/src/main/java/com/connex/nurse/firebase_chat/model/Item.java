package com.connex.nurse.firebase_chat.model;

/**
 * Created by bpncool on 2/23/2016.
 */
public class Item {

    private String name, doctorSpeciality, rating, profilePic, isFeatured, consultCharge, responseTime;
    private String id;

    public Item(String name, String id, String speciality, String rate, String profilePic, String isFeatured, String consultCharge, String responseTime) {
        this.name = name;
        this.id = id;
        this.doctorSpeciality = speciality;
        this.rating = rate;
        this.profilePic = profilePic;
        this.isFeatured = isFeatured;
        this.consultCharge = consultCharge;
        this.responseTime = responseTime;
    }

    public String getResponseTime() {
        return responseTime;
    }

    public void setResponseTime(String responseTime) {
        this.responseTime = responseTime;
    }

    public String getConsultCharge() {
        return consultCharge;
    }

    public void setConsultCharge(String consultCharge) {
        this.consultCharge = consultCharge;
    }

    public String getIsFeatured() {
        return isFeatured;
    }

    public void setIsFeatured(String isFeatured) {
        this.isFeatured = isFeatured;
    }

    public String getProfilePic() {
        return profilePic;
    }

    public void setProfilePic(String profilePic) {
        this.profilePic = profilePic;
    }

    public void setDoctorName(String name) {
        this.name = name;
    }

    public String getDoctorSpeciality() {
        return doctorSpeciality;
    }

    public void setDoctorSpeciality(String doctorSpeciality) {
        this.doctorSpeciality = doctorSpeciality;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public String getDoctorName() {
        return name;
    }
}
