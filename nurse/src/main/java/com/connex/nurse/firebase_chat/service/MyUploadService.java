package com.connex.nurse.firebase_chat.service;

import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;


import com.connex.nurse.firebase_chat.others.ChatConstants;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

public class MyUploadService extends MyBaseTaskService {

    private static final String TAG = "MyUploadService";

    /**
     * Intent Actions
     **/
    public static final String ACTION_UPLOAD = "action_upload";
    public static final String UPLOAD_COMPLETED = "upload_completed";
    public static final String UPLOAD_ERROR = "upload_error";
    public static final String TRANSFERING = "transfering";
    public StorageReference photoRef;
    /**
     * Intent Extras
     **/
    public static final String EXTRA_FILE_URI = "extra_file_uri";
    public static final String EXTRA_DOWNLOAD_URL = "extra_download_url";
    public String FileType = "";
    public String UNIQUE_CHAT_ID = "";

    // [START declare_ref]
    private StorageReference mStorageRef;
    // [END declare_ref]

    @Override
    public void onCreate() {
        super.onCreate();

        // [START get_storage_ref]
        mStorageRef = FirebaseStorage.getInstance().getReference();
        // [END get_storage_ref]
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "onStartCommand:" + intent + ":" + startId);
        if (ACTION_UPLOAD.equals(intent.getAction())) {
            Uri fileUri = intent.getParcelableExtra(EXTRA_FILE_URI);
            FileType = intent.getStringExtra(ChatConstants.FILE_TYPE);
            UNIQUE_CHAT_ID = intent.getStringExtra(ChatConstants.UNIQUE_ID);

            uploadFromUri(fileUri);
        }

        return START_REDELIVER_INTENT;
    }

    // [START upload_from_uri]
    private void uploadFromUri(final Uri fileUri) {
        try {
        Log.d(TAG, "uploadFromUri:src:" + fileUri.toString());

        // [START_EXCLUDE]
        taskStarted();
        // showProgressNotification(getString(R.string.progress_uploading), 0, 0);

        Log.d(TAG, "firechat :filetye:" +FileType);
        if(FileType.equalsIgnoreCase(ChatConstants.FILE_TYPE_IMAGE)) {
            photoRef = mStorageRef.child("photos")
                    .child(fileUri.getLastPathSegment());
        } else   if(FileType.equalsIgnoreCase(ChatConstants.FILE_TYPE_VIDEO)) {
            photoRef = mStorageRef.child("video")
                    .child(fileUri.getLastPathSegment());
        } else  if(FileType.equalsIgnoreCase(ChatConstants.FILE_TYPE_AUDIO)) {
            photoRef = mStorageRef.child("audio")
                    .child(fileUri.getLastPathSegment().replace(".mp3",""));
        }

        // Upload file to Firebase Storage
        Log.d(TAG, "uploadFromUri:dst:" + photoRef.getPath());
        photoRef.putFile(fileUri).
                addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {

                        int percentComplete = 0;
                        if (taskSnapshot.getTotalByteCount() > 0) {
                            percentComplete = (int) (100 * taskSnapshot.getBytesTransferred() / taskSnapshot.getTotalByteCount());
                        }


                        Intent broadcast = new Intent(TRANSFERING)

                                .putExtra(ChatConstants.FILE_TYPE, FileType)
                                .putExtra(ChatConstants.UNIQUE_ID, UNIQUE_CHAT_ID)
                                .putExtra(ChatConstants.PERCENTAGE, percentComplete);
                        LocalBroadcastManager.getInstance(getApplicationContext())
                                .sendBroadcast(broadcast);

                    }
                })
                .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        // Upload succeeded
                        Log.d(TAG, "firechat:onSuccess");

                        // Get the public download URL
                        taskSnapshot.getStorage();
                        Uri downloadUri = taskSnapshot.getMetadata().getDownloadUrl();
                        Log.d(TAG, "firechat: Storage reffrence : " + taskSnapshot.getStorage() );
                        Log.d(TAG, "firechat: uploaded url : " + taskSnapshot.getMetadata().getPath() );
                        Log.d(TAG, "firechat: download url : " + taskSnapshot.getMetadata().getDownloadUrl() );

                        // [START_EXCLUDE]
                        broadcastUploadFinished(taskSnapshot.getStorage().toString(), fileUri);
                       // showUploadFinishedNotification(downloadUri, fileUri);
                        taskCompleted();

                        // [END_EXCLUDE]
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception exception) {
                        // Upload failed
                        Log.w(TAG, "uploadFromUri:onFailure", exception);

                        // [START_EXCLUDE]
                        broadcastUploadFinished(null, fileUri);
                        showUploadFinishedNotification(null, fileUri);
                        taskCompleted();

                        // [END_EXCLUDE]
                    }
                });
    } catch (Exception e){
        e.printStackTrace();
    }
    }
    // [END upload_from_uri]

    /**
     * Broadcast finished upload (success or failure).
     *
     * @return true if a running receiver received the broadcast.
     */
    private boolean broadcastUploadFinished(String downloadUrl, @Nullable Uri fileUri) {
        boolean success = downloadUrl != null;


        String action = success ? UPLOAD_COMPLETED : UPLOAD_ERROR;

        Intent broadcast = new Intent(action)
                .putExtra(EXTRA_DOWNLOAD_URL, downloadUrl)
                .putExtra(EXTRA_FILE_URI, fileUri)
                .putExtra(ChatConstants.FILE_TYPE, FileType)
                .putExtra(ChatConstants.UNIQUE_ID, UNIQUE_CHAT_ID);
        return LocalBroadcastManager.getInstance(getApplicationContext())
                .sendBroadcast(broadcast);
    }

    /**
     * Show a notification for a finished upload.
     */
    private void showUploadFinishedNotification(@Nullable Uri downloadUrl, @Nullable Uri fileUri) {
        // Hide the progress notification
        dismissProgressNotification();

      /*  // Make Intent to MainActivity
        Intent intent = new Intent(this, MainActivity.class)
                .putExtra(EXTRA_DOWNLOAD_URL, downloadUrl)
                .putExtra(EXTRA_FILE_URI, fileUri)
                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);

        boolean success = downloadUrl != null;
        String caption = success ? getString(R.string.upload_success) : getString(R.string.upload_failure);
        showFinishedNotification(caption, intent, success);*/
    }

    public static IntentFilter getIntentFilter() {
        IntentFilter filter = new IntentFilter();
        filter.addAction(UPLOAD_COMPLETED);
        filter.addAction(UPLOAD_ERROR);

        return filter;
    }

}