package com.connex.nurse.firebase_chat.view_holders;

import android.media.MediaPlayer;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.SeekBar;


import com.connex.nurse.R;
import com.connex.nurse.custom_views.RobottoTextView;


public class ViewHolderAudio extends RecyclerView.ViewHolder {

    public RobottoTextView tvDuration;
    public SeekBar seekBar;
    public ImageView imgPlay,imgMic,imgPause;
    public ProgressBar pBar;
    public MediaPlayer player ;
    public RobottoTextView tvHeader;
    public RobottoTextView tvTime;


    public ViewHolderAudio(View itemView) {
        super(itemView);
        seekBar = (SeekBar)   itemView.findViewById(R.id.seekBar);
        tvDuration = (RobottoTextView) itemView.findViewById(R.id.tvDuration);
        pBar = (ProgressBar) itemView.findViewById(R.id.pBar);
        imgPause = (ImageView) itemView.findViewById(R.id.imgPause);
        imgPlay = (ImageView) itemView.findViewById(R.id.imgPlay);
        imgMic = (ImageView) itemView.findViewById(R.id.imgSpeaker);
        tvTime = (RobottoTextView) itemView.findViewById(R.id.tvTime);
        tvHeader = itemView.findViewById(R.id.tvHeader);
        player = new MediaPlayer();

    }
}