package com.connex.nurse.firebase_chat.view_holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.connex.nurse.R;
import com.connex.nurse.custom_views.RobottoTextView;

/**
 * Created by abc on 1/6/2018.
 */

public class ViewHolderSuggestDoctor extends RecyclerView.ViewHolder {

    public TextView tvDoctorName, tvRating, tvTime, tvSpeciality;
    public ImageView ivDoctor,ivDRProfile;
    public RatingBar ratingBar;
    public Button btnBook;
    public RobottoTextView tvHeader;

    public ViewHolderSuggestDoctor(View itemView) {
        super(itemView);

        tvDoctorName = itemView.findViewById(R.id.tvDoctorName);
        tvSpeciality = itemView.findViewById(R.id.tvSpeciality);
        tvRating = itemView.findViewById(R.id.tvRating);
        tvTime = itemView.findViewById(R.id.tvTime);
        tvHeader = itemView.findViewById(R.id.tvHeader);
        ivDoctor = itemView.findViewById(R.id.ivDoctor);
        ivDRProfile = itemView.findViewById(R.id.ivDRProfile);
        ratingBar = itemView.findViewById(R.id.ratingBar);
        btnBook = itemView.findViewById(R.id.btnBook);
    }
}
