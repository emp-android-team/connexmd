package com.connex.nurse.firebase_chat.view_holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;


import com.connex.nurse.R;
import com.connex.nurse.custom_views.RobottoTextView;
import com.connex.nurse.firebase_chat.typing_indicator.DotsTextView;
import com.github.channguyen.adv.AnimatedDotsView;


public class ViewHolderTyping extends RecyclerView.ViewHolder {


    DotsTextView dotsView;
    public RobottoTextView tvMessage;
    public final AnimatedDotsView red;

    public ViewHolderTyping(View itemView) {
        super(itemView);
        red = (AnimatedDotsView) itemView.findViewById(R.id.adv_1);

        //  tvMessage= (RobottoTextView) itemView.findViewById(R.id.tvMessage);
        // dotsView = (DotsTextView) itemView.findViewById(R.id.tvDots);


    }
}