package com.connex.nurse.firebase_chat.view_holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;


import com.connex.nurse.R;
import com.connex.nurse.custom_views.RobottoTextView;
import com.github.siyamed.shapeimageview.RoundedImageView;

public class ViewHolderVideo extends RecyclerView.ViewHolder {
    public RobottoTextView tvMessage;
    public RobottoTextView date_time;
    public RoundedImageView imgPerson;
    public ImageView imgMessage;
    public ImageView imgDownload;
    public ProgressBar pBar;
    public RobottoTextView tvTime;
    public View viewTranspernt;
    public RobottoTextView tvHeader;

    public ViewHolderVideo(View itemView) {
        super(itemView);

        tvTime = (RobottoTextView) itemView.findViewById(R.id.tvTime);
        tvHeader = itemView.findViewById(R.id.tvHeader);
        imgMessage = (ImageView) itemView.findViewById(R.id.imgMessage);
        imgDownload = (ImageView) itemView.findViewById(R.id.imgDownload);
        pBar = (ProgressBar) itemView.findViewById(R.id.pBar);
        viewTranspernt = (View) itemView.findViewById(R.id.viewTransprent);
        pBar.setMax(100);
    }
}