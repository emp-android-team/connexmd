package com.connex.nurse.fragments;


import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.ImageView;

import com.connex.nurse.R;
import com.connex.nurse.activity.NurseDashboardActivity;
import com.connex.nurse.adapter.MyPatientAdapter;
import com.connex.nurse.firebase_chat.others.ChatConstants;
import com.connex.nurse.interfaces.DashboardListener;
import com.connex.nurse.model.MessageHistory;
import com.connex.nurse.others.App;
import com.connex.nurse.utils.DividerItemDecoration;
import com.connex.nurse.ws.MyConstants;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

/**
 * A simple {@link Fragment} subclass.
 */
public class MyPatientFragment extends Fragment implements DashboardListener {

    public RecyclerView rvMyPatients;
    public MyPatientAdapter myPatientAdapter;
    public DatabaseReference fireDB, fireChannel;
    MessageHistory messageHistory;
    int position;
    ImageView ivNoMessages;
    SwipeRefreshLayout swipeRefreshLayout;
    public boolean isLast = false;
    int badgeCount;
    Activity activity;

    public MyPatientFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_my_patient, container, false);

        fireDB = FirebaseDatabase.getInstance().getReference().child(MyConstants.NURSE_CHANNEL);

        activity = ((NurseDashboardActivity) getActivity());

        rvMyPatients = view.findViewById(R.id.rvMyPatients);
        ivNoMessages = view.findViewById(R.id.ivNoMessages);
        swipeRefreshLayout = view.findViewById(R.id.swipeLayout);
        swipeRefreshLayout.setColorScheme(android.R.color.holo_blue_dark,
                android.R.color.holo_blue_light,
                android.R.color.holo_green_dark,
                android.R.color.holo_green_light);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (((NurseDashboardActivity) getActivity()) != null) {
                    ((NurseDashboardActivity) getActivity()).getMessageHistory();
                }
            }
        });

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rvMyPatients.setLayoutManager(layoutManager);

        LayoutAnimationController controller = AnimationUtils.loadLayoutAnimation(rvMyPatients.getContext(), R.anim.layout_animation_fall_down);
        rvMyPatients.setLayoutAnimation(controller);
        rvMyPatients.scheduleLayoutAnimation();

        if (MyConstants.myPatientList.size() > 0) {
            getUpdatedDB();
            rvMyPatients.setVisibility(View.VISIBLE);
            ivNoMessages.setVisibility(View.GONE);
        } else {
            ivNoMessages.setVisibility(View.VISIBLE);
            rvMyPatients.setVisibility(View.GONE);
        }

        return view;
    }

    public void getUpdatedDB() {
        badgeCount = 0;
        try {
            if (myPatientAdapter != null) {
                myPatientAdapter.notifyDataSetChanged();
            }
            myPatientAdapter = new MyPatientAdapter(MyPatientFragment.this, MyConstants.myPatientList);
            rvMyPatients.setHasFixedSize(true);
            //rvMyPatients.addItemDecoration(new VerticalSpacingDecoration(20));
            rvMyPatients.addItemDecoration(new DividerItemDecoration(getActivity(), R.drawable.divider));
            rvMyPatients.setItemViewCacheSize(20);
            rvMyPatients.setDrawingCacheEnabled(true);
            rvMyPatients.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
            rvMyPatients.setAdapter(myPatientAdapter);
            myPatientAdapter.notifyDataSetChanged();

            Handler mHandler = new Handler();
            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    for (int i = 0; i < MyConstants.myPatientList.size(); i++) {
                        new SingleValueEventListner().getSingleValues(MyConstants.myPatientList.get(i), i);
                    }
                    for (int i = 0; i < MyConstants.myPatientList.size(); i++) {
                        new ValueEventsListener().setListener(MyConstants.myPatientList.get(i), i);
                    }

                }
            }, 2000);
            System.out.println("my patient list::" + MyConstants.myPatientList.size());

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onChatAssigned(MessageHistory messageHistory, int position) {

    }

    public class SingleValueEventListner {
        public DatabaseReference dbRef;

        public void getSingleValues(final MessageHistory mHistory, final int pos) {
            try {
                final String uniqueId = mHistory.UniqueId;
                dbRef = fireDB.child(uniqueId);

                dbRef.child("last_message").addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        /*if (pos >= MyConstants.myPatientList.size()) {
                            isLast = true;
                        }*/
                        if (dataSnapshot != null) {
                            System.out.println("datasnapshot::" + dataSnapshot.toString());
                            String last_message = dataSnapshot.getValue(String.class);
                            System.out.println("last message" + last_message);
                            //Log.i("Last Messages : Log : ", last_message);
                            mHistory.lastMessage = last_message;
                            /*if (isLast) {
                                sortingListBasedOnTime();
                            } else {
                                myPatientAdapter.notifyDataSetChanged();
                            }*/
                            myPatientAdapter.notifyDataSetChanged();
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                    }
                });

                dbRef.child("last_date").addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        String time = "";
                        Date inputDate;
                        /*if (pos >= MyConstants.myPatientList.size()) {
                            isLast = true;
                        }*/

                        if (dataSnapshot != null) {
                            String last_date = dataSnapshot.getValue(String.class);
                            System.out.println("last date::" + last_date);
                            //Log.i("Last date : Log : ", last_date);

                            // "2017-10-23T15:48:04.GMT"

                            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm a", Locale.US);
                            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm a", Locale.US);

                            if (last_date != null) {
                                try {
                                    format.setTimeZone(TimeZone.getTimeZone("UTC"));
                                    inputDate = format.parse(last_date);
                                    df.setTimeZone(TimeZone.getDefault());
                                    time = df.format(inputDate);
                                    System.out.println("time::" + time);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                        mHistory.time = time;

                        /*if (isLast) {
                            sortingListBasedOnTime();
                        } else {
                            myPatientAdapter.notifyDataSetChanged();
                        }*/

                        myPatientAdapter.notifyDataSetChanged();
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

                String receiverID = "";
                if (App.user.getUser_Type().equalsIgnoreCase(MyConstants.USER_PT)) {
                    receiverID = mHistory.getNurseId();
                } else {
                    receiverID = mHistory.getUserId();
                }

                dbRef.child(ChatConstants.FIRE_UNREAD_COUNTER).child(receiverID)
                        .addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {

                                if (pos == MyConstants.myPatientList.size() - 1) {
                                    isLast = true;
                                }

                                if (dataSnapshot != null) {
                                    String count = String.valueOf(dataSnapshot.getValue(Integer.class));
                                    System.out.println("count::" + count);

                                    if (count.equalsIgnoreCase("null") || count == null || TextUtils.isEmpty(count)) {
                                        mHistory.counter = "0";
                                        if (MyConstants.myPatientCount.contains(uniqueId)) {
                                            MyConstants.myPatientCount.remove(uniqueId);
                                        }
                                    } else {
                                        mHistory.counter = count;
                                        if (Integer.parseInt(count) > 0) {
                                            if (!MyConstants.myPatientCount.contains(uniqueId)) {
                                                MyConstants.myPatientCount.add(uniqueId);
                                            }
                                        }
                                    }
                                    //((NurseDashboardActivity) getActivity()).setTabBadge(1, String.valueOf(MyConstants.myPatientCount.size()));
                                    myPatientAdapter.notifyDataSetChanged();
                                }
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {

                            }
                        });

            } catch (Exception e) {
                e.printStackTrace();
            }
        }


    }


    public class ValueEventsListener {
        public DatabaseReference dbRef;


        public void setListener(final MessageHistory mHistory, final int pos) {

            try {
                final String uniqueId = mHistory.UniqueId;
                dbRef = fireDB.child(uniqueId);

                if (MyConstants.myPatientList.size() > 0) {
                    dbRef.child("last_message").addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            try {
                                if (pos == MyConstants.myPatientList.size() - 1) {
                                    isLast = true;
                                }

                                if (dataSnapshot != null) {
                                    String last_message = dataSnapshot.getValue(String.class);
                                    System.out.println("last message" + last_message);
                                    //Log.i("Last Messages : Log : ", last_message);
                                    mHistory.lastMessage = last_message;

                                    if (isLast)
                                        sortingListBasedOnTime();

                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                            if (pos == MyConstants.myPatientList.size() - 1) {
                                isLast = true;
                            }
                        }
                    });
                }


                if (MyConstants.myPatientList.size() > 0) {
                    dbRef.child("last_date").addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            try {
                                String time = "";
                                Date inputDate;


                                if (dataSnapshot != null) {
                                    String last_date = dataSnapshot.getValue(String.class);
                                    System.out.println("last date::" + last_date);
                                    //Log.i("Last date : Log : ", last_date);

                                    // "2017-10-23T15:48:04.GMT"

                                    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm a", Locale.US);
                                    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm a", Locale.US);

                                    if (last_date != null) {
                                        try {
                                            format.setTimeZone(TimeZone.getTimeZone("UTC"));
                                            inputDate = format.parse(last_date);
                                            df.setTimeZone(TimeZone.getDefault());
                                            time = df.format(inputDate);
                                            System.out.println("time::" + time);
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }
                                }


                                mHistory.time = time;

                                if (isLast)
                                    sortingListBasedOnTime();

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });
                }

                String receiverID = "";
                if (App.user.getUser_Type().equalsIgnoreCase(MyConstants.USER_PT)) {
                    receiverID = mHistory.getNurseId();
                } else {
                    receiverID = mHistory.getUserId();
                }


                if (MyConstants.myPatientList.size() > 0) {
                    dbRef.child(ChatConstants.FIRE_UNREAD_COUNTER).child(receiverID).
                            addValueEventListener(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {


                                    if (dataSnapshot != null) {

                                        try {
                                            String count = String.valueOf(dataSnapshot.getValue(Integer.class));
                                            System.out.println("count:: in change : " + count);

                                            if (count.equalsIgnoreCase("null") || count == null || TextUtils.isEmpty(count)) {
                                                mHistory.counter = "0";
                                            } else {
                                                mHistory.counter = count;
                                            }

                                            if (isLast)
                                                sortingListBasedOnTime();

                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }
                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {

                                }
                            });
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void sortingListBasedOnTime() {

        Runnable runnable = new Runnable() {
            @Override
            public void run() {

                Collections.sort(MyConstants.myPatientList, new Comparator<MessageHistory>() {
                    public int compare(MessageHistory o1, MessageHistory o2) {
                        if (o1.getTime() == null || o2.getTime() == null || o1.getTime().isEmpty() || o2.getTime().isEmpty())
                            return 0;
                        try {
                            return new SimpleDateFormat("yyyy-MM-dd hh:mm a", Locale.US).parse(o2.getTime()).compareTo(new SimpleDateFormat("yyyy-MM-dd hh:mm a", Locale.US).parse(o1.getTime()));
                        } catch (ParseException e) {
                            e.printStackTrace();
                            return 0;
                        }
                    }
                });
//                myPatientAdapter.notifyDataSetChanged();
            }
        };
        new Thread(runnable).start();
        myPatientAdapter.notifyDataSetChanged();
    }

}
