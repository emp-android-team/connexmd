package com.connex.nurse.fragments;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.ImageView;

import com.connex.nurse.R;
import com.connex.nurse.activity.LoginActivity;
import com.connex.nurse.activity.NurseDashboardActivity;
import com.connex.nurse.adapter.PendingPatientAdapter;
import com.connex.nurse.firebase_chat.activity.NurseFireChatActivity;
import com.connex.nurse.firebase_chat.others.ChatConstants;
import com.connex.nurse.interfaces.AsyncTaskListner;
import com.connex.nurse.interfaces.DashboardListener;
import com.connex.nurse.model.MessageHistory;
import com.connex.nurse.others.App;
import com.connex.nurse.utils.DividerItemDecoration;
import com.connex.nurse.ws.CallRequest;
import com.connex.nurse.ws.Constant;
import com.connex.nurse.ws.MyConstants;

import com.connex.nurse.ws.Utils;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

/**
 * A simple {@link Fragment} subclass.
 */
public class PendingPatientFragment extends Fragment implements DashboardListener, AsyncTaskListner {

    public RecyclerView rvPendingPatients;
    public PendingPatientAdapter pendingPatientAdapter;
    public DatabaseReference fireDB, fireChannel;
    MessageHistory messageHistory;
    int position;
    ImageView ivNoMessages;
    SwipeRefreshLayout swipeRefreshLayout;
    SharedPreferences sharedpreferences;
    SharedPreferences.Editor editor;
    boolean isLast = false;
    int badgeCount;
    Activity activity;

    public PendingPatientFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_pending_patient, container, false);

        sharedpreferences = getActivity().getSharedPreferences(MyConstants.PREF, Context.MODE_PRIVATE);

        fireDB = FirebaseDatabase.getInstance().getReference().child(MyConstants.NURSE_CHANNEL);

        activity = ((NurseDashboardActivity) getActivity());

        rvPendingPatients = view.findViewById(R.id.rvPendingPatients);
        ivNoMessages = view.findViewById(R.id.ivNoMessages);
        swipeRefreshLayout = view.findViewById(R.id.swipeLayout);
        swipeRefreshLayout.setColorScheme(android.R.color.holo_blue_dark,
                android.R.color.holo_blue_light,
                android.R.color.holo_green_dark,
                android.R.color.holo_green_light);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (((NurseDashboardActivity) getActivity()) != null) {
                    ((NurseDashboardActivity) getActivity()).getMessageHistory();
                }
            }
        });

        System.out.println("pending patient size::" + MyConstants.pendingPatientList.size());

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rvPendingPatients.setLayoutManager(layoutManager);

        LayoutAnimationController controller = AnimationUtils.loadLayoutAnimation(rvPendingPatients.getContext(), R.anim.layout_animation_fall_down);
        rvPendingPatients.setLayoutAnimation(controller);
        rvPendingPatients.scheduleLayoutAnimation();

        if (MyConstants.pendingPatientList.size() > 0) {
            getUpdatedDB();
            rvPendingPatients.setVisibility(View.VISIBLE);
            ivNoMessages.setVisibility(View.GONE);
        } else {
            ivNoMessages.setVisibility(View.VISIBLE);
            rvPendingPatients.setVisibility(View.GONE);
        }

        return view;
    }

    public void getUpdatedDB() {
        badgeCount = 0;
        try {

            if (pendingPatientAdapter != null) {
                pendingPatientAdapter.notifyDataSetChanged();
            }
            pendingPatientAdapter = new PendingPatientAdapter(PendingPatientFragment.this, MyConstants.pendingPatientList);
            rvPendingPatients.setHasFixedSize(true);
            //rvPendingPatients.addItemDecoration(new VerticalSpacingDecoration(20));
            rvPendingPatients.addItemDecoration(new DividerItemDecoration(getActivity(), R.drawable.divider));
            rvPendingPatients.setItemViewCacheSize(20);
            rvPendingPatients.setDrawingCacheEnabled(true);
            rvPendingPatients.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
            rvPendingPatients.setAdapter(pendingPatientAdapter);
            pendingPatientAdapter.notifyDataSetChanged();

            Handler mHandler = new Handler();
            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    for (int i = 0; i < MyConstants.pendingPatientList.size(); i++) {
                        new SingleValueEventListner().getSingleValues(MyConstants.pendingPatientList.get(i), i);
                    }
                    for (int i = 0; i < MyConstants.pendingPatientList.size(); i++) {
                        new ValueEventsListener().setListener(MyConstants.pendingPatientList.get(i), i);
                    }

                }
            }, 2000);

            /*for (int i = 0; i < MyConstants.pendingPatientList.size(); i++) {
                new ValueEventsListner().setListner(i);
            }*/



            /*pendingPatientAdapter = new PendingPatientAdapter(PendingPatientFragment.this, MyConstants.pendingPatientList);
            rvPendingPatients.setHasFixedSize(true);
            //rvPendingPatients.addItemDecoration(new VerticalSpacingDecoration(20));
            rvPendingPatients.addItemDecoration(new DividerItemDecoration(getActivity(), R.drawable.divider));
            rvPendingPatients.setItemViewCacheSize(20);
            rvPendingPatients.setDrawingCacheEnabled(true);
            rvPendingPatients.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);

            rvPendingPatients.setAdapter(pendingPatientAdapter);*/

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onChatAssigned(MessageHistory messageHistory, int position) {
        this.messageHistory = messageHistory;
        this.position = position;
        changeAssignStatus();
    }

    private void changeAssignStatus() {

        Map<String, String> map = new HashMap<String, String>();
        map.put("url", MyConstants.NURSE_BASE_URL + "changeAssignStatus");
        map.put("NurseId", App.user.getUserID());
        map.put("ApiToken", MyConstants.API_TOKEN);
        map.put("Version", MyConstants.WS_VERSION);
        map.put("UniqueId", messageHistory.getUniqueId());
        map.put("Status", "1");

        new CallRequest(PendingPatientFragment.this).changeAssignStatus(map);
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        Utils.removeSimpleSpinProgressDialog();
        try {
            if (result != null && !result.isEmpty()) {
                Log.i("TAG", "TAG Result : " + result);
                //      {"document":{"response":{"status":1,"message":"Success."}}}
                switch (request) {
                    case changeAssignStatus:
                        try {
                            JSONObject mainObj = new JSONObject(result);
                            if (mainObj.getString("error_code").equalsIgnoreCase("0")) {
                                //Utils.showToast(mainObj.getString("error_string"), GuestVerifyOTPActivity.this);

                                MyConstants.myPatientList.add(MyConstants.pendingPatientList.get(position));
                                MyConstants.pendingPatientList.remove(position);
                                pendingPatientAdapter.notifyDataSetChanged();

                                pendingPatientAdapter = new PendingPatientAdapter(PendingPatientFragment.this, MyConstants.pendingPatientList);

                                Intent intent = new Intent(getActivity(), NurseFireChatActivity.class);
                                intent.putExtra("end", 0);
                                intent.putExtra("IS_GUEST", messageHistory.getIsGuest());
                                intent.putExtra(MyConstants.IS_PAYMENT_DONE, false);
                                intent.putExtra(MyConstants.UNIQUE_CHAT_ID, messageHistory.getUniqueId());
                                intent.putExtra(MyConstants.Pt_NAME, App.user.getName());
                                intent.putExtra(MyConstants.DR_NAME, messageHistory.getFirstName() + " " + messageHistory.getLastName());
                                intent.putExtra(MyConstants.PT_ID, App.user.getUserID());
                                intent.putExtra(MyConstants.DR_ID, messageHistory.getUserId());
                                intent.putExtra(MyConstants.RECEIVER_IMAGE_URL, messageHistory.getProfilePic());
                                startActivity(intent);


                            } else if (mainObj.getString("error_code").equalsIgnoreCase("1") ||
                                    mainObj.getString("error_code").equalsIgnoreCase("2")) {
                                editor = sharedpreferences.edit();
                                editor.clear();
                                editor.commit();

                                Utils.showToast(mainObj.getString("error_string"), getActivity());

                                startActivity(new Intent(getActivity(), LoginActivity.class));
                                ActivityCompat.finishAffinity(getActivity());
                            } else {
                                Utils.showToast(mainObj.getString("error_string"), getActivity());
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onProgressUpdate(String uniqueMessageId, int progres) {
    }

    @Override
    public void onProgressComplete(String uniqueMessageId, String result, Constant.REQUESTS request) {
    }

    public class SingleValueEventListner {
        public DatabaseReference dbRef;

        public void getSingleValues(final MessageHistory mHistory, final int pos) {
            try {
                final String uniqueId = mHistory.UniqueId;
                dbRef = fireDB.child(uniqueId);

                dbRef.child("last_message").addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        /*if (pos >= MyConstants.pendingPatientList.size()) {
                            isLast = true;
                        }*/
                        if (dataSnapshot != null) {
                            System.out.println("datasnapshot::" + dataSnapshot.toString());
                            String last_message = dataSnapshot.getValue(String.class);
                            System.out.println("last message" + last_message);
                            //Log.i("Last Messages : Log : ", last_message);
                            mHistory.lastMessage = last_message;
                            /*if (isLast) {
                                sortingListBasedOnTime();
                            } else {
                                pendingPatientAdapter.notifyDataSetChanged();
                            }*/
                            pendingPatientAdapter.notifyDataSetChanged();
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                    }
                });

                dbRef.child("last_date").addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        String time = "";
                        Date inputDate;
                        /*if (pos >= MyConstants.pendingPatientList.size()) {
                            isLast = true;
                        }*/

                        if (dataSnapshot != null) {
                            String last_date = dataSnapshot.getValue(String.class);
                            System.out.println("last date::" + last_date);
                            //Log.i("Last date : Log : ", last_date);

                            // "2017-10-23T15:48:04.GMT"

                            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm a", Locale.US);
                            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm a", Locale.US);

                            if (last_date != null) {
                                try {
                                    format.setTimeZone(TimeZone.getTimeZone("UTC"));
                                    inputDate = format.parse(last_date);
                                    df.setTimeZone(TimeZone.getDefault());
                                    time = df.format(inputDate);
                                    System.out.println("time::" + time);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                        mHistory.time = time;

                        /*if (isLast) {
                            sortingListBasedOnTime();
                        } else {
                            pendingPatientAdapter.notifyDataSetChanged();
                        }*/

                        pendingPatientAdapter.notifyDataSetChanged();
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

                String receiverID = "";
                if (App.user.getUser_Type().equalsIgnoreCase(MyConstants.USER_PT)) {
                    receiverID = mHistory.getNurseId();
                } else {
                    receiverID = mHistory.getUserId();
                }

                dbRef.child(ChatConstants.FIRE_UNREAD_COUNTER).child(receiverID)
                        .addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {

                                if (pos == MyConstants.pendingPatientList.size() - 1) {
                                    isLast = true;
                                }

                                if (dataSnapshot != null) {
                                    String count = String.valueOf(dataSnapshot.getValue(Integer.class));
                                    System.out.println("count::" + count);

                                    if (count.equalsIgnoreCase("null") || count == null || TextUtils.isEmpty(count)) {
                                        mHistory.counter = "0";
                                        if (MyConstants.pendingPatientCount.contains(uniqueId)) {
                                            MyConstants.pendingPatientCount.remove(uniqueId);
                                        }
                                    } else {
                                        mHistory.counter = count;
                                        if (Integer.parseInt(count) > 0) {
                                            if (!MyConstants.pendingPatientCount.contains(uniqueId)) {
                                                MyConstants.pendingPatientCount.add(uniqueId);
                                            }
                                        }
                                    }
                                    //((NurseDashboardActivity) getActivity()).setTabBadge(1, String.valueOf(MyConstants.pendingPatientCount.size()));
                                    pendingPatientAdapter.notifyDataSetChanged();
                                }
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {

                            }
                        });

            } catch (Exception e) {
                e.printStackTrace();
            }
        }


    }

    public class ValueEventsListener {
        public DatabaseReference dbRef;


        public void setListener(final MessageHistory mHistory, final int pos) {

            try {
                final String uniqueId = mHistory.UniqueId;
                dbRef = fireDB.child(uniqueId);

                if (MyConstants.pendingPatientList.size() > 0) {
                    dbRef.child("last_message").addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            try {
                                if (pos == MyConstants.pendingPatientList.size() - 1) {
                                    isLast = true;
                                }

                                if (dataSnapshot != null) {
                                    String last_message = dataSnapshot.getValue(String.class);
                                    System.out.println("last message" + last_message);
                                    //Log.i("Last Messages : Log : ", last_message);
                                    mHistory.lastMessage = last_message;

                                    if (isLast)
                                        sortingListBasedOnTime();

                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                            if (pos == MyConstants.pendingPatientList.size() - 1) {
                                isLast = true;
                            }
                        }
                    });
                }


                if (MyConstants.pendingPatientList.size() > 0) {
                    dbRef.child("last_date").addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            try {
                                String time = "";
                                Date inputDate;


                                if (dataSnapshot != null) {
                                    String last_date = dataSnapshot.getValue(String.class);
                                    System.out.println("last date::" + last_date);
                                    //Log.i("Last date : Log : ", last_date);

                                    // "2017-10-23T15:48:04.GMT"

                                    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm a", Locale.US);
                                    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm a", Locale.US);

                                    if (last_date != null) {
                                        try {
                                            format.setTimeZone(TimeZone.getTimeZone("UTC"));
                                            inputDate = format.parse(last_date);
                                            df.setTimeZone(TimeZone.getDefault());
                                            time = df.format(inputDate);
                                            System.out.println("time::" + time);
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }
                                }

                                mHistory.time = time;

                                if (isLast)
                                    sortingListBasedOnTime();

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });
                }

                String receiverID = "";
                if (App.user.getUser_Type().equalsIgnoreCase(MyConstants.USER_PT)) {
                    receiverID = mHistory.getNurseId();
                } else {
                    receiverID = mHistory.getUserId();
                }


                if (MyConstants.pendingPatientList.size() > 0) {
                    dbRef.child(ChatConstants.FIRE_UNREAD_COUNTER).child(receiverID).
                            addValueEventListener(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {


                                    if (dataSnapshot != null) {

                                        try {
                                            String count = String.valueOf(dataSnapshot.getValue(Integer.class));
                                            System.out.println("count:: in change : " + count);

                                            if (count.equalsIgnoreCase("null") || count == null || TextUtils.isEmpty(count)) {
                                                mHistory.counter = "0";
                                            } else {
                                                mHistory.counter = count;
                                            }

                                            if (isLast)
                                                sortingListBasedOnTime();

                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }
                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {

                                }
                            });
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void sortingListBasedOnTime() {

        Runnable runnable = new Runnable() {
            @Override
            public void run() {

                Collections.sort(MyConstants.pendingPatientList, new Comparator<MessageHistory>() {
                    public int compare(MessageHistory o1, MessageHistory o2) {
                        if (o1.getTime() == null || o2.getTime() == null || o1.getTime().isEmpty() || o2.getTime().isEmpty())
                            return 0;
                        try {
                            return new SimpleDateFormat("yyyy-MM-dd hh:mm a", Locale.US).parse(o2.getTime()).compareTo(new SimpleDateFormat("yyyy-MM-dd hh:mm a", Locale.US).parse(o1.getTime()));
                        } catch (ParseException e) {
                            e.printStackTrace();
                            return 0;
                        }
                    }
                });
//                pendingPatientAdapter.notifyDataSetChanged();
            }
        };
        new Thread(runnable).start();
        pendingPatientAdapter.notifyDataSetChanged();
    }
}
