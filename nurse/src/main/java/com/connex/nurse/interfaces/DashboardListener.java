package com.connex.nurse.interfaces;

import com.connex.nurse.model.MessageHistory;

/**
 * Created by abc on 12/25/2017.
 */

public interface DashboardListener {
    public void onChatAssigned(MessageHistory messageHistory, int position);
}
