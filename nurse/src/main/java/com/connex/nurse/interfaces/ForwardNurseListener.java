package com.connex.nurse.interfaces;


import com.connex.nurse.firebase_chat.model.ForwardNurse;

/**
 * Created by lenovo on 2/23/2016.
 */
public interface ForwardNurseListener {
    void itemClicked(ForwardNurse forwardNurse);
}
