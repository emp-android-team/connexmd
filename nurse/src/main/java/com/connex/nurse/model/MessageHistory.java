package com.connex.nurse.model;

import java.io.Serializable;

/**
 * Created by abc on 12/25/2017.
 */

public class MessageHistory implements Serializable {

    public long millis =0;
    public String NurseId, UserId, Status, UniqueId, IsGuest, FirstName, LastName, ProfilePic, SocialType, SocialId, lastMessage, time="", counter;

    public String getLastMessage() {
        return lastMessage;
    }

    public void setLastMessage(String lastMessage) {
        this.lastMessage = lastMessage;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getCounter() {
        return counter;
    }

    public void setCounter(String counter) {
        this.counter = counter;
    }

    public String getNurseId() {
        return NurseId;
    }

    public void setNurseId(String nurseId) {
        NurseId = nurseId;
    }

    public String getUserId() {
        return UserId;
    }

    public void setUserId(String userId) {
        UserId = userId;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public String getUniqueId() {
        return UniqueId;
    }

    public void setUniqueId(String uniqueId) {
        UniqueId = uniqueId;
    }

    public String getIsGuest() {
        return IsGuest;
    }

    public void setIsGuest(String isGuest) {
        IsGuest = isGuest;
    }

    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(String firstName) {
        FirstName = firstName;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String lastName) {
        LastName = lastName;
    }

    public String getProfilePic() {
        return ProfilePic;
    }

    public void setProfilePic(String profilePic) {
        ProfilePic = profilePic;
    }

    public String getSocialType() {
        return SocialType;
    }

    public void setSocialType(String socialType) {
        SocialType = socialType;
    }

    public String getSocialId() {
        return SocialId;
    }

    public void setSocialId(String socialId) {
        SocialId = socialId;
    }
}
