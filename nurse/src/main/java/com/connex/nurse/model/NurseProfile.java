package com.connex.nurse.model;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;

/**
 * Created by abc on 11/13/2017.
 */

public class NurseProfile implements Serializable {

    String about_doctor, NurseId, FirstName, LastName, CosultCharge, Country, ResponseTime, ProfilePic, Rating, TotalRating, speciality, IsSaved;
    List<HashMap<String,String>> professional_rotation, scope_of_practice, membership, education, hospital_affiliation, publications, feedback, under_doctors;

    public String getIsSaved() {
        return IsSaved;
    }

    public void setIsSaved(String isSaved) {
        IsSaved = isSaved;
    }

    public String getProfilePic() {
        return ProfilePic;
    }

    public void setProfilePic(String profilePic) {
        ProfilePic = profilePic;
    }

    public String getRating() {
        return Rating;
    }

    public void setRating(String rating) {
        Rating = rating;
    }

    public String getTotalRating() {
        return TotalRating;
    }

    public void setTotalRating(String totalRating) {
        TotalRating = totalRating;
    }

    public String getNurseId() {
        return NurseId;
    }

    public void setNurseId(String nurseId) {
        NurseId = nurseId;
    }

    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(String firstName) {
        FirstName = firstName;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String lastName) {
        LastName = lastName;
    }

    public String getCosultCharge() {
        return CosultCharge;
    }

    public void setCosultCharge(String cosultCharge) {
        CosultCharge = cosultCharge;
    }

    public String getCountry() {
        return Country;
    }

    public void setCountry(String country) {
        Country = country;
    }

    public String getResponseTime() {
        return ResponseTime;
    }

    public void setResponseTime(String responseTime) {
        ResponseTime = responseTime;
    }

    public String getSpeciality() {
        return speciality;
    }

    public void setSpeciality(String speciality) {
        this.speciality = speciality;
    }

    public List<HashMap<String, String>> getProfessional_rotation() {
        return professional_rotation;
    }

    public void setProfessional_rotation(List<HashMap<String, String>> professional_rotation) {
        this.professional_rotation = professional_rotation;
    }

    public String getAbout_doctor() {
        return about_doctor;
    }

    public void setAbout_doctor(String about_doctor) {
        this.about_doctor = about_doctor;
    }

    public List<HashMap<String, String>> getScope_of_practice() {
        return scope_of_practice;
    }

    public void setScope_of_practice(List<HashMap<String, String>> scope_of_practice) {
        this.scope_of_practice = scope_of_practice;
    }

    public List<HashMap<String, String>> getMembership() {
        return membership;
    }

    public void setMembership(List<HashMap<String, String>> membership) {
        this.membership = membership;
    }

    public List<HashMap<String, String>> getEducation() {
        return education;
    }

    public void setEducation(List<HashMap<String, String>> education) {
        this.education = education;
    }

    public List<HashMap<String, String>> getHospital_affiliation() {
        return hospital_affiliation;
    }

    public void setHospital_affiliation(List<HashMap<String, String>> hospital_affiliation) {
        this.hospital_affiliation = hospital_affiliation;
    }

    public List<HashMap<String, String>> getPublications() {
        return publications;
    }

    public void setPublications(List<HashMap<String, String>> publications) {
        this.publications = publications;
    }

    public List<HashMap<String, String>> getFeedback() {
        return feedback;
    }

    public void setFeedback(List<HashMap<String, String>> feedback) {
        this.feedback = feedback;
    }

    public List<HashMap<String, String>> getUnder_doctors() {
        return under_doctors;
    }

    public void setUnder_doctors(List<HashMap<String, String>> under_doctors) {
        this.under_doctors = under_doctors;
    }
}
