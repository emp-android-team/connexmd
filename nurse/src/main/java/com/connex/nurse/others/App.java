package com.connex.nurse.others;

import android.content.SharedPreferences;
import android.os.Build;
import android.os.StrictMode;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;

import com.crashlytics.android.Crashlytics;
import com.connex.nurse.firebase_chat.model.ChatMessage;
import com.connex.nurse.firebase_chat.model.FireMessage;
import com.onesignal.OneSignal;

import io.fabric.sdk.android.Fabric;
import java.lang.reflect.Method;
import java.util.ArrayList;

/**
 * Created by Sagar Sojitra on 5/16/2016.
 */
public class App extends MultiDexApplication {

    public static App getInstance() {
        return instance;
    }

    public static App instance;
    public SharedPreferences sharedPref;
    public String recentChatId = "";
    public String messageBadgeVal = "";
    public static User user = null;
    public static String HEALTH_TOPIC = "health_tips";
    public ArrayList<ChatMessage> chatArray = new ArrayList<>();
    public ArrayList<FireMessage> fireChatArray = new ArrayList<>();


    /*public ArrayList<Discount> discountArray = new ArrayList<>();
    public ArrayList<ChatMessage> chatArray = new ArrayList<>();
    public ArrayList<FireMessage> fireChatArray = new ArrayList<>();*/
    public String countryName = "", stateName = "", timeZone = "";

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        OneSignal.startInit(this)
                .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
                .unsubscribeWhenNotificationsAreDisabled(true)
                .init();

        if (Build.VERSION.SDK_INT >= 24) {
            try {
                Method m = StrictMode.class.getMethod("disableDeathOnFileUriExposure");
                m.invoke(null);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
            builder.detectFileUriExposure();
        }

        try {

            //FacebookSdk.sdkInitialize(getApplicationContext());
            //AppEventsLogger.activateApp(this);

            instance = this;
            user = new User();
            sharedPref = getSharedPreferences(App.class.getSimpleName(), 0);
            MultiDex.install(this);


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onTerminate() {

        super.onTerminate();
        //AppEventsLogger.deactivateApp(this);
    }

    public App() {
        setInstance(this);
    }


    public static void setInstance(App instance) {
        App.instance = instance;
    }
}
