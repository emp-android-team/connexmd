package com.connex.nurse.utils;

import android.graphics.Bitmap;
import android.os.Environment;
import android.view.View;


import com.connex.nurse.ws.MyConstants;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by abc on 11/6/2017.
 */

public class SaveViewUtil {

    private static final File rootDir = new File(Environment.getExternalStorageDirectory() + File.separator + "dr_pocket/drawings");

    /**
     * Save picture to file
     */
    public static boolean saveScreen(View view) {
        //determine if SDCARD is available
        if (!Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
            return false;
        }
        if (!rootDir.exists()) {
            rootDir.mkdir();
        }
        view.setDrawingCacheEnabled(true);
        view.buildDrawingCache();
        Bitmap bitmap = view.getDrawingCache();
        try {
            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.US)
                    .format(new Date());
            String imageName = "Drawing" + timeStamp + ".jpg";
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, new FileOutputStream(new File(rootDir, imageName)));
            File mediaFile;
            mediaFile = new File(rootDir, imageName);
            MyConstants.DRAWING_PATH = mediaFile.toString();
            System.out.println("file path:::" + MyConstants.DRAWING_PATH);
            return true;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return false;
        } finally {
            view.setDrawingCacheEnabled(false);
            bitmap = null;
        }
    }

}
