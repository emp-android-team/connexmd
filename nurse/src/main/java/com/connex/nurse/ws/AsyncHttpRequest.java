package com.connex.nurse.ws;

import android.content.Context;
import android.content.res.AssetManager;
import android.os.AsyncTask;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.util.Log;


import com.connex.nurse.R;
import com.connex.nurse.interfaces.AsyncTaskListner;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.conn.ssl.X509HostnameVerifier;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.SingleClientConnManager;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.KeyStore;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;

/**
 * Created by Sagar Sojitra on 3/8/2016.
 */
public class AsyncHttpRequest extends AsyncTask<Map<String, String>, Void, String> {

    public Fragment ft;
    public Constant.REQUESTS request;
    public Map<String, String> map;
    public AsyncTaskListner aListner;
    public Constant.POST_TYPE post_type;
    public String chatId = "";

    public AsyncHttpRequest(Fragment ft, Constant.REQUESTS request, Constant.POST_TYPE post_type, Map<String, String> map) {
        this.ft = ft;
        this.ct = ft.getActivity();
        this.request = request;
        this.map = map;
        this.aListner = (AsyncTaskListner) ft;
        this.post_type = post_type;
        this.map.put("unused", System.currentTimeMillis() + "");

        if (map.containsKey("chatId")) {
            chatId = map.get("chatId");
            map.remove("chatId");
        }
        if (Utils.isNetworkAvailable(ft.getActivity())) {

            if (!map.containsKey("show")) {
                Utils.showSimpleSpinProgressDialog(ft.getActivity(), "Please Wait..");
            }


            new MyRequest().execute(this.map);
        } else {
            aListner.onTaskCompleted(null, request);
            Utils.showToast("No Internet, Please try again later", ft.getActivity());
        }
    }

    public Context ct;

    public AsyncHttpRequest(Context ft, Constant.REQUESTS request, Constant.POST_TYPE post_type, Map<String, String> map) {
        this.ct = ft;
        this.request = request;
        this.map = map;
        this.aListner = (AsyncTaskListner) ft;
        this.post_type = post_type;

        if (!map.containsKey("ip")) {
            this.map.put("unused", System.currentTimeMillis() + "");
        } else {
            map.remove("ip");
        }

        if (map.containsKey("chatId")) {
            chatId = map.get("chatId");
            map.remove("chatId");
        }


        if (Utils.isNetworkAvailable(ct)) {
            if (!map.containsKey("show")) {
                Utils.showSimpleSpinProgressDialog(ft, "Please Wait..");
            }
            new MyRequest().execute(this.map);
        } else {
            aListner.onTaskCompleted(null, request);
            Utils.showToast("No Internet, Please try again later", ct);
        }
    }

    @Override
    protected String doInBackground(Map<String, String>... params) {
        return null;
    }


    class MyRequest extends AsyncTask<Map<String, String>, Integer, String>

    {
        MyCustomMultiPartEntity reqEntity;

        @Override
        protected void onPreExecute() {

            super.onPreExecute();
        }


        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
        }

        @Override
        protected String doInBackground(Map<String, String>... map) {
            System.out.println("::urls ::" + map[0].get("url"));

            String responseBody = "";
            try {
                switch (post_type) {
                    case GET:
                        String tempData = "";
                        DefaultHttpClient httpClient = getNewHttpClient();
                        String query = map[0].get("url");
                        System.out.println();
                        tempData += "\n\n\n\n URL : " + map[0].get("url");

                        map[0].remove("url");


                        List<String> values = new ArrayList<String>(map[0].values());


                        List<String> keys = new ArrayList<String>(map[0].keySet());

                        for (int i = 0; i < values.size(); i++) {
                            System.out.println();

                            System.out.println(keys.get(i) + "====>" + values.get(i));
                            query = query + keys.get(i) + "=" + values.get(i);
                            tempData += "\n" + keys.get(i) + "====>" + values.get(i);
                            if (i < values.size() - 1) {
                                query += "&";
                            }
                        }


                        System.out.println("URL" + "====>" + query);
                        HttpGet httpGet = new HttpGet(query);

                        HttpResponse httpResponse = httpClient.execute(httpGet);
                        HttpEntity httpEntity = httpResponse.getEntity();
                        responseBody = EntityUtils.toString(httpEntity);

                        System.out.println("Responce" + "====>" + responseBody);
                        tempData += "\n" + "Responce" + "====>" + responseBody;
                        writeToSDFile(tempData);
                        return responseBody;

                    case POST:
                        tempData = "";
                        System.out.println("new Requested URL >> " + map[0].get("url"));
                        tempData += "\n\n\n\n URL : " + map[0].get("url");
                        HttpPost postRequest = new HttpPost(map[0].get("url"));
                        httpClient = getNewHttpClient();
                        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

                        for (String key : map[0].keySet()) {
                            System.out.println(key + "====>" + map[0].get(key));
                            tempData += "\n" + key + "====>" + map[0].get(key);
                            nameValuePairs.add(new BasicNameValuePair(key, map[0].get(key)));
                        }
                        postRequest.setEntity(new UrlEncodedFormEntity(nameValuePairs));

                        System.out.println("Final URI::" + postRequest.getEntity().toString());
                        HttpResponse response = httpClient.execute(postRequest);
                        httpEntity = response.getEntity();
                        responseBody = EntityUtils.toString(httpEntity);
                        tempData += "\n" + "Responce" + "====>" + responseBody;
                        writeToSDFile(tempData);
                        return responseBody;


                    case POST_WITH_IMAGE:


                        String charset = "UTF-8";
                        try {
                            httpClient = getNewHttpClient();
                            postRequest = new HttpPost(map[0].get("url"));
                            reqEntity = new MyCustomMultiPartEntity(new MyCustomMultiPartEntity.ProgressListener() {
                                @Override
                                public void transferred(long num) {
                                }
                            });
                            BasicHttpContext localContext = new BasicHttpContext();
                            for (String key : map[0].keySet()) {
                                System.out.println();
                                System.out.println(key + "====>" + map[0].get(key));
                                if (matchKeysForImages(key)) {
                                    if (map[0].get(key) != null) {
                                        if (map[0].get(key).length() > 1) {
                                            String type = "image/png";
                                            File f = new File(map[0].get(key));
                                            FileBody fbody = new FileBody(f, f.getName(), type, "UTF-8");
                                            reqEntity.addPart(key, fbody);
                                        }
                                    }
                                } else {
                                    if (map[0].get(key) == null) {
                                        reqEntity.addPart(key, new StringBody(""));
                                    } else {
                                        reqEntity.addPart(key, new StringBody(map[0].get(key)));
                                    }
                                }
                            }
                            postRequest.setEntity(reqEntity);
                            HttpResponse responses = httpClient.execute(postRequest, localContext);
                            BufferedReader reader = new BufferedReader(new InputStreamReader(responses.getEntity().getContent(),
                                    "UTF-8"));
                            String sResponse;
                            while ((sResponse = reader.readLine()) != null) {
                                responseBody = responseBody + sResponse;
                            }
                            System.out.println("Response ::" + responseBody);
                            return responseBody;

                        } catch (IOException e) {
                            Log.d("ExPostActivity", e.getMessage());
                        }
                }

            } catch (Exception e) {
                e.printStackTrace();
                aListner.onTaskCompleted(null, request);
            }
            return null;

        }

        @Override
        protected void onPostExecute(String result) {

            if (!chatId.isEmpty()) {
                aListner.onProgressComplete(chatId, result, request);
            } else {
                aListner.onTaskCompleted(result, request);
            }
            super.onPostExecute(result);
        }
    }


    public DefaultHttpClient HostVerifier() {
        HostnameVerifier hostnameVerifier = SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER;

        DefaultHttpClient client = new DefaultHttpClient();

        SchemeRegistry registry = new SchemeRegistry();
        SSLSocketFactory socketFactory = SSLSocketFactory.getSocketFactory();
        socketFactory.setHostnameVerifier((X509HostnameVerifier) hostnameVerifier);
        registry.register(new Scheme("https", socketFactory, 443));
        SingleClientConnManager mgr = new SingleClientConnManager(client.getParams(), registry);
        DefaultHttpClient httpClient = new DefaultHttpClient(mgr, client.getParams());

// Set verifier
        HttpsURLConnection.setDefaultHostnameVerifier(hostnameVerifier);
        return httpClient;
    }

    public DefaultHttpClient getNewHttpClient() {
        try {
            KeyStore trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
            trustStore.load(null, null);

            MySSLSocketFactory sf = new MySSLSocketFactory(trustStore);
            sf.setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);

            HttpParams params = new BasicHttpParams();
            HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
            HttpProtocolParams.setContentCharset(params, HTTP.UTF_8);

            SchemeRegistry registry = new SchemeRegistry();
            registry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
            registry.register(new Scheme("https", sf, 443));

            ClientConnectionManager ccm = new ThreadSafeClientConnManager(params, registry);

            return new DefaultHttpClient(ccm, params);
        } catch (Exception e) {
            return new DefaultHttpClient();
        }
    }

    public org.apache.http.conn.ssl.SSLSocketFactory createAdditionalCertsSSLSocketFactory() {
        try {
            final KeyStore ks = KeyStore.getInstance("BKS");
            AssetManager assManager = ct.getApplicationContext().getAssets();
            InputStream is = null;
            try {
                is = assManager.open("docpoc.jks");
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            InputStream in = new BufferedInputStream(is);

            // the bks file we generated above

            try {
                // don't forget to put the password used above in strings.xml/mystore_password
                ks.load(in, ct.getString(R.string.mystore_password).toCharArray());
            } finally {
                in.close();
            }

            return new MySSLSocketFactory(ks);

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public boolean matchKeysForImages(String key) {


        if (key.equalsIgnoreCase("Image1") ||
                key.equalsIgnoreCase("Image2") ||
                key.equalsIgnoreCase("Image3") ||
                key.equalsIgnoreCase("Image4") ||
                key.equalsIgnoreCase("Image5") ||
                key.equalsIgnoreCase("Image6") ||
                key.equalsIgnoreCase("Image7") ||
                key.equalsIgnoreCase("Image8") ||
                key.equalsIgnoreCase("Image9") ||
                key.equalsIgnoreCase("Image10") ||
                key.equalsIgnoreCase("profile_picture") ||
                key.equalsIgnoreCase("photo_id") ||
                key.equalsIgnoreCase("profile_pic") ||
                key.equalsIgnoreCase("ProfilePic")) {
            return true;
        } else {
            return false;
        }
    }

    private void checkExternalMedia() {
        boolean mExternalStorageAvailable = false;
        boolean mExternalStorageWriteable = false;
        String state = Environment.getExternalStorageState();

        if (Environment.MEDIA_MOUNTED.equals(state)) {
            // Can read and write the media
            mExternalStorageAvailable = mExternalStorageWriteable = true;
        } else if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
            // Can only read the media
            mExternalStorageAvailable = true;
            mExternalStorageWriteable = false;
        } else {
            // Can't read or write
            mExternalStorageAvailable = mExternalStorageWriteable = false;
        }

    }

    /**
     * Method to write ascii text characters to file on SD card. Note that you must add a
     * WRITE_EXTERNAL_STORAGE permission to the manifest file or this method will throw
     * a FileNotFound Exception because you won't have write permission.
     */

    private void writeToSDFile(String data) {

       /* // Find the root of the external storage.
        // See http://developer.android.com/guide/topics/data/data-  storage.html#filesExternal
        File root = android.os.Environment.getExternalStorageDirectory();


        // See http://stackoverflow.com/questions/3551821/android-write-to-sd-card-folder
        String buffer = "";
        File dir = new File(root.getAbsolutePath() + "/fyndine");
        dir.mkdirs();
        File file = new File(dir, "fyndine_log.txt");
        if (file.exists()) {

            try {

                FileInputStream fIn = new FileInputStream(file);
                BufferedReader myReader = new BufferedReader(
                        new InputStreamReader(fIn));
                String aDataRow = "";
                String aBuffer = "";
                while ((aDataRow = myReader.readLine()) != null) {
                    aBuffer += aDataRow + "\n";
                }
                buffer = aBuffer.toString();
                buffer += data;
                myReader.close();
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
        try {
            FileOutputStream f = new FileOutputStream(file);
            PrintWriter pw = new PrintWriter(f);
            if (buffer.isEmpty()) {
                pw.println(data);
            } else {
                pw.println(buffer);
            }
            pw.flush();
            pw.close();
            f.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            Log.i("", "******* File not found. Did you" +
                    " add a WRITE_EXTERNAL_STORAGE permission to the   manifest?");
        } catch (IOException e) {
            e.printStackTrace();
        }
*/
    }

    /**
     * Method to read in a text file placed in the res/raw directory of the application. The
     * method reads in all lines of the file sequentially.
     */

   /* public void wrtieFileOnInternalStorage(Context mcoContext, String sBody) {
        File file = new File(mcoContext.getFilesDir(), "fyndine");
        if (!file.getParentFile().exists()) {
            file.getParentFile().mkdirs();

            try {
                File gpxfile = new File(file, "Fyndine_Logs.txt");
                FileWriter writer = new FileWriter(gpxfile);
                writer.append(sBody);
                writer.flush();
                writer.close();

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            File sdcard = new File(mcoContext.getExternalFilesDirs(), "fyndine");
            //   File sdcard = Environment.getExternalStorageDirectory();
            if (!file.exists())
                file.getParentFile().mkdirs();
//Get the text file
            File readFile = new File(sdcard, "Fyndine_Logs.txt");

//Read text from file
            StringBuilder text = new StringBuilder();

            try {
                BufferedReader br = new BufferedReader(new FileReader(readFile));

                String line;
                while ((line = br.readLine()) != null) {
                    text.append(line);
                    text.append('\n');
                }
                sBody += text.toString();

                br.close();
            } catch (IOException e) {
                e.printStackTrace();
                //You'll need to add proper error handling here
            }

            try {
                File gpxfile = new File(file, "Fyndine_Logs.txt");
                FileWriter writer = new FileWriter(gpxfile);
                writer.append(sBody);
                writer.flush();
                writer.close();

            } catch (Exception e) {
                e.printStackTrace();
            }

        }


    }*/
}
