package com.connex.nurse.ws;

import android.content.Context;
import android.support.v4.app.Fragment;


import com.connex.nurse.others.App;

import java.util.HashMap;
import java.util.Map;

;

public class CallRequest {

    public App app;
    public Context ct;
    public Fragment ft;
    public static String version = "1";

    public CallRequest(Fragment ft) {
        app = App.getInstance();
        this.ft = ft;
    }

    public CallRequest(Context ct) {
        this.ct = ct;
        app = App.getInstance();
    }

    public CallRequest() {

    }

    public void sendOfflineNotification(String message, String sender_id, String reciever_id, String user_type, String social_id, String social_type) {
        Map<String, String> map = new HashMap<String, String>();

        map.put("url", MyConstants.NOTIFICATION_URL + "offlineNotification");
        map.put("Message", message);
        map.put("SenderId", sender_id);
        map.put("ReceiverId", reciever_id);
        map.put("SocialId", social_id);
        map.put("SocialType", social_type);
        map.put("SenderType", user_type);
        //map.put("ApiToken", MyConstants.API_TOKEN);
        map.put("Version", MyConstants.WS_VERSION);

        map.put("show", "");
        new AsyncHttpRequest(ct, Constant.REQUESTS.offlineNotification, Constant.POST_TYPE.POST, map);

    }

    public void sendOfflineNotificationNurse(String message, String sender_id, String reciever_id, String user_type, String social_id, String social_type, String is_guest) {

        Map<String, String> map = new HashMap<String, String>();

        map.put("url", MyConstants.NOTIFICATION_URL + "nurseOfflineNotification");
        map.put("Message", message);
        map.put("SenderId", sender_id);
        map.put("ReceiverId", reciever_id);
        map.put("SocialId", social_id);
        map.put("SocialType", social_type);
        map.put("SenderType", user_type);
        map.put("IsGuest", is_guest);
        //map.put("ApiToken", MyConstants.API_TOKEN);
        map.put("Version", MyConstants.WS_VERSION);

        map.put("show", "");
        new AsyncHttpRequest(ct, Constant.REQUESTS.offlineNotification, Constant.POST_TYPE.POST, map);

    }

    public void setDeviceToken() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", MyConstants.NURSE_BASE_URL + "updateDeviceToken");
        map.put("NurseId", App.user.getUserID());
        map.put("Version", version);
        map.put("ApiToken", MyConstants.API_TOKEN);
        map.put("DeviceToken", MyConstants.DEVICE_ID);
        map.put("DeviceType", MyConstants.DEVICE_TYPE);

        new AsyncHttpRequest(ft, Constant.REQUESTS.setDeviceToken, Constant.POST_TYPE.POST, map);
    }


    public void sendOfflineNurseNotification(String message, String sender_id, String reciever_id, String user_type) {

        Map<String, String> map = new HashMap<String, String>();
        map.put("url", MyConstants.BASE_URL + "offline_nurse_notification.php?");
        map.put("message", message);
        map.put("sender_id", sender_id.replace(" ", ""));
        map.put("reciever_id", reciever_id.replace(" ", ""));
        map.put("user_type", user_type);

        map.put("show", "");
        new AsyncHttpRequest(ct, Constant.REQUESTS.offlineNotification, Constant.POST_TYPE.POST, map);

    }

    public void changePassword(Map<String, String> map) {
        new AsyncHttpRequest(ct, Constant.REQUESTS.changePassword, Constant.POST_TYPE.POST, map);
    }

    public void nurseLogin(String email, String password) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", MyConstants.NURSE_BASE_URL + "login");
        map.put("email", email);
        map.put("password", password);
        map.put("DeviceType", MyConstants.DEVICE_TYPE);
        map.put("DeviceToken", MyConstants.DEVICE_ID);
        map.put("Version", MyConstants.WS_VERSION);
        new AsyncHttpRequest(ct, Constant.REQUESTS.nurseLogin, Constant.POST_TYPE.POST, map);

    }

    public void getChatHistory(Map<String, String> map) {
        new AsyncHttpRequest(ct, Constant.REQUESTS.chatHistory, Constant.POST_TYPE.POST, map);
    }

    public void getSuggestDoctors(Map<String, String> map) {
        new AsyncHttpRequest(ct, Constant.REQUESTS.suggestDoctors, Constant.POST_TYPE.POST, map);
    }

    public void getNurseList(Map<String, String> map) {
        new AsyncHttpRequest(ct, Constant.REQUESTS.getNurseList, Constant.POST_TYPE.POST, map);
    }

    public void forwardNurse(Map<String, String> map) {
        new AsyncHttpRequest(ct, Constant.REQUESTS.forwardNurse, Constant.POST_TYPE.POST, map);
    }

    public void sendNotificationToUsers(Map<String, String> map) {
        new AsyncHttpRequest(ct, Constant.REQUESTS.sendNotification, Constant.POST_TYPE.POST, map);
    }

    public void checkCancelStatus(String bookingID, String type, String user_id) {

        Map<String, String> map = new HashMap<String, String>();
        if (type.equalsIgnoreCase(MyConstants.USER_PT)) {
            if (MyConstants.isGuest.equalsIgnoreCase("1")) {
                map.put("url", MyConstants.GUEST_BASE_URL + "checkConsult");
                map.put("GuestId", user_id);
            } else {
                map.put("url", MyConstants.BASE_URL + "checkConsult");
                map.put("UserId", user_id);
            }
        } else {
            map.put("url", MyConstants.DOCTOR_BASE_URL + "checkConsult");
            map.put("DoctorId", user_id);
        }
        map.put("ApiToken", MyConstants.API_TOKEN);
        map.put("Version", MyConstants.WS_VERSION);
        map.put("UniqueId", bookingID);


        new AsyncHttpRequest(ct, Constant.REQUESTS.checkCancelStatus, Constant.POST_TYPE.POST, map);

    }

    public void getNurseProfile(Map<String, String> map) {
        new AsyncHttpRequest(ct, Constant.REQUESTS.nurseProfile, Constant.POST_TYPE.POST, map);

    }

    public void resetPassword(Map<String, String> map) {
        new AsyncHttpRequest(ct, Constant.REQUESTS.resetPassword, Constant.POST_TYPE.POST, map);
    }


    public void sendResetPassword(Map<String, String> map) {
        new AsyncHttpRequest(ct, Constant.REQUESTS.sendResetPassword, Constant.POST_TYPE.POST, map);
    }

    public void changeAssignStatus(Map<String, String> map) {
        new AsyncHttpRequest(ft, Constant.REQUESTS.changeAssignStatus, Constant.POST_TYPE.POST, map);
    }

    public void changeAssignStatusActivity(Map<String, String> map) {
        new AsyncHttpRequest(ct, Constant.REQUESTS.changeAssignStatus, Constant.POST_TYPE.POST, map);
    }

    public void getNurseChatDetails(Map<String, String> map) {
        new AsyncHttpRequest(ct, Constant.REQUESTS.nurseChatDetails, Constant.POST_TYPE.POST, map);
    }

}