package com.connex.nurse.ws;

public class Constant {
    public static String IMAGE_PATH = "admindoctorpocket.in/web_serv/v2/uploads/Speciality/";

    public enum POST_TYPE {
        GET, POST, POST_WITH_IMAGE;
    }


    public enum TABS {
        DR_LIST, HISTORY, PROFILE, MESSAGES, HEALTH, NOTIFICATIONS;

    }

    public enum REQUESTS {
        offlineNotification, singup_patient_fb, getProfile, cancelBookingFromUser, doctorLogin, bookingHistoryDoctor, register, suggestDoctors,
        getCountry, getZone, getDoctorProfile, updateDevice, getIP_LOCATION, resetPasswordOTP, verifyAccount, isVerified, nurseProfile,
        updateBadge, get_refferalCode, getDiscounts, updateRefer, patientLogin, guestLogin, saveDoctor, referData, saveCategory,
        chatHistory, cancelChat, setOnOffStatus, getOnOffStatus, updateDoctorProfile, rate_doctor, nurseChatDetails,nurseLogin,setDeviceToken,
        updatePatientProfile, checkCancelStatus, createBooking, GET_MORNING_SLOT, GET_EVENING_SLOT,getReferredDiscount, sendNotification, getNurseList, forwardNurse,
        uploadChatImage, uploadChatVideo, uploadChatAudio, checkTimeOver, acceptReject, getSymptoms, sendResetPassword, changeAssignStatus,
        resetPassword, bookingHistory, getPatientProfile, getPatientProfileActivity, doctorList, historyDoctorBooking, getDoctorProfileFragment, doctorListActivity, getSpecialities, changePassword, setSymptoms
    }

    /**
     * This is for PUSH Notification
     **/

}