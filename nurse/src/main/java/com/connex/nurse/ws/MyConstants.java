package com.connex.nurse.ws;


import com.connex.nurse.model.MessageHistory;

import java.util.ArrayList;
import java.util.List;

public class MyConstants {
    //public static final String MAIN_URL = "http://old-test.doctorpocket.ca/";
   // public static final String CHANNEL = "channels";

    //public static final String MAIN_URL = "https://test-api.doctorpocket.ca/";

    // test channels
    public static final String CHANNEL = "test_channels";
    public static final String NURSE_CHANNEL = "nurse_test_channels";

    // test URLs
    public static final String MAIN_URL = "https://test-api.doctorpocket.ca/";
    public static final String BASE_URL = MAIN_URL + "api/v1/";
    public static final String NURSE_BASE_URL = MAIN_URL + "api/v1/nurse/";
    public static final String DOCTOR_BASE_URL = MAIN_URL + "api/v1/doctor/";
    public static final String GUEST_BASE_URL = MAIN_URL + "api/v1/guest/";
    public static final String NOTIFICATION_URL = MAIN_URL + "api/v1/notif/";

    // live channels
    /*public static final String CHANNEL = "live_channels";
    public static final String NURSE_CHANNEL = "nurse_live_channels";

    // live URLs
    public static final String MAIN_URL = "https://api.doctorpocket.ca/";
    public static final String BASE_URL = MAIN_URL + "api/v1/";
    public static final String NURSE_BASE_URL = MAIN_URL + "api/v1/nurse/";
    public static final String DOCTOR_BASE_URL = MAIN_URL + "api/v1/doctor/";
    public static final String GUEST_BASE_URL = MAIN_URL + "api/v1/guest/";
    public static final String NOTIFICATION_URL = MAIN_URL + "api/v1/notif/";*/

    //ws version
    public static final String WS_VERSION = "1";
    public static boolean CHAT_AS_GUEST = false;
    public static String DRAWING_PATH = "";
    // device id
    public static String DEVICE_ID = "";
    // device type = 1 for android
    public static final String DEVICE_TYPE = "1";
    public static String API_TOKEN = "";


    public static List<MessageHistory> pendingPatientList = new ArrayList<>();
    public static List<MessageHistory> myPatientList = new ArrayList<>();
    public static List<MessageHistory> completeChatList = new ArrayList<>();
    public static int TAB_POSITION;
    public static List<String> pendingPatientCount = new ArrayList<>();
    public static List<String> myPatientCount = new ArrayList<>();
    public static List<String> completeChatCount = new ArrayList<>();

    // for  Stripe
    //0--->Test key
    //1--->Production key
    public static String STRIPE_CONFIG_ENVIRONMENT = "0";
    public static String STRIPE_PUBLISH_KEY_LIVE = "";
    public static String STRIPE_PUBLISH_KEY_TEST = "pk_test_cnfziT3fFHz6bOVyoEdi6Deo00CnPSCEpa";
    public static String STRIPE_SANDBOX_KEY_LIVE = "";
    public static String STRIPE_SANDBOX_KEY_TEST = "sk_test_mQ41zqUXxRGLO6WKA2sr5fqy00uzSV4gmk";
    public static String STRIPE_MERCHANT_NAME = "ConnexMD Payment";
    public static final int REQUEST_STRIPE_PAYMENT = 2000;

    public static String uniqueChatId = "";
    public static String consultId = "";
    public static String paymentType = "";
    public static String DOCTOR_NAME = "";
    public static String DOCTOR_ID = "";
    public static String DOCTOR_PROFILEPIC = "";
    public static String isGuest = "0";
    public static boolean isBooked = false;
    public static boolean isBackPressed = false;
    public static boolean isFirstTime = true;

    public static final String IS_GUEST = "is_guest";
    public static final String PT_PASSWORD = "pt_password";
    public static final String NURSE_PASSWORD = "dr_password";
    public static final String LOGIN_TYPE = "login_type";
    public static final String FIRST_LOGIN = "first_login";
    public static final String IS_FIRST = "first";
    public static final String IS_INTRO_SEEN = "first";
    public static final String USER_TYPE = "user_type";
    public static final String USER_ID = "user_id";
    public static final String PREF = "Doctor_Pocket_Nurse";
    public static final String USER_PT = "user_PT";
    public static final String USER_DR = "user_DR";
    public static final String USER_NURSE = "user_NURSE";
    public static final String USER_EMAIL = "user_email";
    public static final String PT_ZONE = "patient_zone";
    public static final String PT_NAME = "NAME";
    public static final String PT_FIRST_NAME = "first_name";
    public static final String PT_LAST_NAME = "last_name";
    public static final String PROFILE_PIC = "profile_pic";
    public static final String FREE_CONSULT = "free_consult";
    public static final String SINCH_ID = "sinch_id";
    public static final String IS_LOGGED_IN = "is_logged_in";
    public static final String SOCIAL_ID = "social_id";

    public static final String GENDER = "gender";
    public static final String FACEBOOK_ID = "facebookid";
    public static final String MOBILE = "mobile";
    public static final String ZONE = "zone";
    public static final String DATE_OF_BIRTH = "DOB";
    public static final String STATE = "state";
    public static final String COUNTRY = "country";
    public static final String ADDRESS = "address";
    public static final String AGE = "age";
    public static final String CURRENT_DISCOUNT = "current_discount";
    public static final String WALLET_BALANCE = "wallet_balance";
    public static final String REFER_CODE = "refer_code";
    public static final String TOKEN = "token";

    public static final String UNIQUE_CHAT_ID = "unique_chat_id";
    public static final String DR_ID = "dr_id";
    public static final String PT_ID = "pt_id";
    public static final String DR_NAME = "dr_name";
    public static final String Pt_NAME = "pt_name";
    public static final String RECEIVER_IMAGE_URL = "receiver_image_url";
    public static final String IS_PAYMENT_DONE = "is_payment_done";

}
